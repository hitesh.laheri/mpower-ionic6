"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_home_home_module_ts"],{

/***/ 3467:
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePageModule": () => (/* binding */ HomePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home.page */ 62267);







const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_0__.HomePage
    }
];
let HomePageModule = class HomePageModule {
};
HomePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_0__.HomePage]
    })
], HomePageModule);



/***/ }),

/***/ 62267:
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePage": () => (/* binding */ HomePage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _home_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home.page.html?ngResource */ 91670);
/* harmony import */ var _home_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home.page.scss?ngResource */ 1020);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ 80190);
/* harmony import */ var _login_login_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../login/login.page */ 66825);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../provider/message-helper */ 98792);
/* harmony import */ var _home_home_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../home/home.service */ 86078);













let HomePage = class HomePage {
  constructor(loginauth, menuCtrl, platform, storage, msg, loginpage, router, commonfun, homeservice, zone, commonFunction) {
    this.loginauth = loginauth;
    this.menuCtrl = menuCtrl;
    this.platform = platform;
    this.storage = storage;
    this.msg = msg;
    this.loginpage = loginpage;
    this.router = router;
    this.commonfun = commonfun;
    this.homeservice = homeservice;
    this.zone = zone;
    this.commonFunction = commonFunction;
    this.isplatformweb = false;
    this.TAG = "HomePage";
    this.netsales = 0;
    this.pendingsalesorders = 0;
    this.dashboard = false;
    this.isshowdata = false;
    this.defaultDashboard = false;
    this.netSalesDashboard = false;
    this.targetSalesDashboard = false;
    this.isNewLead = false;
    this.isExistingLead = false;
    this.isBusinessPartnerAddress = false;
    this.isNewOrder = false;
    this.isDraftOrder = false;
    this.isOrderStatus = false;
    this.isLatLongFinder = false;
    this.isTravelPlan = false;
    this.isActualTravelPlan = false;
    this.isTravelExpense = false;
    this.isTravelPlanClosure = false;
    this.isApprovalAccess = false;
    this.isCustomerServiceAccess = false;
    this.isComplaintReportingAccess = false;
    this.isCompliantAcceptanceAccess = false;
    this.isFieldVisitAccess = false;
    this.isQuotationAccess = false;
    this.isQuotationApproval = false;
    this.isUpload = false;
    this.isReport = false;
    this.isArVisitSchedule = false;
    this.loginauth.getObservable().subscribe(data => {
      if (data == 'dashbaordEvent') {
        this.zone.run(() => {});
      }
    });
  }

  ngOnInit() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      //this.commonfun.chkcache('home');
      try {
        // setTimeout(() => {
        //   this.chkcust();
        //   this.isshowdata = true;
        //   this.targetSalesDashboard = true;
        //   this.approvalScreenAccess = this.loginauth.approvalScreen;
        //   this.dashboard = this.loginauth.dashboard
        //   if (this.dashboard == true)
        //     this.getdashboard()
        // }, 2000);
        _this.chkcust();

        _this.isshowdata = true; // this.targetSalesDashboard = true;

        _this.approvalScreenAccess = _this.loginauth.approvalScreen;
        _this.dashboardTypeList = _this.loginauth.roleWiseDashboard; //  console.log(this.TAG,"Dashboard List",this.dashboardTypeList);
        // this.commonFunction.loadingPresent();
        // this.dashboardTypeList = await this.homeservice.getDashboardTypeList().toPromise();
        //  console.log(this.TAG,"Dashboard Type List",this.dashboardTypeList);

        if (!!_this.dashboardTypeList) {
          let twoDefault = false;

          if (_this.dashboardTypeList.length > 1) {
            let noOneDefault = false;

            for (let i = 0; i < _this.dashboardTypeList.length; i++) {
              for (let i = 0; i < _this.dashboardTypeList.length; i++) {
                if (_this.dashboardTypeList[i].default == "N") {
                  noOneDefault = true;
                } else {
                  noOneDefault = false;
                }
              }

              if (_this.dashboardTypeList[i].default == "Y" && twoDefault == false) {
                twoDefault = true;

                if (_this.dashboardTypeList[i].code == "HP") {
                  _this.defaultDashboard = true;
                  _this.targetSalesDashboard = false;
                  _this.netSalesDashboard = false;
                  _this.selectedDashboard = _this.dashboardTypeList[i];
                } else if (_this.dashboardTypeList[i].code == "ST") {
                  _this.targetSalesDashboard = true;
                  _this.defaultDashboard = false;
                  _this.netSalesDashboard = false;
                  _this.selectedDashboard = _this.dashboardTypeList[i];
                } else if (_this.dashboardTypeList[i].code == "SDB") {
                  _this.netSalesDashboard = true;
                  _this.targetSalesDashboard = false;
                  _this.defaultDashboard = false;

                  _this.getdashboard();

                  _this.selectedDashboard = _this.dashboardTypeList[i];
                }
              } else if (_this.dashboardTypeList.length > 1 && noOneDefault == true) {
                _this.selectedDashboard = _this.dashboardTypeList[0];

                if (_this.dashboardTypeList[0].code == "HP") {
                  _this.defaultDashboard = true;
                  _this.targetSalesDashboard = false;
                  _this.netSalesDashboard = false;
                  _this.selectedDashboard = _this.dashboardTypeList[0];
                } else if (_this.dashboardTypeList[0].code == "ST") {
                  _this.targetSalesDashboard = true;
                  _this.defaultDashboard = false;
                  _this.netSalesDashboard = false;
                  _this.selectedDashboard = _this.dashboardTypeList[0];
                } else if (_this.dashboardTypeList[0].code == "SDB") {
                  _this.netSalesDashboard = true;
                  _this.targetSalesDashboard = false;
                  _this.defaultDashboard = false;

                  _this.getdashboard();

                  _this.selectedDashboard = _this.dashboardTypeList[0];
                }
              }
            }
          }

          if (_this.dashboardTypeList.length == 1) {
            _this.selectedDashboard = _this.dashboardTypeList[0];

            if (_this.dashboardTypeList[0].code == "HP") {
              _this.defaultDashboard = true;
              _this.targetSalesDashboard = false;
              _this.netSalesDashboard = false;
              _this.selectedDashboard = _this.dashboardTypeList[0];
            } else if (_this.dashboardTypeList[0].code == "ST") {
              _this.targetSalesDashboard = true;
              _this.defaultDashboard = false;
              _this.netSalesDashboard = false;
              _this.selectedDashboard = _this.dashboardTypeList[0];
            } else if (_this.dashboardTypeList[0].code == "SDB") {
              _this.netSalesDashboard = true;
              _this.targetSalesDashboard = false;
              _this.defaultDashboard = false;

              _this.getdashboard();

              _this.selectedDashboard = _this.dashboardTypeList[0];
            }
          }
        } else {
          _this.defaultDashboard = true;
        } //   this.commonFunction.loadingDismiss();

      } catch (error) {
        _this.defaultDashboard = true; //  this.commonFunction.loadingDismiss();
        //  console.log("Error: Home ngonit:", error);
      }
    })();
  }

  ionViewWillEnter() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // this.menuCtrl.enable(false);
      //  console.log("ionViewWillEnter");
      _this2.approvalScreenAccess = _this2.loginauth.approvalScreen;
      _this2.isNewLead = _this2.loginauth.isNewLead;
      _this2.isExistingLead = _this2.loginauth.isExistingLead;
      _this2.isBusinessPartnerAddress = _this2.loginauth.isBusinessPartnerAddress;
      _this2.isNewOrder = _this2.loginauth.isNewOrder;
      _this2.isDraftOrder = _this2.loginauth.isDraftOrder;
      _this2.isOrderStatus = _this2.loginauth.isOrderStatus;
      _this2.isLatLongFinder = _this2.loginauth.isLatLongFinder;
      ;
      _this2.isTravelPlan = _this2.loginauth.isTravelPlan;
      _this2.isActualTravelPlan = _this2.loginauth.isActualTravelPlan;
      _this2.isTravelExpense = _this2.loginauth.isTravelExpense;
      _this2.isTravelPlanClosure = _this2.loginauth.isTravelPlanClosure;
      _this2.isApprovalAccess = _this2.loginauth.isApprovalAccess;
      _this2.isCustomerServiceAccess = _this2.loginauth.isCustomerServiceAccess;
      _this2.isComplaintReportingAccess = _this2.loginauth.isComplaintReportingAccess;
      _this2.isCompliantAcceptanceAccess = _this2.loginauth.isCompliantAcceptanceAccess;
      _this2.isFieldVisitAccess = _this2.loginauth.isFieldVisitAccess;
      _this2.isQuotationAccess = _this2.loginauth.isQuotationAccess;
      _this2.isQuotationApproval = _this2.loginauth.isQuotationApproval;
      _this2.isUpload = _this2.loginauth.isUpload;
      _this2.isReport = _this2.loginauth.isReport;
      _this2.isArVisitSchedule = _this2.loginauth.isARVisitSchedule;
      _this2.dashboard = _this2.loginauth.dashboard;

      if (_this2.dashboard == true) {
        _this2.getdashboard();
      }

      try {
        if (_this2.targetSalesDashboard == true) {
          _this2.commonFunction.loadingPresent();

          _this2.targetSalesMonthList = yield _this2.homeservice.getTargetSalesPeriod().toPromise(); //  console.log(this.TAG,"Target Sales Month List",this.targetSalesMonthList);

          _this2.commonFunction.loadingDismiss();
        }
      } catch (error) {
        _this2.commonFunction.loadingDismiss();
      }
    })();
  }

  chkcust() {
    try {
      if (this.loginauth.defaultprofile != null && this.loginauth.defaultprofile != undefined) {
        this.userName = this.loginauth.defaultprofile[0].name ? this.loginauth.defaultprofile[0].name : 'Demo User';

        if (this.loginauth.defaultprofile[0].mmstOrderusrtype === "CEB") {
          this.checkcust = false;
        }

        if (this.loginauth.defaultprofile[0].mmstOrderusrtype !== "CEB") {
          this.checkcust = true;
        } // if(!this.platform.is("desktop")){


        if (!this.msg.isplatformweb) {
          this.isplatformweb = false;
        } else {
          this.isplatformweb = true;
        }
      } else {
        this.router.navigateByUrl('/login');
      }
    } catch (error) {}
  }

  getdashboard() {
    try {
      // this.homeservice.getdashboardapi()
      this.homeservice.getdashboardapi1().subscribe(data => {
        const response = data['response'];
        var jsondata = response.data; //get month

        this.homeservice.getdashboardapi(jsondata[0].id).subscribe(data2 => {
          const response2 = data2['response'];
          this.monthlist = response2.data;
          this.selectedaMonth = this.monthlist[0];
        }, error => {
          this.commonfun.presentAlert("Message", "Error", error); //  console.log("getdashboardapi:loadingDismiss");
        }); //get month
      }, error => {
        this.commonfun.presentAlert("Message", "Error", error); //  console.log("getdashboardapi1:loadingDismiss");
      });
    } catch (error) {}
  }

  onMonthChange() {
    try {
      // console.log("this.selectedaMonth",this.selectedaMonth);
      this.homeservice.CustomerWiseSalesInfo(this.selectedaMonth.code).subscribe(data => {
        const response = data;
        this.netsales = response.netamt;
        this.pendingsalesorders = response.pendingordervalue; // this.selectedaMonth=this.monthlist[this.monthlist.length-1]
      }, error => {// this.commonfun.presentAlert("Message", "Error", error);
        //  console.log("CustomerWiseSalesInfo", error);
      });
    } catch (error) {}
  }

  onDashboardSelectChange(selectedDashboard) {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        if (selectedDashboard.code == "HP") {
          _this3.defaultDashboard = true;
          _this3.targetSalesDashboard = false;
          _this3.netSalesDashboard = false;
        } else if (selectedDashboard.code == "ST") {
          _this3.targetSalesDashboard = true;
          _this3.defaultDashboard = false;
          _this3.netSalesDashboard = false;

          try {
            _this3.commonFunction.loadingPresent();

            _this3.targetSalesMonthList = yield _this3.homeservice.getTargetSalesPeriod().toPromise(); //  console.log(this.TAG,"Target Sales Month List",this.targetSalesMonthList);

            _this3.commonFunction.loadingDismiss();
          } catch (error) {
            _this3.commonFunction.loadingDismiss();
          }
        } else if (selectedDashboard.code == "SDB") {
          _this3.netSalesDashboard = true;
          _this3.targetSalesDashboard = false;
          _this3.defaultDashboard = false;

          _this3.getdashboard();
        }
      } catch (error) {//  console.log(this.TAG,error);
      }
    })();
  }

  onTargetSalesMonthChange(selectedTargetSalesMonth) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this4.commonFunction.loadingPresent();

        _this4.selectedTargetSalesMonthData = yield _this4.homeservice.getTargetSalesPeriodData(selectedTargetSalesMonth).toPromise(); // console.log(this.TAG,"onTargetSalesMonthChange Data",this.selectedTargetSalesMonthData);
        //  console.log(this.TAG,"onTargetSalesMonthChange Data",this.selectedTargetSalesMonthData);

        if (!!_this4.selectedTargetSalesMonthData) {
          _this4.salesTarget = _this4.selectedTargetSalesMonthData[0].salestarget;
          _this4.salesAchievement = _this4.selectedTargetSalesMonthData[0].salesachievement;
          _this4.varianceValue = _this4.selectedTargetSalesMonthData[0].variancevalue;
          _this4.achievementVariance = _this4.selectedTargetSalesMonthData[0].achmntveriance; //  console.log(this.TAG,"Binded Target DATA",this.salesTarget);
        } else {
          _this4.salesTarget = "";
          _this4.salesAchievement = "";
          _this4.varianceValue = "";
          _this4.achievementVariance = "";
        }

        _this4.commonFunction.loadingDismiss();
      } catch (error) {
        _this4.salesTarget = "";
        _this4.salesAchievement = "";
        _this4.varianceValue = "";
        _this4.achievementVariance = "";

        _this4.commonFunction.loadingDismiss(); //  console.log(this.TAG,error);

      }
    })();
  }

};

HomePage.ctorParameters = () => [{
  type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__.LoginauthService
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.MenuController
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.Platform
}, {
  type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__.Storage
}, {
  type: _provider_message_helper__WEBPACK_IMPORTED_MODULE_7__.Message
}, {
  type: _login_login_page__WEBPACK_IMPORTED_MODULE_5__.LoginPage
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_10__.Router
}, {
  type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_6__.Commonfun
}, {
  type: _home_home_service__WEBPACK_IMPORTED_MODULE_8__.HomeService
}, {
  type: _angular_core__WEBPACK_IMPORTED_MODULE_11__.NgZone
}, {
  type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_6__.Commonfun
}];

HomePage = (0,tslib__WEBPACK_IMPORTED_MODULE_12__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.Component)({
  selector: 'app-home',
  template: _home_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_home_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], HomePage);


/***/ }),

/***/ 86078:
/*!**************************************!*\
  !*** ./src/app/home/home.service.ts ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomeService": () => (/* binding */ HomeService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);





let HomeService = class HomeService {
    constructor(http, loginauth, genericHTTP) {
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
        this.TAG = "Dashboard Service";
    }
    getdashboardapi1() {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/mmst_lov?'
            + '_where=scode=\'IonicML\'', false, true);
    }
    getdashboardapi(id) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/mmst_lov_val?'
            + '_selectedProperties=id,code&'
            + '_where=mmstLov%20IN(\'' + id + '\')'
            + '&_orderby=Created%20Desc', false, true);
    }
    CustomerWiseSalesInfo(date) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.CustomerWiseSalesInfo?'
            + 'date=' + date
            + '&userid=' + this.loginauth.userid
            + '&activity=' + this.loginauth.selectedactivity.id);
    }
    getDashboardTypeList() {
        try {
            let URL = this.loginauth.commonurl + 'ws/in.mbs.webservice.DashBoardSalesTarget?';
            'userid=' + this.loginauth.userid
                + '&activityid=' + this.loginauth.selectedactivity.id;
            return this.genericHTTP.get(URL);
        }
        catch (error) {
            //  console.log(this.TAG,error);
        }
    }
    getTargetSalesPeriod() {
        try {
            let URL = this.loginauth.commonurl + 'ws/in.mbs.webservice.DashBoardSalesTarget?'
                + 'userid=' + this.loginauth.userid
                + '&activityid=' + this.loginauth.selectedactivity.id;
            return this.genericHTTP.get(URL);
        }
        catch (error) {
            //  console.log(this.TAG,error);
        }
    }
    getTargetSalesPeriodData(selectedTargetSalesMonth) {
        try {
            let getTargetSalesPeriodDataURL = this.loginauth.commonurl + 'ws/in.mbs.webservice.DashBoardSalesTarget?'
                + 'userid=' + this.loginauth.userid
                + '&activityid=' + this.loginauth.selectedactivity.id
                + '&mmis_target_id=' + selectedTargetSalesMonth.mmis_target_id;
            //  console.log(this.TAG,"getTargetSalesPeriodData URL",getTargetSalesPeriodDataURL);
            return this.genericHTTP.get(getTargetSalesPeriodDataURL);
        }
        catch (error) {
            //  console.log(this.TAG,error);
        }
    }
};
HomeService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService }
];
HomeService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], HomeService);



/***/ }),

/***/ 1020:
/*!************************************************!*\
  !*** ./src/app/home/home.page.scss?ngResource ***!
  \************************************************/
/***/ ((module) => {

module.exports = "ion-cart {\n  background-color: powderblue;\n}\n\nion-icon {\n  font-size: 6em;\n}\n\nion-card {\n  margin-top: 3px !important;\n  margin-left: 3px !important;\n  margin-right: 3px !important;\n  margin-bottom: 3px !important;\n}\n\n.dashboard-count-card {\n  height: 100%;\n}\n\nion-content {\n  --background:f5f5f5;\n}\n\nh5 {\n  font-style: normal !important;\n  color: black !important;\n  font-size: large !important;\n}\n\n.dashboard-keywidgets {\n  font-size: large;\n  font-weight: 700;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksNEJBQUE7QUFDSjs7QUFDQTtFQUNJLGNBQUE7QUFFSjs7QUFBQTtFQUNJLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0FBR0o7O0FBQUE7RUFDSSxZQUFBO0FBR0o7O0FBQUE7RUFDSSxtQkFBQTtBQUdKOztBQURBO0VBQ0ksNkJBQUE7RUFDQSx1QkFBQTtFQUNBLDJCQUFBO0FBSUo7O0FBREU7RUFDSSxnQkFBQTtFQUNBLGdCQUFBO0FBSU4iLCJmaWxlIjoiaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FydHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHBvd2RlcmJsdWU7XHJcbn1cclxuaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiA2ZW07XHJcbn1cclxuaW9uLWNhcmR7XHJcbiAgICBtYXJnaW4tdG9wOiAzcHggIWltcG9ydGFudDtcclxuICAgIG1hcmdpbi1sZWZ0OiAzcHggIWltcG9ydGFudDtcclxuICAgIG1hcmdpbi1yaWdodDogM3B4ICFpbXBvcnRhbnQ7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzcHggIWltcG9ydGFudDtcclxuICAgIFxyXG59XHJcbi5kYXNoYm9hcmQtY291bnQtY2FyZHtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG5cclxuaW9uLWNvbnRlbnR7XHJcbiAgICAtLWJhY2tncm91bmQgOmY1ZjVmNTtcclxufVxyXG5oNXtcclxuICAgIGZvbnQtc3R5bGU6bm9ybWFsIWltcG9ydGFudDtcclxuICAgIGNvbG9yOiBibGFjayFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXNpemU6IGxhcmdlIWltcG9ydGFudDtcclxuICAgIFxyXG4gIH1cclxuICAuZGFzaGJvYXJkLWtleXdpZGdldHN7XHJcbiAgICAgIGZvbnQtc2l6ZTogbGFyZ2U7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcblxyXG4gICAgICAgfSJdfQ== */";

/***/ }),

/***/ 91670:
/*!************************************************!*\
  !*** ./src/app/home/home.page.html?ngResource ***!
  \************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title size=\"large\" style=\"text-align:center\">\n      MPOWER\n    </ion-title>\n\n  </ion-toolbar>\n\n</ion-header>\n\n<ion-content>\n\n\n  <ion-grid fixed *ngIf=\"isshowdata\">\n    <ion-row class=\"ion-no-padding\">\n      <ion-col class=\"ion-no-padding\">\n        <ion-card color=\"white\">\n          <ion-row>\n            <ion-col size=\"4\">\n              <ion-icon src=\"./assets/profile.svg\"></ion-icon>\n            </ion-col>\n            <ion-col>\n              <h4>Welcome <br>{{userName}}</h4>\n            </ion-col>\n          </ion-row>\n\n\n        </ion-card>\n      </ion-col>\n\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"ion-no-padding\">\n        <ion-card color=\"white\">\n          <ion-item>\n            <ion-label position=\"stacked\">Dashboard Type<span style=\"color:red!important\">*</span></ion-label>\n            <ion-select [(ngModel)]=\"selectedDashboard\" (ionChange)=\"onDashboardSelectChange(selectedDashboard)\" interface=\"popover\"\n              multiple=\"false\" placeholder=\"Select Dashboard Type\">\n              <ion-select-option *ngFor=\"let dashboard of dashboardTypeList\" [value]=\"dashboard\">{{dashboard.value}}\n              </ion-select-option>\n            </ion-select>\n          </ion-item>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <!-- Default Dashboard -->\n    <section *ngIf=\"defaultDashboard\">\n      <ion-row class=\"ion-no-padding\">\n        <ion-col col-6 class=\"ion-no-padding\" *ngIf=\"loginauth?.isNewLead\">\n          <ion-card style=\"text-align:center\" color=\"white\" [routerDirection]=\"'root'\" [routerLink]=\"['/newcustomer']\">\n            <ion-icon color=\"success\" src=\"./assets/existing_lead.svg\"></ion-icon>\n            <h5>New Lead</h5>\n          </ion-card>\n        </ion-col>\n        <ion-col col-6 class=\"ion-no-padding\" *ngIf=\"loginauth?.isExistingLead\">\n          <ion-card style=\"text-align:center\" color=\"white\" [routerDirection]=\"'root'\" [routerLink]=\"['/existingcustomer']\">\n            <ion-icon src=\"./assets/existing_lead_1.svg\"></ion-icon>\n            <h5>Existing Lead</h5>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n      <ion-row no-padding>\n        <ion-col col-6 class=\"ion-no-padding\" *ngIf=\"loginauth?.isNewOrder\">\n          <ion-card style=\"text-align:center\" color=\"white\" [routerDirection]=\"'root'\" [routerLink]=\"['/neworder']\">\n            <ion-icon src=\"./assets/new_order.svg\"></ion-icon>\n            <h5>New Order</h5>\n          </ion-card>\n        </ion-col>\n        <ion-col col-6 class=\"ion-no-padding\" *ngIf=\"loginauth?.isDraftOrder\">\n          <ion-card style=\"text-align:center\" color=\"white\" [routerDirection]=\"'root'\" [routerLink]=\"['/existingorder']\">\n            <ion-icon src=\"./assets/existing_order.svg\"></ion-icon>\n            <h5>Draft Order</h5>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"ion-no-padding\">\n        <ion-col col-6 class=\"ion-no-padding\">\n          <ion-card style=\"text-align:center\" color=\"white\" [routerDirection]=\"'root'\" [routerLink]=\"['/settings']\">\n            <ion-icon src=\"./assets/settings.svg\"></ion-icon>\n            <h5>Settings</h5>\n          </ion-card>\n        </ion-col>\n        <ion-col col-6 class=\"ion-no-padding\" *ngIf=\"loginauth?.isOrderStatus\">\n\n          <ion-card style=\"text-align:center\" color=\"white\" [routerDirection]=\"'root'\" [routerLink]=\"['/order-status']\">\n            <ion-icon src=\"./assets/order_status.svg\"></ion-icon>\n            <h5>Order Status</h5>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n     <ion-row class=\"ion-no-padding\" *ngIf=\"loginauth?.isBusinessPartnerAddress\">\n        <ion-col col-6 class=\"ion-no-padding\">\n          <ion-card style=\"text-align:center\" color=\"white\" [routerDirection]=\"'root'\" [routerLink]=\"['/business-partner-address']\">\n            <ion-icon src=\"./assets/business-partner-address.svg\"></ion-icon>\n            <h5>Business Partner Address</h5>\n          </ion-card>\n        </ion-col>\n        <ion-col *ngIf=\"loginauth?.approvalScreen\" col-6 class=\"ion-no-padding\" [routerDirection]=\"'root'\"\n          [routerLink]=\"['/order-approval']\">\n          <ion-card style=\"text-align:center\" color=\"white\">\n            <ion-icon src=\"./assets/order_confirmation.svg\"></ion-icon>\n            <h5>Approval</h5>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"ion-no-padding\" *ngIf=\"loginauth?.isARVisitSchedule\">\n        <ion-col col-6 class=\"ion-no-padding\">\n          <ion-card style=\"text-align:center\" color=\"white\" [routerDirection]=\"'root'\" [routerLink]=\"['/arvisitschedule']\">\n            <ion-icon src=\"./assets/artracker.svg\"></ion-icon>\n            <h5>AR Visit Schedule</h5>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </section>\n    <!-- Net Sales Dashboard -->\n    <section *ngIf=\"netSalesDashboard\">\n      <ion-row class=\"ion-no-padding\">\n        <ion-col class=\"ion-no-padding\">\n          <ion-card color=\"white\">\n            <ion-item>\n              <ion-label position=\"stacked\">Month<span style=\"color:red!important\">*</span></ion-label>\n              <ion-select [(ngModel)]=\"selectedaMonth\" (ionChange)=\"onMonthChange()\" interface=\"popover\"\n                multiple=\"false\" placeholder=\"Select Month\">\n                <ion-select-option *ngFor=\"let selectedaMonth of monthlist\" [value]=\"selectedaMonth\">{{selectedaMonth._identifier}}\n                </ion-select-option>\n              </ion-select>\n            </ion-item>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n\n\n\n\n      <ion-row class=\"ion-no-padding\">\n        <ion-col col-6 class=\"ion-no-padding\">\n          <ion-card style=\"text-align:center\" class=\"dashboard-count-card\">\n            <br />\n            <ion-label class=\"dashboard-keywidgets\">&#8377;{{netsales}}</ion-label>\n            <br />\n            <br />\n            <ion-label>NET SALES</ion-label>\n\n          </ion-card>\n        </ion-col>\n        <!-- <ion-col col-6 class=\"ion-no-padding\" >\n        <ion-card text-center class=\"dashboard-count-card\">\n          <br />\n          <ion-label  class=\"dashboard-keywidgets\">&#8377;{{pendingsalesorders}}</ion-label>\n          <br />\n          <br />\n          <ion-label>PENDING SALES ORDER VALUE</ion-label>\n        </ion-card>\n      </ion-col> -->\n      </ion-row>\n\n    </section>\n    <section *ngIf=\"targetSalesDashboard\">\n      <ion-row class=\"ion-no-padding\">\n        <ion-col class=\"ion-no-padding\">\n          <ion-card color=\"white\">\n            <ion-item>\n              <ion-label position=\"stacked\">Period<span style=\"color:red!important\">*</span></ion-label>\n              <ion-select [(ngModel)]=\"selectedTargetSalesMonth\" (ionChange)=\"onTargetSalesMonthChange(selectedTargetSalesMonth)\" interface=\"popover\"\n                multiple=\"false\" placeholder=\"Select Period\">\n                <ion-select-option *ngFor=\"let targetSalesMonth of targetSalesMonthList\" [value]=\"targetSalesMonth\">{{targetSalesMonth.period}}\n                </ion-select-option>\n              </ion-select>\n            </ion-item>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n      \n      <ion-row>\n        <ion-col>\n        <ion-card color=\"white\">\n          <ion-item>\n            <!-- <ion-label>Sales Target : {{selectedTargetSalesMonthData?.salestarget}}</ion-label> -->\n            <ion-label>Sales Target : {{salesTarget}}</ion-label>\n          </ion-item>\n          <ion-item>\n            <ion-label>Sales Achievement : {{salesAchievement}}</ion-label>\n          </ion-item>\n          <ion-item>\n            <ion-label>Variance Value : {{varianceValue}}</ion-label>\n          </ion-item>\n          <ion-item>\n            <ion-label>Achievement Variance % : {{achievementVariance}}</ion-label>\n          </ion-item>\n        </ion-card>\n      </ion-col>  \n      </ion-row>\n\n    </section>\n\n  </ion-grid>\n\n\n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_home_home_module_ts.js.map