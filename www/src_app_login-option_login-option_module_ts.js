"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_login-option_login-option_module_ts"],{

/***/ 58845:
/*!*****************************************************!*\
  !*** ./src/app/login-option/login-option.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginOptionPageModule": () => (/* binding */ LoginOptionPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _login_option_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-option.page */ 61645);







const routes = [
    {
        path: '',
        component: _login_option_page__WEBPACK_IMPORTED_MODULE_0__.LoginOptionPage
    }
];
let LoginOptionPageModule = class LoginOptionPageModule {
};
LoginOptionPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_login_option_page__WEBPACK_IMPORTED_MODULE_0__.LoginOptionPage]
    })
], LoginOptionPageModule);



/***/ }),

/***/ 61645:
/*!***************************************************!*\
  !*** ./src/app/login-option/login-option.page.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginOptionPage": () => (/* binding */ LoginOptionPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _login_option_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-option.page.html?ngResource */ 65448);
/* harmony import */ var _login_option_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login-option.page.scss?ngResource */ 86746);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);






let LoginOptionPage = class LoginOptionPage {
    constructor(loginauth, router) {
        this.loginauth = loginauth;
        this.router = router;
        this.mmstReward = false;
        this.mmstOrdercaptureapp = false;
        this.noaccessmsg = "";
    }
    ngOnInit() {
        this.SelectOption();
    }
    SelectOption() {
        try {
            this.mmstReward = this.loginauth.defaultprofile[0].mmstReward;
            this.mmstOrdercaptureapp = this.loginauth.defaultprofile[0].mmstOrdercaptureapp;
            if (this.mmstReward != this.mmstOrdercaptureapp) {
                if (this.mmstReward) {
                    this.onclickLoginTypevet('vet');
                }
                else {
                    this.onclickLoginTypewms('wms');
                }
            }
            if (this.mmstReward == false && this.mmstOrdercaptureapp == false) {
                this.noaccessmsg = "You don't have any access.";
                this.loginauth.defaultprofile = null;
            }
            else {
                this.noaccessmsg = "";
            }
        }
        catch (error) {
        }
    }
    onclickLoginTypewms(type) {
        this.loginauth.logintype = type;
        //if(this.loginauth.defaultprofile[0].mmstOrderusrtype=="CEB")
        this.router.navigate(['terms-of-agreement']);
        //else
        //this.router.navigate(['home']); 
    }
    onclickLoginTypevet(type) {
        this.loginauth.logintype = type;
        this.router.navigate(['use-vetcoins']);
    }
};
LoginOptionPage.ctorParameters = () => [
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router }
];
LoginOptionPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-login-option',
        template: _login_option_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_login_option_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], LoginOptionPage);



/***/ }),

/***/ 86746:
/*!****************************************************************!*\
  !*** ./src/app/login-option/login-option.page.scss?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsb2dpbi1vcHRpb24ucGFnZS5zY3NzIn0= */";

/***/ }),

/***/ 65448:
/*!****************************************************************!*\
  !*** ./src/app/login-option/login-option.page.html?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Login Option</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-row>\n   \n    <p style=\"text-align: center;\">{{noaccessmsg}}</p>\n    <ion-col *ngIf='mmstOrdercaptureapp'>\n      <ion-card style=\"background-color: powderblue;\" (click)=\"onclickLoginTypewms('wms')\">\n        <ion-card-content>\n         \n              <ion-card-title style=\"text-align: center;\">Order Punching</ion-card-title>\n            \n        </ion-card-content>\n      </ion-card>\n    </ion-col>\n      <ion-col *ngIf='mmstReward'>\n        <ion-card style=\"background-color: powderblue;\" (click)=\"onclickLoginTypevet('vet')\">\n          <ion-card-content>\n            <ion-card-title style=\"text-align: center;\">Reward</ion-card-title>\n\n            \n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n     \n  </ion-row>\n\n\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_login-option_login-option_module_ts.js.map