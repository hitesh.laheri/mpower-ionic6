"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_arvisitschedule_arvisitschedule_module_ts"],{

/***/ 70570:
/*!***********************************************************!*\
  !*** ./src/app/arvisitschedule/arvisitschedule.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ArvisitschedulePageModule": () => (/* binding */ ArvisitschedulePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _aruserselect_aruserselect_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./aruserselect/aruserselect.page */ 11090);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _arvisitschedule_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./arvisitschedule.page */ 49229);









const routes = [
    {
        path: '',
        component: _arvisitschedule_page__WEBPACK_IMPORTED_MODULE_1__.ArvisitschedulePage
    }
];
let ArvisitschedulePageModule = class ArvisitschedulePageModule {
};
ArvisitschedulePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterModule.forChild(routes),
            ionic_selectable__WEBPACK_IMPORTED_MODULE_8__.IonicSelectableModule
        ],
        declarations: [_arvisitschedule_page__WEBPACK_IMPORTED_MODULE_1__.ArvisitschedulePage, _aruserselect_aruserselect_page__WEBPACK_IMPORTED_MODULE_0__.AruserselectPage],
        entryComponents: [_aruserselect_aruserselect_page__WEBPACK_IMPORTED_MODULE_0__.AruserselectPage]
    })
], ArvisitschedulePageModule);



/***/ }),

/***/ 49229:
/*!*********************************************************!*\
  !*** ./src/app/arvisitschedule/arvisitschedule.page.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ArvisitschedulePage": () => (/* binding */ ArvisitschedulePage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _arvisitschedule_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./arvisitschedule.page.html?ngResource */ 25881);
/* harmony import */ var _arvisitschedule_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./arvisitschedule.page.scss?ngResource */ 99803);
/* harmony import */ var _aruserselect_aruserselect_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./aruserselect/aruserselect.page */ 11090);
/* harmony import */ var _arvisitschedule_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./arvisitschedule.service */ 48598);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! date-fns */ 86527);












let ArvisitschedulePage = class ArvisitschedulePage {
  constructor(commonfun, arvisitScheduleService, router, loginauth, alertController, modalController) {
    this.commonfun = commonfun;
    this.arvisitScheduleService = arvisitScheduleService;
    this.router = router;
    this.loginauth = loginauth;
    this.alertController = alertController;
    this.modalController = modalController;
    this.isemptyList = false;
    this.offset = 0;
    this.totalRows = 0;
    this.sorder = 1;
    this.offsetcust = 0;
    this.offsetact = 0;
    this.offsetcttv = 0;
    this.isshowfilter = true;
    this.datefrom = '';
    this.dateto = '';
  }

  ngOnInit() {//this.bindDataList();
  }

  ionViewWillEnter() {
    this.offset = 0;
    this.bindDataList();
  }

  formatDate(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_7__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_8__["default"])(value), 'dd-MM-yyyy');
  }

  formatDate1(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_7__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_8__["default"])(value), 'dd-MM-yyyy');
  }

  onToggleFilter() {
    this.isshowfilter = !this.isshowfilter;
  }

  onClickSearch(event) {
    this.offset = 0;
    this.bindDataList();
  }

  onClickClear(event) {
    this.selectedcustomer = undefined;
    this.selectedfromdate = undefined;
    this.selectedtodate = undefined;
    this.selectedstatus = undefined;
    this.selectedactivitylist = undefined;
    this.selectedcttvlist = undefined;
  }

  oncustomerClose(event) {
    if (event !== undefined) event.component.searchText = "";
    this.searchcustomer = "";
  }

  onSearchCustomer(event) {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.searchcustomer = event.text; //.replace(/\s/g,'');

      yield _this.bindCustomer(_this.searchcustomer, 0);
    })();
  }

  bindCustomer(searchcustomer, offsetcust) {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let body = {
          action: "getcustomerforplan",
          offset: offsetcust,
          search: searchcustomer
        };

        _this2.arvisitScheduleService.getArVisitPlan(body).subscribe(data => {
          var response = data;
          _this2.customerlist = response.data;
          _this2.custtotalrow = response.totalRows; //console.log(this.listofitem);
        });
      } catch (error) {
        console.log("getcustomerforplan", error);
      }
    })();
  }

  doInfiniteCust(event) {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let text = (event.text || '').trim().toLowerCase();

        if (_this3.offsetcust < _this3.custtotalrow && _this3.custtotalrow > 20) {
          _this3.offsetcust = _this3.offsetcust + 20;
          var body = {
            action: "getcustomerforplan",
            offset: _this3.offsetcust,
            search: text
          };
          var tempData = yield (yield _this3.arvisitScheduleService.getArVisitPlan(body)).toPromise();

          if (!!tempData) {
            _this3.customerlist = _this3.customerlist.concat(tempData['data']);
            event.component.endInfiniteScroll();
          }
        } else {
          event.component.endInfiniteScroll();
          event.component.disableInfiniteScroll();
        }

        if (_this3.offsetcust > _this3.custtotalrow) {
          event.component.disableInfiniteScroll();
          return;
        }
      } catch (error) {}
    })();
  }

  onActivityClose(event) {
    if (event !== undefined) event.component.searchText = "";
    this.searchact = "";
  }

  onSearchActivity(event) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this4.searchcustomer = event.text; //.replace(/\s/g,'');

      yield _this4.bindActivity(_this4.searchcustomer, 0);
    })();
  }

  bindActivity(searchact, offsetact) {
    var _this5 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let body = {
          action: "getactivityforcurrentmonth",
          offset: offsetact,
          search: searchact
        };

        _this5.arvisitScheduleService.getArVisitPlan(body).subscribe(data => {
          var response = data;
          _this5.orgactlist = response.data;
          _this5.acttotalrow = response.totalRows; //console.log(this.listofitem);
        });
      } catch (error) {
        console.log("getcustomerforplan", error);
      }
    })();
  }

  doInfiniteActivity(event) {
    var _this6 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let text = (event.text || '').trim().toLowerCase();

        if (_this6.offsetact < _this6.acttotalrow && _this6.acttotalrow > 20) {
          _this6.offsetact = _this6.offsetact + 20;
          var body = {
            action: "getactivityforcurrentmonth",
            offset: _this6.offsetact,
            search: text
          };
          var tempData = yield (yield _this6.arvisitScheduleService.getArVisitPlan(body)).toPromise();

          if (!!tempData) {
            _this6.orgactlist = _this6.orgactlist.concat(tempData['data']);
            event.component.endInfiniteScroll();
          }
        } else {
          event.component.endInfiniteScroll();
          event.component.disableInfiniteScroll();
        }

        if (_this6.offsetact > _this6.acttotalrow) {
          event.component.disableInfiniteScroll();
          return;
        }
      } catch (error) {}
    })();
  }

  onLocationClose(event) {
    if (event !== undefined) event.component.searchText = "";
    this.searchcttv = "";
  }

  onSearchLocation(event) {
    var _this7 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this7.searchcustomer = event.text; //.replace(/\s/g,'');

      yield _this7.bindLocation(_this7.searchcustomer, 0);
    })();
  }

  bindLocation(searchcttv, offsetcttv) {
    var _this8 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let body = {
          action: "getcityofcurrentmonth",
          offset: offsetcttv,
          search: searchcttv
        };

        _this8.arvisitScheduleService.getArVisitPlan(body).subscribe(data => {
          var response = data;
          _this8.cttvlist = response.data;
          _this8.cttvtotalrow = response.totalRows; //console.log(this.listofitem);
        });
      } catch (error) {
        console.log("getcityofcurrentmonth", error);
      }
    })();
  }

  doInfiniteLocation(event) {
    var _this9 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let text = (event.text || '').trim().toLowerCase();

        if (_this9.offsetcttv < _this9.cttvtotalrow && _this9.cttvtotalrow > 20) {
          _this9.offsetcttv = _this9.offsetcttv + 20;
          var body = {
            action: "getcityofcurrentmonth",
            offset: _this9.offsetact,
            search: text
          };
          var tempData = yield (yield _this9.arvisitScheduleService.getArVisitPlan(body)).toPromise();

          if (!!tempData) {
            _this9.cttvlist = _this9.cttvlist.concat(tempData['data']);
            event.component.endInfiniteScroll();
          }
        } else {
          event.component.endInfiniteScroll();
          event.component.disableInfiniteScroll();
        }

        if (_this9.offsetcttv > _this9.cttvtotalrow) {
          event.component.disableInfiniteScroll();
          return;
        }
      } catch (error) {}
    })();
  }

  onorgchange(event) {
    //console.log("selectedOrg:", event.value)
    this.selectedcustomer = event.value;
  }

  onToggelGrid(plan) {
    plan.isplusminus = !plan.isplusminus;
  }

  openChecklist(plan) {
    this.commonfun.loadingPresent(); // check for section

    this.sorder = plan.status === 'SCP' ? 1 : 2;
    var body = {
      "action": "getquestionaryforartracker",
      "sectionorder": this.sorder,
      "orgactid": this.loginauth.selectedactivity.id
    };
    this.arvisitScheduleService.getArVisitPlan(body).subscribe(data => {
      this.commonfun.loadingDismiss();
      var response = data;

      if (response.data) {
        this.arvisitScheduleService.visitplanforchecklist = plan; //this.arvisitScheduleService.selectedorg='0';

        this.arvisitScheduleService.selectedinspection = response.data[0];
        this.router.navigateByUrl('/section');
      }
    });
  }

  selectMember(plan) {// console.log(plan);
  }

  AcceptPlan() {
    //get all checkded list
    try {
      let acceptedlist = this.planlist.filter(plan => plan.ischecked);

      if (acceptedlist.length === 0) {
        this.commonfun.presentAlert("Message", "Error", "Please Select Atleast One Record.");
        return;
      }

      let acceptedlistInvalid = this.planlist.filter(plan => plan.ischecked && plan.status !== 'ACP');

      if (acceptedlistInvalid.length > 0) {
        this.commonfun.presentAlert("Message", "Error", " Selected record is already marked 'Accept'");
        return;
      } //console.log(acceptedlist);


      var body = {
        "action": "A",
        "listofarplan": acceptedlist
      };
      this.commonfun.loadingPresent();
      this.arvisitScheduleService.saveArVisitPlan(body).subscribe(data => {
        this.commonfun.loadingDismiss();
        var response = data['response'];

        if (response.hasOwnProperty('messageType')) {
          if (response.messageType == 'success') this.commonfun.presentAlert("Message", "Success", response.messageText);
          this.offset = 0;
          this.bindDataList();
        }

        if (response.error) this.commonfun.presentAlert("Message", "Error", response.error.message);
      });
    } catch (error) {
      this.commonfun.loadingDismiss();
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  rejectPlan(rejectedReason, rejectedlist) {
    try {
      // console.log(rejectedlist);
      var body = {
        "action": "R",
        "listofarplan": rejectedlist,
        "rejectedreason": rejectedReason
      };
      this.commonfun.loadingPresent();
      this.arvisitScheduleService.saveArVisitPlan(body).subscribe(data => {
        this.commonfun.loadingDismiss();
        var response = data['response'];

        if (response.hasOwnProperty('messageType')) {
          if (response.messageType == 'success') this.offset = 0;
          this.bindDataList();
          this.commonfun.presentAlert("Message", "Success", response.messageText);
        }

        if (response.error) this.commonfun.presentAlert("Message", "Error", response.error.message);
      });
    } catch (error) {
      this.commonfun.loadingDismiss();
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  presentAlertRejectedReason() {
    var _this10 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      let rejectedlist = _this10.planlist.filter(plan => plan.ischecked);

      if (rejectedlist.length === 0) {
        _this10.commonfun.presentAlert("Message", "Error", "Please Select Atleast One Record.");

        return;
      }

      let rejectedlistInvalid = rejectedlist.filter(plan => plan.ischecked && plan.status !== 'ACP');

      if (rejectedlistInvalid.length > 0) {
        _this10.commonfun.presentAlert("Message", "Error", " Selected record Can Not Reject.");

        return;
      }

      const alert = yield _this10.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Rejected Reason',
        // message: '<strong>Please Select Rejected Reason</strong>',
        inputs: [{
          name: 'RejectedReason',
          type: 'text',
          placeholder: 'Rejected Reason'
        }],
        buttons: [{
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {//console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: item => {
            //console.log('Confirm Okay',item.RejectedReason);
            if (item.RejectedReason) {} else {
              _this10.commonfun.presentAlert("Message", "Error", "Reason Is Mendotory.");
            }

            _this10.rejectPlan(item.RejectedReason, rejectedlist);
          }
        }]
      });
      yield alert.present();
    })();
  }

  presentCrossAssignment() {
    var _this11 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      let crossassignList = _this11.planlist.filter(plan => plan.ischecked);

      if (crossassignList.length === 0) {
        _this11.commonfun.presentAlert("Message", "Error", "Please Select Atleast One Record.");

        return;
      }

      let rejectedlistInvalid = crossassignList.filter(plan => plan.ischecked && plan.status !== 'ACP');

      if (rejectedlistInvalid.length > 0) {
        _this11.commonfun.presentAlert("Message", "Error", " Selected record Can Not Cross Assign.");

        return;
      }

      const modal = yield _this11.modalController.create({
        component: _aruserselect_aruserselect_page__WEBPACK_IMPORTED_MODULE_3__.AruserselectPage,
        cssClass: 'my-custom-class',
        componentProps: {
          'crossassignList': crossassignList
        }
      });
      modal.onDidDismiss().then(data => {
        const status = data['data']; // Here's your selected user!

        if (status === 'success') {
          console.log("data, ", status); //this.router.navigateByUrl(url);

          _this11.offset = 0;

          _this11.bindDataList();
        }
      });
      return yield modal.present();
    })();
  }

  doClickAddUnPlanned() {
    this.router.navigateByUrl('/unplannedvisit');
  }

  bindDataList() {
    try {
      var body = {
        "action": "getarvisitplan",
        "fromdate": this.selectedfromdate !== undefined ? this.selectedfromdate : '',
        "todate": this.selectedtodate !== undefined ? this.selectedtodate : '',
        "status": this.selectedstatus !== undefined ? this.selectedstatus : '',
        "bpartnerid": this.selectedcustomer !== undefined ? this.selectedcustomer.bpartnerid : '',
        "offset": this.offset
      };

      if (this.selectedactivitylist) {
        body["orgactlist"] = this.selectedactivitylist;
      }

      if (this.selectedcttvlist) {
        body["cttvlist"] = this.selectedcttvlist;
      }

      this.commonfun.loadingPresent();
      this.arvisitScheduleService.getArVisitPlan(body).subscribe(data => {
        this.commonfun.loadingDismiss();
        var response = data;

        if (this.offset == 0) {
          this.planlist = response.data;

          if (this.planlist.length <= 20) {
            this.isemptyList = true;
          } else {
            this.isemptyList = false;
          }
        } else {
          this.planlist = this.planlist.concat(response.data);
        }

        this.totalRows = response.totalRows;
      });
    } catch (error) {
      console.log("bindcusaddress", error);
      this.commonfun.loadingDismiss();
    }
  }

  doInfinite(event) {
    try {
      this.offset += 20;

      if (this.offset < this.totalRows) {
        this.isemptyList = false;
        this.bindDataList();
        event.target.complete();
      } else {
        this.isemptyList = true;
      }
    } catch (error) {
      console.log("errror", error);
    }
  }

};

ArvisitschedulePage.ctorParameters = () => [{
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_5__.Commonfun
}, {
  type: _arvisitschedule_service__WEBPACK_IMPORTED_MODULE_4__.ArvisitscheduleService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_9__.Router
}, {
  type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_6__.LoginauthService
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.AlertController
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.ModalController
}];

ArvisitschedulePage = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
  selector: 'app-arvisitschedule',
  template: _arvisitschedule_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_arvisitschedule_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ArvisitschedulePage);


/***/ }),

/***/ 48598:
/*!************************************************************!*\
  !*** ./src/app/arvisitschedule/arvisitschedule.service.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ArvisitscheduleService": () => (/* binding */ ArvisitscheduleService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);





let ArvisitscheduleService = class ArvisitscheduleService {
    constructor(http, loginauth, genericHTTP) {
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
    }
    getArVisitPlan(body) {
        //console.log("Body: ",body);
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTgetRequest', body, httpOptions);
    }
    saveArVisitPlan(body) {
        //   console.log("Body: ",body);
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTSaveARVisitPlan', body, httpOptions);
    }
    saveArVisitUnplannedPlan(body) {
        //   console.log("Body: ",body);
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTSaveUnplannedARVisitPlan', body, httpOptions);
    }
    getRequest(body) {
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.qcinspection.MQCIgetRequest', body, httpOptions);
    }
    onSaveSectionQuestion(body) {
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Authorization': 'Basic ' + auth
            })
        };
        var specificHeader = { 'Authorization': 'Basic ' + auth };
        return this.genericHTTP.postformdata(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTonSaveChecklistTrx', body, specificHeader, httpOptions);
    }
};
ArvisitscheduleService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_1__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_0__.GenericHttpClientService }
];
ArvisitscheduleService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], ArvisitscheduleService);



/***/ }),

/***/ 99803:
/*!**********************************************************************!*\
  !*** ./src/app/arvisitschedule/arvisitschedule.page.scss?ngResource ***!
  \**********************************************************************/
/***/ ((module) => {

module.exports = "ion-popover {\n  --width: 320px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFydmlzaXRzY2hlZHVsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxjQUFBO0FBQ0oiLCJmaWxlIjoiYXJ2aXNpdHNjaGVkdWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1wb3BvdmVyIHtcbiAgICAtLXdpZHRoOiAzMjBweDtcbiAgfVxuICAvLyBpb24tcG9wb3Zlci5kYXRlVGltZVBvcG92ZXIge1xuICAvLyAgIC0tb2Zmc2V0LXk6IC0zNTBweCAhaW1wb3J0YW50O1xuICAvLyAgIH0iXX0= */";

/***/ }),

/***/ 25881:
/*!**********************************************************************!*\
  !*** ./src/app/arvisitschedule/arvisitschedule.page.html?ngResource ***!
  \**********************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      AR Visit Schedule\n    </ion-title>\n    <ion-icon slot=\"end\" style=\"font-size: xx-large;\" name=\"add-circle-outline\" (click)=\"doClickAddUnPlanned()\">\n    </ion-icon>\n  </ion-toolbar>\n  <div style=\"padding-left: 20px;\">\n    <ion-icon style=\"font-size: 24px; color: orange;\"\n      [name]=\"!isshowfilter ? 'add-circle-outline' : 'remove-circle-outline'\" (click)=\"onToggleFilter()\"></ion-icon>\n  </div>\n  <ion-card *ngIf=\"isshowfilter\" style=\"margin-top: 0px;margin-bottom: 15px;\">\n\n    <ion-row>\n      <ion-col size=\"6\">\n\n        <!-- <ion-datetime  placeholder=\"From Date\" [(ngModel)]=\"selectedfromdate\" display-format=\"DD-MM-YYYY\"></ion-datetime> -->\n        <ion-item>\n          <ion-input placeholder=\"Select Date\" [value]=\"datefrom\"></ion-input>\n          <ion-button fill=\"clear\" id=\"open-date-input-1\">\n            <ion-icon icon=\"calendar\"></ion-icon>\n          </ion-button>\n          <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\n            <ng-template>\n              <ion-datetime #popoverDatetime2 [(ngModel)]=\"selectedfromdate\" presentation=\"date\"\n                showDefaultButtons=\"true\" (ionChange)=\"datefrom = formatDate(popoverDatetime2.value)\"></ion-datetime>\n            </ng-template>\n          </ion-popover>\n        </ion-item>\n\n      </ion-col>\n      <ion-col size=\"6\">\n\n        <!-- <ion-datetime placeholder=\"To Date\" [(ngModel)]=\"selectedtodate\" [min]=\"selectedfromdate\" display-format=\"DD-MM-YYYY\"></ion-datetime> -->\n        <ion-item>\n          <ion-input placeholder=\"Select Date\" [value]=\"dateto\"></ion-input>\n          <ion-button fill=\"clear\" id=\"open-date-input-2\">\n            <ion-icon icon=\"calendar\"></ion-icon>\n          </ion-button>\n          <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-2\" show-backdrop=\"false\">\n            <ng-template>\n              <ion-datetime #popoverDatetime2 [(ngModel)]=\"selectedtodate\" presentation=\"date\" [min]=\"selectedfromdate\"\n                showDefaultButtons=\"true\" (ionChange)=\"dateto = formatDate1(popoverDatetime2.value)\"></ion-datetime>\n            </ng-template>\n          </ion-popover>\n        </ion-item>\n\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" style=\"padding-left: 20px;\">\n\n        <ionic-selectable placeholder=\"Select Activty\" [(ngModel)]=\"selectedactivitylist\" itemValueField=\"orgactid\"\n          itemTextField=\"orgactname\" [items]=\"orgactlist\" [canSearch]=\"true\" [hasInfiniteScroll]=\"true\"\n          (onSearch)=\"onSearchActivity($event)\" (onClose)=\"onActivityClose($event)\" [hasInfiniteScroll]=\"true\"\n          (onInfiniteScroll)=\"doInfiniteActivity($event)\" [isMultiple]=\"true\">\n        </ionic-selectable>\n\n      </ion-col>\n      <ion-col size=\"6\" style=\"padding-left: 20px;\">\n\n        <ionic-selectable placeholder=\"Select Location\" [(ngModel)]=\"selectedcttvlist\" itemValueField=\"cttvid\"\n          itemTextField=\"cttvname\" [items]=\"cttvlist\" [canSearch]=\"true\" [hasInfiniteScroll]=\"true\"\n          (onSearch)=\"onSearchLocation($event)\" (onClose)=\"onLocationClose($event)\" [hasInfiniteScroll]=\"true\"\n          (onInfiniteScroll)=\"doInfiniteLocation($event)\" [isMultiple]=\"true\">\n        </ionic-selectable>\n\n      </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"12\" style=\"padding-left: 20px;\">\n\n          <ionic-selectable placeholder=\"Select Customer\" [(ngModel)]=\"selectedcustomer\" itemValueField=\"bpartnerid\"\n            itemTextField=\"bpartnername\" [items]=\"customerlist\" [canSearch]=\"true\" [hasInfiniteScroll]=\"true\"\n            (onSearch)=\"onSearchCustomer($event)\" (onClose)=\"oncustomerClose($event)\" [hasInfiniteScroll]=\"true\"\n            (onInfiniteScroll)=\"doInfiniteCust($event)\" [isMultiple]=\"false\">\n          </ionic-selectable>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"8\">\n\n          <ion-select interface=\"popover\" [(ngModel)]=\"selectedstatus\" placeholder=\"Select Status\">\n            <ion-select-option value=\"ACP\">Accept Or Reject</ion-select-option>\n            <ion-select-option value=\"SCP\">Update Checklist</ion-select-option>\n            <ion-select-option value=\"VIP\">Update Visit</ion-select-option>\n            <ion-select-option value=\"ONH\">On Hold</ion-select-option>\n          </ion-select>\n\n        </ion-col>\n        <ion-col size=\"2\">\n          <ion-icon style=\"font-size: 35px;\" (click)=\"onClickSearch($event)\" name=\"search\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"2\">\n          <ion-icon style=\"font-size: 35px;\" (click)=\"onClickClear($event)\" name=\"close-circle-outline\"></ion-icon>\n        </ion-col>\n      </ion-row>\n  </ion-card>\n</ion-header>\n\n<ion-content>\n  <ion-grid *ngFor=\"let plan of planlist\">\n    <ion-row>\n      <div style=\"padding-left: 5px;\">\n        <ion-checkbox (click)=\"selectMember(plan)\" [(ngModel)]=\"plan.ischecked\"></ion-checkbox>\n      </div>\n\n      <ion-col size=\"2\">\n        <div style=\"padding-left: 5px;\">\n          <ion-icon style=\"font-size: 24px; color: orange;\" [name]=\"!plan.isplusminus ? 'add' : 'remove'\"\n            (click)=\"onToggelGrid(plan)\"></ion-icon>\n        </div>\n      </ion-col>\n      <ion-col>\n        <div style=\"background-color: antiquewhite;\">\n          {{plan.dateofvisit}} - {{plan.bpartnername}} - {{plan.status==='VIP'?'Update Visit'\n          :(plan.status==='SCP'?'Update Checklist':(plan.status==='ONH'?'On Hold':(plan.status==='ACP'?'Accept Or\n          Reject':'')))}}\n        </div>\n      </ion-col>\n\n      <ion-col size=\"1\">\n        <!--  -->\n\n        <div style=\"text-align: right;\" *ngIf=\"plan.status==='SCP' || plan.status==='VIP' || plan.status==='ONH'\">\n          <ion-icon style=\"font-size: x-large;\" name=\"chevron-forward-circle-outline\" (click)=\"openChecklist(plan)\">\n          </ion-icon>\n        </div>\n      </ion-col>\n    </ion-row>\n    <div *ngIf=\"plan.isplusminus\">\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"font-size: smaller; font-weight: bold;\"> Business Partner</ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller;\"> {{plan.bpartnername}}</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"font-size: smaller; font-weight: bold;\"> Date Of Visit</ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller;\"> {{plan.dateofvisit}}</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"font-size: smaller; font-weight: bold;\"> Collection Branch</ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller;\"> {{plan.collectionbranchname}}</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"font-size: smaller; font-weight: bold;\"> Collection Zone</ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller;\"> {{plan.collectionzone}}</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"font-size: smaller; font-weight: bold;\"> Activity</ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller;\"> {{plan.activityname}}</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"font-size: smaller; font-weight: bold;\"> Customer Contact No.</ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller;\"> {{plan.customercontact}}</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"6\">\n          <ion-label style=\"font-size: smaller; font-weight: bold;\">Address</ion-label>\n        </ion-col>\n        <ion-col size=\"6\">\n          <ion-label style=\"font-size: smaller;\"> {{plan.bpartnerlocationname}}</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"font-size: smaller; font-weight: bold;\"> </ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller; font-weight: bold; background-color: antiquewhite;\"> Plan Details\n          </ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller; font-weight: bold; background-color: antiquewhite;\"> Live Value (As of\n            Yesterday) </ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"font-size: smaller; font-weight: bold;\"> OutStanding</ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller;\"> {{plan.outstanding}}</ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller;\"> {{plan.outstandingcurr}}</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"font-size: smaller; font-weight: bold;\"> Credit</ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller;\"> {{plan.credit}} </ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller;\"> {{plan.creditcurr}} </ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"font-size: smaller; font-weight: bold;\"> Amt. Not Due</ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller;\"> {{plan.amtnotdue}} </ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller;\"> {{plan.amtnotduecurr}} </ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"font-size: smaller; font-weight: bold;\"> Amt. Due</ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller;\"> {{plan.amtdue}} </ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller;\"> {{plan.amtduecurr}} </ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"font-size: smaller; font-weight: bold;\">Target For Month</ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-label style=\"font-size: smaller;\">{{plan.targetformonth}}</ion-label>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-grid>\n  <ion-infinite-scroll (ionInfinite)=\"doInfinite($event)\">\n    <ion-infinite-scroll-content *ngIf=\"!isemptyList\" loadingSpinner=\"bubbles\" loadingText=\"Loading more data...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n<ion-footer>\n  <ion-row *ngIf=\"totalRows!==0\">\n    <ion-item>\n      <ion-button color=\"primary\" text-center (click)=\"AcceptPlan()\">Accept</ion-button>\n      <!-- <ion-button color=\"primary\" text-center [disabled]=\"!formprod.valid || !Iscartproduct\" (click)=\"onSaveTemplate(formprod.value)\">Save Template</ion-button> -->\n    </ion-item>\n    <ion-item>\n      <ion-button color=\"primary\" text-center (click)=\"presentAlertRejectedReason()\">Reject</ion-button>\n      <!-- <ion-button color=\"primary\" text-center [disabled]=\"!formprod.valid || !Iscartproduct\" (click)=\"onSaveTemplate(formprod.value)\">Save Template</ion-button> -->\n    </ion-item>\n    <ion-item>\n      <ion-button color=\"primary\" text-center (click)=\"presentCrossAssignment()\">Cross Assignment</ion-button>\n      <!-- <ion-button color=\"primary\" text-center [disabled]=\"!formprod.valid || !Iscartproduct\" (click)=\"onSaveTemplate(formprod.value)\">Save Template</ion-button> -->\n    </ion-item>\n  </ion-row>\n</ion-footer>";

/***/ })

}]);
//# sourceMappingURL=src_app_arvisitschedule_arvisitschedule_module_ts.js.map