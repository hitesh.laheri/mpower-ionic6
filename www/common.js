"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["common"],{

/***/ 25461:
/*!******************************************************************!*\
  !*** ./src/app/actual-travel-plan/actual-travel-plan.service.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActualTravelPlanService": () => (/* binding */ ActualTravelPlanService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);





let ActualTravelPlanService = class ActualTravelPlanService {
    constructor(http, loginauth, genericHTTP) {
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
        this.TAG = 'ActualTravelPlanService';
    }
    getWMobileUserWisePlanData(istravelclosure, mexp_visitplan_id) {
        if (mexp_visitplan_id) {
            return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileUserWisePlanData?'
                + 'user_id=' + this.loginauth.userid
                + '&istravelclosure=' + istravelclosure
                + '&mexp_visitplan_id=' + mexp_visitplan_id);
        }
        else {
            return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileUserWisePlanData?'
                + 'user_id=' + this.loginauth.userid
                + '&istravelclosure=' + istravelclosure);
        }
    }
    SaveActualPlan(template) {
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileVisitActualPlan', template, httpOptions);
    }
};
ActualTravelPlanService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService }
];
ActualTravelPlanService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], ActualTravelPlanService);



/***/ }),

/***/ 11090:
/*!*******************************************************************!*\
  !*** ./src/app/arvisitschedule/aruserselect/aruserselect.page.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AruserselectPage": () => (/* binding */ AruserselectPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _aruserselect_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./aruserselect.page.html?ngResource */ 72244);
/* harmony import */ var _aruserselect_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./aruserselect.page.scss?ngResource */ 45144);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../provider/commonfun */ 51156);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _arvisitschedule_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../arvisitschedule.service */ 48598);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);









let AruserselectPage = class AruserselectPage {
  constructor(arvisitScheduleService, route, router, viewCtrl, commonfun) {
    this.arvisitScheduleService = arvisitScheduleService;
    this.route = route;
    this.router = router;
    this.viewCtrl = viewCtrl;
    this.commonfun = commonfun;
    this.search = '';
    this.offset = 0;
  }

  ngOnInit() {}

  onClickAssignPlan() {
    //console.log(this.crossassignList);
    if (this.selecteduser === undefined) {
      this.commonfun.presentAlert("Message", "Error", "Please Select Atleast One AR User.");
      return;
    }

    try {
      // console.log(rejectedlist);
      var body = {
        "action": "CA",
        "listofarplan": this.crossassignList,
        "userid": this.selecteduser.userid
      };
      this.commonfun.loadingPresent();
      this.arvisitScheduleService.saveArVisitPlan(body).subscribe(data => {
        this.commonfun.loadingDismiss();
        var response = data['response'];

        if (response.hasOwnProperty('messageType')) {
          if (response.messageType == 'success') this.commonfun.presentAlert("Message", "Success", response.messageText);
          this.dismiss('success');
        }

        if (response.error) this.commonfun.presentAlert("Message", "Error", response.error.message);
      });
    } catch (error) {
      this.commonfun.loadingDismiss();
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  dismiss(status) {
    this.viewCtrl.dismiss(status);
  }

  onClose() {
    this.dismiss('');
  }

  onARUserClose(event) {
    event.component.searchText = "";
    this.offset = 0;
  }

  onARUsersearchChange(event) {
    this.search = event.text; //.replace(/\s/g,'');

    this.offset = 0;
    this.bindArUserapi(this.search, 0);
  }

  bindArUserapi(searchtext, intoffset) {
    try {
      var body = {
        "action": "getaruserfromorg",
        "offset": intoffset,
        "search": searchtext
      };
      this.arvisitScheduleService.getArVisitPlan(body).subscribe(data => {
        var response = data;
        this.aruserlist = response.data;
        this.totalrow = response.totalRows;
      });
    } catch (error) {//console.log("bindOwnedDriverapi",error);
    }
  }

  doInfiniteARUser(event) {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        //Set this initial 0 at top 
        let text = (event.text || '').trim().toLowerCase();

        if (_this.offset < _this.totalrow && _this.totalrow > 20) {
          _this.offset = _this.offset + 20;
          var body = {
            "action": "getaruserfromorg",
            "offset": _this.offset,
            "search": text
          };
          var tempData = yield (yield _this.arvisitScheduleService.getArVisitPlan(body)).toPromise();

          if (!!tempData) {
            _this.aruserlist = _this.aruserlist.concat(tempData['data']);
            event.component.endInfiniteScroll();
          }
        }

        if (_this.offset > _this.totalrow) {
          event.component.disableInfiniteScroll();
          return;
        }
      } catch (error) {}
    })();
  }

};

AruserselectPage.ctorParameters = () => [{
  type: _arvisitschedule_service__WEBPACK_IMPORTED_MODULE_4__.ArvisitscheduleService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.ActivatedRoute
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ModalController
}, {
  type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_3__.Commonfun
}];

AruserselectPage.propDecorators = {
  crossassignList: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Input
  }]
};
AruserselectPage = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
  selector: 'app-aruserselect',
  template: _aruserselect_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_aruserselect_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], AruserselectPage);


/***/ }),

/***/ 84718:
/*!******************************************************************************!*\
  !*** ./src/app/business-partner-address/business-partner-address.service.ts ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BusinessPartnerAddressService": () => (/* binding */ BusinessPartnerAddressService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 93819);






let BusinessPartnerAddressService = class BusinessPartnerAddressService {
    constructor(platform, http, loginauth, genericHTTP) {
        this.platform = platform;
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
    }
    getaddressbycustid(bp_id) {
        //  businessPartner_id="FFF20190328042044745CEDE4F2E670B";
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileBPartnerAddList?'
            + 'bp_id=' + bp_id);
    }
    getexistcustmerapi(searchkey) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileBPatnerAddCust?'
            + 'user_id=' + this.loginauth.userid
            + '&strsearch=' + searchkey
            + '&activity_id=' + this.loginauth.selectedactivity.id);
    }
    SaveAddress(addressjson) {
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileCustomerAddInsert', addressjson, httpOptions);
    }
};
BusinessPartnerAddressService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.Platform },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService }
];
BusinessPartnerAddressService = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable)({
        providedIn: 'root'
    })
], BusinessPartnerAddressService);



/***/ }),

/***/ 89635:
/*!*********************************************************************!*\
  !*** ./src/app/cardinal/service-manager/service-manager.service.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ServiceManagerService": () => (/* binding */ ServiceManagerService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_common_Constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/common/Constants */ 68209);
/* harmony import */ var src_app_common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/common/generic-http-client.service */ 28475);
/* harmony import */ var src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/login/loginauth.service */ 44010);






let ServiceManagerService = class ServiceManagerService {
    constructor(genericHTTP, loginService, httpClient) {
        this.genericHTTP = genericHTTP;
        this.loginService = loginService;
        this.httpClient = httpClient;
        /**
         *
         */
        this.TAG = "ServiceManagerService";
    }
    getComplaintList(screen) {
        try {
            let complainListURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_0__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerServiceDetails?' +
                'userid=' + this.loginService.userid
                + '&appcomplaint=' + 'N'
                + '&servmanager=' + 'Y'
                + '&serveng=' + 'N'
                + '&sparesku=' + 'N'
                + '&activity=' + this.loginService.selectedactivity.id;
            ;
            //  console.log("getComplaintList",complainListURL);
            return this.genericHTTP.get(complainListURL);
        }
        catch (error) {
            console.error(this.TAG, error);
        }
    }
    getDealerList() {
        try {
            let dealerListURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_0__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.BusinessPartnerAsPerCategory?' +
                'activityid=' + this.loginService.selectedactivity.id +
                '&c_bgroup_id=' + this.loginService.dealer_id;
            //  console.log(this.TAG,"Dealer List URL",dealerListURL);
            return this.genericHTTP.get(dealerListURL);
        }
        catch (error) {
            console.error(this.TAG, error);
        }
    }
    getVenderList() {
        try {
            let venderListURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_0__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.BusinessPartnerAsPerCategory?' +
                'activityid=' + this.loginService.selectedactivity.id +
                '&c_bgroup_id=' + this.loginService.vender_id;
            //  console.log(this.TAG,"Vender List URL",venderListURL);
            return this.genericHTTP.get(venderListURL);
        }
        catch (error) {
            // console.log(this.TAG,error);
        }
    }
    getSalesRepresentativeList(bid) {
        try {
            let salesRepresentativeListURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_0__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerServiceSalesRep?' +
                'user_id=' + this.loginService.userid +
                '&bpid=' + bid;
            //  console.log(this.TAG,"Sales Representative List URL",salesRepresentativeListURL);
            return this.genericHTTP.get(salesRepresentativeListURL);
        }
        catch (error) {
            //  console.error(this.TAG,error);
        }
    }
    getDesignationOfComplainerList() {
        try {
            let designationOfComplainerListURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_0__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.ListOfvalues?' +
                'user_id=' + this.loginService.userid +
                '&type=DC';
            return this.genericHTTP.get(designationOfComplainerListURL);
        }
        catch (error) {
            //  console.error(this.TAG,error);
        }
    }
};
ServiceManagerService.ctorParameters = () => [
    { type: src_app_common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService },
    { type: src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpClient }
];
ServiceManagerService = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable)({
        providedIn: 'root'
    })
], ServiceManagerService);



/***/ }),

/***/ 35217:
/*!*********************************************************************!*\
  !*** ./src/app/cardinal/vender-approval/vender-approval.service.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "VenderApprovalService": () => (/* binding */ VenderApprovalService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_common_Constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/common/Constants */ 68209);
/* harmony import */ var src_app_common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/common/generic-http-client.service */ 28475);
/* harmony import */ var src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/login/loginauth.service */ 44010);






let VenderApprovalService = class VenderApprovalService {
    constructor(genericHTTP, loginService, httpClient) {
        this.genericHTTP = genericHTTP;
        this.loginService = loginService;
        this.httpClient = httpClient;
        this.TAG = "VenderApprovalService";
    }
    getVenderApprovalList() {
        try {
            let venderApprovalListURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_0__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerServiceDetails?' +
                'userid=' + this.loginService.userid
                + '&appcomplaint=' + 'Y'
                + '&servmanager=' + 'N'
                + '&serveng=' + 'N'
                + '&sparesku=' + 'N'
                + '&activity=' + this.loginService.selectedactivity.id;
            //  console.log(this.TAG,"GET Vender Approval List",venderApprovalListURL)
            return this.genericHTTP.get(venderApprovalListURL);
        }
        catch (error) {
            console.error(this.TAG, error);
        }
    }
    getServiceEngList() {
        try {
            let serviceEngListURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_0__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.BusinessPartnerVendor?' +
                this.loginService.parameter + '&userid=' + this.loginService.userid +
                '&activityid=' + this.loginService.selectedactivity.id;
            //  console.log(this.TAG,"Service Eng URL",serviceEngListURL);
            return this.genericHTTP.get(serviceEngListURL);
        }
        catch (error) {
            console.error(this.TAG, error);
        }
    }
};
VenderApprovalService.ctorParameters = () => [
    { type: src_app_common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService },
    { type: src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpClient }
];
VenderApprovalService = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable)({
        providedIn: 'root'
    })
], VenderApprovalService);



/***/ }),

/***/ 89792:
/*!******************************************!*\
  !*** ./src/app/hrms/mpr-form.service.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MprFormService": () => (/* binding */ MprFormService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var _common_Constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/Constants */ 68209);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);







let MprFormService = class MprFormService {
    constructor(genericHttpClientService, loginService, commonFunction, httpClient) {
        this.genericHttpClientService = genericHttpClientService;
        this.loginService = loginService;
        this.commonFunction = commonFunction;
        this.httpClient = httpClient;
        this.TAG = "Mpr Form Service";
        this.activity_name = loginService.selectedactivity.name;
    }
    getOrganizationList() {
        let getOrganizationListURL = _common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.WMobileUserWiseOrgActivity?' +
            this.loginService.parameter + '&userid=' + this.loginService.userid + '&activityid=' + this.loginService.selectedactivity.id;
        //   console.log(this.TAG,"get Organization List",getOrganizationListURL);
        return this.genericHttpClientService.get(getOrganizationListURL);
    }
    getReasonForMPRList() {
        let getReasonForMPRListURL = _common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.ListOfvaluesReference?' +
            this.loginService.parameter + '&user_id=' + this.loginService.userid + '&refname=HRMS%20Reason%20for%20MRP%20list';
        //console.log(this.TAG,"Doc Type Service",URL);
        return this.genericHttpClientService.get(getReasonForMPRListURL);
    }
    getJobListingList() {
        let getReasonForMPRListURL = _common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.ListOfvaluesReference?' +
            this.loginService.parameter + '&user_id=' + this.loginService.userid + '&refname=HRMS%20job%20list';
        //console.log(this.TAG,"Doc Type Service",URL);
        return this.genericHttpClientService.get(getReasonForMPRListURL);
    }
    getQualificationList() {
        let getQualificationListURL = _common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.ListOfvalues?' +
            this.loginService.parameter + '&user_id=' + this.loginService.userid + '&type=QLFC';
        //console.log(this.TAG,"Doc Type Service",URL);
        return this.genericHttpClientService.get(getQualificationListURL);
    }
    getMPRMasterDataList(selectedOrganization) {
        let getMPRMasterDataListURL = _common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.MPRDetails?' +
            this.loginService.parameter + '&ad_org_id=' + selectedOrganization.id;
        // console.log(this.TAG,"Doc Type Service",getMPRMasterDataListURL);
        return this.genericHttpClientService.get(getMPRMasterDataListURL);
    }
    getResourceRequirementMasterList() {
        let getResourceRequirementMasterListURL = _common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.ListOfvaluesReference?' +
            this.loginService.parameter + '&user_id=' + this.loginService.userid + '&refname=HRMS%20Employee%20Requirement';
        //console.log(this.TAG,"Doc Type Service",URL);
        return this.genericHttpClientService.get(getResourceRequirementMasterListURL);
    }
    getResourceDepartmentMasterList() {
        let getResourceDepartmentMasterListURL = _common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.ListOfvaluesReference?' +
            this.loginService.parameter + '&user_id=' + this.loginService.userid + '&refname=HRMS%20Department';
        //console.log(this.TAG,"Doc Type Service",URL);
        return this.genericHttpClientService.get(getResourceDepartmentMasterListURL);
    }
    getMPRList() {
        let getMPRListURL = _common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.MPRDetails?' +
            this.loginService.parameter + '&ad_user_id=' + this.loginService.userid;
        // console.log(this.TAG,"Doc Type Service",getMPRListURL);
        return this.genericHttpClientService.get(getMPRListURL);
    }
    postMPRForm(data) {
        let login = this.loginService.user;
        let password = this.loginService.pass;
        const auth = btoa(login + ":" + password);
        const httpOptions = { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Basic ' + auth }) };
        let MPR_FORM_SAVE_URL;
        if (data.is_approve == 'true' || data.is_approve == 'false') {
            MPR_FORM_SAVE_URL = _common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.MPRDetailsUpdate?';
        }
        else {
            MPR_FORM_SAVE_URL = _common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.MPRDetailsInsert?';
        }
        return this.genericHttpClientService.post(MPR_FORM_SAVE_URL, data, httpOptions);
    }
};
MprFormService.ctorParameters = () => [
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_2__.GenericHttpClientService },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__.LoginauthService },
    { type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_0__.Commonfun },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpClient }
];
MprFormService = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Injectable)({
        providedIn: 'root'
    })
], MprFormService);



/***/ }),

/***/ 85189:
/*!****************************************************!*\
  !*** ./src/app/newcustomer/newcustomer.service.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NewcustomerService": () => (/* binding */ NewcustomerService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);




// import { analyzeAndValidateNgModules } from '@angular/compiler';

let NewcustomerService = class NewcustomerService {
    constructor(http, loginauth, genericHTTP) {
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
    }
    getBPCategory(activityid) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/BusinessPartnerCategory?'
            + '_selectedProperties=id,_identifier&'
            + '_where=active=true%20and%20EM_Mmst_Org_Act=\'' + activityid + '\'', false, !this.loginauth.isloginactive);
    }
    getBPartner(activityid) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/BusinessPartner?'
            + '_selectedProperties=id,_identifier&'
            + '_where=EM_Mmst_Org_Act=\'' + activityid + '\'', false, !this.loginauth.isloginactive);
    }
    getPincode(pincode) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/mmst_post_off?'
            + '_where=active=true%20and%20spincode=\'' + pincode + '\'', false, !this.loginauth.isloginactive);
    }
    getregion(pincode) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/Region?'
            + '_selectedProperties=mmstRegioncode,name&'
            + '_where=active=true%20and%20id=\'' + pincode + '\'', false, !this.loginauth.isloginactive);
    }
    getarea(area_id) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/mmst_post_off_val?'
            + '_where=active=true%20and%20Mmst_Post_Off_ID=\'' + area_id + '\'', false, !this.loginauth.isloginactive);
    }
    geteditcustmerheader(Cust_id) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/mmst_customer?'
            + '_where=id=\'' + Cust_id + '\'', false, !this.loginauth.isloginactive);
    }
    geteditcustmerdetailapi(Cust_id) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileCustomerDetails?'
            + 'cust_id=' + Cust_id, false, !this.loginauth.isloginactive);
    }
    checkmobileno(mobile) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/mmst_customer?'
            + '_where=smobile=\'' + mobile + '\'', false, !this.loginauth.isloginactive);
    }
    geteditcustmerbilling(Cust_id) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/mmst_customer_billing?'
            + '_where=mmstCustomer=\'' + Cust_id + '\'', false, !this.loginauth.isloginactive);
    }
    geteditcustmercompliance(Cust_id) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/mmst_cust_compilance?'
            + '_where=mmstCustomer=\'' + Cust_id + '\'', false, !this.loginauth.isloginactive);
    }
    getreferalcustmer(selectedactivity) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/mmst_customer?'
            + '_where=mmstOrgAct=\'' + selectedactivity + '\'', false, !this.loginauth.isloginactive);
    }
    LeadComplete(leadcomplete) {
        let login = '';
        let password = '';
        if (!this.loginauth.isloginactive) {
            login = 'ionic.appuser'; //"P2admin";
            password = 'ionic.appuser'; //"Pass2020";
        }
        else {
            login = this.loginauth.user;
            password = this.loginauth.pass;
        }
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Authorization': 'Basic ' + auth
            })
        };
        var specificHeader = { 'Authorization': 'Basic ' + auth };
        return this.genericHTTP.postformdata(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileNewLeadComplete', leadcomplete, specificHeader, httpOptions);
    }
    getCompilanceDataapi(bpc_id, Cust_id) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileComplianceData?'
            + 'bpc_id=' + bpc_id + '&cust_id=' + Cust_id, false, !this.loginauth.isloginactive);
    }
    getCompilanceData(bpc_id) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/mmst_compliance_details?'
            + '_where=bpGroup=\'' + bpc_id + '\'', false, !this.loginauth.isloginactive);
    }
    getsalesarea(user_id, searchtext) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileSalesAreaUserWise?'
            + 'user_id=' + user_id
            + '&activity_id=' + this.loginauth.selectedactivity.id
            + '&strsearch=' + searchtext, false, !this.loginauth.isloginactive);
    }
    getUserActivityAgreementStatus() {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileUserActivityAgreementStatus?'
            + 'newregistration=Y', false, !this.loginauth.isloginactive);
    }
    getPreferredTransportModes() {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.BusinessPartnerVendor?'
            + 'transport=true');
    }
};
NewcustomerService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService }
];
NewcustomerService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], NewcustomerService);



/***/ }),

/***/ 15411:
/*!**************************************************************************************!*\
  !*** ./src/app/order-approval/show-approval-details-modal/approval-modal.service.ts ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ApprovalModalService": () => (/* binding */ ApprovalModalService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_common_Constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/common/Constants */ 68209);
/* harmony import */ var src_app_common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/common/generic-http-client.service */ 28475);
/* harmony import */ var src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/login/loginauth.service */ 44010);






let ApprovalModalService = class ApprovalModalService {
    constructor(genericHTTP, loginauth, http) {
        this.genericHTTP = genericHTTP;
        this.loginauth = loginauth;
        this.http = http;
        this.TAG = "ApprovalModalService";
    }
    getApprovalDetails(id, record, tab_id) {
        try {
            let url = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_0__.Constants.DOMAIN_URL + "/openbravo/ws/in.mbs.webservice.approval-details?"
                + "tab_id=" + tab_id + "&approval_id=" + id + "&record_id=" + record;
            return this.genericHTTP.get(url);
        }
        catch (error) {
            // console.log(this.TAG,error);
            throw error;
        }
    }
};
ApprovalModalService.ctorParameters = () => [
    { type: src_app_common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService },
    { type: src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpClient }
];
ApprovalModalService = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable)({
        providedIn: 'root'
    })
], ApprovalModalService);



/***/ }),

/***/ 19592:
/*!************************************************************************************************!*\
  !*** ./src/app/order-approval/show-approval-details-modal/show-approval-details-modal.page.ts ***!
  \************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Page": () => (/* binding */ Page),
/* harmony export */   "ShowApprovalDetailsModalPage": () => (/* binding */ ShowApprovalDetailsModalPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _show_approval_details_modal_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./show-approval-details-modal.page.html?ngResource */ 43650);
/* harmony import */ var _show_approval_details_modal_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./show-approval-details-modal.page.scss?ngResource */ 30314);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @swimlane/ngx-datatable */ 93143);
/* harmony import */ var _approval_modal_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./approval-modal.service */ 15411);
/* harmony import */ var _awesome_cordova_plugins_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @awesome-cordova-plugins/screen-orientation/ngx */ 11898);
/* harmony import */ var src_provider_message_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/provider/message-helper */ 98792);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/provider/commonfun */ 51156);











/**
 * An object used to get page information from the server
 */

class Page {
  constructor() {
    // The number of elements in the page
    this.size = 0; // The total number of elements

    this.totalElements = 0; // The total number of pages

    this.totalPages = 0; // The current page number

    this.pageNumber = 0;
  }

}
let ShowApprovalDetailsModalPage = class ShowApprovalDetailsModalPage {
  constructor(modalController, navParams, approvalModalService, screenOrientation, msg, commonfun) {
    this.modalController = modalController;
    this.navParams = navParams;
    this.approvalModalService = approvalModalService;
    this.screenOrientation = screenOrientation;
    this.msg = msg;
    this.commonfun = commonfun;
    this.ColumnMode = _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__.ColumnMode;
    this.SelectionType = _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__.SelectionType;
    this.page = new Page();
    this.rows = new Array();
    this.loadingIndicator = true; // console.log("Pravin data from approval page",navParams.get('id'),this.tab_id)

    this.id = navParams.get('id');
    this.tab_id = navParams.get('tab_id');
    this.record = navParams.get('record');
    this.page.pageNumber = 0;
    this.page.size = 20;
  }

  ngOnInit() {
    this.setPage({
      offset: 0
    }); // set to landscape
  }

  ionViewWillEnter() {
    if (this.msg.isplatformweb == false) {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    }
  }

  closeModal() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (_this.msg.isplatformweb == false) {
        _this.screenOrientation.unlock();
      }

      yield _this.modalController.dismiss();
    })();
  }

  setPage(pageInfo) {
    this.commonfun.loadingPresent();
    this.approvalModalService.getApprovalDetails(this.id, this.record, this.tab_id).subscribe(pagedData => {
      // console.log("Pravin Modal DATA ",pagedData);
      if (!!pagedData.data && !!pagedData.data.colum_names) {
        this.column_data = pagedData.data.colum_names;
        this.rows = pagedData.data.colum_data;
      } else {
        this.loadingIndicator = false;
        this.commonfun.loadingDismiss();
      }

      this.loadingIndicator = false;
      this.commonfun.loadingDismiss();
    });
  }

};

ShowApprovalDetailsModalPage.ctorParameters = () => [{
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.ModalController
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.NavParams
}, {
  type: _approval_modal_service__WEBPACK_IMPORTED_MODULE_3__.ApprovalModalService
}, {
  type: _awesome_cordova_plugins_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_4__.ScreenOrientation
}, {
  type: src_provider_message_helper__WEBPACK_IMPORTED_MODULE_5__.Message
}, {
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_6__.Commonfun
}];

ShowApprovalDetailsModalPage = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
  selector: 'app-show-approval-details-modal',
  template: _show_approval_details_modal_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_show_approval_details_modal_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ShowApprovalDetailsModalPage);


/***/ }),

/***/ 32305:
/*!*****************************************************!*\
  !*** ./src/app/product-list/product-list.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProductListPageModule": () => (/* binding */ ProductListPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _product_list_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./product-list.page */ 47214);






let ProductListPageModule = class ProductListPageModule {
};
ProductListPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            // RouterModule.forChild(routes)
        ],
        declarations: [_product_list_page__WEBPACK_IMPORTED_MODULE_0__.ProductListPage],
        // exports: [ ProductListPage ]
    })
], ProductListPageModule);



/***/ }),

/***/ 23375:
/*!****************************************************************************!*\
  !*** ./src/app/quotation/customer-quotation/customer-quotation.service.ts ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CustomerQuotationService": () => (/* binding */ CustomerQuotationService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var src_app_common_Constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/common/Constants */ 68209);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../common/generic-http-client.service */ 28475);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../login/loginauth.service */ 44010);








let CustomerQuotationService = class CustomerQuotationService {
    constructor(genericHttpClientService, loginService, commonFunction, platform, http) {
        this.genericHttpClientService = genericHttpClientService;
        this.loginService = loginService;
        this.commonFunction = commonFunction;
        this.platform = platform;
        this.http = http;
        this.TAG = "Customer Quotation Service";
    }
    getCustomer(strsearch) {
        try {
            if (!!strsearch) {
                strsearch = strsearch.replace(/ /g, "%20");
            }
            else {
                strsearch = "";
            }
            let getCustomerURL = this.loginService.commonurl + 'ws/in.mbs.webservice.WMobileUserWiseCustomer?'
                + this.loginService.parameter
                + '&user_id=' + this.loginService.userid
                + '&strsearch=' + strsearch
                + '&activity_id=' + this.loginService.selectedactivity.id;
            +'&ordertypeionic=' + "";
            //  console.log(this.TAG,"getCustomer",getCustomerURL);
            //  return this.genericHttpClientService.get(getCustomerURL).map(response =>response.slice(0.9));
            return this.genericHttpClientService.get(getCustomerURL);
        }
        catch (error) {
            //  console.log(this.TAG,error);
        }
    }
    getcustmerbillingaddress(businessPartner_id) {
        try {
            return this.genericHttpClientService.get(this.loginService.commonurl + 'org.openbravo.service.json.jsonrest/BusinessPartnerLocation?'
                + this.loginService.ReadOnlyparameter + '&'
                + '_selectedProperties=id,name,shipToAddress,invoiceToAddress&'
                + '_where=active=true%20and%20businessPartner=\'' + businessPartner_id + '\'');
        }
        catch (error) {
            throw error;
        }
    }
    getServiceProduct(strsearch, bpid) {
        try {
            let getServiceProductURL;
            if (!!strsearch) {
                getServiceProductURL = this.loginService.commonurl + '/ws/in.mbs.webservice.ActivityContractTypeWiseProd?'
                    + this.loginService.parameter
                    + '&user_id=' + this.loginService.userid
                    + '&activity_id=' + this.loginService.selectedactivity.id
                    + '&strsearch=' + strsearch
                    + '&bpid=' + bpid;
            }
            else {
                getServiceProductURL = this.loginService.commonurl + '/ws/in.mbs.webservice.ActivityContractTypeWiseProd?'
                    + this.loginService.parameter
                    + '&user_id=' + this.loginService.userid
                    + '&activity_id=' + this.loginService.selectedactivity.id
                    + '&strsearch=' + ""
                    + '&bpid=' + bpid;
            }
            //  console.log(this.TAG,"getServiceProductURL",getServiceProductURL);
            return this.genericHttpClientService.get(getServiceProductURL);
        }
        catch (error) {
            //  console.log(this.TAG,error);
        }
    }
    getSerialNoProductList(m_product_id, searchchar) {
        try {
            if (this.platform.is('android')) {
                return this.http.get(this.loginService.commonurl + 'ws/in.mbs.webservice.SerialnoProdContracttypeWise?'
                    + this.loginService.parameter
                    + '&m_product_id=' + m_product_id
                    + '&activityid=' + this.loginService.selectedactivity.id
                    + '&strsearch=' + searchchar);
            }
            else if (this.platform.is('ios')) {
                return this.genericHttpClientService.get(this.loginService.commonurl + 'ws/in.mbs.webservice.SerialnoProdContracttypeWise?'
                    + this.loginService.parameter
                    + '&m_product_id=' + m_product_id
                    + '&activityid=' + this.loginService.selectedactivity.id
                    + '&strsearch=' + searchchar);
            }
            else {
                let url = this.loginService.commonurl + 'ws/in.mbs.webservice.SerialnoProdContracttypeWise?'
                    + this.loginService.parameter
                    + '&m_product_id=' + m_product_id
                    + '&activityid=' + this.loginService.selectedactivity.id
                    + '&strsearch=' + searchchar;
                //   console.log(this.TAG,"Serial Number list url",url);
                return this.http.get(url);
            }
        }
        catch (error) {
            //  console.log(this.TAG,error);
        }
    }
    getSerialNoProductDetail(bpid, m_product_id, shippadd, billadd) {
        try {
            let getSerialNoProductDetailURL = this.loginService.commonurl + 'ws/in.mbs.webservice.customerproductwiserate?'
                + this.loginService.parameter
                + '&activity_id=' + this.loginService.selectedactivity.id
                + '&bpid=' + bpid
                + '&m_product_id=' + m_product_id
                + '&shipid=' + shippadd
                + '&billid=' + billadd;
            //  console.log(this.TAG,"getSerialNoProductDetailURL",getSerialNoProductDetailURL);
            return this.genericHttpClientService.get(getSerialNoProductDetailURL);
        }
        catch (error) {
            //  console.log(this.TAG,error);
        }
    }
    saveQuotation(form, serialNumberList) {
        try {
            let login = this.loginService.user;
            let password = this.loginService.pass;
            const auth = btoa(login + ":" + password);
            const httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders({
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Basic ' + auth
                })
            };
            let data = {
                "userid": this.loginService.userid,
                "bpid": form.bpid ? form.bpid : "",
                "billid": form.billid ? form.billid : "",
                "shipid": form.shipid ? form.shipid : "",
                "orderdate": form.orderdate ? form.orderdate : "",
                "m_product_id": form.m_product_id ? form.m_product_id : "",
                "complaintno": form.complaintno ? form.complaintno : "",
                "quatationline": serialNumberList ? serialNumberList : ""
            };
            //  console.log(this.TAG,"Save Quotation Final Data",data);
            let save_quotation = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_0__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerQuotationInsert?';
            return this.genericHttpClientService.post(save_quotation, data, httpOptions);
        }
        catch (error) {
            //  console.log(this.TAG,error);
        }
    }
    getQuotationList() {
        try {
            let getQuotationListURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_0__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerQuotationDetails?' +
                this.loginService.parameter + '&userid=' + this.loginService.userid
                + '&activity=' + this.loginService.selectedactivity.id;
            //  console.log(this.TAG,"GET Vender Approval List",getQuotationListURL)
            return this.genericHttpClientService.get(getQuotationListURL, {});
        }
        catch (error) {
            //  console.log(this.TAG,error);
        }
    }
    approveQuotation(complaintno, approve, reject) {
        try {
            let login = this.loginService.user;
            let password = this.loginService.pass;
            const auth = btoa(login + ":" + password);
            const httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders({
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Basic ' + auth
                })
            };
            let data = {
                "userid": this.loginService.userid,
                "msnr_quotationreq_id": complaintno.msnr_quotationreq_id,
                "approve": approve,
                "reject": reject,
            };
            //  console.log(this.TAG,"Approve Quotation Final Data",data);
            let save_quotation = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_0__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerQuotationApproval?';
            return this.genericHttpClientService.post(save_quotation, data, httpOptions);
        }
        catch (error) {
            //  console.log(this.TAG,error);
        }
    }
};
CustomerQuotationService.ctorParameters = () => [
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_2__.GenericHttpClientService },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__.LoginauthService },
    { type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_1__.Commonfun },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.Platform },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpClient }
];
CustomerQuotationService = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Injectable)({
        providedIn: 'root'
    })
], CustomerQuotationService);



/***/ }),

/***/ 67296:
/*!*****************************************************************!*\
  !*** ./src/app/selectorsingle/selectorsingleservice.service.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SelectorsingleserviceService": () => (/* binding */ SelectorsingleserviceService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);





let SelectorsingleserviceService = class SelectorsingleserviceService {
    constructor(loginservc, http, genericHTTP) {
        this.loginservc = loginservc;
        this.http = http;
        this.genericHTTP = genericHTTP;
        this.json = {};
        this.pageOffset = 0;
    }
    getData(jsonreport) {
        let login = this.loginservc.user; //"P2admin";
        let password = this.loginservc.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginservc.commonurl + 'ws/in.mbs.exportdata.MEXDGetSingleSelector', jsonreport, httpOptions);
    }
};
SelectorsingleserviceService.ctorParameters = () => [
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__.LoginauthService },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService }
];
SelectorsingleserviceService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], SelectorsingleserviceService);



/***/ }),

/***/ 36066:
/*!**********************************************************!*\
  !*** ./src/app/travel-expense/travel-expense.service.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TravelExpenseService": () => (/* binding */ TravelExpenseService)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 58987);






let TravelExpenseService = class TravelExpenseService {
  constructor(http, genericHTTP, loginauth) {
    this.http = http;
    this.genericHTTP = genericHTTP;
    this.loginauth = loginauth;
    this.TAG = 'Travel Expense Service';
  }

  getPlan(id) {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      let methodTAG = 'getPlan';

      try {
        return yield _this.genericHTTP.get('assets/data/planMaster.json');
      } catch (error) {}
    })();
  }

  getWMobileTravelExpenseList(mexp_visitplan_id) {
    if (mexp_visitplan_id) {
      return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileTravelExpenseList?' + 'user_id=' + this.loginauth.userid + '&mexp_visitplan_id=' + mexp_visitplan_id);
    } else {
      return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileTravelExpenseList?' + 'user_id=' + this.loginauth.userid);
    } // console.log("API FOR Travel Plan",url);

  }

  SaveWMobileTravelExpense(template) {
    let login = this.loginauth.user; //"P2admin";

    let password = this.loginauth.pass; //"Pass2020";

    const auth = btoa(login + ":" + password);
    const httpOptions = {
      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Basic ' + auth
      })
    };
    return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileTravelExpense', template, httpOptions);
  }

};

TravelExpenseService.ctorParameters = () => [{
  type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpClient
}, {
  type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService
}, {
  type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService
}];

TravelExpenseService = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable)({
  providedIn: 'root'
})], TravelExpenseService);


/***/ }),

/***/ 85276:
/*!****************************************************!*\
  !*** ./src/app/travel-plan/travel-plan.service.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TravelPlanService": () => (/* binding */ TravelPlanService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);





let TravelPlanService = class TravelPlanService {
    constructor(http, loginauth, genericHTTP) {
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
        this.TAG = 'TravelPlanService';
    }
    getUserWiseSalesPerson(offset, strsearch, ondatechange, lat, long, outstation) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileUserWiseSalesPerson?'
            + 'user_id=' + this.loginauth.userid
            + '&strsearch=' + strsearch
            + '&offset=' + offset
            + '&ondatechange=' + ondatechange
            + '&lat=' + lat
            + '&long=' + long
            + '&outstation=' + outstation);
    }
    getSearchNearestPersoni(bpcutadd_id, offset, strsearch, outstation) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileSearchNearestPerson?'
            + 'userid=' + this.loginauth.userid
            + '&strsearch=' + strsearch
            + '&bpcutadd_id=' + bpcutadd_id
            + '&offset=' + offset
            + '&outstation=' + outstation);
    }
    getPlan(planname) {
        planname = planname.replace(/ /g, "%20");
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/mexp_visitplan?'
            + '_where=Planname=\'' + planname + '\'', false, false);
    }
    SavePlan(template) {
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileVisitPlan', template, httpOptions);
    }
};
TravelPlanService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService }
];
TravelPlanService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], TravelPlanService);



/***/ }),

/***/ 6730:
/*!******************************************************!*\
  !*** ./src/app/use-vetcoins/use-vetcoins.service.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UseVetcoinsService": () => (/* binding */ UseVetcoinsService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);





let UseVetcoinsService = class UseVetcoinsService {
    constructor(http, loginauth, genericHTTP) {
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
    }
    getWMobileVetCoinCustDetailsapi(mobno, mytransaction) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileVetCoinCustDetails?'
            + 'mobno=' + mobno
            + '&mytransaction=' + mytransaction
            + '&userid=' + this.loginauth.userid
            + '&loginmobno=' + this.loginauth.user);
    }
    VetCoinRewardTranstn(tocust, fromcust, description, bal, rewardlimit, rewardtransfer, redeemlimit, ordvalue) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileVetCoinRewardTranstn?'
            + 'tocust=' + tocust
            + '&fromcust=' + fromcust
            + '&description=' + description
            + '&bal=' + bal
            + '&rewardlimit=' + rewardlimit
            + '&rewardtransfer=' + rewardtransfer
            + '&redeemlimit=' + redeemlimit
            + '&ordvalue=' + ordvalue);
    }
    getWMobileSendSmsViaIonic(mobno) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileVetcoinSmsOpt?'
            + 'mobno=' + mobno //this.loginauth.user
            + '&userid=' + this.loginauth.userid);
    }
};
UseVetcoinsService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService }
];
UseVetcoinsService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], UseVetcoinsService);



/***/ }),

/***/ 73696:
/*!*********************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/button-active-8937ead0.js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "c": () => (/* binding */ createButtonActiveGesture)
/* harmony export */ });
/* harmony import */ var _index_88bdeaae_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index-88bdeaae.js */ 39479);
/* harmony import */ var _haptic_683b3b3c_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./haptic-683b3b3c.js */ 70634);
/* harmony import */ var _index_3f1a7d95_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index-3f1a7d95.js */ 82172);
/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */




const createButtonActiveGesture = (el, isButton) => {
  let currentTouchedButton;
  let initialTouchedButton;

  const activateButtonAtPoint = (x, y, hapticFeedbackFn) => {
    if (typeof document === 'undefined') {
      return;
    }

    const target = document.elementFromPoint(x, y);

    if (!target || !isButton(target)) {
      clearActiveButton();
      return;
    }

    if (target !== currentTouchedButton) {
      clearActiveButton();
      setActiveButton(target, hapticFeedbackFn);
    }
  };

  const setActiveButton = (button, hapticFeedbackFn) => {
    currentTouchedButton = button;

    if (!initialTouchedButton) {
      initialTouchedButton = currentTouchedButton;
    }

    const buttonToModify = currentTouchedButton;
    (0,_index_88bdeaae_js__WEBPACK_IMPORTED_MODULE_0__.c)(() => buttonToModify.classList.add('ion-activated'));
    hapticFeedbackFn();
  };

  const clearActiveButton = (dispatchClick = false) => {
    if (!currentTouchedButton) {
      return;
    }

    const buttonToModify = currentTouchedButton;
    (0,_index_88bdeaae_js__WEBPACK_IMPORTED_MODULE_0__.c)(() => buttonToModify.classList.remove('ion-activated'));
    /**
     * Clicking on one button, but releasing on another button
     * does not dispatch a click event in browsers, so we
     * need to do it manually here. Some browsers will
     * dispatch a click if clicking on one button, dragging over
     * another button, and releasing on the original button. In that
     * case, we need to make sure we do not cause a double click there.
     */

    if (dispatchClick && initialTouchedButton !== currentTouchedButton) {
      currentTouchedButton.click();
    }

    currentTouchedButton = undefined;
  };

  return (0,_index_3f1a7d95_js__WEBPACK_IMPORTED_MODULE_2__.createGesture)({
    el,
    gestureName: 'buttonActiveDrag',
    threshold: 0,
    onStart: ev => activateButtonAtPoint(ev.currentX, ev.currentY, _haptic_683b3b3c_js__WEBPACK_IMPORTED_MODULE_1__.a),
    onMove: ev => activateButtonAtPoint(ev.currentX, ev.currentY, _haptic_683b3b3c_js__WEBPACK_IMPORTED_MODULE_1__.b),
    onEnd: () => {
      clearActiveButton(true);
      (0,_haptic_683b3b3c_js__WEBPACK_IMPORTED_MODULE_1__.h)();
      initialTouchedButton = undefined;
    }
  });
};



/***/ }),

/***/ 17481:
/*!***********************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/dir-e8b767a8.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "i": () => (/* binding */ isRTL)
/* harmony export */ });
/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */

/**
 * Returns `true` if the document or host element
 * has a `dir` set to `rtl`. The host value will always
 * take priority over the root document value.
 */
const isRTL = hostEl => {
  if (hostEl) {
    if (hostEl.dir !== '') {
      return hostEl.dir.toLowerCase() === 'rtl';
    }
  }

  return (document === null || document === void 0 ? void 0 : document.dir.toLowerCase()) === 'rtl';
};



/***/ }),

/***/ 69013:
/*!*********************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/focus-visible-5ad6825d.js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "startFocusVisible": () => (/* binding */ startFocusVisible)
/* harmony export */ });
/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */
const ION_FOCUSED = 'ion-focused';
const ION_FOCUSABLE = 'ion-focusable';
const FOCUS_KEYS = ['Tab', 'ArrowDown', 'Space', 'Escape', ' ', 'Shift', 'Enter', 'ArrowLeft', 'ArrowRight', 'ArrowUp', 'Home', 'End'];

const startFocusVisible = rootEl => {
  let currentFocus = [];
  let keyboardMode = true;
  const ref = rootEl ? rootEl.shadowRoot : document;
  const root = rootEl ? rootEl : document.body;

  const setFocus = elements => {
    currentFocus.forEach(el => el.classList.remove(ION_FOCUSED));
    elements.forEach(el => el.classList.add(ION_FOCUSED));
    currentFocus = elements;
  };

  const pointerDown = () => {
    keyboardMode = false;
    setFocus([]);
  };

  const onKeydown = ev => {
    keyboardMode = FOCUS_KEYS.includes(ev.key);

    if (!keyboardMode) {
      setFocus([]);
    }
  };

  const onFocusin = ev => {
    if (keyboardMode && ev.composedPath) {
      const toFocus = ev.composedPath().filter(el => {
        if (el.classList) {
          return el.classList.contains(ION_FOCUSABLE);
        }

        return false;
      });
      setFocus(toFocus);
    }
  };

  const onFocusout = () => {
    if (ref.activeElement === root) {
      setFocus([]);
    }
  };

  ref.addEventListener('keydown', onKeydown);
  ref.addEventListener('focusin', onFocusin);
  ref.addEventListener('focusout', onFocusout);
  ref.addEventListener('touchstart', pointerDown);
  ref.addEventListener('mousedown', pointerDown);

  const destroy = () => {
    ref.removeEventListener('keydown', onKeydown);
    ref.removeEventListener('focusin', onFocusin);
    ref.removeEventListener('focusout', onFocusout);
    ref.removeEventListener('touchstart', pointerDown);
    ref.removeEventListener('mousedown', pointerDown);
  };

  return {
    destroy,
    setFocus
  };
};



/***/ }),

/***/ 92668:
/*!**************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/framework-delegate-ce4f806c.js ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "C": () => (/* binding */ CoreDelegate),
/* harmony export */   "a": () => (/* binding */ attachComponent),
/* harmony export */   "d": () => (/* binding */ detachComponent)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var _helpers_4d272360_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers-4d272360.js */ 59158);


/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */


const attachComponent = /*#__PURE__*/function () {
  var _ref = (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (delegate, container, component, cssClasses, componentProps, inline) {
    var _a;

    if (delegate) {
      return delegate.attachViewToDom(container, component, componentProps, cssClasses);
    }

    if (!inline && typeof component !== 'string' && !(component instanceof HTMLElement)) {
      throw new Error('framework delegate is missing');
    }

    const el = typeof component === 'string' ? (_a = container.ownerDocument) === null || _a === void 0 ? void 0 : _a.createElement(component) : component;

    if (cssClasses) {
      cssClasses.forEach(c => el.classList.add(c));
    }

    if (componentProps) {
      Object.assign(el, componentProps);
    }

    container.appendChild(el);
    yield new Promise(resolve => (0,_helpers_4d272360_js__WEBPACK_IMPORTED_MODULE_1__.c)(el, resolve));
    return el;
  });

  return function attachComponent(_x, _x2, _x3, _x4, _x5, _x6) {
    return _ref.apply(this, arguments);
  };
}();

const detachComponent = (delegate, element) => {
  if (element) {
    if (delegate) {
      const container = element.parentElement;
      return delegate.removeViewFromDom(container, element);
    }

    element.remove();
  }

  return Promise.resolve();
};

const CoreDelegate = () => {
  let BaseComponent;
  let Reference;

  const attachViewToDom = /*#__PURE__*/function () {
    var _ref2 = (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (parentElement, userComponent, userComponentProps = {}, cssClasses = []) {
      var _a, _b;

      BaseComponent = parentElement;
      /**
       * If passing in a component via the `component` props
       * we need to append it inside of our overlay component.
       */

      if (userComponent) {
        /**
         * If passing in the tag name, create
         * the element otherwise just get a reference
         * to the component.
         */
        const el = typeof userComponent === 'string' ? (_a = BaseComponent.ownerDocument) === null || _a === void 0 ? void 0 : _a.createElement(userComponent) : userComponent;
        /**
         * Add any css classes passed in
         * via the cssClasses prop on the overlay.
         */

        cssClasses.forEach(c => el.classList.add(c));
        /**
         * Add any props passed in
         * via the componentProps prop on the overlay.
         */

        Object.assign(el, userComponentProps);
        /**
         * Finally, append the component
         * inside of the overlay component.
         */

        BaseComponent.appendChild(el);
        yield new Promise(resolve => (0,_helpers_4d272360_js__WEBPACK_IMPORTED_MODULE_1__.c)(el, resolve));
      } else if (BaseComponent.children.length > 0) {
        // If there is no component, then we need to create a new parent
        // element to apply the css classes to.
        const el = (_b = BaseComponent.ownerDocument) === null || _b === void 0 ? void 0 : _b.createElement('div');
        cssClasses.forEach(c => el.classList.add(c)); // Move each child from the original template to the new parent element.

        el.append(...BaseComponent.children); // Append the new parent element to the original parent element.

        BaseComponent.appendChild(el);
      }
      /**
       * Get the root of the app and
       * add the overlay there.
       */


      const app = document.querySelector('ion-app') || document.body;
      /**
       * Create a placeholder comment so that
       * we can return this component to where
       * it was previously.
       */

      Reference = document.createComment('ionic teleport');
      BaseComponent.parentNode.insertBefore(Reference, BaseComponent);
      app.appendChild(BaseComponent);
      return BaseComponent;
    });

    return function attachViewToDom(_x7, _x8) {
      return _ref2.apply(this, arguments);
    };
  }();

  const removeViewFromDom = () => {
    /**
     * Return component to where it was previously in the DOM.
     */
    if (BaseComponent && Reference) {
      Reference.parentNode.insertBefore(BaseComponent, Reference);
      Reference.remove();
    }

    return Promise.resolve();
  };

  return {
    attachViewToDom,
    removeViewFromDom
  };
};



/***/ }),

/***/ 70634:
/*!**************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/haptic-683b3b3c.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "a": () => (/* binding */ hapticSelectionStart),
/* harmony export */   "b": () => (/* binding */ hapticSelectionChanged),
/* harmony export */   "c": () => (/* binding */ hapticSelection),
/* harmony export */   "d": () => (/* binding */ hapticImpact),
/* harmony export */   "h": () => (/* binding */ hapticSelectionEnd)
/* harmony export */ });
/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */
const HapticEngine = {
  getEngine() {
    var _a;

    const win = window;
    return win.TapticEngine || ((_a = win.Capacitor) === null || _a === void 0 ? void 0 : _a.isPluginAvailable('Haptics')) && win.Capacitor.Plugins.Haptics;
  },

  available() {
    return !!this.getEngine();
  },

  isCordova() {
    return !!window.TapticEngine;
  },

  isCapacitor() {
    const win = window;
    return !!win.Capacitor;
  },

  impact(options) {
    const engine = this.getEngine();

    if (!engine) {
      return;
    }

    const style = this.isCapacitor() ? options.style.toUpperCase() : options.style;
    engine.impact({
      style
    });
  },

  notification(options) {
    const engine = this.getEngine();

    if (!engine) {
      return;
    }

    const style = this.isCapacitor() ? options.style.toUpperCase() : options.style;
    engine.notification({
      style
    });
  },

  selection() {
    this.impact({
      style: 'light'
    });
  },

  selectionStart() {
    const engine = this.getEngine();

    if (!engine) {
      return;
    }

    if (this.isCapacitor()) {
      engine.selectionStart();
    } else {
      engine.gestureSelectionStart();
    }
  },

  selectionChanged() {
    const engine = this.getEngine();

    if (!engine) {
      return;
    }

    if (this.isCapacitor()) {
      engine.selectionChanged();
    } else {
      engine.gestureSelectionChanged();
    }
  },

  selectionEnd() {
    const engine = this.getEngine();

    if (!engine) {
      return;
    }

    if (this.isCapacitor()) {
      engine.selectionEnd();
    } else {
      engine.gestureSelectionEnd();
    }
  }

};
/**
 * Trigger a selection changed haptic event. Good for one-time events
 * (not for gestures)
 */

const hapticSelection = () => {
  HapticEngine.selection();
};
/**
 * Tell the haptic engine that a gesture for a selection change is starting.
 */


const hapticSelectionStart = () => {
  HapticEngine.selectionStart();
};
/**
 * Tell the haptic engine that a selection changed during a gesture.
 */


const hapticSelectionChanged = () => {
  HapticEngine.selectionChanged();
};
/**
 * Tell the haptic engine we are done with a gesture. This needs to be
 * called lest resources are not properly recycled.
 */


const hapticSelectionEnd = () => {
  HapticEngine.selectionEnd();
};
/**
 * Use this to indicate success/failure/warning to the user.
 * options should be of the type `{ style: 'light' }` (or `medium`/`heavy`)
 */


const hapticImpact = options => {
  HapticEngine.impact(options);
};



/***/ }),

/***/ 61314:
/*!*************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/index-8bf9b0cd.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "I": () => (/* binding */ ION_CONTENT_ELEMENT_SELECTOR),
/* harmony export */   "a": () => (/* binding */ findIonContent),
/* harmony export */   "b": () => (/* binding */ ION_CONTENT_CLASS_SELECTOR),
/* harmony export */   "c": () => (/* binding */ scrollByPoint),
/* harmony export */   "d": () => (/* binding */ disableContentScrollY),
/* harmony export */   "f": () => (/* binding */ findClosestIonContent),
/* harmony export */   "g": () => (/* binding */ getScrollElement),
/* harmony export */   "i": () => (/* binding */ isIonContent),
/* harmony export */   "p": () => (/* binding */ printIonContentErrorMsg),
/* harmony export */   "r": () => (/* binding */ resetContentScrollY),
/* harmony export */   "s": () => (/* binding */ scrollToTop)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var _helpers_4d272360_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers-4d272360.js */ 59158);
/* harmony import */ var _index_9ac92660_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index-9ac92660.js */ 2141);


/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */


const ION_CONTENT_TAG_NAME = 'ION-CONTENT';
const ION_CONTENT_ELEMENT_SELECTOR = 'ion-content';
const ION_CONTENT_CLASS_SELECTOR = '.ion-content-scroll-host';
/**
 * Selector used for implementations reliant on `<ion-content>` for scroll event changes.
 *
 * Developers should use the `.ion-content-scroll-host` selector to target the element emitting
 * scroll events. With virtual scroll implementations this will be the host element for
 * the scroll viewport.
 */

const ION_CONTENT_SELECTOR = `${ION_CONTENT_ELEMENT_SELECTOR}, ${ION_CONTENT_CLASS_SELECTOR}`;

const isIonContent = el => el && el.tagName === ION_CONTENT_TAG_NAME;
/**
 * Waits for the element host fully initialize before
 * returning the inner scroll element.
 *
 * For `ion-content` the scroll target will be the result
 * of the `getScrollElement` function.
 *
 * For custom implementations it will be the element host
 * or a selector within the host, if supplied through `scrollTarget`.
 */


const getScrollElement = /*#__PURE__*/function () {
  var _ref = (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (el) {
    if (isIonContent(el)) {
      yield new Promise(resolve => (0,_helpers_4d272360_js__WEBPACK_IMPORTED_MODULE_1__.c)(el, resolve));
      return el.getScrollElement();
    }

    return el;
  });

  return function getScrollElement(_x) {
    return _ref.apply(this, arguments);
  };
}();
/**
 * Queries the element matching the selector for IonContent.
 * See ION_CONTENT_SELECTOR for the selector used.
 */


const findIonContent = el => {
  /**
   * First we try to query the custom scroll host selector in cases where
   * the implementation is using an outer `ion-content` with an inner custom
   * scroll container.
   */
  const customContentHost = el.querySelector(ION_CONTENT_CLASS_SELECTOR);

  if (customContentHost) {
    return customContentHost;
  }

  return el.querySelector(ION_CONTENT_SELECTOR);
};
/**
 * Queries the closest element matching the selector for IonContent.
 */


const findClosestIonContent = el => {
  return el.closest(ION_CONTENT_SELECTOR);
};
/**
 * Scrolls to the top of the element. If an `ion-content` is found, it will scroll
 * using the public API `scrollToTop` with a duration.
 */


const scrollToTop = (el, durationMs) => {
  if (isIonContent(el)) {
    const content = el;
    return content.scrollToTop(durationMs);
  }

  return Promise.resolve(el.scrollTo({
    top: 0,
    left: 0,
    behavior: durationMs > 0 ? 'smooth' : 'auto'
  }));
};
/**
 * Scrolls by a specified X/Y distance in the component. If an `ion-content` is found, it will scroll
 * using the public API `scrollByPoint` with a duration.
 */


const scrollByPoint = (el, x, y, durationMs) => {
  if (isIonContent(el)) {
    const content = el;
    return content.scrollByPoint(x, y, durationMs);
  }

  return Promise.resolve(el.scrollBy({
    top: y,
    left: x,
    behavior: durationMs > 0 ? 'smooth' : 'auto'
  }));
};
/**
 * Prints an error informing developers that an implementation requires an element to be used
 * within either the `ion-content` selector or the `.ion-content-scroll-host` class.
 */


const printIonContentErrorMsg = el => {
  return (0,_index_9ac92660_js__WEBPACK_IMPORTED_MODULE_2__.a)(el, ION_CONTENT_ELEMENT_SELECTOR);
};
/**
 * Several components in Ionic need to prevent scrolling
 * during a gesture (card modal, range, item sliding, etc).
 * Use this utility to account for ion-content and custom content hosts.
 */


const disableContentScrollY = contentEl => {
  if (isIonContent(contentEl)) {
    const ionContent = contentEl;
    const initialScrollY = ionContent.scrollY;
    ionContent.scrollY = false;
    /**
     * This should be passed into resetContentScrollY
     * so that we can revert ion-content's scrollY to the
     * correct state. For example, if scrollY = false
     * initially, we do not want to enable scrolling
     * when we call resetContentScrollY.
     */

    return initialScrollY;
  } else {
    contentEl.style.setProperty('overflow', 'hidden');
    return true;
  }
};

const resetContentScrollY = (contentEl, initialScrollY) => {
  if (isIonContent(contentEl)) {
    contentEl.scrollY = initialScrollY;
  } else {
    contentEl.style.removeProperty('overflow');
  }
};



/***/ }),

/***/ 2141:
/*!*************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/index-9ac92660.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "a": () => (/* binding */ printRequiredElementError),
/* harmony export */   "b": () => (/* binding */ printIonError),
/* harmony export */   "p": () => (/* binding */ printIonWarning)
/* harmony export */ });
/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */

/**
 * Logs a warning to the console with an Ionic prefix
 * to indicate the library that is warning the developer.
 *
 * @param message - The string message to be logged to the console.
 */
const printIonWarning = message => {
  return console.warn(`[Ionic Warning]: ${message}`);
};
/*
 * Logs an error to the console with an Ionic prefix
 * to indicate the library that is warning the developer.
 *
 * @param message - The string message to be logged to the console.
 * @param params - Additional arguments to supply to the console.error.
 */


const printIonError = (message, ...params) => {
  return console.error(`[Ionic Error]: ${message}`, ...params);
};
/**
 * Prints an error informing developers that an implementation requires an element to be used
 * within a specific selector.
 *
 * @param el The web component element this is requiring the element.
 * @param targetSelectors The selector or selectors that were not found.
 */


const printRequiredElementError = (el, ...targetSelectors) => {
  return console.error(`<${el.tagName.toLowerCase()}> must be used inside ${targetSelectors.join(' or ')}.`);
};



/***/ }),

/***/ 43814:
/*!*************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/index-d74f4afc.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "a": () => (/* binding */ arrowBackSharp),
/* harmony export */   "b": () => (/* binding */ closeCircle),
/* harmony export */   "c": () => (/* binding */ chevronBack),
/* harmony export */   "d": () => (/* binding */ closeSharp),
/* harmony export */   "e": () => (/* binding */ searchSharp),
/* harmony export */   "f": () => (/* binding */ caretBackSharp),
/* harmony export */   "g": () => (/* binding */ arrowDown),
/* harmony export */   "h": () => (/* binding */ reorderTwoSharp),
/* harmony export */   "i": () => (/* binding */ chevronDown),
/* harmony export */   "j": () => (/* binding */ chevronForwardOutline),
/* harmony export */   "k": () => (/* binding */ ellipsisHorizontal),
/* harmony export */   "l": () => (/* binding */ chevronForward),
/* harmony export */   "m": () => (/* binding */ caretUpSharp),
/* harmony export */   "n": () => (/* binding */ caretDownSharp),
/* harmony export */   "o": () => (/* binding */ close),
/* harmony export */   "p": () => (/* binding */ menuOutline),
/* harmony export */   "q": () => (/* binding */ menuSharp),
/* harmony export */   "r": () => (/* binding */ reorderThreeOutline),
/* harmony export */   "s": () => (/* binding */ searchOutline)
/* harmony export */ });
/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */

/* Ionicons v6.0.2, ES Modules */
const arrowBackSharp = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Arrow Back</title><path stroke-linecap='square' stroke-miterlimit='10' stroke-width='48' d='M244 400L100 256l144-144M120 256h292' class='ionicon-fill-none'/></svg>";
const arrowDown = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Arrow Down</title><path stroke-linecap='round' stroke-linejoin='round' stroke-width='48' d='M112 268l144 144 144-144M256 392V100' class='ionicon-fill-none'/></svg>";
const caretBackSharp = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Caret Back</title><path d='M368 64L144 256l224 192V64z'/></svg>";
const caretDownSharp = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Caret Down</title><path d='M64 144l192 224 192-224H64z'/></svg>";
const caretUpSharp = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Caret Up</title><path d='M448 368L256 144 64 368h384z'/></svg>";
const chevronBack = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Chevron Back</title><path stroke-linecap='round' stroke-linejoin='round' stroke-width='48' d='M328 112L184 256l144 144' class='ionicon-fill-none'/></svg>";
const chevronDown = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Chevron Down</title><path stroke-linecap='round' stroke-linejoin='round' stroke-width='48' d='M112 184l144 144 144-144' class='ionicon-fill-none'/></svg>";
const chevronForward = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Chevron Forward</title><path stroke-linecap='round' stroke-linejoin='round' stroke-width='48' d='M184 112l144 144-144 144' class='ionicon-fill-none'/></svg>";
const chevronForwardOutline = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Chevron Forward</title><path stroke-linecap='round' stroke-linejoin='round' stroke-width='48' d='M184 112l144 144-144 144' class='ionicon-fill-none'/></svg>";
const close = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Close</title><path d='M289.94 256l95-95A24 24 0 00351 127l-95 95-95-95a24 24 0 00-34 34l95 95-95 95a24 24 0 1034 34l95-95 95 95a24 24 0 0034-34z'/></svg>";
const closeCircle = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Close Circle</title><path d='M256 48C141.31 48 48 141.31 48 256s93.31 208 208 208 208-93.31 208-208S370.69 48 256 48zm75.31 260.69a16 16 0 11-22.62 22.62L256 278.63l-52.69 52.68a16 16 0 01-22.62-22.62L233.37 256l-52.68-52.69a16 16 0 0122.62-22.62L256 233.37l52.69-52.68a16 16 0 0122.62 22.62L278.63 256z'/></svg>";
const closeSharp = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Close</title><path d='M400 145.49L366.51 112 256 222.51 145.49 112 112 145.49 222.51 256 112 366.51 145.49 400 256 289.49 366.51 400 400 366.51 289.49 256 400 145.49z'/></svg>";
const ellipsisHorizontal = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Ellipsis Horizontal</title><circle cx='256' cy='256' r='48'/><circle cx='416' cy='256' r='48'/><circle cx='96' cy='256' r='48'/></svg>";
const menuOutline = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Menu</title><path stroke-linecap='round' stroke-miterlimit='10' d='M80 160h352M80 256h352M80 352h352' class='ionicon-fill-none ionicon-stroke-width'/></svg>";
const menuSharp = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Menu</title><path d='M64 384h384v-42.67H64zm0-106.67h384v-42.66H64zM64 128v42.67h384V128z'/></svg>";
const reorderThreeOutline = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Reorder Three</title><path stroke-linecap='round' stroke-linejoin='round' d='M96 256h320M96 176h320M96 336h320' class='ionicon-fill-none ionicon-stroke-width'/></svg>";
const reorderTwoSharp = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Reorder Two</title><path stroke-linecap='square' stroke-linejoin='round' stroke-width='44' d='M118 304h276M118 208h276' class='ionicon-fill-none'/></svg>";
const searchOutline = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Search</title><path d='M221.09 64a157.09 157.09 0 10157.09 157.09A157.1 157.1 0 00221.09 64z' stroke-miterlimit='10' class='ionicon-fill-none ionicon-stroke-width'/><path stroke-linecap='round' stroke-miterlimit='10' d='M338.29 338.29L448 448' class='ionicon-fill-none ionicon-stroke-width'/></svg>";
const searchSharp = "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'><title>Search</title><path d='M464 428L339.92 303.9a160.48 160.48 0 0030.72-94.58C370.64 120.37 298.27 48 209.32 48S48 120.37 48 209.32s72.37 161.32 161.32 161.32a160.48 160.48 0 0094.58-30.72L428 464zM209.32 319.69a110.38 110.38 0 11110.37-110.37 110.5 110.5 0 01-110.37 110.37z'/></svg>";


/***/ }),

/***/ 96524:
/*!****************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/keyboard-4d5544a0.js ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "KEYBOARD_DID_CLOSE": () => (/* binding */ KEYBOARD_DID_CLOSE),
/* harmony export */   "KEYBOARD_DID_OPEN": () => (/* binding */ KEYBOARD_DID_OPEN),
/* harmony export */   "copyVisualViewport": () => (/* binding */ copyVisualViewport),
/* harmony export */   "keyboardDidClose": () => (/* binding */ keyboardDidClose),
/* harmony export */   "keyboardDidOpen": () => (/* binding */ keyboardDidOpen),
/* harmony export */   "keyboardDidResize": () => (/* binding */ keyboardDidResize),
/* harmony export */   "resetKeyboardAssist": () => (/* binding */ resetKeyboardAssist),
/* harmony export */   "setKeyboardClose": () => (/* binding */ setKeyboardClose),
/* harmony export */   "setKeyboardOpen": () => (/* binding */ setKeyboardOpen),
/* harmony export */   "startKeyboardAssist": () => (/* binding */ startKeyboardAssist),
/* harmony export */   "trackViewportChanges": () => (/* binding */ trackViewportChanges)
/* harmony export */ });
/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */
const KEYBOARD_DID_OPEN = 'ionKeyboardDidShow';
const KEYBOARD_DID_CLOSE = 'ionKeyboardDidHide';
const KEYBOARD_THRESHOLD = 150;
let previousVisualViewport = {};
let currentVisualViewport = {};
let keyboardOpen = false;
/**
 * This is only used for tests
 */

const resetKeyboardAssist = () => {
  previousVisualViewport = {};
  currentVisualViewport = {};
  keyboardOpen = false;
};

const startKeyboardAssist = win => {
  startNativeListeners(win);

  if (!win.visualViewport) {
    return;
  }

  currentVisualViewport = copyVisualViewport(win.visualViewport);

  win.visualViewport.onresize = () => {
    trackViewportChanges(win);

    if (keyboardDidOpen() || keyboardDidResize(win)) {
      setKeyboardOpen(win);
    } else if (keyboardDidClose(win)) {
      setKeyboardClose(win);
    }
  };
};
/**
 * Listen for events fired by native keyboard plugin
 * in Capacitor/Cordova so devs only need to listen
 * in one place.
 */


const startNativeListeners = win => {
  win.addEventListener('keyboardDidShow', ev => setKeyboardOpen(win, ev));
  win.addEventListener('keyboardDidHide', () => setKeyboardClose(win));
};

const setKeyboardOpen = (win, ev) => {
  fireKeyboardOpenEvent(win, ev);
  keyboardOpen = true;
};

const setKeyboardClose = win => {
  fireKeyboardCloseEvent(win);
  keyboardOpen = false;
};
/**
 * Returns `true` if the `keyboardOpen` flag is not
 * set, the previous visual viewport width equal the current
 * visual viewport width, and if the scaled difference
 * of the previous visual viewport height minus the current
 * visual viewport height is greater than KEYBOARD_THRESHOLD
 *
 * We need to be able to accommodate users who have zooming
 * enabled in their browser (or have zoomed in manually) which
 * is why we take into account the current visual viewport's
 * scale value.
 */


const keyboardDidOpen = () => {
  const scaledHeightDifference = (previousVisualViewport.height - currentVisualViewport.height) * currentVisualViewport.scale;
  return !keyboardOpen && previousVisualViewport.width === currentVisualViewport.width && scaledHeightDifference > KEYBOARD_THRESHOLD;
};
/**
 * Returns `true` if the keyboard is open,
 * but the keyboard did not close
 */


const keyboardDidResize = win => {
  return keyboardOpen && !keyboardDidClose(win);
};
/**
 * Determine if the keyboard was closed
 * Returns `true` if the `keyboardOpen` flag is set and
 * the current visual viewport height equals the
 * layout viewport height.
 */


const keyboardDidClose = win => {
  return keyboardOpen && currentVisualViewport.height === win.innerHeight;
};
/**
 * Dispatch a keyboard open event
 */


const fireKeyboardOpenEvent = (win, nativeEv) => {
  const keyboardHeight = nativeEv ? nativeEv.keyboardHeight : win.innerHeight - currentVisualViewport.height;
  const ev = new CustomEvent(KEYBOARD_DID_OPEN, {
    detail: {
      keyboardHeight
    }
  });
  win.dispatchEvent(ev);
};
/**
 * Dispatch a keyboard close event
 */


const fireKeyboardCloseEvent = win => {
  const ev = new CustomEvent(KEYBOARD_DID_CLOSE);
  win.dispatchEvent(ev);
};
/**
 * Given a window object, create a copy of
 * the current visual and layout viewport states
 * while also preserving the previous visual and
 * layout viewport states
 */


const trackViewportChanges = win => {
  previousVisualViewport = Object.assign({}, currentVisualViewport);
  currentVisualViewport = copyVisualViewport(win.visualViewport);
};
/**
 * Creates a deep copy of the visual viewport
 * at a given state
 */


const copyVisualViewport = visualViewport => {
  return {
    width: Math.round(visualViewport.width),
    height: Math.round(visualViewport.height),
    offsetTop: visualViewport.offsetTop,
    offsetLeft: visualViewport.offsetLeft,
    pageTop: visualViewport.pageTop,
    pageLeft: visualViewport.pageLeft,
    scale: visualViewport.scale
  };
};



/***/ }),

/***/ 43844:
/*!***********************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/spinner-configs-5d6b6fe7.js ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "S": () => (/* binding */ SPINNERS)
/* harmony export */ });
/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */
const spinners = {
  bubbles: {
    dur: 1000,
    circles: 9,
    fn: (dur, index, total) => {
      const animationDelay = `${dur * index / total - dur}ms`;
      const angle = 2 * Math.PI * index / total;
      return {
        r: 5,
        style: {
          top: `${9 * Math.sin(angle)}px`,
          left: `${9 * Math.cos(angle)}px`,
          'animation-delay': animationDelay
        }
      };
    }
  },
  circles: {
    dur: 1000,
    circles: 8,
    fn: (dur, index, total) => {
      const step = index / total;
      const animationDelay = `${dur * step - dur}ms`;
      const angle = 2 * Math.PI * step;
      return {
        r: 5,
        style: {
          top: `${9 * Math.sin(angle)}px`,
          left: `${9 * Math.cos(angle)}px`,
          'animation-delay': animationDelay
        }
      };
    }
  },
  circular: {
    dur: 1400,
    elmDuration: true,
    circles: 1,
    fn: () => {
      return {
        r: 20,
        cx: 48,
        cy: 48,
        fill: 'none',
        viewBox: '24 24 48 48',
        transform: 'translate(0,0)',
        style: {}
      };
    }
  },
  crescent: {
    dur: 750,
    circles: 1,
    fn: () => {
      return {
        r: 26,
        style: {}
      };
    }
  },
  dots: {
    dur: 750,
    circles: 3,
    fn: (_, index) => {
      const animationDelay = -(110 * index) + 'ms';
      return {
        r: 6,
        style: {
          left: `${9 - 9 * index}px`,
          'animation-delay': animationDelay
        }
      };
    }
  },
  lines: {
    dur: 1000,
    lines: 8,
    fn: (dur, index, total) => {
      const transform = `rotate(${360 / total * index + (index < total / 2 ? 180 : -180)}deg)`;
      const animationDelay = `${dur * index / total - dur}ms`;
      return {
        y1: 14,
        y2: 26,
        style: {
          transform: transform,
          'animation-delay': animationDelay
        }
      };
    }
  },
  'lines-small': {
    dur: 1000,
    lines: 8,
    fn: (dur, index, total) => {
      const transform = `rotate(${360 / total * index + (index < total / 2 ? 180 : -180)}deg)`;
      const animationDelay = `${dur * index / total - dur}ms`;
      return {
        y1: 12,
        y2: 20,
        style: {
          transform: transform,
          'animation-delay': animationDelay
        }
      };
    }
  },
  'lines-sharp': {
    dur: 1000,
    lines: 12,
    fn: (dur, index, total) => {
      const transform = `rotate(${30 * index + (index < 6 ? 180 : -180)}deg)`;
      const animationDelay = `${dur * index / total - dur}ms`;
      return {
        y1: 17,
        y2: 29,
        style: {
          transform: transform,
          'animation-delay': animationDelay
        }
      };
    }
  },
  'lines-sharp-small': {
    dur: 1000,
    lines: 12,
    fn: (dur, index, total) => {
      const transform = `rotate(${30 * index + (index < 6 ? 180 : -180)}deg)`;
      const animationDelay = `${dur * index / total - dur}ms`;
      return {
        y1: 12,
        y2: 20,
        style: {
          transform: transform,
          'animation-delay': animationDelay
        }
      };
    }
  }
};
const SPINNERS = spinners;


/***/ }),

/***/ 21812:
/*!******************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/swipe-back-fa30a130.js ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "createSwipeBackGesture": () => (/* binding */ createSwipeBackGesture)
/* harmony export */ });
/* harmony import */ var _helpers_4d272360_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers-4d272360.js */ 59158);
/* harmony import */ var _dir_e8b767a8_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dir-e8b767a8.js */ 17481);
/* harmony import */ var _index_3f1a7d95_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index-3f1a7d95.js */ 82172);
/* harmony import */ var _gesture_controller_17e82006_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./gesture-controller-17e82006.js */ 70607);
/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */





const createSwipeBackGesture = (el, canStartHandler, onStartHandler, onMoveHandler, onEndHandler) => {
  const win = el.ownerDocument.defaultView;
  const rtl = (0,_dir_e8b767a8_js__WEBPACK_IMPORTED_MODULE_1__.i)(el);
  /**
   * Determine if a gesture is near the edge
   * of the screen. If true, then the swipe
   * to go back gesture should proceed.
   */

  const isAtEdge = detail => {
    const threshold = 50;
    const {
      startX
    } = detail;

    if (rtl) {
      return startX >= win.innerWidth - threshold;
    }

    return startX <= threshold;
  };

  const getDeltaX = detail => {
    return rtl ? -detail.deltaX : detail.deltaX;
  };

  const getVelocityX = detail => {
    return rtl ? -detail.velocityX : detail.velocityX;
  };

  const canStart = detail => {
    return isAtEdge(detail) && canStartHandler();
  };

  const onMove = detail => {
    // set the transition animation's progress
    const delta = getDeltaX(detail);
    const stepValue = delta / win.innerWidth;
    onMoveHandler(stepValue);
  };

  const onEnd = detail => {
    // the swipe back gesture has ended
    const delta = getDeltaX(detail);
    const width = win.innerWidth;
    const stepValue = delta / width;
    const velocity = getVelocityX(detail);
    const z = width / 2.0;
    const shouldComplete = velocity >= 0 && (velocity > 0.2 || delta > z);
    const missing = shouldComplete ? 1 - stepValue : stepValue;
    const missingDistance = missing * width;
    let realDur = 0;

    if (missingDistance > 5) {
      const dur = missingDistance / Math.abs(velocity);
      realDur = Math.min(dur, 540);
    }
    /**
     * TODO: stepValue can sometimes return negative values
     * or values greater than 1 which should not be possible.
     * Need to investigate more to find where the issue is.
     */


    onEndHandler(shouldComplete, stepValue <= 0 ? 0.01 : (0,_helpers_4d272360_js__WEBPACK_IMPORTED_MODULE_0__.l)(0, stepValue, 0.9999), realDur);
  };

  return (0,_index_3f1a7d95_js__WEBPACK_IMPORTED_MODULE_2__.createGesture)({
    el,
    gestureName: 'goback-swipe',
    gesturePriority: 40,
    threshold: 10,
    canStart,
    onStart: onStartHandler,
    onMove,
    onEnd
  });
};



/***/ }),

/***/ 50320:
/*!*************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/theme-7670341c.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "c": () => (/* binding */ createColorClasses),
/* harmony export */   "g": () => (/* binding */ getClassMap),
/* harmony export */   "h": () => (/* binding */ hostContext),
/* harmony export */   "o": () => (/* binding */ openURL)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);


/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */
const hostContext = (selector, el) => {
  return el.closest(selector) !== null;
};
/**
 * Create the mode and color classes for the component based on the classes passed in
 */


const createColorClasses = (color, cssClassMap) => {
  return typeof color === 'string' && color.length > 0 ? Object.assign({
    'ion-color': true,
    [`ion-color-${color}`]: true
  }, cssClassMap) : cssClassMap;
};

const getClassList = classes => {
  if (classes !== undefined) {
    const array = Array.isArray(classes) ? classes : classes.split(' ');
    return array.filter(c => c != null).map(c => c.trim()).filter(c => c !== '');
  }

  return [];
};

const getClassMap = classes => {
  const map = {};
  getClassList(classes).forEach(c => map[c] = true);
  return map;
};

const SCHEME = /^[a-z][a-z0-9+\-.]*:/;

const openURL = /*#__PURE__*/function () {
  var _ref = (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (url, ev, direction, animation) {
    if (url != null && url[0] !== '#' && !SCHEME.test(url)) {
      const router = document.querySelector('ion-router');

      if (router) {
        if (ev != null) {
          ev.preventDefault();
        }

        return router.push(url, direction, animation);
      }
    }

    return false;
  });

  return function openURL(_x, _x2, _x3, _x4) {
    return _ref.apply(this, arguments);
  };
}();



/***/ }),

/***/ 45144:
/*!********************************************************************************!*\
  !*** ./src/app/arvisitschedule/aruserselect/aruserselect.page.scss?ngResource ***!
  \********************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcnVzZXJzZWxlY3QucGFnZS5zY3NzIn0= */";

/***/ }),

/***/ 30314:
/*!*************************************************************************************************************!*\
  !*** ./src/app/order-approval/show-approval-details-modal/show-approval-details-modal.page.scss?ngResource ***!
  \*************************************************************************************************************/
/***/ ((module) => {

module.exports = "::ng-deep.ngx-datatable.bootstrap .datatable-body .datatable-body-row.active {\n  background-color: #F39E20 !important;\n}\n\n@media only screen and (min-width: 481px) {\n  ::ng-deep.ngx-datatable.bootstrap .datatable-header .datatable-header-cell {\n    padding-left: 0.75rem !important;\n    padding-right: 0.25rem !important;\n    padding-top: 0.25rem !important;\n    padding-bottom: 0.25rem !important;\n  }\n}\n\n@media (min-width: 1281px) {\n  ::ng-deep.ngx-datatable.bootstrap .datatable-header .datatable-header-cell {\n    padding: 0.75rem !important;\n  }\n}\n\n::ng-deep.ngx-datatable.bootstrap .datatable-header {\n  font-weight: bold !important;\n  background-color: #F39E20 !important;\n}\n\n@media screen and (max-width: 800px) {\n  .desktop-hidden {\n    display: initial;\n  }\n\n  .mobile-hidden {\n    display: none;\n  }\n}\n\n@media screen and (min-width: 800px) {\n  .desktop-hidden {\n    display: none;\n  }\n\n  .mobile-hidden {\n    display: initial;\n  }\n}\n\n@media only screen and (min-height: 768px) and (min-width: 768px) {\n  .sc-ion-modal-ios-h {\n    --width: 900px !important;\n    --height: 626px !important;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNob3ctYXBwcm92YWwtZGV0YWlscy1tb2RhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQ0FBQTtBQUNKOztBQUVBO0VBQ0k7SUFFSSxnQ0FBQTtJQUNBLGlDQUFBO0lBQ0EsK0JBQUE7SUFDQSxrQ0FBQTtFQUFOO0FBQ0Y7O0FBR0E7RUFFSTtJQUVBLDJCQUFBO0VBSEY7QUFDRjs7QUFRQTtFQUNFLDRCQUFBO0VBQ0Esb0NBQUE7QUFORjs7QUFVQTtFQUNJO0lBQ0UsZ0JBQUE7RUFQSjs7RUFTRTtJQUNFLGFBQUE7RUFOSjtBQUNGOztBQVFFO0VBQ0U7SUFDRSxhQUFBO0VBTko7O0VBUUU7SUFDRSxnQkFBQTtFQUxKO0FBQ0Y7O0FBT0U7RUFDRTtJQUNFLHlCQUFBO0lBQ0EsMEJBQUE7RUFMSjtBQUNGIiwiZmlsZSI6InNob3ctYXBwcm92YWwtZGV0YWlscy1tb2RhbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6Om5nLWRlZXAubmd4LWRhdGF0YWJsZS5ib290c3RyYXAgLmRhdGF0YWJsZS1ib2R5IC5kYXRhdGFibGUtYm9keS1yb3cuYWN0aXZle1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGMzlFMjAgIWltcG9ydGFudDtcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOjQ4MXB4KSB7XG4gICAgOjpuZy1kZWVwLm5neC1kYXRhdGFibGUuYm9vdHN0cmFwIC5kYXRhdGFibGUtaGVhZGVyIC5kYXRhdGFibGUtaGVhZGVyLWNlbGx7XG5cbiAgICAgICAgcGFkZGluZy1sZWZ0OjAuNzVyZW0gIWltcG9ydGFudDsgXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDAuMjVyZW0gIWltcG9ydGFudDtcbiAgICAgICAgcGFkZGluZy10b3A6IDAwLjI1cmVtICFpbXBvcnRhbnQ7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAwMC4yNXJlbSAhaW1wb3J0YW50O1xuICAgICAgICAgICAgXG4gICAgIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOjEyODFweCkgeyAgXG4gICAgXG4gICAgOjpuZy1kZWVwLm5neC1kYXRhdGFibGUuYm9vdHN0cmFwIC5kYXRhdGFibGUtaGVhZGVyIC5kYXRhdGFibGUtaGVhZGVyLWNlbGx7XG5cbiAgICBwYWRkaW5nOiAwLjc1cmVtICFpbXBvcnRhbnQ7XG4gICAvLyB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbiAgICBcbiBcbiB9fVxuXG46Om5nLWRlZXAubmd4LWRhdGF0YWJsZS5ib290c3RyYXAgLmRhdGF0YWJsZS1oZWFkZXJ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQtY29sb3I6ICNGMzlFMjAgIWltcG9ydGFudDtcbn1cblxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA4MDBweCkge1xuICAgIC5kZXNrdG9wLWhpZGRlbiB7XG4gICAgICBkaXNwbGF5OiBpbml0aWFsO1xuICAgIH1cbiAgICAubW9iaWxlLWhpZGRlbiB7XG4gICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbiAgfVxuICBAbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA4MDBweCkge1xuICAgIC5kZXNrdG9wLWhpZGRlbiB7XG4gICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbiAgICAubW9iaWxlLWhpZGRlbiB7XG4gICAgICBkaXNwbGF5OiBpbml0aWFsO1xuICAgIH1cbiAgfVxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4taGVpZ2h0OiA3NjhweCkgYW5kIChtaW4td2lkdGg6IDc2OHB4KXtcbiAgICAuc2MtaW9uLW1vZGFsLWlvcy1oe1xuICAgICAgLS13aWR0aDogOTAwcHggIWltcG9ydGFudDtcbiAgICAgIC0taGVpZ2h0OiA2MjZweCAhaW1wb3J0YW50O1xuICAgIH1cbiAgfVxuIl19 */";

/***/ }),

/***/ 72244:
/*!********************************************************************************!*\
  !*** ./src/app/arvisitschedule/aruserselect/aruserselect.page.html?ngResource ***!
  \********************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar  color=\"primary\">\n    <ion-title>Cross Assignment</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-item>\n    <ion-label position=\"stacked\">Select AR User&nbsp;<span style=\"color:red!important\">*</span></ion-label>\n    <ionic-selectable [(ngModel)]=\"selecteduser\" closeButtonSlot=\"end\" placeholder=\"Select AR User\" headerColor=\"primary\"\n     [shouldFocusSearchbar]=\"true\"\n    [items]=\"aruserlist\"\n    itemValueField=\"userid\" \n    itemTextField=\"username\" \n    [canSearch]=\"true\"\n    (onClose)=\"onARUserClose($event)\"\n    [hasInfiniteScroll]=\"true\"\n    (onInfiniteScroll)=\"doInfiniteARUser($event)\"\n    (onSearch)=\"onARUsersearchChange($event)\">\n    </ionic-selectable>\n  </ion-item>\n  <!-- <ion-button (click)=\"onClickAssignPlan()\" expand=\"block\" color=\"primary\" fill=\"outline\">\n    Assign Plan\n  </ion-button> -->\n</ion-content>\n<section class=\"full-width\">\n  <ion-row>\n    <ion-col>\n  <ion-button color=\"primary\" expand=\"full\" (click)=\"onClickAssignPlan()\">Assign Plan</ion-button>\n</ion-col>\n<ion-col>\n  <ion-button color=\"primary\" expand=\"full\" (click)=\"onClose()\">CLOSE</ion-button>\n</ion-col>\n</ion-row>\n</section>\n";

/***/ }),

/***/ 43650:
/*!*************************************************************************************************************!*\
  !*** ./src/app/order-approval/show-approval-details-modal/show-approval-details-modal.page.html?ngResource ***!
  \*************************************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Details</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"closeModal()\">\n        <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ngx-datatable  #datatable style=\"height: 600px; overflow-y:visible\" class=\"bootstrap resizeable\"\n                 [rows]=\"rows\"\n                 [columns]=\"column_data\"\n                 [columnMode]=\"ColumnMode.force\"\n                 [headerHeight]=\"50\"\n                 [footerHeight]=\"50\"\n                 [rowHeight]=\"40\"\n                 [selectionType]=\"SelectionType.single\"\n                 [scrollbarH]=\"true\"\n                 [scrollbarV]=\"true\"\n                 [reorderable]=\"true\"\n                 [loadingIndicator]=\"loadingIndicator\"\n                 [messages]=\"{emptyMessage: 'There are no records...'}\"\n                 [swapColumns]=\"true\">\n    \n  </ngx-datatable>\n \n \n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=common.js.map