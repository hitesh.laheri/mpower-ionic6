"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_options_options_module_ts"],{

/***/ 76305:
/*!*******************************************!*\
  !*** ./src/app/options/options.module.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OptionsPageModule": () => (/* binding */ OptionsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _options_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./options.page */ 55253);







const routes = [
    {
        path: '',
        component: _options_page__WEBPACK_IMPORTED_MODULE_0__.OptionsPage
    }
];
let OptionsPageModule = class OptionsPageModule {
};
OptionsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_options_page__WEBPACK_IMPORTED_MODULE_0__.OptionsPage]
    })
], OptionsPageModule);



/***/ }),

/***/ 55253:
/*!*****************************************!*\
  !*** ./src/app/options/options.page.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OptionsPage": () => (/* binding */ OptionsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _options_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./options.page.html?ngResource */ 81982);
/* harmony import */ var _options_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./options.page.scss?ngResource */ 98997);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);




let OptionsPage = class OptionsPage {
    constructor() { }
    ngOnInit() {
    }
    onClickNewCustomer() {
    }
    onClickNewOrder() {
    }
    onClickOrderStatus() {
    }
    onClickfinStatus() {
    }
    onClickExistingCustomer() { }
};
OptionsPage.ctorParameters = () => [];
OptionsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-options',
        template: _options_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_options_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], OptionsPage);



/***/ }),

/***/ 98997:
/*!******************************************************!*\
  !*** ./src/app/options/options.page.scss?ngResource ***!
  \******************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJvcHRpb25zLnBhZ2Uuc2NzcyJ9 */";

/***/ }),

/***/ 81982:
/*!******************************************************!*\
  !*** ./src/app/options/options.page.html?ngResource ***!
  \******************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n        <ion-back-button defaultHref=\"login\"></ion-back-button>\n      </ion-buttons>\n  <ion-title>Options</ion-title>\n</ion-toolbar>\n</ion-header>\n\n<ion-content>\n<ion-grid fixed>\n  <ion-row>\n    <ion-col size=\"6\">\n      <ion-button (click)=\"onClickNewCustomer()\">\n        New Customer\n      </ion-button>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col size=\"6\">\n      <ion-button (click)=\"onClickExistingCustomer()\">\n        Existing Customer\n      </ion-button>\n    </ion-col>\n  </ion-row>\n\n\n\n  <ion-row>\n    <ion-col size=\"6\">\n      <ion-button (click)=\"onClickNewOrder()\">\n        New Order\n      </ion-button>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col size=\"6\">\n      <ion-button (click)=\"onClickOrderStatus()\">\n        Order Status\n      </ion-button>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col size=\"6\">\n      <ion-button (click)=\"onClickfinStatus()\">\n        Financial Status\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_options_options_module_ts.js.map