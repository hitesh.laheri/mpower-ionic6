"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_travel-plan-closure_travel-plan-closure_module_ts"],{

/***/ 28139:
/*!*******************************************************************!*\
  !*** ./src/app/travel-plan-closure/travel-plan-closure.module.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TravelPlanClosurePageModule": () => (/* binding */ TravelPlanClosurePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @swimlane/ngx-datatable */ 93143);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _travel_plan_closure_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./travel-plan-closure.page */ 8970);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ionic-selectable */ 25073);









const routes = [
    {
        path: '',
        component: _travel_plan_closure_page__WEBPACK_IMPORTED_MODULE_0__.TravelPlanClosurePage
    }
];
let TravelPlanClosurePageModule = class TravelPlanClosurePageModule {
};
TravelPlanClosurePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule,
            ionic_selectable__WEBPACK_IMPORTED_MODULE_6__.IonicSelectableModule,
            _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__.NgxDatatableModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterModule.forChild(routes)
        ],
        declarations: [_travel_plan_closure_page__WEBPACK_IMPORTED_MODULE_0__.TravelPlanClosurePage],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_2__.CUSTOM_ELEMENTS_SCHEMA]
    })
], TravelPlanClosurePageModule);



/***/ }),

/***/ 8970:
/*!*****************************************************************!*\
  !*** ./src/app/travel-plan-closure/travel-plan-closure.page.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TravelPlanClosurePage": () => (/* binding */ TravelPlanClosurePage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _travel_plan_closure_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./travel-plan-closure.page.html?ngResource */ 62832);
/* harmony import */ var _travel_plan_closure_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./travel-plan-closure.page.scss?ngResource */ 83504);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _travel_plan_closure_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./travel-plan-closure.service */ 47216);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _actual_travel_plan_actual_travel_plan_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../actual-travel-plan/actual-travel-plan.service */ 25461);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../provider/message-helper */ 98792);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! date-fns */ 86527);













/**
 * @see https://www.positronx.io/create-ionic-data-table-with-ngx-datatable/
 */

let TravelPlanClosurePage = class TravelPlanClosurePage {
  constructor(travelPlanClosureFormBuilder, travelPlanClosureService, http, loginauth, actualtravelplanservice, commonfun, msg) {
    this.travelPlanClosureFormBuilder = travelPlanClosureFormBuilder;
    this.travelPlanClosureService = travelPlanClosureService;
    this.http = http;
    this.loginauth = loginauth;
    this.actualtravelplanservice = actualtravelplanservice;
    this.commonfun = commonfun;
    this.msg = msg; //

    this.TAG = 'Travel Plan ClosurePage';
    this.sumtotalamount = 0;
    this.expenseList = [];
    this.datefrom = '';
    this.dateto = '';
    this.validation_messages = {
      'selectedplan': [{
        type: 'required',
        message: ' *Please Select Plan.'
      }],
      'remarkErrorMessage': [{
        type: 'required',
        message: ' *Please Enter Your Remark.'
      }]
    };
    this.travelPlanClosureForm = this.travelPlanClosureFormBuilder.group({
      salesperson: [],
      fromdate: [],
      todate: [],
      homelat: [],
      homelong: [],
      outstationChkCtrl: [],
      remarkCtrl: [],
      selectedplan: [, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required]
    });
  }

  ngOnInit() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      setTimeout(() => {
        _this.getsalesperson();
      }, 1500);
    })();
  }

  getsalesperson() {
    let methodTAG = 'getsalesperson';

    try {
      //localhost:8080/openbravo/ws/in.mbs.webservice.WMobileLatLongUpdate?addid=FFF202005200405006176B076E5C7E39&lat=100&long=2006
      this.commonfun.loadingPresent();
      this.actualtravelplanservice.getWMobileUserWisePlanData("Y").subscribe(data => {
        this.commonfun.loadingDismiss();
        var response = data; // console.log("nnnn",response[0].salesperson);

        this.salespersoninfo = response; //  console.log("this.salespersoninfo", this.salespersoninfo);

        this.travelPlanClosureForm.controls["salesperson"].setValue(this.salespersoninfo[0].salesperson);
        this.fromdate = this.salespersoninfo[0].fromdate;
        this.todate = this.salespersoninfo[0].todate;
        this.travelPlanClosureForm.controls["homelat"].setValue(this.salespersoninfo[0].latitude);
        this.travelPlanClosureForm.controls["homelong"].setValue(this.salespersoninfo[0].longitude); // this.first10leads=response.AddressList
      }, error => {
        //  console.log(this.TAG, methodTAG, error)
        this.commonfun.loadingDismiss();
        this.commonfun.presentAlert("Message", "Error", error.error);
      });
    } catch (error) {
      this.commonfun.loadingDismiss(); //  console.log(this.TAG, methodTAG, error)

      this.commonfun.presentAlert("Message", "Error", error.error);
    }
  }

  Dateconversion() {
    try {
      var dl1date = new Date(this.fromdate);
      var nmonth = dl1date.getMonth() + 1;
      var dd1 = dl1date.getDate() < 10 ? "0" + dl1date.getDate() : dl1date.getDate();
      var mm1 = nmonth < 10 ? "0" + nmonth : nmonth;
      var yyyy1 = dl1date.getFullYear();
      this.strfromdate = dd1 + "-" + mm1 + "-" + yyyy1;
      var dl2date = new Date(this.fromdate);
      var nmonth = dl2date.getMonth() + 1;
      var dd2 = dl2date.getDate() < 10 ? "0" + dl2date.getDate() : dl2date.getDate();
      var mm2 = nmonth < 10 ? "0" + nmonth : nmonth;
      var yyyy2 = dl2date.getFullYear();
      this.strtodate = dd2 + "-" + mm2 + "-" + yyyy2;
    } catch (error) {}
  }

  toggle(selectedcartproduct) {
    if (selectedcartproduct.show == false) {
      for (var i = 0; i < this.leads.length; i++) {
        if (this.leads[i].show === "true") {
          this.leads[i].show = "false";
        }
      }
    }

    selectedcartproduct.show = !selectedcartproduct.show;
  }

  formatDate(value) {
    this.ondatechange();
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_9__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_10__["default"])(value), 'dd.MM.yyyy');
  }

  formatDate1(value) {
    this.ondatechange();
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_9__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_10__["default"])(value), 'dd.MM.yyyy');
  }

  onChangeplan() {
    //  console.log("onChangeplan");
    //  console.log("selectedplan", this.travelPlanClosureForm.controls["selectedplan"].value);
    //this.travelPlanClosureForm.controls["selectedplan"].value;
    // var result = this.salespersoninfo.filter(item => item.mexp_visitplan_id == this.travelPlanClosureForm.controls["selectedplan"].value);
    //  console.log("selectedplan", result);
    //  console.log("selectedplan", result[0].AddressList);
    this.commonfun.loadingPresent();
    this.actualtravelplanservice.getWMobileUserWisePlanData("Y", this.travelPlanClosureForm.controls["selectedplan"].value).subscribe(data => {
      this.commonfun.loadingDismiss();
      var result = data; //console.log("PLAN", result);

      if (result.length === 0) {
        return;
      }

      this.leads = result[0].AddressList;
      this.expenseList = result[0].ExpenseList; // console.log("onChangeplan",result);

      if (result[0].outstation == 'Y') {
        this.outstation_chk_box = true;
      } else {
        this.outstation_chk_box = false;
      }

      this.travelPlanClosureForm.controls["fromdate"].setValue(this.dateyyyymmddT0000Z(result[0].fromdate));
      this.travelPlanClosureForm.controls["todate"].setValue(this.dateyyyymmddT0000Z(result[0].todate));
      const TotalAmount = this.expenseList.reduce((sum, item) => sum + (item.amount == "" ? 0 : Number(item.amount)), 0);
      this.sumtotalamount = TotalAmount; //-Number(this.formprod.controls["tobeRedeem"].value);
    });
  }

  dateyyyymmddT0000Z(dt) {
    try {
      var dl1date = new Date(dt.substr(0, 4), dt.substr(5, 2) - 1, dt.substr(8, 2));
      var nmonth = dl1date.getMonth() + 1;
      var dd1 = dl1date.getDate() < 10 ? "0" + dl1date.getDate() : dl1date.getDate();
      var mm1 = nmonth < 10 ? "0" + nmonth : nmonth;
      var yyyy1 = dl1date.getFullYear(); // this.strfromdate=dd1+"-"+mm1+"-"+yyyy1

      return yyyy1 + "-" + mm1 + "-" + dd1 + "T00:00Z";
    } catch (error) {}
  }

  hideshowsublead(selectedLead) {
    if (selectedLead.show == false) {
      for (var i = 0; i < this.leads.length; i++) {
        if (this.leads[i].show === "true") {
          this.leads[i].show = "false";
        }
      }
    }

    selectedLead.show = !selectedLead.show;
  }

  onAddLead(val) {}

  ondatechange() {
    try {//   var df=this.travelPlanClosureForm.controls["fromdate"].value;
      //   var dt=this.travelPlanClosureForm.controls["todate"].value;
      //   var dtr=this.travelPlanClosureForm.controls["traveldate"].value;
      // this.fromdate=new Date(this.dateyyyymmddT0000Z(df)).toISOString();
      // this.todate=new Date(this.dateyyyymmddT0000Z(dt)).toISOString();
      // this.traveldate=new Date(this.dateyyyymmddT0000Z(dtr)).toISOString();
      //     if(this.traveldate<this.fromdate || this.traveldate>this.todate){
      //       this.commonfun.presentAlert("Message", "Alert", "Travel Date must be in the range of from date and to date.");
      //       this.travelPlanClosureForm.controls["traveldate"].setValue(this.fromdate);
      //     }
    } catch (error) {//  console.log("ondatechange()-ERROR:", error);
    }
  }

  onSaveTravelPlanClosure(frnvala) {
    try {
      this.Dateconversion();

      if (this.expenseList.length == 0 && this.outstation_chk_box) {
        this.commonfun.presentAlert("Expense Closure", "Validation", "Please add expense.");
      } else {
        this.commonfun.loadingPresent();
        var jsondatatemp = {
          "salesperson": this.salespersoninfo[0].salesperson,
          "mexp_customervisit_id": this.salespersoninfo[0].salespersonid,
          "salespersonid": this.salespersoninfo[0].salespersonid,
          "addid": this.salespersoninfo[0].addid,
          "longitude": this.salespersoninfo[0].longitude,
          "mexp_visitplan_id": frnvala.selectedplan,
          "AddressList": this.leads,
          "latitude": this.salespersoninfo[0].latitude,
          "fromdate": this.strfromdate,
          "todate": this.strtodate,
          "iscomplete": "Y"
        };
        this.actualtravelplanservice.SaveActualPlan(jsondatatemp).subscribe(data => {
          //  console.log("data", data)
          if (data != null) {
            this.saveplanresponse = data;

            if (this.saveplanresponse.resposemsg == "Success") {
              this.commonfun.loadingDismiss();
              this.commonfun.presentAlert("Message", this.saveplanresponse.resposemsg, this.saveplanresponse.logmsg);
              this.Resetpage();
            } else {
              this.commonfun.loadingDismiss(); //  this.Resetpage();

              this.commonfun.presentAlert("Message", this.saveplanresponse.resposemsg, this.saveplanresponse.logmsg);
            }
          }
        }, error => {
          this.commonfun.loadingDismiss(); //  console.log("error", error);

          this.commonfun.presentAlert("Message", "Error!", error);
        });
      }
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error!", error.text);
    }
  }

  Resetpage() {
    try {
      this.sumtotalamount = 0;
      this.traveldate = null;
      this.travelPlanClosureForm.reset();
      this.leads = null;
      this.expenseList = null;
      setTimeout(() => {
        this.getsalesperson();
      }, 1500);
    } catch (error) {}
  }

  ionViewWillEnter() {}

  onPlanDropDownSearch() {
    let methodTAG = 'onPlanDropDownSearch';

    try {} catch (error) {}
  }

  onPlanDropDownChange() {
    let methodTAG = 'onPlanDropDownChange';

    try {} catch (error) {}
  }

  onFromDateChange() {
    let methodTAG = "onFromDateChange";

    try {} catch (error) {}
  }

  onToDateChange() {
    let methodTAG = 'onToDateChange';

    try {} catch (error) {}
  }

  onPlanStatusChange() {
    let methodTAG = 'onPlanStatusChange';

    try {} catch (error) {}
  }

  onPage() {
    let methodTAG = 'onPage';

    try {} catch (error) {}
  } //By Nilesh


  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {}

};

TravelPlanClosurePage.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_8__.FormBuilder
}, {
  type: _travel_plan_closure_service__WEBPACK_IMPORTED_MODULE_3__.TravelPlanClosureService
}, {
  type: _angular_common_http__WEBPACK_IMPORTED_MODULE_11__.HttpClient
}, {
  type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_5__.LoginauthService
}, {
  type: _actual_travel_plan_actual_travel_plan_service__WEBPACK_IMPORTED_MODULE_4__.ActualTravelPlanService
}, {
  type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_6__.Commonfun
}, {
  type: _provider_message_helper__WEBPACK_IMPORTED_MODULE_7__.Message
}];

TravelPlanClosurePage.propDecorators = {
  table: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_12__.ViewChild,
    args: ['myTable1', {
      static: true
    }]
  }]
};
TravelPlanClosurePage = (0,tslib__WEBPACK_IMPORTED_MODULE_13__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
  selector: 'app-travel-plan-closure',
  template: _travel_plan_closure_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_12__.ViewEncapsulation.None,
  styles: [_travel_plan_closure_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], TravelPlanClosurePage);


/***/ }),

/***/ 47216:
/*!********************************************************************!*\
  !*** ./src/app/travel-plan-closure/travel-plan-closure.service.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TravelPlanClosureService": () => (/* binding */ TravelPlanClosureService)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ 58987);




let TravelPlanClosureService = class TravelPlanClosureService {
  constructor(http) {
    this.http = http;
    this.TAG = 'TravelPlanClosureService';
  }

  getPlan(id) {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      let methodTAG = 'getPlan';

      try {
        return yield _this.http.get('assets/data/planMaster.json');
      } catch (error) {}
    })();
  }

  getExpenseMasterList() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      let methodTAG = 'getExpenseMasterList';

      try {
        return yield _this2.http.get('assets/data/expenseMasterList.json');
      } catch (error) {}
    })();
  }

  getCompletedTaskList() {
    let methodTAG = 'getCompletedTaskList';

    try {
      return this.http.get('assets/data/Expense.json');
    } catch (error) {}
  }

};

TravelPlanClosureService.ctorParameters = () => [{
  type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpClient
}];

TravelPlanClosureService = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
  providedIn: 'root'
})], TravelPlanClosureService);


/***/ }),

/***/ 83504:
/*!******************************************************************************!*\
  !*** ./src/app/travel-plan-closure/travel-plan-closure.page.scss?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

module.exports = ".scroll-grid {\n  overflow-x: scroll !important;\n}\n\n.custom-ion-row {\n  display: table !important;\n  white-space: nowrap !important;\n}\n\nion-scroll[scrollX] {\n  white-space: nowrap;\n  overflow: visible;\n  overflow-y: auto;\n}\n\nion-scroll[scrollX] .scroll-item {\n  display: inline-block;\n}\n\nion-scroll[scrollX] .selectable-icon {\n  margin: 10px 20px 10px 20px;\n  color: red;\n  font-size: 100px;\n}\n\nion-scroll[scrollX] ion-avatar img {\n  margin: 10px;\n}\n\nion-scroll[scroll-avatar] {\n  height: 60px;\n}\n\n/* Hide ion-content scrollbar */\n\n::-webkit-scrollbar {\n  display: none;\n}\n\nh5 {\n  font-style: oblique;\n  color: darkcyan;\n  font-size: large !important;\n}\n\nh5 ion-icon {\n  color: lightcoral;\n}\n\n.forecast_container {\n  overflow-x: scroll !important;\n  overflow-x: visible !important;\n  overflow-y: hidden;\n  word-wrap: break-word;\n  height: 20vw;\n  font-size: 0.8em;\n  font-weight: 300;\n}\n\n.forecast_div {\n  overflow-x: scroll !important;\n  overflow-x: visible !important;\n  overflow-y: hidden;\n  word-wrap: break-word;\n  font-size: 0.8em;\n  font-weight: 300;\n  max-width: 175px;\n}\n\n.grid-header {\n  font-weight: bold;\n  max-width: 175px;\n}\n\nion-popover {\n  --width: 320px;\n}\n\nion-popover.dateTimePopover {\n  --offset-y: -350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRyYXZlbC1wbGFuLWNsb3N1cmUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksNkJBQUE7QUFDSjs7QUFDQTtFQUVJLHlCQUFBO0VBQ0EsOEJBQUE7QUFDSjs7QUFFQTtFQUNJLG1CQUFBO0VBRUEsaUJBQUE7RUFDQSxnQkFBQTtBQUFKOztBQUVJO0VBQ0UscUJBQUE7QUFBTjs7QUFHSTtFQUNFLDJCQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0FBRE47O0FBSUk7RUFDRSxZQUFBO0FBRk47O0FBTUU7RUFDRSxZQUFBO0FBSEo7O0FBTUUsK0JBQUE7O0FBQ0E7RUFDRSxhQUFBO0FBSEo7O0FBTUU7RUFDRSxtQkFBQTtFQUNBLGVBQUE7RUFDQSwyQkFBQTtBQUhKOztBQUlJO0VBQ0UsaUJBQUE7QUFGTjs7QUFPRTtFQUNFLDZCQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUFKSjs7QUFNQTtFQUNFLDZCQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FBSEY7O0FBS0E7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FBRkE7O0FBSUE7RUFDRSxjQUFBO0FBREY7O0FBR0E7RUFDRSw2QkFBQTtBQUFGIiwiZmlsZSI6InRyYXZlbC1wbGFuLWNsb3N1cmUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNjcm9sbC1ncmlke1xuICAgIG92ZXJmbG93LXg6IHNjcm9sbCAhaW1wb3J0YW50O1xufVxuLmN1c3RvbS1pb24tcm93XG57XG4gICAgZGlzcGxheTogdGFibGUgIWltcG9ydGFudDsgICAgICAgXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcCAhaW1wb3J0YW50O1xufVxuXG5pb24tc2Nyb2xsW3Njcm9sbFhdIHtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgLy8gaGVpZ2h0OiAxMjBweDtcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xuLy8gd2lkdGg6MTAwJTtcbiAgICAuc2Nyb2xsLWl0ZW0ge1xuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIH1cblxuICAgIC5zZWxlY3RhYmxlLWljb257XG4gICAgICBtYXJnaW46IDEwcHggMjBweCAxMHB4IDIwcHg7XG4gICAgICBjb2xvcjogcmVkO1xuICAgICAgZm9udC1zaXplOiAxMDBweDtcbiAgICB9XG5cbiAgICBpb24tYXZhdGFyIGltZ3tcbiAgICAgIG1hcmdpbjogMTBweDtcbiAgICB9XG4gIH1cblxuICBpb24tc2Nyb2xsW3Njcm9sbC1hdmF0YXJde1xuICAgIGhlaWdodDogNjBweDtcbiAgfVxuXG4gIC8qIEhpZGUgaW9uLWNvbnRlbnQgc2Nyb2xsYmFyICovXG4gIDo6LXdlYmtpdC1zY3JvbGxiYXJ7XG4gICAgZGlzcGxheTpub25lO1xuICB9XG5cbiAgaDV7XG4gICAgZm9udC1zdHlsZTogb2JsaXF1ZTtcbiAgICBjb2xvcjogZGFya2N5YW47XG4gICAgZm9udC1zaXplOiBsYXJnZSFpbXBvcnRhbnQ7XG4gICAgaW9uLWljb257XG4gICAgICBjb2xvcjogbGlnaHRjb3JhbDtcbiAgICB9XG5cbiAgfVxuICBcbiAgLmZvcmVjYXN0X2NvbnRhaW5lcntcbiAgICBvdmVyZmxvdy14OiBzY3JvbGwhaW1wb3J0YW50O1xuICAgIG92ZXJmbG93LXg6IHZpc2libGUhaW1wb3J0YW50O1xuICAgIG92ZXJmbG93LXk6IGhpZGRlbjtcbiAgICB3b3JkLXdyYXA6IGJyZWFrLXdvcmQ7XG4gICAgaGVpZ2h0OjIwdnc7XG4gICAgZm9udC1zaXplOjAuOGVtO1xuICAgIGZvbnQtd2VpZ2h0OjMwMDtcbn1cbi5mb3JlY2FzdF9kaXZ7XG4gIG92ZXJmbG93LXg6IHNjcm9sbCFpbXBvcnRhbnQ7XG4gIG92ZXJmbG93LXg6IHZpc2libGUhaW1wb3J0YW50O1xuICBvdmVyZmxvdy15OiBoaWRkZW47XG4gIHdvcmQtd3JhcDogYnJlYWstd29yZDtcbiAgZm9udC1zaXplOjAuOGVtO1xuICBmb250LXdlaWdodDozMDA7XG4gIG1heC13aWR0aDogMTc1cHg7XG59XG4uZ3JpZC1oZWFkZXJ7XG5mb250LXdlaWdodDogYm9sZDtcbm1heC13aWR0aDogMTc1cHg7XG59XG5pb24tcG9wb3ZlciB7XG4gIC0td2lkdGg6IDMyMHB4O1xufVxuaW9uLXBvcG92ZXIuZGF0ZVRpbWVQb3BvdmVyIHtcbiAgLS1vZmZzZXQteTogLTM1MHB4ICFpbXBvcnRhbnQ7XG4gIH0iXX0= */";

/***/ }),

/***/ 62832:
/*!******************************************************************************!*\
  !*** ./src/app/travel-plan-closure/travel-plan-closure.page.html?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Expense Closure</ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <form [formGroup]=\"travelPlanClosureForm\" (ngSubmit)=\"onSaveTravelPlanClosure(travelPlanClosureForm.value)\">\n          <ion-card>\n            <ion-card-content>\n              <ion-row>\n                <ion-col>\n                  <h5 ion-text class=\"text-primary\">\n                    <ion-icon name=\"person\"></ion-icon> Plan :\n                  </h5>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col>\n                  <ion-item>\n                    <ion-label position=\"floating\">Sales Person</ion-label>\n                    <ion-input type=\"text\" formControlName=\"salesperson\"\n                      [disabled]=\"true\"></ion-input>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n            <ion-row>\n                <ion-col>\n                  <ion-item>\n                    <ion-label position=\"stacked\">Plan<span style=\"color:red!important\">*</span></ion-label>\n                    <ion-select #C formControlName=\"selectedplan\" interface=\"popover\"\n                      (ionChange)=\"onChangeplan()\" multiple=\"false\" placeholder=\"Select Plan\">\n                      <ion-select-option *ngFor=\"let plan of salespersoninfo\" [value]=\"plan.mexp_visitplan_id\">\n                        {{plan.plan}}</ion-select-option>\n                    </ion-select>\n                  </ion-item>\n                  <div padding-left>\n                    <ng-container *ngFor=\"let validation of validation_messages.selectedplan\">\n                      <div\n                        *ngIf=\"travelPlanClosureForm.get('selectedplan').hasError(validation.type) && travelPlanClosureForm.get('selectedplan').touched\">\n                        <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                      </div>\n                    </ng-container>\n                  </div>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col>\n                  <!-- <ion-item>\n                    <ion-label>From Date</ion-label>\n                    <ion-datetime placeholder=\"Select Date\" [disabled]=\"true\"\n                      formControlName=\"travelPlanClosureForm.controls['fromdate']\" displayFormat=\"DD.MM.YYYY\" max=\"2050\">\n                    </ion-datetime>\n                  </ion-item>  -->\n                  <ion-item [disabled]=\"true\">\n                    <ion-label position=\"stacked\">From Date</ion-label>\n                    <ion-item>\n                      <ion-input placeholder=\"Select Date\" [value]=\"datefrom\"></ion-input>\n                      <ion-button fill=\"clear\" id=\"open-date-input-1\">\n                        <ion-icon icon=\"calendar\"></ion-icon>\n                      </ion-button>\n                      <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\n                        <ng-template>\n                          <ion-datetime\n                            #popoverDatetime2\n                            presentation=\"date\"\n                            showDefaultButtons=\"true\"\n                            max=\"2050\"\n                            formControlName=\"fromdate\" \n                            (ionChange)=\"datefrom = formatDate(popoverDatetime2.value)\"\n                          ></ion-datetime>\n                        </ng-template>\n                      </ion-popover>\n                    </ion-item>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col>\n                  <!-- <ion-item>\n                    <ion-label>To Date</ion-label>\n                    <ion-datetime placeholder=\"Select Date\" [disabled]=\"true\"\n                      formControlName=\"travelPlanClosureForm.controls['todate']\" displayFormat=\"DD.MM.YYYY\" max=\"2050\">\n                    </ion-datetime>\n                  </ion-item> -->\n                  <ion-item [disabled]=\"true\">\n                    <ion-label position=\"stacked\">To Date</ion-label>\n                    <ion-item>\n                      <ion-input placeholder=\"Select Date\" [value]=\"dateto\"></ion-input>\n                      <ion-button fill=\"clear\" id=\"open-date-input-2\">\n                        <ion-icon icon=\"calendar\"></ion-icon>\n                      </ion-button>\n                      <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-2\" show-backdrop=\"false\">\n                        <ng-template>\n                          <ion-datetime\n                            #popoverDatetime2\n                            presentation=\"date\"\n                            showDefaultButtons=\"true\"\n                            max=\"2050\"\n                            formControlName=\"todate\" \n                            (ionChange)=\"dateto = formatDate1(popoverDatetime2.value)\"\n                          ></ion-datetime>\n                        </ng-template>\n                      </ion-popover>\n                    </ion-item>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col>\n                  <ion-item>\n                    <ion-label position=\"floating\">Home Lat</ion-label>\n                    <ion-input type=\"text\" formControlName=\"homelat\" [disabled]=\"true\">\n                    </ion-input>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col>\n                  <ion-item>\n                    <ion-label position=\"floating\">Home Long</ion-label>\n                    <ion-input type=\"text\" formControlName=\"homelong\" [disabled]=\"true\">\n                    </ion-input>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col>\n                  <ion-item>\n                    <ion-label>Outstation?</ion-label>\n                    <ion-checkbox slot=\"end\" [disabled]=\"true\" [checked]=\"outstation_chk_box\"\n                      formControlName=\"outstationChkCtrl\"></ion-checkbox>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n            </ion-card-content>\n          </ion-card>\n          <!-- Customer Detail Card -->\n          <ion-card>\n            <ion-card-content>\n              <ion-row>\n                <ion-col>\n                  <h5 ion-text class=\"text-primary\">\n                    <ion-icon name=\"bookmarks\"></ion-icon> Customer:\n                  </h5>\n                </ion-col>\n                <ion-col>\n                  <div style=\"display: none;\">\n                    <ion-fab-button size=\"small\" float-right (click)=\"onAddLead(null)\">\n                      <ion-icon name=\"add\"></ion-icon>\n                    </ion-fab-button>\n                  </div>\n                </ion-col>\n              </ion-row>\n              <div style=\"overflow-x:auto\">\n                <ion-grid>\n                  <ion-row nowrap>\n                    <ion-col nowrap>\n                      <ion-row nowrap>\n                        <ion-col col-3 size=\"4\" class=\"grid-header\">Line Number</ion-col>\n                        <ion-col size=\"8\" class=\"grid-header\">Customer</ion-col>\n                        <ion-col size=\"8\" class=\"grid-header\">Address</ion-col>\n                        <ion-col size=\"4\" class=\"grid-header\">Day</ion-col>\n                        <ion-col size=\"6\" class=\"grid-header\">Date</ion-col>\n                        <!-- <ion-col size=\"5\" class=\"grid-header\">Address Lat</ion-col> -->\n                        <!-- <ion-col size=\"5\" class=\"grid-header\">Address Long</ion-col> -->\n                        <ion-col size=\"2\" class=\"grid-header\">KM</ion-col>\n                        <ion-col size=\"4\" class=\"grid-header\">Status</ion-col>\n                      </ion-row>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row *ngFor=\"let data of leads; index as i\" nowrap>\n                    <ion-col nowrap>\n                      <ion-row nowrap (click)=\"hideshowsublead(data)\">\n                        <ion-col size=\"1\" style=\"width: 100%; text-align: right;\">\n                          <ion-icon ios=\"ios-checkmark-circle\" md=\"checkmark-circle\" style=\"font-size: x-large;\">\n                          </ion-icon>\n                        </ion-col>\n                        <ion-col size=\"1\" style=\"width: 100%; text-align: right;\">\n\n                        </ion-col>\n                        <ion-col size=\"2\" class=\"forecast_div\">{{data.line}}</ion-col>\n                        <ion-col size=\"8\" class=\"forecast_div\">{{data.custname}}</ion-col>\n                        <ion-col size=\"8\" class=\"forecast_div\">{{data.addressname}}</ion-col>\n                        <ion-col size=\"4\" class=\"forecast_div\">{{data.visit_day}}</ion-col>\n                        <ion-col size=\"6\" class=\"forecast_div\">{{data.visit_date}}</ion-col>\n                        <!-- <ion-col size=\"5\" class=\"forecast_div\">{{data.latitude}}</ion-col> -->\n                        <!-- <ion-col size=\"5\" class=\"forecast_div\">{{data.longitude}}</ion-col> -->\n                        <ion-col size=\"2\" class=\"forecast_div\">{{data.km}} km</ion-col>\n                        <ion-col size=\"4\" class=\"forecast_div\">{{data.status}}</ion-col>\n\n                        <!-- <ion-col size=\"3\" class=\"forecast_div\">{{data.status}}</ion-col> -->\n\n                      </ion-row>\n                      <div *ngIf=\"data.show\">\n                        <ion-row nowrap>\n                          <ion-col nowrap>\n                            <ion-row nowrap>\n                              <ion-col size=\"4\" offsetLg=\"2\"  class=\"grid-header\">Task</ion-col>\n                              <ion-col size=\"2\" class=\"grid-header\">Done?</ion-col>\n                            </ion-row>\n                            <ion-row *ngFor=\"let task of data.TaskList; index as i\" nowrap>\n                              <ion-col size=\"4\"  offsetLg=\"2\" class=\"forecast_div\">{{task.task}}</ion-col>\n                              <ion-col size=\"2\" class=\"forecast_div\">\n                               <ion-checkbox [disabled]=\"true\" [ngModelOptions]=\"{standalone: true}\"\n                                  [(ngModel)]=\"task.Done\"></ion-checkbox>\n                              </ion-col>\n                              <!-- <ion-col size=\"2\" class=\"forecast_div\" *ngIf=\"task.info_required!='N'\" >\n                                <ion-textarea type=\"text\" placeholder=\"Remark\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"task.remark\"></ion-textarea>\n                              </ion-col> -->\n                            </ion-row>\n                          </ion-col>\n                        </ion-row>\n                      </div>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n              </div>\n            </ion-card-content>\n          </ion-card>\n          <ion-card>\n            <ion-card-content>\n              <ion-row>\n                <ion-col>\n                  <h5 ion-text class=\"text-primary\">\n                    <ion-icon name=\"wifi\"></ion-icon> Expense Detail\n                  </h5>\n                </ion-col>\n              </ion-row>\n              <div style=\"overflow-x:auto\">\n                <ion-grid>\n                  <ion-row nowrap>\n                    <ion-col nowrap>\n                      <ion-row nowrap>\n                        <ion-col size=\"6\" class=\"grid-header\">From</ion-col>\n                        <ion-col size=\"6\" class=\"grid-header\">To</ion-col>\n                        <ion-col size=\"6\" class=\"grid-header\">Travel Expense</ion-col>\n                        <ion-col size=\"6\" class=\"grid-header\">Amount</ion-col>\n                      </ion-row>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row *ngFor=\"let data of expenseList; index as i\" nowrap>\n                    <ion-col nowrap>\n                      <ion-row nowrap>\n                        <ion-col size=\"6\" class=\"forecast_div\">{{data.fromdate}}</ion-col>\n                        <ion-col size=\"6\" class=\"forecast_div\">{{data.todate}}</ion-col>\n                        <ion-col size=\"6\" class=\"forecast_div\">{{data.travel_expence}}</ion-col>\n                        <ion-col size=\"6\" class=\"forecast_div\">{{data.amount}}</ion-col>\n                      </ion-row>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row nowrap>\n                    <ion-col nowrap>\n                      <ion-row nowrap>\n                        <ion-col size=\"6\" class=\"grid-header\"></ion-col>\n                        <ion-col size=\"6\" class=\"grid-header\"></ion-col>\n                        <ion-col size=\"6\" class=\"grid-header\"></ion-col>\n                        <ion-col size=\"6\" class=\"grid-header\">{{sumtotalamount}}</ion-col>\n                      </ion-row>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n              </div>\n            </ion-card-content>\n          </ion-card>\n          <ion-item float-right lines=\"none\">\n            <ion-button color=\"primary\" text-center (click)=\"onSaveTravelPlanClosure(travelPlanClosureForm.value)\">Close\n              Plan</ion-button>\n          </ion-item>\n        </form>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_travel-plan-closure_travel-plan-closure_module_ts.js.map