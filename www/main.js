(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["main"],{

/***/ 53899:
/*!**********************************************************!*\
  !*** ./src/app/addeditproduct/addeditproduct.service.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddeditproductService": () => (/* binding */ AddeditproductService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _common_Constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/Constants */ 68209);







let AddeditproductService = class AddeditproductService {
    constructor(http, loginauth, genericHTTP, platform) {
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
        this.platform = platform;
        this.TAG = "Add Edit Product Service";
    }
    getproduct(searchtext, selectedactivity, Isdruglicence) {
        if (Isdruglicence == true) {
            return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/Product?'
                + '_where=active=true%20and%20mmstOrgAct=\'' + selectedactivity + '\' and (lower(translate(sku,\' \',\'\')) LIKE lower(\'%25' + searchtext + '%25\') or lower(translate(description,\' \',\'\')) LIKE lower(\'%25' + searchtext + '%25\'))');
        }
        else {
            return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/Product?'
                + '_selectedProperties=id,_identifier,description&'
                + '_where=mmstOrgAct=\'' + selectedactivity + '\' and (lower(translate(sku,\' \',\'\')) LIKE lower(\'%25' + searchtext + '%25\') or lower(translate(description,\' \',\'\')) LIKE lower(\'%25' + searchtext + '%25\'))');
        }
    }
    getproductapi(bpc_id, searchchar, billing, shipping, ordertypeionic) {
        // searchchar="w";
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileCustComplnsWiseProduct?'
            + 'bpc_id=' + bpc_id
            + '&billing=' + billing
            + '&shipping=' + shipping
            + '&searchchar=' + searchchar
            + '&ordertypeionic=' + ordertypeionic);
    }
    getproductdetail(activityid, cust_id, product_id, qty, ordertemp_id, order_type, shippadd, billadd, special_order, is_advance_payment, enteredfreeqty) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileSchemeData?'
            + 'activityid=' + activityid
            + '&cust_id=' + cust_id
            + '&product_id=' + product_id
            + '&qty=' + qty
            + '&ordertemp_id=' + ordertemp_id
            + '&order_type=' + order_type
            + '&shippadd=' + shippadd
            + '&billadd=' + billadd
            + '&special_order=' + special_order
            + '&is_advance_payment=' + is_advance_payment
            + '&enteredfreeqty=' + enteredfreeqty);
    }
    getFilterProductService(bpc_id, searchchar, billing, shipping, ordertypeionic, filter) {
        try {
            let login = this.loginauth.user;
            let password = this.loginauth.pass;
            const auth = btoa(login + ":" + password);
            const httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpHeaders({
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Basic ' + auth
                })
            };
            let temp = {
                "bpc_id": bpc_id,
                "searchchar": searchchar,
                "billing": billing,
                "shipping": shipping,
                "ordertypeionic": ordertypeionic
            };
            let data = [temp];
            if (!!filter) {
                filter.forEach(element => {
                    data.push(element);
                });
            }
            // console.log(this.TAG,"Get Filter Product Service",data);
            let filter_product_url = _common_Constants__WEBPACK_IMPORTED_MODULE_2__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.WMobileCustComplnsWiseProduct?'
                + '&bpc_id=' + bpc_id
                + '&billing=' + billing
                + '&shipping=' + shipping
                + '&searchchar=' + searchchar
                + '&ordertypeionic=' + ordertypeionic;
            // console.log(this.TAG,"Get Filter Product Service URL",filter_product_url);
            // return  this.genericHTTP.post(filter_product_url,data,httpOptions);
            if (this.platform.is('android')) {
                return this.http.post(filter_product_url, data, httpOptions);
            }
            else {
                return this.genericHTTP.post(filter_product_url, data, httpOptions);
            }
        }
        catch (error) {
            console.log(error);
        }
    }
};
AddeditproductService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.Platform }
];
AddeditproductService = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Injectable)({
        providedIn: 'root'
    })
], AddeditproductService);



/***/ }),

/***/ 90158:
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRoutingModule": () => (/* binding */ AppRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 60124);



const routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    //  { path: '', redirectTo: 'order-approval', pathMatch: 'full' },
    { path: 'login', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_login_login_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./login/login.module */ 80107)).then(m => m.LoginPageModule) },
    { path: 'profile', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_profile_profile_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./profile/profile.module */ 84523)).then(m => m.ProfilePageModule) },
    { path: 'bom', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_bom_bom_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./bom/bom.module */ 12856)).then(m => m.BomPageModule) },
    { path: 'readertest', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_readertest_readertest_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./readertest/readertest.module */ 79241)).then(m => m.ReadertestPageModule) },
    { path: 'joblist', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_joblist_joblist_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./joblist/joblist.module */ 11013)).then(m => m.JoblistPageModule) },
    { path: 'job/:jobid', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_job_job_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./job/job.module */ 34298)).then(m => m.JobPageModule) },
    { path: 'jobdetails/:jobid', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_jobdetails_jobdetails_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./jobdetails/jobdetails.module */ 14451)).then(m => m.JobdetailsPageModule) },
    { path: 'jobtransfer/:jobid', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_jobtransfer_jobtransfer_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./jobtransfer/jobtransfer.module */ 69249)).then(m => m.JobtransferPageModule) },
    { path: 'options', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_options_options_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./options/options.module */ 76305)).then(m => m.OptionsPageModule) },
    { path: 'newcustomer', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng2-file-upload___ivy_ngcc___fesm2015_ng2-file-upload_js"), __webpack_require__.e("default-src_app_components_components_module_ts"), __webpack_require__.e("common"), __webpack_require__.e("src_app_newcustomer_newcustomer_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./newcustomer/newcustomer.module */ 74653)).then(m => m.NewcustomerPageModule) },
    { path: 'productsearch', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_productsearch_productsearch_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./productsearch/productsearch.module */ 32170)).then(m => m.ProductsearchPageModule) },
    { path: 'existingcustomer', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_existingcustomer_existingcustomer_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./existingcustomer/existingcustomer.module */ 22903)).then(m => m.ExistingcustomerPageModule) },
    { path: 'neworder', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_neworder_neworder_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./neworder/neworder.module */ 19195)).then(m => m.NeworderPageModule) },
    { path: 'addeditproduct', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_product-filter_product-filter_page_ts"), __webpack_require__.e("common"), __webpack_require__.e("src_app_addeditproduct_addeditproduct_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./addeditproduct/addeditproduct.module */ 33276)).then(m => m.AddeditproductPageModule) },
    { path: 'home', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_home_home_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./home/home.module */ 3467)).then(m => m.HomePageModule) },
    { path: 'existingorder', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_existingorder_existingorder_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./existingorder/existingorder.module */ 29767)).then(m => m.ExistingorderPageModule) },
    { path: 'business-partner-address', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_business-partner-address_business-partner-address_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./business-partner-address/business-partner-address.module */ 96866)).then(m => m.BusinessPartnerAddressPageModule) },
    { path: 'addedit-business-partner-address', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_addedit-business-partner-address_addedit-business-partner-address_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./addedit-business-partner-address/addedit-business-partner-address.module */ 38978)).then(m => m.AddeditBusinessPartnerAddressPageModule) },
    { path: 'login-option', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_login-option_login-option_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./login-option/login-option.module */ 58845)).then(m => m.LoginOptionPageModule) },
    { path: 'use-vetcoins', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_use-vetcoins_use-vetcoins_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./use-vetcoins/use-vetcoins.module */ 87856)).then(m => m.UseVetcoinsPageModule) },
    { path: 'use-vetcoins-balance', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_use-vetcoins-balance_use-vetcoins-balance_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./use-vetcoins-balance/use-vetcoins-balance.module */ 84203)).then(m => m.UseVetcoinsBalancePageModule) },
    { path: 'use-vetcoins-redemption', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_use-vetcoins-redemption_use-vetcoins-redemption_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./use-vetcoins-redemption/use-vetcoins-redemption.module */ 25704)).then(m => m.UseVetcoinsRedemptionPageModule) },
    { path: 'about', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_about_about_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./about/about.module */ 46985)).then(m => m.AboutPageModule) },
    { path: 'use-vetcoins-transaction', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_use-vetcoins-transaction_use-vetcoins-transaction_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./use-vetcoins-transaction/use-vetcoins-transaction.module */ 16329)).then(m => m.UseVetcoinsTransactionPageModule) },
    { path: 'order-status', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_order-status_order-status_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./order-status/order-status.module */ 73758)).then(m => m.OrderStatusPageModule) },
    { path: 'settings', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_settings_settings_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./settings/settings.module */ 27075)).then(m => m.SettingsPageModule) },
    { path: 'location-finder', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_location-finder_location-finder_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./location-finder/location-finder.module */ 86618)).then(m => m.LocationFinderPageModule) },
    // { path: 'settings', loadChildren: './hrms/mpr-list/mpr-list.module#MprListPageModule' },
    { path: 'travel-expense', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_travel-expense_travel-expense_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./travel-expense/travel-expense.module */ 70115)).then(m => m.TravelExpensePageModule) },
    { path: 'travel-plan-closure', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_swimlane_ngx-datatable___ivy_ngcc___fesm2015_swimlane-ngx-datatable_js"), __webpack_require__.e("common"), __webpack_require__.e("src_app_travel-plan-closure_travel-plan-closure_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./travel-plan-closure/travel-plan-closure.module */ 28139)).then(m => m.TravelPlanClosurePageModule) },
    { path: 'travel-plan', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_travel-plan_travel-plan_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./travel-plan/travel-plan.module */ 55592)).then(m => m.TravelPlanPageModule) },
    { path: 'addedit-travel-plan', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_addedit-travel-plan_addedit-travel-plan_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./addedit-travel-plan/addedit-travel-plan.module */ 95918)).then(m => m.AddeditTravelPlanPageModule) },
    { path: 'lat-long-finder', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_lat-long-finder_lat-long-finder_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./lat-long-finder/lat-long-finder.module */ 54586)).then(m => m.LatLongFinderPageModule) },
    { path: 'actual-travel-plan', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_actual-travel-plan_actual-travel-plan_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./actual-travel-plan/actual-travel-plan.module */ 66045)).then(m => m.ActualTravelPlanPageModule) },
    { path: 'addedit-actual-travel-plan', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_addedit-actual-travel-plan_addedit-actual-travel-plan_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./addedit-actual-travel-plan/addedit-actual-travel-plan.module */ 75313)).then(m => m.AddeditActualTravelPlanPageModule) },
    { path: 'terms-of-agreement', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_terms-of-agreement_terms-of-agreement_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./terms-of-agreement/terms-of-agreement.module */ 35871)).then(m => m.TermsOfAgreementPageModule) },
    { path: 'addedit-travel-expense', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_addedit-travel-expense_addedit-travel-expense_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./addedit-travel-expense/addedit-travel-expense.module */ 97540)).then(m => m.AddeditTravelExpensePageModule) },
    { path: 'order-approval', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_swimlane_ngx-datatable___ivy_ngcc___fesm2015_swimlane-ngx-datatable_js"), __webpack_require__.e("common"), __webpack_require__.e("src_app_order-approval_order-approval_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./order-approval/order-approval.module */ 49026)).then(m => m.OrderApprovalPageModule) },
    { path: 'filter', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_filter_filter_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./filter/filter.module */ 67655)).then(m => m.FilterPageModule) },
    { path: 'sort', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_swimlane_ngx-datatable___ivy_ngcc___fesm2015_swimlane-ngx-datatable_js"), __webpack_require__.e("common"), __webpack_require__.e("src_app_sort_sort_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./sort/sort.module */ 68387)).then(m => m.SortPageModule) },
    { path: 'show-approval-details-modal', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_swimlane_ngx-datatable___ivy_ngcc___fesm2015_swimlane-ngx-datatable_js"), __webpack_require__.e("common"), __webpack_require__.e("src_app_order-approval_show-approval-details-modal_show-approval-details-modal_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./order-approval/show-approval-details-modal/show-approval-details-modal.module */ 14756)).then(m => m.ShowApprovalDetailsModalPageModule) },
    { path: 'scheme-information', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_scheme-information_scheme-information_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./scheme-information/scheme-information.module */ 79590)).then(m => m.SchemeInformationPageModule) },
    { path: 'customer-service', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_angular_material_fesm2020_datepicker_mjs"), __webpack_require__.e("common"), __webpack_require__.e("src_app_cardinal_customer-service_customer-service_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./cardinal/customer-service/customer-service.module */ 39925)).then(m => m.CustomerServicePageModule) },
    { path: 'service-manager', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_cardinal_service-manager_service-manager_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./cardinal/service-manager/service-manager.module */ 93550)).then(m => m.ServiceManagerPageModule) },
    { path: 'vender-approval', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_cardinal_vender-approval_vender-approval_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./cardinal/vender-approval/vender-approval.module */ 68577)).then(m => m.VenderApprovalPageModule) },
    { path: 'service-engineer-visit', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_cardinal_service-engineer-visit_service-engineer-visit_service_ts"), __webpack_require__.e("src_app_cardinal_service-engineer-visit_service-engineer-visit_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./cardinal/service-engineer-visit/service-engineer-visit.module */ 2035)).then(m => m.ServiceEngineerVisitPageModule) },
    { path: 'service-manager-details', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_cardinal_customer-service_customer-service_service_ts-src_assets_model_compla-f181bf"), __webpack_require__.e("common"), __webpack_require__.e("src_app_cardinal_service-manager_service-manager-details_service-manager-details_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./cardinal/service-manager/service-manager-details/service-manager-details.module */ 49990)).then(m => m.ServiceManagerDetailsPageModule) },
    { path: 'vender-approval-details', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_cardinal_customer-service_customer-service_service_ts-src_assets_model_compla-f181bf"), __webpack_require__.e("common"), __webpack_require__.e("src_app_cardinal_vender-approval_vender-approval-details_vender-approval-details_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./cardinal/vender-approval/vender-approval-details/vender-approval-details.module */ 28684)).then(m => m.VenderApprovalDetailsPageModule) },
    { path: 'service-engineer-visit-detail', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_cardinal_customer-service_customer-service_service_ts-src_assets_model_compla-f181bf"), __webpack_require__.e("default-src_app_cardinal_service-engineer-visit_service-engineer-visit_service_ts"), __webpack_require__.e("src_app_cardinal_service-engineer-visit_service-engineer-visit-detail_service-engineer-visit--bb3b16")]).then(__webpack_require__.bind(__webpack_require__, /*! ./cardinal/service-engineer-visit/service-engineer-visit-detail/service-engineer-visit-detail.module */ 99394)).then(m => m.ServiceEngineerVisitDetailPageModule) },
    { path: 'report/:rptid', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_swimlane_ngx-datatable___ivy_ngcc___fesm2015_swimlane-ngx-datatable_js"), __webpack_require__.e("default-node_modules_ng2-file-upload___ivy_ngcc___fesm2015_ng2-file-upload_js"), __webpack_require__.e("default-src_app_components_components_module_ts"), __webpack_require__.e("common"), __webpack_require__.e("src_app_report_report_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./report/report.module */ 54107)).then(m => m.ReportPageModule) },
    { path: 'reportcategory', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_reportcategory_reportcategory_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./reportcategory/reportcategory.module */ 6067)).then(m => m.ReportcategoryPageModule) },
    { path: 'selectorsingle/:rptid/:scontrolname/:controltype', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_selectorsingle_selectorsingle_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./selectorsingle/selectorsingle.module */ 54978)).then(m => m.SelectorsinglePageModule) },
    { path: 'customer-quotation', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_quotation_customer-quotation_customer-quotation_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./quotation/customer-quotation/customer-quotation.module */ 61382)).then(m => m.CustomerQuotationPageModule) },
    { path: 'add-edit-service-product', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_quotation_add-edit-service-product_add-edit-service-product_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./quotation/add-edit-service-product/add-edit-service-product.module */ 90488)).then(m => m.AddEditServiceProductPageModule) },
    { path: 'quotation-approval', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_quotation_quotation-approval_quotation-approval_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./quotation/quotation-approval/quotation-approval.module */ 16007)).then(m => m.QuotationApprovalPageModule) },
    { path: 'consolidation-order', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_swimlane_ngx-datatable___ivy_ngcc___fesm2015_swimlane-ngx-datatable_js"), __webpack_require__.e("common"), __webpack_require__.e("src_app_consolidation-order_consolidation-order_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./consolidation-order/consolidation-order.module */ 80512)).then(m => m.ConsolidationOrderPageModule) },
    { path: 'upload', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng2-file-upload___ivy_ngcc___fesm2015_ng2-file-upload_js"), __webpack_require__.e("src_app_upload_upload_upload_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./upload/upload/upload.module */ 69652)).then(m => m.UploadPageModule) },
    { path: 'product-filter', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_product-filter_product-filter_page_ts"), __webpack_require__.e("src_app_product-filter_product-filter_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./product-filter/product-filter.module */ 67042)).then(m => m.ProductFilterPageModule) },
    { path: 'product-list', loadChildren: () => __webpack_require__.e(/*! import() */ "common").then(__webpack_require__.bind(__webpack_require__, /*! ./product-list/product-list.module */ 32305)).then(m => m.ProductListPageModule) },
    { path: 'custom-alert', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_custom-alert_custom-alert_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./custom-alert/custom-alert.module */ 17906)).then(m => m.CustomAlertPageModule) },
    { path: 'page-name', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_modals_page-name_page-name_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./modals/page-name/page-name.module */ 44354)).then(m => m.PageNamePageModule) },
    { path: 'mpr-form', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_swimlane_ngx-datatable___ivy_ngcc___fesm2015_swimlane-ngx-datatable_js"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_datepicker_mjs"), __webpack_require__.e("common"), __webpack_require__.e("src_app_hrms_mpr-form_mpr-form_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./hrms/mpr-form/mpr-form.module */ 9697)).then(m => m.MprFormPageModule) },
    { path: 'mpr-list', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_hrms_mpr-list_mpr-list_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./hrms/mpr-list/mpr-list.module */ 76208)).then(m => m.MprListPageModule) },
    { path: 'arvisitschedule', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_arvisitschedule_arvisitschedule_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./arvisitschedule/arvisitschedule.module */ 70570)).then(m => m.ArvisitschedulePageModule) },
    { path: 'section', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng2-file-upload___ivy_ngcc___fesm2015_ng2-file-upload_js"), __webpack_require__.e("default-src_app_components_components_module_ts"), __webpack_require__.e("src_app_arvisitschedule_section_section_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./arvisitschedule/section/section.module */ 51836)).then(m => m.SectionPageModule) },
    { path: 'sectionview', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng2-file-upload___ivy_ngcc___fesm2015_ng2-file-upload_js"), __webpack_require__.e("default-src_app_components_components_module_ts"), __webpack_require__.e("src_app_arvisitschedule_sectionview_sectionview_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./arvisitschedule/sectionview/sectionview.module */ 82651)).then(m => m.SectionviewPageModule) },
    { path: 'selectorsinglequestionframwork', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_selectorsinglequestionframwork_selectorsingle_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./selectorsinglequestionframwork/selectorsingle.module */ 38861)).then(m => m.SelectorsinglePageModule) },
    { path: 'multipleentry', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_multipleentry_multipleentry_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./multipleentry/multipleentry.module */ 96930)).then(m => m.MultipleentryPageModule) },
    { path: 'aruserselect', loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_arvisitschedule_aruserselect_aruserselect_module_ts-src_app_arvisitschedule_arvisitsc-c49eca")]).then(__webpack_require__.bind(__webpack_require__, /*! ./arvisitschedule/aruserselect/aruserselect.module */ 29503)).then(m => m.AruserselectPageModule) },
    { path: 'unplannedvisit', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_arvisitschedule_unplannedvisit_unplannedvisit_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./arvisitschedule/unplannedvisit/unplannedvisit.module */ 47894)).then(m => m.UnplannedvisitPageModule) },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.NgModule)({
        imports: [
            //  RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
            _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forRoot(routes, { useHash: true }),
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule]
    })
], AppRoutingModule);



/***/ }),

/***/ 55041:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": () => (/* binding */ AppComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _app_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app.component.html?ngResource */ 33383);
/* harmony import */ var _app_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component.scss?ngResource */ 79259);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login/loginauth.service */ 44010);
/* harmony import */ var _common_Constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./common/Constants */ 68209);
/* harmony import */ var _ionic_native_pin_check_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/pin-check/ngx */ 7384);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ 80190);
/* harmony import */ var _ionic_native_fingerprint_aio_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/fingerprint-aio/ngx */ 63427);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _awesome_cordova_plugins_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @awesome-cordova-plugins/splash-screen/ngx */ 64883);
/* harmony import */ var _awesome_cordova_plugins_status_bar_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @awesome-cordova-plugins/status-bar/ngx */ 1550);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../provider/message-helper */ 98792);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var _ionic_native_market_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/market/ngx */ 68538);
/* harmony import */ var _order_approval_order_approval_service_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./order-approval/order-approval-service.service */ 44159);


















let AppComponent = class AppComponent {
    constructor(platform, splashScreen, statusBar, loginauth, pinCheck, storage, router, menuctrl, fingerprint, msg, commonFunction, market, 
    // public events: Events,
    zone, orderApprovalServiceService) {
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.loginauth = loginauth;
        this.pinCheck = pinCheck;
        this.storage = storage;
        this.router = router;
        this.menuctrl = menuctrl;
        this.fingerprint = fingerprint;
        this.msg = msg;
        this.commonFunction = commonFunction;
        this.market = market;
        this.zone = zone;
        this.orderApprovalServiceService = orderApprovalServiceService;
        this.isNewLead = false;
        this.isExistingLead = false;
        this.isBusinessPartnerAddress = false;
        this.isNewOrder = false;
        this.isDraftOrder = false;
        this.isOrderStatus = false;
        this.isLatLongFinder = false;
        this.isTravelPlan = false;
        this.isActualTravelPlan = false;
        this.isTravelExpense = false;
        this.isTravelPlanClosure = false;
        this.isApprovalAccess = false;
        this.isCustomerServiceAccess = false;
        this.isComplaintReportingAccess = false;
        this.isCompliantAcceptanceAccess = false;
        this.isFieldVisitAccess = false;
        this.isQuotationAccess = false;
        this.isQuotationApproval = false;
        this.isUpload = false;
        this.isReport = false;
        this.isarvisitschedule = false;
        this.isConsolidationOrder = false;
        this.isexpenseaccess = false;
        this.isschemeinfo = false;
        this.TAG = "AppComponent";
        this.appPages = [
            {
                title: 'New Lead', url: '/newcustomer', icon: 'person-add', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Existing Lead', url: '/existingcustomer', icon: 'person', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Business Partner Address', url: '/business-partner-address', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'New Order', url: '/neworder', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Draft Order', url: '/existingorder', icon: 'list-box', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Order Status', url: '/order-status', icon: 'stats', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Lat-Long Finder', url: '/location-finder', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Travel Plan', url: '/travel-plan', icon: 'pin', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Visit Activity Report', url: '/actual-travel-plan', icon: 'pin', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Travel Expense', url: '/travel-expense', icon: 'pin', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Expense Closure', url: '/travel-plan-closure', icon: 'pin', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Approval', url: '/order-approval', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Scheme Information', url: '/scheme-information', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Customer Service', url: '/customer-service', icon: 'megaphone', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Complaint Reporting', url: '/service-manager', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Compliant Acceptance', url: '/vender-approval', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Field Visit', url: '/service-engineer-visit', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Reports', url: '/profile', icon: 'bookmarks', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'AR Visit Schedule', url: '/arvisitschedule', icon: 'bookmarks', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Customer Quotation', url: '/customer-quotation', icon: 'wallet', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Quotation Approval', url: '/quotation-approval', icon: 'wallet', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Consolidation Order', url: '/consolidation-order', icon: 'keypad', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Upload', url: '/upload', icon: 'cloud-upload', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Settings', url: '/settings', icon: 'settings', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Logout', url: '/login', icon: 'log-out', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            }
        ];
        this.vetappPages = [
            {
                title: 'My Transaction', url: '/use-vetcoins-transaction', icon: 'cash', approvalaccess: true, isschemeinfo: true,
                isQuotationAccess: true, isQuotationApproval: true
            },
            {
                title: 'Use VetCoins', url: '/use-vetcoins', icon: 'basketball', approvalaccess: true, isschemeinfo: true,
                isQuotationAccess: true, isQuotationApproval: true
            },
            {
                title: 'About', url: '/about', icon: 'chatbubbles', approvalaccess: true, isschemeinfo: true, isQuotationAccess: true,
                isQuotationApproval: true
            },
            {
                title: 'Logout', url: '/login', icon: 'log-out', approvalaccess: true, isschemeinfo: true, isQuotationAccess: true,
                isQuotationApproval: true
            }
        ];
        this.checkcust = [
            {
                title: 'New Order', url: '/neworder', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Draft Order', url: '/existingorder', icon: 'list-box', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Order Status', url: '/order-status', icon: 'stats', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Lat-Long Finder', url: '/location-finder', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Travel Plan', url: '/travel-plan', icon: 'pin', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Visit Activity Report', url: '/actual-travel-plan', icon: 'pin', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Travel Expense', url: '/travel-expense', icon: 'pin', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Expense Closure', url: '/travel-plan-closure', icon: 'pin', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Approval', url: '/order-approval', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Scheme Information', url: '/scheme-information', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Customer Service', url: '/customer-service', icon: 'megaphone', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Complaint Reporting', url: '/service-manager', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Compliant Acceptance', url: '/vender-approval', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Field Visit', url: '/service-engineer-visit', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Reports', url: '/profile', icon: 'bookmarks', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Customer Quotation', url: '/customer-quotation', icon: 'wallet', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Quotation Approval', url: '/quotation-approval', icon: 'wallet', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Consolidation Order', url: '/consolidation-order', icon: 'keypad', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Upload', url: '/upload', icon: 'cloud-upload', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Settings', url: '/settings', icon: 'settings', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            },
            {
                title: 'Logout', url: '/login', icon: 'log-out', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
            }
        ];
        this.ishidemenu = true;
        this.iswebplatform = false;
        this.storage.create();
        this.VERSION_NO = _common_Constants__WEBPACK_IMPORTED_MODULE_3__.Constants.VERSION_NO;
        this.loginauth.getObservable().subscribe(data => {
            if (data == 'updateMenu') {
                this.zone.run(() => {
                    //console.log('events force update the screen');
                    this.isNewLead = this.loginauth.isNewLead;
                    this.isExistingLead = this.loginauth.isExistingLead;
                    this.isBusinessPartnerAddress = this.loginauth.isBusinessPartnerAddress;
                    this.isNewOrder = this.loginauth.isNewOrder;
                    this.isDraftOrder = this.loginauth.isDraftOrder;
                    this.isOrderStatus = this.loginauth.isOrderStatus;
                    this.isLatLongFinder = this.loginauth.isLatLongFinder;
                    this.isTravelPlan = this.loginauth.isTravelPlan;
                    this.isActualTravelPlan = this.loginauth.isActualTravelPlan;
                    this.isTravelExpense = this.loginauth.isTravelExpense;
                    this.isTravelPlanClosure = this.loginauth.isTravelPlanClosure;
                    this.isApprovalAccess = this.loginauth.isApprovalAccess;
                    this.isschemeinfo = this.loginauth.isschemeinfo;
                    this.isCustomerServiceAccess = this.loginauth.isCustomerServiceAccess;
                    this.isCompliantAcceptanceAccess = this.loginauth.isCompliantAcceptanceAccess;
                    this.isComplaintReportingAccess = this.loginauth.isComplaintReportingAccess;
                    this.isFieldVisitAccess = this.loginauth.isFieldVisitAccess;
                    this.isQuotationAccess = this.loginauth.isQuotationAccess;
                    this.isQuotationApproval = this.loginauth.isQuotationApproval;
                    this.isConsolidationOrder = this.loginauth.isConsolidationOrder;
                    this.isUpload = this.loginauth.isUpload;
                    this.isReport = this.loginauth.isReport;
                    this.isarvisitschedule = this.loginauth.isARVisitSchedule;
                    this.Updatemenu();
                });
            }
        });
        this.router.events.subscribe((e) => {
            this.isNewLead = this.loginauth.isNewLead;
            this.isExistingLead = this.loginauth.isExistingLead;
            this.isBusinessPartnerAddress = this.loginauth.isBusinessPartnerAddress;
            this.isNewOrder = this.loginauth.isNewOrder;
            this.isDraftOrder = this.loginauth.isDraftOrder;
            this.isOrderStatus = this.loginauth.isOrderStatus;
            this.isLatLongFinder = this.loginauth.isLatLongFinder;
            this.isTravelPlan = this.loginauth.isTravelPlan;
            this.isActualTravelPlan = this.loginauth.isActualTravelPlan;
            this.isTravelExpense = this.loginauth.isTravelExpense;
            this.isTravelPlanClosure = this.loginauth.isTravelPlanClosure;
            this.isApprovalAccess = this.loginauth.isApprovalAccess;
            this.isschemeinfo = this.loginauth.isschemeinfo;
            this.isCustomerServiceAccess = this.loginauth.isCustomerServiceAccess;
            this.isCompliantAcceptanceAccess = this.loginauth.isCompliantAcceptanceAccess;
            this.isComplaintReportingAccess = this.loginauth.isComplaintReportingAccess;
            this.isFieldVisitAccess = this.loginauth.isFieldVisitAccess;
            this.isQuotationAccess = this.loginauth.isQuotationAccess;
            this.isQuotationApproval = this.loginauth.isQuotationApproval;
            this.isUpload = this.loginauth.isUpload;
            this.isReport = this.loginauth.isReport;
            this.isarvisitschedule = this.loginauth.isARVisitSchedule;
            this.Updatemenu();
            if (this.router.url != "/order-approval" && this.router.url != "/filter") {
                this.orderApprovalServiceService.filterTab = [];
                this.orderApprovalServiceService.filterOrg = [];
                this.orderApprovalServiceService.filterDocType = [];
                this.orderApprovalServiceService.filterBusinessPartner = 'CLEAR';
                this.orderApprovalServiceService.filterselectedBusinessPartner = '';
                this.orderApprovalServiceService.pageOffset = 0;
            }
            if (this.router.url == "/login" || this.router.url == "/login-option" || this.router.url == "/terms-of-agreement") {
                this.ishidemenu = true;
                this.menuctrl.enable(false);
            }
            else {
                this.ishidemenu = false;
                this.menuctrl.enable(true);
            }
        });
        this.initializeApp();
        if (!this.msg.isplatformweb) {
            this.iswebplatform = false;
        }
        else {
            this.iswebplatform = true;
        }
        this.storage.get('darkMode').then((darkMode) => {
            if (darkMode) {
                this.darkMode = darkMode;
            }
            else {
                this.storage.set('darkMode', false);
                this.darkMode = false;
            }
        });
    }
    themeMode() {
        this.storage.set('darkMode', (!this.darkMode));
        this.darkMode != this.darkMode;
        document.body.classList.toggle('dark');
    }
    // chkdata() {
    //   if (!this.loginauth.defaultprofile) {
    //     this.storage.get('loginauth').then((loginauth) => {
    //       if (loginauth) {
    //         this.loginauth = loginauth;
    //         this.router.navigateByUrl('/home');
    //       }
    //       else {
    //         this.router.navigateByUrl('/login');
    //       }
    //     });
    //   }
    //   else {
    //     this.storage.set('loginauth', this.loginauth);
    //   }
    // }
    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.overlaysWebView(false);
            this.statusBar.isVisible = true;
            this.statusBar.styleDefault();
            this.storage.set('changePasswordFromSettingPage', false);
            this.splashScreen.hide();
            this.fingerprint.isAvailable()
                .then((isAvailableResult) => {
                this.storage.set('Biometric_Type', isAvailableResult);
                this.storage.set('Biometric_Device_Codes', '200');
            })
                .catch((fingerprintError) => {
                this.storage.set('Biometric_Device_Codes', fingerprintError.code);
                this.storage.set('Biometric_Device_Codes', fingerprintError.code);
            });
            this.pinCheck.isPinSetup().then((pinSuccess) => {
                this.storage.set('PIN_Status', "200");
            }).catch((pinError) => {
                this.storage.set('PIN_Status', "400");
            });
        });
    }
    Updatemenu() {
        try {
            //  console.log("TAG","Updatemenu");
            this.appPages = [
                {
                    title: 'New Lead', url: '/newcustomer', icon: 'person-add', isNewLead: this.isNewLead, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Existing Lead', url: '/existingcustomer', icon: 'person', isNewLead: true, isExistingLead: this.isExistingLead, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Business Partner Address', url: '/business-partner-address', icon: 'clipboard',
                    isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: this.isBusinessPartnerAddress,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'New Order', url: '/neworder', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: this.isNewOrder, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Draft Order', url: '/existingorder', icon: 'list', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: this.isDraftOrder, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Order Status', url: '/order-status', icon: 'stats-chart', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: this.isOrderStatus, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Lat-Long Finder', url: '/location-finder', icon: 'clipboard',
                    isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: this.isLatLongFinder, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Travel Plan', url: '/travel-plan', icon: 'pin', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: this.isTravelPlan, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Visit Activity Report', url: '/actual-travel-plan', icon: 'pin',
                    isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: this.isActualTravelPlan,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Travel Expense', url: '/travel-expense', icon: 'pin', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: this.isTravelExpense, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Expense Closure', url: '/travel-plan-closure', icon: 'pin',
                    isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: this.isTravelPlanClosure, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Approval', url: '/order-approval', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: this.isApprovalAccess, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Scheme Information', url: '/scheme-information', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: this.isschemeinfo, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Customer Service', url: '/customer-service', icon: 'megaphone', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: this.isCustomerServiceAccess,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Complaint Reporting', url: '/service-manager', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: this.isComplaintReportingAccess, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Compliant Acceptance', url: '/vender-approval', icon: 'clipboard',
                    isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: this.isCompliantAcceptanceAccess, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Field Visit', url: '/service-engineer-visit', icon: 'bicycle', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: this.isFieldVisitAccess,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Reports', url: '/profile', icon: 'bookmarks', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: this.isReport, isarvisitschedule: true
                },
                {
                    title: 'AR Visit Schedule', url: '/arvisitschedule', icon: 'bookmarks', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: this.isarvisitschedule
                },
                {
                    title: 'Customer Quotation', url: '/customer-quotation', icon: 'wallet', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: this.isQuotationAccess, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Quotation Approval', url: '/quotation-approval', icon: 'wallet', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: this.isQuotationApproval, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Consolidation Order', url: '/consolidation-order', icon: 'keypad', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: this.isConsolidationOrder, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Upload', url: '/upload', icon: 'cloud-upload', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: this.isUpload, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Settings', url: '/settings', icon: 'settings', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Logout', url: '/login', icon: 'log-out', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                }
            ];
            this.vetappPages = [
                {
                    title: 'My Transaction', url: '/use-vetcoins-transaction', icon: 'cash', approvalaccess: true, isschemeinfo: true,
                    isQuotationAccess: true, isQuotationApproval: true
                },
                {
                    title: 'Use VetCoins', url: '/use-vetcoins', icon: 'basketball', approvalaccess: true, isschemeinfo: true,
                    isQuotationAccess: true, isQuotationApproval: true
                },
                {
                    title: 'About', url: '/about', icon: 'chatbubbles', approvalaccess: true, isschemeinfo: true, isQuotationAccess: true,
                    isQuotationApproval: true
                },
                {
                    title: 'Logout', url: '/login', icon: 'log-out', approvalaccess: true, isschemeinfo: true, isQuotationAccess: true,
                    isQuotationApproval: true
                }
            ];
            this.checkcust = [
                {
                    title: 'New Order', url: '/neworder', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: this.isNewOrder, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Draft Order', url: '/existingorder', icon: 'list-box', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: this.isDraftOrder, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Order Status', url: '/order-status', icon: 'stats', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: this.isOrderStatus, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Lat-Long Finder', url: '/location-finder', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: this.isLatLongFinder, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Travel Plan', url: '/travel-plan', icon: 'pin', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: this.isTravelPlan, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Visit Activity Report', url: '/actual-travel-plan', icon: 'pin', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: this.isActualTravelPlan,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Travel Expense', url: '/travel-expense', icon: 'pin', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: this.isTravelExpense, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Expense Closure', url: '/travel-plan-closure', icon: 'pin', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: this.isTravelPlanClosure, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Approval', url: '/order-approval', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: this.isApprovalAccess, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Scheme Information', url: '/scheme-information', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: this.isschemeinfo, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Customer Service', url: '/customer-service', icon: 'megaphone', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: this.isCustomerServiceAccess,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Complaint Reporting', url: '/service-manager', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: this.isComplaintReportingAccess, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Compliant Acceptance', url: '/vender-approval', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: this.isCompliantAcceptanceAccess, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Field Visit', url: '/service-engineer-visit', icon: 'clipboard', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: this.isFieldVisitAccess,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Reports', url: '/profile', icon: 'bookmarks', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: this.isReport, isarvisitschedule: true
                },
                {
                    title: 'AR Visit Schedule', url: '/arvisitschedule', icon: 'bookmarks', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: this.isarvisitschedule
                },
                {
                    title: 'Customer Quotation', url: '/customer-quotation', icon: 'wallet', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: this.isQuotationAccess, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Quotation Approval', url: '/quotation-approval', icon: 'wallet', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: this.isQuotationApproval, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Consolidation Order', url: '/consolidation-order', icon: 'keypad', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: this.isConsolidationOrder, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Upload', url: '/upload', icon: 'cloud-upload', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: this.isUpload, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Settings', url: '/settings', icon: 'settings', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                },
                {
                    title: 'Logout', url: '/login', icon: 'log-out', isNewLead: true, isExistingLead: true, isBusinessPartnerAddress: true,
                    isNewOrder: true, isDraftOrder: true, isOrderStatus: true, isLatLongFinder: true, isTravelPlan: true, isActualTravelPlan: true,
                    isTravelExpense: true, isTravelPlanClosure: true, isApprovalAccess: true, isschemeinfo: true, isCustomerServiceAccess: true,
                    isComplaintReportingAccess: true, isCompliantAcceptanceAccess: true, isFieldVisitAccess: true,
                    isQuotationAccess: true, isQuotationApproval: true, isConsolidationOrder: true, isUpload: true, isReport: true, isarvisitschedule: true
                }
            ];
        }
        catch (error) {
            //  console.log("ERROE Uodatemenu: ", error);
        }
    }
};
AppComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.Platform },
    { type: _awesome_cordova_plugins_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_7__.SplashScreen },
    { type: _awesome_cordova_plugins_status_bar_ngx__WEBPACK_IMPORTED_MODULE_8__.StatusBar },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService },
    { type: _ionic_native_pin_check_ngx__WEBPACK_IMPORTED_MODULE_4__.PinCheck },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__.Storage },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_14__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.MenuController },
    { type: _ionic_native_fingerprint_aio_ngx__WEBPACK_IMPORTED_MODULE_6__.FingerprintAIO },
    { type: _provider_message_helper__WEBPACK_IMPORTED_MODULE_9__.Message },
    { type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_10__.Commonfun },
    { type: _ionic_native_market_ngx__WEBPACK_IMPORTED_MODULE_11__.Market },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_15__.NgZone },
    { type: _order_approval_order_approval_service_service__WEBPACK_IMPORTED_MODULE_12__.OrderApprovalServiceService }
];
AppComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_16__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_15__.Component)({
        selector: 'app-root',
        template: _app_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_app_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], AppComponent);



/***/ }),

/***/ 36747:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": () => (/* binding */ AppModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! @angular/platform-browser */ 34497);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _awesome_cordova_plugins_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @awesome-cordova-plugins/screen-orientation/ngx */ 11898);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _awesome_cordova_plugins_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @awesome-cordova-plugins/splash-screen/ngx */ 64883);
/* harmony import */ var _awesome_cordova_plugins_status_bar_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @awesome-cordova-plugins/status-bar/ngx */ 1550);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ 55041);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ 90158);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login/loginauth.service */ 44010);
/* harmony import */ var _ionic_native_fingerprint_aio_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/fingerprint-aio/ngx */ 63427);
/* harmony import */ var _ionic_storage_angular__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! @ionic/storage-angular */ 47566);
/* harmony import */ var _ionic_native_secure_storage_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/secure-storage/ngx */ 36608);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _awesome_cordova_plugins_image_picker_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @awesome-cordova-plugins/image-picker/ngx */ 92153);
/* harmony import */ var _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @awesome-cordova-plugins/camera/ngx */ 64587);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var src_provider_validator_helper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/provider/validator-helper */ 35096);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../provider/commonfun */ 51156);
/* harmony import */ var _ionic_native_pin_check_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/pin-check/ngx */ 7384);
/* harmony import */ var _awesome_cordova_plugins_geolocation_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @awesome-cordova-plugins/geolocation/ngx */ 36457);
/* harmony import */ var _awesome_cordova_plugins_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @awesome-cordova-plugins/location-accuracy/ngx */ 35192);
/* harmony import */ var _awesome_cordova_plugins_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @awesome-cordova-plugins/native-geocoder/ngx */ 79683);
/* harmony import */ var _awesome_cordova_plugins_http_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @awesome-cordova-plugins/http/ngx */ 26123);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../provider/message-helper */ 98792);
/* harmony import */ var _neworder_neworder_page__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./neworder/neworder.page */ 10990);
/* harmony import */ var _awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @awesome-cordova-plugins/social-sharing/ngx */ 81952);
/* harmony import */ var _ionic_native_market_ngx__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ionic-native/market/ngx */ 68538);
/* harmony import */ var _login_login_page__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./login/login.page */ 66825);
/* harmony import */ var _awesome_cordova_plugins_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @awesome-cordova-plugins/in-app-browser/ngx */ 12407);
/* harmony import */ var _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @awesome-cordova-plugins/file-transfer/ngx */ 80464);
/* harmony import */ var _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @awesome-cordova-plugins/file/ngx */ 25453);
/* harmony import */ var _awesome_cordova_plugins_file_opener_ngx__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @awesome-cordova-plugins/file-opener/ngx */ 8456);
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! @angular/platform-browser/animations */ 37146);
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./material.module */ 63806);
/* harmony import */ var _common_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./common/pipes/pipes.module */ 22522);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! @angular/material/input */ 68562);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! @angular/material/form-field */ 75074);
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! @angular/material/list */ 6517);
/* harmony import */ var _ionic_native_file_chooser_ngx__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @ionic-native/file-chooser/ngx */ 86786);
/* harmony import */ var _awesome_cordova_plugins_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @awesome-cordova-plugins/android-permissions/ngx */ 71183);
/* harmony import */ var _awesome_cordova_plugins_file_path_ngx__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @awesome-cordova-plugins/file-path/ngx */ 3501);
/* harmony import */ var _ionic_native_file_picker_ngx__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @ionic-native/file-picker/ngx */ 63507);
/* harmony import */ var ng2_search_filter__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ng2-search-filter */ 9991);
/* harmony import */ var _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @ionic-native/crop/ngx */ 82475);
/* harmony import */ var _awesome_cordova_plugins_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! @awesome-cordova-plugins/barcode-scanner/ngx */ 38283);
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! @angular/material/stepper */ 44193);


















































let AppModule = class AppModule {
};
AppModule = (0,tslib__WEBPACK_IMPORTED_MODULE_35__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_36__.NgModule)({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__.AppComponent],
        entryComponents: [],
        imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_37__.BrowserModule, _angular_common_http__WEBPACK_IMPORTED_MODULE_38__.HttpClientModule, _angular_forms__WEBPACK_IMPORTED_MODULE_39__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_39__.ReactiveFormsModule, ionic_selectable__WEBPACK_IMPORTED_MODULE_40__.IonicSelectableModule,
            _material_module__WEBPACK_IMPORTED_MODULE_26__.MaterialModule,
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_41__.MatFormFieldModule,
            _angular_material_input__WEBPACK_IMPORTED_MODULE_42__.MatInputModule,
            _angular_material_list__WEBPACK_IMPORTED_MODULE_43__.MatListModule,
            _angular_material_stepper__WEBPACK_IMPORTED_MODULE_44__.MatStepperModule,
            ng2_search_filter__WEBPACK_IMPORTED_MODULE_32__.Ng2SearchPipeModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_45__.IonicModule.forRoot({ mode: 'ios' }), _app_routing_module__WEBPACK_IMPORTED_MODULE_4__.AppRoutingModule, _ionic_storage_angular__WEBPACK_IMPORTED_MODULE_46__.IonicStorageModule.forRoot(), _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_47__.BrowserAnimationsModule],
        providers: [
            _awesome_cordova_plugins_status_bar_ngx__WEBPACK_IMPORTED_MODULE_2__.StatusBar, _login_login_page__WEBPACK_IMPORTED_MODULE_21__.LoginPage,
            _awesome_cordova_plugins_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_1__.SplashScreen,
            _login_loginauth_service__WEBPACK_IMPORTED_MODULE_5__.LoginauthService,
            _ionic_native_fingerprint_aio_ngx__WEBPACK_IMPORTED_MODULE_6__.FingerprintAIO,
            _awesome_cordova_plugins_geolocation_ngx__WEBPACK_IMPORTED_MODULE_13__.Geolocation,
            _awesome_cordova_plugins_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_14__.LocationAccuracy,
            _awesome_cordova_plugins_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_15__.NativeGeocoder,
            _ionic_native_secure_storage_ngx__WEBPACK_IMPORTED_MODULE_7__.SecureStorage,
            _angular_common__WEBPACK_IMPORTED_MODULE_48__.DatePipe, _common_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_27__.PipesModule,
            _awesome_cordova_plugins_image_picker_ngx__WEBPACK_IMPORTED_MODULE_8__.ImagePicker, _provider_message_helper__WEBPACK_IMPORTED_MODULE_17__.Message,
            _ionic_native_pin_check_ngx__WEBPACK_IMPORTED_MODULE_12__.PinCheck,
            _awesome_cordova_plugins_http_ngx__WEBPACK_IMPORTED_MODULE_16__.HTTP,
            _ionic_native_market_ngx__WEBPACK_IMPORTED_MODULE_20__.Market,
            _awesome_cordova_plugins_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_29__.AndroidPermissions,
            _awesome_cordova_plugins_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_0__.ScreenOrientation,
            _awesome_cordova_plugins_file_path_ngx__WEBPACK_IMPORTED_MODULE_30__.FilePath,
            _awesome_cordova_plugins_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_34__.BarcodeScanner,
            _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_9__.Camera, src_provider_validator_helper__WEBPACK_IMPORTED_MODULE_10__.Validator, _provider_commonfun__WEBPACK_IMPORTED_MODULE_11__.Commonfun, _neworder_neworder_page__WEBPACK_IMPORTED_MODULE_18__.NeworderPage, _awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_19__.SocialSharing, _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_23__.FileTransfer, _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_24__.File, _awesome_cordova_plugins_file_opener_ngx__WEBPACK_IMPORTED_MODULE_25__.FileOpener,
            _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_33__.Crop,
            _awesome_cordova_plugins_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_22__.InAppBrowser, _ionic_native_file_chooser_ngx__WEBPACK_IMPORTED_MODULE_28__.FileChooser, _ionic_native_file_picker_ngx__WEBPACK_IMPORTED_MODULE_31__.IOSFilePicker,
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_49__.RouteReuseStrategy, useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_45__.IonicRouteStrategy }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__.AppComponent]
    })
], AppModule);



/***/ }),

/***/ 68209:
/*!*************************************!*\
  !*** ./src/app/common/Constants.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Constants": () => (/* binding */ Constants)
/* harmony export */ });
class Constants {
}
Constants.IS_DEBUG = false; // toggle this to turn on / off for global control
Constants.VERSION_NO = "0.0.124";
Constants.isplatformweb = true;
Constants.APP_NAME = "MPOWER";
Constants.RELEASE_DATE = "28 Jun 2022";
Constants.RELEASE_NOTE = "New customer address line 2 and 3 validation";
//public static DOMAIN_URL = "https://p2uat.pispl.in"; // UAT
//public static DOMAIN_URL = "https://p2prelive.pispl.in";
Constants.DOMAIN_URL = "https://p2test.pispl.in"; // P2TEST


/***/ }),

/***/ 28475:
/*!*******************************************************!*\
  !*** ./src/app/common/generic-http-client.service.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GenericHttpClientService": () => (/* binding */ GenericHttpClientService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _awesome_cordova_plugins_http_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @awesome-cordova-plugins/http/ngx */ 26123);
/* harmony import */ var rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/internal/Observable */ 84758);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../provider/message-helper */ 98792);







let GenericHttpClientService = class GenericHttpClientService {
    constructor(cordovaHTTP, platform, http, msg) {
        this.cordovaHTTP = cordovaHTTP;
        this.platform = platform;
        this.http = http;
        this.msg = msg;
        this.TAG = 'Generic Http Client Service';
        this.ReadOnlyUsername = 'ionic.appuser';
        this.ReadOnlypassword = 'ionic.appuser';
    }
    get(destinationUrl, isfirst, isReadonly) {
        let login = this.user; //"P2admin";
        let password = this.pass; //"Pass2020";
        if (isReadonly) {
            login = this.ReadOnlyUsername; //"P2admin";
            password = this.ReadOnlypassword; //"Pass2020";
        }
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Accept': 'application/json, text/plain, */*',
                'Authorization': 'Basic ' + auth
            })
        };
        destinationUrl = destinationUrl + '&stateless=true';
        if (this.msg.isplatformweb == false) {
            return rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_3__.Observable.create((observer) => {
                this.cordovaHTTP.get(destinationUrl, {}, {
                    'Content-Type': 'application/json;charset=utf-8',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'Accept': 'application/json, text/plain, */*',
                    'Authorization': 'Basic ' + auth
                })
                    .then(response => {
                    let data;
                    if (!!response.data) {
                        data = JSON.parse(response.data);
                    }
                    else {
                        data = response;
                    }
                    observer.next(data);
                    observer.complete();
                }).catch((error) => {
                    console.log("cordovaHTTP get error", error);
                    observer.next("Something Went Wrong Please Try Again");
                    observer.complete();
                });
            });
        }
        else {
            //httpOptions["params"]=requestData;
            return this.http.get(destinationUrl, isfirst ? {} : httpOptions);
        }
    }
    post(URL, requestData, header) {
        let specificHeader;
        let auth = header.headers.get('Authorization');
        this.cordovaHTTP.setDataSerializer('json');
        // if(this.platform.is('cordova')){
        if (this.msg.isplatformweb == false) {
            specificHeader = { 'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': auth
            };
            return rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_3__.Observable.create((observer) => {
                this.cordovaHTTP.post(URL, requestData, specificHeader)
                    .then((response) => {
                    console.log("post data", response);
                    let data = JSON.parse(response.data);
                    observer.next(data);
                    observer.complete();
                }).catch((error) => {
                    console.log("error", error);
                });
            });
        }
        else {
            specificHeader = header;
            return this.http.post(URL, requestData, specificHeader);
        }
    }
    filePost(URL, requestData, header, filePath, fileKey) {
        console.log(this.TAG, "filePost Hit");
        let specificHeader;
        let auth = header.headers.get('Authorization');
        this.cordovaHTTP.setDataSerializer('json');
        // if(this.platform.is('cordova')){
        if (this.msg.isplatformweb == false) {
            specificHeader = {
                'Authorization': auth
            };
            return rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_3__.Observable.create((observer) => {
                this.cordovaHTTP.uploadFile(URL, requestData, specificHeader, filePath, fileKey)
                    .then((response) => {
                    let data = JSON.parse(response.data);
                    observer.next(data);
                    observer.complete();
                }).catch((error) => {
                    console.log(this.TAG, "filePost Hit", error);
                });
            });
        }
        else {
            specificHeader = header;
            return this.http.post(URL, requestData, specificHeader);
        }
    }
    fileDevicePost(URL, requestData, header) {
        let specificHeader;
        let auth = header.headers.get('Authorization');
        this.cordovaHTTP.setDataSerializer('multipart');
        //console.log(this.TAG, "fileDevicePost", URL,requestData);
        // if(this.platform.is('cordova')){
        if (this.msg.isplatformweb == false) {
            specificHeader = {
                'Authorization': auth
            };
            return rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_3__.Observable.create((observer) => {
                this.cordovaHTTP.post(URL, requestData, specificHeader)
                    .then((response) => {
                    let data = JSON.parse(response.data);
                    observer.next(data);
                    observer.complete();
                }).catch((error) => {
                    console.log("error", error);
                });
            });
        }
        else {
            console.log(this.TAG, "else");
            specificHeader = header;
            return this.http.post(URL, requestData, specificHeader);
        }
    }
    postformdata(URL, requestData, header, httpOptions) {
        let specificHeader = header;
        // let auth = header.headers.get('Authorization');
        this.cordovaHTTP.setDataSerializer('multipart');
        // if(this.platform.is('cordova')){
        if (this.msg.isplatformweb == false) {
            console.log("postformdata", URL);
            // specificHeader = { 'Content-Type':'application/json',
            //                     'Accept':'application/json',
            //                     'Authorization':auth
            //                   }
            //  return this.cordovaHTTP.post(URL, requestData,  specificHeader);
            return rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_3__.Observable.create((observer) => {
                this.cordovaHTTP.post(URL, requestData, specificHeader)
                    .then((response) => {
                    // console.log("postformdata:response",response);
                    let data = JSON.parse(response.data);
                    observer.next(data);
                    observer.complete();
                }).catch((error) => {
                });
            });
        }
        else {
            //   let specificHeaderempty;
            specificHeader = header;
            return this.http.post(URL, requestData, httpOptions);
        }
    }
    postformdataDownload(URL, requestData) {
        let login = this.user;
        let password = this.pass;
        const auth = btoa(login + ":" + password);
        if (!this.msg.isplatformweb) {
            this.cordovaHTTP.setDataSerializer('json');
            console.log("postformdata:request", requestData);
            return new rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_3__.Observable((observer) => {
                this.cordovaHTTP.sendRequest(URL, {
                    method: 'post',
                    data: requestData,
                    headers: { 'Authorization': 'Basic ' + auth },
                    responseType: 'blob'
                })
                    .then(response => {
                    console.log("postformdata:response", response.data);
                    observer.next(response.data);
                    observer.complete();
                }).catch((error) => {
                    console.log(error);
                });
            });
        }
        else {
            return this.http.post(URL, requestData, { headers: { 'Authorization': 'Basic ' + auth }, responseType: 'blob' });
        }
    }
};
GenericHttpClientService.ctorParameters = () => [
    { type: _awesome_cordova_plugins_http_ngx__WEBPACK_IMPORTED_MODULE_0__.HTTP },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.Platform },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _provider_message_helper__WEBPACK_IMPORTED_MODULE_1__.Message }
];
GenericHttpClientService = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Injectable)({
        providedIn: 'root'
    })
], GenericHttpClientService);



/***/ }),

/***/ 5882:
/*!**********************************************!*\
  !*** ./src/app/common/pipes/orderby.pipe.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OrderbyPipe": () => (/* binding */ OrderbyPipe)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);


let OrderbyPipe = class OrderbyPipe {
    transform(items, direction, column, type) {
        let sortedItems = [];
        try {
            // console.log("items",items)
            // console.log("direction",direction)
            // console.log("column",column)
            // console.log("type",type)
            sortedItems = direction === "asc" ?
                this.sortAscending(items, column, type) :
                this.sortDescending(items, column, type);
        }
        catch (error) {
        }
        // setTimeout(() => {
        console.log("sortedItems", sortedItems[0]);
        return sortedItems[0];
        // }, 500);
    }
    sortAscending(items, column, type) {
        return [items.sort(function (a, b) {
                if (type === "string") {
                    if (a[column].toUpperCase() < b[column].toUpperCase())
                        return -1;
                }
                else {
                    return a[column] - b[column];
                }
            })];
    }
    sortDescending(items, column, type) {
        return [items.sort(function (a, b) {
                if (type === "string") {
                    if (a[column].toUpperCase() > b[column].toUpperCase())
                        return -1;
                }
                else {
                    return b[column] - a[column];
                }
            })];
    }
};
OrderbyPipe = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Pipe)({
        name: 'orderby'
    })
], OrderbyPipe);



/***/ }),

/***/ 22522:
/*!**********************************************!*\
  !*** ./src/app/common/pipes/pipes.module.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PipesModule": () => (/* binding */ PipesModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _round_pipe_pipe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./round-pipe.pipe */ 64262);
/* harmony import */ var _orderby_pipe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./orderby.pipe */ 5882);




let PipesModule = class PipesModule {
};
PipesModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        declarations: [_round_pipe_pipe__WEBPACK_IMPORTED_MODULE_0__.RoundPipePipe, _orderby_pipe__WEBPACK_IMPORTED_MODULE_1__.OrderbyPipe],
        imports: [],
        exports: [_round_pipe_pipe__WEBPACK_IMPORTED_MODULE_0__.RoundPipePipe, _orderby_pipe__WEBPACK_IMPORTED_MODULE_1__.OrderbyPipe],
    })
], PipesModule);



/***/ }),

/***/ 64262:
/*!*************************************************!*\
  !*** ./src/app/common/pipes/round-pipe.pipe.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RoundPipePipe": () => (/* binding */ RoundPipePipe)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);


let RoundPipePipe = class RoundPipePipe {
    transform(input) {
        return Number((input).toFixed(2));
    }
};
RoundPipePipe = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Pipe)({
        name: 'roundPipe'
    })
], RoundPipePipe);



/***/ }),

/***/ 15717:
/*!***************************************************!*\
  !*** ./src/app/custom-alert/custom-alert.page.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CustomAlertPage": () => (/* binding */ CustomAlertPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _custom_alert_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./custom-alert.page.html?ngResource */ 32672);
/* harmony import */ var _custom_alert_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./custom-alert.page.scss?ngResource */ 40906);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../neworder/neworder.service */ 17216);








let CustomAlertPage = class CustomAlertPage {
    constructor(formBuilder, modalCtrl, commonFunction, neworderservice, navParams) {
        this.formBuilder = formBuilder;
        this.modalCtrl = modalCtrl;
        this.commonFunction = commonFunction;
        this.neworderservice = neworderservice;
        this.TAG = "CustomAlertPage";
        this.showError = false;
        this.templateData = navParams.get('templateData');
    }
    ngOnInit() {
        this.alertFormGroup = this.formBuilder.group({
            txtTemplateName: [, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required]
        });
    }
    cancel() {
        try {
            this.modalCtrl.dismiss();
        }
        catch (error) {
            console.log(error);
        }
    }
    save() {
        try {
            this.showError = false;
            this.templateData.template_name = this.alertFormGroup.get('txtTemplateName').value ? this.alertFormGroup.get('txtTemplateName').value : '';
            console.log(this.TAG, this.templateData.template_name);
            this.neworderservice.SaveTemplate(this.templateData).subscribe(data => {
                this.response = data;
                if (data != null) {
                    if (data.resposemsg == "success") {
                        console.log(this.TAG, data.resposemsg);
                        this.modalCtrl.dismiss(data);
                    }
                    else {
                        this.showError = true;
                        //  this.commonFunction.presentAlert("message", "Fail", data.logmsg);
                    }
                }
            }, error => {
                this.commonFunction.loadingDismiss();
                this.commonFunction.presentAlert("Message", "Error!", error.error.text);
            });
        }
        catch (error) {
            console.log(error);
        }
    }
};
CustomAlertPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormBuilder },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ModalController },
    { type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_2__.Commonfun },
    { type: _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_3__.NeworderService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavParams }
];
CustomAlertPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-custom-alert',
        template: _custom_alert_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_custom_alert_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], CustomAlertPage);



/***/ }),

/***/ 66825:
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPage": () => (/* binding */ LoginPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _login_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page.html?ngResource */ 41729);
/* harmony import */ var _login_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.page.scss?ngResource */ 87047);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _loginauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./loginauth.service */ 44010);
/* harmony import */ var _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../neworder/neworder.service */ 17216);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../provider/message-helper */ 98792);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_native_fingerprint_aio_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/fingerprint-aio/ngx */ 63427);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ 80190);
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! crypto-js */ 36240);
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _common_Constants__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../common/Constants */ 68209);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var _ionic_native_market_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/market/ngx */ 68538);
















let LoginPage = class LoginPage {
  constructor(loginservc, msg, platform, router, neworderservc, fingerprint, storage, loadingController, alertController, genericHTTP, market, modalController) {
    this.loginservc = loginservc;
    this.msg = msg;
    this.platform = platform;
    this.router = router;
    this.neworderservc = neworderservc;
    this.fingerprint = fingerprint;
    this.storage = storage;
    this.loadingController = loadingController;
    this.alertController = alertController;
    this.genericHTTP = genericHTTP;
    this.market = market;
    this.modalController = modalController;
    this.TAG = "LoginPage";
    this.backgroundimg = './assets/ParekhImage.png';
    this.btnLogin = "Login";
    this.plcpassword = "Password";
    this.showForgotPassword = false;
    this.changePassword = false;
    this.showLoginButton = true;
    this.showGetOTPButton = false;
    this.showChangePassword = false;
    this.showFingerprintAuth = false;
    this.showCustomPassword = false;
    this.showFingerprintButton = false;
    this.showBiometericControl = false;
    this.showPinCodeControl = false;
    this.showFaceIdSection = false;
    this.isnewPinconfirm = false;
    this.loginrequired = false;
    this.fingerprintloginallowed = false;
    this.newuser = true;
    this.resetpin = true;
    this.secretKey = 'openbravoprism2';
    this.isipportvisible = true;
    this.isforgetpass = false;
    this.isclickonotp = false;
  }

  ngOnInit() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.loginservc.commonurl = _common_Constants__WEBPACK_IMPORTED_MODULE_9__.Constants.DOMAIN_URL + '/openbravo/';
      _this.bio_Codes = yield _this.storage.get('Biometric_Device_Codes');
      _this.pinCodes = yield _this.storage.get('PIN_Status');
      _this.logBiometricstatus = yield _this.storage.get('Biometric_Status');

      if (_this.bio_Codes == '200') {
        _this.authText = "Enable Fingerprint";
        _this.showFingerprintAuth = true;
        _this.showCustomPassword = false;
      } else if (_this.pinCodes == '200') {
        _this.authText = "Use Device Password,Pattern or PIN";
        _this.showFingerprintAuth = true;
        _this.showCustomPassword = false;
      }

      if (_this.bio_Codes == '-106' && _this.pinCodes != '200') {
        _this.authText = "Enable Fingerprint";
        _this.showFingerprintAuth = false;
        _this.showCustomPassword = true;
      }

      if (_this.bio_Codes == '-104' && _this.pinCodes != '200') {
        _this.authText = "Use Device Password,Pattern or PIN";
        _this.showFingerprintAuth = false;
        _this.showCustomPassword = true;
      }

      if (_this.bio_Codes == '200' && _this.pinCodes == '200') {
        _this.authText = "Enable Fingerprint";
        _this.showFingerprintAuth = true;
        _this.showCustomPassword = false;
      }

      if ((yield _this.storage.get('Biometric_Type')) == 'face' && ((yield _this.storage.get('user')) != undefined || (yield _this.storage.get('user')) != null) && (yield _this.storage.get('changePasswordFromSettingPage')) == false) {
        _this.showFingerprintAuth = false;
        _this.showFaceIdSection = true;
      }

      if (_this.showFingerprintAuth && (yield _this.storage.get('changePasswordFromSettingPage')) == false) {
        _this.onfingerPrintshow();
      } else if ((yield _this.storage.get('changePasswordFromSettingPage')) == false) {
        if (_this.bio_Codes == '-104' && _this.pinCodes != '200') {
          _this.showCustomPassword = true;
        }
      }

      _this.passcode = '';
      _this.message = true;
      _this.int = 0;
      _this.fingerPin = false;
      _this.isnewPinconfirm = false;
      _this.finalPin = '';

      _this.checkUserProfile();

      if (yield _this.storage.get('changePasswordFromSettingPage')) {
        //  this.storage.set('changePasswordFromSettingPage',false);
        _this.loginrequired = true;
        _this.newuser = true;

        _this.onClickForgetPass();
      }
    })();
  }

  ionViewWillEnter() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this2.storage.set('isNewRegistration', false);

      if (_this2.msg.isplatformweb == false) {
        _this2.platform.ready().then(() => {
          try {
            let url = _common_Constants__WEBPACK_IMPORTED_MODULE_9__.Constants.DOMAIN_URL + "/openbravo/ws/in.mbs.webservice.app_version?version_number=" + _common_Constants__WEBPACK_IMPORTED_MODULE_9__.Constants.VERSION_NO; // console.log("URL",url);

            _this2.genericHTTP.get(url, true, true).subscribe(versionResponse => {
              //  console.log(this.TAG, "version check response", versionResponse);
              if (!!versionResponse && versionResponse.update_required && (versionResponse.priority == "Major" || versionResponse.priority == "Minor")) {
                if (_this2.platform.is('ios')) {
                  _this2.presentAlertConfirmiOS(versionResponse);
                } else if (_this2.platform.is('android')) {
                  _this2.presentAlertConfirm(versionResponse);
                }
              } else if (!!versionResponse && versionResponse.update_required && versionResponse.priority == "ServerIsSmaller") {
                if (_this2.platform.is('ios')) {
                  _this2.presentServerAlertConfirmiOS();
                } else if (_this2.platform.is('android')) {
                  console.log(versionResponse);

                  _this2.presentServerAlertConfirm();
                }
              }
            });
          } catch (error) {// console.log(error);
          }
        });
      }

      _this2.pageStatus = 'Enter Your Pin to Login';

      _this2.checkUserProfile();

      if ((yield _this2.storage.get('Biometric_Type')) == 'face' && ((yield _this2.storage.get('user')) != undefined || (yield _this2.storage.get('user')) != null) && (yield _this2.storage.get('changePasswordFromSettingPage')) == false) {
        _this2.showFingerprintAuth = false;
        _this2.showFaceIdSection = true;
      }

      if (_this2.showFingerprintAuth && (yield _this2.storage.get('changePasswordFromSettingPage')) == false) {
        _this2.onfingerPrintshow();
      } else if ((yield _this2.storage.get('changePasswordFromSettingPage')) == false) {
        if (_this2.bio_Codes == '-104' && _this2.pinCodes != '200') {
          _this2.showCustomPassword = true;
        }
      }

      if (yield _this2.storage.get('changePasswordFromSettingPage')) {
        //  this.storage.set('changePasswordFromSettingPage',false);
        _this2.loginrequired = true;
        _this2.newuser = true;

        _this2.onClickForgetPass();
      }
    })();
  }

  checkDeviceAuth() {
    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {})();
  }

  checkUserProfile() {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this3.securityPinSaved = yield _this3.storage.get('Securitypin');
      _this3.logInRequired = yield _this3.storage.get('loginRequired');
      let changeFromSetting = yield _this3.storage.get('changePasswordFromSettingPage');

      if (_this3.msg.isplatformweb == false) {
        if (_this3.securityPinSaved && changeFromSetting == false) {
          _this3.loginrequired = false;
          _this3.newuser = false;
          _this3.message = false;
          _this3.pageStatus = 'Enter Your Pin to Login';
        } else if (_this3.logBiometricstatus && (yield _this3.storage.get('changePasswordFromSettingPage')) == false) {
          _this3.loginrequired = false;
          _this3.newuser = false;
        } else if (_this3.logInRequired != null && _this3.logInRequired != undefined && _this3.logInRequired != true && (yield _this3.storage.get('changePasswordFromSettingPage')) == false) {
          _this3.loginrequired = false;
          _this3.newuser = false;

          if (!_this3.securityPinSaved) {
            _this3.message = false;
            _this3.newuser = true;
            _this3.pageStatus = 'Please Setup Your Pin!';
          }
        } else {
          _this3.loginrequired = true;
          _this3.newuser = true;
        }
      } else {
        _this3.loginrequired = true;
        _this3.newuser = false;
      }
    })();
  }

  biometricStatusChange(event) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (event.detail.checked && (_this4.bio_Codes == '-106' || _this4.bio_Codes == '-104') && _this4.pinCodes != '200') {
        const alert = yield _this4.alertController.create({
          header: 'Login',
          subHeader: 'Security',
          message: 'Your device is not protected by password or biometric',
          buttons: ['OK']
        });
        yield alert.present();
        let result = yield alert.onDidDismiss();
        _this4.biometricstatus = false;
        _this4.logBiometricstatus = false;
      } else {
        _this4.storage.set('Biometric_Status', event.detail.checked);

        _this4.logBiometricstatus = event.detail.checked;
        _this4.showCustomPassword = false;
      }
    })();
  }

  encrypt(value) {
    return crypto_js__WEBPACK_IMPORTED_MODULE_8__.AES.encrypt(value, this.secretKey.trim()).toString();
  }

  decrypt(textToDecrypt) {
    return crypto_js__WEBPACK_IMPORTED_MODULE_8__.AES.decrypt(textToDecrypt, this.secretKey.trim()).toString(crypto_js__WEBPACK_IMPORTED_MODULE_8__.enc.Utf8);
  }

  onClickForgetPass() {
    this.btnLogin = "Get OTP";
    this.isforgetpass = true;
    this.plcpassword = "Enter New Password";
    this.isclickonotp = true;
    this.showGetOTPButton = true;
    this.showLoginButton = false;
  }

  onClickNewRegistration() {
    this.loginservc.isloginactive = false;

    try {
      let navigationExtras = {
        state: {
          isNewRegistration: true
        }
      };
      this.storage.set('isNewRegistration', true);
      this.router.navigate(['newcustomer'], navigationExtras);
    } catch (error) {}
  }

  onClickGetOTP() {
    this.isclickonotp = true;
    this.showGetOTPButton = false;
    this.showChangePassword = true;

    if (this.username) {
      this.isforgetpass = true;
      this.loadingController.create({
        duration: 5000,
        spinner: 'circles',
        message: 'Please Wait...'
      }).then(res => {
        res.present();
      });
      this.loginservc.getmobileno(this.username).subscribe(data => {
        this.response = data['response'];

        if (this.response.messageType === 'success') {
          this.mobileno = this.response.messageText;
          this.txterror = 'OTP Will Send To ' + this.response.messageText;
          this.loadingController.dismiss();
          this.loadingController.create({
            duration: 5000,
            spinner: 'circles',
            message: 'Please Wait...'
          }).then(res => {
            res.present();
          });
          this.loginservc.forgetuser(this.username).subscribe(data => {
            this.response = data['response'];

            if (this.response.messageType !== undefined) {
              if (this.response.messageType === 'success') {
                this.txterror = this.response.messageText;
                this.isclickonotp = true;
                this.loadingController.dismiss();
              }
            } else {
              this.txterror = this.response.error.message;
            }
          }, error => {
            this.txterror = error.message;
          });
        }
      }, error => {
        this.txterror = error.message;
      });
    } else {
      this.txterror = 'Please Enter User Name.';
    }
  }

  onChangePass() {
    this.loadingController.create({
      duration: 5000,
      spinner: 'circles',
      message: 'Verifying OTP...'
    }).then(res => {
      res.present();
    });
    this.loginservc.changepassword(this.username, this.newpass, this.otp).subscribe(data => {
      this.response = data['response']; //  console.log(this.TAG,"changepassword",this.response);

      if (this.response.messageType !== undefined) {
        if (this.response.messageType === 'success') {
          this.txterror = this.response.messageText; // after changing pass need to compulsory setup new pin and login

          this.storage.set('changePasswordFromSettingPage', false);
          this.loginrequired = true;
          this.showLoginButton = true;
          this.showChangePassword = false;
          this.changePassword = false;
          this.newuser = true;
          this.isforgetpass = false;
          this.fingerprintloginallowed = false; //  
        }
      } else {
        this.txterror = this.response.error.message;
      }

      this.loadingController.dismiss();
    }, error => {
      this.txterror = error.message;
    });
  }

  loginSubmit() {
    // console.log(this.storage);
    if (!this.isforgetpass) {
      this.login();
    } else {
      this.showForgotPassword = true;
      this.changePassword = true;
    }
  }

  checkApprovalScreenAccess(url) {
    this.loginservc.checkApprovalScreenAccess().subscribe(appData => {
      // console.log("Pravin Approval Data", appData);
      if (!!appData) {
        //  this.loginservc.approvalScreen = true;
        this.router.navigateByUrl(url);
      } else {
        this.loginservc.approvalScreen = false; // this.router.navigateByUrl(url);
      }
    });
  }

  login() {
    this.loadingController.create({
      spinner: 'circles',
      message: 'Please Wait...'
    }).then(res => {
      res.present();
    });
    this.txterror = '';
    this.storage.set('ipport', this.loginservc.commonurl);
    this.storage.remove('user');
    this.storage.remove('pass');
    this.loginservc.login(this.username, this.password).subscribe(resp => {
      // console.log("login.html", resp); 
      if (resp.showMessage) {
        this.loadingController.dismiss();
        this.txterror = resp.messageText;
        this.txthint = 'Hint: ' + resp.messageTitle;
        this.loginrequired = true;
        this.newuser = true;
      } else {
        this.txterror = "Success";
        this.loginservc.isloginactive = true;
        this.loginservc.getdefaultprofile().subscribe(data => {
          this.loginservc.defaultprofile = [data.response.data[0]];
          this.loginservc.userid = this.loginservc.defaultprofile[0].id;
          this.loginservc.insertuserlog(this.loginservc.userid).subscribe(data1 => {
            if (this.newuser && (!this.logBiometricstatus || this.logBiometricstatus == null || this.logBiometricstatus == undefined)) {
              if (!this.msg.isplatformweb) {
                this.loginrequired = false;
                this.message = false;
                this.showCustomPassword = true;
                this.pageStatus = 'Please Setup Your Pin!';
                this.loadingController.dismiss();
              } else {
                if (!!this.username && this.password) {
                  this.storage.set('user', this.encrypt(this.username));
                  this.storage.set('pass', this.encrypt(this.password));
                }

                this.storage.set('loginRequired', false);
                this.mmstOrdercaptureapp = this.loginservc.defaultprofile[0].mmstOrdercaptureapp;

                if (!this.msg.isplatformweb) {
                  if (this.loginservc.defaultprofile[0].mmstOrdercaptureapp == true && this.loginservc.defaultprofile[0].mmstReward == false) {
                    this.loginrequired = false;
                    this.message = false;
                    this.loadingController.dismiss();
                    this.loginservc.logintype = "wms";
                    this.router.navigate(['terms-of-agreement']);
                  } else if (this.loginservc.defaultprofile[0].mmstOrdercaptureapp == false && this.loginservc.defaultprofile[0].mmstReward == true) {
                    this.loadingController.dismiss();
                    this.loginrequired = false;
                    this.message = false;
                    this.loginservc.logintype = "vet";
                    this.router.navigateByUrl('/use-vetcoins');
                  } else {
                    this.loadingController.dismiss();
                    this.loginrequired = false;
                    this.message = false;
                    this.router.navigateByUrl('/login-option');
                  }
                } else {
                  setTimeout(() => {
                    if (this.mmstOrdercaptureapp == true) {
                      this.loginservc.logintype = 'wms';
                      this.loginrequired = false;
                      this.message = false;
                      this.router.navigate(['terms-of-agreement']);
                    } else {
                      this.loginservc.defaultprofile[0].mmstReward = false;
                      this.loginrequired = false;
                      this.message = false;
                      this.router.navigateByUrl('/login-option');
                    }
                  }, 1500);
                  this.loadingController.dismiss();
                }
              }
            } else {
              if (!!this.username && this.password) {
                this.storage.set('user', this.encrypt(this.username));
                this.storage.set('pass', this.encrypt(this.password));
              }

              this.storage.set('loginRequired', false);
              this.mmstOrdercaptureapp = this.loginservc.defaultprofile[0].mmstOrdercaptureapp;

              if (!this.msg.isplatformweb) {
                if (this.loginservc.defaultprofile[0].mmstOrdercaptureapp == true && this.loginservc.defaultprofile[0].mmstReward == false) {
                  this.loadingController.dismiss();
                  this.loginservc.logintype = "wms";
                  this.router.navigate(['terms-of-agreement']);
                } else if (this.loginservc.defaultprofile[0].mmstOrdercaptureapp == false && this.loginservc.defaultprofile[0].mmstReward == true) {
                  this.loadingController.dismiss();
                  this.loginservc.logintype = "vet";
                  this.router.navigateByUrl('/use-vetcoins');
                } else {
                  this.loadingController.dismiss();
                  this.router.navigateByUrl('/login-option');
                }
              } else {
                setTimeout(() => {
                  if (this.mmstOrdercaptureapp == true) {
                    this.loginservc.logintype = 'wms';
                    this.router.navigate(['terms-of-agreement']);
                  } else {
                    this.loginservc.defaultprofile[0].mmstReward = false;
                    this.router.navigateByUrl('/login-option');
                  }
                }, 1500);
                this.loadingController.dismiss();
              }
            }
          });
        });
        this.txterror = '';
        this.txthint = 'Success';
      }

      if (this.resetPin) {
        this.passcode = '';
        this.resetpin = false;
        this.codeone = null;
        this.codetwo = null;
        this.codethree = null;
        this.codefour = null;
        this.int = 0;
        this.passcode = '';
      } // this.loadingController.dismiss();

    }, error => {
      try {
        // this.txterror = error.message;
        if (error.message.includes("password=", 0)) {
          var x = error.message.split("password=");
          var y = x[1].split(":");
          this.txterror = x[0] + "password=******: " + y[1];
        } else {
          this.txterror = error.message;
        }
      } catch (error) {// console.log(error)
      }
    });
  }

  onfingerPrintshow() {
    var _this5 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this5.biometricstatus = yield _this5.storage.get('Biometric_Status');

      if (_this5.biometricstatus && !!_this5.biometricstatus) {
        _this5.fingerprint.show({
          title: 'Biometric Authentication',
          subtitle: 'Coolest Plugin ever',
          description: 'Please authenticate',
          fallbackButtonTitle: 'Use Backup',
          disableBackup: false
        }).then(result => {
          _this5.storage.get('user').then(user => {
            _this5.username = _this5.decrypt(user); //  
          });

          _this5.storage.get('pass').then(pass => {
            _this5.password = _this5.decrypt(pass); // this.isnewPinconfirm = true;

            _this5.newuser = false;

            _this5.storage.set('loginRequired', false);

            _this5.login(); //

          });
        }).catch(error => {
          if (_this5.biometricstatus && error.code == "-108") {
            // this.showCustomPassword = false;
            // this.loginrequired  = true;
            // this.storage.set('loginRequired', true);
            navigator["app"].exitApp();
          }
        });
      } else {
        _this5.showCustomPassword = true;
      }
    })();
  }

  add(value) {
    //  
    this.storage.get('Securitypin').then(Securitypin => {
      if (Securitypin) {
        this.finalPin = this.decrypt(Securitypin);
      } //  


      if (this.passcode.length < 4) {
        this.passcode = this.passcode + value;

        if (this.int === 0) {
          this.codeone = value;
          this.int++;
        } else if (this.int === 1) {
          this.codetwo = value;
          this.int++;
        } else if (this.int === 2) {
          this.codethree = value;
          this.int++;
        } else if (this.int === 3) {
          this.codefour = value;

          if (this.isnewPinconfirm && this.newuser) {
            this.newPin = this.codeone + this.codetwo + this.codethree + this.codefour;

            if (this.newPin === this.passcode) {
              this.message = false;
              this.pageStatus = 'Pin Confirmed!';
              this.storage.set('Securitypin', this.encrypt(this.passcode));

              if (!!this.loginservc.user && this.loginservc.pass) {
                this.storage.set('user', this.encrypt(this.loginservc.user));
                this.storage.set('pass', this.encrypt(this.loginservc.pass));
                this.username = this.loginservc.user;
                this.password = this.loginservc.pass;
              }

              this.isnewPinconfirm = true;
              this.newuser = false; // 

              this.storage.set('loginRequired', false); // login

              this.login();
              return;
            }
          }

          if (this.newuser) {
            this.passcode = this.codeone + this.codetwo + this.codethree + this.codefour;
            this.message = false;
            this.pageStatus = 'Please Confirm Pin.';
            this.codeone = null;
            this.codetwo = null;
            this.codethree = null;
            this.codefour = null;
            this.int = 0;
            this.passcode = '';
            this.isnewPinconfirm = true;
          } else {
            if (this.finalPin === this.codeone + this.codetwo + this.codethree + this.codefour) {
              // 
              this.message = false;
              this.pageStatus = 'Password Matched.';
              this.storage.get('user').then(user => {
                this.username = this.decrypt(user); // 
              });
              this.storage.get('pass').then(pass => {
                this.password = this.decrypt(pass);
                this.message = false;
                this.pageStatus = '';
                this.storage.set('loginRequired', false);
                this.login(); //  
              }); // login
            } else {
              this.message = false;
              this.resetpin = true;
              this.pageStatus = 'Please Enter Correct Pin.';

              if (this.passcode.length > 0) {
                this.codeone = null;
                this.codetwo = null;
                this.codethree = null;
                this.codefour = null;
                this.int = 0;
                this.passcode = ''; // this.message = true;
              }
            }
          }
        }
      }
    });
  }

  resetPin() {
    this.loginrequired = true;
    this.newuser = true;
    this.password = '';
  }

  delete() {
    if (this.passcode.length > 0) {
      this.codeone = null;
      this.codetwo = null;
      this.codethree = null;
      this.codefour = null;
      this.int = 0;
      this.passcode = '';
      this.message = true;
    }
  }

  presentAlertConfirm(versionResponse) {
    var _this6 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      let ifData;
      let backDropDismiss = true;

      if (!!versionResponse && versionResponse.priority == "Major") {
        backDropDismiss = false;
        ifData = {
          text: 'Exit App',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {
            //console.log('Confirm Cancel: blah');
            navigator["app"].exitApp();
          }
        };
      } else {
        backDropDismiss = true;
        ifData = {
          text: 'NO THANKS',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {// console.log('Confirm Cancel: blah');
          }
        };
      }

      const alert = yield _this6.alertController.create({
        // cssClass: 'my-custom-class',
        header: 'New Version Update available',
        message: '<h6>Please click Update to download latest Version.</h6>',
        buttons: [ifData, {
          text: 'UPDATE',
          handler: () => {
            //  console.log('Confirm Okay');
            navigator["app"].exitApp();

            _this6.market.open('com.multilinetech.OBPrism2');
          }
        }],
        backdropDismiss: backDropDismiss
      });
      yield alert.present();
    })();
  }

  presentAlertConfirmiOS(versionResponse) {
    var _this7 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      let ifData;
      let backDropDismiss = true;

      if (!!versionResponse && versionResponse.priority == "Major") {
        backDropDismiss = false;
        ifData = {
          text: 'Close The App',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {//console.log('Confirm Cancel: blah');
          }
        };
      } else {
        backDropDismiss = true;
        ifData = {
          text: 'NO THANKS',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {//console.log('Confirm Cancel: blah');
          }
        };
      }

      const alert = yield _this7.alertController.create({
        // cssClass: 'my-custom-class',
        header: 'New Version Update available',
        message: '<h6>Please click Update to download latest Version.</h6>',
        buttons: [ifData, {
          text: 'UPDATE',
          handler: () => {
            //console.log('Confirm Okay');
            navigator["app"].exitApp();

            _this7.market.open('com.multilinetech.OBPrism2');
          }
        }],
        backdropDismiss: backDropDismiss
      });
      yield alert.present();
    })();
  }

  presentServerAlertConfirm() {
    var _this8 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const alert = yield _this8.alertController.create({
          // cssClass: 'my-custom-class',
          header: 'This version is not supported by server',
          message: '<h6>Please contact your system administrator.</h6>',
          buttons: [{
            text: 'Exit App',
            role: 'cancel',
            cssClass: 'secondary',
            handler: blah => {
              // console.log('Confirm Cancel: blah');
              navigator["app"].exitApp();
            }
          }],
          backdropDismiss: false
        });
        yield alert.present();
      } catch (error) {//console.log(error);
      }
    })();
  }

  presentServerAlertConfirmiOS() {
    var _this9 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this9.alertController.create({
        // cssClass: 'my-custom-class',
        header: 'This version is not supported by server',
        message: '<h6>Please contact your system administrator.</h6>',
        buttons: [{
          text: 'Close The APP',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {//console.log('Confirm Cancel: blah');
          }
        }],
        backdropDismiss: false
      });
      yield alert.present();
    })();
  }

  catch(error) {//console.log(error);
  }

};

LoginPage.ctorParameters = () => [{
  type: _loginauth_service__WEBPACK_IMPORTED_MODULE_3__.LoginauthService
}, {
  type: _provider_message_helper__WEBPACK_IMPORTED_MODULE_5__.Message
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.Platform
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_13__.Router
}, {
  type: _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_4__.NeworderService
}, {
  type: _ionic_native_fingerprint_aio_ngx__WEBPACK_IMPORTED_MODULE_6__.FingerprintAIO
}, {
  type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__.Storage
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.LoadingController
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.AlertController
}, {
  type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_10__.GenericHttpClientService
}, {
  type: _ionic_native_market_ngx__WEBPACK_IMPORTED_MODULE_11__.Market
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.ModalController
}];

LoginPage = (0,tslib__WEBPACK_IMPORTED_MODULE_14__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_15__.Component)({
  selector: 'app-login',
  template: _login_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_login_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], LoginPage);


/***/ }),

/***/ 44010:
/*!********************************************!*\
  !*** ./src/app/login/loginauth.service.ts ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginauthService": () => (/* binding */ LoginauthService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _common_Constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common/Constants */ 68209);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ 92218);






let LoginauthService = class LoginauthService {
    constructor(http, genericHTTP) {
        this.http = http;
        this.genericHTTP = genericHTTP;
        this.commonurl = _common_Constants__WEBPACK_IMPORTED_MODULE_0__.Constants.DOMAIN_URL + '/openbravo/';
        this.isexpenseaccess = false;
        this.isschemeinfo = false;
        this.isNewLead = false;
        this.isExistingLead = false;
        this.isBusinessPartnerAddress = false;
        this.isNewOrder = false;
        this.isDraftOrder = false;
        this.isOrderStatus = false;
        this.isLatLongFinder = false;
        this.isTravelPlan = false;
        this.isActualTravelPlan = false;
        this.isTravelExpense = false;
        this.isTravelPlanClosure = false;
        this.isApprovalAccess = false;
        this.isCustomerServiceAccess = false;
        this.isComplaintReportingAccess = false;
        this.isCompliantAcceptanceAccess = false;
        this.isFieldVisitAccess = false;
        this.isQuotationAccess = false;
        this.isQuotationApproval = false;
        this.isUpload = false;
        this.isReport = false;
        this.isMPRFormAccess = false;
        this.isConsolidationOrder = false;
        this.isARVisitSchedule = false;
        this.dashboard = false;
        this.logoimgeBase64 = '';
        this.selectedactivity = null;
        this.minlistcount = 10;
        this.mindatetravelplan = 7;
        this.isloginactive = false;
        this.approvalScreen = false;
        this.Discount_Method = '';
        this.appPages = [
            {
                title: 'New Lead',
                url: '/newcustomer',
                icon: 'person-add'
            },
            {
                title: 'Existing Lead',
                url: '/existingcustomer',
                icon: 'person'
            },
            {
                title: 'New Order',
                url: '/neworder',
                icon: 'clipboard'
            },
            {
                title: 'Existing Order',
                url: '/existingorder',
                icon: 'list-box'
            },
            {
                title: 'Order Status',
                url: '/order-status',
                icon: 'stats'
            },
            {
                title: 'Business Partner Address',
                url: '/business-partner-address',
                icon: 'clipboard'
            },
            {
                title: 'Settings',
                url: '/settings',
                icon: 'settings'
            },
            {
                title: 'Logout',
                url: '/login',
                icon: 'log-out'
            }
        ];
        this.OldappPages = [
            {
                title: 'New Lead',
                url: '/newcustomer',
                icon: 'person-add'
            },
            {
                title: 'Existing Lead',
                url: '/existingcustomer',
                icon: 'person'
            },
            {
                title: 'New Order',
                url: '/neworder',
                icon: 'clipboard'
            },
            {
                title: 'Existing Order',
                url: '/existingorder',
                icon: 'list-box'
            },
            {
                title: 'Order Status',
                url: '/order-status',
                icon: 'stats'
            },
            {
                title: 'Business Partner Address',
                url: '/business-partner-address',
                icon: 'clipboard'
            },
            {
                title: 'Settings',
                url: '/settings',
                icon: 'settings'
            },
            {
                title: 'Logout',
                url: '/login',
                icon: 'log-out'
            }
        ];
        this.reportjson = {};
        this.iscutsomdefined = false;
        this.eventData = new rxjs__WEBPACK_IMPORTED_MODULE_2__.Subject();
    }
    publishSomeData(data) {
        this.eventData.next(data);
    }
    getObservable() {
        return this.eventData;
    }
    getreportdet(roleid, type, reportcatid, reportsubcatid) {
        return this.genericHTTP.get(this.commonurl + 'ws/in.mbs.exportdata.MEXDGetReportCategory?'
            + 'roleid=' + roleid + '&type=' + type + '&reportcatid=' + reportcatid + '&reportsubcatid=' + reportsubcatid);
    }
    login(user, pass) {
        this.parameter = 'user=' + user + '&password=' + pass + '&isextapp=Y';
        this.user = user;
        this.pass = pass;
        this.genericHTTP.user = user;
        this.genericHTTP.pass = pass;
        return this.genericHTTP.get(this.commonurl + 'secureApp/LoginHandler.html?'
            + this.parameter);
        // const auth=btoa(user+":"+pass);
        // const httpOptions = {
        //   headers: new HttpHeaders({
        //     'Content-Type':'application/json',
        //     'Accept':'application/json',
        //   'Authorization':'Basic '+auth
        //     })
        //   };
        //  return this.genericHTTP.post(this.commonurl + 'secureApp/LoginHandler.html?' + this.parameter
        //  ,{}, httpOptions);
    }
    insertuserlog(userid) {
        return this.genericHTTP.get(this.commonurl + 'ws/in.mbs.webservice.WMobileUserLogService?'
            + 'userid=' + userid);
    }
    forgetuser(username) {
        return this.genericHTTP.get(this.commonurl
            + 'ws/in.mbs.webservice.MobileOtpService?user=ionic.appuser&password=ionic.appuser&action=forgetpass&username='
            + username);
    }
    getmobileno(username) {
        return this.genericHTTP.get(this.commonurl
            + 'ws/in.mbs.webservice.MobileOtpService?user=ionic.appuser&password=ionic.appuser&action=getmobileno&username='
            + username);
    }
    changepassword(username, newpass, otp) {
        return this.genericHTTP.get(this.commonurl
            + 'ws/in.mbs.webservice.MobileOtpService?user=ionic.appuser&password=ionic.appuser&action=changepass&username='
            + username + '&newpass=' + newpass + '&otp=' + otp);
    }
    getdefaultprofile() {
        return this.genericHTTP.get(this.commonurl + 'org.openbravo.service.json.jsonrest/ADUser?'
            + '_selectedProperties=id,name,email,defaultClient,defaultClient$_identifier,defaultOrganization,'
            + 'defaultOrganization$_identifier,mmstOrderusrtype,defaultRole,defaultRole$_identifier,defaultWarehouse,defaultWarehouse$_identifier,'
            + 'mmstReward,mmstOrdercaptureapp&'
            + '_where=username=\'' + this.user + '\'');
    }
    getrolelist(userid) {
        return this.genericHTTP.get(this.commonurl + 'org.openbravo.service.json.jsonrest/ADUserRoles?'
            + '_selectedProperties=role,role$_identifier,mmsaSecurerule,userContact&'
            + '_where=userContact=\'' + userid + '\'');
    }
    getuserorg(userid, mmsaSecurerule) {
        return this.genericHTTP.get(this.commonurl + 'org.openbravo.service.json.jsonrest/MMSA_UserOrg?'
            + '_selectedProperties=organization,organization$_identifier&_sortBy=name&'
            + '_where=user=\'' + userid + '\'%20and%20mmsaSecurerule=\'' + mmsaSecurerule + '\'');
    }
    getorgwarehouse(orgid) {
        return this.genericHTTP.get(this.commonurl + 'org.openbravo.service.json.jsonrest/Warehouse?'
            + '_selectedProperties=id,_identifier&_sortBy=_identifier&'
            + '_where=organization=\'' + orgid + '\'');
        // 
    }
    setdefaultprofile(selprof) {
        this.selprofile = selprof;
        // 
    }
    GetReportPage(jsonreport) {
        let login = this.user; //"P2admin";
        let password = this.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.commonurl + 'ws/in.mbs.exportdata.MEXDGetReportParaPage', jsonreport, httpOptions);
    }
    getReportPara(rptid) {
        return this.genericHTTP.get(this.commonurl + 'ws/in.mbs.exportdata.MEXDGetReportPara?'
            + 'rptid=' + rptid);
    }
    sendEmailReport(jsonreport) {
        let login = this.user; //"P2admin";
        let password = this.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.commonurl + 'ws/in.mbs.exportdata.MEXDEmailAction', jsonreport, httpOptions);
    }
    downloadExcelPDF(jsonreport) {
        return this.genericHTTP.postformdataDownload(this.commonurl + 'ws/in.mbs.exportdata.MEXDEmailAction', jsonreport);
    }
    getbomlist() {
        return this.genericHTTP.get(this.commonurl + 'org.openbravo.service.json.jsonrest/mbom_bom?'
            + '_selectedProperties=id,_identifier');
    }
    getjoblist(userid) {
        return this.genericHTTP.get(this.commonurl + 'org.openbravo.service.json.jsonrest/mwms_joblist?'
            + '_where=reject=false and mmstScomplete=false and mwmsJobassignment.user=\'' + userid + '\'');
    }
    getjob(jobid) {
        return this.genericHTTP.get(this.commonurl + 'org.openbravo.service.json.jsonrest/mwms_joblist?'
            + '_where=id=\'' + jobid + '\'');
    }
    getjobassignment(jobassignmentid) {
        return this.genericHTTP.get(this.commonurl + 'org.openbravo.service.json.jsonrest/mwms_jobassignment?'
            + '_where=id=\'' + jobassignmentid + '\'');
    }
    getuserlist(jobtypeid) {
        return this.genericHTTP.get(this.commonurl + 'org.openbravo.service.json.jsonrest/mwms_wmsuser?'
            + '_selectedProperties=user,$user$_identifier&'
            + '_where=mwmsJobtype=\'' + jobtypeid + '\'');
    }
    acceptJob(joblistid) {
        return this.genericHTTP.get(this.commonurl + 'ws/in.mbs.webservice.WMobileJobListRequest?'
            + 'joblistid=' + joblistid + '&accept=true');
    }
    rejectJob(joblistid) {
        return this.genericHTTP.get(this.commonurl + 'ws/in.mbs.webservice.WMobileJobListRequest?'
            + 'joblistid=' + joblistid + '&reject=true');
    }
    transferJob(joblistid, userid, reason) {
        return this.genericHTTP.get(this.commonurl + 'ws/in.mbs.webservice.WMobileJobListRequest?'
            + 'joblistid=' + joblistid + '&transfer=true&userid=' + userid + '&reason=' + reason);
    }
    getProductlistFromBatch(batch) {
        return this.genericHTTP.get(this.commonurl + 'ws/in.mbs.webservice.WMobileTask?'
            + 'batch=' + batch);
    }
    insertJobDetails(joblistid, productmovtype, batch, productid, qty, binno, tobinno, batchid, iscomplete) {
        // 
        return this.genericHTTP.get(this.commonurl + 'ws/in.mbs.webservice.WMobileJoblistDetails?'
            + 'joblistid=' + joblistid + '&productmovtype=' + productmovtype
            + '&batch=' + batch + '&productid=' + productid + '&qty=' + qty
            + '&binno=' + binno + '&tobinno=' + tobinno + '&batchid=' + batchid + '&iscomplete=' + iscomplete);
    }
    getuserwiseactivity(user_id) {
        return this.genericHTTP.get(this.commonurl + 'ws/in.mbs.webservice.WMobileUserWiseOrgActivity?'
            + 'userid=' + user_id);
    }
    getAllActivity() {
        return this.genericHTTP.get(this.commonurl + 'org.openbravo.service.json.jsonrest/mmst_org_act?'
            + '_selectedProperties=id,_identifier&');
    }
    getAllActivity1(organization) {
        return this.genericHTTP.get(this.commonurl + 'org.openbravo.service.json.jsonrest/Organization?'
            + '_selectedProperties= mmstOrgAct$_identifier,mmstOrgAct&'
            + '_where=id IN (' + organization + ')');
    }
    getuserorgwise(userid) {
        return this.genericHTTP.get(this.commonurl + 'org.openbravo.service.json.jsonrest/MMSA_UserOrg?'
            + '_selectedProperties=organization,organization$_identifier&'
            + '_where=user=\'' + userid + '\'');
    }
    getexistcustmer(selectedactivity, searchkey) {
        return this.genericHTTP.get(this.commonurl + 'org.openbravo.service.json.jsonrest/mmst_customer?'
            //+ '_selectedProperties=id,_identifier,Sfname,Slname&' 
            + '_where=scomplete=false and mmstOrgAct=\'' + selectedactivity + '\' and createdBy=\'' + this.userid + '\''
            + 'and ((lower(translate(sfirmName,\' \',\'\')) LIKE lower(\'%25' + searchkey + '%25\'))'
            + 'or (lower(translate(sfname,\' \',\'\')) LIKE lower(\'%25' + searchkey + '%25\'))'
            + 'or (lower(translate(slname,\' \',\'\')) LIKE lower(\'%25' + searchkey + '%25\')))');
    }
    getexistcustmerapi(selectedactivity, searchkey) {
        // 
        return this.genericHTTP.get(this.commonurl + 'ws/in.mbs.webservice.WMobileActivityWiseLead?'
            + 'user_id=' + this.userid
            + '&strsearch=' + searchkey
            + '&activity_id=' + selectedactivity);
    }
    checkApprovalScreenAccess() {
        this.getrolelist(this.userid);
        return this.genericHTTP.get(this.commonurl + 'ws/in.mbs.webservice.OrderStatusApprove?'
            + 'user_id=' + this.userid);
    }
};
LoginauthService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpClient },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService }
];
LoginauthService = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable)({
        providedIn: 'root'
    })
], LoginauthService);



/***/ }),

/***/ 63806:
/*!************************************!*\
  !*** ./src/app/material.module.ts ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MaterialModule": () => (/* binding */ MaterialModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/table */ 85288);
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/stepper */ 44193);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/button */ 84522);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/form-field */ 75074);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/input */ 68562);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/icon */ 57822);
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/select */ 57371);
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/paginator */ 36060);
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/sort */ 92197);








// import {MatOptionModule} from "@angular/material/";



let MaterialModule = class MaterialModule {
};
MaterialModule = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.NgModule)({
        exports: [
            _angular_material_table__WEBPACK_IMPORTED_MODULE_2__.MatTableModule,
            _angular_material_stepper__WEBPACK_IMPORTED_MODULE_3__.MatStepperModule,
            _angular_material_button__WEBPACK_IMPORTED_MODULE_4__.MatButtonModule,
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__.MatFormFieldModule,
            _angular_material_input__WEBPACK_IMPORTED_MODULE_6__.MatInputModule,
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__.MatIconModule,
            // MatOptionModule,
            _angular_material_select__WEBPACK_IMPORTED_MODULE_8__.MatSelectModule,
            _angular_material_paginator__WEBPACK_IMPORTED_MODULE_9__.MatPaginatorModule,
            _angular_material_sort__WEBPACK_IMPORTED_MODULE_10__.MatSortModule
        ]
    })
], MaterialModule);



/***/ }),

/***/ 10990:
/*!*******************************************!*\
  !*** ./src/app/neworder/neworder.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NeworderPage": () => (/* binding */ NeworderPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _neworder_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./neworder.page.html?ngResource */ 88716);
/* harmony import */ var _neworder_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./neworder.page.scss?ngResource */ 7392);
/* harmony import */ var _product_list_product_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../product-list/product-list.page */ 47214);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _neworder_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./neworder.service */ 17216);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _addeditproduct_addeditproduct_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../addeditproduct/addeditproduct.service */ 53899);
/* harmony import */ var _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @awesome-cordova-plugins/camera/ngx */ 64587);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../provider/message-helper */ 98792);
/* harmony import */ var _upload_upload_upload_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../upload/upload/upload.service */ 57883);
/* harmony import */ var _ionic_native_file_chooser_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/file-chooser/ngx */ 86786);
/* harmony import */ var _awesome_cordova_plugins_file_path_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @awesome-cordova-plugins/file-path/ngx */ 3501);
/* harmony import */ var _ionic_native_file_picker_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/file-picker/ngx */ 63507);
/* harmony import */ var _common_Constants__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../common/Constants */ 68209);
/* harmony import */ var _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @awesome-cordova-plugins/file-transfer/ngx */ 80464);
/* harmony import */ var _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @awesome-cordova-plugins/file/ngx */ 25453);
/* harmony import */ var _custom_alert_custom_alert_page__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../custom-alert/custom-alert.page */ 15717);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! date-fns */ 86527);

























let NeworderPage = class NeworderPage {
  constructor(neworderservice, router, route, fb, loginauth, alertCtrl, commonfun, addeditproductservice, camera, platform, msg, uploadService, fileChooser, filePath, filePicker, modalController, transfer, file) {
    //this.addprod();
    this.neworderservice = neworderservice;
    this.router = router;
    this.route = route;
    this.fb = fb;
    this.loginauth = loginauth;
    this.alertCtrl = alertCtrl;
    this.commonfun = commonfun;
    this.addeditproductservice = addeditproductservice;
    this.camera = camera;
    this.platform = platform;
    this.msg = msg;
    this.uploadService = uploadService;
    this.fileChooser = fileChooser;
    this.filePath = filePath;
    this.filePicker = filePicker;
    this.modalController = modalController;
    this.transfer = transfer;
    this.file = file;
    this.TAG = "New Order Page";
    this.today = new Date().toJSON().split('T')[0];
    this.reftextcount = 0;
    this.items = [];
    this.ishidden = false;
    this.buttonName = '';
    this.activity = '';
    this.Iscartproduct = false;
    this.POimg64 = '';
    this.edtitdocid = '';
    this.Isdruglicence = false;
    this.isdisabledCustomer = true;
    this.showhideRedeemReward = true;
    this.Isshowtemplate = false;
    this.Showoverdue = true;
    this.showcrlimit = false;
    this.checkboxtab = true;
    this.orderlevelper = "0";
    this.isdesktop = false;
    this.istriggeronOrderTypeChange = true;
    this.showExpectedDeliveryDateCtrl = false;
    this.expireDateEdit = false;
    this.isPrimaryCustomer = true;
    this.sp_order_chk_box = false;
    this.is_advance_payment = false;
    this.advance_payment_chk_status = false;
    this.dateexpire = '';
    this.validation_messages = {
      'selectedBusinessPartner': [{
        type: 'required',
        message: ' *Please Select Customer.'
      }],
      'selectedBPaddressshipping': [{
        type: 'required',
        message: '*Please Select Shipping Address.'
      }],
      'selectedBPaddressbilling': [{
        type: 'required',
        message: '*Please Select Billing Address.'
      }],
      'selectedordertype': [{
        type: 'required',
        message: '*Please Select Order Type.'
      }]
    };
    this.formprod = this.fb.group({
      selectedBusinessPartner: [, _angular_forms__WEBPACK_IMPORTED_MODULE_18__.Validators.required],
      selectedBPaddressshipping: [, _angular_forms__WEBPACK_IMPORTED_MODULE_18__.Validators.required],
      selectedBPaddressbilling: [, _angular_forms__WEBPACK_IMPORTED_MODULE_18__.Validators.required],
      CreditLimit: [],
      OverdueInvoice: [],
      Overdueamount: [],
      duedatedays: [],
      selectedordertype: [, _angular_forms__WEBPACK_IMPORTED_MODULE_18__.Validators.required],
      selectedBPtemplate: [],
      ponumber: [],
      avaliableRedeem: [],
      tobeRedeem: [],
      expecteddeliverydate: [],
      orderdescription: [],
      specialOrderCtrl: [],
      selectedPrimaryBusinessPartner: [],
      isAdvancePaymentChk: [false]
    });
  }

  ngOnInit() {
    this.neworderservice.isadvancepaymentcheck = false;

    if (!!this.loginauth.selectedactivity.expecteddeliverytype && this.loginauth.selectedactivity.expecteddeliverytype == 'C') {
      this.showExpectedDeliveryDateCtrl = true;
      this.expireDateEdit = true;
    } else if (!!this.loginauth.selectedactivity.expecteddeliverytype && this.loginauth.selectedactivity.expecteddeliverytype == 'F') {
      this.showExpectedDeliveryDateCtrl = true;
      this.expireDateEdit = false;
    } else {
      this.showExpectedDeliveryDateCtrl = false;
    }

    this.advance_payment_chk_status = this.neworderservice.isadvancepaymentcheck;
    if (!!this.loginauth.selectedactivity.is_advance_payment) if (this.loginauth.selectedactivity.is_advance_payment === 'Y') {
      this.is_advance_payment = true;
    } else {
      this.is_advance_payment = false;
    }
    if (!!this.loginauth.selectedactivity.issplorderonfreeqty) if (this.loginauth.selectedactivity.issplorderonfreeqty === 'Y') {
      this.issplorderonfreeqty = true;
    } else {
      this.issplorderonfreeqty = false;
    }
    this.addprod();

    try {
      // this.commonfun.chkcache('neworder');
      setTimeout(() => {
        this.checkplatform();
        this.mmstOrderusrtype = this.loginauth.defaultprofile[0].mmstOrderusrtype;
        this.route.queryParams.subscribe(params => {
          this.edtitdocid = params["edtitdocid"];
        });
      }, 3000);
    } catch (error) {}
  }

  ionViewWillEnter() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // console.log(this.TAG,"Selected Acitivity",this.loginauth.selectedactivity);
      //this.addprod();
      _this.checkcust();
    })();
  }

  formatDate(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_19__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_20__["default"])(value), 'dd-MM-yyyy');
  }

  checkplatform() {
    try {
      if (!this.msg.isplatformweb) {
        this.isdesktop = false;
      } else {
        this.isdesktop = true;
      }
    } catch (error) {
      console.log("checkplatform:Erroe", error);
    }
  }

  uploadImage(str) {
    try {
      this.fileType = "";
      this.fileName = str.target.files[0].name.substring(str.target.files[0].name.lastIndexOf("/") + 1);
      this.fileType = str.target.files[0].name.substring(str.target.files[0].name.lastIndexOf(".") + 1);

      if (!!this.fileType && this.fileType == 'pdf') {
        this.POimg64 = str.target.files[0]; //console.log(this.TAG, "File Size", str.target.files[0].size / 1024 / 1024);

        if (str.target.files[0].size / 1024 / 1024 > 3) {
          // console.log(this.TAG, "File Size Too Large");
          this.commonfun.presentAlert("New Order", "Validation", "File size must be less than Equal to 3 MB");
          this.POimg64 = null;
          this.fileName = "";
          this.fileType = "";
        }
      } else {
        var file = str.target.files[0]; //   console.log("File Selected", str);

        var myreader = new FileReader();

        myreader.onloadend = () => {
          var b64 = myreader.result.toString().replace(/^data:.+;base64,/, '');
          this.POimg64 = b64;
        };

        myreader.readAsDataURL(file);
      }
    } catch (error) {}
  }

  checkcust() {
    try {
      if (this.loginauth.defaultprofile[0].mmstOrderusrtype == "CEB" || this.loginauth.defaultprofile[0].mmstOrderusrtype == "ST") {
        // this.commonfun.loadingPresent();
        this.neworderservice.getnewordercustmersearchapi("", this.formprod.controls["selectedordertype"].value).subscribe(data => {
          // this.commonfun.loadingDismiss();
          var response = data;
          this.BusinessPartnerlist = response;
          console.log("Customer Data ", data);
          this.leastBusinessPartnerlist = response;

          if (this.leastBusinessPartnerlist) {
            if (this.leastBusinessPartnerlist.length > this.loginauth.minlistcount) {} else {
              this.dontClear = true;
            }
          }

          if (this.BusinessPartnerlist.length == 1) {
            this.formprod.controls["selectedBusinessPartner"].setValue(this.BusinessPartnerlist[0]);
            this.selectedBusinessPartner = this.BusinessPartnerlist[0];
            this.neworderservice.getOrderType(this.selectedBusinessPartner.id, this.selectedBusinessPartner.id).subscribe(result => {
              const distictResult = result.filter((thing, index, self) => index === self.findIndex(t => JSON.stringify(t) === JSON.stringify(thing)));
              this.orderTypeList = [];
              distictResult.forEach(element => {
                if (element.method_code == "FEFO") {
                  this.orderTypeList.push({
                    "code": "SL",
                    "name": "Saleable"
                  });
                } else if (element.method_code == "LIQD") {
                  this.orderTypeList.push({
                    "code": "NE",
                    "name": "Near Expiry"
                  });
                } else if (element.method_code == "COPSMPEMP") {
                  this.orderTypeList.push({
                    "code": "ST",
                    "name": "Sample Order - Sales Team"
                  });
                } else if (element.method_code == "COPSMPCUST") {
                  this.orderTypeList.push({
                    "code": "SOC",
                    "name": "Sample Order-Customer"
                  });
                }
              });
            });

            if (!!this.selectedBusinessPartner.primary_customer && this.selectedBusinessPartner.primary_customer == "Y") {
              this.isPrimaryCustomer = true;
            } else {
              this.isPrimaryCustomer = false;
              this.bindPrimaryCustomerFromApi(this.selectedBusinessPartner.id, "");
            }

            this.activity = this.selectedBusinessPartner.mmstOrgAct$_identifier;
            this.activityid = this.selectedBusinessPartner.mmstOrgAct;
            this.formprod.controls["CreditLimit"].setValue(this.selectedBusinessPartner.creditLimit);
            this.bindcustmerbillingaddress();
            this.getTemplate();
            this.getoverdueinvoiceamt();
            this.bindDate();

            if (this.selectedBusinessPartner.showcrlimit == "N") {
              this.showcrlimit = false;
            } else {
              this.showcrlimit = true;
            }
          } else {}

          this.orderlevelper = this.selectedBusinessPartner.orderlevelper;
        }, error => {
          this.commonfun.loadingDismiss();
        });

        if (this.edtitdocid != undefined || this.edtitdocid != '') {
          this.editOrder(this.edtitdocid);
        }
      }
    } catch (error) {
      this.commonfun.loadingDismiss();
    }
  }

  onChangetobeRedeem() {
    this.calculation();
  }

  calculation() {
    try {
      if (this.cartproduct != undefined || this.cartproduct != null) {
        if (this.cartproduct.length == 0) this.Iscartproduct = false;else this.Iscartproduct = true;
        this.tobeRedeem = this.formprod.controls["tobeRedeem"].value;
        const TotalAmount = this.cartproduct.reduce((sum, item) => sum + (item.TotalAmount == "" ? 0 : Number(item.TotalAmount)), 0);
        this.sumtotalamount = TotalAmount; //-Number(this.formprod.controls["tobeRedeem"].value);

        const discount = this.cartproduct.reduce((sum, item) => sum + (item.discount == "" ? 0 : Number(item.discount)), 0);
        this.sumdiscount = discount;
        const GstAmount = this.cartproduct.reduce((sum, item) => sum + (item.GstAmount == "" ? 0 : Number(item.GstAmount)), 0);
        this.sumGstAmount = GstAmount;
        const amount = this.cartproduct.reduce((sum, item) => sum + (item.Amount == "" ? 0 : Number(item.Amount)), 0); // sumtotalamount

        if (this.cartproduct.some(item => item.value === 'MRP')) {
          var amt = this.cartproduct.reduce((sum, item) => sum + (item.Amount == "" ? 0 : Number(item.Amount)), 0);
          var dscamt = this.cartproduct.reduce((sum, item) => sum + (item.discount == "" ? 0 : Number(item.discount)), 0);
          this.sumamount = amt + dscamt;
        } else {
          this.sumamount = amount;
        }
      } else {
        this.Iscartproduct = false;
        this.sumtotalamount = 0;
        this.sumdiscount = 0;
        this.sumGstAmount = 0;
        this.sumamount = 0;
      }
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  onChangeAdvancechk() {
    this.neworderservice.isadvancepaymentcheck = this.advance_payment_chk_status;
  }

  addprod() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        console.log("New Order addprod");

        _this2.route.params.subscribe( /*#__PURE__*/(0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
          if (!!_this2.router.getCurrentNavigation()) if (_this2.router.getCurrentNavigation().extras.state) {
            if (_this2.router.getCurrentNavigation().extras.state.selectedddlproduct) {
              _this2.selectedbunch = null;
              _this2.selectedbunch = _this2.router.getCurrentNavigation().extras.state.selectedddlproduct;
              _this2.cartproduct = _this2.selectedbunch;
              console.log("SET VALUE 360");
              _this2.orderTypeList = [{
                "code": "SL",
                "name": "Saleable"
              }, {
                "code": "NE",
                "name": "Near Expiry"
              }, {
                "code": "ST",
                "name": "Sample Order - Sales Team"
              }, {
                "code": "SOC",
                "name": "Sample Order-Customer"
              }];

              if (_this2.router.getCurrentNavigation().extras.state.orderType == "SL") {
                _this2.formprod.controls["selectedordertype"].setValue("SL");
              } else if (_this2.router.getCurrentNavigation().extras.state.orderType == "NE") {
                _this2.formprod.controls["selectedordertype"].setValue("NE");
              } else if (_this2.router.getCurrentNavigation().extras.state.orderType == "ST") {
                _this2.formprod.controls["selectedordertype"].setValue("ST");
              } else if (_this2.router.getCurrentNavigation().extras.state.orderType == "SOC") {
                _this2.formprod.controls["selectedordertype"].setValue("SOC");
              } //this.formprod.controls["selectedordertype"].setValue(this.router.getCurrentNavigation().extras.state.orderType);


              _this2.formprod.controls["selectedBusinessPartner"].setValue(_this2.router.getCurrentNavigation().extras.state.passTempSelectedBusinessPartner);

              _this2.selectedBusinessPartner = _this2.router.getCurrentNavigation().extras.state.passTempSelectedBusinessPartner;

              if (!!_this2.selectedBusinessPartner.primary_customer && _this2.selectedBusinessPartner.primary_customer == "Y") {
                _this2.isPrimaryCustomer = true;
              } else {
                _this2.isPrimaryCustomer = false;
              }

              if (!!_this2.router.getCurrentNavigation().extras.state.passTempPrimaryCustomer) {
                _this2.formprod.controls["selectedPrimaryBusinessPartner"].setValue(_this2.router.getCurrentNavigation().extras.state.passTempPrimaryCustomer);
              }

              _this2.advance_payment_chk_status = _this2.neworderservice.isadvancepaymentcheck;
              _this2.temp_addressbilling = _this2.router.getCurrentNavigation().extras.state.addressbilling;
              _this2.temp_addressshipping = _this2.router.getCurrentNavigation().extras.state.addressshipping; // this.sp_order_chk_box = this.router.getCurrentNavigation().extras.state.special_order_add_edit;

              if (_this2.router.getCurrentNavigation().extras.state.special_order_add_edit === 'Y') {
                _this2.sp_order_chk_box = true;
              } else {
                _this2.sp_order_chk_box = false;
              } //  console.log('special order',this.sp_order_chk_box);


              _this2.setDataFromPreviousPage(_this2.router.getCurrentNavigation().extras.state.passTempSelectedBusinessPartner);
            } else if (_this2.router.getCurrentNavigation().extras.state.BusinessPartnerlist) {
              _this2.selecteddocumentno = _this2.router.getCurrentNavigation().extras.state.selecteddocumentno;

              if (!!_this2.selecteddocumentno) {
                if (_this2.selecteddocumentno.isgovorder == true) {
                  _this2.sp_order_chk_box = true;
                } else if (_this2.selecteddocumentno.isgovorder == false) {
                  _this2.sp_order_chk_box = false;
                }

                if (_this2.selecteddocumentno.isadvancepay === true) {
                  _this2.advance_payment_chk_status = true;
                  _this2.neworderservice.isadvancepaymentcheck = true;

                  _this2.formprod.get('isAdvancePaymentChk').setValue(true);
                } else {
                  _this2.advance_payment_chk_status = false;
                  _this2.neworderservice.isadvancepaymentcheck = false;

                  _this2.formprod.get('isAdvancePaymentChk').setValue(false);

                  console.log('inside');
                }

                _this2.loadPrimaryCustomer();
              }

              _this2.BusinessPartnerlist = _this2.router.getCurrentNavigation().extras.state.BusinessPartnerlist;
              _this2.selectedBusinessPartner = _this2.router.getCurrentNavigation().extras.state.selectedBusinessPartner;

              if (!!_this2.selectedBusinessPartner.primary_customer && _this2.selectedBusinessPartner.primary_customer == "Y") {
                _this2.isPrimaryCustomer = true;
              } // else {
              //   this.isPrimaryCustomer = false;
              // }


              _this2.formprod.controls["ponumber"].setValue(_this2.selecteddocumentno.poreference == "null" ? "" : _this2.selecteddocumentno.poreference);

              _this2.formprod.controls["tobeRedeem"].setValue(_this2.selecteddocumentno.ntotredeem == "null" ? "0" : _this2.selecteddocumentno.ntotredeem);

              _this2.tobeRedeem = _this2.selecteddocumentno.ntotredeem;
              _this2.istriggeronOrderTypeChange = false;

              _this2.getproductbydocumentid(_this2.selecteddocumentno.id);

              _this2.formprod.controls["selectedBusinessPartner"].setValue(_this2.selectedBusinessPartner);

              if (_this2.selectedBusinessPartner.showcrlimit == "N") {
                _this2.showcrlimit = false;
              } else {
                _this2.showcrlimit = true;
              }

              _this2.activity = _this2.selectedBusinessPartner.mmstOrgAct$_identifier;
              _this2.activityid = _this2.selectedBusinessPartner.mmstOrgAct;

              _this2.formprod.controls["CreditLimit"].setValue(_this2.selectedBusinessPartner.creditLimit);

              _this2.bindcustmerbillingaddress();

              _this2.getTemplate();

              _this2.getoverdueinvoiceamt();

              _this2.bindDate();
            } else {}
          } else {
            // this.formprod.controls["selectedordertype"].setValue('SL');
            console.log("SET VALUE 449"); //  this.onOrderTypeChange();
          }

          _this2.calculation();
        }));
      } catch (error) {
        _this2.commonfun.presentAlert("Message", "Error", error);
      }
    })();
  }

  loadPrimaryCustomer() {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        if (!!_this3.selecteddocumentno.tagprimarycust) {
          _this3.isPrimaryCustomer = false;
          yield _this3.asyncBindPrimaryCustomerFromApi(_this3.selecteddocumentno.bpartner, "");

          try {
            let indexPrimary = _this3.primaryBusinessPartnerList.findIndex(record => record.bpid === _this3.selecteddocumentno.tagprimarycust);

            _this3.formprod.controls["selectedPrimaryBusinessPartner"].setValue(_this3.primaryBusinessPartnerList[indexPrimary]);
          } catch (error) {
            console.log("CATCH", error);
          }
        } else {
          _this3.isPrimaryCustomer = true;
        }
      } catch (error) {
        console.log(error);
      }
    })();
  }

  bindDate() {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let reponse = yield (yield _this4.neworderservice.getExpDateDelivery(_this4.selectedBusinessPartner.id)).toPromise();

        if (!!reponse.expirydate && Object.keys(reponse.expirydate).length != 0) {
          _this4.formprod.controls["expecteddeliverydate"].setValue(reponse.expirydate);
        }
      } catch (error) {// console.log(error);
      }
    })();
  }

  getordertypeby(docid) {
    var _this5 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        yield _this5.neworderservice.getordertypeby(docid).subscribe(data => {
          const response = data['response'];
          console.log("SET VALUE 496");

          if (response.data.length > 0) {
            //              this.formprod.controls["selectedordertype"].setValue(response.data[0].mmstWarehouseCode);
            _this5.orderTypeList = [{
              "code": "SL",
              "name": "Saleable"
            }, {
              "code": "NE",
              "name": "Near Expiry"
            }, {
              "code": "ST",
              "name": "Sample Order - Sales Team"
            }, {
              "code": "SOC",
              "name": "Sample Order-Customer"
            }];

            if (response.data[0].mmstWarehouseCode == "SL") {
              _this5.formprod.controls["selectedordertype"].setValue("SL");
            } else if (response.data[0].mmstWarehouseCode == "NE") {
              _this5.formprod.controls["selectedordertype"].setValue("NE");
            } else if (response.data[0].mmstWarehouseCode == "ST") {
              _this5.formprod.controls["selectedordertype"].setValue("ST");
            } else if (response.data[0].mmstWarehouseCode == "SOC") {
              _this5.formprod.controls["selectedordertype"].setValue("SOC");
            }
          } else {
            _this5.formprod.controls["selectedordertype"].setValue(null);
          }
        });
      } catch (error) {
        _this5.commonfun.presentAlert("Message", "Error", error);
      }
    })();
  }

  getproductbydocumentid(docid) {
    try {
      this.neworderservice.getproductbydocumentidapi(docid).subscribe(data => {
        this.cartproduct = data;
        this.cartproduct.reverse();
        console.log("SET VALUE 512");
        if (data != null) this.orderTypeList = [{
          "code": "SL",
          "name": "Saleable"
        }, {
          "code": "NE",
          "name": "Near Expiry"
        }, {
          "code": "ST",
          "name": "Sample Order - Sales Team"
        }, {
          "code": "SOC",
          "name": "Sample Order-Customer"
        }];

        if (this.cartproduct[0].ordertypeionic == "SL") {
          this.formprod.controls["selectedordertype"].setValue("SL");
        } else if (this.cartproduct[0].ordertypeionic == "NE") {
          this.formprod.controls["selectedordertype"].setValue("NE");
        } else if (this.cartproduct[0].ordertypeionic == "ST") {
          this.formprod.controls["selectedordertype"].setValue("ST");
        } else if (this.cartproduct[0].ordertypeionic == "SOC") {
          this.formprod.controls["selectedordertype"].setValue("SOC");
        }

        this.formprod.controls["orderdescription"].setValue(this.cartproduct[0].description);
        this.calculation();
      }, error => {
        this.commonfun.presentAlert("Message", "Error", error.error.text);
      });
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  bindcustomerapi(strsearch) {
    try {
      // console.log("bindcustomerapi");  
      if (strsearch != "") {
        this.neworderservice.getnewordercustmersearchapi(strsearch, this.formprod.controls["selectedordertype"].value).subscribe(data => {
          // console.log("Customer DAta",data);
          var response = data;
          this.BusinessPartnerlist = response;

          if (this.edtitdocid != undefined || this.edtitdocid == '') {
            this.editOrder(this.edtitdocid);
          }
        });
      } else {
        // this.BusinessPartnerlist=null;
        //=============start for top 10=================
        if (this.leastBusinessPartnerlist) {
          if (this.leastBusinessPartnerlist.length > this.loginauth.minlistcount) {
            this.BusinessPartnerlist = null;
          } else {
            this.BusinessPartnerlist = this.leastBusinessPartnerlist;
            this.dontClear = true;
          }
        } else {
          this.neworderservice.getnewordercustmersearchapi("", this.formprod.controls["selectedordertype"].value).subscribe(data => {
            const response = data;

            if (!!response) {
              this.leastBusinessPartnerlist = response;
            }

            if (this.leastBusinessPartnerlist) {
              if (this.leastBusinessPartnerlist.length > this.loginauth.minlistcount) {
                this.BusinessPartnerlist = null;
              } else {
                this.dontClear = true;
                this.BusinessPartnerlist = this.leastBusinessPartnerlist;
              }
            }
          });
        } //=============end for top 10=================

      }
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error); // this.commonfun.loadingDismiss();
    }
  }

  bindPrimaryCustomerFromApi(bp_id, strsearch) {
    try {
      //   console.log("bindPrimaryCustomerFromApi");
      //let searchText = this.portComponent.searchText;  
      //console.log("bindPrimaryCustomerFromApi",searchText);     
      // this.portComponent.clear();  
      if (strsearch != "") {
        this.neworderservice.getPrimaryCustomerService(bp_id, strsearch, this.formprod.controls["selectedordertype"].value).subscribe(data => {
          //  console.log("Primary Customer DAta",data);
          this.primaryBusinessPartnerList = data;
        });
      } else {
        this.neworderservice.getPrimaryCustomerService(bp_id, strsearch, this.formprod.controls["selectedordertype"].value).subscribe(data => {
          //  console.log("Primary Customer DAta",data);
          this.leastPrimaryBusinessPartnerList = data; //  this.primaryBusinessPartnerList =  this.leastPrimaryBusinessPartnerList;

          if (this.leastPrimaryBusinessPartnerList.length > this.loginauth.minlistcount) {
            this.primaryBusinessPartnerList = null;
          } else {
            this.dontPrimaryClear = true;
            this.primaryBusinessPartnerList = this.leastPrimaryBusinessPartnerList;
          }

          if (this.leastPrimaryBusinessPartnerList.length == 1) {
            this.formprod.controls["selectedPrimaryBusinessPartner"].setValue(this.leastPrimaryBusinessPartnerList[0]);
          }
        });
      }
    } catch (error) {
      console.log(this.TAG, error);
      this.commonfun.presentAlert("New Order", "Error", error);
    }
  }

  asyncBindPrimaryCustomerFromApi(bp_id, strsearch) {
    var _this6 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        if (strsearch != "") {
          let data = yield _this6.neworderservice.getPrimaryCustomerService(bp_id, strsearch, _this6.formprod.controls["selectedordertype"].value).toPromise(); // console.log("Primary Customer async DAta",data);

          _this6.primaryBusinessPartnerList = data;
        } else {
          let data = yield _this6.neworderservice.getPrimaryCustomerService(bp_id, strsearch, _this6.formprod.controls["selectedordertype"].value).toPromise(); // console.log("Primary Customer DAta",data);

          _this6.leastPrimaryBusinessPartnerList = data; //  this.primaryBusinessPartnerList =  this.leastPrimaryBusinessPartnerList;

          if (_this6.leastPrimaryBusinessPartnerList.length > _this6.loginauth.minlistcount) {
            _this6.primaryBusinessPartnerList = null;
          } else {
            _this6.primaryBusinessPartnerList = _this6.leastPrimaryBusinessPartnerList;
            _this6.dontPrimaryClear = true;
          }

          if (_this6.leastPrimaryBusinessPartnerList.length == 1) {
            _this6.formprod.controls["selectedPrimaryBusinessPartner"].setValue(_this6.leastPrimaryBusinessPartnerList[0]);
          }
        }
      } catch (error) {
        console.log(_this6.TAG, error);

        _this6.commonfun.presentAlert("New Order", "Error", error);
      }
    })();
  }

  bindBusinessPartner() {
    this.loginauth.getuserwiseactivity(this.loginauth.userid).subscribe(data => {
      this.activitylist = data;
      var organization = this.activitylist.map(function (item) {
        return '\'' + item.id + '\'';
      });
      this.neworderservice.activitywiseBusinessPartner(organization).subscribe(actdata => {
        const actresponse = actdata['response']; // this.Allcustomer = organization;

        this.BusinessPartnerlist = actresponse.data;
      });

      if (this.edtitdocid != undefined || this.edtitdocid == '') {
        this.editOrder(this.edtitdocid);
      }
    });
  }

  onTemplateChange() {
    try {
      if (this.formprod.controls["selectedBPaddressshipping"].value != undefined && this.formprod.controls["selectedBPaddressshipping"].value != null && this.formprod.controls["selectedBPaddressshipping"].value != "" && this.formprod.controls["selectedBPaddressbilling"].value != undefined && this.formprod.controls["selectedBPaddressbilling"].value != null && this.formprod.controls["selectedBPaddressbilling"].value != "") {
        this.cartproduct = null;
        this.calculation();
        this.gettemplateproductdetail(this.formprod.controls["selectedBPtemplate"].value, this.formprod.controls["selectedordertype"].value);
      } else {
        if (this.formprod.controls["selectedBPtemplate"].value != undefined && this.formprod.controls["selectedBPtemplate"].value != null && this.formprod.controls["selectedBPtemplate"].value != "") {
          this.commonfun.presentAlert("Message", "Alert", "Please select Address first.");
          this.formprod.controls["selectedBPaddressbilling"].markAsTouched();
          this.formprod.controls["selectedBPaddressshipping"].markAsTouched();
          this.formprod.controls["selectedBPtemplate"].setValue("");
        }
      }
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  bindcustmerbillingaddress() {
    try {
      //  console.log("bindcustmerbillingaddress",this.selectedBusinessPartner.id)
      this.neworderservice.getcustmerbillingaddress(this.selectedBusinessPartner.id).subscribe(data => {
        const response = data['response'];
        var jsondata = response.data;
        this.BPaddressbilling = jsondata.filter(e => e.invoiceToAddress == true);
        this.BPaddressshipping = jsondata.filter(e => e.shipToAddress == true); // console.log("BPaddressbilling",this.BPaddressbilling);
        // console.log("BPaddressbilling",this.BPaddressshipping);

        setTimeout(() => {
          if (this.BPaddressshipping.length == 1) {
            this.formprod.controls["selectedBPaddressshipping"].setValue(this.BPaddressshipping[0].id);
          } else if (this.selecteddocumentno) {
            this.formprod.controls["selectedBPaddressshipping"].setValue(this.selecteddocumentno.bpartnerLocation);
          } else if (!!this.temp_addressshipping && this.BPaddressshipping.length != 1) {
            let index = this.BPaddressshipping.findIndex(x => x.id === this.temp_addressshipping); // console.log("YEs Selected",this.BPaddressshipping[index]);

            this.formprod.controls["selectedBPaddressshipping"].setValue(this.BPaddressshipping[index].id);
          }

          if (this.BPaddressbilling.length == 1) {
            this.formprod.controls["selectedBPaddressbilling"].setValue(this.BPaddressbilling[0].id);
          } else if (this.selecteddocumentno) {
            this.formprod.controls["selectedBPaddressbilling"].setValue(this.selecteddocumentno.billto);
          } else if (!!this.temp_addressbilling && this.BPaddressbilling.length != 1) {
            let index = this.BPaddressbilling.findIndex(x => x.id === this.temp_addressbilling); // console.log("YEs Selected",this.BPaddressbilling[index]);

            this.formprod.controls["selectedBPaddressbilling"].setValue(this.BPaddressbilling[index].id);
          }
        }, 1000);
      }, error => {
        this.commonfun.presentAlert("Message", "Error", error);
      });
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  getoverdueinvoiceamt() {
    try {
      var orderno = '';
      if (this.selecteddocumentno != undefined) orderno = this.selecteddocumentno.id == undefined ? "" : this.selecteddocumentno.id;
      this.neworderservice.getoverdueinvoiceamt(this.selectedBusinessPartner.id, orderno).subscribe(data => {
        if (data != null) {
          this.overdueinvoiceamtresponse = data;
          this.Showoverdue = this.overdueinvoiceamtresponse[0].Showoverdue;
          this.ispomandatory = this.overdueinvoiceamtresponse[0].ispomandatory;
          this.ispoimg = this.overdueinvoiceamtresponse[0].ispoimg;
          this.formprod.controls["Overdueamount"].setValue(Number(this.overdueinvoiceamtresponse[0].overdueamt).toLocaleString('en-IN'));
          this.formprod.controls["OverdueInvoice"].setValue(this.overdueinvoiceamtresponse[0].overdueinvoice);
          this.formprod.controls["duedatedays"].setValue(this.overdueinvoiceamtresponse[0].duedatedays);
          this.autocalculation = this.overdueinvoiceamtresponse[0].autocalculation;
          this.formprod.controls["avaliableRedeem"].setValue(this.overdueinvoiceamtresponse[0].vetcoins);
          if (this.overdueinvoiceamtresponse[0].usevetcoin != 0 || this.overdueinvoiceamtresponse[0].usevetcoin != "") this.formprod.controls["tobeRedeem"].setValue(this.overdueinvoiceamtresponse[0].usevetcoin);
          this.tobeRedeem = this.overdueinvoiceamtresponse[0].usevetcoin;
          this.orderlevelper = this.overdueinvoiceamtresponse[0].orderlevelper;
          this.checkboxtab = this.overdueinvoiceamtresponse[0].checkboxtab;
        } else {
          this.Showoverdue = false;
          this.checkboxtab = false;
        }
      }, error => {
        this.commonfun.presentAlert("Message", "Error", error.error);
      });
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  oncustchange(event) {
    var _this7 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // console.log("oncustchange");
      _this7.BPaddressbilling = null;
      _this7.BPaddressshipping = null;
      _this7.BPtemplate = null;
      _this7.cartproduct = null;

      _this7.calculation();

      _this7.formprod.controls["selectedBPaddressshipping"].setValue('');

      _this7.formprod.controls["selectedBPaddressbilling"].setValue('');

      _this7.formprod.controls["CreditLimit"].setValue(null);

      _this7.formprod.controls["selectedBPtemplate"].setValue(null);

      _this7.formprod.controls["OverdueInvoice"].setValue(null);

      _this7.formprod.controls["duedatedays"].setValue(null);

      _this7.formprod.controls["avaliableRedeem"].setValue(null);

      _this7.formprod.controls["tobeRedeem"].setValue("0");

      _this7.formprod.controls["ponumber"].setValue(null);

      _this7.selecteddocumentno = "";
      _this7.BPtemplate = null;

      _this7.formprod.controls["selectedBPtemplate"].setValue(null);

      console.log("This Customer", event.value);
      const result = yield (yield _this7.neworderservice.getOrderType(event.value.id, event.value.id)).toPromise();
      const distictResult = result.filter((thing, index, self) => index === self.findIndex(t => JSON.stringify(t) === JSON.stringify(thing))); // console.log("RESULT FROM ORDER TYPE",distictResult);

      _this7.orderTypeList = [];
      distictResult.forEach(element => {
        if (element.method_code == "FEFO") {
          _this7.orderTypeList.push({
            "code": "SL",
            "name": "Saleable"
          });
        } else if (element.method_code == "LIQD") {
          _this7.orderTypeList.push({
            "code": "NE",
            "name": "Near Expiry"
          });
        } else if (element.method_code == "COPSMPEMP") {
          _this7.orderTypeList.push({
            "code": "ST",
            "name": "Sample Order - Sales Team"
          });
        } else if (element.method_code == "COPSMPCUST") {
          _this7.orderTypeList.push({
            "code": "SOC",
            "name": "Sample Order-Customer"
          });
        }
      }); // console.log("LIST",this.orderTypeList);

      if (event.value != undefined) {
        _this7.activity = event.value.mmstOrgAct$_identifier;
        _this7.activityid = event.value.mmstOrgAct;

        if (event.value.showcrlimit == "N") {
          _this7.showcrlimit = false;
        } else {
          _this7.showcrlimit = true;

          _this7.formprod.controls["CreditLimit"].setValue(event.value.creditLimit);
        }

        _this7.selectedBusinessPartner = event.value;

        if (!!_this7.selectedBusinessPartner.primary_customer && _this7.selectedBusinessPartner.primary_customer == "Y") {
          _this7.isPrimaryCustomer = true; //console.log("true")
        } else {
          _this7.isPrimaryCustomer = false; // console.log("false")

          _this7.formprod.controls["selectedPrimaryBusinessPartner"].reset();
        }

        _this7.bindcustmerbillingaddress();

        _this7.getoverdueinvoiceamt();

        _this7.getTemplate();

        _this7.bindPrimaryCustomerFromApi(_this7.selectedBusinessPartner.id, "");
      } else {
        _this7.activity = '';
      }

      let reponse = yield (yield _this7.neworderservice.getExpDateDelivery(event.value.id)).toPromise();

      if (!!reponse.expirydate && Object.keys(reponse.expirydate).length != 0) {
        _this7.formprod.controls["expecteddeliverydate"].setValue(reponse.expirydate);
      }

      event.component._searchText = "";
    })();
  }

  onPrimaryCustomerChange(event) {
    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {} catch (error) {}
    })();
  }

  setDataFromPreviousPage(value) {
    var _this8 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        // console.log("setDataFromPreviousPage");
        _this8.BPaddressbilling = null;
        _this8.BPaddressshipping = null;
        _this8.BPtemplate = null; //  this.cartproduct = null;

        _this8.calculation();

        _this8.formprod.controls["selectedBPaddressshipping"].setValue('');

        _this8.formprod.controls["selectedBPaddressbilling"].setValue('');

        _this8.formprod.controls["CreditLimit"].setValue(null);

        _this8.formprod.controls["selectedBPtemplate"].setValue(null);

        _this8.formprod.controls["OverdueInvoice"].setValue(null);

        _this8.formprod.controls["duedatedays"].setValue(null);

        _this8.formprod.controls["avaliableRedeem"].setValue(null);

        _this8.formprod.controls["tobeRedeem"].setValue("0"); // this.formprod.controls["ponumber"].setValue(null);
        //  this.selecteddocumentno = "";


        _this8.BPtemplate = null;

        _this8.formprod.controls["selectedBPtemplate"].setValue(null);

        if (value != undefined) {
          _this8.activity = value.mmstOrgAct$_identifier;
          _this8.activityid = value.mmstOrgAct;

          if (value.showcrlimit == "N") {
            _this8.showcrlimit = false;
            1;
          } else {
            _this8.showcrlimit = true;

            _this8.formprod.controls["CreditLimit"].setValue(value.creditLimit);
          }

          _this8.selectedBusinessPartner = value;

          if (!!_this8.selectedBusinessPartner.primary_customer && _this8.selectedBusinessPartner.primary_customer == "Y") {
            _this8.isPrimaryCustomer = true;
          } else {
            _this8.isPrimaryCustomer = false;
          }

          _this8.bindcustmerbillingaddress();

          _this8.getoverdueinvoiceamt();

          _this8.getTemplate();
        } else {
          _this8.activity = '';
        }

        let reponse = yield (yield _this8.neworderservice.getExpDateDelivery(value.id)).toPromise();

        if (!!reponse.expirydate && Object.keys(reponse.expirydate).length != 0) {
          _this8.formprod.controls["expecteddeliverydate"].setValue(reponse.expirydate);
        }
      } catch (error) {
        console.log(_this8.TAG, error);
      }
    })();
  }

  onOrderTypeChange() {
    try {
      // console.log("onOrderTypeChange");
      //  this.leastBusinessPartnerlist = null;
      //  this.BusinessPartnerlist = null;
      if (this.formprod.controls["selectedordertype"].value == "ST" || this.formprod.controls["selectedordertype"].value == "SOC") {
        this.Isshowtemplate = true;
      } else {
        this.Isshowtemplate = false;
      }

      if (this.istriggeronOrderTypeChange == true) {
        // this.getTemplate();
        //   this.BPaddressbilling = null;
        //   this.BPaddressshipping = null;
        //   this.BPtemplate = null;
        //   this.cartproduct = null;
        this.calculation(); //this.formprod.controls["selectedBPaddressshipping"].setValue('');
        //  this.formprod.controls["selectedBPaddressbilling"].setValue('');
        // this.formprod.controls["CreditLimit"].setValue(null);
        //  this.formprod.controls["selectedBPtemplate"].setValue(null);
        //   this.formprod.controls["OverdueInvoice"].setValue(null);
        //  this.formprod.controls["duedatedays"].setValue(null);
        //  this.formprod.controls["avaliableRedeem"].setValue(null);
        //  this.formprod.controls["tobeRedeem"].setValue("0");
        //  this.activity = '';
        //  this.formprod.controls["ponumber"].setValue(null);
        //    this.formprod.controls["selectedordertype"].setValue(null);
        //  this.selecteddocumentno = "";
        //  this.BPtemplate = null;
        //  this.formprod.controls["selectedBPtemplate"].setValue(null);
        ///   this.formprod.controls["selectedBusinessPartner"].setValue(null);
        //  this.checkcust();
      } else {
        this.istriggeronOrderTypeChange = true;
      }
    } catch (error) {//  console.log("Ordertypechange:Erroe", error);
    }
  }

  custsearchChange(event) {
    console.log("custsearchChange");
    this.reftextcount++;
    var custsearchtext = event.text; //.replace(/\s/g,'');

    if (custsearchtext.length >= 3) {
      this.bindcustomerapi(custsearchtext);
      this.reftextcount = 0;
    } else {
      if (!!this.dontClear && this.dontClear == true) {
        this.checkcust();
      } else {
        this.BusinessPartnerlist = []; // this.checkcust();
      } // if (this.leastBusinessPartnerlist) {
      //   if (this.leastBusinessPartnerlist.length > this.loginauth.minlistcount) {
      //     this.BusinessPartnerlist = [];
      //   } else {
      //     this.BusinessPartnerlist = this.leastBusinessPartnerlist;
      //   }
      // }

    }
  }

  onPrimaryCustSearch(event) {
    try {
      if (event.text.length >= 3) {
        this.bindPrimaryCustomerFromApi(this.selectedBusinessPartner.id, event.text);
      } else {
        if (!!this.dontPrimaryClear && this.dontPrimaryClear == true) {} else {
          this.primaryBusinessPartnerList = [];
        }
      }
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  toggle(selectedcartproduct) {
    if (selectedcartproduct.show == false) {
      for (var i = 0; i < this.cartproduct.length; i++) {
        if (this.cartproduct[i].show === "true") {
          this.cartproduct[i].show = "false";
        }
      }
    }

    selectedcartproduct.show = !selectedcartproduct.show;
  }

  removeProduct(post) {
    try {
      const result = this.cartproduct.filter(item => item.MainProductid != post.MainProductid);
      let body = {
        "productlist": result
      };
      this.neworderservice.checkCOPBlockOrder(body).subscribe(() => {
        this.cartproduct = result;
        this.calculation();
        this.onAddProduct(); // if (index > -1) {
        //   this.cartproduct.splice(index, 1);
        // }
      }, error => {
        this.commonfun.presentAlert("Message", "Error", error);
        return;
      });
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  editProduct(post) {
    try {
      if (this.addproductvalidation()) {
        this.onShowProductListModel(post);
      }
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  onShowProductListModel(product) {
    var _this9 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const modal = yield _this9.modalController.create({
          //component: EditNewOrderProductPage,
          component: _product_list_product_list_page__WEBPACK_IMPORTED_MODULE_3__.ProductListPage,
          cssClass: 'my-custom-class',
          backdropDismiss: false,
          componentProps: {
            editMode: true,
            productToBeEdit: product,
            activity_id: _this9.activityid,
            cust_id: _this9.selectedBusinessPartner.id,
            bpBillingAddress: _this9.formprod.controls["selectedBPaddressbilling"].value,
            bpShippingAddress: _this9.formprod.controls["selectedBPaddressshipping"].value,
            orderType: _this9.formprod.controls["selectedordertype"].value,
            tempProductSelectedOn: _this9.cartproduct,
            selectedFilter: "",
            special_order_add_edit_param: _this9.sp_order_chk_box ? "Y" : "N",
            is_advance_payment_param: _this9.advance_payment_chk_status,
            issplorderonfreeqty: _this9.issplorderonfreeqty
          }
        });
        modal.onDidDismiss().then(data => {
          //  console.log("Product List Model Data",data);
          if (!!data.data) {
            _this9.cartproduct = data.data;

            _this9.calculation();

            _this9.onAddProduct();
          }
        });
        return yield modal.present();
      } catch (error) {
        console.log(error);
      }
    })();
  }

  addproductvalidation() {
    var isvalidall = true;
    var msgs = [];

    if (this.selectedBusinessPartner != undefined && this.selectedBusinessPartner != null) {} else {
      isvalidall = false;
      this.formprod.controls["selectedBusinessPartner"].markAsTouched();
      msgs[msgs.length] = "Customer";
    }

    if (this.formprod.controls["selectedBPaddressshipping"].value != undefined && this.formprod.controls["selectedBPaddressshipping"].value != null && this.formprod.controls["selectedBPaddressshipping"].value != "") {} else {
      isvalidall = false;
      msgs[msgs.length] = "Shipping Address";
      this.formprod.controls["selectedBPaddressshipping"].markAsTouched();
    }

    if (this.formprod.controls["selectedBPaddressbilling"].value != undefined && this.formprod.controls["selectedBPaddressbilling"].value != null && this.formprod.controls["selectedBPaddressbilling"].value != "") {} else {
      isvalidall = false;
      msgs[msgs.length] = "Billing Address";
      this.formprod.controls["selectedBPaddressbilling"].markAsTouched();
    }

    if (this.formprod.controls["selectedordertype"].value != undefined && this.formprod.controls["selectedordertype"].value != null && this.formprod.controls["selectedordertype"].value != "") {} else {
      //  if(isvalidall==false)msgs+=","
      isvalidall = false;
      msgs[msgs.length] = "Order Type";
      this.formprod.controls["selectedordertype"].markAsTouched();
    }

    if (isvalidall == false) {
      var errmsg = "Please select ";

      for (var i = 0; i < msgs.length; i++) {
        if (msgs.length == 1) //only one
          errmsg += msgs[i] + ".";else if (i == msgs.length - 1) //last
          errmsg += " and " + msgs[i] + ".";else if (i == 0) //first
          errmsg += msgs[i];else //middle
          errmsg += ", " + msgs[i];
      }

      this.commonfun.presentAlert("Message", "Alert", errmsg);
    }

    return isvalidall;
  }

  onAddProduct() {
    try {
      if (this.addproductvalidation()) {
        // console.log("SELECTED selectedBPaddressshipping",this.formprod.controls["selectedBPaddressshipping"].value);
        //  console.log("SELECTED selectedBPaddressshipping",this.formprod.controls["selectedBPaddressbilling"].value);
        console.log('this.formprod.controls["specialOrderCtrl"].value', this.sp_order_chk_box);
        console.log('this.formprod.controls["specialOrderCtrl"].value', this.sp_order_chk_box ? "Y" : "N");
        let navigationExtras = {
          state: {
            cartproduct: this.cartproduct,
            cust_id: this.selectedBusinessPartner.id,
            businessPartnerCategory: this.selectedBusinessPartner.businessPartnerCategory,
            tempSelectedBusinessPartner: this.selectedBusinessPartner,
            activityid: this.selectedBusinessPartner.mmstOrgAct,
            //  Isdruglicence:this.Isdruglicence,
            order_type: this.formprod.controls["selectedordertype"].value,
            addressshipping: this.formprod.controls["selectedBPaddressshipping"].value,
            addressbilling: this.formprod.controls["selectedBPaddressbilling"].value,
            special_order: this.sp_order_chk_box ? "Y" : "N",
            tempPrimaryCustomer: this.formprod.controls["selectedPrimaryBusinessPartner"].value,
            is_advance_payment_param: this.advance_payment_chk_status,
            issplorderonfreeqty: this.issplorderonfreeqty
          }
        }; //console.log('addedit inside');

        this.router.navigate(['addeditproduct'], navigationExtras); // this.formprod.controls["selectedBusinessPartner"].
      }
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  Resetpage() {
    this.formprod.reset();
    this.showhideRedeemReward = true; //this.hideallRedeemReward=false;

    this.activity = '';
    this.selectedBusinessPartner = null;
    this.selecteddocumentno = null;
    this.cartproduct = null;
    this.mergedcart = null;
    this.BPaddressshipping = null;
    this.BPaddressbilling = null;
    this.POimg64 = '';
    this.fileName = "";
    this.fileType = "";
    this.calculation();
    this.BPtemplate = null;
    this.autocalculation = false;
    this.Showoverdue = true;
    this.showcrlimit = false;
    this.sp_order_chk_box = false;
    this.advance_payment_chk_status = false;
    this.isPrimaryCustomer = true;
    this.primaryBusinessPartnerList = [];
    this.leastPrimaryBusinessPartnerList = []; // this.formprod.controls["selectedordertype"].setValue("SL");
    // this.checkcust();

    this.Isshowtemplate = false; //    this.router.navigateByUrl('/neworder');
  }

  onClose(event) {
    event.component.searchText = "";
  }

  onPrimaryCustomerClose(event) {
    try {
      //  console.log(this.TAG,"onPrimaryCustomerClose",this.portComponent);
      this.portComponent._searchText = ""; // this.primaryBusinessPartnerList = [];
      //  event.component._searchText == "";
    } catch (error) {}
  }

  mergproduct() {
    try {
      this.mergedcart = this.cartproduct.filter(e => e.MainProductid === e.product_id && e.MainProduct != '');

      for (var k = 0; k < this.cartproduct.length; k++) {
        if (this.cartproduct[k].MainProduct == '') {
          var Isexistproduct = false;

          for (var l = 0; l < this.mergedcart.length; l++) {
            if (this.mergedcart[l].product_id == this.cartproduct[k].product_id) {
              Isexistproduct = true;
              var mfreeqty = this.mergedcart[l].freeqty == '' ? 0 : this.mergedcart[l].freeqty;
              var cfreeqty = this.cartproduct[k].freeqty == '' ? 0 : this.cartproduct[k].freeqty;
              this.mergedcart[l].freeqty = Number(mfreeqty) + Number(cfreeqty);
              this.mergedcart[l].Scheme = "| " + this.cartproduct[k].Scheme;
              this.mergedcart[l].Schemeid = "| " + this.cartproduct[k].Schemeid;
            }
          }

          if (Isexistproduct == false) {
            this.mergedcart.push(this.cartproduct[k]);
          }
        }
      } //mergedcart

    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  getTemplate() {
    try {
      if (this.selectedBusinessPartner) {
        this.neworderservice.getOrdertemplate(this.selectedBusinessPartner.id).subscribe(data => {
          const response = data['response']; // var jsondata = response.data;

          this.BPtemplate = response.data;
        }, error => {
          this.commonfun.presentAlert("Message", "Error", error);
          this.commonfun.loadingDismiss(); //  console.log("getTemplate:loadingDismiss");
        });
      } else {
        if (this.formprod.controls["selectedordertype"].value != undefined && this.formprod.controls["selectedordertype"].value != null && this.formprod.controls["selectedordertype"].value != "") {
          this.BPtemplate = null;
          console.log("SET VALUE 1245");
          this.formprod.controls["selectedordertype"].setValue("");
          this.commonfun.presentAlert("Message", "Alert", "Please select customer first.");
        }

        this.commonfun.loadingDismiss();
      }
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  onSaveTemplate(fvalue) {
    var _this10 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        yield _this10.commonfun.loadingPresent();
        _this10.mainproducts = _this10.cartproduct.filter(e => e.MainProductid === e.product_id && e.MainProduct != '');
        var jsondatatemp = {
          "cust_id": fvalue.selectedBusinessPartner.id,
          "activity_id": fvalue.selectedBusinessPartner.mmstOrgAct,
          "CreditLimit": fvalue.selectedBusinessPartner.creditLimit,
          "OverdueInvoice": fvalue.OverdueInvoice == null ? "" : fvalue.OverdueInvoice,
          "Overdueamount": fvalue.Overdueamount == undefined ? "" : fvalue.Overdueamount,
          "template": fvalue.selectedtemplate,
          "ponumber": fvalue.ponumber,
          "ShippingAddress": fvalue.selectedBPaddressshipping,
          "BillingAddress": fvalue.selectedBPaddressbilling,
          "OrderType": fvalue.selectedordertype,
          "POimage": _this10.POimg64 == undefined ? "" : _this10.POimg64,
          "usevetcoin": fvalue.tobeRedeem == null ? "" : fvalue.tobeRedeem,
          "vetcoins": fvalue.avaliableRedeem == null ? "" : fvalue.avaliableRedeem,
          "autocalculation": _this10.autocalculation,
          "orderlevelper": _this10.orderlevelper,
          "products": _this10.mainproducts == undefined ? "" : _this10.mainproducts,
          "orderdiscription": fvalue.orderdescription,
          "expirydate": fvalue.expecteddeliverydate
        };

        _this10.neworderservice.SaveTemplate(jsondatatemp).subscribe(data => {
          _this10.commonfun.loadingDismiss();

          if (data != null) {
            _this10.orderpunchresponse = data;

            if (_this10.orderpunchresponse.resposemsg == "success") {
              _this10.commonfun.presentAlert("Message", "Success", "Template saved successfully."); //  this.Resetpage();

            } else {
              //  this.Resetpage();
              _this10.commonfun.presentAlert("message", "Fail", _this10.orderpunchresponse.logmsg);
            }
          }
        }, error => {
          _this10.commonfun.loadingDismiss();

          _this10.commonfun.presentAlert("Message", "Error!", error.error.text);
        });
      } catch (error) {
        _this10.commonfun.loadingDismiss();

        _this10.commonfun.presentAlert("Message", "Error", error);
      }
    })();
  }

  saveTemplate(fvalue) {
    var _this11 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this11.mainproducts = _this11.cartproduct.filter(e => e.MainProductid === e.product_id && e.MainProduct != '');
        var jsondatatemp = {
          "cust_id": fvalue.selectedBusinessPartner.id,
          "activity_id": fvalue.selectedBusinessPartner.mmstOrgAct,
          "CreditLimit": fvalue.selectedBusinessPartner.creditLimit,
          "OverdueInvoice": fvalue.OverdueInvoice == null ? "" : fvalue.OverdueInvoice,
          "Overdueamount": fvalue.Overdueamount == undefined ? "" : fvalue.Overdueamount,
          "template": fvalue.selectedtemplate,
          "ponumber": fvalue.ponumber,
          "ShippingAddress": fvalue.selectedBPaddressshipping,
          "BillingAddress": fvalue.selectedBPaddressbilling,
          "OrderType": fvalue.selectedordertype,
          "POimage": _this11.POimg64 == undefined ? "" : _this11.POimg64,
          "usevetcoin": fvalue.tobeRedeem == null ? "" : fvalue.tobeRedeem,
          "vetcoins": fvalue.avaliableRedeem == null ? "" : fvalue.avaliableRedeem,
          "autocalculation": _this11.autocalculation,
          "orderlevelper": _this11.orderlevelper,
          "products": _this11.mainproducts == undefined ? "" : _this11.mainproducts,
          "orderdiscription": fvalue.orderdescription,
          "expirydate": fvalue.expecteddeliverydate
        };
        const modal = yield _this11.modalController.create({
          component: _custom_alert_custom_alert_page__WEBPACK_IMPORTED_MODULE_17__.CustomAlertPage,
          cssClass: 'custom-alert',
          backdropDismiss: false,
          componentProps: {
            templateData: jsondatatemp
          }
        });
        modal.onDidDismiss().then(data => {
          //  console.log("CustomAlertPage",data);
          if (!!data.data) {
            //   console.log("REturn DATA",data.data);
            _this11.orderpunchresponse = data.data;

            if (_this11.orderpunchresponse.resposemsg == "success") {
              _this11.commonfun.presentAlert("Message", "Success", "Template saved successfully.");
            }
          }
        });
        return yield modal.present();
      } catch (error) {
        console.log(error);
      }
    })();
  }

  gettemplateproductdetail(ordertemp_id, order_type) {
    try {
      this.commonfun.loadingPresent();
      this.addeditproductservice.getproductdetail(this.activityid, this.selectedBusinessPartner.id, "", "", ordertemp_id, order_type, this.formprod.controls["selectedBPaddressshipping"].value, this.formprod.controls["selectedBPaddressbilling"].value, this.sp_order_chk_box ? "Y" : "N", this.advance_payment_chk_status, 0).subscribe(data => {
        this.commonfun.loadingDismiss();

        if (data != null) {
          this.templateproductdetailresponse = data;
          this.selectedbunch = null;
          this.selectedbunch = this.templateproductdetailresponse.reverse();

          if (this.cartproduct == undefined || this.cartproduct == null) {
            this.cartproduct = this.selectedbunch;
          } else if (this.cartproduct.length >= 0) {
            for (var c = 0; c < this.selectedbunch.length; c++) {
              var sameprod = this.cartproduct.find(e => e.MainProductid === this.selectedbunch[c].MainProductid);

              if (sameprod != null || sameprod != undefined) {
                this.removeProduct(sameprod);
              }
            }

            for (var b = 0; b < this.selectedbunch.length; b++) {
              this.cartproduct.push(this.selectedbunch[b]);
            }
          } else {}

          this.calculation();
        } else {
          this.commonfun.loadingDismiss();
          this.commonfun.presentAlert("Message", "Error!", "No product found.");
        }
      }, error => {
        this.commonfun.loadingDismiss(); //  this.commonfun.presentAlert("Message","Error!",error.statusText +" with status code :"+error.status);

        this.commonfun.presentAlert("Message", "Error!", error.error.text);
      });
    } catch (error) {
      //  console.log("gettemplateproductdetail:Erroe", error);
      this.commonfun.loadingDismiss();
    }
  }

  SaveOrder(jsondata, stype) {
    try {
      if (this.ispomandatory == "Y" && (this.formprod.controls['ponumber'].value == "" || this.formprod.controls['ponumber'].value == null || this.formprod.controls['ponumber'].value == undefined)) {
        this.commonfun.presentAlert("Message", "Alert!", "Please enter PO no.");
        return false;
      }

      if (this.ispoimg == "Y" && (this.POimg64 == "" || this.POimg64 == null || this.POimg64 == undefined)) {
        this.commonfun.presentAlert("Message", "Alert!", "Please select PO image.");
        return false;
      }

      this.commonfun.loadingPresent();

      if (this.fileType != 'pdf') {
        this.neworderservice.OrderPunch(jsondata).subscribe(data => {
          this.commonfun.loadingDismiss();

          if (data != null) {
            this.orderpunchresponse = data;

            if (this.orderpunchresponse.resposemsg == "success") {
              if (stype == "N") this.commonfun.presentAlert("Message", "Success!", "Order successfully saved." + (this.orderpunchresponse.schemeinfo == undefined ? "" : this.orderpunchresponse.schemeinfo));else this.commonfun.presentAlert("", "", "Order Punched Successfully." + "<br/><br/>" + this.loginauth.selectedactivity.disclaimer_text + (this.orderpunchresponse.schemeinfo == undefined ? "" : this.orderpunchresponse.schemeinfo)); // this.commonfun.presentAlert("Message","Success",);

              this.Resetpage();
            } else {
              // this.Resetpage();
              this.commonfun.presentAlert("message", "Fail", this.orderpunchresponse.logmsg);
            }
          }
        }, error => {
          this.commonfun.loadingDismiss();
          this.commonfun.presentAlert("message", "Fail", error);
        });
      } else {
        if (this.msg.isplatformweb == true) {
          this.neworderservice.orderPunchWithPdf(jsondata).subscribe(data => {
            this.commonfun.loadingDismiss();

            if (data != null) {
              this.orderpunchresponse = data;

              if (this.orderpunchresponse.resposemsg == "success") {
                if (stype == "N") this.commonfun.presentAlert("Message", "Success!", "Order successfully saved." + (this.orderpunchresponse.schemeinfo == undefined ? "" : this.orderpunchresponse.schemeinfo));else this.commonfun.presentAlert("", "", "Order Punched Successfully." + "<br/><br/>" + this.loginauth.selectedactivity.disclaimer_text + (this.orderpunchresponse.schemeinfo == undefined ? "" : this.orderpunchresponse.schemeinfo)); // this.commonfun.presentAlert("Message","Success",);

                this.Resetpage();
              } else {
                // this.Resetpage();
                this.commonfun.presentAlert("message", "Fail", this.orderpunchresponse.logmsg);
              }
            }
          }, error => {
            this.commonfun.loadingDismiss();
            this.commonfun.presentAlert("message", "Fail", error);
          });
        } else if (this.platform.is('android')) {
          this.neworderservice.uploadPDFFileServiceAndroidiOS(jsondata, this.selectedURI).subscribe(data => {
            this.commonfun.loadingDismiss();

            if (data != null) {
              this.orderpunchresponse = data;

              if (this.orderpunchresponse.resposemsg == "success") {
                if (stype == "N") this.commonfun.presentAlert("Message", "Success!", "Order successfully saved." + (this.orderpunchresponse.schemeinfo == undefined ? "" : this.orderpunchresponse.schemeinfo));else this.commonfun.presentAlert("", "", "Order Punched Successfully." + "<br/><br/>" + this.loginauth.selectedactivity.disclaimer_text + (this.orderpunchresponse.schemeinfo == undefined ? "" : this.orderpunchresponse.schemeinfo)); // this.commonfun.presentAlert("Message","Success",);

                this.Resetpage();
              } else {
                // this.Resetpage();
                this.commonfun.presentAlert("message", "Fail", this.orderpunchresponse.logmsg);
              }
            }
          }, error => {
            this.commonfun.loadingDismiss();
            this.commonfun.presentAlert("message", "Fail", error);
          });
        } else if (this.platform.is('ios')) {
          let login = this.loginauth.user;
          let password = this.loginauth.pass;
          const auth = btoa(login + ":" + password);
          let save_file_url = _common_Constants__WEBPACK_IMPORTED_MODULE_14__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.WMobileOrderPunchOB?';
          const fileTransfer = this.transfer.create();
          let options = {
            fileKey: 'POimage',
            fileName: this.fileName,
            params: jsondata,
            headers: {
              'Authorization': 'Basic ' + auth
            }
          };
          fileTransfer.upload(this.selectedURI, save_file_url, options).then(data => {
            // console.log("pravin YESSSSS",data);
            let formatResponse = JSON.parse(data.response); // console.log("File Uplaod Result",formatResponse);

            this.commonfun.loadingDismiss();

            if (data != null) {
              this.orderpunchresponse = formatResponse;

              if (this.orderpunchresponse.resposemsg == "success") {
                if (stype == "N") this.commonfun.presentAlert("Message", "Success!", "Order successfully saved." + (this.orderpunchresponse.schemeinfo == undefined ? "" : this.orderpunchresponse.schemeinfo));else this.commonfun.presentAlert("", "", "Order Punched Successfully." + "<br/><br/>" + this.loginauth.selectedactivity.disclaimer_text + (this.orderpunchresponse.schemeinfo == undefined ? "" : this.orderpunchresponse.schemeinfo)); // this.commonfun.presentAlert("Message","Success",);

                this.Resetpage();
              } else {
                // this.Resetpage();
                this.commonfun.presentAlert("message", "Fail", this.orderpunchresponse.logmsg);
              }
            }
          }, err => {
            this.commonfun.presentAlert("message", "Fail", err);
            this.commonfun.loadingDismiss();
          });
        }
      }
    } catch (error) {
      this.commonfun.loadingDismiss();
      this.commonfun.presentAlert("message", "Fail", error);
    }
  }

  onSaveOrder(fvalue) {
    try {
      this.mergproduct();
      var jsondata = {
        "mbso_copord_id": this.selecteddocumentno == undefined || this.selecteddocumentno == "" || this.selecteddocumentno == null ? "" : this.selecteddocumentno.id,
        "cust_id": fvalue.selectedBusinessPartner.id,
        "activity_id": fvalue.selectedBusinessPartner.mmstOrgAct,
        "CreditLimit": fvalue.selectedBusinessPartner.creditLimit,
        "OverdueInvoice": fvalue.OverdueInvoice == null ? "" : fvalue.OverdueInvoice,
        "Overdueamount": fvalue.Overdueamount == undefined ? "" : fvalue.Overdueamount,
        "template": fvalue.selectedtemplate == undefined ? "" : fvalue.selectedtemplate,
        "ponumber": fvalue.ponumber,
        "ShippingAddress": fvalue.selectedBPaddressshipping,
        "BillingAddress": fvalue.selectedBPaddressbilling,
        "OrderType": fvalue.selectedordertype,
        "complete": "false",
        "POimage": this.POimg64 == undefined ? "" : this.POimg64,
        "usevetcoin": fvalue.tobeRedeem == null ? "" : fvalue.tobeRedeem,
        "vetcoins": fvalue.avaliableRedeem == null ? "" : fvalue.avaliableRedeem,
        "autocalculation": this.autocalculation,
        "orderlevelper": this.orderlevelper,
        "products": this.mergedcart == undefined ? "" : this.mergedcart,
        "orderdiscription": fvalue.orderdescription,
        "expirydate": fvalue.expecteddeliverydate,
        "file_type": "image",
        "special_order": this.sp_order_chk_box ? "Y" : "N",
        "iscancel": this.neworderservice.iscancelpopup
      };

      if (!this.isPrimaryCustomer) {
        jsondata["primary_customer"] = fvalue.selectedPrimaryBusinessPartner.bpid;
      }

      jsondata["is_advance_payment"] = fvalue.isAdvancePaymentChk;
      this.tobeRedeem = Number(fvalue.tobeRedeem);
      this.avaliableRedeem = Number(fvalue.avaliableRedeem);

      if (this.tobeRedeem <= this.avaliableRedeem) {
        if (this.fileType != 'pdf') {
          this.SaveOrder(jsondata, "N");
        } else {
          let formData = new FormData();
          formData.append('POimage', this.POimg64, this.POimg64.name);
          formData.append('mbso_copord_id', this.selecteddocumentno == undefined || this.selecteddocumentno == "" || this.selecteddocumentno == null ? "" : this.selecteddocumentno.id);
          formData.append('cust_id', fvalue.selectedBusinessPartner.id);
          formData.append('activity_id', fvalue.selectedBusinessPartner.mmstOrgAct);
          formData.append('CreditLimit', fvalue.selectedBusinessPartner.creditLimit);
          formData.append('OverdueInvoice', fvalue.OverdueInvoice == null ? "" : fvalue.OverdueInvoice);
          formData.append('Overdueamount', fvalue.Overdueamount == undefined ? "" : fvalue.Overdueamount);
          formData.append('template', fvalue.selectedtemplate == undefined ? "" : fvalue.selectedtemplate);
          formData.append('ponumber', fvalue.ponumber);
          formData.append('ShippingAddress', fvalue.selectedBPaddressshipping);
          formData.append('BillingAddress', fvalue.selectedBPaddressbilling);
          formData.append('OrderType', fvalue.selectedordertype);
          formData.append('complete', "false");
          formData.append('usevetcoin', fvalue.tobeRedeem == null ? "" : fvalue.tobeRedeem);
          formData.append('vetcoins', fvalue.avaliableRedeem == null ? "" : fvalue.avaliableRedeem);
          formData.append('autocalculation', this.autocalculation);
          formData.append('orderlevelper', this.orderlevelper);
          formData.append('products', JSON.stringify(this.mergedcart));
          formData.append('orderdiscription', fvalue.orderdescription);
          formData.append('expirydate', fvalue.expecteddeliverydate);
          formData.append('file_type', this.fileType);
          formData.append('special_order', fvalue.specialOrderCtrl ? "Y" : "N");
          formData.append("iscancel", this.neworderservice.iscancelpopup ? "true" : "false");

          if (!this.isPrimaryCustomer) {
            formData.append('primary_customer', fvalue.selectedPrimaryBusinessPartner.bpid);
          }

          formData.append('is_advance_payment', fvalue.isAdvancePaymentChk);
          this.SaveOrder(formData, "N");
        }
      } else {
        this.commonfun.presentAlert("Message", "Alert", "Total Points to be Redeem must be less than Points avaliable for Redeem.");
      }

      this.commonfun.loadingDismiss();
    } catch (error) {
      this.commonfun.loadingDismiss();

      if (!this.isPrimaryCustomer) {
        if (fvalue.selectedPrimaryBusinessPartner == null || fvalue.selectedPrimaryBusinessPartner == undefined) {
          this.commonfun.presentAlert("Message", "Error", "Primary customer can not be blank");
        } else {
          this.commonfun.presentAlert("Message", "Error", error);
        }
      } else {
        this.commonfun.presentAlert("Message", "Error", error);
      }
    }
  }

  onCompleteOrder(fvalue) {
    var _this12 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        //console.log("onCompleteOrder");
        _this12.mergproduct();

        if (_this12.fileType != 'pdf') {
          // console.log("!= 'pdf'");
          var jsondatacomplete = {
            "mbso_copord_id": _this12.selecteddocumentno == undefined || _this12.selecteddocumentno == "" || _this12.selecteddocumentno == null ? "" : _this12.selecteddocumentno.id,
            "cust_id": fvalue.selectedBusinessPartner.id,
            "activity_id": fvalue.selectedBusinessPartner.mmstOrgAct,
            "CreditLimit": fvalue.selectedBusinessPartner.creditLimit,
            "OverdueInvoice": fvalue.OverdueInvoice == null ? "" : fvalue.OverdueInvoice,
            "Overdueamount": fvalue.Overdueamount == undefined ? "" : fvalue.Overdueamount,
            "template": fvalue.selectedtemplate == undefined ? "" : fvalue.selectedtemplate,
            "ponumber": fvalue.ponumber,
            "ShippingAddress": fvalue.selectedBPaddressshipping,
            "BillingAddress": fvalue.selectedBPaddressbilling,
            "OrderType": fvalue.selectedordertype,
            "complete": "true",
            "POimage": _this12.POimg64 == undefined ? "" : _this12.POimg64,
            "usevetcoin": fvalue.tobeRedeem == null ? "" : fvalue.tobeRedeem,
            "vetcoins": fvalue.avaliableRedeem == null ? "" : fvalue.avaliableRedeem,
            "autocalculation": _this12.autocalculation,
            "orderlevelper": _this12.orderlevelper,
            "products": _this12.mergedcart == undefined ? "" : _this12.mergedcart,
            "orderdiscription": fvalue.orderdescription,
            "expirydate": fvalue.expecteddeliverydate,
            "file_type": "image",
            "special_order": _this12.sp_order_chk_box ? "Y" : "N",
            "iscancel": _this12.neworderservice.iscancelpopup
          };

          if (!_this12.isPrimaryCustomer) {
            jsondatacomplete["primary_customer"] = fvalue.selectedPrimaryBusinessPartner.bpid;
          }

          jsondatacomplete["is_advance_payment"] = fvalue.isAdvancePaymentChk;

          _this12.SaveOrder(jsondatacomplete, "Y");
        } else {
          // console.log("!= 'pdf else'");
          if (_this12.msg.isplatformweb == true) {
            //  console.log("Array Converete", this.mergedcart[0].value.toString());
            let formData = new FormData();
            formData.append('test', "12345678");
            formData.append('POimage', _this12.POimg64, _this12.POimg64.name);
            formData.append('mbso_copord_id', _this12.selecteddocumentno == undefined || _this12.selecteddocumentno == "" || _this12.selecteddocumentno == null ? "" : _this12.selecteddocumentno.id);
            formData.append('cust_id', fvalue.selectedBusinessPartner.id);
            formData.append('activity_id', fvalue.selectedBusinessPartner.mmstOrgAct);
            formData.append('CreditLimit', fvalue.selectedBusinessPartner.creditLimit);
            formData.append('OverdueInvoice', fvalue.OverdueInvoice == null ? "" : fvalue.OverdueInvoice);
            formData.append('Overdueamount', fvalue.Overdueamount == undefined ? "" : fvalue.Overdueamount);
            formData.append('template', fvalue.selectedtemplate == undefined ? "" : fvalue.selectedtemplate);
            formData.append('ponumber', fvalue.ponumber);
            formData.append('ShippingAddress', fvalue.selectedBPaddressshipping);
            formData.append('BillingAddress', fvalue.selectedBPaddressbilling);
            formData.append('OrderType', fvalue.selectedordertype);
            formData.append('complete', "true");
            formData.append('usevetcoin', fvalue.tobeRedeem == null ? "" : fvalue.tobeRedeem);
            formData.append('vetcoins', fvalue.avaliableRedeem == null ? "" : fvalue.avaliableRedeem);
            formData.append('autocalculation', _this12.autocalculation);
            formData.append('orderlevelper', _this12.orderlevelper);
            formData.append('products', JSON.stringify(_this12.mergedcart));
            formData.append('orderdiscription', fvalue.orderdescription);
            formData.append('expirydate', fvalue.expecteddeliverydate);
            formData.append('file_type', _this12.fileType);
            formData.append('special_order', _this12.sp_order_chk_box ? "Y" : "N");
            formData.append("iscancel", _this12.neworderservice.iscancelpopup ? "true" : "false");

            if (!_this12.isPrimaryCustomer) {
              formData.append('primary_customer', fvalue.selectedPrimaryBusinessPartner.bpid);
            }

            formData.append('is_advance_payment', fvalue.isAdvancePaymentChk);

            _this12.SaveOrder(formData, "Y");
          } else {
            let tempVetcoins = fvalue.avaliableRedeem;

            if (typeof tempVetcoins === 'string' || tempVetcoins instanceof String) {} else {
              tempVetcoins = JSON.stringify(tempVetcoins);
            }

            let tempUseVetCoin = fvalue.tobeRedeem;

            if (typeof tempUseVetCoin === 'string' || tempUseVetCoin instanceof String) {} else {
              tempUseVetCoin = JSON.stringify(tempUseVetCoin);
            }

            let validateData = {
              "mbso_copord_id": _this12.selecteddocumentno == undefined || _this12.selecteddocumentno == "" || _this12.selecteddocumentno == null ? "" : _this12.selecteddocumentno.id,
              "cust_id": fvalue.selectedBusinessPartner.id,
              "activity_id": fvalue.selectedBusinessPartner.mmstOrgAct,
              "CreditLimit": fvalue.selectedBusinessPartner.creditLimit,
              "OverdueInvoice": fvalue.OverdueInvoice == null ? "" : fvalue.OverdueInvoice,
              "Overdueamount": fvalue.Overdueamount == undefined ? "" : fvalue.Overdueamount,
              "template": fvalue.selectedtemplate == undefined ? "" : fvalue.selectedtemplate,
              "ponumber": fvalue.ponumber == null ? "" : fvalue.ponumber,
              "ShippingAddress": fvalue.selectedBPaddressshipping,
              "BillingAddress": fvalue.selectedBPaddressbilling,
              "OrderType": fvalue.selectedordertype == null ? "" : fvalue.selectedordertype,
              "complete": "true",
              "usevetcoin": fvalue.tobeRedeem == null ? "" : tempUseVetCoin,
              "vetcoins": fvalue.avaliableRedeem == null ? "" : tempVetcoins,
              "autocalculation": _this12.autocalculation == null ? "" : JSON.stringify(_this12.autocalculation),
              "orderlevelper": _this12.orderlevelper == undefined ? "" : JSON.stringify(_this12.orderlevelper),
              "products": JSON.stringify(_this12.mergedcart),
              "orderdiscription": fvalue.orderdescription == null ? "" : fvalue.orderdescription,
              "expirydate": fvalue.expecteddeliverydate == null ? "" : fvalue.expecteddeliverydate,
              "file_type": "pdf",
              "special_order": _this12.sp_order_chk_box ? "Y" : "N",
              "iscancel": _this12.neworderservice.iscancelpopup
            };

            if (!_this12.isPrimaryCustomer) {
              validateData["primary_customer"] = fvalue.selectedPrimaryBusinessPartner.bpid;
            }

            validateData["is_advance_payment"] = fvalue.isAdvancePaymentChk; //  console.log("PAram", validateData);

            _this12.SaveOrder(validateData, "Y");
          } // let formatResponse = await this.neworderservice.orderPunchWithPdf(formData).toPromise();
          //   if(!!formatResponse){
          //       console.log(formatResponse);
          //   }

        }
      } catch (error) {
        // this.commonfun.loadingDismiss();
        if (!_this12.isPrimaryCustomer) {
          if (fvalue.selectedPrimaryBusinessPartner == null || fvalue.selectedPrimaryBusinessPartner == undefined) {
            _this12.commonfun.presentAlert("Message", "Error", "Primary customer can not be blank");
          } else {
            _this12.commonfun.presentAlert("Message", "Error", error);
          }
        } else {
          _this12.commonfun.presentAlert("Message", "Error", error);
        }
      }
    })();
  }

  getpoImage() {
    var _this13 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const alert = yield _this13.alertCtrl.create({
          header: 'Select Option',
          message: "Select Option to get Picture.",
          buttons: [{
            text: 'Gallery',
            handler: () => {
              _this13.getimage();
            }
          }, {
            text: 'Camera',
            handler: () => {
              _this13.takePicture();
            }
          }, {
            text: 'PDF File',
            handler: () => {
              _this13.uploadPDFDevice();
            }
          }]
        });
        yield alert.present();
      } catch (error) {
        _this13.commonfun.presentAlert("Message", "Error", error);
      }
    })();
  }

  uploadPDFDevice() {
    try {
      if (this.platform.is('android')) {
        let filter = {
          "mime": "application/pdf"
        };
        this.fileChooser.open(filter).then(uri => {
          this.selectedURI = uri; //  console.log(this.TAG,"Selected File",uri);

          this.filePath.resolveNativePath(uri).then(filePathResult => {
            //   console.log(this.TAG,"Selected fileInfo",filePathResult);
            this.file.resolveLocalFilesystemUrl(filePathResult).then(fileEntry => {
              // console.log(this.TAG,"Selected fileInfo fileEntry",fileEntry);
              this.getFileSize(fileEntry).then(metadata => {
                if (metadata.size / 1024 / 1024 > 3) {
                  //  console.log(this.TAG,"File Size Too Large");
                  this.commonfun.presentAlert("New Order", "Validation", "File size must be less than Equal to 3 MB");
                  this.POimg64 = null;
                  this.fileName = "";
                  this.fileType = "";
                  this.selectedURI = "";
                } else {
                  //  console.log(this.TAG,"File Size IS OK");
                  this.fileName = filePathResult.substring(filePathResult.lastIndexOf("/") + 1);
                  this.fileType = filePathResult.substring(filePathResult.lastIndexOf(".") + 1);
                }
              });
            }).catch(error => {
              this.commonfun.presentAlert("New Order", "Error", error);
            });
          });
        }).catch(e => console.log(e));
      } else if (this.platform.is('ios')) {
        this.filePicker.pickFile().then(uri => {
          this.selectedURI = uri;
          this.fileName = uri.substring(uri.lastIndexOf("/") + 1);
          this.fileType = uri.substring(uri.lastIndexOf(".") + 1);
        }).catch(err => console.log('File Picker Error', err));
      }
    } catch (error) {}
  }

  getFileSize(FileEntry) {
    let self = this;
    let promise = new Promise(function (resolve, reject) {
      FileEntry.getMetadata(metadata => {
        resolve(metadata);
      });
    });
    return promise;
  }

  ImageViewr(img) {
    var _this14 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this14.alertCtrl.create({
        message: '<div>' + '<img class="viewImagecss" src="data:image/jpeg;base64,' + img + '">' + '</div>',
        buttons: [{
          text: 'Remove',
          handler: () => {
            _this14.POimg64 = null;
            _this14.fileName = "";
          }
        }, {
          text: 'OK'
        }]
      });
      yield alert.present();
    })();
  }

  editOrder(edtitdocId) {
    try {
      if (edtitdocId != '' && edtitdocId != undefined) {
        this.commonfun.loadingPresent();
        this.neworderservice.geteditorderheader(edtitdocId).subscribe(data => {
          this.commonfun.loadingDismiss();
          const response = data['response'];
          this.orderheader = response.data; //bind Category

          this.formprod.controls["selectedBusinessPartner"].setValue(this.orderheader[0].BPartner);
          this.formprod.controls["ponumber"].setValue(this.orderheader[0].ponumber);
        }, error => {
          this.commonfun.loadingDismiss(); //  console.log("geteditorderheader:Erroe", error);
        });
      }
    } catch (error) {
      //  console.log("editOrder:Erroe", error);
      this.commonfun.loadingDismiss();
    }
  } //Capture Image from Camera


  takePicture() {
    try {
      const options = {
        quality: 50,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType.CAMERA,
        targetWidth: 1500,
        targetHeight: 1500
      };
      this.camera.getPicture(options).then(imageData => {
        this.POimg64 = imageData;
        this.fileName = "";
        this.fileType = ""; // return this.img64;
      }, err => {
        this.commonfun.presentAlert("Message", "Error", err);
      });
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  } //Select Image from library


  getimage() {
    try {
      const options = {
        quality: 50,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        targetWidth: 1500,
        targetHeight: 1500
      };
      this.camera.getPicture(options).then(imageData => {
        this.POimg64 = imageData;
        this.fileName = "";
        this.fileType = "";
      }, err => {
        this.commonfun.presentAlert("Message", "Error", err);
      });
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

};

NeworderPage.ctorParameters = () => [{
  type: _neworder_service__WEBPACK_IMPORTED_MODULE_4__.NeworderService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_21__.Router
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_21__.ActivatedRoute
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_18__.FormBuilder
}, {
  type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_5__.LoginauthService
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_22__.AlertController
}, {
  type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_6__.Commonfun
}, {
  type: _addeditproduct_addeditproduct_service__WEBPACK_IMPORTED_MODULE_7__.AddeditproductService
}, {
  type: _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_8__.Camera
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_22__.Platform
}, {
  type: _provider_message_helper__WEBPACK_IMPORTED_MODULE_9__.Message
}, {
  type: _upload_upload_upload_service__WEBPACK_IMPORTED_MODULE_10__.UploadService
}, {
  type: _ionic_native_file_chooser_ngx__WEBPACK_IMPORTED_MODULE_11__.FileChooser
}, {
  type: _awesome_cordova_plugins_file_path_ngx__WEBPACK_IMPORTED_MODULE_12__.FilePath
}, {
  type: _ionic_native_file_picker_ngx__WEBPACK_IMPORTED_MODULE_13__.IOSFilePicker
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_22__.ModalController
}, {
  type: _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_15__.FileTransfer
}, {
  type: _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_16__.File
}];

NeworderPage.propDecorators = {
  portComponent: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_23__.ViewChild,
    args: ['portComponent', {
      static: false
    }]
  }]
};
NeworderPage = (0,tslib__WEBPACK_IMPORTED_MODULE_24__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_23__.Component)({
  selector: 'app-neworder',
  template: _neworder_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_neworder_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], NeworderPage);


/***/ }),

/***/ 17216:
/*!**********************************************!*\
  !*** ./src/app/neworder/neworder.service.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NeworderService": () => (/* binding */ NeworderService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var _common_Constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/Constants */ 68209);
/* harmony import */ var _awesome_cordova_plugins_http_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @awesome-cordova-plugins/http/ngx */ 26123);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ 12378);








let NeworderService = class NeworderService {
    constructor(http, loginauth, genericHTTP, cordovaHTTP) {
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
        this.cordovaHTTP = cordovaHTTP;
        this.iscancelpopup = true;
        this.isadvancepaymentcheck = false;
        this.TAG = "New Order Service";
    }
    getdocumentidbycust(businessPartner_id) {
        // businessPartner_id="FFF20190328042044745CEDE4F2E670B";
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/mbso_copord?'
            + '_where=docstatus=\'DR\'%20and%20bpartner=\'' + businessPartner_id + '\'');
    }
    getproductbydocumentid(docid) {
        //  businessPartner_id="FFF20190328042044745CEDE4F2E670B";
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/mbso_copord_line?'
            + '_where=mbsoCopord=\'' + docid + '\'');
    }
    getproductbydocumentidapi(docid) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileExistingOrderEdit?'
            + 'existingorder_id=' + docid);
    }
    getexistcustmersearch(sfname, organization) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/BusinessPartner?'
            + '_selectedProperties=id,_identifier,mmstOrgAct$_identifier,mmstOrgAct,creditLimit,mmstArCreditlimit,businessPartnerCategory&'
            + '_where=active=true%20and%20(mmstOrgAct IN (' + organization + ')) and (lower(translate(name,\' \',\'\')) LIKE lower(\'%25' + sfname + '%25\'))');
    }
    getnewordercustmersearchapi(strsearch, ordertypeionic) {
        strsearch = strsearch.replace(/ /g, "%20");
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileUserWiseCustomer?'
            + 'user_id=' + this.loginauth.userid
            + '&strsearch=' + strsearch
            + '&ordertypeionic=' + ordertypeionic
            + '&activity_id=' + this.loginauth.selectedactivity.id);
    }
    getPrimaryCustomerService(bp_id, strsearch, ordertypeionic) {
        strsearch = strsearch.replace(/ /g, "%20");
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.BusinessPartnerDetails?'
            + 'user_id=' + this.loginauth.userid
            + '&strsearch=' + strsearch + '&ordertypeionic=' + ordertypeionic
            + '&activity_id=' + this.loginauth.selectedactivity.id
            + '&bp_id=' + bp_id);
    }
    getexistcustmersearchapi(strsearch, selectedactivity) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileActivityWiseCustomer?'
            + 'activity_id=' + selectedactivity
            + '&strsearch=' + strsearch
            + '&user_id=' + this.loginauth.userid);
    }
    getBPCategory(businessPartnerCategory) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/BusinessPartnerCategory?'
            + '_where=id=\'' + businessPartnerCategory + '\'');
    }
    bindBusinessPartner() {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/BusinessPartner?');
    }
    activitywiseBusinessPartner(organization) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/BusinessPartner?'
            + '_selectedProperties=id,_identifier,mmstOrgAct$_identifier,mmstOrgAct,creditLimit,mmstArCreditlimit,businessPartnerCategory&'
            + '_where=active=true%20and%20mmstOrgAct IN (' + organization + ')');
    }
    getcustmerbillingaddress(businessPartner_id) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/BusinessPartnerLocation?'
            + '_selectedProperties=id,name,shipToAddress,invoiceToAddress&'
            + '_where=active=true%20and%20businessPartner=\'' + businessPartner_id + '\'');
    }
    getordertypeby(WarehouseId) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/Warehouse?'
            + '_selectedProperties=id,mmstWarehouseCode&'
            + '_where=active=true and id=\'' + WarehouseId + '\'', false, true);
    }
    getoverdueinvoiceamt(businessPartner_id, order_id) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileOverdueReward?'
            + 'cust_id=' + businessPartner_id + '&user_id=' + this.loginauth.userid
            + '&order_id=' + order_id
        // + '_selectedProperties=id,name,shipToAddress,invoiceToAddress&' 
        // + '_where=businessPartner=\'' + businessPartner_id + '\''
        );
    }
    checkForCashDiscountPopup(body) {
        console.log("Body: ", body);
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.webservice.checkForCashDiscountPopup', body, httpOptions);
    }
    onCancelCashDiscount(body) {
        console.log("Body: ", body);
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.webservice.OnCancelCashDiscount', body, httpOptions);
    }
    checkCOPBlockOrder(body) {
        console.log("Body: ", body);
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.webservice.CheckCOPBlockOrder', body, httpOptions);
    }
    getOrdertemplate(businessPartner_id) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/mbso_moordtemp?'
            + '_selectedProperties=id,sname&'
            + '_where=active=true%20and%20bpartner=\'' + businessPartner_id + '\'', false, true);
    }
    SaveTemplate(template) {
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                //  'Authorization':'Basic UDJhZG1pbjpQYXNzMjAyMA=='
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileSaveTemplate', template, httpOptions);
    }
    OrderPunch(Order) {
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileOrderPunchOB', Order, httpOptions);
    }
    orderPunchWithPdf(data) {
        try {
            let login = this.loginauth.user;
            let password = this.loginauth.pass;
            const auth = btoa(login + ":" + password);
            const httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders({
                    'Authorization': 'Basic ' + auth
                })
            };
            let save_quotation = _common_Constants__WEBPACK_IMPORTED_MODULE_2__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.WMobileOrderPunchOB?';
            return this.genericHTTP.fileDevicePost(save_quotation, data, httpOptions);
        }
        catch (error) {
        }
    }
    uploadPDFFileServiceAndroidiOS(data, path) {
        try {
            let login = this.loginauth.user;
            let password = this.loginauth.pass;
            this.cordovaHTTP.setDataSerializer('multipart');
            const filePath = [path];
            const auth = btoa(login + ":" + password);
            const httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders({
                    'Authorization': 'Basic ' + auth
                })
            };
            let auth1 = httpOptions.headers.get('Authorization');
            let specificHeader = {
                'Authorization': auth1
            };
            console.log(this.TAG, "file upload data", data);
            let save_file_url = _common_Constants__WEBPACK_IMPORTED_MODULE_2__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.WMobileOrderPunchOB?';
            return rxjs__WEBPACK_IMPORTED_MODULE_5__.Observable.create((observer) => {
                this.cordovaHTTP.uploadFile(save_file_url, data, specificHeader, filePath, "POimage").then((response) => {
                    let data;
                    if (!!response.data) {
                        data = JSON.parse(response.data);
                    }
                    else {
                        data = response;
                    }
                    observer.next(data);
                    observer.complete();
                }).catch((error) => {
                    throw error;
                    //  this.commonFunction.loadingDismiss();
                    //  console.log(this.TAG, "file upload error", error);
                });
            });
        }
        catch (error) {
            console.log(this.TAG, error);
        }
    }
    geteditorderheader(edtitdocId) {
        return null;
    }
    getExpDateDelivery(bid) {
        try {
            let expDateDeliveryURL = _common_Constants__WEBPACK_IMPORTED_MODULE_2__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.GetExpiryDate?' +
                'bpid=' + bid;
            //  console.log("Get Exp Date Delivery Url",expDateDeliveryURL);
            return this.genericHTTP.get(expDateDeliveryURL);
        }
        catch (error) {
            // console.log(error);
        }
    }
    getOrderType(bid, mmstOrgAct) {
        try {
            let getOrderTypeURL = _common_Constants__WEBPACK_IMPORTED_MODULE_2__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.OrderType?' +
                'business_partner_id=' + bid + '&mmstOrgAct=' + mmstOrgAct + '&user_id=' + this.loginauth.userid;
            //  console.log("Get Exp Date Delivery Url",expDateDeliveryURL);
            return this.genericHTTP.get(getOrderTypeURL);
        }
        catch (error) {
            // console.log(error);
        }
    }
    checkUniqueTemplateName(template_name) {
        try {
            let checkUniqueTemplateNameURL = "https://jsonplaceholder.typicode.com/comments?postId=1";
            return this.genericHTTP.get(checkUniqueTemplateNameURL);
        }
        catch (error) {
            console.log(error);
        }
    }
};
NeworderService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService },
    { type: _awesome_cordova_plugins_http_ngx__WEBPACK_IMPORTED_MODULE_3__.HTTP }
];
NeworderService = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Injectable)({
        providedIn: 'root'
    })
], NeworderService);



/***/ }),

/***/ 44159:
/*!******************************************************************!*\
  !*** ./src/app/order-approval/order-approval-service.service.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OrderApprovalServiceService": () => (/* binding */ OrderApprovalServiceService)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var _common_Constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/Constants */ 68209);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ 84505);








let OrderApprovalServiceService = class OrderApprovalServiceService {
  constructor(http, genericHTTP, loginauth) {
    this.http = http;
    this.genericHTTP = genericHTTP;
    this.loginauth = loginauth;
    this.TAG = "OrderApprovalServiceService";
    this.allFilterClearStatus = false;
    this.filterTab = [];
    this.filterOrg = [];
    this.filterDocType = [];
    this.filterBusinessPartner = '';
    this.filterselectedBusinessPartner = '';
    this.pageOffset = 0;
    this.messageSource = new rxjs__WEBPACK_IMPORTED_MODULE_4__.BehaviorSubject('default message');
    this.currentMessage = this.messageSource.asObservable();
  }

  changeMessage(message) {
    this.messageSource.next(message);
  }

  getOrder(URL) {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      let methodTAG = 'getPlan'; // console.log(this.TAG,"Filter URL",URL);

      try {
        //  console.log(this.TAG,URL);
        return yield _this.genericHTTP.get(URL);
      } catch (error) {}
    })();
  }

  saveOrderStatus(id, record, tab_id, status, remark) {
    let data = {
      "inprecord": record,
      "inpmappApprovalId": id,
      "inpadTabId": tab_id,
      "user_id": this.loginauth.userid,
      "_params": {
        "ActionList": status,
        "Remarks": remark
      }
    };
    let login = this.loginauth.user;
    let password = this.loginauth.pass;
    const auth = btoa(login + ":" + password);
    const httpOptions = {
      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Basic ' + auth
      })
    };
    let SAVEURL = _common_Constants__WEBPACK_IMPORTED_MODULE_2__.Constants.DOMAIN_URL + '/openbravo' + "/ws/in.mbs.webservice.OrderStatusApprove?"; //  console.log(this.TAG,SAVEURL);

    return this.genericHTTP.post(SAVEURL, data, httpOptions);
  }

  getFilterData() {
    try {
      let url = _common_Constants__WEBPACK_IMPORTED_MODULE_2__.Constants.DOMAIN_URL + '/openbravo' + "/ws/in.mbs.webservice.Filter?" + 'user_id=' + this.loginauth.userid + '&activity_id=' + this.loginauth.selectedactivity.id + '&action=' + 'all';
      return this.genericHTTP.get(url);
    } catch (error) {}
  }

  getBusinessPartnerData(searchkey) {
    try {
      return this.genericHTTP.get(_common_Constants__WEBPACK_IMPORTED_MODULE_2__.Constants.DOMAIN_URL + '/openbravo' + "/ws/in.mbs.webservice.Filter?" + 'user_id=' + this.loginauth.userid + '&action=' + 'bs' + '&activity_id=' + this.loginauth.selectedactivity.id + '&searchkey=' + searchkey);
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

};

OrderApprovalServiceService.ctorParameters = () => [{
  type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient
}, {
  type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService
}, {
  type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__.LoginauthService
}];

OrderApprovalServiceService = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Injectable)({
  providedIn: 'root'
})], OrderApprovalServiceService);


/***/ }),

/***/ 47214:
/*!***************************************************!*\
  !*** ./src/app/product-list/product-list.page.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProductListPage": () => (/* binding */ ProductListPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _product_list_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./product-list.page.html?ngResource */ 38636);
/* harmony import */ var _product_list_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./product-list.page.scss?ngResource */ 47068);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _addeditproduct_addeditproduct_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../addeditproduct/addeditproduct.service */ 53899);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var src_provider_message_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/provider/message-helper */ 98792);
/* harmony import */ var _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../neworder/neworder.service */ 17216);











let ProductListPage = class ProductListPage {
  constructor(neworderservice, formBuilder, addEditProductService, commonfun, viewCtrl, alertController, modalCtrl, msg) {
    this.neworderservice = neworderservice;
    this.formBuilder = formBuilder;
    this.addEditProductService = addEditProductService;
    this.commonfun = commonfun;
    this.viewCtrl = viewCtrl;
    this.alertController = alertController;
    this.modalCtrl = modalCtrl;
    this.msg = msg;
    this.TAG = "ProductListPage";
    this.isTempCartProduct = false;
    this.editMode = false;
  }

  ngOnInit() {
    this.cust_id = this.cust_id;
    this.activityid = this.activity_id;
    this.selectedBPBillingAddress = this.bpBillingAddress;
    this.selectedBPShippingAddress = this.bpShippingAddress;
    this.orderType = this.orderType;
    this.tempCartProductList = this.tempProductSelectedOn;
    this.selectedFilter = this.selectedFilter;
    this.editMode = this.editMode;
    this.editedProduct = this.productToBeEdit;
    this.special_order_product = this.special_order_add_edit_param;
    this.issplorderonfreeqty = this.issplorderonfreeqty;
    this.is_advance_payment_product_param = this.is_advance_payment_filter;
    this.productListForm = this.formBuilder.group({
      items: this.formBuilder.array([])
    });
  }

  ionViewWillEnter() {
    // console.log(this.TAG,this.editMode);
    if (this.editMode == false) {
      if (!!this.selectedFilter && this.selectedFilter.length != 0) {
        this.bindProductFromApi("", this.selectedFilter);
      }
    } else {
      this.productList = [{
        "id": this.editedProduct.product_id,
        "_identifier": this.editedProduct.product,
        "shipperqty": this.editedProduct.shipperqty,
        "minorderqty": this.editedProduct.minorderqty,
        "isreeproduct": this.editedProduct.isreeproduct,
        "enteredfreeqty": this.editedProduct.enteredfreeqty,
        "MainProductQty": this.editedProduct.MainProductQty
      }];
      this.createItem();
    }
  }

  cancel() {
    try {} catch (error) {
      console.log(error);
    }
  }

  onChangeProductSearch(event) {
    try {
      if (event.detail.value.length != 0 && event.detail.value.length >= 3) {
        this.bindProductFromApi(event.detail.value, this.selectedFilter);
      } else if (event.detail.value.length == 0) {
        this.productList = [];
      }
    } catch (error) {
      console.log(error);
    }
  }

  onAddToCart(product, i) {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.commonfun.loadingPresent();

      try {
        const controlArray = _this.productListForm.get('items');

        if (!!_this.productListForm.get('items').value[i].key || !!_this.productListForm.get('items').value[i].freeqty) {
          _this.selectedIndex = i;

          let QtyO = _this.productListForm.get('items').value[i].key; //check for free qty


          let FreeQty;

          if (_this.special_order_product === 'Y' && product.isreeproduct && _this.issplorderonfreeqty) {
            FreeQty = _this.productListForm.get('items').value[i].freeqty;
          }

          let Qty = 0;

          if (QtyO === null || QtyO === '') {
            QtyO = 0;
          }

          if (FreeQty === null || FreeQty === '' || FreeQty === undefined) {
            FreeQty = 0;
          }

          Qty = +QtyO + +FreeQty;

          if (Qty >= product.minorderqty) {
            if (Qty % product.shipperqty == 0) {
              yield _this.getProductDetailAPI(QtyO, FreeQty, 'save', product.id, i, product);
            } else {
              _this.commonfun.presentAlert("Message", "Alert!", "Quantity must be divisible by " + product.shipperqty);
            }
          } else {
            _this.commonfun.presentAlert("Message", "Alert!", "Quantity must be greater than or equal to " + product.minorderqty);
          }
        } else {
          _this.commonfun.presentAlert("Message", "Alert!", "Please Enter Quantity");
        }

        _this.commonfun.loadingDismiss();
      } catch (error) {
        console.log(error);

        _this.commonfun.loadingDismiss();
      }
    })();
  }

  bindProductFromApi(txt, filter) {
    try {
      this.addEditProductService.getFilterProductService(this.cust_id, txt, this.selectedBPBillingAddress, this.selectedBPShippingAddress, this.orderType, filter).subscribe(data => {
        var response = data;
        this.productList = response; //  console.log("Product Data",response);

        this.productListForm.get('items').clear();
        this.createItem();
      }, error => {
        this.commonfun.presentAlert("Message", "Error!", error.statusText + " with status code :" + error.status);
      });
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  createItem() {
    this.items = this.productListForm.get('items');
    this.productList.forEach(elem => {
      const ctrl = this.formBuilder.group({
        key: [elem.MainProductQty, [_angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required]],
        freeqty: [elem.enteredfreeqty],
        test: ''
      });
      this.items.push(ctrl);
    });
  }

  getProductDetailAPI(Qty, FreeQty, saveoradd, product_id, index, product) {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        // console.log(this.TAG,"Product Detail API Started",this.todaysDataTime);
        let tempProductDetailResponse = yield _this2.addEditProductService.getproductdetail(_this2.activityid, _this2.cust_id, product_id, Qty, "", _this2.orderType, _this2.selectedBPShippingAddress, _this2.selectedBPBillingAddress, _this2.special_order_product, _this2.is_advance_payment_product_param, FreeQty).toPromise();

        if (tempProductDetailResponse != null) {
          tempProductDetailResponse = tempProductDetailResponse.reverse();

          _this2.addProductToCart(tempProductDetailResponse, index, Qty, FreeQty, product);
        }
      } catch (error) {
        console.log(error);
      }
    })();
  }

  addProductToCart(productDetailResponse, index, Qty, FreeQty, product) {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        // console.log(this.TAG,"Product Detail Response",productDetailResponse);
        if (_this3.tempCartProductList == undefined || _this3.tempCartProductList == null) {
          let body = {
            "productlist": productDetailResponse
          };

          _this3.neworderservice.checkCOPBlockOrder(body).subscribe(data => {
            if (data.resposemsg === 'Success') {
              _this3.tempCartProductList = productDetailResponse;
              _this3.productList[index].isSelected = true;
            } else {
              _this3.commonfun.presentAlert("Message", "Error", data.logmsg);

              return;
            }
          }, error => {
            _this3.commonfun.presentAlert("Message", "Error", error);

            return;
          }); // console.log(this.TAG,"Product Added",this.productList);

        } else {
          //first remove same product
          productDetailResponse.forEach(element => {
            var sameProduct = _this3.tempCartProductList.find(e => e.MainProductid === element.MainProductid);

            if (sameProduct != null || sameProduct != undefined) {
              _this3.removeProduct(sameProduct);
            }
          });

          for (var c = 0; c < productDetailResponse.length; c++) {
            let productresponse = productDetailResponse[c]; //console.log('inside',this.tempCartProductList)
            // let productlist =[];
            // productlist=this.tempCartProductList;
            // productlist.push(productDetailResponse[c]);

            let body = {
              "productlist": [..._this3.tempCartProductList, productresponse]
            };

            _this3.neworderservice.checkCOPBlockOrder(body).subscribe(data => {
              if (data.resposemsg === 'Success') {
                _this3.tempCartProductList = [..._this3.tempCartProductList, productresponse];
                _this3.productList[index].isSelected = true;
              } else {
                _this3.commonfun.presentAlert("Message", "Error", data.logmsg);

                return;
              }
            }, error => {
              _this3.commonfun.presentAlert("Message", "Error", error);

              return;
            });
          }
        }

        _this3.isCartEmpty();
      } catch (error) {
        console.log(error);
      }
    })();
  }

  removeProduct(post) {
    try {
      let index = this.tempCartProductList.indexOf(post);
      const result = this.tempCartProductList.filter(item => item.MainProductid != post.MainProductid);
      this.tempCartProductList = result;
      this.isCartEmpty();
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  isCartEmpty() {
    if (this.tempCartProductList != null && this.tempCartProductList.length > 0) {
      this.isTempCartProduct = true;
    } else {
      this.isTempCartProduct = false;
    }
  }

  checkProductIsExits(product_id) {
    try {
      if (this.tempCartProductList != undefined) {
        const result = this.tempCartProductList.filter(item => item.product_id == product_id && item.MainProductid == product_id);

        if (result.length > 0) {
          return true; //  this.commonfun.presentAlert("Message", "Alert", "This product is already added with " + result[0].MainProductQty + " quantity.")
        } else {
          return false;
        }
      }

      this.isCartEmpty();
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  onClose() {
    try {
      this.modalCtrl.dismiss(this.tempCartProductList);
    } catch (error) {
      console.log(error);
    }
  }

  numberOnly(event) {
    try {
      const charCode = event.which ? event.which : event.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }

      return true;
    } catch (error) {
      console.log(error);
    }
  }

};

ProductListPage.ctorParameters = () => [{
  type: _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_6__.NeworderService
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormBuilder
}, {
  type: _addeditproduct_addeditproduct_service__WEBPACK_IMPORTED_MODULE_3__.AddeditproductService
}, {
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__.Commonfun
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.ModalController
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.AlertController
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.ModalController
}, {
  type: src_provider_message_helper__WEBPACK_IMPORTED_MODULE_5__.Message
}];

ProductListPage.propDecorators = {
  cust_id: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_9__.Input
  }]
};
ProductListPage = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
  selector: 'app-product-list',
  template: _product_list_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_product_list_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ProductListPage);


/***/ }),

/***/ 57883:
/*!*************************************************!*\
  !*** ./src/app/upload/upload/upload.service.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UploadService": () => (/* binding */ UploadService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _awesome_cordova_plugins_http_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @awesome-cordova-plugins/http/ngx */ 26123);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ 12378);
/* harmony import */ var src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/common/Constants */ 68209);
/* harmony import */ var src_app_common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/common/generic-http-client.service */ 28475);
/* harmony import */ var src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/login/loginauth.service */ 44010);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/provider/commonfun */ 51156);









let UploadService = class UploadService {
    constructor(genericHttpClientService, loginService, httpClient, commonFunction, cordovaHTTP) {
        this.genericHttpClientService = genericHttpClientService;
        this.loginService = loginService;
        this.httpClient = httpClient;
        this.commonFunction = commonFunction;
        this.cordovaHTTP = cordovaHTTP;
        this.TAG = "Upload Service";
    }
    /**
     * @description This method will fetch list for Master Category List from muul_interfacecategory table.
     * @author Pravin Bhosale.
     * @param null
     * @returns {Json Array} Master Category List.
     * @kind Service Function
     */
    getMasterCategoryList() {
        try {
            let getMasterCategoryURL = this.loginService.commonurl + 'ws/in.mbs.webservice.UploadService?'
                + this.loginService.parameter
                + '&user_id=' + this.loginService.userid
                + '&action=' + 'master_category_list'
                + '&activity_id=' + this.loginService.selectedactivity.id;
            console.log(this.TAG, "getMasterCategory", getMasterCategoryURL);
            return this.genericHttpClientService.get(getMasterCategoryURL);
        }
        catch (error) {
            console.log(this.TAG, error);
        }
    }
    getMasterSubCategoryList(master_id) {
        try {
            let getMasterSubCategoryURL = this.loginService.commonurl + 'ws/in.mbs.webservice.UploadService?'
                + this.loginService.parameter
                + '&user_id=' + this.loginService.userid
                + '&action=' + 'master_sub_category_list'
                + '&master_id=' + master_id
                + '&activity_id=' + this.loginService.selectedactivity.id;
            console.log(this.TAG, "getMasterSubCategoryList", getMasterSubCategoryURL);
            return this.genericHttpClientService.get(getMasterSubCategoryURL);
        }
        catch (error) {
            console.log(this.TAG, error);
        }
    }
    getMasterNameList(master_id, master_sub_category_id) {
        try {
            let getMasterNameURL = this.loginService.commonurl + 'ws/in.mbs.webservice.UploadService?'
                + this.loginService.parameter
                + '&user_id=' + this.loginService.userid
                + '&activity_id=' + this.loginService.selectedactivity.id
                + '&action=' + 'master_name_list'
                + '&master_id=' + master_id
                + '&master_sub_category_id=' + master_sub_category_id;
            //console.log(this.TAG, "getMasterNameList", getMasterNameURL);
            return this.genericHttpClientService.get(getMasterNameURL);
        }
        catch (error) {
            console.log(this.TAG, error);
        }
    }
    getOrganizationList(strsearch) {
        try {
            if (!!strsearch) {
                strsearch = strsearch.replace(/ /g, "%20");
            }
            else {
                strsearch = "";
            }
            let getOrganizationListURL = this.loginService.commonurl + 'ws/in.mbs.webservice.WMobileUserWiseOrgActivity?'
                + this.loginService.parameter
                + '&userid=' + this.loginService.userid
                + '&activityid=' + this.loginService.selectedactivity.id;
            //console.log(this.TAG, "getOrganizationList", getOrganizationListURL);
            return this.genericHttpClientService.get(getOrganizationListURL);
        }
        catch (error) {
            console.log(this.TAG, error);
        }
    }
    uploadFileServiceAndroidiOS(data, path) {
        try {
            let login = this.loginService.user;
            let password = this.loginService.pass;
            this.cordovaHTTP.setDataSerializer('multipart');
            this.cordovaHTTP.setRequestTimeout(300);
            const filePath = [path];
            const auth = btoa(login + ":" + password);
            const httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpHeaders({
                    'Authorization': 'Basic ' + auth
                })
            };
            let auth1 = httpOptions.headers.get('Authorization');
            let specificHeader = {
                'Authorization': auth1
            };
            console.log(this.TAG, "file upload data", data);
            let save_file_url = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.UploadService?';
            return rxjs__WEBPACK_IMPORTED_MODULE_6__.Observable.create((observer) => {
                this.cordovaHTTP.uploadFile(save_file_url, data, specificHeader, filePath, "inpFile").then((response) => {
                    let data;
                    if (!!response.data) {
                        data = JSON.parse(response.data);
                    }
                    else {
                        data = response;
                    }
                    observer.next(data);
                    observer.complete();
                }).catch((error) => {
                    this.commonFunction.loadingDismiss();
                    console.log(this.TAG, "file upload error", error);
                });
            });
        }
        catch (error) {
            console.log(this.TAG, error);
        }
    }
    uploadFileService(data) {
        try {
            let login = this.loginService.user;
            let password = this.loginService.pass;
            const auth = btoa(login + ":" + password);
            const httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpHeaders({
                    'Authorization': 'Basic ' + auth
                })
            };
            console.log(this.TAG, "Save Quotation Final Data", data);
            let save_quotation = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.UploadService?';
            return this.genericHttpClientService.fileDevicePost(save_quotation, data, httpOptions);
        }
        catch (error) {
            console.log(this.TAG, error);
        }
    }
    validateService(data) {
        try {
            let login = this.loginService.user;
            let password = this.loginService.pass;
            const auth = btoa(login + ":" + password);
            const httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpHeaders({
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Basic ' + auth
                })
            };
            console.log(this.TAG, "Save Quotation Final Data", data);
            let save_quotation = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.UploadService?';
            return this.genericHttpClientService.post(save_quotation, data, httpOptions);
        }
        catch (error) {
            console.log(this.TAG, error);
        }
    }
    processService(data) {
        try {
            let login = this.loginService.user;
            let password = this.loginService.pass;
            const auth = btoa(login + ":" + password);
            const httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpHeaders({
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Basic ' + auth
                })
            };
            console.log(this.TAG, "Save Quotation Final Data", data);
            let save_quotation = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.UploadService?';
            return this.genericHttpClientService.post(save_quotation, data, httpOptions);
        }
        catch (error) {
            console.log(this.TAG, error);
        }
    }
    formatService(data) {
        try {
            return this.genericHttpClientService.get(data);
        }
        catch (error) {
            console.log(this.TAG, error);
        }
    }
};
UploadService.ctorParameters = () => [
    { type: src_app_common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_2__.GenericHttpClientService },
    { type: src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__.LoginauthService },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient },
    { type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__.Commonfun },
    { type: _awesome_cordova_plugins_http_ngx__WEBPACK_IMPORTED_MODULE_0__.HTTP }
];
UploadService = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Injectable)({
        providedIn: 'root'
    })
], UploadService);



/***/ }),

/***/ 92340:
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "environment": () => (/* binding */ environment)
/* harmony export */ });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ 14431:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ 15977);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ 76057);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app/app.module */ 36747);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./environments/environment */ 92340);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_2__.environment.production) {
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.enableProdMode)();
}
(0,_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_4__.platformBrowserDynamic)().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_1__.AppModule)
    .catch(err => console.log(err));


/***/ }),

/***/ 51156:
/*!***********************************!*\
  !*** ./src/provider/commonfun.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Commonfun": () => (/* binding */ Commonfun)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @awesome-cordova-plugins/camera/ngx */ 64587);
/* harmony import */ var _app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app/login/loginauth.service */ 44010);
/* harmony import */ var _app_login_login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app/login/login.page */ 66825);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ 80190);
/* harmony import */ var _ionic_native_market_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/market/ngx */ 68538);
/* harmony import */ var _message_helper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./message-helper */ 98792);
/* harmony import */ var src_app_common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/common/generic-http-client.service */ 28475);












let Commonfun = class Commonfun {
  constructor(alertCtrl, camera, loadingController, loginpage, loginauth, genericHTTP, router, storage, market, msg) {
    this.alertCtrl = alertCtrl;
    this.camera = camera;
    this.loadingController = loadingController;
    this.loginpage = loginpage;
    this.loginauth = loginauth;
    this.genericHTTP = genericHTTP;
    this.router = router;
    this.storage = storage;
    this.market = market;
    this.msg = msg;
    this.isLoading = false;
    this.messageToShow = 'Please wait ...';
  }

  presentAlertConfirm() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this.alertCtrl.create({
        // cssClass: 'my-custom-class',
        header: 'New version available',
        message: '<h6>Please, update app to new verion to continue use.</h6>',
        buttons: [{
          text: 'NO THANKS',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {
            //console.log('Confirm Cancel: blah');
            navigator["app"].exitApp();
          }
        }, {
          text: 'UPDATE',
          handler: () => {
            //console.log('Confirm Okay');
            _this.market.open('com.multilinetech.OBPrism2');
          }
        }]
      });
      yield alert.present();
    })();
  }

  presentAlertConfirmcom(header, message, cancelText, okText) {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      return new Promise( /*#__PURE__*/function () {
        var _ref = (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (resolve) {
          const alert = yield _this2.alertCtrl.create({
            header: header,
            message: message,
            buttons: [{
              text: cancelText,
              role: 'cancel',
              cssClass: 'secondary',
              handler: cancel => {
                resolve('cancel');
              }
            }, {
              text: okText,
              handler: ok => {
                resolve('ok');
              }
            }]
          });
          alert.present();
        });

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());
    })();
  }

  loadingPresent(message, timeout) {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this3.isLoading = true;
      if (!!message) _this3.messageToShow = message;else _this3.messageToShow = 'Please wait ...';
      return yield _this3.loadingController.create({
        message: _this3.messageToShow,
        spinner: 'circles'
      }).then(a => {
        a.present().then(() => {
          //console.log('loading presented');
          //console.log("this.isLoadingP", this.isLoading);
          if (!!timeout) setTimeout(() => {
            a.dismiss();
          }, timeout);

          if (!_this3.isLoading) {
            a.dismiss().then(() => console.log('abort laoding'));
          }
        });
      });
    })();
  }

  loadingDismiss() {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this4.isLoading = false;
      return yield _this4.loadingController.dismiss().then();
    })();
  } //Alert message


  presentAlert(Header, SubHeader, Message) {
    var _this5 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this5.alertCtrl.create({
        header: Header,
        subHeader: SubHeader,
        message: Message,
        buttons: ['OK']
      });
      yield alert.present();
    })();
  }

  ImageViewr(img) {
    var _this6 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      //console.log("comimg");
      const alert = yield _this6.alertCtrl.create({
        message: '<div>' + '<img class="viewImagecss" src="data:image/jpeg;base64,' + img + '">' + '</div>',
        buttons: ['OK']
      });
      yield alert.present();
    })();
  } //Capture Image from Camera


  takePicture() {
    try {
      const options = {
        quality: 50,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType.CAMERA,
        targetWidth: 1500,
        targetHeight: 1500
      };
      this.camera.getPicture(options).then(imageData => {
        this.img64 = imageData;
        return this.img64;
      }, err => {
        this.presentAlert("Message", "Error", err);
      });
    } catch (error) {
      this.presentAlert("Message", "Error", error);
    }
  } //Select Image from library


  getimage() {
    try {
      const options = {
        quality: 50,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        targetWidth: 1500,
        targetHeight: 1500
      };
      this.camera.getPicture(options).then(imageData => {
        this.img64 = imageData;
      }, err => {
        this.presentAlert("Message", "Error", err);
      });
    } catch (error) {
      this.presentAlert("Message", "Error", error);
    }
  }

  LoginonClick(username, password, url) {
    this.loginauth.login(username, password).subscribe(resp => {
      if (resp.showMessage) {
        this.loginpage.txterror = resp.messageText;
        this.loginpage.txthint = 'Hint: ' + resp.messageTitle;
        this.loginpage.loginrequired = true;
        this.loginpage.newuser = true;
      } else {
        this.loginauth.getdefaultprofile().subscribe(data => {
          this.loginauth.defaultprofile = [data.response.data[0]];
          this.loginauth.userid = this.loginauth.defaultprofile[0].id;
          this.loginauth.isexpenseaccess = this.loginauth.selectedactivity.expenseaccess == "Y" && this.loginauth.defaultprofile[0].mmstOrderusrtype == "ST" ? true : false;
          this.loginauth.insertuserlog(this.loginauth.userid).subscribe(data1 => {
            // this.loginauth.getadminpass().subscribe(data => {
            //   var response1 = data;
            //   this.genericHTTP.ReadOnlyUsername = response1["username"];
            //   this.genericHTTP.ReadOnlypassword = response1["password"];
            //   //this.loginauth.ReadOnlyparameter = 'user=' + this.loginauth.ReadOnlyUsername + '&password=' + this.loginauth.ReadOnlypassword;
            // });
            this.loginauth.logintype = 'wms';
            this.loginauth.checkApprovalScreenAccess().subscribe(appData => {
              if (!!appData) {
                this.loginauth.approvalScreen = true; // this.events.publish('updateMenu');

                this.loginauth.publishSomeData('updateMenu');
                this.router.navigateByUrl('/' + url);
              } else {
                this.loginauth.approvalScreen = false; // this.events.publish('updateMenu');

                this.loginauth.publishSomeData('updateMenu');
                this.router.navigateByUrl('/' + url);
              }
            });
          });
        });
      }
    }); //-------------
  }

  Dateconversionddmmyyyy(date) {
    var ddmmyyyy = '';

    try {
      var dl1date = new Date(date);
      var nmonth = dl1date.getMonth() + 1;
      var dd1 = dl1date.getDate() < 10 ? "0" + dl1date.getDate() : dl1date.getDate();
      var mm1 = nmonth < 10 ? "0" + nmonth : nmonth;
      var yyyy1 = dl1date.getFullYear();
      ddmmyyyy = dd1 + "-" + mm1 + "-" + yyyy1;
    } catch (error) {
      ddmmyyyy = '';
    }

    return ddmmyyyy;
  }

};

Commonfun.ctorParameters = () => [{
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.AlertController
}, {
  type: _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_1__.Camera
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.LoadingController
}, {
  type: _app_login_login_page__WEBPACK_IMPORTED_MODULE_3__.LoginPage
}, {
  type: _app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService
}, {
  type: src_app_common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_7__.GenericHttpClientService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_9__.Router
}, {
  type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__.Storage
}, {
  type: _ionic_native_market_ngx__WEBPACK_IMPORTED_MODULE_5__.Market
}, {
  type: _message_helper__WEBPACK_IMPORTED_MODULE_6__.Message
}];

Commonfun = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.Injectable)()], Commonfun);


/***/ }),

/***/ 98792:
/*!****************************************!*\
  !*** ./src/provider/message-helper.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Message": () => (/* binding */ Message)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/common/Constants */ 68209);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ 93819);





let Message = class Message {
  constructor(alertCtrl, platform) {
    this.alertCtrl = alertCtrl;
    this.platform = platform;
    this.isplatformweb = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.isplatformweb; //set true if platform is web

    this.maxfile = 1;
    this.istakephoto = true;
  }

  presentAlert(Header, SubHeader, Message) {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this.alertCtrl.create({
        header: Header,
        subHeader: SubHeader,
        message: Message,
        buttons: ['OK']
      });
      yield alert.present();
    })();
  }

};

Message.ctorParameters = () => [{
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.AlertController
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.Platform
}];

Message = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)()], Message);


/***/ }),

/***/ 35096:
/*!******************************************!*\
  !*** ./src/provider/validator-helper.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Validator": () => (/* binding */ Validator)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);


let Validator = class Validator {
    constructor() { }
    emailValid(control) {
        return new Promise(resolve => {
            const emailPattern = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,7})$/;
            const regex = new RegExp(emailPattern);
            if (!regex.test(control.value)) {
                //console.log("control.value",control.value);
                resolve({ emailValid: true });
            }
            else {
                resolve(null);
            }
        });
    }
    digit15Valid(control) {
        return new Promise(resolve => {
            const pattern = /^(?=.{15}$)[0-9]*/;
            const regex = new RegExp(pattern);
            if (!regex.test(control.value)) {
                resolve({ digit15Valid: true });
            }
            resolve(null);
        });
    }
    digit10Valid(control) {
        return new Promise(resolve => {
            const pattern = /^(?=.{10}$)[0-9]*/;
            const regex = new RegExp(pattern);
            if (!regex.test(control.value)) {
                resolve({ digit15Valid: true });
            }
            resolve(null);
        });
    }
    nameValid(control) {
        return new Promise(resolve => {
            const pattern = /[0-9]/;
            if (pattern.test(control.value)) {
                resolve({ nameValid: true });
            }
            resolve(null);
        });
    }
    positivenumberValid(control) {
        return new Promise(resolve => {
            const pattern = /(^\d*\.?\d*[1-9]+\d*$)|(^[1-9]+\d*\.\d*$)/;
            if (!pattern.test(control.value) && !((control.value == '') || (control.value == null))) {
                resolve({ positivenumberValid: true });
            }
            else {
                resolve(null);
            }
        });
    }
    integernumberValid(control) {
        return new Promise(resolve => {
            //  const pattern = /\b(?<!\.)\d+(?!\.)\b/
            const pattern = /^[0-9]*$/;
            if (!pattern.test(control.value)) {
                resolve({ integernumberValid: true });
            }
            resolve(null);
        });
    }
    vehiclenumberValid(control) {
        return new Promise(resolve => {
            //  const pattern = /\b(?<!\.)\d+(?!\.)\b/
            const pattern = /^[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}$/;
            if (control.value == null || control.value == undefined || control.value == "") {
                resolve(null);
            }
            else {
                if (!pattern.test(control.value.toUpperCase())) {
                    resolve({ vehiclenumberValid: true });
                }
            }
            resolve(null);
        });
    }
    //Phone No. Validation
    numberValid(control) {
        return new Promise(resolve => {
            const pattern = /^[1-9]{1}[0-9]{9}$/;
            if (!pattern.test(control.value) && !((control.value == '') || (control.value == null))) {
                resolve({ numberValid: true });
            }
            else
                resolve(null);
        });
    }
    pincodeValid(control) {
        return new Promise(resolve => {
            const pattern = /^\d{6}$/;
            if (!pattern.test(control.value) && !(control.value == '')) {
                resolve({ pincodeValid: true });
            }
            resolve(null);
        });
    }
    gstnumberValid(control) {
        return new Promise(resolve => {
            const pattern = /\d{2}[A-Za-z]{5}\d{4}[A-Za-z\d]{4}$/;
            if (!pattern.test(control.value) && !((control.value == '') || (control.value == null))) {
                resolve({ gstnumberValid: true });
            }
            resolve(null);
        });
    }
    pannoValid(control) {
        return new Promise(resolve => {
            const pattern = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
            const regex = new RegExp(pattern);
            if (!regex.test(control.value)) {
                resolve({ pannoValid: true });
            }
            else {
                resolve(null);
            }
        });
    }
};
Validator.ctorParameters = () => [];
Validator = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Injectable)()
], Validator);



/***/ }),

/***/ 50863:
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/ lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \******************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./ion-accordion_2.entry.js": [
		70079,
		"common",
		"node_modules_ionic_core_dist_esm_ion-accordion_2_entry_js"
	],
	"./ion-action-sheet.entry.js": [
		25593,
		"common",
		"node_modules_ionic_core_dist_esm_ion-action-sheet_entry_js"
	],
	"./ion-alert.entry.js": [
		13225,
		"common",
		"node_modules_ionic_core_dist_esm_ion-alert_entry_js"
	],
	"./ion-app_8.entry.js": [
		4812,
		"common",
		"node_modules_ionic_core_dist_esm_ion-app_8_entry_js"
	],
	"./ion-avatar_3.entry.js": [
		86655,
		"common",
		"node_modules_ionic_core_dist_esm_ion-avatar_3_entry_js"
	],
	"./ion-back-button.entry.js": [
		44856,
		"common",
		"node_modules_ionic_core_dist_esm_ion-back-button_entry_js"
	],
	"./ion-backdrop.entry.js": [
		13059,
		"node_modules_ionic_core_dist_esm_ion-backdrop_entry_js"
	],
	"./ion-breadcrumb_2.entry.js": [
		58648,
		"common",
		"node_modules_ionic_core_dist_esm_ion-breadcrumb_2_entry_js"
	],
	"./ion-button_2.entry.js": [
		98308,
		"common",
		"node_modules_ionic_core_dist_esm_ion-button_2_entry_js"
	],
	"./ion-card_5.entry.js": [
		44690,
		"common",
		"node_modules_ionic_core_dist_esm_ion-card_5_entry_js"
	],
	"./ion-checkbox.entry.js": [
		64090,
		"common",
		"node_modules_ionic_core_dist_esm_ion-checkbox_entry_js"
	],
	"./ion-chip.entry.js": [
		36214,
		"common",
		"node_modules_ionic_core_dist_esm_ion-chip_entry_js"
	],
	"./ion-col_3.entry.js": [
		69447,
		"node_modules_ionic_core_dist_esm_ion-col_3_entry_js"
	],
	"./ion-datetime_3.entry.js": [
		79689,
		"common",
		"node_modules_ionic_core_dist_esm_ion-datetime_3_entry_js"
	],
	"./ion-fab_3.entry.js": [
		18840,
		"common",
		"node_modules_ionic_core_dist_esm_ion-fab_3_entry_js"
	],
	"./ion-img.entry.js": [
		40749,
		"node_modules_ionic_core_dist_esm_ion-img_entry_js"
	],
	"./ion-infinite-scroll_2.entry.js": [
		69667,
		"common",
		"node_modules_ionic_core_dist_esm_ion-infinite-scroll_2_entry_js"
	],
	"./ion-input.entry.js": [
		83288,
		"common",
		"node_modules_ionic_core_dist_esm_ion-input_entry_js"
	],
	"./ion-item-option_3.entry.js": [
		35473,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item-option_3_entry_js"
	],
	"./ion-item_8.entry.js": [
		53634,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item_8_entry_js"
	],
	"./ion-loading.entry.js": [
		22855,
		"common",
		"node_modules_ionic_core_dist_esm_ion-loading_entry_js"
	],
	"./ion-menu_3.entry.js": [
		495,
		"common",
		"node_modules_ionic_core_dist_esm_ion-menu_3_entry_js"
	],
	"./ion-modal.entry.js": [
		58737,
		"common",
		"node_modules_ionic_core_dist_esm_ion-modal_entry_js"
	],
	"./ion-nav_2.entry.js": [
		99632,
		"common",
		"node_modules_ionic_core_dist_esm_ion-nav_2_entry_js"
	],
	"./ion-picker-column-internal.entry.js": [
		54446,
		"common",
		"node_modules_ionic_core_dist_esm_ion-picker-column-internal_entry_js"
	],
	"./ion-picker-internal.entry.js": [
		32275,
		"node_modules_ionic_core_dist_esm_ion-picker-internal_entry_js"
	],
	"./ion-popover.entry.js": [
		48050,
		"common",
		"node_modules_ionic_core_dist_esm_ion-popover_entry_js"
	],
	"./ion-progress-bar.entry.js": [
		18994,
		"common",
		"node_modules_ionic_core_dist_esm_ion-progress-bar_entry_js"
	],
	"./ion-radio_2.entry.js": [
		23592,
		"common",
		"node_modules_ionic_core_dist_esm_ion-radio_2_entry_js"
	],
	"./ion-range.entry.js": [
		35454,
		"common",
		"node_modules_ionic_core_dist_esm_ion-range_entry_js"
	],
	"./ion-refresher_2.entry.js": [
		290,
		"common",
		"node_modules_ionic_core_dist_esm_ion-refresher_2_entry_js"
	],
	"./ion-reorder_2.entry.js": [
		92666,
		"common",
		"node_modules_ionic_core_dist_esm_ion-reorder_2_entry_js"
	],
	"./ion-ripple-effect.entry.js": [
		64816,
		"node_modules_ionic_core_dist_esm_ion-ripple-effect_entry_js"
	],
	"./ion-route_4.entry.js": [
		45534,
		"common",
		"node_modules_ionic_core_dist_esm_ion-route_4_entry_js"
	],
	"./ion-searchbar.entry.js": [
		94902,
		"common",
		"node_modules_ionic_core_dist_esm_ion-searchbar_entry_js"
	],
	"./ion-segment_2.entry.js": [
		91938,
		"common",
		"node_modules_ionic_core_dist_esm_ion-segment_2_entry_js"
	],
	"./ion-select_3.entry.js": [
		14832,
		"common",
		"node_modules_ionic_core_dist_esm_ion-select_3_entry_js"
	],
	"./ion-slide_2.entry.js": [
		90668,
		"node_modules_ionic_core_dist_esm_ion-slide_2_entry_js"
	],
	"./ion-spinner.entry.js": [
		61624,
		"common",
		"node_modules_ionic_core_dist_esm_ion-spinner_entry_js"
	],
	"./ion-split-pane.entry.js": [
		19989,
		"node_modules_ionic_core_dist_esm_ion-split-pane_entry_js"
	],
	"./ion-tab-bar_2.entry.js": [
		28902,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab-bar_2_entry_js"
	],
	"./ion-tab_2.entry.js": [
		70199,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab_2_entry_js"
	],
	"./ion-text.entry.js": [
		48395,
		"common",
		"node_modules_ionic_core_dist_esm_ion-text_entry_js"
	],
	"./ion-textarea.entry.js": [
		96357,
		"common",
		"node_modules_ionic_core_dist_esm_ion-textarea_entry_js"
	],
	"./ion-toast.entry.js": [
		38268,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toast_entry_js"
	],
	"./ion-toggle.entry.js": [
		15269,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toggle_entry_js"
	],
	"./ion-virtual-scroll.entry.js": [
		32875,
		"node_modules_ionic_core_dist_esm_ion-virtual-scroll_entry_js"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(() => {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(() => {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = () => (Object.keys(map));
webpackAsyncContext.id = 50863;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 79259:
/*!***********************************************!*\
  !*** ./src/app/app.component.scss?ngResource ***!
  \***********************************************/
/***/ ((module) => {

"use strict";
module.exports = "ion-menu ion-content {\n  --background: var(--ion-item-background, var(--ion-background-color,#fff));\n}\n\nion-item {\n  max-width: 100% !important;\n}\n\n.custom-avatar {\n  height: 100%;\n  width: 100%;\n  border: 1px solid #fff;\n  border-radius: 50%;\n  display: inline-block;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.boder-footer {\n  border-right-width: initial;\n  border-right-style: inset;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDBFQUFBO0FBQ0o7O0FBQ0U7RUFDRSwwQkFBQTtBQUVKOztBQUFFO0VBR0UsWUFBQTtFQUNELFdBQUE7RUFDQyxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBQ0o7O0FBUUU7RUFDRSwyQkFBQTtFQUNBLHlCQUFBO0FBTEoiLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLW1lbnUgaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWl0ZW0tYmFja2dyb3VuZCwgdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IsI2ZmZikpO1xuICB9XG4gIGlvbi1pdGVte1xuICAgIG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICAgICAgfVxuICAuY3VzdG9tLWF2YXRhciB7XG4gICAgLy8gaGVpZ2h0OiAyMHZ3O1xuICAgIC8vIHdpZHRoOjIwdnc7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgd2lkdGg6IDEwMCU7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2ZmZjtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICBcbiAgICAvL2JhY2tncm91bmQtaW1hZ2U6IHVybCgnLi4vYXNzZXRzL2ljb24vaWNfbGF1bmNoZXIucG5nJyk7XG4gIH1cblxuICAvLyBpb24tY29sIHtcbiAgLy8gICBib3JkZXI6IDJweCBzb2xpZCAjYWViN2NhO1xuICAvLyB9XG5cbiAgLmJvZGVyLWZvb3RlcntcbiAgICBib3JkZXItcmlnaHQtd2lkdGg6IGluaXRpYWw7XG4gICAgYm9yZGVyLXJpZ2h0LXN0eWxlOiBpbnNldDtcbiAgfSJdfQ== */";

/***/ }),

/***/ 40906:
/*!****************************************************************!*\
  !*** ./src/app/custom-alert/custom-alert.page.scss?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

"use strict";
module.exports = ".footer-back-color {\n  background-color: white;\n}\n\n.footer-btn-color {\n  background-color: #F39E20 !important;\n  height: 35px !important;\n}\n\n.login-item {\n  --border-width: 1px !important;\n  margin-top: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImN1c3RvbS1hbGVydC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx1QkFBQTtBQUNKOztBQUNBO0VBQ0ksb0NBQUE7RUFDQSx1QkFBQTtBQUVKOztBQUFBO0VBQ0ksOEJBQUE7RUFDQSxnQkFBQTtBQUdKIiwiZmlsZSI6ImN1c3RvbS1hbGVydC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9vdGVyLWJhY2stY29sb3J7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG4uZm9vdGVyLWJ0bi1jb2xvcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjM5RTIwICFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OjM1cHggIWltcG9ydGFudDtcbn1cbi5sb2dpbi1pdGVte1xuICAgIC0tYm9yZGVyLXdpZHRoOiAxcHggIWltcG9ydGFudDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xufSJdfQ== */";

/***/ }),

/***/ 87047:
/*!**************************************************!*\
  !*** ./src/app/login/login.page.scss?ngResource ***!
  \**************************************************/
/***/ ((module) => {

"use strict";
module.exports = ".img-contaimer {\n  width: 100%;\n  height: auto;\n  padding: 0;\n  text-align: center;\n}\n\n.height-img {\n  height: auto;\n}\n\n.ion-color-vibrant {\n  --ion-color-base: #263247;\n  --ion-color-base-rgb: 255,255,0;\n  --ion-color-contrast-rgb: 0,0,0;\n  --ion-color-shade: #263247;\n  --ion-color-tint:#263247;\n}\n\n.login-item {\n  padding-left: 15px;\n  --border-width: 1px !important;\n  margin-bottom: 25px;\n}\n\n.no-item {\n  --border-width: 0px !important;\n  margin-bottom: 0px;\n}\n\n.submit-btn {\n  padding-top: 10px;\n  padding-left: 15px;\n  padding-right: 15px;\n}\n\n.re-db {\n  color: deepskyblue;\n}\n\n.build {\n  background-color: white !important;\n}\n\n.custom-loader-class {\n  --background:#F4B004;\n  --spinner-color:#F94701;\n}\n\n.scroll-content {\n  overflow-y: auto;\n}\n\n.ion-scroll.scroll-y .scroll-content::-webkit-scrollbar {\n  display: none;\n}\n\n.number-dial-card {\n  position: absolute;\n  width: -webkit-fill-available;\n  bottom: 15px;\n  font-size: 30px !important;\n  font-weight: bold;\n  --background:white;\n}\n\n.sec-password-template {\n  background-color: #f5f5f2;\n}\n\n.digit {\n  padding: 10%;\n}\n\n.row-digit {\n  border-bottom-style: solid !important;\n  border-color: #f5f5f2 !important;\n}\n\n.col-right {\n  border-right-style: inset !important;\n}\n\n.col-all {\n  text-align: center;\n}\n\nion-content {\n  background: rgba(0, 151, 19, 0.1);\n}\n\n.login-form-section {\n  position: fixed;\n  top: 41%;\n  left: 50%;\n  width: 30em;\n  margin-top: -9em;\n  margin-left: -15em;\n}\n\n.logingrid {\n  background-repeat: no-repeat;\n  background-size: cover;\n  background-position: top;\n  height: 100%;\n}\n\n.rounded-div {\n  text-align: center;\n  padding: 14px;\n  border-style: solid;\n  border-radius: 90px;\n  border-color: #10DC60;\n  background: white;\n}\n\n.rounded-div-selected {\n  text-align: center;\n  padding: 28px;\n  color: #cfd8dc;\n  background-color: #cfd8dc;\n  border-radius: 90px;\n}\n\n.rounded-div-profile {\n  text-align: center;\n  padding: 21px;\n  border-radius: 99px;\n  max-width: max-content;\n  background: #f06292;\n  font-size: xx-large;\n  color: white;\n}\n\n.profile-name {\n  font-weight: bolder;\n  font-size: larger;\n  text-align: center;\n}\n\n.new-error-message {\n  margin-top: 5%;\n  margin-left: 25%;\n}\n\n.reset-pin {\n  margin-top: 17%;\n}\n\n.auth-btn {\n  margin-bottom: 8px !important;\n}\n\n.auth-div {\n  text-align: -webkit-right !important;\n}\n\n.reset-pin-css {\n  text-align: end !important;\n  margin-right: 5% !important;\n}\n\n.logincard {\n  width: 40%;\n}\n\n@media only screen and (max-width: 600px) {\n  .logincard {\n    width: 80%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0FBQVI7O0FBRUk7RUFDTSxZQUFBO0FBQ1Y7O0FBQ0k7RUFDSSx5QkFBQTtFQUNBLCtCQUFBO0VBQ0EsK0JBQUE7RUFDQSwwQkFBQTtFQUNBLHdCQUFBO0FBRVI7O0FBS0k7RUFDSSxrQkFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7QUFGUjs7QUFJSTtFQUNJLDhCQUFBO0VBQ0Usa0JBQUE7QUFEVjs7QUFHSTtFQUNBLGlCQUFBO0VBQ0Usa0JBQUE7RUFDQSxtQkFBQTtBQUFOOztBQUVJO0VBQ0ksa0JBQUE7QUFDUjs7QUFDSTtFQUNJLGtDQUFBO0FBRVI7O0FBQUk7RUFDSSxvQkFBQTtFQUNBLHVCQUFBO0FBR1I7O0FBREk7RUFDSSxnQkFBQTtBQUlSOztBQUZJO0VBQ0ksYUFBQTtBQUtSOztBQUhFO0VBQ0Usa0JBQUE7RUFDQSw2QkFBQTtFQUNBLFlBQUE7RUFDQSwwQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFNSjs7QUFKSTtFQUNJLHlCQUFBO0FBT1I7O0FBTEk7RUFFRyxZQUFBO0FBT1A7O0FBTEk7RUFDSSxxQ0FBQTtFQUNBLGdDQUFBO0FBUVI7O0FBTEk7RUFDSSxvQ0FBQTtBQVFSOztBQU5JO0VBRUcsa0JBQUE7QUFRUDs7QUFOSTtFQUVELGlDQUFBO0FBUUg7O0FBTEk7RUFPQyxlQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBRUEsZ0JBQUE7RUFDQSxrQkFBQTtBQUNMOztBQUdJO0VBRUksNEJBQUE7RUFDQSxzQkFBQTtFQUNBLHdCQUFBO0VBQ0EsWUFBQTtBQURSOztBQUdJO0VBQ0ksa0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7QUFBUjs7QUFFSTtFQUNJLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUVBLG1CQUFBO0FBQVI7O0FBR0k7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUdBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUFBUjs7QUFFSTtFQUNJLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQUNSOztBQUNJO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0FBRVI7O0FBQUk7RUFDSSxlQUFBO0FBR1I7O0FBQUk7RUFDSSw2QkFBQTtBQUdSOztBQUFJO0VBQ0ksb0NBQUE7QUFHUjs7QUFESTtFQUNJLDBCQUFBO0VBQ0EsMkJBQUE7QUFJUjs7QUFGSTtFQUNJLFVBQUE7QUFLUjs7QUFKUTtFQUZKO0lBR1EsVUFBQTtFQU9WO0FBQ0YiLCJmaWxlIjoibG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4gICAgLmltZy1jb250YWltZXJ7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IGF1dG87XG4gICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gICAgLmhlaWdodC1pbWcge1xuICAgICAgICAgIGhlaWdodDogYXV0bztcbiAgICB9XG4gICAgLmlvbi1jb2xvci12aWJyYW50IHtcbiAgICAgICAgLS1pb24tY29sb3ItYmFzZTogIzI2MzI0NztcbiAgICAgICAgLS1pb24tY29sb3ItYmFzZS1yZ2I6IDI1NSwyNTUsMDtcbiAgICAgICAgLS1pb24tY29sb3ItY29udHJhc3QtcmdiOiAwLDAsMDtcbiAgICAgICAgLS1pb24tY29sb3Itc2hhZGU6ICMyNjMyNDc7XG4gICAgICAgIC0taW9uLWNvbG9yLXRpbnQ6IzI2MzI0NztcbiAgICAgICBcbiAgICB9XG4gICAgLy8gLml0ZW0ge1xuICAgIC8vICAgICAtLWJvcmRlci13aWR0aDogMXB4ICFpbXBvcnRhbnQ7XG4gICAgLy8gICAgIG1hcmdpbi1ib3R0b206IDI1cHg7XG4gICAgLy8gfVxuICAgIC5sb2dpbi1pdGVte1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gICAgICAgIC0tYm9yZGVyLXdpZHRoOiAxcHggIWltcG9ydGFudDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMjVweDtcbiAgICB9XG4gICAgLm5vLWl0ZW17XG4gICAgICAgIC0tYm9yZGVyLXdpZHRoOiAwcHggIWltcG9ydGFudDtcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gICAgfVxuICAgIC5zdWJtaXQtYnRue1xuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICAgICAgcGFkZGluZy1yaWdodDogMTVweDtcbiAgICB9XG4gICAgLnJlLWRie1xuICAgICAgICBjb2xvcjpkZWVwc2t5Ymx1ZVxuICAgIH1cbiAgICAuYnVpbGR7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlICFpbXBvcnRhbnRcbiAgICB9XG4gICAgLmN1c3RvbS1sb2FkZXItY2xhc3N7XG4gICAgICAgIC0tYmFja2dyb3VuZDojRjRCMDA0O1xuICAgICAgICAtLXNwaW5uZXItY29sb3I6I0Y5NDcwMTtcbiAgICB9XG4gICAgLnNjcm9sbC1jb250ZW50IHtcbiAgICAgICAgb3ZlcmZsb3cteTogYXV0bztcbiAgICB9XG4gICAgLmlvbi1zY3JvbGwuc2Nyb2xsLXkgLnNjcm9sbC1jb250ZW50Ojotd2Via2l0LXNjcm9sbGJhcntcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgIH1cbiAgLm51bWJlci1kaWFsLWNhcmR7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHdpZHRoOiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlO1xuICAgIGJvdHRvbTogMTVweDtcbiAgICBmb250LXNpemU6IDMwcHggIWltcG9ydGFudDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAtLWJhY2tncm91bmQgOndoaXRlO1xufVxuICAgIC5zZWMtcGFzc3dvcmQtdGVtcGxhdGV7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmNWY1ZjI7XG4gICAgfVxuICAgIC5kaWdpdHtcbiAgICAgICAvLyB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudCA7XG4gICAgICAgcGFkZGluZzogMTAlO1xuICAgIH1cbiAgICAucm93LWRpZ2l0IHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbS1zdHlsZTogc29saWQgIWltcG9ydGFudDtcbiAgICAgICAgYm9yZGVyLWNvbG9yOiAjZjVmNWYyICFpbXBvcnRhbnQ7XG4gICAgICAvLyAgbWluLWhlaWdodDogNzBweCAhaW1wb3J0YW50O1xuICAgIH1cbiAgICAuY29sLXJpZ2h0IHtcbiAgICAgICAgYm9yZGVyLXJpZ2h0LXN0eWxlOiBpbnNldCAhaW1wb3J0YW50O1xuICAgIH1cbiAgICAuY29sLWFsbHtcbiAgICAgICAvLyBtYXJnaW4tdG9wOiAzJTtcbiAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICAgIGlvbi1jb250ZW50e1xuICAgLy8gLS1iYWNrZ3JvdW5kOiAjZjVmNWYyO1xuICAgYmFja2dyb3VuZDogcmdiYSgwLCAxNTEsIDE5LCAwLjEpOyBcbiAgICAgICAgfVxuXG4gICAgLmxvZ2luLWZvcm0tc2VjdGlvbntcbiAgICAgICBcbiAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsMjU1LDI1NSwwLjQwKTtcbiAgICAgLy8gbWF4LXdpZHRoOiA0MDBweDtcbiAgICAgLy8gbWFyZ2luLXRvcDogMTQwcHg7XG4gICAgIC8vIG1hcmdpbi1sZWZ0OiA4MHB4O1xuICAgICAvLyBtYXJnaW4tYm90dG9tOiAxMDBweDtcbiAgICAgcG9zaXRpb246IGZpeGVkO1xuICAgICB0b3A6IDQxJTtcbiAgICAgbGVmdDogNTAlO1xuICAgICB3aWR0aDogMzBlbTtcbiAgICAvLyAgIGhlaWdodDogMjZlbTsgXG4gICAgIG1hcmdpbi10b3A6IC05ZW07XG4gICAgIG1hcmdpbi1sZWZ0OiAtMTVlbTtcblxuICAgIH0gICBcbiAgXG4gICAgLmxvZ2luZ3JpZHtcbiAgICAgIC8vICBiYWNrZ3JvdW5kLWltYWdlOnVybChcIi4uLy4uL2Fzc2V0cy9QYXJla2hJbWFnZS5wbmdcIik7XG4gICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IHRvcDtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgIH0gXG4gICAgLnJvdW5kZWQtZGl2e1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIHBhZGRpbmc6IDE0cHg7XG4gICAgICAgIGJvcmRlci1zdHlsZTogc29saWQ7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDkwcHg7XG4gICAgICAgIGJvcmRlci1jb2xvcjogICMxMERDNjA7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIH1cbiAgICAucm91bmRlZC1kaXYtc2VsZWN0ZWR7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgcGFkZGluZzogMjhweDtcbiAgICAgICAgY29sb3I6ICNjZmQ4ZGM7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6I2NmZDhkYztcbiAgICAgICAgXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDkwcHg7XG4gICAgfVxuICBcbiAgICAucm91bmRlZC1kaXYtcHJvZmlsZXtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBwYWRkaW5nOiAyMXB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiA5OXB4O1xuICAgICAgICBtYXgtd2lkdGg6IC13ZWJraXQtbWF4LWNvbnRlbnQ7XG4gICAgICAgIG1heC13aWR0aDogLW1vei1tYXgtY29udGVudDtcbiAgICAgICAgbWF4LXdpZHRoOiBtYXgtY29udGVudDtcbiAgICAgICAgYmFja2dyb3VuZDogI2YwNjI5MjtcbiAgICAgICAgZm9udC1zaXplOiB4eC1sYXJnZTtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgIH1cbiAgICAucHJvZmlsZS1uYW1le1xuICAgICAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgICAgICBmb250LXNpemU6IGxhcmdlcjtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbiAgICAubmV3LWVycm9yLW1lc3NhZ2V7XG4gICAgICAgIG1hcmdpbi10b3A6IDUlO1xuICAgICAgICBtYXJnaW4tbGVmdDogMjUlXG4gICAgfVxuICAgIC5yZXNldC1waW57XG4gICAgICAgIG1hcmdpbi10b3A6IDE3JTtcbiAgICB9XG5cbiAgICAuYXV0aC1idG57XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDhweCAhaW1wb3J0YW50O1xuICAgIH1cblxuICAgIC5hdXRoLWRpdntcbiAgICAgICAgdGV4dC1hbGlnbjogLXdlYmtpdC1yaWdodCAhaW1wb3J0YW50XG4gICAgfVxuICAgIC5yZXNldC1waW4tY3Nze1xuICAgICAgICB0ZXh0LWFsaWduOiBlbmQgIWltcG9ydGFudDtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA1JSAhaW1wb3J0YW50O1xuICAgIH1cbiAgICAubG9naW5jYXJke1xuICAgICAgICB3aWR0aDogNDAlO1xuICAgICAgICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gICAgICAgICAgICB3aWR0aDogODAlO1xuICAgICAgICAgIH1cbiAgICB9Il19 */";

/***/ }),

/***/ 7392:
/*!********************************************************!*\
  !*** ./src/app/neworder/neworder.page.scss?ngResource ***!
  \********************************************************/
/***/ ((module) => {

"use strict";
module.exports = "ion-scroll[scrollX] {\n  white-space: nowrap;\n  overflow: visible;\n  overflow-y: auto;\n}\nion-scroll[scrollX] .scroll-item {\n  display: inline-block;\n}\nion-scroll[scrollX] .selectable-icon {\n  margin: 10px 20px 10px 20px;\n  color: red;\n  font-size: 100px;\n}\nion-scroll[scrollX] ion-avatar img {\n  margin: 10px;\n}\nion-scroll[scroll-avatar] {\n  height: 60px;\n}\n/* Hide ion-content scrollbar */\n::-webkit-scrollbar {\n  display: none;\n}\nh5 {\n  font-style: oblique;\n  color: darkcyan;\n  font-size: large;\n}\nh5 ion-icon {\n  color: lightcoral;\n}\n.inputfile {\n  color: transparent;\n}\n#selectedFile {\n  position: absolute;\n  opacity: 0;\n}\n.myFakeUploadButton {\n  background-color: #f39e20;\n  padding: 30px;\n}\n::ng-deep .custom .alert-message.sc-ion-alert-ios {\n  color: red !important;\n}\n.pdf-help-text {\n  padding: 10px !important;\n}\nion-popover {\n  --width: 320px;\n}\nion-popover.dateTimePopover {\n  --offset-y: -350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5ld29yZGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFBO0VBRUEsaUJBQUE7RUFDQSxnQkFBQTtBQUFKO0FBRUk7RUFDRSxxQkFBQTtBQUFOO0FBR0k7RUFDRSwyQkFBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtBQUROO0FBSUk7RUFDRSxZQUFBO0FBRk47QUFNRTtFQUNFLFlBQUE7QUFISjtBQU1FLCtCQUFBO0FBQ0E7RUFDRSxhQUFBO0FBSEo7QUFNRTtFQUNFLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBSEo7QUFJSTtFQUNFLGlCQUFBO0FBRk47QUFPRTtFQUNFLGtCQUFBO0FBSko7QUFNRTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtBQUhKO0FBTUU7RUFDQyx5QkFBQTtFQUNBLGFBQUE7QUFISDtBQU1FO0VBQ0sscUJBQUE7QUFIUDtBQVlJO0VBQ0Usd0JBQUE7QUFUTjtBQWdCSTtFQUNFLGNBQUE7QUFiTjtBQWVJO0VBQ0UsNkJBQUE7QUFaTiIsImZpbGUiOiJuZXdvcmRlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tc2Nyb2xsW3Njcm9sbFhdIHtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgLy8gaGVpZ2h0OiAxMjBweDtcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xuLy8gd2lkdGg6MTAwJTtcbiAgICAuc2Nyb2xsLWl0ZW0ge1xuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIH1cblxuICAgIC5zZWxlY3RhYmxlLWljb257XG4gICAgICBtYXJnaW46IDEwcHggMjBweCAxMHB4IDIwcHg7XG4gICAgICBjb2xvcjogcmVkO1xuICAgICAgZm9udC1zaXplOiAxMDBweDtcbiAgICB9XG5cbiAgICBpb24tYXZhdGFyIGltZ3tcbiAgICAgIG1hcmdpbjogMTBweDtcbiAgICB9XG4gIH1cblxuICBpb24tc2Nyb2xsW3Njcm9sbC1hdmF0YXJde1xuICAgIGhlaWdodDogNjBweDtcbiAgfVxuXG4gIC8qIEhpZGUgaW9uLWNvbnRlbnQgc2Nyb2xsYmFyICovXG4gIDo6LXdlYmtpdC1zY3JvbGxiYXJ7XG4gICAgZGlzcGxheTpub25lO1xuICB9XG5cbiAgaDV7XG4gICAgZm9udC1zdHlsZTogb2JsaXF1ZTtcbiAgICBjb2xvcjogZGFya2N5YW47XG4gICAgZm9udC1zaXplOiBsYXJnZTtcbiAgICBpb24taWNvbntcbiAgICAgIGNvbG9yOiBsaWdodGNvcmFsO1xuICAgIH1cblxuICB9XG4gIFxuICAuaW5wdXRmaWxlIHtcbiAgICBjb2xvcjogdHJhbnNwYXJlbnQ7XG4gIH1cbiAgI3NlbGVjdGVkRmlsZXtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxuICBcbiAgLm15RmFrZVVwbG9hZEJ1dHRvbntcbiAgIGJhY2tncm91bmQtY29sb3I6ICNmMzllMjA7XG4gICBwYWRkaW5nOiAzMHB4O1xuICAgXG4gIH1cbiAgOjpuZy1kZWVwIC5jdXN0b20gLmFsZXJ0LW1lc3NhZ2Uuc2MtaW9uLWFsZXJ0LWlvc3tcbiAgICAgICBjb2xvcjogcmVkICFpbXBvcnRhbnQ7XG4gICB9XG4gXG4gICAgLy8gOjpuZy1kZWVwIC5jdXN0b20tYWxlcnQgLnNjLWlvbi1tb2RhbC1pb3Mge1xuICAgIC8vICAgIGhlaWdodDogMjUwcHggIWltcG9ydGFudDtcbiAgICAvLyAgICB3aWR0aDogMzUwcHggIWltcG9ydGFudDtcbiAgICAvLyAgICBwb3NpdGlvbjogYWJzb2x1dGU7IFxuICAgIC8vICAgIGRpc3BsYXk6IGJsb2NrOyAgXG4gICAgLy8gfVxuICAgIC5wZGYtaGVscC10ZXh0e1xuICAgICAgcGFkZGluZzoxMHB4ICFpbXBvcnRhbnQ7XG4gICAgICAvL2JhY2tncm91bmQ6YW50aXF1ZXdoaXRlO1xuICAgIH1cbiAgICAvLyAucGRmLWhlbHAtdGV4dC1kZXZpY2Uge1xuICAgIC8vICAvLyBwYWRkaW5nOjEwcHggIWltcG9ydGFudDtcbiAgICAvLyAgLy8gYmFja2dyb3VuZDphbnRpcXVld2hpdGU7XG4gICAgLy8gfVxuICAgIGlvbi1wb3BvdmVyIHtcbiAgICAgIC0td2lkdGg6IDMyMHB4O1xuICAgIH1cbiAgICBpb24tcG9wb3Zlci5kYXRlVGltZVBvcG92ZXIge1xuICAgICAgLS1vZmZzZXQteTogLTM1MHB4ICFpbXBvcnRhbnQ7XG4gICAgICB9Il19 */";

/***/ }),

/***/ 47068:
/*!****************************************************************!*\
  !*** ./src/app/product-list/product-list.page.scss?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "ion-input.custom-class {\n  font-size: smaller;\n}\n\nion-col ion-input {\n  border: solid 1px #757575;\n}\n\n.footer-btn-color {\n  background-color: #F39E20 !important;\n}\n\n.ion-color-vibrant {\n  --ion-color-base: #78909C;\n  --ion-color-base-rgb: 255,255,0;\n  --ion-color-contrast-rgb: 0,0,0;\n  --ion-color-shade: #78909C;\n  --ion-color-tint:#78909C;\n}\n\n.custom-add-btn {\n  height: 30px;\n}\n\n.amc-for-two-Year {\n  background-color: #FF5252;\n  padding: 5px;\n  border-radius: 5px;\n  color: white;\n  font-weight: bold;\n}\n\nion-card {\n  margin-top: 5px !important;\n  margin-bottom: 10px !important;\n}\n\n.added {\n  background-color: #4CAF50;\n  padding: 5px;\n  border-radius: 5px;\n  color: white;\n  font-weight: bold;\n}\n\n.add-to-cart {\n  background-color: #FF5252;\n  padding: 2px;\n  border-radius: 5px;\n  color: white;\n  font-weight: bold;\n}\n\n::ng-deep input[type=number]::-webkit-inner-spin-button,\n::ng-deep input[type=number]::-webkit-outer-spin-button {\n  -webkit-appearance: none;\n  appearance: none;\n  margin: 0;\n}\n\n/* Chrome, Safari, Edge, Opera */\n\ninput::-webkit-outer-spin-button,\ninput::-webkit-inner-spin-button {\n  -webkit-appearance: none;\n  margin: 0;\n}\n\n/* Firefox */\n\ninput[type=number] {\n  -moz-appearance: textfield;\n}\n\n.custom-serarch {\n  height: 35px !important;\n  margin-top: 12px !important;\n}\n\n.sc-ion-searchbar-ios-h {\n  padding-inline-end: 0px !important;\n}\n\n.custom-serarch-device {\n  height: 35px !important;\n  margin-top: 12px !important;\n  --padding-start: 0.3em !important;\n  --padding-end: 0.3em !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2R1Y3QtbGlzdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDRSxrQkFBQTtBQUFOOztBQUtFO0VBQ0cseUJBQUE7QUFGTDs7QUFLQTtFQUNFLG9DQUFBO0FBRkY7O0FBSUE7RUFDRSx5QkFBQTtFQUNBLCtCQUFBO0VBQ0EsK0JBQUE7RUFDQSwwQkFBQTtFQUNBLHdCQUFBO0FBREY7O0FBSUE7RUFDRSxZQUFBO0FBREY7O0FBSUE7RUFDRSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtBQURGOztBQUdBO0VBQ0UsMEJBQUE7RUFDQSw4QkFBQTtBQUFGOztBQUVBO0VBQ0UseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUFDRjs7QUFDQTtFQUNFLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBRUY7O0FBQ0E7O0VBRUEsd0JBQUE7RUFFQSxnQkFBQTtFQUNBLFNBQUE7QUFFQTs7QUFDQSxnQ0FBQTs7QUFDQTs7RUFFRSx3QkFBQTtFQUNBLFNBQUE7QUFFRjs7QUFDQSxZQUFBOztBQUNBO0VBQ0UsMEJBQUE7QUFFRjs7QUFDQTtFQUNFLHVCQUFBO0VBQ0EsMkJBQUE7QUFFRjs7QUFBQTtFQUVHLGtDQUFBO0FBR0g7O0FBREE7RUFDRSx1QkFBQTtFQUNBLDJCQUFBO0VBQ0EsaUNBQUE7RUFDQSwrQkFBQTtBQUlGIiwiZmlsZSI6InByb2R1Y3QtbGlzdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taW5wdXQge1xuICAgICYuY3VzdG9tLWNsYXNzIHtcbiAgICAgIGZvbnQtc2l6ZTogc21hbGxlcjtcbiAgICB9XG59XG5cbmlvbi1jb2x7XG4gIGlvbi1pbnB1dHtcbiAgICAgYm9yZGVyOiBzb2xpZCAxcHggIzc1NzU3NTtcbiAgfVxufVxuLmZvb3Rlci1idG4tY29sb3J7XG4gIGJhY2tncm91bmQtY29sb3I6ICNGMzlFMjAgIWltcG9ydGFudDtcbn1cbi5pb24tY29sb3ItdmlicmFudCB7XG4gIC0taW9uLWNvbG9yLWJhc2U6ICM3ODkwOUM7XG4gIC0taW9uLWNvbG9yLWJhc2UtcmdiOiAyNTUsMjU1LDA7XG4gIC0taW9uLWNvbG9yLWNvbnRyYXN0LXJnYjogMCwwLDA7XG4gIC0taW9uLWNvbG9yLXNoYWRlOiAjNzg5MDlDO1xuICAtLWlvbi1jb2xvci10aW50OiM3ODkwOUM7XG4gXG59XG4uY3VzdG9tLWFkZC1idG57XG4gIGhlaWdodDogMzBweDtcbn1cblxuLmFtYy1mb3ItdHdvLVllYXJ7XG4gIGJhY2tncm91bmQtY29sb3I6I0ZGNTI1MjtcbiAgcGFkZGluZzogNXB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC13ZWlnaHQ6Ym9sZDtcbn1cbmlvbi1jYXJke1xuICBtYXJnaW4tdG9wOjVweCAhaW1wb3J0YW50O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4ICFpbXBvcnRhbnQ7XG59XG4uYWRkZWR7XG4gIGJhY2tncm91bmQtY29sb3I6IzRDQUY1MDtcbiAgcGFkZGluZzogNXB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC13ZWlnaHQ6Ym9sZDtcbn1cbi5hZGQtdG8tY2FydHtcbiAgYmFja2dyb3VuZC1jb2xvcjojRkY1MjUyO1xuICBwYWRkaW5nOiAycHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXdlaWdodDpib2xkO1xufVxuXG46Om5nLWRlZXAgaW5wdXRbdHlwZT1udW1iZXJdOjotd2Via2l0LWlubmVyLXNwaW4tYnV0dG9uLFxuOjpuZy1kZWVwIGlucHV0W3R5cGU9bnVtYmVyXTo6LXdlYmtpdC1vdXRlci1zcGluLWJ1dHRvbiB7XG4td2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XG4tbW96LWFwcGVhcmFuY2U6IG5vbmU7XG5hcHBlYXJhbmNlOiBub25lO1xubWFyZ2luOiAwO1xufVxuXG4vKiBDaHJvbWUsIFNhZmFyaSwgRWRnZSwgT3BlcmEgKi9cbmlucHV0Ojotd2Via2l0LW91dGVyLXNwaW4tYnV0dG9uLFxuaW5wdXQ6Oi13ZWJraXQtaW5uZXItc3Bpbi1idXR0b24ge1xuICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XG4gIG1hcmdpbjogMDtcbn1cblxuLyogRmlyZWZveCAqL1xuaW5wdXRbdHlwZT1udW1iZXJdIHtcbiAgLW1vei1hcHBlYXJhbmNlOiB0ZXh0ZmllbGQ7XG59XG5cbi5jdXN0b20tc2VyYXJjaHtcbiAgaGVpZ2h0OiAzNXB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi10b3A6IDEycHggIWltcG9ydGFudDtcbn1cbi5zYy1pb24tc2VhcmNoYmFyLWlvcy1oe1xuICAtd2Via2l0LXBhZGRpbmctZW5kOiAwcHggIWltcG9ydGFudDsgXG4gICBwYWRkaW5nLWlubGluZS1lbmQ6IDBweCAhaW1wb3J0YW50O1xufVxuLmN1c3RvbS1zZXJhcmNoLWRldmljZXtcbiAgaGVpZ2h0OiAzNXB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi10b3A6IDEycHggIWltcG9ydGFudDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwLjNlbSAhaW1wb3J0YW50O1xuICAtLXBhZGRpbmctZW5kOiAwLjNlbSAhaW1wb3J0YW50O1xufVxuIl19 */";

/***/ }),

/***/ 33383:
/*!***********************************************!*\
  !*** ./src/app/app.component.html?ngResource ***!
  \***********************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ion-app>\n  <ion-split-pane contentId=\"main-content\" >\n    <ion-menu contentId=\"main-content\" type=\"overlay\">\n     \n      <ion-content>\n        <ion-list style=\"background-color: #F39E20; padding:20px 0px 0px 0px\">\n\n          <ion-row>\n            <ion-col size=\"3\">\n              <img *ngIf=\"loginauth.logoimgeBase64==''\" src='./assets/icon/ic_launcher.png' class=\"custom-avatar\"/>\n\n              <img [src]=\"loginauth.logoimgeBase64\" class=\"custom-avatar\"/>\n          </ion-col>\n          <ion-col size=\"9\" *ngIf=\"loginauth.defaultprofile\">\n            <ion-list-header style=\"color:#FFF; margin-top: 10px;\">Welcome</ion-list-header>\n            <ion-note style=\"color:#FFF; width: 100%; font-size: small;\">{{loginauth.defaultprofile[0].name}} </ion-note>\n          </ion-col>\n          </ion-row>    \n\n        \n          <ion-menu-toggle auto-hide=\"false\" *ngFor=\"let p of appPages\">\n            <ion-item lines=\"none\" [routerDirection]=\"'root'\"  [routerLink]=\"[p.url]\" *ngIf=\"loginauth.logintype==='wms' && \n            p.isNewLead===true && p.isExistingLead===true  && \n            p.isBusinessPartnerAddress===true && p.isNewOrder===true && p.isDraftOrder===true && p.isOrderStatus===true &&\n            p.isLatLongFinder===true && p.isTravelPlan===true && p.isActualTravelPlan===true &&\n            p.isTravelExpense===true && p.isTravelPlanClosure===true && p.isApprovalAccess===true &&\n            p.isschemeinfo===true && p.isCustomerServiceAccess===true && p.isComplaintReportingAccess===true && \n            p.isCompliantAcceptanceAccess===true && p.isFieldVisitAccess===true && p.isQuotationAccess===true && \n            p.isQuotationApproval===true && p.isConsolidationOrder===true && p.isUpload===true && p.isReport===true && p.isarvisitschedule===true\">\n             \n            <ion-icon slot=\"start\" [name]=\"p.icon\" style=\"color:#F39E20;\"></ion-icon>\n              <ion-label>\n                {{p.title}}\n              </ion-label>\n            </ion-item>\n           \n          </ion-menu-toggle>\n\n          <!-- <ion-menu-toggle auto-hide=\"false\" *ngFor=\"let p of checkcust\">\n            <ion-item lines=\"none\" [routerDirection]=\"'root'\" [routerLink]=\"[p.url]\" *ngIf=\"loginauth.logintype==='wms' && \n            loginauth.defaultprofile[0].mmstOrderusrtype==='CEB' && \n            p.isNewLead===true && p.isDraftOrder===true && p.isschemeinfo===true && p.isNewOrder === true &&\n            p.isOrderStatus===true && p.isLatLongFinder===true && p.isTravelPlan===true &&\n            p.isActualTravelPlan===true && p.isTravelExpense===true && p.isTravelPlanClosure===true &&\n            p.isApprovalAccess===true && p.isDraftOrder===true && p.isCustomerServiceAccess===true && \n            p.isComplaintReportingAccess===true && p.isCompliantAcceptanceAccess===true && p.isFieldVisitAccess===true && \n            p.isQuotationAccess===true && p.isQuotationApproval===true && p.isConsolidationOrder===true && p.isUpload===true  && \n            p.isReport===true\">\n              <ion-icon slot=\"start\" [name]=\"p.icon\" style=\"color: #F39E20;\"></ion-icon>\n              <ion-label>\n                {{p.title}}\n              </ion-label>\n            </ion-item>\n          \n          </ion-menu-toggle> -->\n\n          <ion-menu-toggle auto-hide=\"false\" *ngFor=\"let p of vetappPages\">\n            <ion-item [routerDirection]=\"'root'\" [routerLink]=\"[p.url]\" *ngIf=\"loginauth.logintype==='vet' && \n             p.approvalaccess===true  && p.isschemeinfo===true\">\n              <ion-icon slot=\"start\" [name]=\"p.icon\" style=\"color: #F39E20;\"></ion-icon>\n              <ion-label>\n                {{p.title}}\n              </ion-label>\n            </ion-item>\n          </ion-menu-toggle>\n\n\n        </ion-list>\n      </ion-content>\n      <ion-footer class=\"ion-no-border\">\n        <ion-item lines=\"none\" *ngIf=\"!iswebplatform\">\n          <ion-icon slot=\"start\" name=\"moon\" style=\"color: #F39E20;\"></ion-icon>\n          <ion-label>Toggle Dark Theme</ion-label>\n          <ion-toggle slot=\"end\" [ngModel]=\"darkMode\" (ionChange)=\"themeMode()\"></ion-toggle>\n                  </ion-item>\n\n        <ion-toolbar >\n          <ion-title style=\"font-size: xx-small;padding: inherit;\" color=\"primary\"> VERSION {{VERSION_NO}}</ion-title>\n        </ion-toolbar>\n      </ion-footer>\n     </ion-menu>\n    <ion-router-outlet id=\"main-content\"></ion-router-outlet>\n  </ion-split-pane>\n</ion-app>";

/***/ }),

/***/ 32672:
/*!****************************************************************!*\
  !*** ./src/app/custom-alert/custom-alert.page.html?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ion-content>\n  <form [formGroup]=\"alertFormGroup\" (ngSubmit)=\"save()\">\n  <ion-grid>\n    <ion-row>\n      <ion-col text-center>\n        <ion-label>New Order</ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col text-center>\n        <ion-label>Save Template</ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <ion-item class=\"login-item\">\n            <ion-input formControlName=\"txtTemplateName\" type=\"text\" placeholder=\"Enter Template Name\" maxlength=\"50\" required></ion-input>\n        </ion-item>\n        <div *ngIf=\"showError\"><small><ion-label color=\"red\">{{response.logmsg}}</ion-label></small></div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  </form>\n</ion-content>\n<ion-footer class=\"footer-back-color\">\n  <ion-row>\n    <ion-col class=\"ion-no-padding\">\n      <ion-button  (click)=\"cancel()\"  color=\"primary\"  size=\"large\" expand=\"full\" fill=\"outline\" class=\"footer-btn-color\">\n        <ion-label style=\"color: white;\">\n          Cancel\n        </ion-label>\n      </ion-button>\n    </ion-col>\n    <ion-col class=\"ion-no-padding\">\n      <ion-button  (click)=\"save()\" class=\"submit-btn\" [disabled]=\"!alertFormGroup.valid\" color=\"primary\"  size=\"large\" expand=\"full\" fill=\"outline\" class=\"footer-btn-color\">\n        <ion-label style=\"color: white;\">\n          Save\n        </ion-label>\n      </ion-button>\n    </ion-col>\n  </ion-row>\n  \n</ion-footer>\n";

/***/ }),

/***/ 41729:
/*!**************************************************!*\
  !*** ./src/app/login/login.page.html?ngResource ***!
  \**************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ion-content no-bounce class=\"main-content\">\n  <div class=\"img-contaimer\" *ngIf=\"loginrequired\">\n    <img *ngIf=\"!msg.isplatformweb\" class=\"height-img\" src=\"../../assets/test.png\" />\n  </div>\n  <section id=\"loginFormSection\" *ngIf=\"loginrequired\" [ngClass]=\"{'logingrid' : msg.isplatformweb}\"\n    [ngStyle]=\"{'background-image':msg.isplatformweb === true ? 'url('+backgroundimg+')' : 'none' }\">\n    <form (ngSubmit)=\"loginSubmit()\" #loginForm=\"ngForm\">\n      <ion-grid>\n        <ion-row justify-content-center>\n          <ion-col align-self-center class=\"logincard\">\n            <ion-card style=\" background-color: white;\" [ngClass]=\"{'login-form-section' : msg.isplatformweb}\">\n              <div style=\"text-align:center\">\n                <h1>Welcome</h1>\n              </div>\n              <div padding>\n                <ion-item style=\"border: 1px solid lightgray;margin-left: 20px;margin-right: 20px;\">\n                  <ion-icon name=\"person\" size=\"large\" slot=\"start\"></ion-icon>\n                  <ion-input id=\"txtUserName\" name=\"username\" type=\"text\" placeholder=\"Username\" [(ngModel)]=\"username\"\n                    maxlength=\"40\" required></ion-input>\n                </ion-item><br>\n                <ion-item  *ngIf=\"!isforgetpass\"style=\"border: 1px solid lightgray;margin-left: 20px;margin-right: 20px;\">\n                  <ion-icon name=\"lock-closed\" size=\"large\" slot=\"start\"></ion-icon>\n                  <ion-input id=\"txtPassword\" name=\"password\" type=\"password\" placeholder=\"Password\"\n                    [(ngModel)]=\"password\" maxlength=\"20\" required></ion-input>\n                </ion-item>\n                <ion-item  *ngIf=\"showChangePassword\" style=\"border: 1px solid lightgray;margin-left: 20px;margin-right: 20px;\">\n                  <ion-icon name=\"key\" size=\"large\" slot=\"start\"></ion-icon>\n                  <ion-input id=\"txtPassword\" name=\"otp\" type=\"password\" placeholder=\"OTP\" [(ngModel)]=\"otp\"\n                    maxlength=\"20\" required></ion-input>\n                </ion-item>\n                <ion-item  *ngIf=\"showChangePassword\" style=\"border: 1px solid lightgray;margin-left: 20px;margin-right: 20px;\">\n                  <ion-icon name=\"lock-closed\" size=\"large\" slot=\"start\"></ion-icon>\n                  <ion-input id=\"txtPassword\" name=\"password\" type=\"password\" placeholder=\"Enter New Password\"\n                    [(ngModel)]=\"password\" maxlength=\"20\" required></ion-input>\n                </ion-item>\n                <br>\n                <ion-item *ngIf=\"!isforgetpass && !msg.isplatformweb\" lines=\"none\">\n                  <ion-label>{{authText}}</ion-label>\n                  <ion-toggle name=\"authStatus\" mode=\"ios\" [(ngModel)]=\"logBiometricstatus\"\n                    [checked]=\"logBiometricstatus\" (ionChange)=\"biometricStatusChange($event)\"></ion-toggle>\n                </ion-item>\n              </div>\n              <div class=\"submit-btn\">\n                <ion-button *ngIf=\"showLoginButton\" color=\"vibrant\" (click)=\"loginSubmit()\"  expand=\"block\"\n                   [disabled]=\"!loginForm.valid\">Login</ion-button>\n                <ion-button *ngIf=\"showGetOTPButton\" color=\"vibrant\" (click)=\"onClickGetOTP()\" \n                  expand=\"block\"  [disabled]=\"!loginForm.valid\">Get OTP</ion-button>\n                <ion-button *ngIf=\"showChangePassword\" (click)=\"onChangePass()\" color=\"vibrant\" \n                  expand=\"block\"  [disabled]=\"!loginForm.valid\">Change Password</ion-button>\n              </div>\n              <div *ngIf=\"!isforgetpass\">\t\n                <ion-row>\t\n                  <ion-col text-left (click)=\"onClickForgetPass()\">\t\n                <h5 style=\"text-decoration: underline;cursor:pointer\">Forgot Password?</h5>\t\n              </ion-col>\t\n              <ion-col  (click)=\"onClickNewRegistration()\"style=\"text-align:right\"> \t\n                <h5 style=\"text-decoration: underline;cursor:pointer\">Lead Creation</h5>\t\n              </ion-col>\t\n              </ion-row>\t\n              </div>\n              <div *ngIf=\"isforgetpass && loginForm.valid\" text-center (click)=\"onClickForgetPass()\">\n                <h5>OTP will be send on your register mobile number</h5>\n              </div>\n              <div>\n                <h5>{{txterror}}</h5>\n              </div>\n              <div>\n                <h5>{{txthint}}</h5>\n              </div>\n            </ion-card>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </form>\n  </section>\n  <section *ngIf=\"!loginrequired && showCustomPassword && msg.isplatformweb == false\" class=\"sec-password-template\">\n    <ion-grid class=\"four-digit-password\">\n      <ion-row>\n        <ion-col class=\"text-align-center\">\n          <ion-avatar>\n            <div style=\"background-color: brown;\"></div>\n          </ion-avatar>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col class=\"text-align-center\">\n          <div class=\"profile-name\">Welcome</div>\n        </ion-col>\n      </ion-row>\n\n      <ion-row class=\"text-align-center\">\n\n        <ion-col offset=\"1.8\" size=\"2\">\n          <ion-avatar>\n            <div [ngClass]=\"codeone || codeone == 0 ? 'rounded-div': 'rounded-div-selected'\">\n              <div *ngIf=\"codeone || codeone == 0\">&#10033;</div>\n            </div>\n          </ion-avatar>\n        </ion-col>\n\n        <ion-col size=\"2\">\n          <ion-avatar>\n            <div [ngClass]=\"codetwo || codetwo == 0 ? 'rounded-div': 'rounded-div-selected'\">\n              <div *ngIf=\"codetwo || codetwo == 0\">&#10033;</div>\n            </div>\n          </ion-avatar>\n        </ion-col>\n        <ion-col size=\"2\">\n          <ion-avatar>\n            <div [ngClass]=\"codethree || codethree == 0 ? 'rounded-div': 'rounded-div-selected'\">\n              <div *ngIf=\"codethree || codethree == 0\">&#10033;</div>\n            </div>\n          </ion-avatar>\n        </ion-col>\n        <ion-col size=\"2\">\n          <ion-avatar>\n            <div [ngClass]=\"codefour || codefour == 0 ? 'rounded-div': 'rounded-div-selected'\">\n              <div *ngIf=\"codefour || codefour == 0\">&#10033;</div>\n            </div>\n          </ion-avatar>\n        </ion-col>\n\n\n\n      </ion-row>\n\n      <ion-row size=\"12\" text-center padding>\n        <ion-col>\n          <div *ngIf=\"!message\">\n            <span>{{pageStatus}}</span>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"reset-pin-css\">\n        <ion-col>\n          <div class=\"reset-pin\" (click)=\"resetPin()\">\n            Reset Pin!\n          </div>\n        </ion-col>\n      </ion-row>\n\n\n      <ion-row text-center style=\"margin-left: -1%;\">\n        <ion-card class=\"number-dial-card\">\n\n          <ion-row class=\"row-digit\">\n            <ion-col class=\"col-right col-all\">\n              <div class=\"digit\" (click)=\"add('1')\">1</div>\n            </ion-col>\n            <ion-col class=\"col-right col-all\">\n              <div class=\"digit\" (click)=\"add('2')\">2</div>\n            </ion-col>\n            <ion-col class=\"col-all\">\n              <div class=\"digit\" (click)=\"add('3')\">3</div>\n            </ion-col>\n          </ion-row>\n          <ion-row class=\"row-digit \">\n            <ion-col class=\"col-right col-all\">\n              <div class=\"digit\" (click)=\"add('4')\">4</div>\n            </ion-col>\n            <ion-col class=\"col-right col-all\">\n              <div class=\"digit\" (click)=\"add('5')\">5</div>\n            </ion-col>\n            <ion-col class=\"col-all\">\n              <div class=\"digit\" (click)=\"add('6')\">6</div>\n            </ion-col>\n          </ion-row>\n          <ion-row class=\"row-digit\">\n            <ion-col class=\"col-right col-all\">\n              <div class=\"digit\" (click)=\"add('7')\">7</div>\n            </ion-col>\n            <ion-col class=\"col-right col-all\">\n              <div class=\"digit\" (click)=\"add('8')\">8</div>\n            </ion-col>\n            <ion-col class=\"col-all\">\n              <div class=\"digit\" (click)=\"add('9')\">9</div>\n            </ion-col>\n          </ion-row>\n          <ion-row style=\"min-height: 50px !important;\">\n            <ion-col class=\"col-right col-all\">\n              <div class=\"digit\" (click)=\"delete()\">\n                <ion-icon name=\"backspace\"></ion-icon>\n              </div>\n            </ion-col>\n            <ion-col class=\"col-right col-all\">\n              <div class=\"digit\" (click)=\"add('0')\">0</div>\n            </ion-col>\n            <ion-col class=\"col-all\">\n              <!-- <div class=\"digit\"><ion-button  shape=\"round\" color=\"success\" expand=\"full\">OK</ion-button></div> -->\n            </ion-col>\n          </ion-row>\n\n        </ion-card>\n      </ion-row>\n\n\n\n    </ion-grid>\n\n  </section>\n  <section id=\"loginWithFaceSection\" class=\"height100\"\n    *ngIf=\"!loginrequired && !showCustomPassword && showFaceIdSection\">\n    <ion-grid class=\"height100\">\n      <ion-row class=\"vertical-center\">\n        <ion-col>\n          <!-- Icons -->\n          <ion-button (click)=\"onfingerPrintshow()\" style=\"height: 105px !important;\">\n            <ion-icon slot=\"start\" name=\"person\"></ion-icon>\n            Tap Here To Authenticate.\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </section>\n\n\n</ion-content>";

/***/ }),

/***/ 88716:
/*!********************************************************!*\
  !*** ./src/app/neworder/neworder.page.html?ngResource ***!
  \********************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      New Order\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <form [formGroup]=\"formprod\" (ngSubmit)=\"onSaveOrder(formprod.value)\">\n    <ion-grid fixed>\n      <div>\n        <ion-card>\n          <ion-card-content>\n            <ion-row>\n              <ion-col>\n                <h5 ion-text class=\"text-primary\">\n                  <ion-icon name=\"person\"></ion-icon> Customer Detail :\n                </h5>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"stacked\">Customer<span style=\"color:red!important\">*</span></ion-label>\n                  <ionic-selectable [searchDebounce]=\"1000\" placeholder=\"Select Customer\" disable=\"true\"\n                    formControlName=\"selectedBusinessPartner\" [items]=\"BusinessPartnerlist\"\n                    itemValueField=\"id\" itemTextField=\"_identifier\" [canSearch]=\"true\" (onChange)=\"oncustchange($event)\"\n                    (onClose)=\"onClose($event)\" (onSearch)=\"custsearchChange($event)\">\n                  </ionic-selectable>\n                  <!-- (onSearch)=\"custsearchChange($event)\" -->\n                </ion-item>\n                <div padding-left>\n                  <ng-container *ngFor=\"let validation of validation_messages.selectedBusinessPartner\">\n                    <div\n                      *ngIf=\"formprod.get('selectedBusinessPartner').hasError(validation.type) && formprod.get('selectedBusinessPartner').touched\">\n                      <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                    </div>\n                  </ng-container>\n                </div>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"stacked\">Order Type<span style=\"color:red!important\">*</span></ion-label>\n                  <ion-select placeholder=\"Select One\" [disabled]=\"Iscartproduct\"\n                    formControlName=\"selectedordertype\" interface=\"popover\"\n                    (ionChange)=\"onOrderTypeChange()\">\n                    <!-- <ion-select-option value=\"SL\">Saleable</ion-select-option>\n                    <ion-select-option value=\"NE\">Near Expiry</ion-select-option>\n                    <ion-select-option value=\"ST\" *ngIf=\"mmstOrderusrtype!='CEB'\">Sample Order - Sales Team\n                    </ion-select-option>\n                    <ion-select-option value=\"SOC\" *ngIf=\"mmstOrderusrtype!='CEB'\">Sample Order-Customer\n                    </ion-select-option> -->\n                    <ion-select-option *ngFor=\"let orderType of orderTypeList\" [value]=\"orderType.code\">{{orderType.name}}\n                    </ion-select-option>\n                 \n                  </ion-select>\n                </ion-item>\n                <div padding-left>\n                  <ng-container *ngFor=\"let validation of validation_messages.selectedordertype\">\n                    <div\n                      *ngIf=\"formprod.get('selectedordertype').hasError(validation.type) && formprod.get('selectedordertype').touched\">\n                      <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                    </div>\n                  </ng-container>\n                </div>\n              </ion-col>\n            </ion-row>\n            <!-- Primary Customer Drop Down -->\n            <ion-row *ngIf=\"!isPrimaryCustomer\">\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"stacked\">Primary Customer</ion-label>\n                  <ionic-selectable #portComponent placeholder=\"Select Primary Customer\" [canSearch]=\"true\" disable=\"true\" \n                    formControlName=\"selectedPrimaryBusinessPartner\" [items]=\"primaryBusinessPartnerList\"\n                    itemValueField=\"bpid\" itemTextField=\"name\" (onChange)=\"onPrimaryCustomerChange($event)\"\n                    (onClose)=\"onPrimaryCustomerClose($event)\" (onSearch)=\"onPrimaryCustSearch($event)\">\n                  </ionic-selectable>\n                </ion-item>\n        \n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"floating\">Activity</ion-label>\n                  <ion-input type=\"text\" value=\"{{activity}}\" [disabled]=\"true\"></ion-input>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-row *ngIf=\"showcrlimit\">\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"floating\">Credit Limit</ion-label>\n                  <ion-input type=\"text\" formControlName=\"CreditLimit\" [disabled]=\"true\"></ion-input>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-row *ngIf=\"Showoverdue\">\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"floating\">Overdue Invoice</ion-label>\n                  <ion-input type=\"text\" formControlName=\"OverdueInvoice\" [disabled]=\"true\">\n                  </ion-input>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-row *ngIf=\"Showoverdue\">\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"floating\">Overdue amount</ion-label>\n                  <ion-input type=\"text\" formControlName=\"Overdueamount\" [disabled]=\"true\"></ion-input>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-row *ngIf=\"Showoverdue\">\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"floating\">Ageing from Oldest Invoice</ion-label>\n                  <ion-input type=\"text\" formControlName=\"duedatedays\" [disabled]=\"true\"></ion-input>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n        <ion-card>\n          <ion-card-content>\n            <ion-row>\n              <ion-col>\n                <h5 ion-text class=\"text-primary\">\n                  <ion-icon name=\"locate\"></ion-icon> Shipping Information:\n                </h5>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"stacked\">Shipping Address<span style=\"color:red!important\">*</span></ion-label>\n                  <ion-select [disabled]=\"Iscartproduct\" #C\n                    formControlName=\"selectedBPaddressshipping\" interface=\"popover\" multiple=\"false\"\n                    placeholder=\"Select Address\">\n                    <ion-select-option *ngFor=\"let sadd of BPaddressshipping\" [value]=\"sadd.id\">{{sadd.name}}\n                    </ion-select-option>\n                  </ion-select>\n                </ion-item>\n                <div padding-left>\n                  <ng-container *ngFor=\"let validation of validation_messages.selectedBPaddressshipping\">\n                    <div\n                      *ngIf=\"formprod.get('selectedBPaddressshipping').hasError(validation.type) && formprod.get('selectedBPaddressshipping').touched\">\n                      <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                    </div>\n                  </ng-container>\n                </div>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"stacked\">Billing Address<span style=\"color:red!important\">*</span></ion-label>\n                  <ion-select [disabled]=\"Iscartproduct\" #C\n                    formControlName=\"selectedBPaddressbilling\" interface=\"popover\" multiple=\"false\"\n                    placeholder=\"Select Address\">\n                    <ion-select-option *ngFor=\"let badd of BPaddressbilling\" [value]=\"badd.id\">{{badd.name}}\n                    </ion-select-option>\n                  </ion-select>\n                </ion-item>\n                <div padding-left>\n                  <ng-container *ngFor=\"let validation of validation_messages.selectedBPaddressbilling\">\n                    <div\n                      *ngIf=\"formprod.get('selectedBPaddressbilling').hasError(validation.type) && formprod.get('selectedBPaddressbilling').touched\">\n                      <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                    </div>\n                  </ng-container>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n        <ion-card>\n          <ion-card-content>\n            <ion-row>\n              <ion-col>\n                <h5 ion-text class=\"text-primary\">\n                  <ion-icon name=\"bookmarks\"></ion-icon> Order Detail:\n                </h5>\n              </ion-col>\n            </ion-row>\n            <ion-row *ngIf=\"!loginauth.selectedactivity.iscashdiscovertraddisc\">\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"stacked\">Create from Template</ion-label>\n                  <ion-select #C formControlName=\"selectedBPtemplate\" interface=\"popover\"\n                    multiple=\"false\" aria-placeholder=\"Select Template\" (ionChange)=\"onTemplateChange()\"\n                    [disabled]=\"Isshowtemplate\">\n                    <ion-select-option *ngFor=\"let sadd of BPtemplate\" [value]=\"sadd.id\">{{sadd.sname}}\n                    </ion-select-option>\n                  </ion-select>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-row *ngIf=\"!!selectedBusinessPartner && selectedBusinessPartner.special_order == 'Y' \">\n              <ion-col> \n                <ion-item>\n                  <ion-label>Special Order</ion-label>\n                  <ion-checkbox [disabled]=\"Iscartproduct\" slot=\"end\" [(ngModel)]=\"sp_order_chk_box\" formControlName=\"specialOrderCtrl\"></ion-checkbox>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-row *ngIf=\"is_advance_payment == true\">\n              <ion-col> \n                <ion-item>\n                  <ion-label>Advance Payment</ion-label>\n                  <ion-checkbox [disabled]=\"Iscartproduct || loginauth.selectedactivity.iscashdiscovertraddisc\" slot=\"end\" [(ngModel)]=\"advance_payment_chk_status\" formControlName=\"isAdvancePaymentChk\" (ionChange)=\"onChangeAdvancechk()\"></ion-checkbox>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"floating\">PO Number</ion-label>\n                  <ion-input formControlName=\"ponumber\" type=\"text\"></ion-input>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-card>\n              <ion-row>\n                <ion-col *ngIf=\"isdesktop===false\" size=\"12\">\n                  <ion-row>\n                    <ion-col>\n                      <ion-button (click)=\"getpoImage()\">Customer PO</ion-button>\n                    </ion-col>\n                    <div class=\"pdf-help-text-device\"><ion-label><small>Images and PDF supported Maximum file size 3 MB limit.</small></ion-label></div>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"4\">\n                      <img src=\"./assets/transparent-pdf1.png\" *ngIf=\"fileType=='pdf'\"\n                        style=\"width: 50px; height: 50px;\">\n                    </ion-col>\n                    <ion-col size=\"8\">\n                      <small>\n                        <ion-label>{{fileName}}</ion-label>\n                      </small>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n                <ion-col *ngIf=\"isdesktop===true\">\n                  <ion-item>\n                  <div style=\"padding:10px\">\n                    <input type=\"file\" name=\"file\" accept=\"image/*,application/pdf\" id='selectedFile'\n                    (change)=\"uploadImage($event)\" class=\"inputfile\" />\n                    <label class=\"myFakeUploadButton\" for=\"selectedFile\">Customer PO</label>\n                    \n                  </div>\n                \n                </ion-item>\n                <div class=\"pdf-help-text\"><ion-label>Images and PDF supported Maximum file size 3 MB limit.</ion-label></div>\n               \n                </ion-col>\n                <ion-col>\n                  <img (click)=\"ImageViewr(POimg64)\" [src]=\"'data:image/jpeg;base64,'+POimg64\"\n                    *ngIf=\"POimg64 && fileType!='pdf'\" style=\"width: 50px; height: 50px;\">\n                  <img src=\"./assets/transparent-pdf1.png\" *ngIf=\"POimg64 && fileType=='pdf' && isdesktop===true\" \n                    style=\"width: 50px; height: 50px;\">\n                    <small  *ngIf=\"isdesktop===true\">\n                      <ion-label>{{fileName}}</ion-label>\n                    </small>\n                </ion-col>\n              </ion-row>\n            \n\n            </ion-card>\n          </ion-card-content>\n        </ion-card>\n        <ion-card *ngIf=\"checkboxtab\">\n          <ion-card-content>\n            <ion-row>\n              <ion-col size=\"8\">\n                <h5 ion-text class=\"text-primary\">\n                  <ion-icon name=\"bookmark\"></ion-icon> Redeem Reward:\n                </h5>\n              </ion-col>\n              <ion-col size=\"4\">\n                <h5 ion-text class=\"text-primary\" style=\"text-align: right;\">\n                  <ion-checkbox slot=\"end\" [(ngModel)]=\"showhideRedeemReward\" [ngModelOptions]=\"{standalone: true}\"\n                    [disabled]=\"autocalculation\"></ion-checkbox>\n                </h5>\n              </ion-col>\n            </ion-row>\n            <ion-row *ngIf=\"showhideRedeemReward\">\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"floating\">Points avaliable for Redeem</ion-label>\n                  <ion-input type=\"text\" formControlName=\"avaliableRedeem\" [disabled]=\"true\">\n                  </ion-input>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-row *ngIf=\"showhideRedeemReward\">\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"floating\">Total Points to be Redeem</ion-label>\n                  <ion-input type=\"text\" formControlName=\"tobeRedeem\" [disabled]=\"autocalculation\"\n                    (change)='onChangetobeRedeem()'></ion-input>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n        <ion-card>\n          <ion-card-content>\n            <ion-row *ngIf=\"showExpectedDeliveryDateCtrl\">\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"floating\">Expected Delivery Date</ion-label>\n                  <!-- <ion-datetime formControlName=\"formprod.controls['expecteddeliverydate']\" displayFormat=\"DD-MM-YYYY\"\n                    [disabled]=\"expireDateEdit\" [min]=\"today\" max=\"2050-12-31\" placeholder=\"Select Date\"></ion-datetime> -->\n                    <ion-item>\n                      <ion-input placeholder=\"Select Date\" [value]=\"dateexpire\"></ion-input>\n                      <ion-button fill=\"clear\" id=\"open-date-input-1\">\n                        <ion-icon icon=\"calendar\"></ion-icon>\n                      </ion-button>\n                      <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\n                        <ng-template>\n                          <ion-datetime\n                            #popoverDatetime2\n                            formControlName=\"expecteddeliverydate\"\n                            presentation=\"date\"\n                            [disabled]=\"expireDateEdit\" [min]=\"today\" max=\"2050-12-31\"\n                            showDefaultButtons=\"true\"\n                            (ionChange)=\"dateexpire = formatDate(popoverDatetime2.value)\"\n                          ></ion-datetime>\n                        </ng-template>\n                      </ion-popover>\n                    </ion-item>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-item>\n                  <ion-label position=\"floating\">Order Instructions</ion-label>\n                  <ion-textarea type=\"text\" formControlName=\"orderdescription\"></ion-textarea>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </div>\n    </ion-grid>\n  </form>\n  <ion-card>\n    <ion-card-content>\n      <ion-row>\n        <ion-col>\n          <h5 ion-text class=\"text-primary\">\n            <ion-icon name=\"cart\"></ion-icon> Products:\n          </h5>\n        </ion-col>\n        <ion-col>\n          <ion-fab-button size=\"small\" float-right (click)=\"onAddProduct()\">\n            <ion-icon name=\"add\"></ion-icon>\n          </ion-fab-button>\n        </ion-col>\n      </ion-row>\n      <ion-item *ngFor=\"let item of cartproduct; index as i\" text-wrap style=\"font-size: small;\">\n        <!-- <ion-content scrollX=\"true\"> -->\n        <div style=\"width: 100%;\" (click)=\"toggle(item)\">\n          <ion-row>\n            <ion-col size=\"8\">\n              <ion-label style=\"white-space: normal\">\n                <ion-icon *ngIf=\"!item.MainProductQty\" style=\"color: springgreen;\" name=\"pricetags\"></ion-icon>\n                {{ item.product }}\n              </ion-label>\n            </ion-col>\n            <ion-col size=\"2\" style=\"width: 100%; text-align: right;\" *ngIf=\"item.MainProductQty\">\n              <ion-icon name=\"create\" (click)=\"editProduct(item)\" style=\"font-size: x-large;color: green;\"></ion-icon>\n            </ion-col>\n\n            <ion-col size=\"2\" style=\"width: 100%; text-align: right;\" *ngIf=\"item.MainProductQty\">\n              <ion-icon name=\"trash\" (click)=\"removeProduct(item)\" style=\"font-size: x-large;color: red;\"></ion-icon>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col>\n              <ion-label color=\"green\" style=\"font-size: small;font-weight: bold;\" *ngIf=\"item.MainProductQty!=='0' && item.MainProductQty!==''\">Qty: {{\n                item.MainProductQty }}</ion-label>\n                <ion-label color=\"green\" style=\"font-size: small;font-weight: bold;\" *ngIf=\"!!item.enteredfreeqty && item.enteredfreeqty!=='0' && item.enteredfreeqty!==''\">Entered Free Qty: {{\n                  item.enteredfreeqty }}</ion-label>\n                <ion-label color=\"green\" style=\"font-size: small;font-weight: bold;\" *ngIf=\"item.MainProductQty!=='0' && item.MainProductQty!==''\">{{item.value}}: {{ item.Rate\n                  |number:'1.2-2'}}</ion-label>\n                <ion-label color=\"green\" style=\"font-size: small;font-weight: bold;\" *ngIf=\"item.freeqty\">Free Qty: {{ item.freeqty }}</ion-label>  \n            </ion-col>\n            <ion-col>\n              <ion-label color=\"green\" style=\"font-size: small;font-weight: bold;\"  *ngIf=\"item.MainProductQty!=='0' && item.MainProductQty!==''\">Net Value: {{item.MainProductQty * item.Rate | number:'1.2-2' }}</ion-label>\n              <ion-label color=\"green\" style=\"font-size: small;font-weight: bold;\" *ngIf=\"item.MainProductQty!=='0' && item.MainProductQty!==''\">Gross Value: {{item.TotalAmount | number:'1.2-2'}}</ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col nowrap>\n              <div *ngIf=\"item.show\">\n                \n                <ion-label style=\"font-size: small;\" *ngIf=\"item.MainProductQty!=='0' && item.MainProductQty!==''\">UOM: {{ item.UOM }}</ion-label>\n                <ion-label style=\"font-size: small;\" *ngIf=\"item.MainProductQty!=='0' && item.MainProductQty!==''y\">{{item.value}}: {{ item.Rate\n                  |number:'1.2-2'}}</ion-label>\n\n                <ion-label style=\"font-size: small;\" *ngIf=\"loginauth?.Discount_Method=='DOR' && item.DiscountPer \">Rate after Discount:\n                  {{(item.Rate - ((item.Rate/100)*item.DiscountPer)) | roundPipe}} <span\n                    *ngIf=\"item.DiscountPer\">({{item.DiscountPer | number:'1.2-2'}}%)</span></ion-label>\n\n                <ion-label  *ngIf=\"item.Scheme\" style=\"font-size: small;\">Scheme Information: {{ item.Scheme }}</ion-label>\n                \n                <ion-label style=\"font-size: small;\" *ngIf=\"!item.MainProductQty\">Free Qty: {{ item.freeqty |\n                  number:'1.2-2'}}</ion-label>\n                \n                <ion-label style=\"font-size: small;\" *ngIf=\"item.MainProductQty!=='0' && item.MainProductQty!=='' && loginauth?.Discount_Method!='DOR' && item.discount\">\n                  Trade Discount: {{ item.discount | number:'1.2-2'}} <span *ngIf=\"item.DiscountPer\">({{item.DiscountPer\n                    | number:'1.2-2'}}%)</span></ion-label>\n                <ion-label style=\"font-size: small;\" *ngIf=\"!!item.cashdiscount\">Cash Discount: {{ (item.cashdiscount*item.MainProductQty*item.Rate/100) |\n                      number:'1.2-2'}}<span >({{item.cashdiscount\n                        | number:'1.2-2'}}%)</span></ion-label>\n                <ion-label style=\"font-size: small;\" *ngIf=\"item.MainProductQty!=='0' && item.MainProductQty!=='' && item.rewardpoint\">{{item.reward}}: {{ item.rewardpoint }}\n                </ion-label>\n                <!-- <ion-label style=\"font-size: small;\" *ngIf=\"item.MainProductQty\">Amount: {{ item.Amount | number:'1.2-2'}}</ion-label> -->\n               \n                <ion-label style=\"font-size: small;\" *ngIf=\"item.MainProductQty!=='0' && item.MainProductQty!==''\">Tax Information: {{ item.Tax }}\n                </ion-label>\n              \n                <ion-label style=\"font-size: small;\" *ngIf=\"item.MainProductQty!=='0' && item.MainProductQty!==''\">Tax Value: {{ item.GstAmount |\n                  number:'1.2-2'}}</ion-label>\n\n                <ion-label style=\"font-size: small;\" *ngIf=\"item.MainProductQty!=='0' && item.MainProductQty!==''\">Gross Value: {{\n                  item.TotalAmount | number:'1.2-2'}}</ion-label>\n              </div>\n            </ion-col>\n          </ion-row>\n        </div>\n        <!-- </ion-content> -->\n      </ion-item>\n    </ion-card-content>\n  </ion-card>\n  <ion-card>\n    <ion-card-content>\n      <ion-row>\n        <ion-col>\n          <h5 ion-text class=\"text-primary\">\n            <span style=\"color: lightcoral;\">&#x20b9;</span> Order Summary:\n          </h5>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"font-size: small;\">Total Amount: <br> {{sumamount | number:'1.2-2'}}</ion-label>\n        </ion-col>\n        <ion-col *ngIf=\"sumdiscount\">\n          <ion-label style=\"font-size: small;\">Total Discount:<br> {{sumdiscount | number:'1.2-2'}}</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"font-size: small;\">Total Tax:<br> {{sumGstAmount | number:'1.2-2'}}</ion-label>\n        </ion-col>\n        <!-- <ion-col *ngIf=\"checkredeem\">\n          <ion-label style=\"font-size: small;\">Redeem Points Used:<br> {{tobeRedeem | number:'1.2-2'}}</ion-label>\n        </ion-col> -->\n        <ion-col>\n          <ion-label style=\"font-size: small;\">Final Amount:<br> {{sumtotalamount | number:'1.2-2'}}</ion-label>\n        </ion-col>\n      </ion-row>\n\n    </ion-card-content>\n  </ion-card>\n  <ion-row *ngIf=\"!loginauth.selectedactivity.iscashdiscovertraddisc\">\n    <ion-col>\n      <ion-item float-right lines=\"none\">\n        <ion-button color=\"primary\" text-center [disabled]=\"!formprod.valid || !Iscartproduct\"\n          (click)=\"saveTemplate(formprod.value)\">Save Template</ion-button>\n        <!-- <ion-button color=\"primary\" text-center [disabled]=\"!formprod.valid || !Iscartproduct\" (click)=\"onSaveTemplate(formprod.value)\">Save Template</ion-button> -->\n      </ion-item>\n    </ion-col>\n  </ion-row>\n  <ion-item *ngIf=\"loginauth.isDraftOrder\" float-right lines=\"none\">\n    <ion-button color=\"primary\" text-center [disabled]=\"!formprod.valid || !Iscartproduct\"\n      (click)=\"onSaveOrder(formprod.value)\">Save as Draft Order</ion-button>\n  </ion-item>\n  <ion-item float-right lines=\"none\">\n    <ion-button color=\"primary\" text-center [disabled]=\"!formprod.valid || !Iscartproduct\"\n      (click)=\"onCompleteOrder(formprod.value)\">Order Complete</ion-button>\n  </ion-item>\n</ion-content>";

/***/ }),

/***/ 38636:
/*!****************************************************************!*\
  !*** ./src/app/product-list/product-list.page.html?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Product</ion-title>\n    <!-- <ion-buttons slot=\"start\">\n      <ion-button (click)=\"dismiss()\">Cancel</ion-button>\n    </ion-buttons> -->\n  </ion-toolbar>\n  <ion-searchbar animated [debounce]=\"1000\" showCancelButton=\"focus\" [disabled]=\"editMode\" (ionChange)=\"onChangeProductSearch($event)\"></ion-searchbar>\n  \n  <!-- <ion-row>\n    <ion-col size='10' no-padding no-margin>\n      <ion-searchbar animated  [debounce]=\"1000\" [disabled]=\"editMode\" (ionChange)=\"onChangeProductSearch($event)\"></ion-searchbar>\n    </ion-col>\n    <ion-col size='2' no-padding no-margin>\n     <ion-button *ngIf=\"msg.isplatformweb\" class=\"custom-serarch\">Search</ion-button>\n     <ion-button *ngIf=\"!msg.isplatformweb\" class=\"custom-serarch-device\"><small>Search</small></ion-button>\n    </ion-col>\n  </ion-row> -->\n \n</ion-header>\n\n<ion-content>\n  <form [formGroup]=\"productListForm\" novalidate style=\"width: -webkit-fill-available;\">\n    <section formArrayName=\"items\">\n      <ion-list  *ngFor=\"let product of productList;let i = index \" [formGroupName]=\"i\" class=\"back-color\" >\n       <ion-card>  \n        <ion-grid style=\"padding-left: 10px;\"> \n            <ion-row>\n              <ion-col size=\"9\">\n                <ion-label>\n                    <small>{{product._identifier}}</small>\n                  </ion-label>\n                  {{this.productListForm.get('items.i')}}\n              </ion-col>\n              <ion-col size=\"3\">\n               <button [ngClass]=\"{'added': product.hasOwnProperty('isSelected') == true,'add-to-cart': product.hasOwnProperty('isSelected') == false}\" (click)=\"onAddToCart(product,i)\">\n                <small>{{ product.hasOwnProperty('isSelected') ? 'Added' : 'Add To Cart' }}</small> \n                </button>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-row>\n                  <ion-col text-center>\n                    <small><ion-label class=\"asterisk_input\">Quantity</ion-label></small>\n                  </ion-col>\n                </ion-row>\n                <ion-row text-center>\n                  <ion-input type=\"text\" (keypress)=\"numberOnly($event)\" class=\"custom-class\" placeholder=\"Enter Qty\" formControlName=\"key\" min=\"0\">\n                  </ion-input>\n                 \n                </ion-row>\n              </ion-col>\n              <ion-col *ngIf=\"product.isreeproduct && special_order_product==='Y' && issplorderonfreeqty\">\n                <ion-row>\n                  <ion-col text-center>\n                    <small><ion-label >Free Qty</ion-label></small>\n                  </ion-col>\n                </ion-row>\n                <ion-row text-center>\n                  <ion-input type=\"text\" (keypress)=\"numberOnly($event)\" class=\"custom-class\" placeholder=\"Enter Free Qty\" formControlName=\"freeqty\" min=\"0\">\n                  </ion-input>\n                 \n                </ion-row>\n              </ion-col>\n              <ion-col>\n                <ion-row>\n                  <ion-col text-center>\n                    <small><ion-label>Min Order Qty</ion-label></small>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col text-center>\n                    <small>{{product.minorderqty}}</small>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              <ion-col>\n                <ion-row>\n                  <ion-col text-center>\n                    <small><ion-label>Pack Size</ion-label></small>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col text-center>\n                    <small>{{product.shipperqty}}</small>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              \n            </ion-row>\n            <!-- <ion-row>\n              <ion-col>\n              {{productListForm.get('items')['controls'][i].valid}}\n                <ion-button class=\"custom-add-btn\"  color=\"vibrant\" expand=\"block\" (click)=\"onAddToCart(product,i)\">Add to cart</ion-button>\n              </ion-col>\n            </ion-row> -->\n          </ion-grid>\n        </ion-card> \n      </ion-list>\n    </section>     \n  </form>\n</ion-content>\n<ion-footer class=\"footer-back-color\">\n  <ion-button  (click)=\"onClose()\"  color=\"primary\"  size=\"large\" expand=\"full\" fill=\"outline\" class=\"footer-btn-color\">\n    <ion-label style=\"color: white;\">\n      Close\n    </ion-label>\n  </ion-button>\n</ion-footer>\n";

/***/ }),

/***/ 42480:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/***/ (() => {

/* (ignored) */

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendor"], () => (__webpack_exec__(14431)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main.js.map