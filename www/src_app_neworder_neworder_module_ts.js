"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_neworder_neworder_module_ts"],{

/***/ 19195:
/*!*********************************************!*\
  !*** ./src/app/neworder/neworder.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NeworderPageModule": () => (/* binding */ NeworderPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _product_list_product_list_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../product-list/product-list.module */ 32305);
/* harmony import */ var _product_list_product_list_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../product-list/product-list.page */ 47214);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _neworder_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./neworder.page */ 10990);
/* harmony import */ var _common_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common/pipes/pipes.module */ 22522);
/* harmony import */ var _custom_alert_custom_alert_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../custom-alert/custom-alert.page */ 15717);












const routes = [
    {
        path: '',
        component: _neworder_page__WEBPACK_IMPORTED_MODULE_2__.NeworderPage
    }
];
let NeworderPageModule = class NeworderPageModule {
};
NeworderPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_7__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__.FormsModule, ionic_selectable__WEBPACK_IMPORTED_MODULE_9__.IonicSelectableModule, _common_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_3__.PipesModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.IonicModule, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.ReactiveFormsModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_11__.RouterModule.forChild(routes),
            _product_list_product_list_module__WEBPACK_IMPORTED_MODULE_0__.ProductListPageModule
        ],
        declarations: [_neworder_page__WEBPACK_IMPORTED_MODULE_2__.NeworderPage, _custom_alert_custom_alert_page__WEBPACK_IMPORTED_MODULE_4__.CustomAlertPage],
        entryComponents: [_custom_alert_custom_alert_page__WEBPACK_IMPORTED_MODULE_4__.CustomAlertPage, _product_list_product_list_page__WEBPACK_IMPORTED_MODULE_1__.ProductListPage],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_6__.CUSTOM_ELEMENTS_SCHEMA]
    })
], NeworderPageModule);



/***/ })

}]);
//# sourceMappingURL=src_app_neworder_neworder_module_ts.js.map