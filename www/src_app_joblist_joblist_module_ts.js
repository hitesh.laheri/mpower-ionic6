"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_joblist_joblist_module_ts"],{

/***/ 11013:
/*!*******************************************!*\
  !*** ./src/app/joblist/joblist.module.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "JoblistPageModule": () => (/* binding */ JoblistPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _joblist_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./joblist.page */ 28930);







const routes = [
    {
        path: '',
        component: _joblist_page__WEBPACK_IMPORTED_MODULE_0__.JoblistPage
    }
];
let JoblistPageModule = class JoblistPageModule {
};
JoblistPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_joblist_page__WEBPACK_IMPORTED_MODULE_0__.JoblistPage]
    })
], JoblistPageModule);



/***/ }),

/***/ 28930:
/*!*****************************************!*\
  !*** ./src/app/joblist/joblist.page.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "JoblistPage": () => (/* binding */ JoblistPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _joblist_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./joblist.page.html?ngResource */ 20732);
/* harmony import */ var _joblist_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./joblist.page.scss?ngResource */ 75891);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);








let JoblistPage = class JoblistPage {
    constructor(loginservc, alertController, datePipe, router, loadingController) {
        this.loginservc = loginservc;
        this.alertController = alertController;
        this.datePipe = datePipe;
        this.router = router;
        this.loadingController = loadingController;
    }
    ngOnInit() {
        this.loadingController.create({
            duration: 5000,
            spinner: 'circles',
            message: 'Please Wait...'
        }).then((res) => {
            res.present();
        });
        this.loginservc.getjoblist(this.loginservc.userid).subscribe(data => {
            this.response = data['response'];
            this.joblist = this.response['data'];
            this.totalrow = this.response.totalRows;
            this.loadingController.dismiss();
        });
    }
    onAccept(jobid) {
        this.loginservc.acceptJob(jobid).subscribe(data => {
            this.router.navigate(['/job', jobid]);
        });
    }
    onReject(jobid) {
        this.loginservc.rejectJob(jobid).subscribe(data => {
            this.doRefresh(null);
        });
    }
    onTransfer(jobid) {
        this.router.navigate(['/jobtransfer', jobid]);
    }
    doRefresh(event) {
        this.loginservc.getjoblist(this.loginservc.userid).subscribe(data => {
            this.response = data['response'];
            this.joblist = this.response['data'];
            this.totalrow = this.response.totalRows;
        });
        setTimeout(() => {
            if (event) {
                event.target.complete();
            }
        }, 2000);
    }
};
JoblistPage.ctorParameters = () => [
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.AlertController },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_4__.DatePipe },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.LoadingController }
];
JoblistPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-joblist',
        template: _joblist_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_joblist_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], JoblistPage);



/***/ }),

/***/ 75891:
/*!******************************************************!*\
  !*** ./src/app/joblist/joblist.page.scss?ngResource ***!
  \******************************************************/
/***/ ((module) => {

module.exports = "page-home .scroll-content {\n  margin: 0 auto !important;\n  padding: 0px !important;\n  background: #B4B8AB;\n}\npage-home .main-app-top {\n  position: relative;\n  padding: 0px !important;\n  margin: 0px;\n  height: 300px;\n  width: 100%;\n  background: #2d2d2d;\n  z-index: 0;\n}\npage-home .main-app-top:before {\n  position: absolute;\n  content: \"\";\n  z-index: 1;\n  background: #B4B8AB;\n  width: 200%;\n  height: 50%;\n  transform-origin: center;\n  bottom: 10%;\n  transform: rotate(-8deg) scale(1.5);\n}\npage-home .header-md,\npage-home .header-ios,\npage-home .header-wp {\n  background: #2d2d2d;\n}\npage-home .top-header {\n  font-size: 28px;\n  padding-left: 12px;\n  padding-right: 12px;\n  padding-top: 12px;\n  color: white;\n}\npage-home .list-md {\n  margin-top: 15%;\n  padding-bottom: 5%;\n}\npage-home .item-md,\npage-home .item-ios,\npage-home .item-wp {\n  background: transparent;\n  margin: 0;\n  z-index: 1;\n}\npage-home .item-md img {\n  z-index: 1;\n  height: 90px;\n  width: 90px;\n  object-fit: cover;\n  background-size: cover;\n  background-position: center;\n  max-height: 100px;\n  max-width: 100px;\n  border-radius: 5%;\n  border: 1px solid burlywood;\n  box-shadow: 1px 1px 1px 2px rgba(0, 0, 0, 0.5);\n}\npage-home .item-detail-container {\n  z-index: -1;\n  border-radius: 10px;\n  position: relative;\n  margin: 1rem;\n  font-family: Righteous;\n  width: 90%;\n  height: 120px;\n  max-width: 90%;\n  max-height: 120px;\n  background: white;\n  border-radius: 10px;\n  overflow: hidden;\n  margin-left: 5%;\n  margin-top: -12%;\n  text-align: start;\n  box-shadow: 2px 1px 2px 2px rgba(0, 0, 0, 0.25);\n}\npage-home .item-container h3 {\n  font-weight: 400;\n  font-size: 20px;\n  padding-top: 1rem;\n  color: black;\n}\npage-home .item-container p {\n  font-weight: lighter;\n  color: black;\n  line-height: 2em;\n  font-size: 14px;\n}\npage-home .item-container h4 {\n  color: black;\n  font-size: 16px;\n  font-weight: 300;\n}\npage-home .item-container {\n  position: relative;\n  margin-left: 10rem;\n}\npage-home .fa-1 {\n  float: left;\n}\npage-home .fa-2 {\n  float: right;\n}\npage-home .fa-star {\n  color: goldenrod;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImpvYmxpc3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0kseUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBQVI7QUFFSTtFQUNJLGtCQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7QUFBUjtBQUVJO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSx3QkFBQTtFQUNBLFdBQUE7RUFDQSxtQ0FBQTtBQUFSO0FBRUk7OztFQUdJLG1CQUFBO0FBQVI7QUFFSTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0FBQVI7QUFFSTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtBQUFSO0FBRUk7OztFQUdJLHVCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7QUFBUjtBQUVJO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLDJCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsMkJBQUE7RUFDQSw4Q0FBQTtBQUFSO0FBRUk7RUFDSSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSwrQ0FBQTtBQUFSO0FBRUk7RUFDSSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUFBUjtBQUVJO0VBQ0ksb0JBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBQVI7QUFFSTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFBUjtBQUVJO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtBQUFSO0FBRUk7RUFDSSxXQUFBO0FBQVI7QUFFSTtFQUNJLFlBQUE7QUFBUjtBQUdJO0VBQ0ksZ0JBQUE7QUFEUiIsImZpbGUiOiJqb2JsaXN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInBhZ2UtaG9tZSB7XG4gICAgLnNjcm9sbC1jb250ZW50IHtcbiAgICAgICAgbWFyZ2luOiAwIGF1dG8gIWltcG9ydGFudDtcbiAgICAgICAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIGJhY2tncm91bmQ6ICNCNEI4QUI7XG4gICAgfVxuICAgIC5tYWluLWFwcC10b3Age1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xuICAgICAgICBtYXJnaW46IDBweDtcbiAgICAgICAgaGVpZ2h0OiAzMDBweDtcbiAgICAgICAgd2lkdGg6IDEwMCU7IFxuICAgICAgICBiYWNrZ3JvdW5kOiAjMmQyZDJkO1xuICAgICAgICB6LWluZGV4OiAwO1xuICAgIH1cbiAgICAubWFpbi1hcHAtdG9wOmJlZm9yZSB7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgY29udGVudDogJyc7XG4gICAgICAgIHotaW5kZXg6IDE7XG4gICAgICAgIGJhY2tncm91bmQ6ICNCNEI4QUI7XG4gICAgICAgIHdpZHRoOiAyMDAlO1xuICAgICAgICBoZWlnaHQ6IDUwJTtcbiAgICAgICAgdHJhbnNmb3JtLW9yaWdpbjogY2VudGVyO1xuICAgICAgICBib3R0b206IDEwJTtcbiAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoLThkZWcpIHNjYWxlKDEuNSk7XG4gICAgfVxuICAgIC5oZWFkZXItbWQsXG4gICAgLmhlYWRlci1pb3MsXG4gICAgLmhlYWRlci13cCB7XG4gICAgICAgIGJhY2tncm91bmQ6ICMyZDJkMmQ7XG4gICAgfVxuICAgIC50b3AtaGVhZGVyIHtcbiAgICAgICAgZm9udC1zaXplOiAyOHB4O1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDEycHg7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDEycHg7XG4gICAgICAgIHBhZGRpbmctdG9wOiAxMnB4O1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgfVxuICAgIC5saXN0LW1kIHtcbiAgICAgICAgbWFyZ2luLXRvcDogMTUlO1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogNSU7XG4gICAgfVxuICAgIC5pdGVtLW1kLFxuICAgIC5pdGVtLWlvcyxcbiAgICAuaXRlbS13cCB7XG4gICAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIHotaW5kZXg6IDE7XG4gICAgfVxuICAgIC5pdGVtLW1kIGltZyB7XG4gICAgICAgIHotaW5kZXg6IDE7XG4gICAgICAgIGhlaWdodDogOTBweDtcbiAgICAgICAgd2lkdGg6IDkwcHg7XG4gICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgIG1heC1oZWlnaHQ6IDEwMHB4O1xuICAgICAgICBtYXgtd2lkdGg6IDEwMHB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1JTtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgYnVybHl3b29kO1xuICAgICAgICBib3gtc2hhZG93OiAxcHggMXB4IDFweCAycHggcmdiYSgwLCAwLCAwLCAwLjUpO1xuICAgIH1cbiAgICAuaXRlbS1kZXRhaWwtY29udGFpbmVyIHtcbiAgICAgICAgei1pbmRleDogLTE7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgbWFyZ2luOiAxcmVtO1xuICAgICAgICBmb250LWZhbWlseTogUmlnaHRlb3VzO1xuICAgICAgICB3aWR0aDogOTAlO1xuICAgICAgICBoZWlnaHQ6IDEyMHB4O1xuICAgICAgICBtYXgtd2lkdGg6IDkwJTtcbiAgICAgICAgbWF4LWhlaWdodDogMTIwcHg7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgICAgICBtYXJnaW4tbGVmdDogNSU7XG4gICAgICAgIG1hcmdpbi10b3A6IC0xMiU7XG4gICAgICAgIHRleHQtYWxpZ246IHN0YXJ0O1xuICAgICAgICBib3gtc2hhZG93OiAycHggMXB4IDJweCAycHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICB9XG4gICAgLml0ZW0tY29udGFpbmVyIGgzIHsgICAgICAgIFxuICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIHBhZGRpbmctdG9wOiAxcmVtO1xuICAgICAgICBjb2xvcjogYmxhY2s7XG4gICAgfVxuICAgIC5pdGVtLWNvbnRhaW5lciBwIHtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG4gICAgICAgIGNvbG9yOiBibGFjaztcbiAgICAgICAgbGluZS1oZWlnaHQ6IDJlbTtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgIH1cbiAgICAuaXRlbS1jb250YWluZXIgaDQge1xuICAgICAgICBjb2xvcjogYmxhY2s7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDMwMDtcbiAgICB9XG4gICAgLml0ZW0tY29udGFpbmVye1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHJlbTtcbiAgICB9XG4gICAgLmZhLTEge1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICB9XG4gICAgLmZhLTIge1xuICAgICAgICBmbG9hdDogcmlnaHQ7XG4gICAgfVxuXG4gICAgLmZhLXN0YXJ7XG4gICAgICAgIGNvbG9yOiBnb2xkZW5yb2Q7XG4gICAgfVxufSJdfQ== */";

/***/ }),

/***/ 20732:
/*!******************************************************!*\
  !*** ./src/app/joblist/joblist.page.html?ngResource ***!
  \******************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n      <ion-buttons slot=\"start\">\n          <ion-back-button defaultHref=\"login\"></ion-back-button>\n        </ion-buttons>\n    <ion-title>Job List</ion-title>\n    <ion-icon slot=\"end\" style=\"font-size: 35px\" name=\"refresh\" (click)=\"doRefresh(null)\"></ion-icon>\n    <ion-badge slot=\"end\" style=\"font-size: 35px\" color=\"danger\">{{totalrow}}</ion-badge>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n    <ion-list no-lines>\n        <ion-card>\n          <ion-item *ngFor=\"let job of joblist\">\n              <ion-card-content>\n                <ion-grid fix>\n                  <ion-row>\n                    <ion-col size=\"6\">\n                        <ion-label position=\"stacked\">Assign Date  </ion-label>\n                        <ion-card-subtitle>{{job.assigndate | date: 'dd/MM/yyyy'}}</ion-card-subtitle>\n                    </ion-col>\n                    <ion-col size=\"6\">\n                        <ion-label position=\"stacked\">Job Type</ion-label>\n                        <ion-card-subtitle>{{job.mwmsJobtype$_identifier}}</ion-card-subtitle>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"12\">\n                        <ion-label position=\"stacked\">Task Descriptions  </ion-label>\n                        {{job.staskdescription}}\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                   <ion-col size=\"4\">\n                        <ion-button (click)=\"onAccept(job.id)\">\n                            Accept\n                          </ion-button>\n                    </ion-col>\n                    <ion-col size=\"4\">\n                        <ion-button (click)=\"onReject(job.id)\">\n                            Reject\n                        </ion-button>\n                    </ion-col>\n                    <ion-col size=\"4\">\n                        <ion-button (click)=\"onTransfer(job.id)\">\n                            transfer\n                        </ion-button>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n                \n              </ion-card-content>\n            </ion-item>\n      </ion-card>\n    </ion-list>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_joblist_joblist_module_ts.js.map