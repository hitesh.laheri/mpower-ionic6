"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_use-vetcoins-transaction_use-vetcoins-transaction_module_ts"],{

/***/ 16329:
/*!*****************************************************************************!*\
  !*** ./src/app/use-vetcoins-transaction/use-vetcoins-transaction.module.ts ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UseVetcoinsTransactionPageModule": () => (/* binding */ UseVetcoinsTransactionPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _use_vetcoins_transaction_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./use-vetcoins-transaction.page */ 89043);







const routes = [
    {
        path: '',
        component: _use_vetcoins_transaction_page__WEBPACK_IMPORTED_MODULE_0__.UseVetcoinsTransactionPage
    }
];
let UseVetcoinsTransactionPageModule = class UseVetcoinsTransactionPageModule {
};
UseVetcoinsTransactionPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_use_vetcoins_transaction_page__WEBPACK_IMPORTED_MODULE_0__.UseVetcoinsTransactionPage]
    })
], UseVetcoinsTransactionPageModule);



/***/ }),

/***/ 89043:
/*!***************************************************************************!*\
  !*** ./src/app/use-vetcoins-transaction/use-vetcoins-transaction.page.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UseVetcoinsTransactionPage": () => (/* binding */ UseVetcoinsTransactionPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _use_vetcoins_transaction_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./use-vetcoins-transaction.page.html?ngResource */ 52099);
/* harmony import */ var _use_vetcoins_transaction_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./use-vetcoins-transaction.page.scss?ngResource */ 92250);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _use_vetcoins_use_vetcoins_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../use-vetcoins/use-vetcoins.service */ 6730);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);






let UseVetcoinsTransactionPage = class UseVetcoinsTransactionPage {
    constructor(usevetcoinsservice, commonfun) {
        this.usevetcoinsservice = usevetcoinsservice;
        this.commonfun = commonfun;
    }
    ngOnInit() {
        this.transdata();
    }
    transdata() {
        try {
            this.usevetcoinsservice.getWMobileVetCoinCustDetailsapi("", "Y").subscribe(data => {
                if (data != null) {
                    this.balamount = data["Balance"].balamount;
                    this.transactiondata = data["Transactions"];
                }
                else {
                    this.commonfun.presentAlert("Message", "Error", "Mobile No. is not valid.");
                }
            }, error => {
                this.commonfun.presentAlert("Message", "Error", error.error.text);
            });
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    Resetpage() {
    }
};
UseVetcoinsTransactionPage.ctorParameters = () => [
    { type: _use_vetcoins_use_vetcoins_service__WEBPACK_IMPORTED_MODULE_2__.UseVetcoinsService },
    { type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_3__.Commonfun }
];
UseVetcoinsTransactionPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-use-vetcoins-transaction',
        template: _use_vetcoins_transaction_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_use_vetcoins_transaction_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], UseVetcoinsTransactionPage);



/***/ }),

/***/ 92250:
/*!****************************************************************************************!*\
  !*** ./src/app/use-vetcoins-transaction/use-vetcoins-transaction.page.scss?ngResource ***!
  \****************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1c2UtdmV0Y29pbnMtdHJhbnNhY3Rpb24ucGFnZS5zY3NzIn0= */";

/***/ }),

/***/ 52099:
/*!****************************************************************************************!*\
  !*** ./src/app/use-vetcoins-transaction/use-vetcoins-transaction.page.html?ngResource ***!
  \****************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar class=\"cssion-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: white;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title style=\"font-size:large\">\n     VetCoins Balance   &#x20b9; {{balamount}}\n    </ion-title>\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n  \n\n</ion-header>\n\n\n<ion-content>\n\n \n    \n  <ion-row style=\"border-bottom: 1px solid grey !important;\">\n    <ion-col size=\"4\" style=\"text-align: center;\">Date</ion-col>\n    <ion-col size=\"4\">Descriptions</ion-col>\n    <ion-col size=\"2\">Credit</ion-col>\n    <ion-col size=\"2\">Debit</ion-col>\n  </ion-row>\n  \n\n  \n      \n<ion-row  *ngFor=\"let item of transactiondata; index as i\"   ng-class=\"{$even ? odd : even}\">\n\n  <ion-card style=\"width: 100%;\">\n    <ion-card-content size=\"12\"> \n  <!-- <ion-item style=\"font-size: small;\"> -->\n    <ion-row style=\"font-size: small;\">\n  <ion-col size=\"4\" style=\"text-align: left;\"> {{item.date | date: 'dd/MM/yyyy'}}</ion-col>\n  <ion-col size=\"4\" style=\"text-align: left;\" >{{item.descriptions}}</ion-col>\n  <ion-col size=\"2\" style=\"text-align: right;font-size: smaller;\">{{item.credit}}</ion-col>\n  <ion-col size=\"2\" style=\"text-align: right;font-size: smaller;\">{{item.debit}}</ion-col>\n<!-- </ion-item> -->\n</ion-row>\n </ion-card-content>\n</ion-card>\n</ion-row>\n \n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_use-vetcoins-transaction_use-vetcoins-transaction_module_ts.js.map