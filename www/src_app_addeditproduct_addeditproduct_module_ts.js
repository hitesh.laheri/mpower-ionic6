"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_addeditproduct_addeditproduct_module_ts"],{

/***/ 33276:
/*!*********************************************************!*\
  !*** ./src/app/addeditproduct/addeditproduct.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddeditproductPageModule": () => (/* binding */ AddeditproductPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _product_list_product_list_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../product-list/product-list.module */ 32305);
/* harmony import */ var _product_list_product_list_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../product-list/product-list.page */ 47214);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _addeditproduct_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./addeditproduct.page */ 88408);
/* harmony import */ var _product_filter_product_filter_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../product-filter/product-filter.page */ 90865);
/* harmony import */ var ng2_search_filter__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-search-filter */ 9991);









//import { NeworderPage } from '../neworder/neworder.page';



const routes = [
    {
        path: '',
        component: _addeditproduct_page__WEBPACK_IMPORTED_MODULE_2__.AddeditproductPage
    }
];
let AddeditproductPageModule = class AddeditproductPageModule {
};
AddeditproductPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_7__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__.FormsModule, ionic_selectable__WEBPACK_IMPORTED_MODULE_9__.IonicSelectableModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.IonicModule, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.ReactiveFormsModule,
            ng2_search_filter__WEBPACK_IMPORTED_MODULE_4__.Ng2SearchPipeModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_11__.RouterModule.forChild(routes),
            _product_list_product_list_module__WEBPACK_IMPORTED_MODULE_0__.ProductListPageModule
        ],
        declarations: [_addeditproduct_page__WEBPACK_IMPORTED_MODULE_2__.AddeditproductPage, _product_filter_product_filter_page__WEBPACK_IMPORTED_MODULE_3__.ProductFilterPage],
        entryComponents: [_product_filter_product_filter_page__WEBPACK_IMPORTED_MODULE_3__.ProductFilterPage, _product_list_product_list_page__WEBPACK_IMPORTED_MODULE_1__.ProductListPage],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_6__.CUSTOM_ELEMENTS_SCHEMA]
    })
], AddeditproductPageModule);



/***/ }),

/***/ 88408:
/*!*******************************************************!*\
  !*** ./src/app/addeditproduct/addeditproduct.page.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddeditproductPage": () => (/* binding */ AddeditproductPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _addeditproduct_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./addeditproduct.page.html?ngResource */ 87767);
/* harmony import */ var _addeditproduct_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./addeditproduct.page.scss?ngResource */ 86656);
/* harmony import */ var _product_list_product_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../product-list/product-list.page */ 47214);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _provider_validator_helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../provider/validator-helper */ 35096);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _addeditproduct_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./addeditproduct.service */ 53899);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _neworder_neworder_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../neworder/neworder.page */ 10990);
/* harmony import */ var _product_filter_product_filter_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../product-filter/product-filter.page */ 90865);
/* harmony import */ var _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../neworder/neworder.service */ 17216);

















let AddeditproductPage = class AddeditproductPage {
  constructor(fb, val, router, alertCtrl, loadingController, addeditproductservice, route, commonfun, neworderpage, loginauth, modalController, formBuilder, alertController, neworderservice) {
    this.fb = fb;
    this.val = val;
    this.router = router;
    this.alertCtrl = alertCtrl;
    this.loadingController = loadingController;
    this.addeditproductservice = addeditproductservice;
    this.route = route;
    this.commonfun = commonfun;
    this.neworderpage = neworderpage;
    this.loginauth = loginauth;
    this.modalController = modalController;
    this.formBuilder = formBuilder;
    this.alertController = alertController;
    this.neworderservice = neworderservice;
    this.Istempcartproduct = false;
    this.prodsearchtextcount = 0;
    this.isLoading = false;
    this.activityid = '';
    this.cust_id = '';
    this.order_type = '';
    this.businessPartnerCategory = '';
    this.product_id = '';
    this.TaxRate = ''; //Isdruglicence:boolean=false;
    //#region constructor

    this.totalFilterApplied = 0;
    this.totalCartAmount = 0;
    this.validation_messages = {
      'productQuantityCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Enter Quantity.'
      }]
    };
    this.getparam();
    this.form = this.fb.group({
      Quantity: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_11__.Validators.required],
      selectedddlproduct: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_11__.Validators.required]
    });
  }

  //#endregion constructor
  //#region ngOnInit()
  ngOnInit() {
    // this.commonfun.chkcache('neworder');
    this.portForm = this.formBuilder.group({
      productQuantityCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_11__.Validators.required]
    });
  } //#endregion ngOnInit()


  getparam() {
    try {
      this.route.params.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          // this.selectedbunch=null;
          //  console.log("Add Edit Param GET Param",this.businessPartnerCategory);
          this.cust_id = this.router.getCurrentNavigation().extras.state.cust_id;
          this.order_type = this.router.getCurrentNavigation().extras.state.order_type;
          this.businessPartnerCategory = this.router.getCurrentNavigation().extras.state.businessPartnerCategory;
          this.tempSelectedBusinessPartner = this.router.getCurrentNavigation().extras.state.tempSelectedBusinessPartner;
          this.temp_selected_primary_customer = this.router.getCurrentNavigation().extras.state.tempPrimaryCustomer; // console.log("Add Edit Param GET Param",this.temp_selected_primary_customer);

          this.activityid = this.router.getCurrentNavigation().extras.state.activityid;
          this.existingcartproduct = this.router.getCurrentNavigation().extras.state.cartproduct;
          this.tempcartproduct = this.router.getCurrentNavigation().extras.state.cartproduct; // console.log('cart product',this.tempcartproduct);

          this.calculateTotalCartAmount();
          this.selectedBPaddressbilling = this.router.getCurrentNavigation().extras.state.addressbilling;
          this.selectedBPaddressshipping = this.router.getCurrentNavigation().extras.state.addressshipping;
          this.special_order_add_edit = this.router.getCurrentNavigation().extras.state.special_order;
          this.temp_is_advance_payment = this.router.getCurrentNavigation().extras.state.is_advance_payment_param;
          this.issplorderonfreeqty = this.router.getCurrentNavigation().extras.state.issplorderonfreeqty; //console.log('this.issplorderonfreeqty',this.special_order_add_edit);

          this.iscartempty(); //------------------------------------

          var editproduct = this.router.getCurrentNavigation().extras.state.selectedproduct; // console.log('edit product',editproduct);

          if (editproduct) {
            this.selectedddlproduct = editproduct;
            this.product_id = editproduct.id;
            this.getqty(this.product_id, "Y");
            this.shipperqty = editproduct.shipperqty;
            this.minorderqty = editproduct.minorderqty;
            this.enteredfreeqty = editproduct.enteredfreeqty;
          } //this.ddlproduct[0]=this.selectedddlproduct


          this.form.controls["selectedddlproduct"].setValue(this.router.getCurrentNavigation().extras.state.selectedproduct); //----------------------------------
          // this.Isdruglicence=this.router.getCurrentNavigation().extras.state.Isdruglicence;
        }
      });
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  iscartempty() {
    if (this.tempcartproduct != null && this.tempcartproduct.length > 0) {
      this.Istempcartproduct = true;
    } else {
      this.Istempcartproduct = false;
    }
  } //#region onSave(value)


  onSave(saveoradd) {
    this.commonfun.loadingPresent();

    try {
      this.iscartempty();
      var Qty = this.form.controls['Quantity'].value;

      if (Qty != "" && Qty != 0 && Qty != null) {
        if (Qty >= this.minorderqty) {
          if (Qty % this.shipperqty == 0) {
            this.getproductdetail(Qty, saveoradd);
          } else {
            this.commonfun.presentAlert("Message", "Alert!", "Quantity must be divisible by " + this.shipperqty);
          }
        } else {
          this.commonfun.presentAlert("Message", "Alert!", "Quantity must be greater than or equal to " + this.minorderqty);
        }
      } else if (this.tempcartproduct != null && this.tempcartproduct != undefined) {
        let body = {
          "productlist": this.tempcartproduct,
          "activityid": this.activityid,
          "cust_id": this.cust_id,
          "billadd": this.selectedBPaddressbilling,
          "shippadd": this.selectedBPaddressshipping,
          "order_type": this.order_type,
          "isspecialorder": this.special_order_add_edit === 'Y' ? true : false,
          "isadvancepayment": this.neworderservice.isadvancepaymentcheck
        };
        this.neworderservice.checkForCashDiscountPopup(body).subscribe(data => {
          if (data.msg) {
            this.checkForCashDiscountPopup(data.msg);
          } else if (data.noscheme) {
            if (this.loginauth.selectedactivity.iscashdiscovertraddisc) {
              this.neworderservice.isadvancepaymentcheck = false;
            }

            let body = {
              "productlist": this.tempcartproduct,
              "activityid": this.activityid,
              "cust_id": this.cust_id,
              "billadd": this.selectedBPaddressbilling,
              "shippadd": this.selectedBPaddressshipping,
              "order_type": this.order_type,
              "isspecialorder": this.special_order_add_edit === 'Y' ? true : false,
              "isadvancepayment": this.neworderservice.isadvancepaymentcheck,
              "iscancel": true
            };
            this.neworderservice.iscancelpopup = true;
            this.neworderservice.onCancelCashDiscount(body).subscribe(data => {
              this.tempcartproduct = data;
              let navigationExtras = {
                state: {
                  selectedddlproduct: this.tempcartproduct,
                  orderType: this.order_type,
                  passTempSelectedBusinessPartner: this.tempSelectedBusinessPartner,
                  addressbilling: this.selectedBPaddressbilling,
                  addressshipping: this.selectedBPaddressshipping,
                  special_order_add_edit: this.special_order_add_edit,
                  passTempPrimaryCustomer: this.temp_selected_primary_customer,
                  passIsAdvancePayment: false
                }
              };
              this.router.navigate(['neworder'], navigationExtras);
            });
          } else {
            if (this.loginauth.selectedactivity.iscashdiscovertraddisc) {
              this.neworderservice.isadvancepaymentcheck = false;
            }

            let navigationExtras = {
              state: {
                selectedddlproduct: this.tempcartproduct,
                orderType: this.order_type,
                passTempSelectedBusinessPartner: this.tempSelectedBusinessPartner,
                addressbilling: this.selectedBPaddressbilling,
                addressshipping: this.selectedBPaddressshipping,
                special_order_add_edit: this.special_order_add_edit,
                passTempPrimaryCustomer: this.temp_selected_primary_customer,
                passIsAdvancePayment: this.neworderservice.isadvancepaymentcheck
              }
            };
            this.neworderservice.iscancelpopup = true;
            this.router.navigate(['neworder'], navigationExtras);
          }
        }, error => {
          this.commonfun.presentAlert("Message", "Error", error);
          this.commonfun.loadingDismiss();
          return;
        });
      }

      this.commonfun.loadingDismiss();
    } catch (error) {
      this.commonfun.loadingDismiss();
      this.commonfun.presentAlert("Message", "Error!", error);
    }
  } //#endregion onSave(value)


  checkForCashDiscountPopup(msg) {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const alert = yield _this.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Message',
          message: '<p>You are Eligible for Advance Payment Discount.</p><p>(Scheme Information:' + msg.schemename + ', (' + msg.cashdiscount + '%)</p><p>Click Ok to Apply Discount.</p>',
          buttons: [{
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              _this.commonfun.loadingPresent();

              if (_this.loginauth.selectedactivity.iscashdiscovertraddisc) {
                _this.neworderservice.isadvancepaymentcheck = false;
              }

              let body = {
                "productlist": _this.tempcartproduct,
                "activityid": _this.activityid,
                "cust_id": _this.cust_id,
                "billadd": _this.selectedBPaddressbilling,
                "shippadd": _this.selectedBPaddressshipping,
                "order_type": _this.order_type,
                "isspecialorder": _this.special_order_add_edit === 'Y' ? true : false,
                "isadvancepayment": _this.neworderservice.isadvancepaymentcheck,
                "iscancel": true
              };
              _this.neworderservice.iscancelpopup = true;

              _this.neworderservice.onCancelCashDiscount(body).subscribe(data => {
                _this.tempcartproduct = data;
                let navigationExtras = {
                  state: {
                    selectedddlproduct: _this.tempcartproduct,
                    orderType: _this.order_type,
                    passTempSelectedBusinessPartner: _this.tempSelectedBusinessPartner,
                    addressbilling: _this.selectedBPaddressbilling,
                    addressshipping: _this.selectedBPaddressshipping,
                    special_order_add_edit: _this.special_order_add_edit,
                    passTempPrimaryCustomer: _this.temp_selected_primary_customer,
                    passIsAdvancePayment: false
                  }
                };

                _this.router.navigate(['neworder'], navigationExtras);

                _this.commonfun.loadingDismiss();
              });
            }
          }, {
            text: 'Okay',
            handler: () => {
              _this.commonfun.loadingPresent();

              let body = {
                "productlist": _this.tempcartproduct,
                "activityid": _this.activityid,
                "cust_id": _this.cust_id,
                "billadd": _this.selectedBPaddressbilling,
                "shippadd": _this.selectedBPaddressshipping,
                "order_type": _this.order_type,
                "isspecialorder": _this.special_order_add_edit === 'Y' ? true : false,
                "isadvancepayment": true,
                "cashdiscount": msg.cashdiscount,
                "schemename": msg.schemename,
                "Schemeid": msg.Schemeid,
                "iscancel": false
              };
              _this.neworderservice.iscancelpopup = false;

              _this.neworderservice.onCancelCashDiscount(body).subscribe(data => {
                _this.tempcartproduct = data;
                _this.neworderservice.isadvancepaymentcheck = true;
                let navigationExtras = {
                  state: {
                    selectedddlproduct: _this.tempcartproduct,
                    orderType: _this.order_type,
                    passTempSelectedBusinessPartner: _this.tempSelectedBusinessPartner,
                    addressbilling: _this.selectedBPaddressbilling,
                    addressshipping: _this.selectedBPaddressshipping,
                    special_order_add_edit: _this.special_order_add_edit,
                    passTempPrimaryCustomer: _this.temp_selected_primary_customer,
                    passIsAdvancePayment: true
                  }
                };

                _this.commonfun.loadingDismiss();

                _this.router.navigate(['neworder'], navigationExtras);
              });
            }
          }]
        });
        yield alert.present();
      } catch (error) {
        _this.commonfun.loadingDismiss();

        console.log(error);
      }
    })();
  } //#region onCancel()


  onCancel() {
    this.Resetpage();
    this.router.navigate(['neworder']);
  } //#endregion


  removeProduct(post) {
    try {
      let index = this.tempcartproduct.indexOf(post);
      const result = this.tempcartproduct.filter(item => item.MainProductid != post.MainProductid);
      this.tempcartproduct = result;
      this.iscartempty();
      this.calculateTotalCartAmount();
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  addproduct() {
    try {
      if (this.tempcartproduct == undefined || this.tempcartproduct == null) {
        this.tempcartproduct = this.selectedddlproductany;
      } else if (this.tempcartproduct.length >= 0) {
        for (var c = 0; c < this.selectedddlproductany.length; c++) {
          var sameprod = this.tempcartproduct.find(e => e.MainProductid === this.selectedddlproductany[c].MainProductid);

          if (sameprod != null || sameprod != undefined) {
            this.removeProduct(sameprod);
          }
        }

        for (var b = 0; b < this.selectedddlproductany.length; b++) {
          this.tempcartproduct.push(this.selectedddlproductany[b]);
        }
      } else {}

      this.iscartempty();
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  } //#region getproductdetail(qty:string)


  getproductdetail(qty, saveoradd) {
    try {
      this.commonfun.loadingPresent();
      this.addeditproductservice.getproductdetail(this.activityid, this.cust_id, this.product_id, qty, "", this.order_type, this.selectedBPaddressshipping, this.selectedBPaddressbilling, this.special_order_add_edit, this.temp_is_advance_payment, 0).subscribe(data => {
        this.commonfun.loadingDismiss();

        if (data != null) {
          this.productdetailresponse = data; //  console.log("Product DATA",data);

          this.selectedddlproductany = data;
          this.selectedddlproductany = this.selectedddlproductany.reverse();
          this.addproduct();

          if (saveoradd == "save") {
            let navigationExtras = {
              state: {
                selectedddlproduct: this.tempcartproduct
              }
            };
            this.router.navigate(['neworder'], navigationExtras);
          } else {
            this.form.reset();
            this.shipperqty = "";
            this.minorderqty = "";
          }
        } else {
          //  this.commonfun.loadingDismiss();
          this.commonfun.presentAlert("Message", "Error!", "No product found.");
        }
      }, error => {
        this.commonfun.loadingDismiss(); //  this.commonfun.presentAlert("Message","Error!",error.statusText +" with status code :"+error.status);

        this.commonfun.presentAlert("Message", "Error!", error.error.text);
      });
    } catch (error) {
      this.commonfun.loadingDismiss();
    }
  } //#endregion getproductdetail(qty:string)
  //#region onchangeproductselect()


  onchangeproductselect(event) {
    try {
      this.shipperqty = "";
      this.minorderqty = "";
      this.form.controls['Quantity'].setValue("");

      if (event.value != undefined) {
        this.product_id = event.value.id;
        this.selectedddlproduct = event.value;
        this.getqty(this.product_id, "N");
        this.shipperqty = event.value.shipperqty;
        this.minorderqty = event.value.minorderqty;
      }

      event.component._searchText = "";
    } catch (error) {
      console.log("Error:onchangeproductselect", error);
    }
  } //#endregion onchangeproductselect()


  getqty(product_id, isedit) {
    if (this.tempcartproduct != undefined) {
      const result = this.tempcartproduct.filter(item => item.product_id == product_id && item.MainProductid == product_id);

      if (result.length > 0) {
        this.form.controls["Quantity"].setValue(result[0].MainProductQty);
        if (isedit == "N") this.commonfun.presentAlert("Message", "Alert", "This product is already added with " + result[0].MainProductQty + " quantity.");
      }
    }

    this.iscartempty();
  }

  oneditProduct(post) {
    try {
      var selectedproduct = {
        id: post.product_id,
        minorderqty: post.minorderqty,
        shipperqty: post.shipperqty,
        _identifier: post.product
      }; //------------------------------------
      //

      var editproduct = post; //this.router.getCurrentNavigation().extras.state.selectedproduct;

      this.selectedddlproduct = editproduct; //this.ddlproduct[0]=this.selectedddlproduct

      this.form.controls["selectedddlproduct"].setValue(selectedproduct);
      this.product_id = editproduct.product_id;
      this.getqty(post.product_id, "Y");
      this.shipperqty = editproduct.shipperqty;
      this.minorderqty = editproduct.minorderqty; //----------------------------------
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  onEditCartProduct(editProduct) {
    try {
      this.onShowProductListModel(true, editProduct);
    } catch (error) {
      console.log(error);
    }
  } //#region productsearchChange


  productsearchChange(event) {
    this.prodsearchtextcount++;
    var custsearchtext = event.text;

    if (custsearchtext.length % 3 == 0) {
      this.prodsearchtextcount = 0;
      this.bindproduct(custsearchtext);
    }
  } //#endregion


  toggle(varr) {}

  Resetpage() {
    this.form.reset();
    this.shipperqty = "";
    this.minorderqty = "";
    this.tempcartproduct = [];
    this.selectedAddEditFilter = [];
    this.totalCartAmount = 0;
    this.totalFilterApplied = 0;
    this.iscartempty();
    this.router.navigateByUrl('/addeditproduct');
  }

  onClose(event) {
    event.component.searchText = "";
  } //#region bindproduct(strsearch:string)  


  bindproduct(strsearch) {
    try {
      if (strsearch != "") {
        //console.log("PRavin",this.cust_id,strsearch,this.selectedBPaddressbilling,this.selectedBPaddressshipping,this.order_type);
        //  this.addeditproductservice.getproduct(strsearch,this.activityid,this.Isdruglicence).subscribe(data=>{
        this.addeditproductservice.getproductapi(this.cust_id, strsearch, this.selectedBPaddressbilling, this.selectedBPaddressshipping, this.order_type).subscribe(data => {
          //    const response= data['response'];
          var response = data;
          this.ddlproduct = response; //  console.log("Product Data",response);
          //shipperqty
          // this.commonfun.loadingDismiss();
        }, error => {
          //this.commonfun.loadingDismiss();
          this.commonfun.presentAlert("Message", "Error!", error.statusText + " with status code :" + error.status);
        });
      } else {
        this.ddlproduct = null;
      }
    } catch (error) {
      // this.commonfun.loadingDismiss();
      this.commonfun.presentAlert("Message", "Error!", error.statusText + " with status code :" + error.status);
    }
  }

  onShowFilterModel() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const modal = yield _this2.modalController.create({
          component: _product_filter_product_filter_page__WEBPACK_IMPORTED_MODULE_9__.ProductFilterPage,
          cssClass: 'my-custom-filter-class',
          backdropDismiss: false,
          componentProps: {
            selectedPreviousFilter: _this2.selectedAddEditFilter,
            business_partner: _this2.tempSelectedBusinessPartner,
            is_advance_payment_filter: _this2.temp_is_advance_payment
          }
        });
        modal.onDidDismiss().then(data => {
          //console.log("Filter Model Data",data.data);
          _this2.selectedAddEditFilter = data.data;

          if (!!data.data[0] && !!data.data[0].catvalues) {
            _this2.totalFilterApplied = 0;

            _this2.selectedAddEditFilter.forEach(ele => {
              _this2.totalFilterApplied = _this2.totalFilterApplied + ele.catvalues.length;
            });

            _this2.onShowProductListModel(false);
          } else {
            _this2.totalFilterApplied = 0;
          }
        });
        return yield modal.present();
      } catch (error) {
        console.log("Add Edit Page", error);
      }
    })();
  }

  onShowProductListModel(edit, product) {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const modal = yield _this3.modalController.create({
          component: _product_list_product_list_page__WEBPACK_IMPORTED_MODULE_3__.ProductListPage,
          cssClass: 'my-custom-class',
          backdropDismiss: false,
          componentProps: {
            editMode: edit,
            productToBeEdit: product,
            activity_id: _this3.activityid,
            cust_id: _this3.cust_id,
            bpBillingAddress: _this3.selectedBPaddressbilling,
            bpShippingAddress: _this3.selectedBPaddressshipping,
            orderType: _this3.order_type,
            tempProductSelectedOn: _this3.tempcartproduct,
            selectedFilter: _this3.selectedAddEditFilter,
            special_order_add_edit_param: _this3.special_order_add_edit,
            issplorderonfreeqty: _this3.issplorderonfreeqty
          }
        });
        modal.onDidDismiss().then(data => {
          //  console.log("Product List Model Data",data);
          if (!!data.data) {
            _this3.tempcartproduct = data.data;

            _this3.calculateTotalCartAmount();

            _this3.iscartempty();
          }
        });
        return yield modal.present();
      } catch (error) {
        console.log("Add Edit Page", error);
      }
    })();
  }

  getMoreProduct(event) {
    try {} catch (error) {
      console.log(error);
    }
  }

  bindFilterProductFromApi(strsearch, filter) {
    try {
      this.addeditproductservice.getFilterProductService(this.cust_id, strsearch, this.selectedBPaddressbilling, this.selectedBPaddressshipping, this.order_type, this.selectedAddEditFilter).subscribe(data => {
        var response = data;
        this.ddlproduct = response; //  console.log("Product Data",response);
      }, error => {
        this.commonfun.presentAlert("Message", "Error!", error.statusText + " with status code :" + error.status);
      });
    } catch (error) {
      console.log(error);
    }
  }

  clearFilter() {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const alert = yield _this4.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Confirm!',
          message: 'Do you want to clear all the filter?',
          buttons: [{
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: blah => {//  console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Okay',
            handler: () => {
              //  console.log('Confirm Okay');
              _this4.selectedAddEditFilter = [];
              _this4.totalFilterApplied = 0;
            }
          }]
        });
        yield alert.present();
      } catch (error) {
        console.log(error);
      }
    })();
  }

  calculateTotalCartAmount(tempCart) {
    try {
      if (!!this.tempcartproduct) {
        this.totalCartAmount = 0;
        this.tempcartproduct.forEach(ele => {
          this.totalCartAmount = this.totalCartAmount + Number(ele.TotalAmount);
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

};

AddeditproductPage.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_11__.FormBuilder
}, {
  type: _provider_validator_helper__WEBPACK_IMPORTED_MODULE_4__.Validator
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_12__.Router
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.AlertController
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.LoadingController
}, {
  type: _addeditproduct_service__WEBPACK_IMPORTED_MODULE_5__.AddeditproductService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_12__.ActivatedRoute
}, {
  type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_6__.Commonfun
}, {
  type: _neworder_neworder_page__WEBPACK_IMPORTED_MODULE_8__.NeworderPage
}, {
  type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_7__.LoginauthService
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.ModalController
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_11__.FormBuilder
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.AlertController
}, {
  type: _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_10__.NeworderService
}];

AddeditproductPage = (0,tslib__WEBPACK_IMPORTED_MODULE_14__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_15__.Component)({
  selector: 'app-addeditproduct',
  template: _addeditproduct_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_addeditproduct_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], AddeditproductPage);


/***/ }),

/***/ 86656:
/*!********************************************************************!*\
  !*** ./src/app/addeditproduct/addeditproduct.page.scss?ngResource ***!
  \********************************************************************/
/***/ ((module) => {

module.exports = "::ng-deep .ionic-selectable-item {\n  max-width: none !important;\n  --min-height: 20px;\n}\n\nion-input.custom-class {\n  font-size: smaller;\n}\n\nion-col ion-input {\n  border: solid 1px #757575;\n}\n\n.amc-for-two-Year {\n  background-color: #FF5252;\n  padding: 5px;\n  border-radius: 5px;\n  color: white;\n  font-weight: bold;\n}\n\nion-item {\n  max-width: -webkit-fill-available !important;\n}\n\n.save-cart {\n  background: #f39e20;\n}\n\n.rupee-font {\n  font-size: x-large;\n}\n\n.total-amount {\n  font-size: x-large;\n  font-weight: bold;\n}\n\nleft-padding {\n  padding-left: 10px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZGVkaXRwcm9kdWN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDBCQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFJSTtFQUNFLGtCQUFBO0FBRE47O0FBTUU7RUFDRyx5QkFBQTtBQUhMOztBQU9BO0VBQ0UseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUFKRjs7QUFNQTtFQUNFLDRDQUFBO0FBSEY7O0FBS0E7RUFDRSxtQkFBQTtBQUZGOztBQUlBO0VBQ0Usa0JBQUE7QUFERjs7QUFHQTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7QUFBRjs7QUFFQTtFQUNFLDZCQUFBO0FBQ0YiLCJmaWxlIjoiYWRkZWRpdHByb2R1Y3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOjpuZy1kZWVwIC5pb25pYy1zZWxlY3RhYmxlLWl0ZW0ge1xuICAgIG1heC13aWR0aDogbm9uZSAhaW1wb3J0YW50O1xuICAgIC0tbWluLWhlaWdodDogMjBweDtcbiAgICBcbiAgfVxuIFxuICBpb24taW5wdXQge1xuICAgICYuY3VzdG9tLWNsYXNzIHtcbiAgICAgIGZvbnQtc2l6ZTogc21hbGxlcjtcbiAgICB9XG59XG5cbmlvbi1jb2x7XG4gIGlvbi1pbnB1dHtcbiAgICAgYm9yZGVyOiBzb2xpZCAxcHggIzc1NzU3NTtcbiAgfVxufVxuXG4uYW1jLWZvci10d28tWWVhcntcbiAgYmFja2dyb3VuZC1jb2xvcjojRkY1MjUyO1xuICBwYWRkaW5nOiA1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXdlaWdodDpib2xkO1xufVxuaW9uLWl0ZW17XG4gIG1heC13aWR0aDotd2Via2l0LWZpbGwtYXZhaWxhYmxlICFpbXBvcnRhbnRcbn1cbi5zYXZlLWNhcnR7XG4gIGJhY2tncm91bmQ6I2YzOWUyMDtcbn1cbi5ydXBlZS1mb250e1xuICBmb250LXNpemU6IHgtbGFyZ2U7XG59XG4udG90YWwtYW1vdW50e1xuICBmb250LXNpemU6IHgtbGFyZ2U7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxubGVmdC1wYWRkaW5ne1xuICBwYWRkaW5nLWxlZnQ6IDEwcHggIWltcG9ydGFudDtcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA3NjhweCkgYW5kIChtaW4taGVpZ2h0OiA3NjhweCl7XG4gIFxufVxuLy8gOjpuZy1kZWVwIC5zYy1pb24tbW9kYWwtaW9zLWgge1xuLy8gICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDc2OHB4KSBhbmQgKG1pbi1oZWlnaHQ6IDc2OHB4KXtcbi8vICAgICAtLXdpZHRoOiA2NzBweCAhaW1wb3J0YW50O1xuLy8gICAgIC0taGVpZ2h0OiA2NzBweCAhaW1wb3J0YW50O1xuLy8gICB9XG4gIFxuLy8gfVxuLm15LWN1c3RvbS1maWx0ZXItY2xhc3MgLm1vZGFsLXdyYXBwZXJ7XG4gIFxufSJdfQ== */";

/***/ }),

/***/ 87767:
/*!********************************************************************!*\
  !*** ./src/app/addeditproduct/addeditproduct.page.html?ngResource ***!
  \********************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Select Product</ion-title>\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <form [formGroup]=\"form\" (ngSubmit)=\"onSave(form.value)\">\n    <!-- <ion-card> -->\n    <ion-row>\n      <ion-col no-margin no-padding>\n        <ion-item (click)=\"onShowFilterModel()\" text-center no-padding no-margin\n          style=\"border-bottom-style:groove;border-right-style:groove\">\n          <!-- <ion-icon slot=\"start\" name=\"funnel\" color=\"primary\" ></ion-icon> -->\n          <ion-label style=\"margin-left:10px\">Filter</ion-label>\n        </ion-item>\n      </ion-col>\n      <ion-col no-margin no-padding>\n        <ion-item (click)=\"clearFilter()\" text-center no-padding no-margin style=\"border-bottom-style:groove\"\n          [disabled]=\"totalFilterApplied==0\">\n          <ion-label>Clear Filter <span class=\"amc-for-two-Year\">{{totalFilterApplied}}</span></ion-label>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <!-- </ion-card> -->\n    <ion-list>\n      <ion-card>\n        <ion-row>\n          <ion-col>\n            <ion-item (click)=\"onShowProductListModel(false)\">\n              <ion-label>Select Product</ion-label>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-list>\n    <ion-list >\n      <ion-card *ngFor=\"let item of tempcartproduct; index as i\">\n          <ion-row class=\"left-padding\" style=\"padding-left: 15px; padding-top: 15px;\">\n            <ion-label style=\"white-space: normal;font-weight: bold;\" >\n              <ion-icon *ngIf=\"!item.MainProductQty\" style=\"color: springgreen;\" name=\"pricetags\"></ion-icon>\n              {{ item.product }}\n            </ion-label>\n          </ion-row>\n          <ion-row style=\"padding-left: 10px;\">\n            <ion-col>\n              <ion-label color=\"green\" style=\"font-size: small;font-weight: bold;\" *ngIf=\"item.MainProductQty\">Qty: {{\n                item.MainProductQty }}</ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row style=\"padding-left: 10px;\">\n            <ion-col>\n              <ion-label color=\"green\" style=\"font-size: small;font-weight: bold;\" *ngIf=\"item.freeqty\">Free Qty: {{ item.freeqty }}\n              </ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row style=\"padding-left: 10px;\">\n            <ion-col>\n              <ion-label color=\"green\" style=\"font-size: small;font-weight: bold;\" *ngIf=\"!!item.enteredfreeqty && item.enteredfreeqty!=='0' && item.enteredfreeqty!==''\">Entered Free Qty: {{ item.enteredfreeqty }}\n              </ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row *ngIf=\"item.MainProductQty\" no-padding style=\"border-top-style:ridge;margin-top:10px\">\n            <ion-col no-padding style=\"border-right-style:ridge\">\n              <ion-button (click)=\"onEditCartProduct(item)\" expand=\"full\" \n              style=\"--background: white;color: black;margin-inline:0px;--background-activated:whitesmoke;--background-hover:white;\">Edit\n                <ion-icon slot=\"start\" name=\"create\"></ion-icon>\n              </ion-button>\n            </ion-col>\n            <ion-col no-padding>\n              <ion-button (click)=\"removeProduct(item)\" expand=\"full\" style=\"--background: white;color: black;margin-inline:0px;--background-activated:whitesmoke;--background-hover:white\">Remove\n                <ion-icon slot=\"start\" name=\"trash\"></ion-icon>\n              </ion-button>\n            </ion-col>\n          </ion-row>\n          \n      </ion-card>\n    </ion-list>\n    \n  </form>\n</ion-content>\n<ion-footer>\n  <ion-card no-margin>\n    <ion-row>\n      <ion-col>\n        <!-- <ion-item>\n          <ion-icon color=\"primary\" src=\"./assets/rupee-indian.svg\" class=\"rupee-font\"></ion-icon>\n          <ion-label class=\"total-amount\">{{ totalCartAmount | number:'1.2-2' }}</ion-label>\n        </ion-item> -->\n      </ion-col>\n      <ion-col>\n        <ion-button [disabled]=\"!form.valid && !Istempcartproduct\" (click)=\"onSave('save')\" color=\"primary\"\n          class=\"save-cart\" expand=\"full\" fill=\"outline\">\n          <ion-label style=\"color: white;\">\n            Save\n          </ion-label>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n</ion-footer>";

/***/ })

}]);
//# sourceMappingURL=src_app_addeditproduct_addeditproduct_module_ts.js.map