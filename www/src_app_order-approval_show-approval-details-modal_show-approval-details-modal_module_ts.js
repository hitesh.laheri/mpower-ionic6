"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_order-approval_show-approval-details-modal_show-approval-details-modal_module_ts"],{

/***/ 14756:
/*!**************************************************************************************************!*\
  !*** ./src/app/order-approval/show-approval-details-modal/show-approval-details-modal.module.ts ***!
  \**************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ShowApprovalDetailsModalPageModule": () => (/* binding */ ShowApprovalDetailsModalPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _show_approval_details_modal_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./show-approval-details-modal.page */ 19592);
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @swimlane/ngx-datatable */ 93143);








const routes = [
    {
        path: '',
        component: _show_approval_details_modal_page__WEBPACK_IMPORTED_MODULE_0__.ShowApprovalDetailsModalPage
    }
];
let ShowApprovalDetailsModalPageModule = class ShowApprovalDetailsModalPageModule {
};
ShowApprovalDetailsModalPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__.NgxDatatableModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterModule.forChild(routes)
        ],
        //declarations: [ShowApprovalDetailsModalPage]
    })
], ShowApprovalDetailsModalPageModule);



/***/ })

}]);
//# sourceMappingURL=src_app_order-approval_show-approval-details-modal_show-approval-details-modal_module_ts.js.map