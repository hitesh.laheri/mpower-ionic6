"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_arvisitschedule_sectionview_sectionview_module_ts"],{

/***/ 42241:
/*!***************************************************************************!*\
  !*** ./src/app/arvisitschedule/sectionview/sectionview-routing.module.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SectionviewPageRoutingModule": () => (/* binding */ SectionviewPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _sectionview_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./sectionview.page */ 90180);




const routes = [
    {
        path: '',
        component: _sectionview_page__WEBPACK_IMPORTED_MODULE_0__.SectionviewPage
    }
];
let SectionviewPageRoutingModule = class SectionviewPageRoutingModule {
};
SectionviewPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SectionviewPageRoutingModule);



/***/ }),

/***/ 82651:
/*!*******************************************************************!*\
  !*** ./src/app/arvisitschedule/sectionview/sectionview.module.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SectionviewPageModule": () => (/* binding */ SectionviewPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _sectionview_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./sectionview-routing.module */ 42241);
/* harmony import */ var _sectionview_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sectionview.page */ 90180);
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/components.module */ 45642);








let SectionviewPageModule = class SectionviewPageModule {
};
SectionviewPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _sectionview_routing_module__WEBPACK_IMPORTED_MODULE_0__.SectionviewPageRoutingModule,
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__.ComponentsModule
        ],
        declarations: [_sectionview_page__WEBPACK_IMPORTED_MODULE_1__.SectionviewPage]
    })
], SectionviewPageModule);



/***/ }),

/***/ 90180:
/*!*****************************************************************!*\
  !*** ./src/app/arvisitschedule/sectionview/sectionview.page.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SectionviewPage": () => (/* binding */ SectionviewPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _sectionview_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./sectionview.page.html?ngResource */ 29355);
/* harmony import */ var _sectionview_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sectionview.page.scss?ngResource */ 86328);
/* harmony import */ var _arvisitschedule_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../arvisitschedule.service */ 48598);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var src_provider_message_helper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/provider/message-helper */ 98792);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/provider/commonfun */ 51156);








let SectionviewPage = class SectionviewPage {
    constructor(checklistservice, router, msg, commonfn) {
        this.checklistservice = checklistservice;
        this.router = router;
        this.msg = msg;
        this.commonfn = commonfn;
        this.sectionlist = [];
    }
    ngOnInit() {
        for (let section of this.checklistservice.selectedinspection.sections) {
            let secjson = {};
            var questionlist = [];
            for (let que of section.questions) {
                let quejson = {};
                if (que.ans !== '' && que.isload) {
                    if (que.type === 'IMAGE' || que.type === 'IMAGEC') {
                        quejson["queno"] = que.queno;
                        //console.log('queansImage',que.ans);
                        let fileField = que.ans;
                        let imageFile;
                        for (let item of fileField.queue) {
                            imageFile = item;
                        }
                        //       console.log('queansImage',imageFile);
                        quejson["ans"] = imageFile;
                        quejson["question"] = que.question;
                        quejson["colorbackground"] = que.colorbackground;
                        quejson["forecolor"] = que.forecolor;
                        quejson["type"] = que.type;
                    }
                    else if (que.type === 'DTIME') {
                        quejson["queno"] = que.queno;
                        let datetime = que.ans.split('T');
                        let sdate = datetime[0];
                        let stime = datetime[1].substring(0, 8);
                        quejson["ans"] = (sdate + ' ' + stime);
                        quejson["question"] = que.question;
                        quejson["colorbackground"] = que.colorbackground;
                        quejson["forecolor"] = que.forecolor;
                        quejson["type"] = que.type;
                    }
                    else {
                        quejson["queno"] = que.queno;
                        quejson["ans"] = que.ans;
                        quejson["question"] = que.question;
                        quejson["colorbackground"] = que.colorbackground;
                        quejson["forecolor"] = que.forecolor;
                        quejson["type"] = que.type;
                    }
                    questionlist.push(quejson);
                }
            }
            secjson["sectionname"] = section.sectionname;
            secjson["questions"] = questionlist;
            this.sectionlist.push(secjson);
        }
    }
    backPress(event) {
        this.router.navigateByUrl('/deshboard');
    }
    submitForm(event) {
        this.commonfn.loadingPresent();
        var body = {};
        var questionlist = [];
        let formData = new FormData();
        for (let section of this.checklistservice.selectedinspection.sections) {
            //console.log('section',section);
            for (let que of section.questions) {
                let quesjson = {};
                if (que.ans !== '' && que.isload) {
                    if (que.type === 'IMAGE' || que.type === 'IMAGEC') {
                        quesjson["questionid"] = que.questionid;
                        quesjson["sectionid"] = section.sectionid;
                        quesjson["colorid"] = que.colorid;
                        quesjson["type"] = que.type;
                        let fileField = que.ans;
                        let files = fileField.queue;
                        let i = 0;
                        files.forEach((fileItem) => {
                            var ext = fileItem.file.name.substr(fileItem.file.name.lastIndexOf('.') + 1);
                            var filename = que.questionid + '.' + ext;
                            formData.append(que.questionid, fileItem.file.rawFile, filename);
                            i++;
                        });
                        quesjson["ans"] = '';
                    }
                    else if (que.type === 'DTIME') {
                        quesjson["questionid"] = que.questionid;
                        let datetime = que.ans.split('T');
                        let sdate = datetime[0];
                        let stime = datetime[1].substring(0, 8);
                        quesjson["ans"] = (sdate + ' ' + stime);
                        quesjson["sectionid"] = section.sectionid;
                        quesjson["colorid"] = que.colorid;
                        quesjson["type"] = que.type;
                    }
                    else {
                        quesjson["questionid"] = que.questionid;
                        quesjson["ans"] = que.ans;
                        quesjson["sectionid"] = section.sectionid;
                        quesjson["colorid"] = que.colorid;
                        quesjson["type"] = que.type;
                    }
                    questionlist.push(quesjson);
                }
            }
        }
        body["questionlist"] = questionlist;
        body["orgid"] = '0';
        body["arplanid"] = this.checklistservice.visitplanforchecklist.arplanid;
        body["checklisttrxid"] = this.checklistservice.visitplanforchecklist.checklisttrxid;
        body["inspectionid"] = this.checklistservice.selectedinspection.inspectionid;
        formData.append('jsonpara', JSON.stringify(body));
        this.checklistservice.onSaveSectionQuestion(formData).subscribe(data => {
            this.commonfn.loadingDismiss();
            var response = data['response'];
            if (response.hasOwnProperty('messageType')) {
                if (response.messageType == 'success') {
                    this.msg.presentAlert("Message", "Success", "Data saved successfully.");
                    this.router.navigateByUrl('/arvisitschedule');
                }
            }
            if (response.error)
                this.msg.presentAlert("Message", "Error", response.error.message);
        });
    }
};
SectionviewPage.ctorParameters = () => [
    { type: _arvisitschedule_service__WEBPACK_IMPORTED_MODULE_2__.ArvisitscheduleService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: src_provider_message_helper__WEBPACK_IMPORTED_MODULE_3__.Message },
    { type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__.Commonfun }
];
SectionviewPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-sectionview',
        template: _sectionview_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_sectionview_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], SectionviewPage);



/***/ }),

/***/ 86328:
/*!******************************************************************************!*\
  !*** ./src/app/arvisitschedule/sectionview/sectionview.page.scss?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

module.exports = ".img-container {\n  position: relative;\n}\n\n.topright {\n  position: absolute;\n  top: -8px;\n  right: 25px;\n  font-size: 18px;\n  color: red;\n}\n\nimg {\n  width: 100%;\n  height: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlY3Rpb252aWV3LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUU7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7QUFDSjs7QUFFRTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FBQ0oiLCJmaWxlIjoic2VjdGlvbnZpZXcucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmltZy1jb250YWluZXIge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgfVxuICBcbiAgLnRvcHJpZ2h0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAtOHB4O1xuICAgIHJpZ2h0OiAyNXB4O1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBjb2xvcjogcmVkO1xuICB9XG4gIFxuICBpbWcgeyBcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgXG4gIH0iXX0= */";

/***/ }),

/***/ 29355:
/*!******************************************************************************!*\
  !*** ./src/app/arvisitschedule/sectionview/sectionview.page.html?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Submission Details</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-icon (click)=\"submitForm($event)\" style=\"font-size: 40px;\" name=\"save\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n  <ion-card>\n    <ion-row>\n      <ion-col size=\"12\">\n        <div style=\"background-color: antiquewhite;\">\n          {{checklistservice.visitplanforchecklist.dateofvisit}} - {{checklistservice.visitplanforchecklist.bpartnername}} \n      </div>\n      </ion-col>\n    </ion-row>\n    \n  </ion-card>\n</ion-header>\n\n<ion-content>\n  <div style=\"width: 100%;padding-top: 10px;padding-bottom: 10px; padding-right: 10px; padding-left: 10px;\">\n  <!-- <ion-row>\n    <ion-label position=\"stacked\">\n      Branch: {{checklistservice.selectedorg.organization$_identifier}}\n    </ion-label>\n  </ion-row> -->\n  <ion-row>\n    <ion-label position=\"stacked\">\n      Inspection Name: {{checklistservice.selectedinspection.inspectionname}}\n    </ion-label>\n  </ion-row>\n  <ion-row>\n    <ion-label position=\"stacked\">\n      Inspection ID: {{checklistservice.selectedinspection.inspectionno}}\n    </ion-label>\n  </ion-row>\n</div>\n  <div style=\"width: 100%;padding-top: 10px;padding-bottom: 10px; padding-right: 10px; padding-left: 10px;\"*ngFor=\"let section of sectionlist\">\n    <ion-card-header color=\"primary\">\n      <ion-card-title>{{section.sectionname}}</ion-card-title>\n    </ion-card-header>\n    <div style=\"width: 100%;padding-top: 10px;padding-left: 10px;\" *ngFor=\"let question of section.questions\">\n <ion-row>\n   <ion-col>\n    \n    <ion-label position=\"stacked\">\n      {{question.queno}}.{{question.question}}\n    </ion-label>\n   \n  </ion-col>\n  </ion-row>\n  <ion-row *ngIf=\"question.type!=='IMAGE' && question.type!=='IMAGEC' && question.type!=='DATE'\">\n    <ion-col>\n      <div [ngStyle]=\"{'background-color': question.colorbackground,'color':question.forecolor}\">\n    <ion-text >\n      {{question.ans}}\n    </ion-text>\n    </div>\n    </ion-col>\n  </ion-row>\n  <ion-row *ngIf=\"question.type==='DATE'\">\n    <ion-col>\n      <div [ngStyle]=\"{'background-color': question.colorbackground,'color':question.forecolor}\">\n    <ion-text >\n      {{question.ans | date: 'dd-MM-yyyy'}}\n    </ion-text>\n    </div>\n    </ion-col>\n  </ion-row>\n<ion-row *ngIf=\"question.type==='IMAGE' || question.type==='IMAGEC'\">\n    <ion-col>\n      <div [ngStyle]=\"{'background-color': question.colorbackground,'color':question.forecolor}\">\n        <img style=\"height: 50px;width: 50px;\" *ngIf=\"question.ans?.file?.type!='application/pdf'\" [src]=\"\" [ImagePreviewDirective] =\"question.ans.file\" />\n    </div>\n    </ion-col>\n  </ion-row>\n  </div>\n  </div>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_arvisitschedule_sectionview_sectionview_module_ts.js.map