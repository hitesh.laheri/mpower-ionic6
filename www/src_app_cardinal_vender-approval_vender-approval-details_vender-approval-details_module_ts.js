"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_cardinal_vender-approval_vender-approval-details_vender-approval-details_module_ts"],{

/***/ 28684:
/*!****************************************************************************************************!*\
  !*** ./src/app/cardinal/vender-approval/vender-approval-details/vender-approval-details.module.ts ***!
  \****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "VenderApprovalDetailsPageModule": () => (/* binding */ VenderApprovalDetailsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _vender_approval_details_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./vender-approval-details.page */ 94185);
/* harmony import */ var src_app_material_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/material.module */ 63806);
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/checkbox */ 44792);









const routes = [
    {
        path: '',
        component: _vender_approval_details_page__WEBPACK_IMPORTED_MODULE_0__.VenderApprovalDetailsPage
    }
];
let VenderApprovalDetailsPageModule = class VenderApprovalDetailsPageModule {
};
VenderApprovalDetailsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            src_app_material_module__WEBPACK_IMPORTED_MODULE_1__.MaterialModule,
            _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_7__.MatCheckboxModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterModule.forChild(routes)
        ],
        declarations: [_vender_approval_details_page__WEBPACK_IMPORTED_MODULE_0__.VenderApprovalDetailsPage]
    })
], VenderApprovalDetailsPageModule);



/***/ }),

/***/ 94185:
/*!**************************************************************************************************!*\
  !*** ./src/app/cardinal/vender-approval/vender-approval-details/vender-approval-details.page.ts ***!
  \**************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "VenderApprovalDetailsPage": () => (/* binding */ VenderApprovalDetailsPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _vender_approval_details_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./vender-approval-details.page.html?ngResource */ 45372);
/* harmony import */ var _vender_approval_details_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./vender-approval-details.page.scss?ngResource */ 87785);
/* harmony import */ var _vender_approval_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../vender-approval.service */ 35217);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var src_provider_validator_helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/provider/validator-helper */ 35096);
/* harmony import */ var src_assets_model_complain__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/assets/model/complain */ 6337);
/* harmony import */ var _customer_service_customer_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../customer-service/customer-service.service */ 59813);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! date-fns */ 86527);














let VenderApprovalDetailsPage = class VenderApprovalDetailsPage {
  constructor(formBuilder, validator, route, router, venderApprovalService, customerService, alertCtrl, commonFunction) {
    this.formBuilder = formBuilder;
    this.validator = validator;
    this.route = route;
    this.router = router;
    this.venderApprovalService = venderApprovalService;
    this.customerService = customerService;
    this.alertCtrl = alertCtrl;
    this.commonFunction = commonFunction;
    this.TAG = "VenderApprovalDetailsPage";
    this.now = new Date();
    this.year = this.now.getFullYear();
    this.month = this.now.getMonth() + 1;
    this.day = this.now.getDate();
    this.datevisit = '';
    this.maxDate = this.year + "-" + this.month + "-" + this.day;
    this.isLinear = false;
    /**
     *
     */

    this.detailsStepValid = false;
    this.equimentStepValid = false;
    this.customerDetailStepValid = false;
    this.validation_messages = {
      'nameOfComplainer': [{
        type: 'required',
        message: '*Please Enter Name of complainer.'
      }],
      'designation_of_complainer_mss': [{
        type: 'required',
        message: '*Please Enter designation_of_complainer.'
      }],
      'mobileno': [{
        type: 'required',
        message: '*Please Enter Contact No.'
      }, {
        type: 'InvalidNumber',
        message: '*Please Enter Valid Contact No.'
      }],
      'email': [{
        type: 'required',
        message: '*Please Enter Email.'
      }, {
        type: 'InvalidEmail',
        message: '*Please Enter Valid Email.'
      }],
      'complaintDescription': [{
        type: 'required',
        message: '*Please Enter Complaint Description'
      }],
      'serialNo': [{
        type: 'required',
        message: '*Please Enter Serial no .'
      }],
      'customerAddress1': [{
        type: 'required',
        message: '*Please Enter Customer Address1'
      }],
      'customerAddress2': [{
        type: 'required',
        message: '*Please Enter Customer Customer Address 2'
      }],
      'customerAddress3': [{
        type: 'required',
        message: '*Please Enter Customer Customer Address 3'
      }],
      'pinCode': [{
        type: 'required',
        message: '*Please Enter Pin Code'
      }],
      'area': [{
        type: 'required',
        message: '*Please Select Your Area'
      }],
      'dateOfVisitCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Date Of Visit'
      }],
      'serviceVendorRemarksCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Enter Service Vendor Remark'
      }]
    };
    this.isProductCompliantTab = false;
    this.isConsumablesTab = false;
    this.isEquimentTab = false;
  }

  ngOnInit() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.route.queryParams.subscribe(params => {
        if (params && params.special) {
          _this.service = JSON.parse(params.special); // console.log(this.TAG,JSON.parse(params.special));
        }
      });

      _this.detailFormGroup = _this.formBuilder.group({
        nameOfComplainerCtrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        desigOfComplainerCtrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        mobilenoCtrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required, _this.validator.numberValid],
        emailIDCtrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required, _this.validator.emailValid],
        complaintDescriptionCtrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required]
      });
      _this.productCompliantFormGroup = _this.formBuilder.group({
        lotNoCtrl: []
      });
      _this.consumablesFormGroup = _this.formBuilder.group({
        lotNoConsumablesCtrl: []
      });
      _this.equimentFormGroup = _this.formBuilder.group({
        serialNoCtrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        dealerNameCtrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        installationDateCtrl: [] // newDealerNameCtrl:[]

      });
      _this.skuDetailsFormGroup = _this.formBuilder.group({});
      _this.customerDetailFormGroup = _this.formBuilder.group({
        customerAddress1Ctrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        customerAddress2Ctrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        customerAddress3Ctrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        pinCodeCtrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        areaCtrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        cityCtrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        stateCtrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        countryCtrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        serviceEngMobilenoCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required, _this.validator.numberValid],
        serviceVendorRemarksCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        dateOfVisitCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        approveComplaintCtrl: [,],
        rejectComplaintCtrl: [,],
        serviceEngineerNameCtrl: [,]
      });
      _this.serviceEngList = yield _this.venderApprovalService.getServiceEngList().toPromise(); // console.log(this.TAG,"Service Eng List",this.serviceEngList);
    })();
  }

  formatDate(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_9__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_10__["default"])(value), 'dd-MM-yyyy');
  }

  ionViewWillEnter() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this2.commonFunction.loadingPresent(); // console.log("Min Date",this.maxDate);


      if (!!_this2.service.newdealername) {
        _this2.service.dealername = _this2.service.newdealername;
      }
      /**
       *
       */
      // console.log("serveice",this.service);


      if (_this2.service.doctype[0].name == 'Product Compliant') {
        _this2.isProductCompliantTab = true;
        _this2.isConsumablesTab = false;
        _this2.isEquimentTab = false;

        _this2.productCompliantFormGroup.controls.lotNoCtrl.patchValue(_this2.service.lotno);
      } else if (_this2.service.doctype[0].name == 'Consumables') {
        _this2.isProductCompliantTab = false;
        _this2.isConsumablesTab = true;
        _this2.isEquimentTab = false;

        _this2.consumablesFormGroup.controls.lotNoCtrl.patchValue(_this2.service.lotno);
      } else if (_this2.service.doctype[0].name == 'Equipment') {
        _this2.isProductCompliantTab = false;
        _this2.isConsumablesTab = false;
        _this2.isEquimentTab = true;
      }

      if (_this2.service.producttobereturn == "MSNR_Y") {
        _this2.productToBeReturn = 'Yes';
      } else if (_this2.service.producttobereturn == "MSNR_N") {
        _this2.productToBeReturn = 'No';
      }

      _this2.contractTypeList = yield (yield _this2.customerService.getContractTypeList()).toPromise(); // console.log("contractTypeList",this.contractTypeList);

      for (let i = 0; i < _this2.contractTypeList.length; i++) {
        // console.log("contractTypeList of i=",i);
        if (_this2.contractTypeList[i].code == _this2.service.contracttype) {
          _this2.contracttype = _this2.contractTypeList[i].name;
        }
      }

      _this2.customerDetailFormGroup.controls.areaCtrl.patchValue(_this2.service.area[0].name);

      _this2.customerDetailFormGroup.controls.cityCtrl.patchValue(_this2.service.city[0].name);

      _this2.customerDetailFormGroup.controls.stateCtrl.patchValue(_this2.service.state[0].name);

      _this2.customerDetailFormGroup.controls.countryCtrl.patchValue(_this2.service.country[0].name);

      _this2.commonFunction.loadingDismiss();
    })();
  }

  refreshPage() {
    try {} catch (error) {//   console.error(this.TAG,error);
    }
  }

  matDetailSettper(event) {
    try {} catch (error) {//  console.error(this.TAG,error);
    }
  }

  matSettperProductCompliant(event) {
    try {} catch (error) {// console.error(this.TAG,error);
    }
  }

  matSettperConsumables(event) {
    try {} catch (error) {//  console.error(this.TAG,error);
    }
  }

  matSettperSKUDetails(event) {
    try {} catch (error) {//  console.error(this.TAG,error);
    }
  }

  matSettperEquiment(event) {
    try {} catch (error) {//  console.log(this.TAG,error);
    }
  }

  serviceEngSelectedChange(serviceEngSelected) {
    try {
      this.customerDetailFormGroup.controls.serviceEngMobilenoCtrl.patchValue(serviceEngSelected.phone);
      this.customerDetailFormGroup.get('serviceEngMobilenoCtrl').disable();
    } catch (error) {
      console.error(this.TAG, error);
    }
  }

  approveServiceCheckbox(event) {
    try {
      console.log(this.TAG, "CheckBox Value", event);
      this.detailFormGroup.patchValue({
        rejectComplaintCtrl: false
      });
    } catch (error) {
      console.error(this.TAG, error);
    }
  }

  rejectServiceCheckbox() {
    try {} catch (error) {
      console.error(this.TAG, error);
    }
  }

  presentAlert(Header, SubHeader, Message) {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this3.alertCtrl.create({
        header: Header,
        subHeader: SubHeader,
        message: Message,
        buttons: [{
          text: "OK",
          handler: ok => {
            _this3.router.navigateByUrl('/vender-approval');
          }
        }]
      });
      alert.dismiss(() => console.log('The alert has been closed.'));
      yield alert.present();
    })();
  }

  reject(form) {
    try {//  console.log("Submiited from",form);
    } catch (error) {// console.log(error);
    }
  }

  submit(event, status) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let approveVenderComplain = new src_assets_model_complain__WEBPACK_IMPORTED_MODULE_5__.Complain();

        if (status == 'Accept') {
          approveVenderComplain.Appect = 'true';
          approveVenderComplain.reject = '';
        } else {
          approveVenderComplain.Appect = '';
          approveVenderComplain.reject = 'true';
        }

        approveVenderComplain.complaint_no = _this4.service.complaintno;
        approveVenderComplain.complaintid = _this4.service.complaintid;
        approveVenderComplain.complaint_date = _this4.service.complaintdate;
        approveVenderComplain.doctype = _this4.service.doctype[0].id;
        approveVenderComplain.nameofcomplainer = _this4.service.nameofcomplainer;
        approveVenderComplain.desofcomplnr = _this4.service.desofcomplnr[0].id;
        approveVenderComplain.contnumber = _this4.service.contnumber;
        approveVenderComplain.email = _this4.service.email;
        approveVenderComplain.eventdate = _this4.service.eventdate;
        approveVenderComplain.business = _this4.service.business;
        approveVenderComplain.description = _this4.service.description;
        approveVenderComplain.lotno = _this4.service.lotno;
        approveVenderComplain.serialno = _this4.service.srnoequipment;
        approveVenderComplain.srnoequipment = _this4.service.srnoequipment;
        approveVenderComplain.contracttype = _this4.service.contracttype;
        approveVenderComplain.invoiceno = _this4.service.invoiceno;
        approveVenderComplain.invoicedate = _this4.service.invoicedate;
        approveVenderComplain.errorcode = ""; // approveVenderComplain.dealername = this.service.dealername;
        // approveVenderComplain.newdealername = this.service.newdealername;
        // approveVenderComplain.salesrepresentative = this.service.salesrepresentative;

        approveVenderComplain.installationdate = _this4.service.installationdate;
        approveVenderComplain.skucode = _this4.service.skucode;
        approveVenderComplain.skuname = _this4.service.skuname;
        approveVenderComplain.brandname = _this4.service.brandname;
        approveVenderComplain.producttobereturn = _this4.service.producttobereturn;
        approveVenderComplain.custname = _this4.service.custname;
        approveVenderComplain.add1 = _this4.service.add1;
        approveVenderComplain.add2 = _this4.service.add2;
        approveVenderComplain.add3 = _this4.service.add3;
        approveVenderComplain.pincode = _this4.service.pincode;
        approveVenderComplain.area = _this4.service.area[0].id;
        approveVenderComplain.city = _this4.service.city[0].id;
        approveVenderComplain.state = _this4.service.state[0].id;
        approveVenderComplain.country = _this4.service.country[0].id;
        approveVenderComplain.serviceengname = _this4.customerDetailFormGroup.get('serviceEngineerNameCtrl').value ? _this4.customerDetailFormGroup.get('serviceEngineerNameCtrl').value.id : '';
        approveVenderComplain.serviceengcontact = _this4.customerDetailFormGroup.get('serviceEngMobilenoCtrl').value ? _this4.customerDetailFormGroup.get('serviceEngMobilenoCtrl').value : '';
        approveVenderComplain.visitdate = _this4.customerDetailFormGroup.get('dateOfVisitCtrl').value ? _this4.customerDetailFormGroup.get('dateOfVisitCtrl').value : '';
        approveVenderComplain.servicevendorremark = _this4.customerDetailFormGroup.get('serviceVendorRemarksCtrl').value ? _this4.customerDetailFormGroup.get('serviceVendorRemarksCtrl').value : ''; //approveVenderComplain.assigntoservvendor = "true";

        approveVenderComplain.appcomplaint = "true"; // console.log(this.TAG,"Final Vender Approval Form",approveVenderComplain);

        _this4.saveComplainResponse = yield _this4.customerService.saveComplain(approveVenderComplain).toPromise(); // console.log(this.TAG,"Response From Save Complain",this.saveComplainResponse);

        if (_this4.saveComplainResponse) {
          _this4.presentAlert("Compliant Acceptance Details", "", _this4.saveComplainResponse.msg);
        } else {
          _this4.presentAlert("Compliant Acceptance Details", "", "Something went wrong please try again" + _this4.saveComplainResponse.msg);
        }
      } catch (error) {
        //  console.error(this.TAG,error);
        _this4.presentAlert("Compliant Acceptance Details", "", error.error);
      }
    })();
  }

};

VenderApprovalDetailsPage.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_8__.FormBuilder
}, {
  type: src_provider_validator_helper__WEBPACK_IMPORTED_MODULE_4__.Validator
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_11__.ActivatedRoute
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_11__.Router
}, {
  type: _vender_approval_service__WEBPACK_IMPORTED_MODULE_3__.VenderApprovalService
}, {
  type: _customer_service_customer_service_service__WEBPACK_IMPORTED_MODULE_6__.CustomerServiceService
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.AlertController
}, {
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_7__.Commonfun
}];

VenderApprovalDetailsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_13__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_14__.Component)({
  selector: 'app-vender-approval-details',
  template: _vender_approval_details_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_vender_approval_details_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], VenderApprovalDetailsPage);


/***/ }),

/***/ 87785:
/*!***************************************************************************************************************!*\
  !*** ./src/app/cardinal/vender-approval/vender-approval-details/vender-approval-details.page.scss?ngResource ***!
  \***************************************************************************************************************/
/***/ ((module) => {

module.exports = ".mat-stepper-vertical {\n  margin-top: 8px;\n}\n\n.mat-form-field {\n  margin-top: 16px;\n}\n\n.example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%;\n}\n\n.example-full-width {\n  width: 100%;\n}\n\n.cus {\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  width: 100%;\n}\n\nion-datetime {\n  width: 100%;\n}\n\nion-popover {\n  --width: 320px;\n}\n\nion-popover.dateTimePopover {\n  --offset-y: -350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZlbmRlci1hcHByb3ZhbC1kZXRhaWxzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQUE7QUFDSjs7QUFFRTtFQUNFLGdCQUFBO0FBQ0o7O0FBRUU7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQUNKOztBQUVFO0VBQ0UsV0FBQTtBQUNKOztBQUNFO0VBRUUsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBQ0c7RUFDQyxXQUFBO0FBRUo7O0FBQUU7RUFDRSxjQUFBO0FBR0o7O0FBREU7RUFDRSw2QkFBQTtBQUlKIiwiZmlsZSI6InZlbmRlci1hcHByb3ZhbC1kZXRhaWxzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXQtc3RlcHBlci12ZXJ0aWNhbCB7XG4gICAgbWFyZ2luLXRvcDogOHB4O1xuICB9XG4gIFxuICAubWF0LWZvcm0tZmllbGQge1xuICAgIG1hcmdpbi10b3A6IDE2cHg7XG4gIH1cblxuICAuZXhhbXBsZS1mb3JtIHtcbiAgICBtaW4td2lkdGg6IDE1MHB4O1xuICAgIG1heC13aWR0aDogNTAwcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbiAgXG4gIC5leGFtcGxlLWZ1bGwtd2lkdGgge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIC5jdXN7XG4gIFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlciA7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgfVxuICAgaW9uLWRhdGV0aW1lIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuICBpb24tcG9wb3ZlciB7XG4gICAgLS13aWR0aDogMzIwcHg7XG4gIH1cbiAgaW9uLXBvcG92ZXIuZGF0ZVRpbWVQb3BvdmVyIHtcbiAgICAtLW9mZnNldC15OiAtMzUwcHggIWltcG9ydGFudDtcbiAgICB9Il19 */";

/***/ }),

/***/ 45372:
/*!***************************************************************************************************************!*\
  !*** ./src/app/cardinal/vender-approval/vender-approval-details/vender-approval-details.page.html?ngResource ***!
  \***************************************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      Compliant Acceptance Details\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <!-- <ion-buttons (click)=\"refreshPage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons> -->\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <mat-vertical-stepper [linear]=\"isLinear\" #stepper>\n    <!-- Detail Tab -->\n    <mat-step [stepControl]=\"detailFormGroup\">\n      <form [formGroup]=\"detailFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Details</ng-template>\n        <!-- complaint_no -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Complaint No.</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.complaintno\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- complaint_date -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Complaint Date</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.complaintdate\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Document Type -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Document Type</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.doctype[0].name\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Name of Complainer -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Name of Complainer</mat-label>\n          <input matInput formControlName=\"nameOfComplainerCtrl\" [(ngModel)]=\"service.nameofcomplainer\">\n        </mat-form-field>\n        <!-- Designation of Complainer -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Designation of Complainer</mat-label>\n          <input matInput formControlName=\"desigOfComplainerCtrl\" [(ngModel)]=\"service.desofcomplnr[0].name\">\n        </mat-form-field>\n        <!-- Contact No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Contact No</mat-label>\n          <input matInput formControlName=\"mobilenoCtrl\" [(ngModel)]=\"service.contnumber\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.mobileno\">\n              <div *ngIf=\"detailFormGroup.get('mobilenoCtrl').hasError(validation.type) && detailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Email ID -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Email ID</mat-label>\n          <input matInput formControlName=\"emailIDCtrl\" [(ngModel)]=\"service.email\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.email\">\n              <div *ngIf=\"detailFormGroup.get('emailIDCtrl').hasError(validation.type) && detailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Event Date -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Event Date</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.eventdate\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Complaint Description -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Complaint Description</mat-label>\n          <input matInput formControlName=\"complaintDescriptionCtrl\" [(ngModel)]=\"service.description\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.complaintDescription\">\n              <div\n                *ngIf=\"detailFormGroup.get('complaintDescriptionCtrl').hasError(validation.type) && detailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperNext (click)=\"matDetailSettper($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Product Compliant Tab-->\n    <mat-step [stepControl]=\"productCompliantFormGroup\" *ngIf=\"isProductCompliantTab\">\n      <form [formGroup]=\"productCompliantFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Product Compliant</ng-template>\n        <!-- Lot No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Lot No.</mat-label>\n          <input matInput formControlName=\"lotNoCtrl\">\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperProductCompliant($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Consumables Tab  -->\n    <mat-step [stepControl]=\"consumablesFormGroup\" *ngIf=\"isConsumablesTab\">\n      <form [formGroup]=\"consumablesFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Consumables</ng-template>\n        <!-- Lot No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Lot No.</mat-label>\n          <input matInput formControlName=\"lotNoConsumablesCtrl\">\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperConsumables($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Equiment Tab-->\n    <mat-step [stepControl]=\"equimentFormGroup\" *ngIf=\"isEquimentTab\">\n      <form [formGroup]=\"equimentFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Equiment</ng-template>\n        <!-- Serial No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Serial No.</mat-label>\n          <input matInput formControlName=\"serialNoCtrl\" [(ngModel)]=\"service.serialno\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.serialNo\">\n              <div *ngIf=\"equimentFormGroup.get('serialNoCtrl').hasError(validation.type) && equimentStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Contract Type -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Contract Type</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"contracttype\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Invoice No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Invoice No.</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.invoiceno\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Invoice Date -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Invoice Date</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.invoicedate\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Error Code -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Error Code</mat-label>\n            <input matInput [disabled]='true' [(ngModel)]=\"service.errorcode[0].code\" [ngModelOptions]=\"{standalone: true}\">  \n        </mat-form-field>\n        <!-- Dealer Name -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Dealer Name</mat-label>\n          <input matInput formControlName=\"dealerNameCtrl\" [(ngModel)]=\"service.dealername\">\n        </mat-form-field>\n         <!-- New Dealer Name\n         <mat-form-field class=\"example-full-width\">\n          <mat-label>Dealer Name</mat-label>\n          <input matInput formControlName=\"newDealerNameCtrl\" [(ngModel)]=\"service.newdealername\">\n        </mat-form-field> -->\n        \n        <!-- Sales Representative -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Sales Representative</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.salesrep\"\n            [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Installation Date -->\n         <mat-form-field class=\"example-full-width\">\n          <mat-label>Installation Date</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.installationdate\"\n            [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperEquiment($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- SKU Details Tab-->\n    <mat-step [stepControl]=\"skuDetailsFormGroup\">\n      <form [formGroup]=\"skuDetailsFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>SKU Details</ng-template>\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>SKU Code</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.skucode\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- SKU Name / Description -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>SKU Name / Description</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.skuname\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Brand Name -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Brand Name</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.brandname\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Product to be returned -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Product to be returned</mat-label>\n          <mat-select [disabled]='true' [(ngModel)]=\"productToBeReturn\" [ngModelOptions]=\"{standalone: true}\">\n            <mat-option value=\"Yes\">Yes</mat-option>\n            <mat-option value=\"No\">No</mat-option>\n          </mat-select>\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperSKUDetails($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Customer Detail Tab -->\n    <mat-step [stepControl]=\"customerDetailFormGroup\">\n      <form [formGroup]=\"customerDetailFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Customer Detail</ng-template>\n        <!-- Customer Name -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Customer Name</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.custname\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Customer Address 1 -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Customer Address 1</mat-label>\n          <input matInput formControlName=\"customerAddress1Ctrl\" [(ngModel)]=\"service.add1\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.customerAddress1\">\n              <div\n                *ngIf=\"customerDetailFormGroup.get('customerAddress1Ctrl').hasError(validation.type) && customerDetailStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Customer Address 2 -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Customer Address 2</mat-label>\n          <input matInput formControlName=\"customerAddress2Ctrl\" [(ngModel)]=\"service.add2\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.customerAddress2\">\n              <div\n                *ngIf=\"customerDetailFormGroup.get('customerAddress2Ctrl').hasError(validation.type) && customerDetailStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Customer Address 3 -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Customer Address 3</mat-label>\n          <input matInput formControlName=\"customerAddress3Ctrl\" [(ngModel)]=\"service.add3\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.customerAddress3\">\n              <div\n                *ngIf=\"customerDetailFormGroup.get('customerAddress3Ctrl').hasError(validation.type) && customerDetailStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Pin Code -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Pin Code</mat-label>\n          <input type=\"number\" matInput formControlName=\"pinCodeCtrl\" [(ngModel)]=\"service.pincode\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.pinCode\">\n              <div\n                *ngIf=\"customerDetailFormGroup.get('pinCodeCtrl').hasError(validation.type) && customerDetailStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Area -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Area</mat-label>\n          <input matInput [disabled]='true' formControlName=\"areaCtrl\">\n        </mat-form-field>\n        <!-- City -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>City</mat-label>\n          <input matInput [disabled]='true' formControlName=\"cityCtrl\">\n        </mat-form-field>\n        <!-- State -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>State</mat-label>\n          <input matInput [disabled]='true' formControlName=\"stateCtrl\">\n        </mat-form-field>\n        <!-- Country -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Country</mat-label>\n          <input matInput [disabled]='true' formControlName=\"countryCtrl\">\n        </mat-form-field>\n        <!-- Service Engineer Name -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Service Engineer Name</mat-label>\n            <mat-select formControlName=\"serviceEngineerNameCtrl\" [(ngModel)]=\"serviceEngSelected\"\n                (selectionChange)=\"serviceEngSelectedChange(serviceEngSelected)\">\n               <mat-option *ngFor=\"let serviceEngObj of serviceEngList\" [value]=\"serviceEngObj\">\n                  {{serviceEngObj.name}}\n              </mat-option>\n            </mat-select>\n        </mat-form-field>\n         <!--Service Engineer Contact No -->\n         <mat-form-field class=\"example-full-width\">\n          <mat-label>Contact No</mat-label>\n          <input matInput formControlName=\"serviceEngMobilenoCtrl\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.mobileno\">\n              <div *ngIf=\"customerDetailFormGroup.get('serviceEngMobilenoCtrl').hasError(validation.type) && detailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Date of Visit -->\n        <div class=\"example-full-width bottom-border\">\n          <ion-label style=\"color: darkgray;\">Date of Visit</ion-label>\n          <section class=\"cus\">\n           <!-- <ion-datetime  style=\"--padding-start:0px\" displayFormat=\"DD-MM-YYYY\"  formControlName=\"dateOfVisitCtrl\" max=\"2050-12-31\" [min]=\"maxDate | date:'yyyy-MM-dd'\">\n          </ion-datetime>\n          <ion-icon name=\"calendar\"></ion-icon>  -->\n          <ion-item>\n            <ion-input placeholder=\"Select Date\" [value]=\"datevisit\"></ion-input>\n            <ion-button fill=\"clear\" id=\"open-date-input-1\">\n              <ion-icon icon=\"calendar\"></ion-icon>\n            </ion-button>\n            <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\n              <ng-template>\n                <ion-datetime\n                  #popoverDatetime2\n                  formControlName=\"dateOfVisitCtrl\" \n                  max=\"2050-12-31\" [min]=\"maxDate | date:'yyyy-MM-dd'\"\n                  presentation=\"date\"\n                  showDefaultButtons=\"true\"\n                  (ionChange)=\"datevisit = formatDate(popoverDatetime2.value)\"\n                ></ion-datetime>\n              </ng-template>\n            </ion-popover>\n          </ion-item>\n        </section>\n        </div>\n        <div padding-left>\n          <ng-container *ngFor=\"let validation of validation_messages.dateOfVisitCtrlErrorMessage\">\n            <div *ngIf=\"customerDetailFormGroup.get('serviceVendorRemarksCtrl').hasError(validation.type) && detailsStepValid\">\n              <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n            </div>\n          </ng-container>\n        </div>\n\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label>Date of Visit</mat-label>\n          <input type=\"date\" matInput formControlName=\"dateOfVisitCtrl\" [min]=\"maxDate | date:'yyyy-MM-dd'\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.dateOfVisitCtrlErrorMessage\">\n              <div *ngIf=\"customerDetailFormGroup.get('serviceVendorRemarksCtrl').hasError(validation.type) && detailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <!-- Service Vendor Remarks -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Service Vendor Remarks</mat-label>\n          <input matInput formControlName=\"serviceVendorRemarksCtrl\" [(ngModel)]=\"service.service_vendor_remarks\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.serviceVendorRemarksCtrlErrorMessage\">\n              <div *ngIf=\"customerDetailFormGroup.get('serviceVendorRemarksCtrl').hasError(validation.type) && detailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <div>\n          <ion-row>\n            <ion-col size=\"6\" size-lg=\"3\" no-padding>\n              <ion-button size=\"default\" [disabled]=\"!customerDetailFormGroup.controls.serviceVendorRemarksCtrl.valid\" expand=\"block\" color=\"primary\" (click)=\"submit($event,'Reject')\">Reject Complaint</ion-button>\n            </ion-col>\n            <ion-col size=\"6\" size-lg=\"3\" no-padding>\n              <ion-button size=\"default\" [disabled]=\"!customerDetailFormGroup.valid\" expand=\"block\" color=\"primary\" (click)=\"submit($event,'Accept')\">Accept Complaint\n              </ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n       \n      </form>\n    </mat-step>\n  </mat-vertical-stepper>\n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_cardinal_vender-approval_vender-approval-details_vender-approval-details_module_ts.js.map