"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_addedit-business-partner-address_addedit-business-partner-address_module_ts"],{

/***/ 38978:
/*!*********************************************************************************************!*\
  !*** ./src/app/addedit-business-partner-address/addedit-business-partner-address.module.ts ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddeditBusinessPartnerAddressPageModule": () => (/* binding */ AddeditBusinessPartnerAddressPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _addedit_business_partner_address_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./addedit-business-partner-address.page */ 35979);







const routes = [
    {
        path: '',
        component: _addedit_business_partner_address_page__WEBPACK_IMPORTED_MODULE_0__.AddeditBusinessPartnerAddressPage
    }
];
let AddeditBusinessPartnerAddressPageModule = class AddeditBusinessPartnerAddressPageModule {
};
AddeditBusinessPartnerAddressPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_addedit_business_partner_address_page__WEBPACK_IMPORTED_MODULE_0__.AddeditBusinessPartnerAddressPage]
    })
], AddeditBusinessPartnerAddressPageModule);



/***/ }),

/***/ 35979:
/*!*******************************************************************************************!*\
  !*** ./src/app/addedit-business-partner-address/addedit-business-partner-address.page.ts ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddeditBusinessPartnerAddressPage": () => (/* binding */ AddeditBusinessPartnerAddressPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _addedit_business_partner_address_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./addedit-business-partner-address.page.html?ngResource */ 27679);
/* harmony import */ var _addedit_business_partner_address_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./addedit-business-partner-address.page.scss?ngResource */ 36252);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _provider_validator_helper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../provider/validator-helper */ 35096);
/* harmony import */ var _newcustomer_newcustomer_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../newcustomer/newcustomer.service */ 85189);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _business_partner_address_business_partner_address_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../business-partner-address/business-partner-address.service */ 84718);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @awesome-cordova-plugins/camera/ngx */ 64587);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../provider/message-helper */ 98792);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! date-fns */ 86527);
















let AddeditBusinessPartnerAddressPage = class AddeditBusinessPartnerAddressPage {
  constructor(fb, val, newcustomerservice, router, route, commonfun, businesspartneraddressservice, alertCtrl, camera, platform, msg, loginauth) {
    this.fb = fb;
    this.val = val;
    this.newcustomerservice = newcustomerservice;
    this.router = router;
    this.route = route;
    this.commonfun = commonfun;
    this.businesspartneraddressservice = businesspartneraddressservice;
    this.alertCtrl = alertCtrl;
    this.camera = camera;
    this.platform = platform;
    this.msg = msg;
    this.loginauth = loginauth;
    this.district = '';
    this.samename = false;
    this.IsUnregistered = false;
    this.fileUrl = '';
    this.img = '';
    this.isgst = false;
    this.mmstRegioncode = "";
    this.isdesktop = false;
    this.datefrom = '';
    this.showPreferredTransportControl = false;
    this.showPreferredTransportNameControl = false;
    this.validation_messages = {
      'add1': [{
        type: 'required',
        message: '*Please Enter Address Line 1'
      }],
      'name': [{
        type: 'required',
        message: '*Please Enter Name.'
      }],
      'pincode': [{
        type: 'required',
        message: '*Please Enter Pin Code'
      }],
      'selectedarea': [{
        type: 'required',
        message: '*Please Select Area.'
      }],
      'gstno': [{
        type: 'InvalidgstNumber',
        message: '*Please Enter Valid GST No.'
      }],
      'preferredTransportNameMss': [{
        type: 'required',
        message: '*Please Enter Preferred Transport Name'
      }],
      'preferredTransportContactNumberCtrlMss': [{
        type: 'required',
        message: '*Please Enter Preferred Transport Contact Number'
      }],
      'preferredTransportEmailIDCtrllMss': [{
        type: 'InvalidEmail',
        message: '*Please Enter Valid Email.'
      }]
    };

    try {
      this.getparam();
    } catch {}

    this.form = this.fb.group({
      add1: [, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required],
      name: [, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required],
      add2: [],
      add3: [],
      pincode: [, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required],
      selectedarea: [, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required],
      isbill: [],
      isShip: [],
      fromdate: [],
      todate: [],
      NamesameasCustome: [],
      gstno: [],
      Unregistered: [],
      preferredCustomerTransport: [],
      preferredTransportName: [],
      preferredTransportContactNumberCtrl: [],
      preferredTransportEmailIDCtrl: []
    });
  }

  ngOnInit() {
    this.Resetpage(); //this.commonfun.chkcache('business-partner-address');

    setTimeout(() => {
      this.checkplatform();
    }, 1500);
  }

  formatDate(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_11__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_12__["default"])(value), 'dd.MM.yyyy');
  }

  getparam() {
    try {
      this.route.params.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          // this.selectedbunch=null;
          // 
          this.businessPartner = this.router.getCurrentNavigation().extras.state.businessPartner;
          this.cust_id = this.router.getCurrentNavigation().extras.state.businessPartner.id;
          this.samename = true;
          this.form.controls["name"].setValue(this.businessPartner["fname"]);
          this.ComplianceList = this.router.getCurrentNavigation().extras.state.ComplianceList;
          this.cartaddresslist = this.router.getCurrentNavigation().extras.state.cartaddress; //this.onchangeNamesameasCustome();
        }
      });
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  checkplatform() {
    try {
      //  if(!this.platform.is("desktop")){
      if (!this.msg.isplatformweb) {
        // 
        this.isdesktop = false;
      } else {
        this.isdesktop = true;
      }
    } catch (error) {}
  }

  uploadImage(str) {
    try {
      var file = str.target.files[0];
      var myreader = new FileReader();

      myreader.onloadend = e => {
        var b64 = myreader.result.toString().replace(/^data:.+;base64,/, '');
        this.fileUrl = myreader.result;
        this.img = b64;
      };

      myreader.readAsDataURL(file);
    } catch (error) {}
  }

  uploadcompImage(str, id) {
    try {
      var file = str.target.files[0];
      var myreader = new FileReader();

      myreader.onloadend = e => {
        var b64 = myreader.result.toString().replace(/^data:.+;base64,/, ''); //  this.fileUrl=myreader.result;
        // this.img=b64;
        //this.ChosePic("complince",id);

        for (var i = 0; i < this.ComplianceList.length; i++) {
          if (this.ComplianceList[i].code == id) {
            this.ComplianceList[i].image = b64;
          }
        }
      };

      myreader.readAsDataURL(file);
    } catch (error) {}
  }

  onchangeNamesameasCustome() {
    this.samename = this.form.controls["NamesameasCustome"].value;

    if (this.samename == true) {
      this.form.controls["name"].setValue(this.businessPartner["fname"]);
    }
  }

  onchangeUnregistered() {
    try {
      this.IsUnregistered = this.form.controls["Unregistered"].value;

      if (this.IsUnregistered == true) {
        this.form.controls["gstno"].setValue(""); // this.isgst=false;  

        this.fileUrl = "";
        this.img = "";
        this.isgst = true;
      } else {
        this.isgst = false;

        for (var x = 0; x < this.cartaddresslist.length; x++) {
          if (this.pincodelist[0].region == this.cartaddresslist[x].regionid) {
            if (this.cartaddresslist[x].gstno != "") {
              this.isgst = true; //  
              //  

              this.form.controls["gstno"].setValue(this.cartaddresslist[x].gstno);
            }
          }
        } //this.isgst=true;  

      }
    } catch {}
  }

  onChangepin(id = '', customerId) {
    //var that = this;
    this.isgst = false; //this.form.controls["gstno"].

    this.form.controls["gstno"].setValue(null);
    this.form.controls['Unregistered'].setValue(false);
    this.onchangeUnregistered();
    this.city = "";
    this.state = "";
    this.country = "";
    this.district = "";
    this.newcustomerservice.getPincode(this.form.controls["pincode"].value).subscribe(data => {
      const response = data['response'];
      this.pincodelist = response.data;

      if (this.pincodelist.length > 0) {
        this.invalidpincode = '';
        this.newcustomerservice.getarea(this.pincodelist[0].id).subscribe(data => {
          const response = data['response'];
          this.arealist = response.data;
          this.state = this.pincodelist[0].region$_identifier;
          this.country = this.pincodelist[0].country$_identifier;
          this.district = this.pincodelist[0].district$_identifier;

          if (id != '' || id == undefined) {
            this.selectedarea = this.arealist.find(item => item.id === id);
            setTimeout(() => {
              this.form.controls["selectedarea"].setValue(this.selectedarea);
            }, 1500);
          }
        });
        this.newcustomerservice.getregion(this.pincodelist[0].region).subscribe(data => {
          const response = data['response'];
          this.mmstRegioncode = response.data[0].mmstRegioncode;
        });
        setTimeout(() => {
          var isexistpin = false;

          for (var x = 0; x < this.cartaddresslist.length; x++) {
            if (this.pincodelist[0].region == this.cartaddresslist[x].regionid) {
              if (this.cartaddresslist[x].gstno != "") {
                this.isgst = true;
                isexistpin = true;
                this.form.controls["gstno"].setValue(this.cartaddresslist[x].gstno);
              }
            }
          }

          if (isexistpin == false) {
            this.isgst = false;
            this.form.controls["gstno"].setValue("");
          }
        }, 1600);
        this.invalidpincode = '';
      } else {
        this.state = '';
        this.country = '';
        this.district = '';
        this.invalidpincode = 'Invalid Pincode';
        this.arealist = null;
        this.city = '';
        this.form.controls["selectedarea"].setValue(null);
        this.form.controls["gstno"].setValue("");
        this.isgst = false;
      }
    }), error => {
      this.state = '';
      this.country = '';
      this.district = '';
      this.invalidpincode = 'Invalid Pincode';
      this.arealist = null;
      this.city = '';
    };
  }

  onChangegst() {
    var validgstn = false;

    try {
      var gstn = this.form.controls["gstno"].value;

      if (gstn != null && gstn != undefined && gstn != "") {
        if (gstn.length > 2) {
          if (this.mmstRegioncode != gstn.substr(0, 2)) {
            this.commonfun.presentAlert("Message", "Alert", "GST Number must be of same state.");
            this.form.controls["gstno"].setValue("");
            validgstn = false;
          } else if (this.isgst == false) {
            if (this.fileUrl == null || this.fileUrl == undefined || this.fileUrl == "") {
              this.commonfun.presentAlert("Message", "Alert", "Please upload GST image.");
              validgstn = false;
            } else {
              validgstn = true;
            }
          } else {
            validgstn = true;
          }
        } else {
          validgstn = true;
        }
      } else {
        validgstn = true;
      }
    } catch (error) {}

    return validgstn;
  }

  onChangeArea() {
    this.selectedarea = this.form.controls["selectedarea"].value;
    this.city = this.selectedarea.cttv$_identifier;
  }

  Resetpage() {
    try {
      this.form.reset();
      this.isgst = false;
      this.form.controls["fromdate"].setValue(new Date().toISOString());
      this.form.controls["todate"].setValue(new Date().toISOString());
      this.form.controls["NamesameasCustome"].setValue(true);
      this.form.controls["name"].setValue(this.businessPartner["fname"]);
      this.district = "";
      this.state = "";
      this.country = "";
    } catch {}
  }

  onSaveDraft(value) {
    if (this.onChangegst() == false) {
      return;
    }

    this.commonfun.loadingPresent();
    var jsondata = {
      // "mbso_copord_id":this.selecteddocumentno==undefined||this.selecteddocumentno==""||this.selecteddocumentno==null?"":this.selecteddocumentno.id,
      "cust_id": this.cust_id,
      "name": value.name,
      "billing": value.isbill == true ? "true" : "false",
      "shipping": value.isShip == true ? 'true' : 'false',
      "add1": value.add1,
      "add2": value.add2,
      "add3": value.add3,
      "pincode": value.pincode,
      "area": value.selectedarea.id,
      "city": this.selectedarea.cttv,
      "state": this.pincodelist[0].region,
      "country": this.pincodelist[0].country,
      "district": this.pincodelist[0].district,
      "fromdate": value.fromdate,
      "gst": value.gstno != null ? value.gstno.toUpperCase() : "",
      "gstimg": this.img,
      "ComplianceList": this.ComplianceList == undefined ? "" : this.ComplianceList
    };

    if (this.showPreferredTransportNameControl) {
      jsondata["cusPreferredTransport"] = value.preferredCustomerTransport.id;
      jsondata["cusPreferredTransportOther"] = this.showPreferredTransportNameControl;
      jsondata["preferredTransportName"] = value.preferredTransportName;
      jsondata["preferredTransportContact"] = value.preferredTransportContactNumberCtrl;
      jsondata["preferredTransportEmailID"] = value.preferredTransportEmailIDCtrl;
    }

    this.businesspartneraddressservice.SaveAddress(jsondata).subscribe(data => {
      if (data != null) {
        this.response = data;

        if (this.response.resposemsg == "success") {
          this.commonfun.loadingDismiss();
          this.commonfun.presentAlert("Message", "Success", "Address Successfully added, Its under KYC Apporval.");
          this.ComplianceList = null;
          this.Resetpage();
          let navigationExtras = {
            state: {
              reset: true
            }
          };
          this.router.navigate(['/business-partner-address'], navigationExtras);
        } else {
          this.commonfun.loadingDismiss();
          this.commonfun.presentAlert("message", "Fail", this.response.logmsg);
        }
      }
    });
  } //Capture Image from Camera


  takePicture(Type, id) {
    const options = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      targetWidth: 1500,
      targetHeight: 1500
    };
    this.camera.getPicture(options).then(imageData => {
      if (Type == "PANpic") {
        this.fileUrl = 'data:image/jpeg;base64,' + imageData;
        this.img = imageData;
      }

      if (Type == "complince") {
        for (var i = 0; i < this.ComplianceList.length; i++) {
          if (this.ComplianceList[i].code == id) {
            this.ComplianceList[i].image = imageData;
          }
        }
      }
    }, err => {});
  } //Select Image from library


  getimage(Type, id) {
    const options = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 1500,
      targetHeight: 1500
    };
    this.camera.getPicture(options).then(imageData => {
      if (Type == "PANpic") {
        this.fileUrl = 'data:image/jpeg;base64,' + imageData;
        this.img = imageData;
      }

      if (Type == "complince") {
        for (var i = 0; i < this.ComplianceList.length; i++) {
          if (this.ComplianceList[i].code == id) {
            this.ComplianceList[i].image = imageData;
          }
        }
      }
    }, err => {});
  }

  ChosePic(Type, code) {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this.alertCtrl.create({
        header: 'Select Option',
        message: "Select Option to get Picture.",
        buttons: [{
          text: 'Gallery',
          handler: data => {
            _this.getimage(Type, code);
          }
        }, {
          text: 'Camera',
          handler: data => {
            _this.takePicture(Type, code);
          }
        }]
      });
      yield alert.present();
    })();
  }

  ImageViewr(img, rowcompliancedata) {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      var msg = "";

      if (rowcompliancedata != null) {
        msg = '<div>' + '<img class="viewImagecss" src="data:image/png;base64,' + img + '">' + '</div>';
      } else {
        msg = '<div>' + '<img class="viewImagecss" src="' + img + '">' + '</div>';
      }

      const alert = yield _this2.alertCtrl.create({
        message: msg,
        buttons: [{
          text: 'Remove',
          handler: data => {
            //this.POimg64=null
            if (rowcompliancedata != null) {
              _this2.ComplianceList = _this2.ComplianceList.map(function (comdata) {
                comdata.image = comdata.code == rowcompliancedata.code ? null : comdata.image;
                return comdata;
              });
            } else {
              _this2.fileUrl = null;
            }
          }
        }, {
          text: 'OK'
        }]
      });
      yield alert.present();
    })();
  }

  onShippingChange(value) {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        console.log("Shipping Address ", value.detail.checked);

        if (value.detail.checked) {
          if (_this3.loginauth.selectedactivity.preferred_transport_required) {
            let control1 = null;
            control1 = _this3.form.get('preferredCustomerTransport');
            control1.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required);
            control1.updateValueAndValidity();
            _this3.showPreferredTransportControl = true;

            _this3.commonfun.loadingPresent();

            const result = yield _this3.newcustomerservice.getPreferredTransportModes().toPromise();
            _this3.preferredTransportList = result[0];
            console.log("Preferred Transport Modes ", _this3.preferredTransportList);

            _this3.commonfun.loadingDismiss();
          }
        } else {
          setTimeout(() => {
            _this3.showPreferredTransportControl = false;
            _this3.showPreferredTransportNameControl = false;
          });

          _this3.form.controls['preferredCustomerTransport'].reset();

          let control1 = null;
          control1 = _this3.form.get('preferredCustomerTransport');
          control1.clearValidators();
          control1.updateValueAndValidity();
        }
      } catch (error) {
        _this3.commonfun.loadingDismiss();

        console.log(error);
      }
    })();
  }

  onChangePreferredTransportContact() {}

  onCusPreTransChange() {
    try {
      console.log("SELECTED PREPED TRANS", this.selectedPreferredTransport);
      this.form.controls['preferredTransportName'].reset();
      this.form.controls['preferredTransportContactNumberCtrl'].reset();
      this.form.controls['preferredTransportEmailIDCtrl'].reset();

      if (this.selectedPreferredTransport.name == "Others") {
        this.showPreferredTransportNameControl = true;
        let control2 = null;
        control2 = this.form.get('preferredTransportName');
        control2.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required);
        control2.updateValueAndValidity();
      } else {
        setTimeout(() => {
          this.showPreferredTransportNameControl = false;
        });
        let control2 = null;
        control2 = this.form.get('preferredTransportName');
        control2.clearValidators();
        control2.updateValueAndValidity();
      }
    } catch (error) {
      console.log(error);
    }
  }

  test(form) {
    console.log(form);
  }

};

AddeditBusinessPartnerAddressPage.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_10__.FormBuilder
}, {
  type: _provider_validator_helper__WEBPACK_IMPORTED_MODULE_3__.Validator
}, {
  type: _newcustomer_newcustomer_service__WEBPACK_IMPORTED_MODULE_4__.NewcustomerService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_13__.Router
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_13__.ActivatedRoute
}, {
  type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_5__.Commonfun
}, {
  type: _business_partner_address_business_partner_address_service__WEBPACK_IMPORTED_MODULE_6__.BusinessPartnerAddressService
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_14__.AlertController
}, {
  type: _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_7__.Camera
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_14__.Platform
}, {
  type: _provider_message_helper__WEBPACK_IMPORTED_MODULE_8__.Message
}, {
  type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_9__.LoginauthService
}];

AddeditBusinessPartnerAddressPage = (0,tslib__WEBPACK_IMPORTED_MODULE_15__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_16__.Component)({
  selector: 'app-addedit-business-partner-address',
  template: _addedit_business_partner_address_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_addedit_business_partner_address_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], AddeditBusinessPartnerAddressPage);


/***/ }),

/***/ 36252:
/*!********************************************************************************************************!*\
  !*** ./src/app/addedit-business-partner-address/addedit-business-partner-address.page.scss?ngResource ***!
  \********************************************************************************************************/
/***/ ((module) => {

module.exports = ".inputfile {\n  color: transparent;\n}\n\nion-popover {\n  --width: 320px;\n}\n\nion-popover.dateTimePopover {\n  --offset-y: -350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZGVkaXQtYnVzaW5lc3MtcGFydG5lci1hZGRyZXNzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0FBQ0o7O0FBQ0U7RUFDRSxjQUFBO0FBRUo7O0FBQUU7RUFDRSw2QkFBQTtBQUdKIiwiZmlsZSI6ImFkZGVkaXQtYnVzaW5lc3MtcGFydG5lci1hZGRyZXNzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbnB1dGZpbGUge1xuICAgIGNvbG9yOiB0cmFuc3BhcmVudDtcbiAgfVxuICBpb24tcG9wb3ZlciB7XG4gICAgLS13aWR0aDogMzIwcHg7XG4gIH1cbiAgaW9uLXBvcG92ZXIuZGF0ZVRpbWVQb3BvdmVyIHtcbiAgICAtLW9mZnNldC15OiAtMzUwcHggIWltcG9ydGFudDtcbiAgICB9Il19 */";

/***/ }),

/***/ 27679:
/*!********************************************************************************************************!*\
  !*** ./src/app/addedit-business-partner-address/addedit-business-partner-address.page.html?ngResource ***!
  \********************************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      Add New Address\n    </ion-title>\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]=\"form\" (ngSubmit)=\"onSaveDraft(form.value)\">\n    <ion-grid>\n      <ion-card>\n        <ion-card-header style=\"background: #FFCC80;\">\n          Address :\n        </ion-card-header>\n        <ion-row>\n          <ion-col size=\"12\" size-lg=\"4\">\n            <ion-item>\n              <ion-label position=\"floating\">Name<span style=\"color:red!important\">*</span></ion-label>\n              <ion-input formControlName=\"name\" type=\"text\" [disabled]=\"samename\"></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.name\">\n                <div *ngIf=\"form.get('name').hasError(validation.type) && form.get('name').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"4\">\n            <ion-item>\n              <ion-label>Name same as Customer</ion-label>\n              <ion-checkbox slot=\"end\" formControlName=\"NamesameasCustome\"\n                (ionChange)=\"onchangeNamesameasCustome()\"></ion-checkbox>\n            </ion-item>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"4\">\n            <ion-item>\n              <ion-label>Billing</ion-label>\n              <ion-checkbox slot=\"end\" formControlName=\"isbill\"></ion-checkbox>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col size=\"12\" size-lg=\"4\">\n            <ion-item>\n              <ion-label>Shipping</ion-label>\n              <ion-checkbox slot=\"end\" formControlName=\"isShip\" (ionChange)=\"onShippingChange($event)\">\n              </ion-checkbox>\n            </ion-item>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"4\" *ngIf=\"showPreferredTransportControl\">\n            <ion-item>\n              <ion-label position=\"stacked\">Customer Preferred Transport<span style=\"color:red!important\">*</span>\n              </ion-label>\n              <ion-select [(ngModel)]=\"selectedPreferredTransport\" formControlName=\"preferredCustomerTransport\" interface=\"popover\"\n                (ionChange)=\"onCusPreTransChange()\" multiple=\"false\" placeholder=\"Select Activity\">\n                <ion-select-option *ngFor=\"let preferredTransport of preferredTransportList\"\n                  [value]=\"preferredTransport\">{{preferredTransport.name}}\n                </ion-select-option>\n              </ion-select>\n            </ion-item>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"4\" *ngIf=\"showPreferredTransportNameControl\">\n            <ion-item>\n              <ion-label position=\"floating\">Preferred Transport Name<span style=\"color:red!important\">*</span>\n              </ion-label>\n              <ion-input formControlName=\"preferredTransportName\" type=\"text\"></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.preferredTransportNameMss\">\n                <div\n                  *ngIf=\"form.get('preferredTransportName').hasError(validation.type) && form.get('preferredTransportName').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"4\" *ngIf=\"showPreferredTransportNameControl\">\n            <ion-item>\n              <ion-label position=\"floating\">Preferred Transport Contact Number</ion-label>\n              <ion-input formControlName=\"preferredTransportContactNumberCtrl\" type=\"number\"\n                (change)='onChangePreferredTransportContact()'></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.preferredTransportContactNumberCtrlMss\">\n                <div\n                  *ngIf=\"form.get('preferredTransportContactNumberCtrl').hasError(validation.type) && form.get('preferredTransportContactNumberCtrl').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n\n          <ion-col size=\"12\" size-lg=\"4\" *ngIf=\"showPreferredTransportNameControl\">\n            <ion-item>\n              <ion-label position=\"floating\">Preferred Transport Email ID</ion-label>\n              <ion-input formControlName=\"preferredTransportEmailIDCtrl\" \n              pattern=\"[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})\">\n              </ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.preferredTransportEmailIDCtrllMss\">\n                <div\n                  *ngIf=\"form.get('preferredTransportEmailIDCtrl').hasError(validation.type) && form.get('preferredTransportEmailIDCtrl').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"4\">\n            <ion-item>\n              <ion-label position=\"floating\">Address Line 1<span style=\"color:red!important\">*</span></ion-label>\n              <ion-input formControlName=\"add1\" type=\"text\"></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.add1\">\n                <div *ngIf=\"form.get('add1').hasError(validation.type) && form.get('add1').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"4\">\n            <ion-item>\n              <ion-label position=\"floating\">Address Line 2 </ion-label>\n              <ion-input formControlName=\"add2\" type=\"text\"></ion-input>\n            </ion-item>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"4\">\n            <ion-item>\n                <ion-label position=\"floating\">Address Line 3</ion-label>\n                <ion-input formControlName=\"add3\"  type=\"text\"></ion-input>\n            </ion-item>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"4\">\n            <ion-item>\n              <ion-row>\n                <ion-col>\n                  <ion-label position=\"floating\">Pin Code<span style=\"color:red!important\">*</span></ion-label>\n                </ion-col>\n              </ion-row>\n              <ion-input formControlName=\"pincode\"  type=\"number\" (change)='onChangepin()'></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.pincode\">\n                <div *ngIf=\"form.get('pincode').hasError(validation.type) && form.get('pincode').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n            <div padding-left>\n              <ng-container>\n                <div>\n                  <p style=\"color: red;font-size: small;\">{{invalidpincode}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"4\">\n            <ion-item >\n                <ion-label position=\"stacked\">Area<span style=\"color:red!important\">*</span></ion-label>\n                <ion-select  #C  formControlName=\"selectedarea\" interface=\"popover\" (ionChange)=\"onChangeArea()\" multiple=\"false\" placeholder=\"Select Area\" required>\n                  <ion-select-option *ngFor=\"let area of arealist\" [value]=\"area\">{{area.area}}</ion-select-option>\n                </ion-select>\n              </ion-item>\n              <div padding-left>\n                <ng-container *ngFor=\"let validation of validation_messages.selectedarea\">\n                  <div *ngIf=\"form.get('selectedarea').hasError(validation.type) && form.get('selectedarea').touched\">\n                    <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                  </div>\n                </ng-container>\n              </div>\n        </ion-col>\n        <ion-col size=\"12\" size-lg=\"4\">\n          <ion-item >\n            <ion-label position=\"floating\">City</ion-label>\n            <ion-input type=\"text\" value=\"{{city}}\" disabled=\"true\"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col size=\"12\" size-lg=\"4\">\n          <ion-item>\n            <ion-label position=\"floating\">District</ion-label>\n            <ion-input type=\"text\" value=\"{{district}}\" disabled=\"true\"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col size=\"12\" size-lg=\"4\">\n          <ion-item>\n            <ion-label position=\"floating\">State</ion-label>\n            <ion-input type=\"text\" value=\"{{state}}\" disabled=\"true\"></ion-input>\n          </ion-item >\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"4\">\n            <ion-item>\n              <ion-label position=\"floating\">Country</ion-label>\n              <ion-input type=\"text\" value=\"{{country}}\" disabled=\"true\"></ion-input>\n            </ion-item>\n          </ion-col>\n          <!-- <ion-col size=\"12\" size-lg=\"4\">\n            <ion-item>\n              <ion-label>From Date</ion-label>\n              <ion-datetime placeholder=\"Select Date\" [disabled]=\"true\"  formControlName=\"form.controls['fromdate']\" displayFormat=\"DD.MM.YYYY\" max=\"2050\"></ion-datetime>\n            </ion-item>\n          </ion-col> -->\n          <ion-item>\n            <ion-label position=\"stacked\">From Date</ion-label>\n            <ion-item>\n              <ion-input placeholder=\"Select Date\" [value]=\"datefrom\"></ion-input>\n              <ion-button fill=\"clear\" id=\"open-date-input-1\">\n                <ion-icon icon=\"calendar\"></ion-icon>\n              </ion-button>\n              <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\n                <ng-template>\n                  <ion-datetime\n                    #popoverDatetime2\n                    formControlName=\"fromdate\" \n                    presentation=\"date\"\n                    showDefaultButtons=\"true\"\n                    max=\"2050\"\n                    (ionChange)=\"datefrom = formatDate(popoverDatetime2.value)\"\n                  ></ion-datetime>\n                </ng-template>\n              </ion-popover>\n            </ion-item>\n          </ion-item>\n          <ion-col size=\"12\" size-lg=\"4\">\n            <ion-item>\n              <ion-label>Unregistered </ion-label>\n              <ion-checkbox slot=\"end\" formControlName=\"Unregistered\" (ionChange)=\"onchangeUnregistered()\"></ion-checkbox>\n            </ion-item>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"4\">\n            <ion-item>\n                <ion-label position=\"floating\">GST Number</ion-label>\n                <ion-input formControlName=\"gstno\" [required]=\"!IsUnregistered\" type=\"text\" [disabled]=\"isgst\" \n                 class=\"ion-text-uppercase\" pattern=\"^([0][1-9]|[1-2][0-9]|[3][0-5])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[a-zA-Z]{1}[0-9a-zA-Z]{1})+$\"></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.gstno\">\n                <div *ngIf=\"form.get('gstno').hasError(validation.type)\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <ion-col *ngIf=\"isdesktop===false\" size=\"12\" size-lg=\"4\" >\n            <ion-button (click)=\"ChosePic('PANpic','')\" [disabled]=\"isgst\">\n           <ion-icon name=\"Photos\"></ion-icon>\n              \n            </ion-button>\n           </ion-col>\n           <ion-col *ngIf=\"isdesktop===true\" size=\"12\" size-lg=\"4\">\n             <input type=\"file\" name=\"file\" accept=\"image/*\" id='selectedFile' (change)=\"uploadImage($event)\" class=\"inputfile\" [disabled]=\"isgst\"/>\n     \n            </ion-col>\n     \n           <ion-col>\n             <img [src]=\"fileUrl\" *ngIf=\"fileUrl\" (click)=\"ImageViewr(fileUrl)\" style=\"width: 50px; height: 50px;\">\n           </ion-col>   \n        </ion-row>\n      </ion-card>\n      <ion-card>\n        <ion-card-header style=\"background: #FFCC80;\">\n          Compliance:\n        </ion-card-header>\n        <ion-row style=\"background-color: #b8b3b3; white-space:normal; font-size: xx-small; text-align: center;\" class =\"row\">\n          <ion-col size=\"3\"> Compliance Type</ion-col>\n          <ion-col size=\"3\">Number</ion-col>\n          <ion-col size=\"2\"> Image</ion-col>\n          <ion-col size=\"3\"> </ion-col>\n        </ion-row>\n        <ion-row *ngFor=\"let item of ComplianceList; index as i\" style=\"border: think solid black;\">\n          <ion-col size=\"3\"> {{ item.name }}</ion-col>\n          <ion-item>\n          <ion-input size=\"3\" type=\"text\" [(ngModel)]=\"item.num\" [ngModelOptions]=\"{standalone: true}\"></ion-input>\n          </ion-item>\n          <ion-col size=\"2\" *ngIf=\"isdesktop===false\">\n            <ion-button (click)=\"ChosePic('complince',item.code)\">\n              <!-- <ion-label>Capture</ion-label> -->\n              <ion-icon name=\"Photos\"></ion-icon>\n            </ion-button>\n          </ion-col>\n  \n          <ion-col size=\"2\" *ngIf=\"isdesktop===true\">\n            <input type=\"file\" name=\"file\" accept=\"image/*\" id='selectedFile' (change)=\"uploadcompImage($event,item.code)\" class=\"inputfile\"/>\n        \n           </ion-col>\n          <ion-col size=\"3\">\n            <img [src]=\"'data:image/jpeg;base64,'+item.image\" (click)=\"ImageViewr(item.image,item)\" *ngIf=\"item.image\" style=\"width: 35px; height: 35px;\">\n          </ion-col>\n        </ion-row>\n        \n          <ion-row>\n            <ion-col>\n               <ion-label style=\"color: red;\" class=\"ion-text-wrap\"></ion-label>\n             </ion-col>\n          </ion-row>\n      </ion-card>\n    </ion-grid>\n  </form>\n  <ion-footer>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-button style=\"width: 100%;\" (click)=\"onSaveDraft(form.value)\" [disabled]=\"!form.valid\">\n          <ion-label >Save</ion-label>\n        </ion-button>\n        <!-- <ion-button style=\"width: 100%;\" (click)=\"test(form)\">\n          <ion-label >TEST</ion-label>\n        </ion-button> -->\n       </ion-col>\n  </ion-row>\n  </ion-footer>\n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_addedit-business-partner-address_addedit-business-partner-address_module_ts.js.map