"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_use-vetcoins_use-vetcoins_module_ts"],{

/***/ 87856:
/*!*****************************************************!*\
  !*** ./src/app/use-vetcoins/use-vetcoins.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UseVetcoinsPageModule": () => (/* binding */ UseVetcoinsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _use_vetcoins_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./use-vetcoins.page */ 70048);







const routes = [
    {
        path: '',
        component: _use_vetcoins_page__WEBPACK_IMPORTED_MODULE_0__.UseVetcoinsPage
    }
];
let UseVetcoinsPageModule = class UseVetcoinsPageModule {
};
UseVetcoinsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_use_vetcoins_page__WEBPACK_IMPORTED_MODULE_0__.UseVetcoinsPage]
    })
], UseVetcoinsPageModule);



/***/ }),

/***/ 70048:
/*!***************************************************!*\
  !*** ./src/app/use-vetcoins/use-vetcoins.page.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UseVetcoinsPage": () => (/* binding */ UseVetcoinsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _use_vetcoins_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./use-vetcoins.page.html?ngResource */ 47550);
/* harmony import */ var _use_vetcoins_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./use-vetcoins.page.scss?ngResource */ 12595);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @awesome-cordova-plugins/social-sharing/ngx */ 81952);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _provider_validator_helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../provider/validator-helper */ 35096);
/* harmony import */ var _use_vetcoins_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./use-vetcoins.service */ 6730);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);











let UseVetcoinsPage = class UseVetcoinsPage {
    constructor(loginauth, socialSharing, router, route, fb, val, usevetcoinsservice, commonfun) {
        this.loginauth = loginauth;
        this.socialSharing = socialSharing;
        this.router = router;
        this.route = route;
        this.fb = fb;
        this.val = val;
        this.usevetcoinsservice = usevetcoinsservice;
        this.commonfun = commonfun;
        this.validation_messages = {
            'mobileno': [
                { type: 'required', message: ' *Please Enter Mobile No.' },
            ]
        };
        try {
            this.formvet = this.fb.group({
                mobileno: [, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required, val.numberValid],
            });
            route.params.subscribe(val => {
                this.getparam();
                //this.formvet.controls["mobileno"].setValue("");
                this.formvet.reset();
                // this.formvet.controls["mobileno"].untouched=t
            });
        }
        catch (error) {
        }
    }
    ngOnInit() {
        this.Resetpage();
        this.getreferelcode();
    }
    getparam() {
        this.route.params.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                if (this.router.getCurrentNavigation().extras.state.reset) {
                    this.Resetpage();
                }
            }
        });
    }
    shareto() {
        // this is the complete list of currently supported params you can pass to the plugin (all optional)
        var options = {
            message: 'Please use my referral code ' + this.referalcode + ' to register on Pawtect (https://www.pawtectindia.com/pawtect/) and buy Pawtect plan (medical cover) for your pet. Contact : +917506562266 /+91 20 26633615   Email: customerservice@pawtectindia.com for any questions or help with registration. Thanks ' + this.loginauth.defaultprofile[0].name,
            subject: 'the subject',
            // files: ['', ''], // an array of filenames either locally or remotely
            url: 'https://www.website.com/foo/#bar?a=b',
            // chooserTitle: 'Pick an app', // Android only, you can override the default share sheet title
            // appPackageName: 'com.apple.social.facebook', // Android only, you can provide id of the App you want to share with
            // iPadCoordinates: '0,0,0,0' //IOS only iPadCoordinates for where the popover should be point.  Format with x,y,width,height
        };
        this.socialSharing.shareWithOptions(options);
    }
    getreferelcode() {
        try {
            this.usevetcoinsservice.getWMobileVetCoinCustDetailsapi("1", "N").subscribe(data => {
                if (data != null) {
                    if (data["referalcode"]) {
                        this.referalcode = data["referalcode"].referalcode;
                    }
                    else {
                        this.commonfun.presentAlert("Message", "Error", "Mobile No. is not valid.");
                    }
                }
                else {
                    this.commonfun.presentAlert("Message", "Error", "Mobile No. is not valid.");
                }
            }, error => {
                this.commonfun.presentAlert("Message", "Error", error.error.text);
            });
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    onCheckVetCoins() {
        try {
            if (this.loginauth.user != this.formvet.controls["mobileno"].value) {
                this.usevetcoinsservice.getWMobileVetCoinCustDetailsapi(this.formvet.controls["mobileno"].value, "N").subscribe(data => {
                    if (data != null) {
                        let navigationExtras = {
                            state: {
                                VetCoinCustDetails: data,
                                mobno: this.formvet.controls["mobileno"].value
                            }
                        };
                        this.router.navigate(['use-vetcoins-balance'], navigationExtras);
                    }
                    else {
                        this.commonfun.presentAlert("Message", "Error", "Mobile No. is not valid.");
                    }
                }, error => {
                    this.commonfun.presentAlert("Message", "Error!", error.error.text);
                });
            }
            else {
                this.commonfun.presentAlert("Message", "Error", "You can't use your own Mobile No.");
            }
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    Resetpage() {
        this.formvet.controls["mobileno"].setValue("");
        this.router.navigate(['use-vetcoins']);
    }
};
UseVetcoinsPage.ctorParameters = () => [
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService },
    { type: _awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_3__.SocialSharing },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.ActivatedRoute },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormBuilder },
    { type: _provider_validator_helper__WEBPACK_IMPORTED_MODULE_4__.Validator },
    { type: _use_vetcoins_service__WEBPACK_IMPORTED_MODULE_5__.UseVetcoinsService },
    { type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_6__.Commonfun }
];
UseVetcoinsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
        selector: 'app-use-vetcoins',
        template: _use_vetcoins_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_use_vetcoins_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], UseVetcoinsPage);



/***/ }),

/***/ 12595:
/*!****************************************************************!*\
  !*** ./src/app/use-vetcoins/use-vetcoins.page.scss?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = ".mycolor {\n  color: white !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVzZS12ZXRjb2lucy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx1QkFBQTtBQUNKIiwiZmlsZSI6InVzZS12ZXRjb2lucy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubXljb2xvcntcbiAgICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbn0iXX0= */";

/***/ }),

/***/ 47550:
/*!****************************************************************!*\
  !*** ./src/app/use-vetcoins/use-vetcoins.page.html?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar class=\"cssion-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: white;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Use Vetcoins\n    </ion-title>\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n  \n\n</ion-header>\n\n\n\n\n<ion-content>\n  <form [formGroup]=\"formvet\">\n\n  <ion-card style=\"text-align: center;\">\n    <ion-card-content>\n      <ion-row>\n        <ion-col>\n          <!-- <img src=\"http://placehold.it/300x200\" class=\"custom-avatar\"/> -->\n          <!-- <ion-icon name=\"cloud-circle\" style=\"color: #F39E20;\"></ion-icon> -->\n          <ion-label>\n            {{loginauth.defaultprofile[0].name}}\n          </ion-label>\n        </ion-col>\n      </ion-row>\n\n      <ion-row (click)=\"shareto()\">\n        <ion-col>\n         <ion-label>\n            Share your referral Code {{referalcode}}\n           \n            <ion-icon name=\"share\" style=\"font-size: x-large;\"></ion-icon>\n          \n          </ion-label>\n        </ion-col>\n      </ion-row>\n\n\n      <ion-row>\n        <ion-col>\n          <ion-item>\n            <ion-label position=\"stacked\">Enter customer mobile to redeem VetCoins in his/her account</ion-label>\n            <ion-input type=\"number\"  formControlName=\"mobileno\" style=\"padding-top: 21px!important;\"></ion-input>\n          </ion-item>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.mobileno\">\n              <div *ngIf=\"formvet.get('mobileno').hasError(validation.type) && formvet.get('mobileno').touched\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <div class=\"ion-padding\">\n            <ion-button color=\"orangevet\" expand=\"block\" text-center (click)=\"onCheckVetCoins()\" [disabled]=\"!formvet.valid\">Check VetCoins</ion-button>\n</div>\n          \n\n    </ion-card-content>\n  </ion-card>\n</form>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_use-vetcoins_use-vetcoins_module_ts.js.map