"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_reportcategory_reportcategory_module_ts"],{

/***/ 98648:
/*!*****************************************************************!*\
  !*** ./src/app/reportcategory/reportcategory-routing.module.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ReportcategoryPageRoutingModule": () => (/* binding */ ReportcategoryPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _reportcategory_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportcategory.page */ 92949);




const routes = [
    {
        path: '',
        component: _reportcategory_page__WEBPACK_IMPORTED_MODULE_0__.ReportcategoryPage
    }
];
let ReportcategoryPageRoutingModule = class ReportcategoryPageRoutingModule {
};
ReportcategoryPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ReportcategoryPageRoutingModule);



/***/ }),

/***/ 6067:
/*!*********************************************************!*\
  !*** ./src/app/reportcategory/reportcategory.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ReportcategoryPageModule": () => (/* binding */ ReportcategoryPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _reportcategory_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportcategory-routing.module */ 98648);
/* harmony import */ var _reportcategory_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportcategory.page */ 92949);







let ReportcategoryPageModule = class ReportcategoryPageModule {
};
ReportcategoryPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _reportcategory_routing_module__WEBPACK_IMPORTED_MODULE_0__.ReportcategoryPageRoutingModule
        ],
        declarations: [_reportcategory_page__WEBPACK_IMPORTED_MODULE_1__.ReportcategoryPage]
    })
], ReportcategoryPageModule);



/***/ }),

/***/ 92949:
/*!*******************************************************!*\
  !*** ./src/app/reportcategory/reportcategory.page.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ReportcategoryPage": () => (/* binding */ ReportcategoryPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _reportcategory_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportcategory.page.html?ngResource */ 64831);
/* harmony import */ var _reportcategory_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportcategory.page.scss?ngResource */ 96403);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ 80190);







let ReportcategoryPage = class ReportcategoryPage {
    constructor(loginservc, router, storage) {
        this.loginservc = loginservc;
        this.router = router;
        this.storage = storage;
        this.TAG = "Report Category Page";
    }
    ionViewWillEnter() {
        this.selectedReportcat = undefined;
        this.selectedSubCat = undefined;
        this.selectedreport = undefined;
        this.loginservc.getreportdet(this.loginservc.selectedprof.role, 'category', '', '').subscribe(data => {
            this.reportcatlist = data['data'];
            if (this.reportcatlist.length === 1) {
                this.selectedReportcat = this.reportcatlist[0];
            }
            // console.log(this.TAG,"getreportdet",this.reportcatlist);
        });
    }
    ngOnInit() {
        this.loginservc.getreportdet(this.loginservc.selectedprof.role, 'category', '', '').subscribe(data => {
            this.reportcatlist = data['data'];
            if (this.reportcatlist.length === 1) {
                this.selectedReportcat = this.reportcatlist[0];
            }
            // console.log(this.TAG,"getreportdet",this.reportcatlist);
        });
    }
    onChangeReportcat(reportcat) {
        this.selectedReportcat = reportcat;
        this.loginservc.getreportdet(this.loginservc.selectedprof.role, 'subcategory', this.selectedReportcat.id, '').subscribe(data => {
            this.reportsubcatlist = data['data'];
            if (this.reportsubcatlist.length === 1) {
                this.selectedSubCat = this.reportsubcatlist[0];
            }
            //console.log(this.TAG,"onChangeReportcat",this.reportsubcatlist);
            // this.selectedSubCat=undefined;
            // this.selectedreport=undefined;
        });
    }
    onChangeSubcat(reportsubcat) {
        this.selectedSubCat = reportsubcat;
        this.loginservc.getreportdet(this.loginservc.selectedprof.role, 'report', '', this.selectedSubCat.id).subscribe(data => {
            this.reportlist = data['data'];
            if (this.reportlist.length === 1) {
                this.selectedreport = this.reportlist[0];
            }
            //  console.log(this.TAG,"onChangeSubcat",this.reportlist);
        });
    }
    onChangeReport(report) {
        this.selectedreport = report;
        // console.log(this.TAG,"onChangeReport",this.selectedreport);
    }
    onClick() {
        this.loginservc.reportjson = {};
        this.loginservc.selectedreport = this.selectedreport;
        this.router.navigate(['/report', this.selectedreport.id]);
    }
};
ReportcategoryPage.ctorParameters = () => [
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.Router },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__.Storage }
];
ReportcategoryPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-reportcategory',
        template: _reportcategory_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_reportcategory_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ReportcategoryPage);



/***/ }),

/***/ 96403:
/*!********************************************************************!*\
  !*** ./src/app/reportcategory/reportcategory.page.scss?ngResource ***!
  \********************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXBvcnRjYXRlZ29yeS5wYWdlLnNjc3MifQ== */";

/***/ }),

/***/ 64831:
/*!********************************************************************!*\
  !*** ./src/app/reportcategory/reportcategory.page.html?ngResource ***!
  \********************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Report Category</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"profile\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n     \n          <ion-list>\n              <ion-item>\n                <ion-label color=\"primary\" position=\"stacked\">Report Category</ion-label>\n                <ion-select #C [ngModel]=\"selectedReportcat\" (ionChange)=\"onChangeReportcat(C.value)\" multiple=\"false\" placeholder=\"Select Report Category\" interface=\"popover\">\n                  <ion-select-option *ngFor=\"let rptcat of reportcatlist\" [value]=\"rptcat\">{{rptcat._identifier}}</ion-select-option>\n                </ion-select>\n              </ion-item>\n              <ion-item>\n                  <ion-label color=\"primary\" position=\"stacked\">Sub Category</ion-label>\n                  <ion-select #F [ngModel]=\"selectedSubCat\" (ionChange)=\"onChangeSubcat(F.value)\" multiple=\"false\" placeholder=\"Select Sub Category\" interface=\"popover\">\n                    <ion-select-option *ngFor=\"let sucat of reportsubcatlist\" [value]=\"sucat\">{{sucat._identifier}}</ion-select-option>\n                  </ion-select>\n                </ion-item>\n              <ion-item>\n                  <ion-label color=\"primary\" position=\"stacked\">Report</ion-label>\n                  <ion-select class=\"ion-text-wrap select-text\" #W [ngModel]=\"selectedreport\" (ionChange)=\"onChangeReport(W.value)\" multiple=\"false\" placeholder=\"Select Report\" interface=\"popover\">\n                    <ion-select-option *ngFor=\"let report of reportlist\" [value]=\"report\">{{report._identifier}}</ion-select-option>\n                  </ion-select>\n              </ion-item>\n            </ion-list>\n        \n        <ion-row>\n          <ion-col class=\"ion-text-right\">\n                  <ion-button (click)=\"onClick()\">\n                        Apply\n                    </ion-button>\n          </ion-col>\n        </ion-row>\n       \n</ion-content>\n\n";

/***/ })

}]);
//# sourceMappingURL=src_app_reportcategory_reportcategory_module_ts.js.map