"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_settings_settings_module_ts"],{

/***/ 27075:
/*!*********************************************!*\
  !*** ./src/app/settings/settings.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SettingsPageModule": () => (/* binding */ SettingsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _settings_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settings.page */ 7162);







const routes = [
    {
        path: '',
        component: _settings_page__WEBPACK_IMPORTED_MODULE_0__.SettingsPage
    }
];
let SettingsPageModule = class SettingsPageModule {
};
SettingsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_settings_page__WEBPACK_IMPORTED_MODULE_0__.SettingsPage]
    })
], SettingsPageModule);



/***/ }),

/***/ 7162:
/*!*******************************************!*\
  !*** ./src/app/settings/settings.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SettingsPage": () => (/* binding */ SettingsPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _settings_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settings.page.html?ngResource */ 75375);
/* harmony import */ var _settings_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./settings.page.scss?ngResource */ 2282);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ 80190);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);








let SettingsPage = class SettingsPage {
  constructor(storage, alertController, router) {
    this.storage = storage;
    this.alertController = alertController;
    this.router = router;
    this.TAG = "SettingsPage";
    this.showBiometericControl = false;
    this.showPinCodeControl = false;
    this.showFingerprintButton = false;
  }

  ngOnInit() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.biometricstatus = yield _this.storage.get('Biometric_Status');
      _this.bio_Codes = yield _this.storage.get('Biometric_Device_Codes');
      _this.pinCodes = yield _this.storage.get('PIN_Status');

      if (_this.bio_Codes == '200') {
        _this.showFingerprintButton = true;
        _this.showPinCodeControl = false;
      } else if (_this.pinCodes == '200') {
        _this.showPinCodeControl = true;
        _this.showFingerprintButton = false;
      }

      if (_this.bio_Codes == '-106' && _this.pinCodes != '200') {
        _this.showFingerprintButton = true;
        _this.showPinCodeControl = false;
      }

      if (_this.bio_Codes == '200' && _this.pinCodes == '200') {
        _this.showFingerprintButton = true;
        _this.showPinCodeControl = false;
      }
    })();
  }

  biometricStatusChange(event) {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (event.detail.checked && _this2.bio_Codes == '-106') {
        const alert = yield _this2.alertController.create({
          header: 'Setting',
          subHeader: 'Biometric',
          message: 'You have not added fingerprint',
          buttons: ['OK']
        });
        yield alert.present();
        let result = yield alert.onDidDismiss();
        _this2.biometricstatus = false;
      } else {
        _this2.storage.set('Biometric_Status', event.detail.checked);
      }
    })();
  }

  deviceAuthenticationStatusChange(event) {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this3.storage.set('Biometric_Status', event.detail.checked);
    })();
  }

  onChangePassFromSetting() {
    try {
      this.storage.set('changePasswordFromSettingPage', true);
      this.router.navigateByUrl('/login');
    } catch (error) {}
  }

};

SettingsPage.ctorParameters = () => [{
  type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__.Storage
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.AlertController
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router
}];

SettingsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
  selector: 'app-settings',
  template: _settings_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_settings_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], SettingsPage);


/***/ }),

/***/ 2282:
/*!********************************************************!*\
  !*** ./src/app/settings/settings.page.scss?ngResource ***!
  \********************************************************/
/***/ ((module) => {

module.exports = "ion-toggle {\n  --background-checked:#4db6ac;\n}\n\n.server-item {\n  --border-width: 1px !important;\n  margin-bottom: 25px;\n}\n\n.server-card {\n  padding: 2%;\n}\n\nion-button {\n  --ion-color-contrast:white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNldHRpbmdzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLDRCQUFBO0FBQUo7O0FBS0U7RUFDRSw4QkFBQTtFQUNBLG1CQUFBO0FBRko7O0FBSUE7RUFDSSxXQUFBO0FBREo7O0FBR0E7RUFDSSwwQkFBQTtBQUFKIiwiZmlsZSI6InNldHRpbmdzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b2dnbGUge1xuICAgIC8vLS1iYWNrZ3JvdW5kOiAjMDAwO1xuICAgIC0tYmFja2dyb3VuZC1jaGVja2VkOiM0ZGI2YWM7XG4gIFxuICAgIC8vLS1oYW5kbGUtYmFja2dyb3VuZDogIzRkYjZhYztcbiAgIC8vIC0taGFuZGxlLWJhY2tncm91bmQtY2hlY2tlZDogIzAwMDtcbiAgfVxuICAuc2VydmVyLWl0ZW0ge1xuICAgIC0tYm9yZGVyLXdpZHRoOiAxcHggIWltcG9ydGFudDtcbiAgICBtYXJnaW4tYm90dG9tOiAyNXB4O1xufVxuLnNlcnZlci1jYXJke1xuICAgIHBhZGRpbmc6IDIlO1xufVxuaW9uLWJ1dHRvbiB7XG4gICAgLS1pb24tY29sb3ItY29udHJhc3Q6d2hpdGU7XG4gIH0iXX0= */";

/***/ }),

/***/ 75375:
/*!********************************************************!*\
  !*** ./src/app/settings/settings.page.html?ngResource ***!
  \********************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Settings\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col text-center>\n        <img style=\"width: 400px;height: 400px;\" src=\"./assets/settings.png\">\n      </ion-col>\n    </ion-row>\n    \n    <ion-row>\n        <ion-col>\n          <ion-card class=\"server-card\" *ngIf=\"showFingerprintButton\">\n           <ion-item lines=\"none\"> \n              <ion-label>Enable Fingerprint</ion-label>\n                 <ion-toggle  mode=\"ios\" [(ngModel)]=\"biometricstatus\" [checked]=\"biometricstatus\" (ionChange)=\"biometricStatusChange($event)\"></ion-toggle>\n            </ion-item>\n          </ion-card>\n         \n          <ion-card class=\"server-card\" *ngIf=\"showPinCodeControl\"> \n            <ion-item lines=\"none\"> \n               <ion-label>Use Device Authentication</ion-label>\n                  <ion-toggle  mode=\"ios\" [(ngModel)]=\"biometricstatus\" [checked]=\"biometricstatus\" (ionChange)=\"deviceAuthenticationStatusChange($event)\"></ion-toggle>\n             </ion-item>\n           </ion-card>\n           <ion-card class=\"server-card\"> \n            <ion-item lines=\"none\"> \n                <ion-button  style=\"width: -webkit-fill-available;\"  (click)=\"onChangePassFromSetting()\" color=\"bluegrey\" size=\"large\" expand=\"block\">Change Password</ion-button>\n              </ion-item>\n           </ion-card>\n\n          <!-- <ion-card class=\"server-card\">\n            <ion-item lines=\"none\">\n              <ion-label>Change APP Server URL</ion-label>\n            </ion-item>\n            <ion-item class=\"server-item\"> \n              <ion-input id=\"txtServerUrl\" name=\"username\" type=\"text\" placeholder=\"URL\"  [(ngModel)] = \"serverurl\" maxlength = \"80\" required></ion-input>\n            </ion-item>\n            <ion-button strong=\"true\" color=\"teal\" expand=\"block\">Apply</ion-button>\n          </ion-card>\n           -->\n          \n\n\n          \n        </ion-col>\n       \n     </ion-row>\n     \n\n  </ion-grid>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_settings_settings_module_ts.js.map