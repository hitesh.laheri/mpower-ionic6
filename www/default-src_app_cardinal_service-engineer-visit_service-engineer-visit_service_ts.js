"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["default-src_app_cardinal_service-engineer-visit_service-engineer-visit_service_ts"],{

/***/ 39797:
/*!***********************************************************************************!*\
  !*** ./src/app/cardinal/service-engineer-visit/service-engineer-visit.service.ts ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ServiceEngineerVisitService": () => (/* binding */ ServiceEngineerVisitService)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/internal/Observable */ 84758);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/common/Constants */ 68209);
/* harmony import */ var src_app_common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/common/generic-http-client.service */ 28475);
/* harmony import */ var src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/login/loginauth.service */ 44010);
/* harmony import */ var src_provider_message_helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/provider/message-helper */ 98792);
/* harmony import */ var _awesome_cordova_plugins_http_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @awesome-cordova-plugins/http/ngx */ 26123);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @awesome-cordova-plugins/file-transfer/ngx */ 80464);












let ServiceEngineerVisitService = class ServiceEngineerVisitService {
  constructor(genericHTTP, loginService, httpClient, msg, cordovaHTTP, platform, transfer) {
    this.genericHTTP = genericHTTP;
    this.loginService = loginService;
    this.httpClient = httpClient;
    this.msg = msg;
    this.cordovaHTTP = cordovaHTTP;
    this.platform = platform;
    this.transfer = transfer;
    this.TAG = "ServiceEngineerVisitService";
  }

  getVenderApprovalList() {
    try {
      let complainListURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerServiceDetails?' + 'userid=' + this.loginService.userid + '&appcomplaint=' + 'N' + '&servmanager=' + 'N' + '&serveng=' + 'Y' + '&sparesku=' + 'Y' + '&activity=' + this.loginService.selectedactivity.id; // let complainListURL = "https://p2test.pispl.in/openbravo/ws/in.mbs.webservice.CustomerServiceDetails?user=ps.medical&password=pass&userid=FFF20210114113527411DBCE268A3D75&appcomplaint=N&servmanager=N&serveng=Y&sparesku=Y&activity=FFF202012061211195489D3E4DD35FC1"
      //  console.log("getComplaintList",complainListURL);

      return this.genericHTTP.get(complainListURL); //return this.httpClient.get('assets/data/complain.json');
    } catch (error) {
      console.error(this.TAG, error);
    }
  }

  getServiceManagerList() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let serviceManagerListURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.BusinessPartnerAsPerCategory?' + 'activityid=' + _this.loginService.selectedactivity.id + '&c_bgroup_id=' + _this.loginService.service_manager_id;

        if (!!_this.serviceManagerList) {
          //  console.log(this.TAG,"Not Empty");
          return rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_7__.Observable.create(observer => {
            observer.next(_this.serviceManagerList);
            observer.complete();
          });
        } else {
          //  console.log(this.TAG,"Empty");
          _this.serviceManagerList = yield _this.genericHTTP.get(serviceManagerListURL).toPromise(); //  console.log(this.TAG,"Empty",this.serviceManagerList);

          return rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_7__.Observable.create(observer => {
            observer.next(_this.serviceManagerList);
            observer.complete();
          });
        }
      } catch (error) {// console.log(this.TAG,error);
      }
    })();
  }

  getSpareSKUCode() {
    try {
      return this.genericHTTP.get('assets/data/skuCode.json');
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  getProposedActionList() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let getProposedActionListURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.ListOfvaluesReference?' + 'user_id=' + _this2.loginService.userid + '&refname=' + 'MSNR%20Proposed%20Action';

        if (!!_this2.proposedActionList) {
          //  console.log(this.TAG,"Not Empty");
          return rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_7__.Observable.create(observer => {
            observer.next(_this2.proposedActionList);
            observer.complete();
          });
        } else {
          //  console.log(this.TAG,"Empty");
          _this2.proposedActionList = yield _this2.genericHTTP.get(getProposedActionListURL).toPromise(); // console.log(this.TAG,"Empty",this.proposedActionList);

          return rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_7__.Observable.create(observer => {
            observer.next(_this2.proposedActionList);
            observer.complete();
          });
        }
      } catch (error) {// console.log(this.TAG,error);
      }
    })();
  }

  getComplaintStatus() {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let getProposedActionListURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.ListOfvaluesReference?' + 'refname=' + 'MSNR%20Status';

        if (!!_this3.complaintStatusList) {
          //  console.log(this.TAG,"Not Empty");
          return rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_7__.Observable.create(observer => {
            observer.next(_this3.complaintStatusList);
            observer.complete();
          });
        } else {
          // console.log(this.TAG,"Empty");
          _this3.complaintStatusList = yield _this3.genericHTTP.get(getProposedActionListURL).toPromise(); // console.log(this.TAG,"Empty",this.complaintStatusList);

          return rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_7__.Observable.create(observer => {
            observer.next(_this3.complaintStatusList);
            observer.complete();
          });
        }
      } catch (error) {//  console.log(this.TAG,error);
      }
    })();
  }

  punchedCOPSalesOrderPost(spareObject) {
    try {
      let login = this.loginService.user;
      let password = this.loginService.pass;
      const auth = btoa(login + ":" + password);
      const httpOptions = {
        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__.HttpHeaders({
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Basic ' + auth
        })
      };
      console.log(this.TAG, "punched COP Sales Order Post FINAL", spareObject);
      let punchedOrder_url = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerServiceInsert?';
      return this.genericHTTP.post(punchedOrder_url, spareObject, httpOptions);
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  getAddress(name) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        return _this4.genericHTTP.get(_this4.loginService.commonurl + 'org.openbravo.service.json.jsonrest/BusinessPartner?' + _this4.loginService.ReadOnlyparameter + '&' + '_selectedProperties=id,name,shipToAddress,invoiceToAddress&' + '_where=active=true%20and%20name=\'' + name + '\'');
      } catch (error) {//  console.log(this.TAG,error);
      }
    })();
  }

  finalCloser(complain) {
    try {
      let login = this.loginService.user;
      let password = this.loginService.pass;
      const auth = btoa(login + ":" + password);
      const httpOptions = {
        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__.HttpHeaders({
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Basic ' + auth
        })
      };
      let oneObject = {
        'complaintno': complain.complaint_no,
        'complaintid': complain.complaintid,
        'doctype': complain.doctype,
        'nameofcomplainer': complain.nameofcomplainer,
        'desofcomplnr': complain.desofcomplnr,
        'contnumber': complain.contnumber,
        'email': complain.email,
        'eventdate': complain.eventdate,
        'serialno': complain.serialno,
        "srnoequipment": complain.srnoequipment,
        "contracttype": complain.contracttype,
        'invoiceno': complain.invoiceno,
        "invoicedate": complain.invoicedate,
        "errorcode": complain.errorcode,
        "dealername": complain.dealername,
        "installationdate": complain.installationdate,
        "skucode": complain.skucode,
        "skuname": complain.skuname,
        "brandname": complain.brandname,
        "producttobereturn": complain.producttobereturn,
        "custname": complain.custname,
        "add1": complain.add1,
        "add2": complain.add2,
        "add3": complain.add3,
        "pincode": complain.pincode,
        "area": complain.area,
        "city": complain.city,
        "state": complain.state,
        "country": complain.country,
        "description": complain.description,
        "lotno": complain.lotno ? complain.lotno : '',
        "appcomplaint": complain.appcomplaint,
        "assigntoservvendor": complain.assigntoservvendor,
        "salesrepresentative": complain.salesrepresentative,
        "newdealername": complain.newdealername,
        "serviceengname": complain.serviceengname,
        "serviceengcontact": complain.serviceengcontact,
        "visitdate": complain.visitdate,
        //  "servicevendorremark":complain.servicevendorremark,
        "assigntoservmg": complain.assigntoservmg,
        "activity": this.loginService.selectedactivity.id,
        "Appect": complain.Appect,
        "problemobserv": complain.problem_observed,
        "fieldvisit": complain.field_visit_remarks,
        "proposeactn": complain.proposed_action,
        "assigntofieldvisit": complain.assign_to,
        "closureatfield": complain.closureatfield,
        "compltstatus": complain.complaint_status,
        "imagebase64": complain.imagebase64,
        "file_type": complain.file_type
      };

      if (!!complain.spare_received_date) {
        oneObject.sparesinstall = [{
          "recvdate": complain.spare_received_date,
          "repairreport": complain.repair_report,
          "compltstatus": complain.complaint_status,
          "compltndate": complain.complaint_date,
          "replacesparepartno": complain.replaced_spare_part_serialNo,
          "serviceattendedby": complain.service_attended,
          "defectivepartno": complain.defective_spare_part_no,
          "docketno": complain.def_spare_docket_no,
          "courier": complain.def_spare_courier,
          "sentdate": complain.def_spare_sent_date,
          "spareinstallclose": complain.spare_install_closed
        }];
      }

      if (!!complain.servicevendorremark) {
        oneObject.needtosendcah = [{
          "sermangremarks": complain.servicevendorremark,
          "defsparepartno": complain.defective_spare_part_no,
          "defsparerecevdate": complain.defective_spare_part_received_date,
          "refno": complain.smart_solve_ref_no
        }];
      }

      console.log(this.TAG, "FINAL Closure API Data Service", oneObject);
      let complain_url = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerServiceInsert?'; //return  this.genericHTTP.post(complain_url,oneObject,httpOptions);

      if (complain.file_type == "image") {
        return this.genericHTTP.post(complain_url, oneObject, httpOptions);
      } else if (complain.file_type == "pdf") {
        if (this.msg.isplatformweb == true) {
          let formData = new FormData();
          let postData = JSON.stringify(oneObject);
          formData.append("postData", postData); // formData.append('complaintid',complain.complaintid)
          // formData.append('compltstatus',complain.complaint_status)

          formData.append('imagebase64', oneObject.imagebase64, oneObject.imagebase64.name);
          formData.append('file_type', complain.file_type);
          let login = this.loginService.user;
          let password = this.loginService.pass;
          const auth = btoa(login + ":" + password);
          const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__.HttpHeaders({
              'Authorization': 'Basic ' + auth
            })
          };
          console.log(this.TAG, "final Closer With Pdf Final Data", formData);
          let save_quotation = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerServiceInsert?';
          return this.genericHTTP.fileDevicePost(save_quotation, formData, httpOptions);
        } else if (this.platform.is('android')) {
          this.uploadPDFFileServiceAndroidiOS(oneObject, complain.file_selected_uri).then(data => {});
        } else if (this.platform.is('ios')) {
          return this.uploadPDFFileiOS(oneObject, complain.file_selected_uri);
        }
      }
    } catch (error) {// console.log(this.TAG,error)
    }
  }

  finalCloserWithPdf(data) {
    try {
      let login = this.loginService.user;
      let password = this.loginService.pass;
      const auth = btoa(login + ":" + password);
      const httpOptions = {
        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__.HttpHeaders({
          'Authorization': 'Basic ' + auth
        })
      };
      console.log(this.TAG, "final Closer With Pdf Final Data", data);
      let save_quotation = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerServiceInsert?';
      return this.genericHTTP.fileDevicePost(save_quotation, data, httpOptions);
    } catch (error) {}
  }

  uploadPDFFileServiceAndroidiOS(data, path) {
    try {
      let login = this.loginService.user;
      let password = this.loginService.pass;
      this.cordovaHTTP.setDataSerializer('multipart');
      const filePath = [path];
      const auth = btoa(login + ":" + password);
      const httpOptions = {
        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__.HttpHeaders({
          'Authorization': 'Basic ' + auth
        })
      };
      let auth1 = httpOptions.headers.get('Authorization');
      let specificHeader = {
        'Authorization': auth1
      };
      console.log(this.TAG, "file upload data", data);
      let save_file_url = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerServiceInsert?';
      return rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_7__.Observable.create(observer => {
        this.cordovaHTTP.uploadFile(save_file_url, data, specificHeader, filePath, "imagebase64").then(response => {
          let data;

          if (!!response.data) {
            data = JSON.parse(response.data);
          } else {
            data = response;
          }

          observer.next(data);
          observer.complete();
        }).catch(error => {
          // this.commonFunction.loadingDismiss();
          console.log(this.TAG, "file upload error", error);
          throw error; //  this.commonFunction.loadingDismiss();
          //  console.log(this.TAG, "file upload error", error);
        });
      });
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  uploadPDFFileiOS(jsondata, selectedURI) {
    try {
      let login = this.loginService.user;
      let password = this.loginService.pass;
      const auth = btoa(login + ":" + password);
      let save_file_url = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.WMobileOrderPunchOB?';
      const fileTransfer = this.transfer.create();
      let options = {
        fileKey: 'imagebase64',
        fileName: jsondata.fileName,
        params: jsondata,
        headers: {
          'Authorization': 'Basic ' + auth
        }
      };
      fileTransfer.upload(selectedURI, save_file_url, options).then(data => {
        console.log("pravin YESSSSS", data);
        let formatResponse = JSON.parse(data.response);
        console.log("File Uplaod Result", formatResponse);

        if (data != null) {}
      }, err => {});
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  serviceManagerClose(complain) {
    try {
      let login = this.loginService.user;
      let password = this.loginService.pass;
      const auth = btoa(login + ":" + password);
      const httpOptions = {
        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__.HttpHeaders({
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Basic ' + auth
        })
      };
      let oneObject = {
        'complaintno': complain.complaint_no,
        'complaintid': complain.complaintid,
        'doctype': complain.doctype,
        'nameofcomplainer': complain.nameofcomplainer,
        'desofcomplnr': complain.desofcomplnr,
        'contnumber': complain.contnumber,
        'email': complain.email,
        'eventdate': complain.eventdate,
        'serialno': complain.serialno,
        "srnoequipment": complain.srnoequipment,
        "contracttype": complain.contracttype,
        'invoiceno': complain.invoiceno,
        "invoicedate": complain.invoicedate,
        "errorcode": complain.errorcode,
        "dealername": complain.dealername,
        "installationdate": complain.installationdate,
        "skucode": complain.skucode,
        "skuname": complain.skuname,
        "brandname": complain.brandname,
        "producttobereturn": complain.producttobereturn,
        "custname": complain.custname,
        "add1": complain.add1,
        "add2": complain.add2,
        "add3": complain.add3,
        "pincode": complain.pincode,
        "area": complain.area,
        "city": complain.city,
        "state": complain.state,
        "country": complain.country,
        "description": complain.description,
        "lotno": complain.lotno ? complain.lotno : '',
        "appcomplaint": complain.appcomplaint,
        "assigntoservvendor": complain.assigntoservvendor,
        "salesrepresentative": complain.salesrepresentative,
        "newdealername": complain.newdealername,
        "serviceengname": complain.serviceengname,
        "serviceengcontact": complain.serviceengcontact,
        "visitdate": complain.visitdate,
        "servicevendorremark": complain.servicevendorremark,
        "assigntoservmg": complain.assigntoservmg,
        "activity": this.loginService.selectedactivity.id,
        "Appect": complain.Appect,
        "problemobserv": complain.problem_observed,
        "fieldvisit": complain.field_visit_remarks,
        "assigntofieldvisit": complain.assign_to
      };

      if (!!complain.servicevendorremark) {
        oneObject.needtosendcah = [{
          "sermangremarks": complain.servicevendorremark,
          "defsparepartno": complain.defective_spare_part_no,
          "defsparerecevdate": complain.defective_spare_part_received_date,
          "refno": complain.smart_solve_ref_no
        }];
      } //  console.log(this.TAG,"FINAL Closure API Data",oneObject);


      let complain_url = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerServiceInsert?';
      return this.genericHTTP.post(complain_url, oneObject, httpOptions);
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  spareInstallSaveToOB(spareComplain) {
    try {
      let login = this.loginService.user;
      let password = this.loginService.pass;
      const auth = btoa(login + ":" + password);
      const httpOptions = {
        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__.HttpHeaders({
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Basic ' + auth
        })
      };
      let spareInstallObject = {
        "complaintno": spareComplain.complaint_no,
        "complaintid": spareComplain.complaintid,
        "activity": this.loginService.selectedactivity.id,
        "problemobserv": spareComplain.problem_observed,
        "fieldvisit": spareComplain.field_visit_remarks,
        "proposeactn": spareComplain.proposed_action
      };
      spareInstallObject.sparesinstall = [{
        "recvdate": spareComplain.spare_received_date,
        "repairreport": spareComplain.repair_report,
        "compltstatus": spareComplain.complaint_status,
        "compltndate": spareComplain.complaint_date,
        "replacesparepartno": spareComplain.replaced_spare_part_serialNo,
        "serviceattendedby": spareComplain.service_attended,
        "defectivepartno": spareComplain.defective_spare_part_no,
        "docketno": spareComplain.def_spare_docket_no,
        "courier": spareComplain.def_spare_courier,
        "sentdate": spareComplain.def_spare_sent_date,
        "spareinstallclose": spareComplain.spare_install_closed,
        "orderid": spareComplain.orderid,
        "sapreskuid": spareComplain.sapreskuid,
        "qty": spareComplain.qty,
        "sparerequiredid": spareComplain.sparerequiredid
      }];
      console.log(this.TAG, "FINAL Spare Install API Data", spareInstallObject);
      let complain_url = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerServiceInsert?';
      return this.genericHTTP.post(complain_url, spareInstallObject, httpOptions);
    } catch (error) {
      //  console.log(this.TAG,error);
      throw error;
    }
  }

  getErrorCodeList() {
    var _this5 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let errorCodeURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.ListOfvalues?' + 'user_id=' + _this5.loginService.userid + '&type=EC';

        if (!!_this5.errorCodeList) {
          // console.log(this.TAG,"Not Empty");
          return rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_7__.Observable.create(observer => {
            observer.next(_this5.errorCodeList);
            observer.complete();
          });
        } else {
          // console.log(this.TAG,"Empty");
          _this5.errorCodeList = yield _this5.genericHTTP.get(errorCodeURL).toPromise(); //  console.log(this.TAG,"Empty",this.errorCodeList);

          return rxjs_internal_Observable__WEBPACK_IMPORTED_MODULE_7__.Observable.create(observer => {
            observer.next(_this5.errorCodeList);
            observer.complete();
          });
        }
      } catch (error) {
        console.error(_this5.TAG, error);
      }
    })();
  }

  loadMasterData() {
    var _this6 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this6.proposedActionList = yield (yield _this6.getProposedActionList()).toPromise();
        _this6.complaintStatusList = yield (yield _this6.getComplaintStatus()).toPromise();
        _this6.errorCodeList = yield (yield _this6.getErrorCodeList()).toPromise();
        _this6.serviceManagerList = yield (yield _this6.getServiceManagerList()).toPromise();
      } catch (error) {
        console.log(_this6.TAG, error);
      }
    })();
  }

  getCustomerServiceDetailById(complain_id) {
    try {
      let complainDetailByIdURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerServiceDetails?' + 'userid=' + this.loginService.userid + '&appcomplaint=' + 'N' + '&servmanager=' + 'N' + '&serveng=' + 'Y' + '&sparesku=' + 'Y' + '&activity=' + this.loginService.selectedactivity.id + '&complaint_id=' + complain_id;
      return this.genericHTTP.get(complainDetailByIdURL);
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

};

ServiceEngineerVisitService.ctorParameters = () => [{
  type: src_app_common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_2__.GenericHttpClientService
}, {
  type: src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__.LoginauthService
}, {
  type: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__.HttpClient
}, {
  type: src_provider_message_helper__WEBPACK_IMPORTED_MODULE_4__.Message
}, {
  type: _awesome_cordova_plugins_http_ngx__WEBPACK_IMPORTED_MODULE_5__.HTTP
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.Platform
}, {
  type: _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_6__.FileTransfer
}];

ServiceEngineerVisitService = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.Injectable)({
  providedIn: 'root'
})], ServiceEngineerVisitService);


/***/ })

}]);
//# sourceMappingURL=default-src_app_cardinal_service-engineer-visit_service-engineer-visit_service_ts.js.map