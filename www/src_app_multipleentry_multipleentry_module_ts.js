"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_multipleentry_multipleentry_module_ts"],{

/***/ 61958:
/*!***************************************************************!*\
  !*** ./src/app/multipleentry/multipleentry-routing.module.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MultipleentryPageRoutingModule": () => (/* binding */ MultipleentryPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _multipleentry_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./multipleentry.page */ 89192);




const routes = [
    {
        path: '',
        component: _multipleentry_page__WEBPACK_IMPORTED_MODULE_0__.MultipleentryPage
    }
];
let MultipleentryPageRoutingModule = class MultipleentryPageRoutingModule {
};
MultipleentryPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], MultipleentryPageRoutingModule);



/***/ }),

/***/ 96930:
/*!*******************************************************!*\
  !*** ./src/app/multipleentry/multipleentry.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MultipleentryPageModule": () => (/* binding */ MultipleentryPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _multipleentry_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./multipleentry-routing.module */ 61958);
/* harmony import */ var _multipleentry_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./multipleentry.page */ 89192);







let MultipleentryPageModule = class MultipleentryPageModule {
};
MultipleentryPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule,
            _multipleentry_routing_module__WEBPACK_IMPORTED_MODULE_0__.MultipleentryPageRoutingModule
        ],
        declarations: [_multipleentry_page__WEBPACK_IMPORTED_MODULE_1__.MultipleentryPage]
    })
], MultipleentryPageModule);



/***/ }),

/***/ 89192:
/*!*****************************************************!*\
  !*** ./src/app/multipleentry/multipleentry.page.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MultipleentryPage": () => (/* binding */ MultipleentryPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _multipleentry_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./multipleentry.page.html?ngResource */ 42672);
/* harmony import */ var _multipleentry_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./multipleentry.page.scss?ngResource */ 83782);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);






let MultipleentryPage = class MultipleentryPage {
    constructor(formBuilder, route, router) {
        this.formBuilder = formBuilder;
        this.route = route;
        this.router = router;
        this.noofcontrol = 1;
        this.myForm = formBuilder.group({
            control1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.required]
        });
    }
    ngOnInit() {
        this.route.params.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.question = this.router.getCurrentNavigation().extras.state.question;
            }
        });
    }
    addControl() {
        this.noofcontrol = this.noofcontrol + 1;
        this.myForm.addControl('control' + this.noofcontrol, new _angular_forms__WEBPACK_IMPORTED_MODULE_2__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.required));
    }
    removeControl(control) {
        this.myForm.removeControl(control.key);
        this.noofcontrol = this.noofcontrol - 1;
    }
    onSubmit() {
        let finalvalue = '';
        Object.keys(this.myForm.controls).forEach(key => {
            finalvalue = finalvalue + ',' + this.myForm.get(key).value;
        });
        this.question.ans = finalvalue.substring(1);
        let navigationExtras = {
            state: {
                question: this.question
            }
        };
        this.router.navigate(['section'], navigationExtras);
    }
};
MultipleentryPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__.FormBuilder },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.ActivatedRoute },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router }
];
MultipleentryPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-multipleentry',
        template: _multipleentry_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_multipleentry_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], MultipleentryPage);



/***/ }),

/***/ 83782:
/*!******************************************************************!*\
  !*** ./src/app/multipleentry/multipleentry.page.scss?ngResource ***!
  \******************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJtdWx0aXBsZWVudHJ5LnBhZ2Uuc2NzcyJ9 */";

/***/ }),

/***/ 42672:
/*!******************************************************************!*\
  !*** ./src/app/multipleentry/multipleentry.page.html?ngResource ***!
  \******************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Multiple Entry</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"section\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]=\"myForm\">\n    <ion-item *ngFor=\"let control of myForm.controls | keyvalue\">\n      <ion-input\n        required\n        type=\"text\"\n        [formControlName]=\"control.key\"\n        placeHolder=\"Enter Value...\"\n      ></ion-input>\n      <ion-icon (click)=\"removeControl(control)\" name=\"close-circle\"></ion-icon>\n    </ion-item>\n  </form>\n  <ion-button expand=\"full\" color=\"primary\" (click)=\"addControl()\">Add Input</ion-button>\n  <ion-button expand=\"full\" color=\"primary\" [disabled]=\"!myForm.valid\" (click)=\"onSubmit()\">Submit</ion-button>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_multipleentry_multipleentry_module_ts.js.map