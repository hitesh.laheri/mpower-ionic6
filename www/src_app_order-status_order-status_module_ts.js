"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_order-status_order-status_module_ts"],{

/***/ 73758:
/*!*****************************************************!*\
  !*** ./src/app/order-status/order-status.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OrderStatusPageModule": () => (/* binding */ OrderStatusPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _order_status_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./order-status.page */ 85207);








const routes = [
    {
        path: '',
        component: _order_status_page__WEBPACK_IMPORTED_MODULE_0__.OrderStatusPage
    }
];
let OrderStatusPageModule = class OrderStatusPageModule {
};
OrderStatusPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule, ionic_selectable__WEBPACK_IMPORTED_MODULE_6__.IonicSelectableModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterModule.forChild(routes)
        ],
        declarations: [_order_status_page__WEBPACK_IMPORTED_MODULE_0__.OrderStatusPage]
    })
], OrderStatusPageModule);



/***/ }),

/***/ 85207:
/*!***************************************************!*\
  !*** ./src/app/order-status/order-status.page.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OrderStatusPage": () => (/* binding */ OrderStatusPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _order_status_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./order-status.page.html?ngResource */ 36227);
/* harmony import */ var _order_status_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./order-status.page.scss?ngResource */ 55608);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../neworder/neworder.service */ 17216);
/* harmony import */ var _order_status_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./order-status.service */ 82749);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! date-fns */ 86527);









let OrderStatusPage = class OrderStatusPage {
    constructor(commonfun, loginauth, neworderservice, orderstatusservice) {
        this.commonfun = commonfun;
        this.loginauth = loginauth;
        this.neworderservice = neworderservice;
        this.orderstatusservice = orderstatusservice;
        this.activitylist = [];
        this.selectedactivity = '';
        this.offset = 0;
        this.Issinglecust = false;
        this.datefrom = (0,date_fns__WEBPACK_IMPORTED_MODULE_6__["default"])(new Date(), 'dd.MM.yyyy');
        this.dateto = (0,date_fns__WEBPACK_IMPORTED_MODULE_6__["default"])(new Date(), 'dd.MM.yyyy');
    }
    ngOnInit() {
        //this.commonfun.chkcache('order-status');
        setTimeout(() => {
            this.Bindallactivity();
            this.fromdate = new Date().toISOString();
            this.todate = new Date().toISOString();
        }, 1500);
    }
    formatDate(value) {
        if (this.fromdate > this.todate) {
            // this.todate=this.fromdate;
            this.commonfun.presentAlert("Message", "Alert", "From Date can not be Greater than To Date");
        }
        return (0,date_fns__WEBPACK_IMPORTED_MODULE_6__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_7__["default"])(value), 'dd.MM.yyyy');
    }
    formatDate1(value) {
        if (this.fromdate > this.todate) {
            // this.todate=this.fromdate;
            this.commonfun.presentAlert("Message", "Alert", "From Date can not be Greater than To Date");
        }
        return (0,date_fns__WEBPACK_IMPORTED_MODULE_6__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_7__["default"])(value), 'dd.MM.yyyy');
    }
    Bindallactivity() {
        try {
            this.selectedcustomer = null;
            // this.selecteddocumentno=null;
            this.activitylist[0] = this.loginauth.selectedactivity;
            this.selectedactivity = this.activitylist[0].id;
            this.exonActChange();
        }
        catch (error) {
        }
    }
    exonActChange() {
        this.exBusinessPartnerlist = null;
        this.exselectedBusinessPartner = null;
        this.checkcust();
    }
    checkcust() {
        try {
            if (this.loginauth.defaultprofile[0].mmstOrderusrtype == "CEB") {
                this.neworderservice.getexistcustmersearchapi("", this.selectedactivity).subscribe(data => {
                    // var response = data;
                    const response = data;
                    //console.log("ORDER STATUS",response); 
                    this.exBusinessPartnerlist = response;
                    this.leastBusinessPartnerlist = this.exBusinessPartnerlist;
                    if (this.leastBusinessPartnerlist.length > this.loginauth.minlistcount) {
                        // console.log("Dont Clear");
                    }
                    else {
                        this.dontClearOrderStatus = true;
                    }
                    if (this.exBusinessPartnerlist.length == 1) {
                        this.exBusinessPartnerlist = response;
                        // this.passexBusinessPartnerlist=this.exBusinessPartnerlist;
                        this.exselectedBusinessPartner = this.exBusinessPartnerlist[0];
                        this.onChangeCustomer();
                        this.Issinglecust = true;
                    }
                    else {
                        this.Issinglecust = false;
                        // this.exBusinessPartnerlist = null;
                        this.exselectedBusinessPartner = null;
                    }
                });
                //
                // if (this.edtitdocid != undefined || this.edtitdocid != '') {
                //   this.editOrder(this.edtitdocid);
                // }
            }
        }
        catch (error) {
        }
    }
    refChange(event) {
        this.onChangeCustomer();
        // 
        event.component._searchText = "";
    }
    onChangeCustomer() {
        try {
            if (this.exselectedBusinessPartner != null) {
                this.BindOrder();
            }
            // this.commonfun.loadingDismiss();
        }
        catch (error) {
            // this.commonfun.loadingDismiss();
            //  console.log("error:Onchangecust:",error); 
        }
    }
    BindOrder() {
    }
    custsearchChange(event) {
        if (event.text.length >= 3) {
            this.bindcustomer1api(event.text);
        }
        else {
            //  this.exBusinessPartnerlist = [];
            if (!!this.dontClearOrderStatus && this.dontClearOrderStatus == true) {
            }
            else {
                // console.log("Cleared");
                this.exBusinessPartnerlist = [];
            }
        }
    }
    bindcustomer1api(strsearch, ispageload) {
        try {
            if (strsearch != "") {
                this.neworderservice.getexistcustmersearchapi(strsearch, this.selectedactivity).subscribe(data => {
                    var response = data;
                    this.exBusinessPartnerlist = response;
                });
            }
            else {
                // this.exBusinessPartnerlist=null;
                //=============start for top 10================= 
                if (this.leastBusinessPartnerlist) {
                    if (this.leastBusinessPartnerlist.length > this.loginauth.minlistcount) {
                        this.exBusinessPartnerlist = null;
                    }
                    else {
                        this.exBusinessPartnerlist = this.leastBusinessPartnerlist;
                        this.dontClearOrderStatus = true;
                    }
                }
                //=============end for top 10================= 
            }
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
            //  this.commonfun.loadingDismiss();
        }
    }
    onCustomerClose(event) {
        event.component._searchText = "";
    }
    ondatechange() {
        if (this.fromdate > this.todate) {
            // this.todate=this.fromdate;
            this.commonfun.presentAlert("Message", "Alert", "From Date can not be Greater than To Date");
        }
    }
    getOrderStatus() {
        try {
            if (this.validation() == false) {
                return;
            }
            var fromdate = this.fromdate.split('T')[0];
            var todate = this.todate.split('T')[0];
            this.orderstatusservice.getOrderStatus(fromdate, todate, this.exselectedBusinessPartner.id, this.offset).subscribe(data => {
                this.orderdata = data;
                if (this.orderdata.length < 10 || this.orderdata.length == 0) {
                    this.Islast = true;
                }
                else {
                    this.Islast = false;
                }
            }, error => {
                this.commonfun.presentAlert("Message", "Error", error.error.text);
            });
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    validation() {
        if (!this.exselectedBusinessPartner) {
            this.commonfun.presentAlert("Message", "Alert", "Please select Business Partner.");
            return false;
        }
        if (!this.fromdate) {
            this.commonfun.presentAlert("Message", "Alert", "Please select From date.");
            return false;
        }
        if (!this.todate) {
            this.commonfun.presentAlert("Message", "Alert", "Please select To date.");
            return false;
        }
        if (this.fromdate > this.todate) {
            // this.todate=this.fromdate;
            this.commonfun.presentAlert("Message", "Alert", "From Date can not be Greater than To Date");
            return false;
        }
    }
    onShow() {
        this.offset = 0;
        //&& !this.todate && !(this.exselectedBusinessPartner!=undefined || this.exselectedBusinessPartner!=null))
        this.getOrderStatus();
        //  else
        // this.commonfun.presentAlert("Message", "Error", "Please select above information.")
    }
    OrderStatus(item) {
        var ordercreated = '';
        var approveupdated = '';
        var invoicecreated = '';
        var dispatchupdated = '';
        var deliverupdated = '';
        var color = '';
        if (item.ordercreated)
            ordercreated = 'is-done';
        if (item.approveupdated)
            approveupdated = 'is-done';
        if (item.invoicecreated)
            invoicecreated = 'is-done';
        if (item.dispatchupdated)
            dispatchupdated = 'is-done';
        if (item.deliverupdated)
            deliverupdated = 'is-done';
        //if(item.ordercreated!='' && item.approveupdated=='') ordercreated='current';
        if (item.ordercreated != '' && item.approveupdated == '')
            ordercreated = 'is-done';
        if (item.approveupdated != '' && item.invoicecreated == '')
            approveupdated = 'is-done';
        if (item.invoicecreated != '' && item.dispatchupdated == '')
            invoicecreated = 'is-done';
        if (item.dispatchupdated != '' && item.deliverupdated == '')
            dispatchupdated = 'is-done';
        if (item.OrderQuantity == item.DeliverQuantity) {
            deliverupdated = 'is-done';
            color = "StepComplete";
        }
        else {
            color = "StepProgress";
        }
        var text = `
<div class="wrapper">
<ul class="` + color + `">
  <li class="` + color + `-item ` + ordercreated + `"><strong>Order</strong>
  <span *ngIf="item.ordercreated">` + item.ordercreated + `</span>
  </li>
  <li class="` + color + `-item ` + approveupdated + `"><strong>Approve</strong>
  <span *ngIf="item.approveupdated">` + item.approveupdated + `</span>
  </li>
  <li class="` + color + `-item ` + invoicecreated + `"><strong>Invoice</strong>
  <span *ngIf="item.invoicecreated">` + item.invoicecreated + `</span>
  </li>
  <li class="` + color + `-item ` + dispatchupdated + `"><strong>Dispatch</strong>
  <span *ngIf="item.dispatchupdated">` + item.dispatchupdated + `</span>
  </li>
  <li class="` + color + `-item ` + deliverupdated + `"><strong>Delivery</strong>
  <span *ngIf="item.deliverupdated">` + item.deliverupdated + `</span>
  </li>
</ul>
</div>`;
        this.commonfun.presentAlert("Status", "Order: " + item.DocumentNo, text);
    }
    toggleOrder(selectedcartproduct) {
        if (selectedcartproduct.show == false) {
            for (var i = 0; i < this.orderdata.length; i++) {
                if (this.orderdata[i].show === "true") {
                    this.orderdata[i].show = "false";
                }
            }
        }
        selectedcartproduct.show = !selectedcartproduct.show;
    }
    onPrevious() {
        if (this.offset > 1) {
            this.offset = this.offset - 10;
            this.getOrderStatus();
        }
    }
    onNext() {
        this.offset = this.offset + 10;
        this.getOrderStatus();
    }
    Resetpage() {
        this.fromdate = new Date().toISOString();
        this.todate = new Date().toISOString();
        this.orderdata = null;
        if (this.activitylist.length != 1) {
            this.selectedactivity = null;
            this.exBusinessPartnerlist = null;
        }
        if (this.Issinglecust != true) {
            this.exselectedBusinessPartner = null;
        }
    }
};
OrderStatusPage.ctorParameters = () => [
    { type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_2__.Commonfun },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__.LoginauthService },
    { type: _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_4__.NeworderService },
    { type: _order_status_service__WEBPACK_IMPORTED_MODULE_5__.OrderStatusService }
];
OrderStatusPage = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
        selector: 'app-order-status',
        template: _order_status_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_order_status_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], OrderStatusPage);



/***/ }),

/***/ 82749:
/*!******************************************************!*\
  !*** ./src/app/order-status/order-status.service.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OrderStatusService": () => (/* binding */ OrderStatusService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);





let OrderStatusService = class OrderStatusService {
    constructor(http, loginauth, genericHTTP) {
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
    }
    getOrderStatus(fromdate, todate, bp_id, offset) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileOrderStatus?'
            + 'fromdate=' + fromdate
            + '&todate=' + todate
            + '&bp_id=' + bp_id
            + '&offset=' + offset);
    }
};
OrderStatusService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService }
];
OrderStatusService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], OrderStatusService);



/***/ }),

/***/ 55608:
/*!****************************************************************!*\
  !*** ./src/app/order-status/order-status.page.scss?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = "ion-scroll[scrollX] {\n  white-space: nowrap;\n  overflow: visible;\n  overflow-y: auto;\n}\nion-scroll[scrollX] .scroll-item {\n  display: inline-block;\n}\nion-scroll[scrollX] .selectable-icon {\n  margin: 10px 20px 10px 20px;\n  color: red;\n  font-size: 100px;\n}\nion-scroll[scrollX] ion-avatar img {\n  margin: 10px;\n}\nion-scroll[scroll-avatar] {\n  height: 60px;\n}\n/* Hide ion-content scrollbar */\n::-webkit-scrollbar {\n  display: none;\n}\nh5 {\n  font-style: oblique;\n  color: darkcyan;\n  font-size: large;\n}\nh5 ion-icon {\n  color: lightcoral;\n}\nion-popover {\n  --width: 320px;\n}\nion-popover.dateTimePopover {\n  --offset-y: -350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVyLXN0YXR1cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtFQUVBLGlCQUFBO0VBQ0EsZ0JBQUE7QUFBSjtBQUVJO0VBQ0UscUJBQUE7QUFBTjtBQUdJO0VBQ0UsMkJBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7QUFETjtBQUlJO0VBQ0UsWUFBQTtBQUZOO0FBTUU7RUFDRSxZQUFBO0FBSEo7QUFNRSwrQkFBQTtBQUNBO0VBQ0UsYUFBQTtBQUhKO0FBTUU7RUFDRSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUhKO0FBSUk7RUFDRSxpQkFBQTtBQUZOO0FBT0U7RUFDRSxjQUFBO0FBSko7QUFNRTtFQUNFLDZCQUFBO0FBSEoiLCJmaWxlIjoib3JkZXItc3RhdHVzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1zY3JvbGxbc2Nyb2xsWF0ge1xuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAvLyBoZWlnaHQ6IDEyMHB4O1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgIG92ZXJmbG93LXk6IGF1dG87XG4vLyB3aWR0aDoxMDAlO1xuICAgIC5zY3JvbGwtaXRlbSB7XG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgfVxuXG4gICAgLnNlbGVjdGFibGUtaWNvbntcbiAgICAgIG1hcmdpbjogMTBweCAyMHB4IDEwcHggMjBweDtcbiAgICAgIGNvbG9yOiByZWQ7XG4gICAgICBmb250LXNpemU6IDEwMHB4O1xuICAgIH1cblxuICAgIGlvbi1hdmF0YXIgaW1ne1xuICAgICAgbWFyZ2luOiAxMHB4O1xuICAgIH1cbiAgfVxuXG4gIGlvbi1zY3JvbGxbc2Nyb2xsLWF2YXRhcl17XG4gICAgaGVpZ2h0OiA2MHB4O1xuICB9XG5cbiAgLyogSGlkZSBpb24tY29udGVudCBzY3JvbGxiYXIgKi9cbiAgOjotd2Via2l0LXNjcm9sbGJhcntcbiAgICBkaXNwbGF5Om5vbmU7XG4gIH1cblxuICBoNXtcbiAgICBmb250LXN0eWxlOiBvYmxpcXVlO1xuICAgIGNvbG9yOiBkYXJrY3lhbjtcbiAgICBmb250LXNpemU6IGxhcmdlO1xuICAgIGlvbi1pY29ue1xuICAgICAgY29sb3I6IGxpZ2h0Y29yYWw7XG4gICAgfVxuXG4gIH1cbiAgXG4gIGlvbi1wb3BvdmVyIHtcbiAgICAtLXdpZHRoOiAzMjBweDtcbiAgfVxuICBpb24tcG9wb3Zlci5kYXRlVGltZVBvcG92ZXIge1xuICAgIC0tb2Zmc2V0LXk6IC0zNTBweCAhaW1wb3J0YW50O1xuICAgIH0iXX0= */";

/***/ }),

/***/ 36227:
/*!****************************************************************!*\
  !*** ./src/app/order-status/order-status.page.html?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Order Status\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n   \n\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid fixed>\n  <ion-card>\n    <ion-card-content>\n      <ion-row>\n        <ion-col>\n          <ion-item>\n            <ion-label position=\"stacked\">Activity<span style=\"color:red!important\">*</span></ion-label>\n            <ion-select name=\"selectedactivity\" #C [(ngModel)]=\"selectedactivity\" (ionChange)=\"exonActChange()\" interface=\"popover\"\n              multiple=\"false\" placeholder=\"Select Activity\">\n              <ion-select-option *ngFor=\"let activity of activitylist\" [value]=\"activity.id\">{{activity.name}}\n              </ion-select-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <ion-item>\n            <ion-label position=\"stacked\">Customer<span style=\"color:red!important\">*</span></ion-label>\n            <ionic-selectable placeholder=\"Select Customer\" [searchDebounce]=\"1000\"\n            [(ngModel)]=\"exselectedBusinessPartner\" \n            [items]=\"exBusinessPartnerlist\"\n            itemValueField=\"id\" \n            itemTextField=\"name\" \n            [canSearch]=\"true\" \n            (onChange)=\"refChange($event)\"\n            (onSearch)=\"custsearchChange($event)\" (onClose)=\"onCustomerClose($event)\">\n            </ionic-selectable>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <!-- <ion-item>\n            <ion-label>From Date</ion-label>\n            <ion-datetime placeholder=\"Select Date\" [(ngModel)]=\"fromdate\" (ionChange)=\"ondatechange()\" displayFormat=\"DD.MM.YYYY\" max=\"2050\"></ion-datetime>\n            \n          </ion-item> -->\n          <ion-item>\n            <ion-label position=\"stacked\">From Date</ion-label>\n            <ion-item>\n              <ion-input placeholder=\"Select Date\" [value]=\"datefrom\"></ion-input>\n              <ion-button fill=\"clear\" id=\"open-date-input-1\">\n                <ion-icon icon=\"calendar\"></ion-icon>\n              </ion-button>\n              <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\n                <ng-template>\n                  <ion-datetime\n                    #popoverDatetime2\n                    presentation=\"date\"\n                    showDefaultButtons=\"true\"\n                    max=\"2050\"\n                    [(ngModel)]=\"fromdate\"\n                    (ionChange)=\"datefrom = formatDate(popoverDatetime2.value)\"\n                  ></ion-datetime>\n                </ng-template>\n              </ion-popover>\n            </ion-item>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      \n      <ion-row>\n        <ion-col>\n          <!-- <ion-item>\n            <ion-label>To Date</ion-label>\n            <ion-datetime placeholder=\"Select Date\" [(ngModel)]=\"todate\" (ionChange)=\"ondatechange()\" displayFormat=\"DD.MM.YYYY\" max=\"2050\"></ion-datetime>\n      \n          </ion-item> -->\n          <ion-item>\n            <ion-label position=\"stacked\">To Date</ion-label>\n            <ion-item>\n              <ion-input placeholder=\"Select Date\" [value]=\"dateto\"></ion-input>\n              <ion-button fill=\"clear\" id=\"open-date-input-2\">\n                <ion-icon icon=\"calendar\"></ion-icon>\n              </ion-button>\n              <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-2\" show-backdrop=\"false\">\n                <ng-template>\n                  <ion-datetime\n                    #popoverDatetime2\n                    presentation=\"date\"\n                    showDefaultButtons=\"true\"\n                    max=\"2050\"\n                    [(ngModel)]=\"todate\"\n                    (ionChange)=\"dateto = formatDate1(popoverDatetime2.value)\"\n                  ></ion-datetime>\n                </ng-template>\n              </ion-popover>\n            </ion-item>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col size=\"12\">\n          <ion-button (click)=\"onShow()\">\n            <ion-label >Show</ion-label>\n            </ion-button>\n         </ion-col>\n    </ion-row>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-content>\n    <ion-row>\n      <ion-col>\n        <h5 ion-text class=\"text-primary\">\n          <ion-icon name=\"navigate\"></ion-icon>  Status:\n        </h5>\n      </ion-col>\n    </ion-row>\n\n\n\n    <ion-row>\n      <ion-col size=\"8\">\n        <ion-button (click)=\"onPrevious()\">\n          <ion-label slot=\"start\">Previous</ion-label>\n          </ion-button>\n       </ion-col>\n       <ion-col size=\"4\">\n        <ion-button (click)=\"onNext()\" [disabled]=\"Islast\">\n          <ion-label>Next</ion-label>\n          </ion-button>\n       </ion-col>\n\n\n  </ion-row>\n  <ion-row style=\"font-size: small;\">\n    <ion-col size=\"5\">\n      <ion-label style=\"white-space: normal;color:#EE9E1E;\">&nbsp;&nbsp;Order</ion-label>\n    </ion-col>\n    <ion-col size=\"4\">\n      <ion-label style=\"white-space: normal;color: #EE9E1E;\">Date</ion-label>\n    </ion-col> \n    <ion-col size=\"3\">\n      <ion-label style=\"white-space: normal;color: #EE9E1E;\">Detail</ion-label>\n    </ion-col>\n  </ion-row>\n\n    <ion-item *ngFor=\"let item of orderdata; index as i\"  text-wrap style=\"font-size: small; max-width: 100% !important;\">\n      <!-- <ion-content scrollX=\"true\"> -->\n        <div style=\"width: 100%;\" (click)=\"toggleOrder(item)\">\n      <ion-row>\n        <ion-col size=\"5\">\n          <ion-label style=\"white-space: normal;\">\n        <ion-icon style=\"color: springgreen;\" name=\"locate\"></ion-icon>\n            Order : {{ item.DocumentNo }}\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"4\">\n          <ion-label style=\"white-space: normal;\">\n        \n         {{ item.OrderDate }}\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"3\">\n          <!-- <ion-button (click)=\"OrderStatus(item)\">\n            <ion-label slot=\"end\">Status</ion-label>\n            </ion-button> -->\n            &nbsp;&nbsp;\n            <ion-icon name=\"arrow-forward\" (click)=\"OrderStatus(item)\"></ion-icon>\n\n\n        </ion-col>\n\n      </ion-row>\n      \n      <ion-row>\n        <ion-col nowrap>\n          <div *ngIf=\"item.show\">\n            <ion-label style=\"font-size: small;\">Order Quantity: {{item.OrderQuantity}}</ion-label>\n            <ion-label style=\"font-size: small;\">Approve Quantity: {{item.ApprovedQty}}</ion-label>\n            <ion-label style=\"font-size: small;\">Invoice Quantity: {{item.InvoicedQuantity}}</ion-label>\n            <ion-label style=\"font-size: small;\">Dispatch Quantity: {{item.DispatchQuantity}}</ion-label>\n            <ion-label style=\"font-size: small;\">Delivery Quantity: {{item.DeliverQuantity}}</ion-label>\n           \n          </div>\n        </ion-col>\n       \n      </ion-row>\n    </div>\n      <!-- </ion-content> -->\n    </ion-item>\n\n\n  </ion-card-content>\n</ion-card>\n</ion-grid>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_order-status_order-status_module_ts.js.map