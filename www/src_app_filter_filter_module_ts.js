"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_filter_filter_module_ts"],{

/***/ 67655:
/*!*****************************************!*\
  !*** ./src/app/filter/filter.module.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FilterPageModule": () => (/* binding */ FilterPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _filter_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./filter.page */ 432);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ionic-selectable */ 25073);








const routes = [
    {
        path: '',
        component: _filter_page__WEBPACK_IMPORTED_MODULE_0__.FilterPage
    }
];
let FilterPageModule = class FilterPageModule {
};
FilterPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule,
            ionic_selectable__WEBPACK_IMPORTED_MODULE_6__.IonicSelectableModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterModule.forChild(routes)
        ],
        declarations: [_filter_page__WEBPACK_IMPORTED_MODULE_0__.FilterPage]
    })
], FilterPageModule);



/***/ }),

/***/ 432:
/*!***************************************!*\
  !*** ./src/app/filter/filter.page.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FilterPage": () => (/* binding */ FilterPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _filter_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./filter.page.html?ngResource */ 3497);
/* harmony import */ var _filter_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./filter.page.scss?ngResource */ 49848);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ 80190);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _order_approval_order_approval_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../order-approval/order-approval-service.service */ 44159);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ 58987);










let FilterPage = class FilterPage {
  constructor(alertController, storage, router, orderApprovalServiceService, http) {
    this.alertController = alertController;
    this.storage = storage;
    this.router = router;
    this.orderApprovalServiceService = orderApprovalServiceService;
    this.http = http;
    this.TAG = "FilterPage";
    this.active = 0;
    this.mainMenuId = 0;
    this.tab = true;
    this.org = false;
    this.doc_type = false;
    this.business_type = false;
    this.tabSelected = [];
    this.orgSelected = [];
    this.docTypeSelected = [];
    this.showSide = false;
    this.reftextcount = 0;
    this.test = true;
  }

  ngOnInit() {
    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {})();
  }

  ionViewWillEnter() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // console.log("ionViewWillEnter");
      _this.filterList = yield (yield _this.orderApprovalServiceService.getFilterData()).toPromise();

      if (!!_this.filterList) {
        _this.showSide = true;
      }

      _this.test = false;
      _this.tabSelected = _this.orderApprovalServiceService.filterTab;
      _this.orgSelected = _this.orderApprovalServiceService.filterOrg;
      _this.docTypeSelected = _this.orderApprovalServiceService.filterDocType;
      _this.businessParterSelected = _this.orderApprovalServiceService.filterBusinessPartner;
      _this.selectedBusinessPartner = _this.orderApprovalServiceService.filterselectedBusinessPartner;

      _this.filterList[0].subData.forEach(item => {
        //console.log(this.TAG,"clearFilterInnerMethod",item);
        var istabchecked = false;

        for (var i = 0; i < _this.tabSelected.length; i++) {
          if (_this.tabSelected[i] == item.id) {
            istabchecked = true;
          }
        }

        item.checked = istabchecked;
      });

      _this.filterList[1].subData.forEach(item => {
        var isorgchecked = false;

        for (var i = 0; i < _this.orgSelected.length; i++) {
          if (_this.orgSelected[i] == item.id) {
            isorgchecked = true;
          }
        }

        item.checked = isorgchecked;
      });

      _this.filterList[2].subData.forEach(item => {
        // console.log(this.TAG,"clearFilterInnerMethod",item);
        var isdocTypechecked = false;

        for (var i = 0; i < _this.docTypeSelected.length; i++) {
          if (_this.docTypeSelected[i] == item.id) {
            isdocTypechecked = true;
          }
        }

        item.checked = isdocTypechecked;
      });

      _this.filterList[3].subData.forEach(item => {
        // console.log(this.TAG,"clearFilterInnerMethod",item);
        item.checked = false;
      });
    })();
  } // async ionViewWillEnter()
  // {
  //   this.test = false;
  //   this.tabSelected=this.orderApprovalServiceService.filterTab;
  //   this.orgSelected=this.orderApprovalServiceService.filterOrg;
  //   this.docTypeSelected=this.orderApprovalServiceService.filterDocType;
  //   this.businessParterSelected = this.orderApprovalServiceService.filterBusinessPartner;
  //   this.isChecked = false;
  //                   this.test = false;
  //                   this.tabSelected=[];
  //                   this.orgSelected=[];
  //                   this.docTypeSelected=[];
  //                   this.businessParterSelected = "";
  //                   this.orderApprovalServiceService.filterTab=[];
  //                   this.orderApprovalServiceService.filterOrg=[];
  //                   this.orderApprovalServiceService.filterDocType=[];
  //                   this.orderApprovalServiceService.filterBusinessPartner='';
  //                  // this.selectedBusinessPartner = ""
  //                   this.clearFilterInnerMethod();
  // }


  menuItemClick(index) {
    let methodTAG = "menuItemClick";

    try {
      // console.log(this.TAG,methodTAG,index);
      this.active = index;
      this.mainMenuId = index;

      if (index == 0) {
        this.tab = true;
        this.org = false;
        this.doc_type = false;
        this.business_type = false;
      }

      if (index == 1) {
        this.tab = false;
        this.org = true;
        this.doc_type = false;
        this.business_type = false;
      }

      if (index == 2) {
        this.tab = false;
        this.org = false;
        this.doc_type = true;
        this.business_type = false;
      }

      if (index == 3) {
        this.tab = false;
        this.org = false;
        this.doc_type = false;
        this.business_type = true;
      }
    } catch (error) {}
  }

  clearFilter() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      let methodTAG = "clearFilter";

      try {
        // console.log(this,methodTAG);
        const alert = yield _this2.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Filters',
          subHeader: 'Clear Filters',
          message: 'Would you like to clear all filters?',
          buttons: [{
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: blah => {// console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Ok',
            handler: data => {
              // console.log('Confirm Okay');
              _this2.isChecked = false;
              _this2.test = false;
              _this2.tabSelected = [];
              _this2.orgSelected = [];
              _this2.docTypeSelected = [];
              _this2.businessParterSelected = "";
              _this2.orderApprovalServiceService.filterTab = [];
              _this2.orderApprovalServiceService.filterOrg = [];
              _this2.orderApprovalServiceService.filterDocType = [];
              _this2.orderApprovalServiceService.filterBusinessPartner = '';
              _this2.orderApprovalServiceService.filterselectedBusinessPartner = ''; // this.selectedBusinessPartner = ""

              _this2.clearFilterInnerMethod();
            }
          }]
        });
        yield alert.present();
      } catch (error) {// console.log(this.TAG,methodTAG,error);
      }
    })();
  }

  chkTabSelect(selectedData, id, i, event) {
    //  console.log(this.TAG,"chkTabSelect",selectedData,id);
    try {
      if (event.detail.checked) {
        var isnew = true;

        for (let i = 0; i < this.tabSelected.length; i++) {
          if (this.tabSelected[i] === id) {
            isnew = false;
          }
        }

        if (isnew == true) {
          this.tabSelected.push(id);
        } // console.log(this.TAG,"Pravin1",this.tabSelected);

      } else if (event.detail.checked == false) {
        for (let i = 0; i < this.tabSelected.length; i++) {
          if (this.tabSelected[i] === id) {
            this.tabSelected.splice(i, 1);
          }
        } //  console.log(this.TAG,"Pravin2",this.tabSelected);

      }
    } catch (error) {// console.log(this.TAG,"chkTabSelect",error);
    }
  }

  chkOrgSelect(selectedData, id, event) {
    let methodTAG = "chkOrgSelect";

    try {
      // console.log(this.TAG,methodTAG,selectedData,id);
      if (event.detail.checked) {
        var isnew = true;

        for (let i = 0; i < this.orgSelected.length; i++) {
          if (this.orgSelected[i] === id) {
            isnew = false;
          }
        }

        if (isnew == true) {
          this.orgSelected.push(id);
        }
      } else if (event.detail.checked == false) {
        for (let i = 0; i < this.orgSelected.length; i++) {
          if (this.orgSelected[i] === id) {
            this.orgSelected.splice(i, 1);
          }
        }
      } //  console.log("this.orgSelected",this.orgSelected);

    } catch (error) {//  console.log(this.TAG,methodTAG,error);
    }
  }

  chkDocTypeSelect(selectedData, id, event) {
    let methodTAG = "chkDocTypeSelect";

    try {
      // console.log(this.TAG,methodTAG,selectedData,id);
      if (event.detail.checked) {
        var isnew = true;

        for (let i = 0; i < this.docTypeSelected.length; i++) {
          if (this.docTypeSelected[i] === id) {
            isnew = false;
          }
        }

        if (isnew == true) {
          this.docTypeSelected.push(id);
        }
      } else if (event.detail.checked == false) {
        for (let i = 0; i < this.docTypeSelected.length; i++) {
          if (this.docTypeSelected[i] === id) {
            this.docTypeSelected.splice(i, 1);
          }
        }
      }
    } catch (error) {//  console.log(this.TAG,methodTAG,error);
    }
  }

  clearFilterInnerMethod() {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // console.log(this.TAG,"clearFilterInnerMethod");
      try {
        _this3.isChecked = false;
        _this3.test = false; // this.tabSelected.slice(0,this.tabSelected.length)
        //this.orgSelected.slice(0,this.orgSelected.length);
        //this.docTypeSelected.slice(0,this.docTypeSelected.length);

        _this3.businessParterSelected = "";
        _this3.selectedBusinessPartner = "";
        _this3.orderApprovalServiceService.filterTab;
        _this3.orderApprovalServiceService.filterOrg;
        _this3.orderApprovalServiceService.filterDocType;
        _this3.orderApprovalServiceService.filterBusinessPartner;
        _this3.orderApprovalServiceService.pageOffset = 0; // this.cdRef.detectChanges(); 

        _this3.filterList[0].subData.forEach(item => {
          // console.log(this.TAG,"clearFilterInnerMethod",item);
          item.checked = false;
        });

        _this3.filterList[1].subData.forEach(item => {
          // console.log(this.TAG,"clearFilterInnerMethod",item);
          item.checked = false;
        });

        _this3.filterList[2].subData.forEach(item => {
          // console.log(this.TAG,"clearFilterInnerMethod",item);
          item.checked = false;
        });

        _this3.filterList[3].subData.forEach(item => {
          // console.log(this.TAG,"clearFilterInnerMethod",item);
          item.checked = false;
        });

        _this3.businessParterSelected = "";
      } catch (error) {//  console.log(this.TAG,error);
      }
    })();
  }

  applyFilter() {
    let methodTAG = "applyFilter";

    try {
      this.orderApprovalServiceService.filterTab = this.tabSelected;
      this.orderApprovalServiceService.filterOrg = this.orgSelected;
      this.orderApprovalServiceService.filterDocType = this.docTypeSelected;

      if (!!this.selectedBusinessPartner) {
        this.orderApprovalServiceService.filterBusinessPartner = this.selectedBusinessPartner.id ? this.selectedBusinessPartner.id : 'CLEAR';
        this.orderApprovalServiceService.filterselectedBusinessPartner = this.selectedBusinessPartner ? this.selectedBusinessPartner : '';
      }

      let obj = {
        "tab": this.tabSelected,
        "org": this.orgSelected,
        "doc_Type": this.docTypeSelected,
        "business_Par": this.businessParterSelected
      };
      this.router.navigateByUrl('/order-approval', {
        state: {
          tab: obj
        }
      });
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  onBusinessPartnerchange(event) {
    try {} catch (error) {// console.log(this.TAG,error);
    }
  }

  onBusinessPartnerClose(event) {
    event.component.searchText = "";
    this.BusinessPartnerlist = null;
  }

  onBusinessPartnerSearch(event) {
    try {
      this.reftextcount++;
      var custsearchtext = event.text;

      if (custsearchtext.length % 3 == 0) {
        this.bindBusinessPartnerFromApi(custsearchtext);
        this.reftextcount = 0;
      }
    } catch (error) {// console.log(this.TAG,error);
    }
  }

  bindBusinessPartnerFromApi(searchkey) {
    try {
      if (searchkey) {
        this.orderApprovalServiceService.getBusinessPartnerData(searchkey).subscribe(businessPartnerList => {
          this.BusinessPartnerlist = businessPartnerList; //  console.log("BusinessPartnerlist",this.BusinessPartnerlist)
        });
      }
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  getFilterData() {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let url = "http://192.168.43.158:8080/openbravo/ws/in.mbs.webservice.Filter?user=hardik.pandya&password=pass&user_id=FFF202001310332122424C1A38AB7A41&action=all";
        return _this4.http.get(url, {});
      } catch (error) {}
    })();
  }

};

FilterPage.ctorParameters = () => [{
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.AlertController
}, {
  type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__.Storage
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.Router
}, {
  type: _order_approval_order_approval_service_service__WEBPACK_IMPORTED_MODULE_4__.OrderApprovalServiceService
}, {
  type: _angular_common_http__WEBPACK_IMPORTED_MODULE_7__.HttpClient
}];

FilterPage = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
  selector: 'app-filter',
  template: _filter_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_filter_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], FilterPage);


/***/ }),

/***/ 49848:
/*!****************************************************!*\
  !*** ./src/app/filter/filter.page.scss?ngResource ***!
  \****************************************************/
/***/ ((module) => {

module.exports = ".box {\n  height: 100% !important;\n  display: flex;\n  flex-flow: column;\n  padding: 0px !important;\n}\n\n.list-item {\n  margin-top: 25px !important;\n}\n\n.right-div {\n  height: 70vh !important;\n  overflow-y: auto;\n}\n\nion-content {\n  --overflow: hidden;\n}\n\n.row-custom {\n  height: -webkit-fill-available !important;\n}\n\n.custom-activated {\n  --ion-background-color: white !important;\n}\n\n.ion-item-custom {\n  --ion-background-color:#eeeeee !important;\n  padding: 0px !important;\n}\n\n.back-color {\n  background-color: #eeeeee !important;\n}\n\n.custom-ion-col {\n  padding: 0px !important;\n}\n\n.footer-back-color {\n  background-color: white;\n}\n\n.footer-btn-color {\n  background-color: #F39E20 !important;\n}\n\n.right-div {\n  height: 80vh !important;\n  overflow-y: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZpbHRlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx1QkFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0FBQ0o7O0FBR0E7RUFDSSwyQkFBQTtBQUFKOztBQUVBO0VBQ0ksdUJBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUlBO0VBQ0ksa0JBQUE7QUFESjs7QUFHQTtFQUNJLHlDQUFBO0FBQUo7O0FBRUE7RUFDSSx3Q0FBQTtBQUNKOztBQUNBO0VBQ0kseUNBQUE7RUFDQSx1QkFBQTtBQUVKOztBQUFBO0VBQ0ksb0NBQUE7QUFHSjs7QUFBQTtFQUNJLHVCQUFBO0FBR0o7O0FBREE7RUFDSSx1QkFBQTtBQUlKOztBQUZBO0VBQ0ksb0NBQUE7QUFLSjs7QUFIQTtFQUNJLHVCQUFBO0VBQ0EsZ0JBQUE7QUFNSiIsImZpbGUiOiJmaWx0ZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJveHtcbiAgICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDsgXG4gICAgZGlzcGxheTogZmxleDsgXG4gICAgZmxleC1mbG93OiBjb2x1bW47XG4gICAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG59XG5cblxuLmxpc3QtaXRlbXtcbiAgICBtYXJnaW4tdG9wOiAyNXB4ICFpbXBvcnRhbnQ7XG59XG4ucmlnaHQtZGl2e1xuICAgIGhlaWdodDogNzB2aCAhaW1wb3J0YW50OyBcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xufVxuXG5cblxuaW9uLWNvbnRlbnQge1xuICAgIC0tb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5yb3ctY3VzdG9te1xuICAgIGhlaWdodDogLXdlYmtpdC1maWxsLWF2YWlsYWJsZSAhaW1wb3J0YW50O1xufVxuLmN1c3RvbS1hY3RpdmF0ZWQge1xuICAgIC0taW9uLWJhY2tncm91bmQtY29sb3IgOiB3aGl0ZSAhaW1wb3J0YW50O1xufVxuLmlvbi1pdGVtLWN1c3RvbXtcbiAgICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiNlZWVlZWUgIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cbi5iYWNrLWNvbG9ye1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNlZWVlZWUgIWltcG9ydGFudDtcbn1cblxuLmN1c3RvbS1pb24tY29se1xuICAgIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuLmZvb3Rlci1iYWNrLWNvbG9ye1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuLmZvb3Rlci1idG4tY29sb3J7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0YzOUUyMCAhaW1wb3J0YW50O1xufVxuLnJpZ2h0LWRpdntcbiAgICBoZWlnaHQ6IDgwdmggIWltcG9ydGFudDsgXG4gICAgb3ZlcmZsb3cteTogYXV0bztcbn0iXX0= */";

/***/ }),

/***/ 3497:
/*!****************************************************!*\
  !*** ./src/app/filter/filter.page.html?ngResource ***!
  \****************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar>\n   \n    <ion-buttons slot=\"start\">\n      <ion-back-button  defaultHref=\"order-approval\"></ion-back-button>\n    </ion-buttons>\n    <ion-title slot=\"start\">Filters</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"clearFilter()\"> Clear Filters</ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content no-margin no-padding>\n\n  <ion-grid class=\"box\">\n    <ion-row class=\"row-custom\">\n      <ion-col size=\"4\" class=\"back-color custom-ion-col\">\n        <ion-list *ngFor=\"let filterItem of filterList;let i = index\" class=\"back-color\">\n          <ion-item lines=\"none\" class=\"list-item\" (click)=\"menuItemClick(i)\" [ngClass]=\"{'custom-activated': (active == i),'ion-item-custom':(active != i)}\">\n            <ion-label color=\"primary\">\n              {{ filterItem.name }}\n            </ion-label>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n      <ion-col size=\"8\">\n      <div *ngIf=\"showSide\" class=\"right-div\">\n        <div *ngIf=\"tab\">\n          <ion-list *ngFor=\"let subData of filterList[0].subData;let i=index\">\n            <ion-item lines=\"none\" text-wrap>\n              <ion-label>{{subData.sub_name}}</ion-label>\n              <ion-checkbox slot=\"start\"  value=\"subData.sub_name\" (ionChange)=\"chkTabSelect(subData.sub_name,subData.id,i,$event)\" [(ngModel)]=\"subData.checked\" (ngModelChange)=\"$event ? checkedIdx = i : checkedIdx = -1\"></ion-checkbox>\n            </ion-item>\n          </ion-list>\n        </div>\n        <div *ngIf=\"org\" class=\"right-div\">\n          <ion-list *ngFor=\"let subData of filterList[1].subData;let i=index\">\n            <ion-item lines=\"none\" text-wrap>\n              <ion-label>{{subData.sub_name}}</ion-label>\n              <ion-checkbox slot=\"start\" value=\"subData.sub_name\" (ionChange)=\"chkOrgSelect(subData.sub_name,subData.id,$event)\" [(ngModel)]=\"subData.checked\"  (ngModelChange)=\"$event ? checkedOrgIdx = i : checkedOrgIdx = -1\"></ion-checkbox>\n            </ion-item>\n          </ion-list>\n        </div>\n        <div  *ngIf=\"doc_type\" class=\"right-div\">\n          <ion-list *ngFor=\"let subData of filterList[2].subData;let i=index\">\n            <ion-item lines=\"none\" text-wrap>\n              <ion-label>{{subData.sub_name}}</ion-label>\n              <ion-checkbox slot=\"start\" value=\"subData.sub_name\" (ionChange)=\"chkDocTypeSelect(subData.sub_name,subData.id,$event)\" [(ngModel)]=\"subData.checked\" (ngModelChange)=\"$event ? checked_Doc_TypeIdx = i : checked_Doc_TypeIdx = -1\"></ion-checkbox>\n            </ion-item>\n          </ion-list>\n        </div>\n        <div  id=\"business_type\" *ngIf=\"business_type\">\n          <ion-item>\n            <ion-label position=\"stacked\">Business Partner</ion-label>\n            <ionic-selectable placeholder=\"Select Business Partner\" disable=\"true\"\n            [(ngModel)]=\"selectedBusinessPartner\"\n            [items]=\"BusinessPartnerlist\" \n            itemValueField=\"id\" \n            itemTextField=\"name\" \n            [canSearch]=\"true\"\n            (onChange)=\"onBusinessPartnerchange($event)\"\n            (onClose)=\"onBusinessPartnerClose($event)\"\n            multiple={false}\n            (onSearch)=\"onBusinessPartnerSearch($event)\">\n            </ionic-selectable>\n          </ion-item>\n          <!-- <ion-list *ngFor=\"let subData of filterList[3]?.subData;let i=index\">\n            <ion-item lines=\"none\" text-wrap>\n              <ion-label>{{subData.sub_name}}</ion-label>\n              <ion-checkbox slot=\"start\" value=\"subData.sub_name\" (ionChange)=\"chkBusinessTypeSelect(subData.sub_name,$event)\" [(ngModel)]=\"subData.checked\"  (ngModelChange)=\"$event ? checked_Business_Type_Idx = i : checked_Business_Type_Idx = -1\" [disabled]=\"checked_Business_Type_Idx >= 0 && checked_Business_Type_Idx != i\"></ion-checkbox>\n            </ion-item>\n          </ion-list> -->\n        </div>\n      </div>  \n    </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n<ion-footer class=\"footer-back-color\">\n  <ion-button  (click)=\"applyFilter()\"  color=\"primary\"  size=\"large\" expand=\"full\" fill=\"outline\" class=\"footer-btn-color\">\n    <ion-label style=\"color: white;\">\n      Apply\n    </ion-label>\n  </ion-button>\n</ion-footer>\n ";

/***/ })

}]);
//# sourceMappingURL=src_app_filter_filter_module_ts.js.map