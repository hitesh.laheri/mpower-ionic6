"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_hrms_mpr-form_mpr-form_module_ts"],{

/***/ 9697:
/*!**************************************************!*\
  !*** ./src/app/hrms/mpr-form/mpr-form.module.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MprFormPageModule": () => (/* binding */ MprFormPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _mpr_form_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mpr-form.page */ 19691);
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/core */ 59121);
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/datepicker */ 42298);
/* harmony import */ var src_app_material_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/material.module */ 63806);
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @swimlane/ngx-datatable */ 93143);











const routes = [
    {
        path: '',
        component: _mpr_form_page__WEBPACK_IMPORTED_MODULE_0__.MprFormPage
    }
];
let MprFormPageModule = class MprFormPageModule {
};
MprFormPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            src_app_material_module__WEBPACK_IMPORTED_MODULE_1__.MaterialModule,
            _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_7__.MatDatepickerModule,
            _angular_material_core__WEBPACK_IMPORTED_MODULE_8__.MatNativeDateModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule,
            _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_9__.NgxDatatableModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_10__.RouterModule.forChild(routes)
        ],
        declarations: [_mpr_form_page__WEBPACK_IMPORTED_MODULE_0__.MprFormPage]
    })
], MprFormPageModule);



/***/ }),

/***/ 19691:
/*!************************************************!*\
  !*** ./src/app/hrms/mpr-form/mpr-form.page.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MprFormPage": () => (/* binding */ MprFormPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _mpr_form_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mpr-form.page.html?ngResource */ 78555);
/* harmony import */ var _mpr_form_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./mpr-form.page.scss?ngResource */ 12525);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var src_provider_message_helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/provider/message-helper */ 98792);
/* harmony import */ var _mpr_form_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../mpr-form.service */ 89792);
/* harmony import */ var src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/login/loginauth.service */ 44010);
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @swimlane/ngx-datatable */ 93143);
/* harmony import */ var src_app_order_approval_show_approval_details_modal_show_approval_details_modal_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/order-approval/show-approval-details-modal/show-approval-details-modal.page */ 19592);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic/angular */ 93819);














let MprFormPage = class MprFormPage {
  constructor(fb, commonfun, msg, MPRService, loginService, el, route, router, alertCtrl) {
    this.fb = fb;
    this.commonfun = commonfun;
    this.msg = msg;
    this.MPRService = MPRService;
    this.loginService = loginService;
    this.el = el;
    this.route = route;
    this.router = router;
    this.alertCtrl = alertCtrl; // @ViewChild('myInput',{static: false}) myInput: ElementRef;

    this.TAG = "Mpr Form Page";
    this.selectedQualificationList = [];
    this.now = new Date();
    this.year = this.now.getFullYear();
    this.month = this.now.getMonth() + 1;
    this.day = this.now.getDate();
    this.maxDate = this.year + "-" + this.month + "-" + this.day;
    this.ColumnMode = _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_8__.ColumnMode;
    this.SelectionType = _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_8__.SelectionType;
    this.page = new src_app_order_approval_show_approval_details_modal_show_approval_details_modal_page__WEBPACK_IMPORTED_MODULE_7__.Page();
    this.workExperienceModel_lower = 0;
    this.workExperienceModel_upper = 0;
    this.maxCTCLbl = " Lakh";
    this.disable_save_jd_btn = true;
    this.existingMPRStatus = false;
    this.validation_messages = {
      'organizationDropDownErrorMessage': [{
        type: 'required',
        message: ' *Please Select Organization.'
      }],
      'documentStatusDropDownErrorMessage': [{
        type: 'required',
        message: ' *Please Select Document Status.'
      }],
      'reasonForMPRDropDownErrorMessage': [{
        type: 'required',
        message: ' *Please Select Reason for MPR.'
      }],
      'gradeDropDownErrorMessage': [{
        type: 'required',
        message: '*Please Select Grade.'
      }],
      'jobTitleDropDownErrorMessage': [{
        type: 'required',
        message: '*Please Select Job Title.'
      }],
      'designationOrRoleDropDownErrorMessage': [{
        type: 'required',
        message: '*Please Select Designation / Role.'
      }],
      'departmentDropDownErrorMessage': [{
        type: 'required',
        message: '*Please Select Department.'
      }],
      'locationDropDownErrorMessage': [{
        type: 'required',
        message: '*Please Select Location.'
      }],
      'numberOfPositionTxtErrorMessage': [{
        type: 'required',
        message: '*Please Enter Number Of Position.'
      }],
      'positionClosedTxtErrorMessage': [{
        type: 'required',
        message: '*Please Enter Number Of Position Closed.'
      }],
      'workExpFromTxtErrorMessage': [{
        type: 'required',
        message: '*Please Select Work Experience From.'
      }],
      'workExpFromToTxtErrorMessage': [{
        type: 'required',
        message: '*Please Select Work Experience To.'
      }],
      'ctcRangeFromTxtErrorMessage': [{
        type: 'required',
        message: '*Please Select CTC Range From.'
      }],
      'ctcRangeToTxtErrorMessage': [{
        type: 'required',
        message: '*Please Select CTC Range To.'
      }],
      'ageFromTxtErrorMessage': [{
        type: 'required',
        message: '*Please Select Age Range From.'
      }],
      'ageToTxtErrorMessage': [{
        type: 'required',
        message: '*Please Select Age Range To.'
      }],
      'jobListingDropDownErrorMessage': [{
        type: 'required',
        message: '*Please Select Job Listing.'
      }],
      'onCompanyRollChkErrorMessage': [{
        type: 'required',
        message: '*Please Select Company Roll.'
      }],
      'qualificationDropDownErrorMessage': [{
        type: 'required',
        message: '*Please Select Qualification.'
      }],
      'jobDescriptionTxtErrorMessage': [{
        type: 'required',
        message: '*Please Select Job Description.'
      }],
      'resourceRequirementDropDownErrorMessage': [{
        type: 'required',
        message: '*Please Select Resource Requirement.'
      }],
      'resourceDepartmentDropDownErrorMessage': [{
        type: 'required',
        message: '*Please Select Resource Requirement.'
      }],
      'resourceRequiredChkErrorMessage': [{
        type: 'required',
        message: '*Please Select Resource Required.'
      }]
    };
    this.MPRForm = this.fb.group({
      organizationDropDown: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
      reasonForMPRDropDown: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
      gradeDropDown: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
      jobTitleDropDown: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
      designationOrRoleDropDown: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
      departmentDropDown: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
      locationDropDown: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
      numberOfPositionTxt: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
      positionClosedTxt: [],
      jobListingDropDown: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
      onCompanyRollChk: [],
      qualificationDropDown: [],
      jobDescriptionTxt: [],
      resourceRequirementDropDown: [],
      resourceDepartmentDropDown: [],
      resourceRequiredChk: []
    });
  }

  ngOnInit() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this.commonfun.loadingPresent();

        _this.organizationList = yield _this.MPRService.getOrganizationList().toPromise();
        _this.qualificationList = yield _this.MPRService.getQualificationList().toPromise();
        _this.reasonForMPRList = yield _this.MPRService.getReasonForMPRList().toPromise();
        _this.jobListingList = yield _this.MPRService.getJobListingList().toPromise();
        _this.resourceRequirementMasterList = yield _this.MPRService.getResourceRequirementMasterList().toPromise();
        _this.resourceDepartmentMasterList = yield _this.MPRService.getResourceDepartmentMasterList().toPromise();

        if (_this.selectedQualificationList == undefined || _this.selectedQualificationList.length == 0) {
          let qualificationDropDownTemp = null;
          qualificationDropDownTemp = _this.MPRForm.get('qualificationDropDown');
          qualificationDropDownTemp.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required);
          qualificationDropDownTemp.updateValueAndValidity();
        }

        _this.route.queryParams.subscribe(params => {
          if (params && params.special) {
            _this.existingMPR = JSON.parse(params.special);

            if (!!_this.existingMPR) {
              _this.existingMPRStatus = true;

              _this.bindExistingData();
            }

            console.log(_this.TAG, JSON.parse(params.special));
          }
        });

        _this.commonfun.loadingDismiss();
      } catch (error) {
        _this.commonfun.loadingDismiss();

        console.log(_this.TAG, error);
      }
    })();
  }

  ionViewWillEnter() {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  }

  refreshPage() {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  }

  bindExistingData() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        // this.existingMPRStatus
        _this2.organizationList.forEach(org => {
          if (org.id == _this2.existingMPR.org_id) {
            _this2.selectedOrganization = org;

            _this2.onOrganizationChange();
          }
        });

        _this2.reasonForMPRList.forEach(reason => {
          if (reason.code == _this2.existingMPR.mpr_reason.code) {
            _this2.MPRForm.controls.reasonForMPRDropDown.setValue(reason);
          }
        });

        _this2.MPRForm.controls.numberOfPositionTxt.setValue(_this2.existingMPR.number_of_position);

        _this2.workExperienceModel = {
          lower: Number(_this2.existingMPR.work_rang_from),
          upper: Number(_this2.existingMPR.work_rang_to)
        };
        _this2.CTCRangeModel = {
          lower: Number(_this2.existingMPR.ctc_rang_from),
          upper: Number(_this2.existingMPR.ctc_rang_to)
        };
        _this2.AgeRangeModel = {
          lower: Number(_this2.existingMPR.age_rang_from),
          upper: Number(_this2.existingMPR.age_rang_to)
        };

        _this2.jobListingList.forEach(jobList => {
          if (jobList.code = _this2.existingMPR.Job_list.code) _this2.selectedJobListing = jobList;
        });

        _this2.qualificationList.forEach(qualification => {
          if (qualification.qualification_id = _this2.existingMPR.Qualification_List[0].qualification_id) {
            _this2.selectedQualificationList = [qualification];

            if (_this2.selectedQualificationList == undefined || _this2.selectedQualificationList.length == 0) {
              let control1 = null;
              control1 = _this2.MPRForm.get('qualificationDropDown');
              control1.clearValidators();
              control1.updateValueAndValidity();
            }
          }
        });

        _this2.MPRForm.controls.onCompanyRollChk.setValue(_this2.existingMPR.on_company_roll);
      } catch (error) {
        console.log(_this2.TAG, error);
      }
    })();
  }

  onOrganizationChange() {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this3.MPRMasterDataList = yield _this3.MPRService.getMPRMasterDataList(_this3.selectedOrganization).toPromise();
        console.log(_this3.TAG, _this3.MPRMasterDataList);
        _this3.activityLbl = _this3.MPRService.activity_name;

        _this3.organizationList.forEach(organization => {
          if (organization.hasOwnProperty('hrbranch')) {
            _this3.taggedHRBranchLbl = organization.name;
          }
        });

        _this3.jobTitleList = _this3.MPRMasterDataList.jobtitlename.JobTitleList;
        _this3.gradeList = _this3.MPRMasterDataList.Gradename.GradeList;
        _this3.departmentList = _this3.MPRMasterDataList.Department.DepartmentList;
        _this3.locationList = _this3.MPRMasterDataList.Locationname.LocationList;
        _this3.designationOrRoleList = _this3.MPRMasterDataList.Rolename.RoleList;

        if (!!_this3.existingMPR) {
          _this3.jobTitleList.forEach(job => {
            if (job.Standardtemp_id == _this3.existingMPR.Standardtemp_id) {
              _this3.selectedJOBTitle = job;
            }
          });

          _this3.gradeList.forEach(grade => {
            if (grade.grade_id == _this3.existingMPR.grade_id) {
              _this3.selectedGrade = grade;
            }
          });

          _this3.departmentList.forEach(department => {
            if (department.department_id == _this3.existingMPR.department_id) {
              _this3.selectedDepartment = department;
            }
          });

          _this3.locationList.forEach(location => {
            if (location.location_id == _this3.existingMPR.location_id) {
              _this3.selectedLocation = location;
            }
          });

          _this3.designationOrRoleList.forEach(role => {
            if (role.role_id == _this3.existingMPR.role_id) {
              _this3.selectedDesignation = role;
            }
          });
        }
      } catch (error) {
        console.log(_this3.TAG, error);
      }
    })();
  }

  onReasonForMPRChange() {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  }

  onGradeChange() {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  }

  onJobTitleChange() {
    try {
      console.log(this.TAG, this.MPRForm.controls['jobTitleDropDown'].value);
      this.jobDescriptionList = this.MPRForm.controls['jobTitleDropDown'].value.Job_Description_List; // this.resourceRequirementList = this.MPRForm.controls['jobTitleDropDown'].value.Resource_requirement_List;

      this.resource_requirement_List = this.MPRForm.controls['jobTitleDropDown'].value.Resource_requirement_List;

      if (this.resource_requirement_List == undefined || this.resource_requirement_List.length == 0) {
        let control1 = null;
        control1 = this.MPRForm.get('resourceRequirementDropDown');
        control1.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required);
        control1.updateValueAndValidity();
        let control2 = null;
        control2 = this.MPRForm.get('resourceDepartmentDropDown');
        control2.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required);
        control2.updateValueAndValidity();
      } //  console.log(this.TAG, this.resource_requirement_List);

    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  onDesignationOrRoleChange() {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  }

  onDepartmentChange() {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  }

  onLocationChange() {
    try {
      const ionSelects = document.querySelectorAll('ion-select');
      ionSelects.forEach(sel => {
        sel.shadowRoot.querySelectorAll('.select-text').forEach(elem => {
          //  console.log("HTML",elem);
          //  setTimeout(() => {
          //   console.log("HTML",elem.textContent);
          //   let test = elem.textContent;
          //   var split_str = test.split(",");
          //   if(split_str.length > 1){
          //     console.log("HTML DATA",split_str);
          //     let tags = split_str.map(skill => `<ion-badge style="background: #27AE60 !important;margin: 2px;padding: 5px;border-radius: 10px;">${skill}</ion-badge>`).join('');
          //     elem.innerHTML = tags;
          //   }
          //  }, 1000);
          // var test = elem.toString();
          //  var result = test.match(/<div>(.*?)<\/div>/g).map(function(val){
          //   return val.replace(/<\/?div>/g,'');
          //  });
          //   console.log("TET ",test);
          elem.setAttribute('style', 'white-space: break-spaces !important;');
        });
      });
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  onQualificationChange() {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  }

  removeSelectedQualification(selectedQualification) {
    try {
      const result = this.selectedQualificationList.filter(item => item.id != selectedQualification.id);
      this.selectedQualificationList = result;
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  onAddQualification() {
    try {
      //console.log(this.TAG,"onAddQualification",this.MPRForm.controls['qualificationDropDown'].value);    
      if (this.MPRForm.controls['qualificationDropDown'].value != null) {
        if (this.selectedQualificationList.length > 0) {
          let sameQualificationCheck = this.selectedQualificationList.find(e => e.id === this.MPRForm.controls['qualificationDropDown'].value.id);

          if (sameQualificationCheck != null || sameQualificationCheck != undefined) {
            this.commonfun.presentAlert("MPR Form", "Validation", " This qualification is already added");
          } else {
            let temp1 = {
              "code": this.MPRForm.controls['qualificationDropDown'].value.code,
              "qualification_id": this.MPRForm.controls['qualificationDropDown'].value.id,
              "name": this.MPRForm.controls['qualificationDropDown'].value.name
            };
            this.selectedQualificationList.push(temp1);
          }
        } else {
          let temp = {
            "code": this.MPRForm.controls['qualificationDropDown'].value.code,
            "qualification_id": this.MPRForm.controls['qualificationDropDown'].value.id,
            "name": this.MPRForm.controls['qualificationDropDown'].value.name
          };
          this.selectedQualificationList = [temp];
        }
      } else {
        this.commonfun.presentAlert("MPR Form", "Validation", "Please select qualification");
      }
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  onWorkExperienceChange(data) {
    // console.log(data);
    this.workExperienceModel_lower = data.lower;
    this.workExperienceModel_upper = data.upper;
  }

  onCTCRangeChange(selectedCTC) {
    this.CTCRangeModel_lower = selectedCTC.lower;
    this.CTCRangeModel_upper = selectedCTC.upper;

    if (this.CTCRangeModel_upper == 100) {
      this.maxCTCLbl = " CR";
      this.CTCRangeModel_upper = 1;
    } else {
      this.maxCTCLbl = " Lakh";
    }
  }

  onAgeRangeChange(selectedAgeRange) {
    this.AgeRangeModel_lower = selectedAgeRange.lower;
    this.AgeRangeModel_upper = selectedAgeRange.upper;
  }

  editSelectedJobDescription(selectedJD) {
    try {
      this.disable_save_jd_btn = false;
      this.MPRForm.controls.jobDescriptionTxt.setValue(selectedJD.job_description);
      this.selectedJDForEdit = selectedJD;
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  onSaveJD() {
    try {
      let jdId = this.jobDescriptionList.findIndex(jd => jd.job_des_id === this.selectedJDForEdit.job_des_id);
      this.jobDescriptionList[jdId].job_description = this.MPRForm.get('jobDescriptionTxt').value;
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  onAddResourceRequirement() {
    try {
      let resource = {
        "Resource_requirement": {
          "id": this.MPRForm.get('resourceRequirementDropDown').value.id,
          "name": this.MPRForm.get('resourceRequirementDropDown').value.name,
          "code": this.MPRForm.get('resourceRequirementDropDown').value.code
        },
        "Department": {
          "id": this.MPRForm.get('resourceDepartmentDropDown').value.id,
          "name": this.MPRForm.get('resourceDepartmentDropDown').value.name,
          "code": this.MPRForm.get('resourceDepartmentDropDown').value.code
        },
        "Required": {
          "isrequired": this.MPRForm.get('resourceRequiredChk').value
        }
      }; //console.log(this.MPRForm.get('resourceRequirementDropDown').value);

      if (!!this.resource_requirement_List) {
        this.resource_requirement_List.push(resource);
      } else {
        this.resource_requirement_List = [resource]; // console.log(this.resource_requirement_List);
      }
    } catch (error) {}
  }

  removeResourceRequirement(resourceRequirement) {
    console.log(this.TAG, resourceRequirement);
    const result = this.resource_requirement_List.filter(item => item.Resource_requirement.id != resourceRequirement.Resource_requirement.id);
    this.resource_requirement_List = result;
  }

  presentAlert(Header, SubHeader, Message) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this4.alertCtrl.create({
        header: Header,
        subHeader: SubHeader,
        message: Message,
        buttons: [{
          text: "OK",
          handler: ok => {
            _this4.router.navigateByUrl('/home');
          }
        }]
      });
      alert.dismiss(() => console.log('The alert has been closed.'));
      yield alert.present();
    })();
  }

  onMPRFormSubmit(MPRForm, status) {
    var _this5 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        console.log(_this5.TAG, "Location", _this5.MPRForm.get('locationDropDown').value);

        if (_this5.CTCRangeModel != undefined && _this5.CTCRangeModel.upper != undefined && _this5.CTCRangeModel.upper != 0) {
          if (_this5.AgeRangeModel != undefined && _this5.AgeRangeModel.upper != undefined && _this5.AgeRangeModel.upper != 0) {
            if (!!_this5.selectedQualificationList && _this5.selectedQualificationList.length > 0) {
              if (!!_this5.jobDescriptionList && _this5.jobDescriptionList.length > 0) {
                //  console.log(this.TAG,MPRForm);
                let mprPostData = {
                  "Standardtemp_id": _this5.MPRForm.controls['jobTitleDropDown'].value.Standardtemp_id,
                  "org_id": _this5.MPRForm.get('organizationDropDown').value.id,
                  "mpr_reason_code": _this5.MPRForm.get('reasonForMPRDropDown').value.code,
                  "grade_id": _this5.MPRForm.get('gradeDropDown').value.grade_id,
                  "role_id": _this5.MPRForm.get('designationOrRoleDropDown').value.role_id,
                  "department_id": _this5.MPRForm.get('departmentDropDown').value.department_id,
                  "location_id": _this5.MPRForm.get('locationDropDown').value.location_id,
                  "number_of_position": _this5.MPRForm.get('numberOfPositionTxt').value,
                  "position_closed": "",
                  "work_rang_from": _this5.workExperienceModel_lower,
                  "work_rang_to": _this5.workExperienceModel_upper,
                  "ctc_rang_from": _this5.CTCRangeModel.lower,
                  "ctc_rang_to": _this5.CTCRangeModel.upper,
                  "age_rang_from": _this5.AgeRangeModel.lower,
                  "age_rang_to": _this5.AgeRangeModel.upper,
                  "job_list_code": _this5.MPRForm.get('jobListingDropDown').value.code,
                  "on_company_roll": _this5.MPRForm.get('onCompanyRollChk').value,
                  "qualification": _this5.selectedQualificationList,
                  "Job_Description_List": _this5.jobDescriptionList,
                  "Resource_requirement_List": _this5.resource_requirement_List,
                  "is_update": _this5.existingMPRStatus,
                  "is_approve": status,
                  "mpr_id": _this5.existingMPR.mpr_id ? _this5.existingMPR.mpr_id : '0'
                };
                let postMPRForm = yield _this5.MPRService.postMPRForm(mprPostData).toPromise();
                console.log(_this5.TAG, "Post Form", mprPostData);

                if (!!postMPRForm && postMPRForm.resposemsg == "Success") {
                  console.log(_this5.TAG, postMPRForm);

                  _this5.presentAlert("MPR Form", postMPRForm.resposemsg, postMPRForm.logmsg);
                } else {
                  console.log(_this5.TAG, postMPRForm);

                  _this5.commonfun.presentAlert("MPR Form", "Error", "Some things went wrong please try again.");
                }
              } else {
                _this5.commonfun.presentAlert("MPR Form", "Validation", "Job Description Can Not Be Empty");
              }
            } else {
              _this5.commonfun.presentAlert("MPR Form", "Validation", "Please Add Qualification");
            }
          } else {
            _this5.commonfun.presentAlert("MPR Form", "Validation", "Please Select Age Range");
          }
        } else {
          _this5.commonfun.presentAlert("MPR Form", "Validation", "Please Select CTC Range");
        }
      } catch (error) {
        console.log(_this5.TAG, error);

        _this5.commonfun.presentAlert("MPR Form", "Error", error.message);
      }
    })();
  }

  approved() {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  }

  disApproved() {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  }

};

MprFormPage.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_9__.FormBuilder
}, {
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_3__.Commonfun
}, {
  type: src_provider_message_helper__WEBPACK_IMPORTED_MODULE_4__.Message
}, {
  type: _mpr_form_service__WEBPACK_IMPORTED_MODULE_5__.MprFormService
}, {
  type: src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_6__.LoginauthService
}, {
  type: _angular_core__WEBPACK_IMPORTED_MODULE_10__.ElementRef
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_11__.ActivatedRoute
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_11__.Router
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.AlertController
}];

MprFormPage = (0,tslib__WEBPACK_IMPORTED_MODULE_13__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
  selector: 'app-mpr-form',
  template: _mpr_form_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_mpr_form_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], MprFormPage);


/***/ }),

/***/ 12525:
/*!*************************************************************!*\
  !*** ./src/app/hrms/mpr-form/mpr-form.page.scss?ngResource ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = ".example-full-width {\n  width: 100%;\n}\n\n.custom-error {\n  padding-left: 15px;\n}\n\n.footer-back-color {\n  background-color: white;\n}\n\n.footer-btn-color {\n  background-color: #F39E20 !important;\n}\n\n.add-qualification_btn {\n  margin-top: 32px;\n}\n\n.add-button-color {\n  background-color: #F39E20 !important;\n}\n\n@media only screen and (min-height: 768px) and (min-width: 768px) {\n  .resource-required {\n    padding-top: 27px;\n  }\n}\n\n@media (max-width: 820px) {\n  .font-for-mobile {\n    font-size: small;\n  }\n}\n\n::ng-deep .select-text {\n  white-space: pre-line !important;\n}\n\n.back {\n  background-color: aqua;\n  margin: 2px;\n  padding: 5px;\n  border-radius: 10px;\n}\n\n.item-custom-label {\n  margin-left: 16px;\n  color: black;\n}\n\n.add-resource-btn {\n  padding-left: 16px !important;\n}\n\n.custom-range {\n  --knob-background: #F39E20;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1wci1mb3JtLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7QUFDSjs7QUFRQTtFQUNFLGtCQUFBO0FBTEY7O0FBT0E7RUFDRSx1QkFBQTtBQUpGOztBQU1BO0VBQ0Usb0NBQUE7QUFIRjs7QUFLQTtFQUNFLGdCQUFBO0FBRkY7O0FBTUE7RUFDRSxvQ0FBQTtBQUhGOztBQU1BO0VBQ0U7SUFDRSxpQkFBQTtFQUhGO0FBQ0Y7O0FBS0E7RUFDRTtJQUNHLGdCQUFBO0VBSEg7QUFDRjs7QUFPRTtFQUNFLGdDQUFBO0FBTEo7O0FBUUE7RUFDRSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUFMRjs7QUFPQTtFQUNFLGlCQUFBO0VBQ0EsWUFBQTtBQUpGOztBQU1BO0VBQ0UsNkJBQUE7QUFIRjs7QUFLQTtFQUNFLDBCQUFBO0FBRkYiLCJmaWxlIjoibXByLWZvcm0ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmV4YW1wbGUtZnVsbC13aWR0aCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbi5yb3VuZGVkLWNvcm5lcntcbiAgLy9ib3JkZXItcmFkaXVzOiAyMHB4O1xuICAvL2JvcmRlcjogMnB4IHNvbGlkICM4NzhhODQ7XG4gIC8vcGFkZGluZzogMjBweDtcbiAgLy93aWR0aDogMjAwcHg7XG4gIC8vaGVpZ2h0OiAxNTBweDtcbn1cbi5jdXN0b20tZXJyb3J7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbn1cbi5mb290ZXItYmFjay1jb2xvcntcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG4uZm9vdGVyLWJ0bi1jb2xvcntcbiAgYmFja2dyb3VuZC1jb2xvcjogI0YzOUUyMCAhaW1wb3J0YW50O1xufVxuLmFkZC1xdWFsaWZpY2F0aW9uX2J0bntcbiAgbWFyZ2luLXRvcDogMzJweDtcbi8vICB0ZXh0LWFsaWduOiBlbmQ7XG4gLy8gbWFyZ2luLXJpZ2h0OiAxOHB4O1xufVxuLmFkZC1idXR0b24tY29sb3J7XG4gIGJhY2tncm91bmQtY29sb3I6ICNGMzlFMjAgIWltcG9ydGFudDtcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWhlaWdodDogNzY4cHgpIGFuZCAobWluLXdpZHRoOiA3NjhweCl7XG4gIC5yZXNvdXJjZS1yZXF1aXJlZHtcbiAgICBwYWRkaW5nLXRvcDogMjdweDtcbiAgfVxufVxuQG1lZGlhIChtYXgtd2lkdGg6IDgyMHB4KSB7XG4gIC5mb250LWZvci1tb2JpbGUge1xuICAgICBmb250LXNpemU6IHNtYWxsO1xuICB9XG59XG5cbjo6bmctZGVlcHtcbiAgLnNlbGVjdC10ZXh0e1xuICAgIHdoaXRlLXNwYWNlOnByZS1saW5lICFpbXBvcnRhbnQ7XG4gIH1cbn1cbi5iYWNrIHsgXG4gIGJhY2tncm91bmQtY29sb3I6IGFxdWE7XG4gIG1hcmdpbjogMnB4O1xuICBwYWRkaW5nOiA1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG4uaXRlbS1jdXN0b20tbGFiZWx7XG4gIG1hcmdpbi1sZWZ0OiAxNnB4O1xuICBjb2xvcjogYmxhY2s7XG59XG4uYWRkLXJlc291cmNlLWJ0bntcbiAgcGFkZGluZy1sZWZ0OiAxNnB4ICFpbXBvcnRhbnQ7XG59XG4uY3VzdG9tLXJhbmdle1xuICAtLWtub2ItYmFja2dyb3VuZFx0OiAjRjM5RTIwO1xufSJdfQ== */";

/***/ }),

/***/ 78555:
/*!*************************************************************!*\
  !*** ./src/app/hrms/mpr-form/mpr-form.page.html?ngResource ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button *ngIf=\"!existingMPRStatus\"></ion-menu-button>\n      <ion-back-button *ngIf=\"existingMPRStatus\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      MPR Form\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <ion-buttons (click)=\"refreshPage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]=\"MPRForm\">\n    <ion-grid>\n      <ion-card>\n        <!-- Organization -->\n        \n        <ion-row>\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-item>\n              <ion-label position=\"stacked\">Organization<span style=\"color:red!important\">*</span></ion-label>\n                <ion-select placeholder=\"Select Organization\" formControlName=\"organizationDropDown\" \n                  interface=\"popover\" [(ngModel)]=\"selectedOrganization\" (ionChange)=\"onOrganizationChange()\">\n                  <ion-select-option *ngFor=\"let organization of organizationList\" [value]=\"organization\">{{organization.name}}</ion-select-option>\n                </ion-select>\n            </ion-item>\n            <div padding-left class=\"custom-error\">\n              <ng-container *ngFor=\"let validation of validation_messages.organizationDropDownErrorMessage\">\n                <div *ngIf=\"MPRForm.get('organizationDropDown').hasError(validation.type) && MPRForm.get('organizationDropDown').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        <!-- Activity -->\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-item>\n              <ion-label position=\"stacked\">Activity</ion-label>\n              <ion-input [value]=\"activityLbl\" [disabled]=\"true\"></ion-input>\n            </ion-item>\n          </ion-col>\n        <!-- Tagged HR Branch -->\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-item>\n              <ion-label position=\"stacked\">Tagged HR Branch</ion-label>\n              <ion-input [value]=\"taggedHRBranchLbl\" [disabled]=\"true\"></ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <!-- Document Status -->\n        <ion-row>\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-item>\n              <ion-label position=\"stacked\">Reason for MPR<span style=\"color:red!important\">*</span></ion-label>\n                <ion-select placeholder=\"Select Reason for MPR\" formControlName=\"reasonForMPRDropDown\" interface=\"popover\"\n                  (ionChange)=\"onReasonForMPRChange()\">\n                  <ion-select-option *ngFor=\"let reason of reasonForMPRList\" [value]=\"reason\">{{reason.name}}</ion-select-option>\n                </ion-select>\n            </ion-item>\n            <div padding-left class=\"custom-error\">\n              <ng-container *ngFor=\"let validation of validation_messages.reasonForMPRDropDownErrorMessage\">\n                <div *ngIf=\"MPRForm.get('reasonForMPRDropDown').hasError(validation.type) && MPRForm.get('reasonForMPRDropDown').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <!-- Reason for MPR -->\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-item>\n              <ion-label position=\"stacked\">JOB Title<span style=\"color:red!important\">*</span></ion-label>\n                  <ion-select placeholder=\"Select JOB Title\" formControlName=\"jobTitleDropDown]\" interface=\"popover\"\n                    (ionChange)=\"onJobTitleChange()\" [(ngModel)]=\"selectedJOBTitle\">\n                    <ion-select-option *ngFor=\"let  jobTitle of jobTitleList\" [value]=\"jobTitle\">{{jobTitle.JobTitle}}</ion-select-option>\n                  </ion-select>\n            </ion-item>\n            <div padding-left class=\"custom-error\">\n              <ng-container *ngFor=\"let validation of validation_messages.jobTitleDropDownErrorMessage\">\n                <div *ngIf=\"MPRForm.get('jobTitleDropDown').hasError(validation.type) && MPRForm.get('jobTitleDropDown').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <!-- JOB Title -->\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-item>\n              <ion-label position=\"stacked\">Grade<span style=\"color:red!important\">*</span></ion-label>\n                <ion-select placeholder=\"Select Grade\" formControlName=\"gradeDropDown\" interface=\"popover\"\n                  (ionChange)=\"onGradeChange()\" [(ngModel)]=\"selectedGrade\">\n                  <ion-select-option *ngFor=\"let grade of gradeList\" [value]=\"grade\">{{grade.gradename}}</ion-select-option>\n                </ion-select>\n            </ion-item>\n            <div padding-left class=\"custom-error\">\n              <ng-container *ngFor=\"let validation of validation_messages.gradeDropDownErrorMessage\">\n                <div *ngIf=\"MPRForm.get('gradeDropDown').hasError(validation.type) && MPRForm.get('gradeDropDown').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- Grade -->\n        \n        <ion-row>\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-item>\n              <ion-label position=\"stacked\">Designation / Role<span style=\"color:red!important\">*</span></ion-label>\n                  <ion-select placeholder=\"Select Designation / Role\" formControlName=\"designationOrRoleDropDown\" interface=\"popover\"\n                    (ionChange)=\"onDesignationOrRoleChange()\" [(ngModel)]=\"selectedDesignation\">\n                    <ion-select-option *ngFor=\"let designationOrRole of designationOrRoleList\" [value]=\"designationOrRole\">{{designationOrRole.rolename}}</ion-select-option>\n                  </ion-select>\n            </ion-item>\n            <div padding-left class=\"custom-error\">\n              <ng-container *ngFor=\"let validation of validation_messages.designationOrRoleDropDownErrorMessage\">\n                <div\n                  *ngIf=\"MPRForm.get('designationOrRoleDropDown').hasError(validation.type) && MPRForm.get('designationOrRoleDropDown').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <!-- Designation / Role -->\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-item>\n              <ion-label position=\"stacked\">Department<span style=\"color:red!important\">*</span></ion-label>\n                  <ion-select placeholder=\"Select Department\" formControlName=\"departmentDropDown\" interface=\"popover\"\n                    (ionChange)=\"onDepartmentChange()\" [(ngModel)]=\"selectedDepartment\">\n                    <ion-select-option *ngFor=\"let department of departmentList\" [value]=\"department\">{{department.departmentname}}</ion-select-option>\n                  </ion-select>\n            </ion-item>\n            <div padding-left class=\"custom-error\">\n              <ng-container *ngFor=\"let validation of validation_messages.departmentDropDownErrorMessage\">\n                <div *ngIf=\"MPRForm.get('departmentDropDown').hasError(validation.type) && MPRForm.get('departmentDropDown').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <!-- Department -->\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-item>\n              <ion-label position=\"stacked\">Location<span style=\"color:red!important\">*</span></ion-label>\n                <ion-select id=\"demo\" class=\"ion-text-wrap select-text demo\" placeholder=\"Select Location\" formControlName=\"locationDropDown\" interface=\"popover\"\n                  (ionChange)=\"onLocationChange()\" [(ngModel)]=\"selectedLocation\">\n                  <ion-select-option *ngFor=\"let location of locationList\" [value]=\"location\">{{location.locationname}}</ion-select-option>\n                </ion-select>\n            </ion-item>\n            <div padding-left class=\"custom-error\">\n              <ng-container *ngFor=\"let validation of validation_messages.locationDropDownErrorMessage\">\n                <div *ngIf=\"MPRForm.get('locationDropDown').hasError(validation.type) && MPRForm.get('locationDropDown').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-item>\n              <ion-label position=\"stacked\" class=\"asterisk_input\">Number of Position</ion-label>\n              <ion-input type=\"text\" formControlName=\"numberOfPositionTxt\"></ion-input>\n            </ion-item>\n            <div padding-left class=\"custom-error\">\n              <ng-container *ngFor=\"let validation of validation_messages.numberOfPositionTxtErrorMessage\">\n                <div *ngIf=\"MPRForm.get('numberOfPositionTxt').hasError(validation.type) && MPRForm.get('numberOfPositionTxt').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-item>\n              <ion-label position=\"stacked\">Position Closed</ion-label>\n              <ion-input type=\"text\" formControlName=\"positionClosedTxt\" [disabled]=\"true\"></ion-input>\n            </ion-item>\n            <div padding-left class=\"custom-error\">\n              <ng-container *ngFor=\"let validation of validation_messages.positionClosedTxtErrorMessage\">\n                <div *ngIf=\"MPRForm.get('positionClosedTxt').hasError(validation.type) && MPRForm.get('positionClosedTxt').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-label class=\"asterisk_input item-custom-label\">Work Experience</ion-label>\n            <ion-item>\n              <ion-range min=\"0\" max=\"50\" dual-knobs=\"true\" class=\"custom-range\"  (ionChange)=\"onWorkExperienceChange(workExperienceModel)\" [(ngModel)]=\"workExperienceModel\"  [ngModelOptions]=\"{standalone: true}\" pin=\"true\">\n              <ion-label slot=\"start\">{{workExperienceModel_lower}}</ion-label>\n              <ion-label slot=\"end\">{{workExperienceModel_upper}}</ion-label>\n            </ion-range>\n          </ion-item>\n          </ion-col>  \n        </ion-row>\n        <ion-row>\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-label class=\"asterisk_input item-custom-label\">CTC Range (Per Annum)</ion-label>\n            <ion-item>\n                <ion-range min=\"0\" max=\"100\" dual-knobs=\"true\" class=\"custom-range\" (ionChange)=\"onCTCRangeChange(CTCRangeModel)\" [(ngModel)]=\"CTCRangeModel\"  [ngModelOptions]=\"{standalone: true}\" pin=\"true\">\n                <ion-label slot=\"start\">{{CTCRangeModel_lower}}</ion-label>\n                <ion-label slot=\"end\">{{CTCRangeModel_upper}}{{maxCTCLbl}}</ion-label>\n              </ion-range>\n            </ion-item>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-label class=\"asterisk_input item-custom-label\">Age Range</ion-label>\n            <ion-item>\n                <ion-range min=\"0\" max=\"100\" dual-knobs=\"true\" class=\"custom-range\" (ionChange)=\"onAgeRangeChange(AgeRangeModel)\" [(ngModel)]=\"AgeRangeModel\"  [ngModelOptions]=\"{standalone: true}\" pin=\"true\">\n                <ion-label slot=\"start\">{{AgeRangeModel_lower}}</ion-label>\n                <ion-label slot=\"end\">{{AgeRangeModel_upper}}</ion-label>\n              </ion-range>\n            </ion-item>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-item>\n              <ion-label position=\"stacked\">Job Listing<span style=\"color:red!important\">*</span></ion-label>\n                  <ion-select placeholder=\"Select Job Listing\" formControlName=\"jobListingDropDown\" interface=\"popover\"\n                    (ionChange)=\"onLocationChange()\" [(ngModel)]=\"selectedJobListing\">\n                    <ion-select-option *ngFor=\"let jobListing of jobListingList\" [value]=\"jobListing\">{{jobListing.name}}</ion-select-option>\n                  </ion-select>\n            </ion-item>\n            <div padding-left class=\"custom-error\">\n              <ng-container *ngFor=\"let validation of validation_messages.jobListingDropDownErrorMessage\">\n                <div *ngIf=\"MPRForm.get('jobListingDropDown').hasError(validation.type) && MPRForm.get('jobListingDropDown').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label>On Company Roll</ion-label>\n              <ion-toggle slot=\"end\" formControlName=\"onCompanyRollChk\"></ion-toggle>\n           </ion-item>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n      \n      <ion-card>\n        <ion-row>\n          <ion-col size=\"9\">\n            <ion-item>\n              <ion-label position=\"stacked\">Qualification<span style=\"color:red!important\">*</span></ion-label>\n                <ion-select placeholder=\"Select Qualification\" formControlName=\"qualificationDropDown\" interface=\"popover\"\n                  (ionChange)=\"onQualificationChange()\" [(ngModel)]=\"selectedQualification\">\n                  <ion-select-option *ngFor=\"let qualification of qualificationList\" [value]=\"qualification\">{{qualification.name}}</ion-select-option>\n                </ion-select>\n            </ion-item>\n            <div padding-left class=\"custom-error\">\n              <ng-container *ngFor=\"let validation of validation_messages.qualificationDropDownErrorMessage\">\n                <div *ngIf=\"MPRForm.get('qualificationDropDown').hasError(validation.type) && MPRForm.get('qualificationDropDown').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <ion-col size=\"3\" class=\"add-qualification_btn\">\n            <button mat-raised-button color=\"add-button-color\" (click)=\"onAddQualification()\" class=\"add-button-color\" [disabled]=\"MPRForm.controls['qualificationDropDown'].hasError('required')\">Add</button>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-card>\n              <ion-list>\n                <ion-item lines=\"none\" style=\"--background:darkgray !important;max-width: 100% !important;\">\n                  <ion-label>Qualification</ion-label>\n                </ion-item>  \n                <ion-item lines=\"none\" [color]=\"even? 'drak-gray' : 'white'\" *ngFor=\"let selectedQualification of selectedQualificationList;let even = even\" \n                          style=\"max-width: 100% !important;\">\n                  <ion-label>{{selectedQualification.name}}</ion-label>\n                  <button mat-icon-button (click)=\"removeSelectedQualification(selectedQualification)\">\n                    <mat-icon>delete</mat-icon>\n                  </button>\n                </ion-item>\n              </ion-list>\n            </ion-card>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n      \n      <ion-card>\n        <ion-row>\n          <ion-col>\n            <ion-item style=\"max-width: 100% !important;\">\n              <ion-label position=\"stacked\" class=\"asterisk_input\">Job Description</ion-label>\n              <ion-textarea type=\"text\" auto-grow=\"true\" formControlName=\"jobDescriptionTxt\"></ion-textarea>\n            </ion-item>\n            <div padding-left class=\"custom-error\">\n              <ng-container *ngFor=\"let validation of validation_messages.jobDescriptionTxtErrorMessage\">\n                <div *ngIf=\"MPRForm.get('jobDescriptionTxt').hasError(validation.type) && MPRForm.get('jobDescriptionTxt').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col class=\"add-resource-btn\">\n            <button mat-raised-button color=\"add-button-color\" (click)=\"onSaveJD()\" class=\"add-button-color\" [disabled]=\"disable_save_jd_btn\">Save</button>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-card>\n              <ion-list>\n                <ion-item lines=\"none\" style=\"--background:darkgray !important;max-width: 100% !important;\">\n                  <ion-label>Job Descriptions</ion-label>\n                </ion-item>  \n                <ion-item lines=\"none\" [color]=\"even? 'drak-gray' : 'white'\" *ngFor=\"let jobDescription of jobDescriptionList;let even = even\" \n                          style=\"max-width: 100% !important;\">\n                  <ion-label>{{jobDescription.job_description}}</ion-label>\n                  <button mat-icon-button (click)=\"editSelectedJobDescription(jobDescription)\">\n                    <mat-icon>edit</mat-icon>\n                  </button>\n                </ion-item>\n              </ion-list>\n            </ion-card>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n      \n      <ion-card>\n        <ion-row>\n          <!-- Resource Requirement * -->\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-item>\n              <ion-label position=\"stacked\">Resource Requirement<span style=\"color:red!important\">*</span></ion-label>\n                <ion-select placeholder=\"Select Resource Requirement\" formControlName=\"resourceRequirementDropDown\" interface=\"popover\"\n                  (ionChange)=\"onLocationChange()\">\n                  <ion-select-option *ngFor=\"let resourceRequirement of resourceRequirementMasterList\" [value]=\"resourceRequirement\">{{resourceRequirement.name}}</ion-select-option>\n                </ion-select>\n            </ion-item>\n            <div padding-left class=\"custom-error\">\n              <ng-container *ngFor=\"let validation of validation_messages.resourceRequirementDropDownErrorMessage\">\n                <div *ngIf=\"MPRForm.get('resourceRequirementDropDown').hasError(validation.type) && MPRForm.get('resourceRequirementDropDown').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <!-- Resource Department  -->\n          <ion-col size=\"12\" size-lg=\"3\">\n            <ion-item>\n              <ion-label position=\"stacked\">Department<span style=\"color:red!important\">*</span></ion-label>\n                <ion-select placeholder=\"Select Department\" formControlName=\"resourceDepartmentDropDown\" interface=\"popover\"\n                  (ionChange)=\"onLocationChange()\">\n                  <ion-select-option *ngFor=\"let resourceDepartment of resourceDepartmentMasterList\" [value]=\"resourceDepartment\">{{resourceDepartment.name}}</ion-select-option>\n                </ion-select>\n            </ion-item>\n            <div padding-left class=\"custom-error\">\n              <ng-container *ngFor=\"let validation of validation_messages.resourceDepartmentDropDownErrorMessage\">\n                <div *ngIf=\"MPRForm.get('resourceDepartmentDropDown').hasError(validation.type) && MPRForm.get('resourceDepartmentDropDown').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <!-- Required  -->\n          <ion-col size=\"12\" size-lg=\"3\" class=\"resource-required\">\n            <ion-item>\n              <ion-label>Required</ion-label>\n              <ion-toggle slot=\"end\" formControlName=\"resourceRequiredChk\"></ion-toggle>\n            </ion-item>\n            <div padding-left class=\"custom-error\">\n              <ng-container *ngFor=\"let validation of validation_messages.resourceRequiredChkErrorMessage\">\n                <div *ngIf=\"MPRForm.get('resourceRequiredChk').hasError(validation.type) && MPRForm.get('resourceRequiredChk').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n          <ion-col size=\"12\" size-lg=\"3\" class=\"add-resource-btn\">\n            <button mat-raised-button color=\"add-button-color\" (click)=\"onAddResourceRequirement()\" class=\"add-button-color\" [disabled]=\"MPRForm.controls['resourceRequirementDropDown'].hasError('required') || MPRForm.controls['resourceDepartmentDropDown'].hasError('required')\">Add</button>\n          </ion-col>\n        </ion-row>\n        \n        <ion-row>\n          <ion-col>\n          <ion-card>\n          <ion-row>\n            <ion-col no-padding class=\"ion-no-margin\">\n              <ion-item lines=\"none\" style=\"--background:darkgray !important;max-width: 100% !important;\">\n                <ion-label>Requirement</ion-label>\n              </ion-item>\n            </ion-col>\n            <ion-col no-padding class=\"ion-no-margin\">\n              <ion-item lines=\"none\" style=\"--background:darkgray !important;max-width: 100% !important;\"><ion-label>Department</ion-label></ion-item> \n            </ion-col>\n            <ion-col no-padding class=\"ion-no-margin\">\n              <ion-item lines=\"none\" style=\"--background:darkgray !important;max-width: 100% !important;\"><ion-label>Required</ion-label></ion-item> \n            </ion-col>\n          </ion-row>\n          \n          <ion-row>\n            <ion-col>\n              <ion-list>\n                 <ion-item lines=\"none\" [color]=\"even? 'drak-gray' : 'white'\" *ngFor=\"let resourceRequirement of resource_requirement_List;let even = even\" \n                          style=\"max-width: 100% !important;\">\n                  <ion-label class=\"font-for-mobile\">{{resourceRequirement.Resource_requirement.name}}</ion-label>\n                  <ion-label class=\"font-for-mobile\">{{resourceRequirement.Department.name}}</ion-label>\n                  <!-- <ion-label class=\"font-for-mobile\">{{resourceRequirement.Required.isrequired ? \"YES\":\"NO\"}}</ion-label> -->\n                  <button mat-icon-button (click)=\"removeResourceRequirement(resourceRequirement)\">\n                    <mat-icon>delete</mat-icon>\n                  </button>\n                 </ion-item>\n              </ion-list>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n        </ion-row>\n        \n      </ion-card>\n    </ion-grid>\n  </form>\n</ion-content>\n<ion-footer class=\"footer-back-color\">\n  <ion-row *ngIf=\"!existingMPRStatus\">\n    <ion-button style=\"width: 100%;\" no-margin (click)=\"onMPRFormSubmit(MPRForm,'NOT')\"  color=\"primary\"  size=\"large\" expand=\"full\" fill=\"outline\" \n        class=\"footer-btn-color\" [disabled]=\"!MPRForm.valid\">\n        <ion-label style=\"color: white;\">Complete</ion-label>\n    </ion-button>\n  </ion-row>\n  <ion-row *ngIf=\"existingMPRStatus\">\n    <ion-col>\n      <ion-button no-margin class=\"footer-btn-color\" color=\"primary\" size=\"large\" expand=\"full\" fill=\"outline\" (click)=\"onMPRFormSubmit(MPRForm,'true')\"> <ion-label style=\"color: white;\">Approved</ion-label></ion-button>\n    </ion-col>\n    <ion-col>\n      <ion-button no-margin class=\"footer-btn-color\"  color=\"primary\" size=\"large\" expand=\"full\" fill=\"outline\" (click)=\"onMPRFormSubmit(MPRForm,'false')\"> <ion-label style=\"color: white;\">DisApproved</ion-label>\n      </ion-button>\n    </ion-col>\n  </ion-row>\n  \n</ion-footer>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_hrms_mpr-form_mpr-form_module_ts.js.map