"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_order-approval_order-approval_module_ts"],{

/***/ 49026:
/*!*********************************************************!*\
  !*** ./src/app/order-approval/order-approval.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OrderApprovalPageModule": () => (/* binding */ OrderApprovalPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @swimlane/ngx-datatable */ 93143);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _order_approval_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./order-approval.page */ 72754);
/* harmony import */ var _show_approval_details_modal_show_approval_details_modal_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./show-approval-details-modal/show-approval-details-modal.page */ 19592);









const routes = [
    {
        path: '',
        component: _order_approval_page__WEBPACK_IMPORTED_MODULE_0__.OrderApprovalPage
    }
];
let OrderApprovalPageModule = class OrderApprovalPageModule {
};
OrderApprovalPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__.NgxDatatableModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterModule.forChild(routes)
        ],
        declarations: [_order_approval_page__WEBPACK_IMPORTED_MODULE_0__.OrderApprovalPage, _show_approval_details_modal_show_approval_details_modal_page__WEBPACK_IMPORTED_MODULE_1__.ShowApprovalDetailsModalPage],
        entryComponents: [_show_approval_details_modal_show_approval_details_modal_page__WEBPACK_IMPORTED_MODULE_1__.ShowApprovalDetailsModalPage],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_3__.CUSTOM_ELEMENTS_SCHEMA]
    })
], OrderApprovalPageModule);



/***/ }),

/***/ 72754:
/*!*******************************************************!*\
  !*** ./src/app/order-approval/order-approval.page.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OrderApprovalPage": () => (/* binding */ OrderApprovalPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _order_approval_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./order-approval.page.html?ngResource */ 61622);
/* harmony import */ var _order_approval_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./order-approval.page.scss?ngResource */ 25961);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @swimlane/ngx-datatable */ 93143);
/* harmony import */ var _order_approval_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order-approval-service.service */ 44159);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ 80190);
/* harmony import */ var _common_Constants__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../common/Constants */ 68209);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../provider/message-helper */ 98792);
/* harmony import */ var _show_approval_details_modal_show_approval_details_modal_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./show-approval-details-modal/show-approval-details-modal.page */ 19592);















let OrderApprovalPage = class OrderApprovalPage {
  constructor(orderApprovalServiceService, commonfun, alertController, storage, loginauth, router, msg, modalController) {
    this.orderApprovalServiceService = orderApprovalServiceService;
    this.commonfun = commonfun;
    this.alertController = alertController;
    this.storage = storage;
    this.loginauth = loginauth;
    this.router = router;
    this.msg = msg;
    this.modalController = modalController;
    this.TAG = "OrderApprovalPage";
    this.ColumnMode = _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_10__.ColumnMode;
    this.orderData = [];
    this.filterTab = [];
    this.filterOrg = [];
    this.filterDocType = [];
    this.totalFilterApplied = 0;
    this.totalSortApplied = 0;
    this.emptyCart = false; // console.log("Roouter Values",this.router.getCurrentNavigation().extras.state);
  }

  ngOnInit() {
    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {})();
  }

  createURL() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this.getOrderURL = _common_Constants__WEBPACK_IMPORTED_MODULE_6__.Constants.DOMAIN_URL + '/openbravo' + "/ws/in.mbs.webservice.OrderApproval?" + 'user_id=' + _this.loginauth.userid + '&offset=' + _this.orderApprovalServiceService.pageOffset + '&activity_id=' + _this.loginauth.selectedactivity.id; //  this.getOrderURL = Constants.DOMAIN_URL + '/openbravo'+"/ws/in.mbs.webservice.OrderApproval?" 
        //  + '&user='+'hardik.pandya'+'&password='+'pass' + '&user_id=' + 'FFF202001310332122424C1A38AB7A41'
        //  +'&offset=' + this.orderApprovalServiceService.pageOffset
        //  + '&activity_id=' + 'FFF20200217114608722C4817FB0AEB1'  ;

        _this.filterTab = _this.orderApprovalServiceService.filterTab;

        if (!!_this.filterTab && _this.filterTab.length > 0) {
          _this.getOrderURL = _this.getOrderURL.concat('&tab=' + _this.filterTab.toString());
        }

        _this.filterOrg = _this.orderApprovalServiceService.filterOrg;

        if (!!_this.filterOrg && _this.filterOrg.length > 0) {
          _this.getOrderURL = _this.getOrderURL + '&org=' + _this.filterOrg.toString();
        } // this.filterDocType = await this.storage.get('filterDocType');


        _this.filterDocType = _this.orderApprovalServiceService.filterDocType;

        if (!!_this.filterDocType && _this.filterDocType.length > 0) {
          //  this.totalFilterApplied = this.totalFilterApplied + 1;
          _this.getOrderURL = _this.getOrderURL + '&doc_type=' + _this.filterDocType.toString();
        } // this.filterBusinessPartner = await this.storage.get('filterBusinessPartner');


        _this.filterBusinessPartner = _this.orderApprovalServiceService.filterBusinessPartner;

        if (!!_this.filterBusinessPartner && _this.filterBusinessPartner !== 'CLEAR') {
          //  this.totalFilterApplied = this.totalFilterApplied + 1;
          _this.getOrderURL = _this.getOrderURL + '&business=' + _this.filterBusinessPartner;
        }

        _this.filterStartDate = yield _this.storage.get('filterStartDate');

        if (!!_this.filterStartDate && _this.filterStartDate !== 'CLEAR') {
          //  this.totalSortApplied = this.totalSortApplied + 1;
          _this.getOrderURL = _this.getOrderURL + '&start_date=this.filterStartDate' + '&end_date=' + _this.filterEndDate;
        }

        _this.filterEndDate = yield _this.storage.get('filterEndDate');
      } catch (error) {}
    })();
  }

  ionViewWillEnter() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this2.Pageload();
    })();
  }

  Pageload() {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this3.commonfun.loadingPresent();

      _this3.orderApprovalServiceService.pageOffset = 0;
      _this3.filterTab = _this3.orderApprovalServiceService.filterTab;

      if (!!_this3.filterTab && _this3.filterTab.length > 0) {
        _this3.totalFilterApplied = _this3.totalFilterApplied + 1;
      } //this.filterOrg = await this.storage.get('filterOrg');


      _this3.filterOrg = _this3.orderApprovalServiceService.filterOrg;

      if (!!_this3.filterOrg && _this3.filterOrg.length > 0) {
        _this3.totalFilterApplied = _this3.totalFilterApplied + 1;
      } // this.filterDocType = await this.storage.get('filterDocType');


      _this3.filterDocType = _this3.orderApprovalServiceService.filterDocType;

      if (!!_this3.filterDocType && _this3.filterDocType.length > 0) {
        _this3.totalFilterApplied = _this3.totalFilterApplied + 1;
      } // this.filterBusinessPartner = await this.storage.get('filterBusinessPartner');


      _this3.filterBusinessPartner = _this3.orderApprovalServiceService.filterBusinessPartner;

      if (!!_this3.filterBusinessPartner && _this3.filterBusinessPartner !== 'CLEAR') {
        _this3.totalFilterApplied = _this3.totalFilterApplied + 1;
      }

      _this3.createURL();

      _this3.getOrderURL = _this3.getOrderURL.replace(/ /g, "%20");
      _this3.orderData = yield (yield _this3.orderApprovalServiceService.getOrder(_this3.getOrderURL)).toPromise();

      if (!!_this3.orderData && _this3.orderData.length > 0) {
        _this3.emptyCart = true;
      } else {
        _this3.emptyCart = false;
      }

      _this3.commonfun.loadingDismiss();
    })();
  }

  RefreshPage(el) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      el.scrollIntoView();
      _this4.orderApprovalServiceService.filterTab = [];
      _this4.orderApprovalServiceService.filterOrg = [];
      _this4.orderApprovalServiceService.filterDocType = [];
      _this4.orderApprovalServiceService.filterBusinessPartner = '';
      _this4.orderApprovalServiceService.filterselectedBusinessPartner = '';
      _this4.orderApprovalServiceService.pageOffset = 0;
      _this4.totalFilterApplied = 0;

      _this4.ionViewWillEnter();
    })();
  }

  saveConfirm(id, record, tab_id, status) {
    var _this5 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let message;

        if (status == "C") {
          message = "Do you really want to mark this incomplete";
        } else if (status == "A") {
          message = "Do you really want to approve this order";
        }

        const alert = yield _this5.alertController.create({
          header: 'Confirm!',
          message: message,
          buttons: [{
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: blah => {//  console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Okay',
            handler: () => {
              //  console.log('Confirm Okay');
              _this5.save(id, record, tab_id, status, "Approve from Ionic");
            }
          }]
        });
        yield alert.present();
      } catch (error) {}
    })();
  }

  getDetails(id, record, tab_id) {
    var _this6 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const modal = yield _this6.modalController.create({
          component: _show_approval_details_modal_show_approval_details_modal_page__WEBPACK_IMPORTED_MODULE_9__.ShowApprovalDetailsModalPage,
          cssClass: 'my-custom-class',
          componentProps: {
            "id": id,
            "record": record,
            "tab_id": tab_id
          }
        });
        return yield modal.present();
      } catch (error) {}
    })();
  }

  rejectConfirm(id, record, tab_id, status) {
    var _this7 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      let methodTAG = 'rejectConfirm';

      try {
        let alert = yield _this7.alertController.create({
          header: 'Confirm!',
          subHeader: 'Do you really want to reject this order',
          message: '',
          cssClass: 'my-custom-class',
          backdropDismiss: false,
          buttons: [{
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: blah => {//  console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Ok',
            handler: data => {
              if (data.txtRemark != null && data.txtRemark.length > 0) {
                //  console.log('Confirm Okay');
                _this7.save(id, record, tab_id, status, data.txtRemark);
              } else {
                alert.message = '<b style="color: red;">Enter valid remark.</b>';
                return false;
              }
            }
          }],
          inputs: [{
            name: 'txtRemark',
            type: 'text',
            placeholder: 'Enter Remark'
          }]
        });
        yield alert.present();
      } catch (error) {//  console.log(this.TAG,methodTAG,error);
      }
    })();
  }

  save(id, record, tab_id, status, remark) {
    var _this8 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this8.commonfun.loadingPresent();

        let response = yield _this8.orderApprovalServiceService.saveOrderStatus(id, record, tab_id, status, remark).toPromise();

        _this8.commonfun.loadingDismiss(); //  console.log(this.TAG,"Pravin",response);


        if (response.res == "Success") {
          _this8.orderData = _this8.orderData.filter(item => item.id !== id);

          if (!!_this8.orderData && _this8.orderData.length > 0) {
            _this8.emptyCart = false;
          } else {
            _this8.emptyCart = true;
          }

          _this8.commonfun.presentAlert("Order", "", "Your order has been updated successfully");
        } else {
          _this8.commonfun.presentAlert("Order", "", "Some things went wrong please try again late");
        } // this.orderApprovalServiceService.saveOrderStatus(id,record,tab_id,status,remark).subscribe(response =>{
        //   this.commonfun.loadingDismiss();
        //   if(response.res=="Success"){
        //     this.orderData = this.orderData.filter((item) => item.id !== id);
        //     if((!!this.orderData && this.orderData.length > 0)){
        //       this.emptyCart = false;
        //     } else {
        //       this.emptyCart = true;
        //     }
        //     this.commonfun.presentAlert("Order","","Your order has been updated successfully");
        //   }
        //   else {
        //     this.commonfun.presentAlert("Order","","Some things went wrong please try again late");
        //   }
        // });

      } catch (error) {
        //  console.log(this.TAG,"Pravin",error);
        _this8.commonfun.loadingDismiss();

        _this8.commonfun.presentAlert("Order", "Error", error.error);
      }
    })();
  }

  doInfinite(event) {
    var _this9 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this9.orderApprovalServiceService.pageOffset = _this9.orderApprovalServiceService.pageOffset + 20;

        _this9.createURL();

        let tempData = yield (yield _this9.orderApprovalServiceService.getOrder(_this9.getOrderURL)).toPromise();

        if (!!tempData) {
          _this9.orderData = _this9.orderData.concat(tempData); //  event.state = "closed";

          event.target.complete();
        } else {}

        if (!!tempData && tempData.length > 0) {
          _this9.emptyCart = false;
        } else {
          _this9.emptyCart = true;
        }
      } catch (error) {// console.log(this.TAG,error);
      }
    })();
  }

};

OrderApprovalPage.ctorParameters = () => [{
  type: _order_approval_service_service__WEBPACK_IMPORTED_MODULE_3__.OrderApprovalServiceService
}, {
  type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_4__.Commonfun
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__.AlertController
}, {
  type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__.Storage
}, {
  type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_7__.LoginauthService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_12__.Router
}, {
  type: _provider_message_helper__WEBPACK_IMPORTED_MODULE_8__.Message
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__.ModalController
}];

OrderApprovalPage = (0,tslib__WEBPACK_IMPORTED_MODULE_13__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_14__.Component)({
  selector: 'app-order-approval',
  template: _order_approval_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_order_approval_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], OrderApprovalPage);


/***/ }),

/***/ 25961:
/*!********************************************************************!*\
  !*** ./src/app/order-approval/order-approval.page.scss?ngResource ***!
  \********************************************************************/
/***/ ((module) => {

module.exports = "#myFixZone {\n  height: 100px;\n}\n\n.container {\n  width: 100%;\n  padding: 10px;\n  background-color: wheat;\n  overflow: hidden;\n  white-space: nowrap;\n}\n\n.container ::-webkit-scrollbar {\n  display: none;\n}\n\n.container .scroll {\n  overflow: auto;\n}\n\n.selector {\n  line-height: 1;\n  letter-spacing: 0.5px;\n  width: 100% !important;\n  max-width: 100% !important;\n  justify-content: center;\n  border: solid 2px rgb(82, 79, 79);\n  border-radius: 10px;\n  text-align: left;\n}\n\n.rotate-90 {\n  display: inline-block;\n  transform: rotate(90deg);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVyLWFwcHJvdmFsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBQUk7RUFDRSxhQUFBO0FBRU47O0FBQ0k7RUFDRSxjQUFBO0FBQ047O0FBR0U7RUFFRSxjQUFBO0VBQ0EscUJBQUE7RUFFQSxzQkFBQTtFQUNBLDBCQUFBO0VBQ0EsdUJBQUE7RUFDQSxpQ0FBQTtFQUNBLG1CQUFBO0VBRUEsZ0JBQUE7QUFISjs7QUFNRTtFQUNFLHFCQUFBO0VBQ0Esd0JBQUE7QUFISiIsImZpbGUiOiJvcmRlci1hcHByb3ZhbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjbXlGaXhab25lIHtcbiAgICBoZWlnaHQ6IDEwMHB4O1xufVxuXG4uY29udGFpbmVyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6d2hlYXQ7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjsgXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICA6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuICAgIFxuICAgIC5zY3JvbGwge1xuICAgICAgb3ZlcmZsb3c6IGF1dG87XG4gICAgfVxuICB9XG5cbiAgLnNlbGVjdG9yIHtcbiAgICBcbiAgICBsaW5lLWhlaWdodDogMS4wO1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjVweDtcbiAgICBcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICAgIG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGJvcmRlcjogc29saWQgMnB4ICByZ2IoODIsIDc5LCA3OSk7IFxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgLy9iYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cblxuICAucm90YXRlLTkwIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IFxuICAgIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbiAgfSJdfQ== */";

/***/ }),

/***/ 61622:
/*!********************************************************************!*\
  !*** ./src/app/order-approval/order-approval.page.html?ngResource ***!
  \********************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Approval\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <ion-buttons (click)=\"RefreshPage(target)\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <div #target>\n  <ion-row>\n    <ion-col>\n      <ion-item lines=\"full\" [routerDirection]=\"'root'\" [routerLink]=\"['/filter']\">\n         <ion-icon name=\"funnel\" color=\"primary\"></ion-icon>\n         <ion-label>Filters {{totalFilterApplied}}</ion-label>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n</div>\n  <ion-list *ngFor=\"let order of orderData\">\n    <ion-card no-padding>\n      <ion-card-header no-padding>\n        <ion-item-divider style=\"background-color:#80cbc4;\" no-padding>\n          <ion-row style=\"width:-webkit-fill-available; text-align: end\">\n            <ion-col text-left>\n              <div>{{order.tab}}</div>\n            </ion-col>\n            <!-- <ion-col>\n              <div>{{order.approved_by}}</div>\n            </ion-col> -->\n          </ion-row>\n        </ion-item-divider>\n      </ion-card-header>\n      <ion-card-content>\n        <ion-row>\n          <ion-col>\n            <ion-text color=\"primary\" text-left>\n              <h6>Organization</h6>\n            </ion-text>\n            <div>{{order.organization}}</div>\n          </ion-col>\n          <ion-col>\n            <ion-text color=\"primary\">\n              <h6>Document Type</h6>\n            </ion-text>\n            <div>{{order.document_type}}</div>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-text color=\"primary\" text-left>\n              <h6>Business Partner</h6>\n            </ion-text>\n            <div>{{order.business_partner}}</div>\n          </ion-col>\n          <ion-col>\n            <ion-text color=\"primary\" text-left>\n              <h6>Document No.</h6>\n            </ion-text>\n            <div>{{order.document_no}}</div>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <div class=\"container\">\n            <div scrollY=\"true\" class=\"scroll\">\n              <div>\n                <h2>Info1: {{order.info1}}</h2>\n              </div>\n              <div *ngIf=\"order.info2\">\n                <h2>Info2: {{order.info2}}</h2>\n              </div>\n              <div *ngIf=\"order.info3\">\n                <h2>Info3: {{order.info3}}</h2>\n              </div>\n              <div *ngIf=\"order.info4\">\n                <h2>Info4: {{order.info4}}</h2>\n              </div>\n              <div *ngIf=\"order.info5\">\n                <h2>Info5: {{order.info5}}</h2>\n              </div>\n              <div *ngIf=\"order.info6\">\n                <h2>Info6: {{order.info6}}</h2>\n              </div>\n              <div *ngIf=\"order.info7\">\n                <h2>Info7: {{order.info7}}</h2>\n              </div>\n              <div *ngIf=\"order.info8\">\n                <h2>Info8: {{order.info8}}</h2>\n              </div>\n              <div *ngIf=\"order.info9\">\n                <h2>Info9: {{order.info9}}</h2>\n              </div>\n              <div *ngIf=\"order.info10\">\n                <h2>Info10:{{order.info10}}</h2>\n              </div>\n            </div>\n          </div>\n        </ion-row>\n      \n       <ion-row>\n          <ion-col size=\"6\" size-lg=\"3\" no-padding>\n              <ion-button size=\"small\" expand=\"block\" color=\"primary\" (click)=\"rejectConfirm(order.id,order.record,order.tab_id,'D')\">Reject</ion-button>\n          </ion-col>\n          <ion-col size=\"6\" size-lg=\"3\" no-padding>\n              <ion-button size=\"small\" expand=\"block\" color=\"primary\" (click)=\"saveConfirm(order.id,order.record,order.tab_id,'C')\">Incomplete</ion-button>\n          </ion-col>\n          <ion-col size=\"6\" size-lg=\"3\" no-padding>\n              <ion-button size=\"small\" expand=\"block\" color=\"primary\" (click)=\"saveConfirm(order.id,order.record,order.tab_id,'A')\">Approve</ion-button>\n          </ion-col>\n          <ion-col size=\"6\" size-lg=\"3\" *ngIf=\"order.show_query == 'Y'\" no-padding>\n              <ion-button size=\"small\" expand=\"block\" color=\"primary\" (click)=\"getDetails(order.id,order.record,order.tab_id)\">Details</ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-card-content>\n    </ion-card>\n  </ion-list>\n  <ion-infinite-scroll (ionInfinite)=\"doInfinite($event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more data...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n  <section *ngIf=\"emptyCart\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <img src=\"./assets/new_order.svg\" style=\"padding: 20%;\">\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col text-center>\n          <h3>You dont have any order.</h3>\n        </ion-col>\n      </ion-row>\n    </ion-grid>  \n\n  </section>\n\n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_order-approval_order-approval_module_ts.js.map