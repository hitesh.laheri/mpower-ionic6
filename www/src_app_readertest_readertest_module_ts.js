"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_readertest_readertest_module_ts"],{

/***/ 79241:
/*!*************************************************!*\
  !*** ./src/app/readertest/readertest.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ReadertestPageModule": () => (/* binding */ ReadertestPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _readertest_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./readertest.page */ 46480);







const routes = [
    {
        path: '',
        component: _readertest_page__WEBPACK_IMPORTED_MODULE_0__.ReadertestPage
    }
];
let ReadertestPageModule = class ReadertestPageModule {
};
ReadertestPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_readertest_page__WEBPACK_IMPORTED_MODULE_0__.ReadertestPage]
    })
], ReadertestPageModule);



/***/ }),

/***/ 46480:
/*!***********************************************!*\
  !*** ./src/app/readertest/readertest.page.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ReadertestPage": () => (/* binding */ ReadertestPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _readertest_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./readertest.page.html?ngResource */ 17863);
/* harmony import */ var _readertest_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./readertest.page.scss?ngResource */ 51490);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);




let ReadertestPage = class ReadertestPage {
    constructor(ngZone) {
        this.ngZone = ngZone;
        this.window = window;
        this.message = null;
        this.barcodes = [];
        this.status = 'Initialization... Are you on device?';
    }
    ngOnInit() {
        this.listen();
    }
    scanPressed() {
        this.window.plugins.honeywell.softwareTriggerStart(data => {
            this.ngZone.run(() => {
                this.barcodes = ['${data} @ ${new Date().toISOString().replace(/[T,Z]/g, " ").split(\'.\')[0]}', ...this.barcodes];
            });
        }, error => {
            this.ngZone.run(() => {
                this.barcodes = ['${error} @ ${new Date().toISOString().replace(/[T,Z]/g, " ").split(\'.\')[0]}', ...this.barcodes];
            });
        });
    }
    scanReleased() {
        this.window.plugins.honeywell.softwareTriggerStop();
    }
    listen() {
        this.status = `enabled`;
        this.window.plugins.honeywell.listen(data => {
            this.ngZone.run(() => {
                this.barcodes = [`${data} @ ${new Date().toISOString().replace(/[T,Z]/g, ' ').split('.')[0]}`, ...this.barcodes];
            });
        }, error => {
            this.ngZone.run(() => {
                this.barcodes = [`${error} @ ${new Date().toISOString().replace(/[T,Z]/g, ' ').split('.')[0]}`, ...this.barcodes];
            });
        });
    }
    disable() {
        this.status = `disabled`;
        this.window.plugins.honeywell.release(success => {
            this.message = `DISABLE_SUCCESS: ${success}`;
        }, error => {
            this.message = `DISABLE_ERROR: ${error}`;
        });
    }
    enable() {
        this.status = 'enabled';
        this.window.plugins.honeywell.claim(success => {
            this.message = `ENABLE_SUCCESS: ${success}`;
            this.listen();
        }, error => {
            this.message = `ENABLE_ERROR ${error}`;
        });
    }
};
ReadertestPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgZone }
];
ReadertestPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'app-readertest',
        template: _readertest_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_readertest_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ReadertestPage);



/***/ }),

/***/ 51490:
/*!************************************************************!*\
  !*** ./src/app/readertest/readertest.page.scss?ngResource ***!
  \************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZWFkZXJ0ZXN0LnBhZ2Uuc2NzcyJ9 */";

/***/ }),

/***/ 17863:
/*!************************************************************!*\
  !*** ./src/app/readertest/readertest.page.html?ngResource ***!
  \************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>readertest</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <div class=\"ion-padding ion-text-center\">\n    <div *ngIf=\"message\" style=\"margin: 10px 0; padding:20px\">{{message}}</div>\n    <button style=\"margin-bottom: 40px; padding: 20px; width: 100%; color: #ffffff;\" \n    [ngStyle]=\"{'background': status == 'disabled' ? 'darkred' : 'darkgreen'}\" \n    (click)=\"status == 'disabled' ? enable() : disable()\">SCAN READER IS {{status == 'disabled' ? 'DISABLED' : 'ENABLED' }}</button>\n    <button *ngIf=\"status == 'enabled'\" \n    style=\"margin-bottom: 10px; padding: 20px; background: purple; color: #ffffff; width: 100%;\" \n    (touchstart)=\"scanPressed()\" (touchend)=\"scanReleased()\">CLICK AND HOLD TO SCAN</button>\n    <ul>\n      <li *ngFor=\"let code of barcodes\">{{code}}<br></li>\n    </ul>\n  </div>\n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_readertest_readertest_module_ts.js.map