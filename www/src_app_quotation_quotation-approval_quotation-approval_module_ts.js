"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_quotation_quotation-approval_quotation-approval_module_ts"],{

/***/ 16007:
/*!***************************************************************************!*\
  !*** ./src/app/quotation/quotation-approval/quotation-approval.module.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "QuotationApprovalPageModule": () => (/* binding */ QuotationApprovalPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _quotation_approval_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./quotation-approval.page */ 4517);







const routes = [
    {
        path: '',
        component: _quotation_approval_page__WEBPACK_IMPORTED_MODULE_0__.QuotationApprovalPage
    }
];
let QuotationApprovalPageModule = class QuotationApprovalPageModule {
};
QuotationApprovalPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_quotation_approval_page__WEBPACK_IMPORTED_MODULE_0__.QuotationApprovalPage]
    })
], QuotationApprovalPageModule);



/***/ }),

/***/ 4517:
/*!*************************************************************************!*\
  !*** ./src/app/quotation/quotation-approval/quotation-approval.page.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "QuotationApprovalPage": () => (/* binding */ QuotationApprovalPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _quotation_approval_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./quotation-approval.page.html?ngResource */ 73489);
/* harmony import */ var _quotation_approval_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./quotation-approval.page.scss?ngResource */ 96304);
/* harmony import */ var _customer_quotation_customer_quotation_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../customer-quotation/customer-quotation.service */ 23375);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/provider/commonfun */ 51156);








let QuotationApprovalPage = class QuotationApprovalPage {
  constructor(commonFunction, alertCtrl, customerQuotationService) {
    this.commonFunction = commonFunction;
    this.alertCtrl = alertCtrl;
    this.customerQuotationService = customerQuotationService;
    this.TAG = "Quotation Approval Page";
  }

  ngOnInit() {}

  ionViewWillEnter() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this.commonFunction.loadingPresent();

        _this.quotationList = yield _this.customerQuotationService.getQuotationList().toPromise(); //  console.log(this.TAG,this.quotationList);

        _this.commonFunction.loadingDismiss();
      } catch (error) {
        _this.commonFunction.loadingDismiss(); // console.log(this.TAG,error);

      }
    })();
  }

  refreshPage() {
    this.ionViewWillEnter();
  }

  detailsClick(data) {
    try {} catch (error) {//  console.log(this.TAG,error);
    }
  }

  presentAlert(Header, SubHeader, Message) {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this2.alertCtrl.create({
        header: Header,
        subHeader: SubHeader,
        message: Message,
        buttons: [{
          text: "OK",
          handler: ok => {
            _this2.ionViewWillEnter();
          }
        }]
      });
      alert.dismiss(() => console.log('The alert has been closed.'));
      yield alert.present();
    })();
  }

  reject(complaintno) {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        try {
          _this3.commonFunction.loadingPresent();

          const approveQuotationResponse = yield _this3.customerQuotationService.approveQuotation(complaintno, "", true).toPromise();

          _this3.commonFunction.loadingDismiss();

          if (!!approveQuotationResponse) {
            _this3.presentAlert("Quotation Approval", "", approveQuotationResponse.msg);
          }
        } catch (error) {
          //  console.log(this.TAG,error);
          _this3.commonFunction.loadingDismiss();

          _this3.commonFunction.presentAlert("Quotation Approval", "Error", error.error);
        }
      } catch (error) {//  console.log(this.TAG,error);
      }
    })();
  }

  approve(complaintno) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this4.commonFunction.loadingPresent();

        const approveQuotationResponse = yield _this4.customerQuotationService.approveQuotation(complaintno, true, "").toPromise();

        _this4.commonFunction.loadingDismiss();

        if (!!approveQuotationResponse) {
          _this4.presentAlert("Quotation Approval", "", approveQuotationResponse.msg);
        }
      } catch (error) {
        //  console.log(this.TAG,error);
        _this4.commonFunction.loadingDismiss();

        _this4.commonFunction.presentAlert("Quotation Approval", "Error", error.error);
      }
    })();
  }

};

QuotationApprovalPage.ctorParameters = () => [{
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__.Commonfun
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.AlertController
}, {
  type: _customer_quotation_customer_quotation_service__WEBPACK_IMPORTED_MODULE_3__.CustomerQuotationService
}];

QuotationApprovalPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
  selector: 'app-quotation-approval',
  template: _quotation_approval_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_quotation_approval_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], QuotationApprovalPage);


/***/ }),

/***/ 96304:
/*!**************************************************************************************!*\
  !*** ./src/app/quotation/quotation-approval/quotation-approval.page.scss?ngResource ***!
  \**************************************************************************************/
/***/ ((module) => {

module.exports = ".serial-no-row-custom {\n  background-color: bisque;\n  font-weight: bolder;\n  padding: 5px;\n}\n\n.quo-btn-custom {\n  --height: 30px !important;\n  height: 30px !important;\n}\n\n.doc-custom {\n  background-color: #C8E6C9;\n  padding: 5px;\n  border-radius: 5px;\n  color: #1B5E20;\n}\n\n.doc-custom-test {\n  background-color: #E0E0E0;\n  padding: 5px;\n  border-radius: 5px;\n  color: #9E9E9E;\n}\n\n.amc-one-year {\n  background-color: #FFCCBC;\n  padding: 5px;\n  border-radius: 5px;\n  color: #FF5722;\n}\n\n.service-charges-others {\n  background-color: #FFECB3;\n  padding: 5px;\n  border-radius: 5px;\n  color: #FFA000;\n}\n\n.default-custom {\n  background-color: #BBDEFB;\n  padding: 5px;\n  border-radius: 5px;\n  color: #1565C0;\n}\n\n.amc-for-two-Year {\n  background-color: #ffcdd2;\n  padding: 5px;\n  border-radius: 5px;\n  color: #d32f2f;\n}\n\n.amc-for-three-Year {\n  background-color: #F8BBD0;\n  padding: 5px;\n  border-radius: 5px;\n  color: #C2185B;\n}\n\n.camc-for-one-year {\n  background-color: #E1BEE7;\n  padding: 5px;\n  border-radius: 5px;\n  color: #7B1FA2;\n}\n\n.camc-for-two-year {\n  background-color: #D1C4E9;\n  padding: 5px;\n  border-radius: 5px;\n  color: #512DA8;\n}\n\n.camc-for-three-year {\n  background-color: #C8E6C9;\n  padding: 5px;\n  border-radius: 5px;\n  color: #1B5E20;\n}\n\n.extended-warranty-for-one-year {\n  background-color: #B2EBF2;\n  padding: 5px;\n  border-radius: 5px;\n  color: #0097A7;\n}\n\n.extended-warranty-for-two-year {\n  background-color: #B2DFDB;\n  padding: 5px;\n  border-radius: 5px;\n  color: #00796B;\n}\n\n.extended-warranty-for-three-year {\n  background-color: #C8E6C9;\n  padding: 5px;\n  border-radius: 5px;\n  color: #388E3C;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1b3RhdGlvbi1hcHByb3ZhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx3QkFBQTtFQUdBLG1CQUFBO0VBQ0EsWUFBQTtBQURKOztBQUdBO0VBQ0kseUJBQUE7RUFDQSx1QkFBQTtBQUFKOztBQUVBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBQ0o7O0FBQ0E7RUFDSSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFFSjs7QUFBQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQUdKOztBQURBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBSUo7O0FBRkE7RUFDSSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFLSjs7QUFIQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQU1KOztBQUpBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBT0o7O0FBTEE7RUFDSSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFRSjs7QUFOQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQVNKOztBQVBBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBVUo7O0FBUkE7RUFDSSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFXSjs7QUFUQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQVlKOztBQVZBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBYUoiLCJmaWxlIjoicXVvdGF0aW9uLWFwcHJvdmFsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zZXJpYWwtbm8tcm93LWN1c3RvbXtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBiaXNxdWU7XG4gICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiBsaW5lYXItZ3JhZGllbnQodG8gYm90dG9tLCAjZmZlNGM0IDAlLCAjNjk2OTY5IDEwMCUpO1xuICAgIC8vLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMTk3ZGVnLCByZ2JhKDEwMCwxMDAsMTAwLDEpIDAlLCByZ2JhKDYzLDYzLDYzLDEpIDEzLjUlLCByZ2JhKDI5LDI5LDI5LDEpIDMzLjMzJSwgcmdiYSgwLDAsMCwxKSAxMDAlKSAhaW1wb3J0YW50O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgcGFkZGluZzogNXB4O1xufVxuLnF1by1idG4tY3VzdG9te1xuICAgIC0taGVpZ2h0OiAzMHB4ICFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiAzMHB4ICFpbXBvcnRhbnQ7XG59XG4uZG9jLWN1c3RvbXtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNDOEU2Qzk7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBjb2xvcjogIzFCNUUyMDtcbn1cbi5kb2MtY3VzdG9tLXRlc3R7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojRTBFMEUwO1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgY29sb3I6ICM5RTlFOUU7XG59XG4uYW1jLW9uZS15ZWFye1xuICAgIGJhY2tncm91bmQtY29sb3I6I0ZGQ0NCQztcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGNvbG9yOiAjRkY1NzIyO1xufVxuLnNlcnZpY2UtY2hhcmdlcy1vdGhlcnN7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojRkZFQ0IzO1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgY29sb3I6ICNGRkEwMDA7XG59XG4uZGVmYXVsdC1jdXN0b217XG4gICAgYmFja2dyb3VuZC1jb2xvcjojQkJERUZCO1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgY29sb3I6ICMxNTY1QzA7XG59XG4uYW1jLWZvci10d28tWWVhcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNmZmNkZDI7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBjb2xvcjogI2QzMmYyZjtcbn1cbi5hbWMtZm9yLXRocmVlLVllYXJ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojRjhCQkQwO1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgY29sb3I6ICNDMjE4NUI7XG59XG4uY2FtYy1mb3Itb25lLXllYXJ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojRTFCRUU3O1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgY29sb3I6ICM3QjFGQTI7XG59XG4uY2FtYy1mb3ItdHdvLXllYXJ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojRDFDNEU5O1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgY29sb3I6ICM1MTJEQTg7XG59XG4uY2FtYy1mb3ItdGhyZWUteWVhcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNDOEU2Qzk7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBjb2xvcjogIzFCNUUyMDtcbn1cbi5leHRlbmRlZC13YXJyYW50eS1mb3Itb25lLXllYXJ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojQjJFQkYyO1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgY29sb3I6ICMwMDk3QTc7XG59XG4uZXh0ZW5kZWQtd2FycmFudHktZm9yLXR3by15ZWFye1xuICAgIGJhY2tncm91bmQtY29sb3I6I0IyREZEQjtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGNvbG9yOiAjMDA3OTZCO1xufVxuLmV4dGVuZGVkLXdhcnJhbnR5LWZvci10aHJlZS15ZWFye1xuICAgIGJhY2tncm91bmQtY29sb3I6I0M4RTZDOTtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGNvbG9yOiAjMzg4RTNDO1xufSJdfQ== */";

/***/ }),

/***/ 73489:
/*!**************************************************************************************!*\
  !*** ./src/app/quotation/quotation-approval/quotation-approval.page.html?ngResource ***!
  \**************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Quotation Approval</ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <ion-buttons (click)=\"refreshPage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-card class=\"card-custom\" *ngFor=\"let quotation of quotationList\" (click)=\"detailsClick(quotation)\" padding>\n      \n      <ion-row>\n        <ion-col size=\"3\">\n          <ion-label class=\"comname-custom\">\n           {{quotation.documentno}}\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"3\">\n          <ion-label class=\"comname-custom\">\n            {{quotation.orderdate | date:'dd-MMM-yyyy'}}\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"6\" text-end>\n          <ion-label [ngClass]=\"{'camc-for-one-year': quotation.productname =='CAMC1-CAMC for 1 Year',\n                                 'camc-for-three-year' : quotation.productname == 'CAMC for 3 Year',\n                                 'service-charges-others' : quotation.productname == 'Service Charges - Others',\n                                 'amc-one-year' : quotation.productname == 'AMC for 1 Year',\n                                 'default-custom': quotation.productname != 'AMC for 1 Year' &&  quotation.productname != 'Service Charges - Others' \n                                 && quotation.productname != 'CAMC for 3 Year'\n                                 && quotation.productname!= 'CAMC1-CAMC for 1 Year'}\">\n            {{quotation.productname}}\n          </ion-label>\n         </ion-col>\n      </ion-row>\n      \n      <ion-row *ngFor=\"let line of quotation.line\">\n        <ion-col nowrap>\n          <ion-row class=\"serial-no-row-custom\">\n            <ion-label style=\"font-size: small;\">{{ line.serialno }}</ion-label>\n          </ion-row>\n          <ion-row>\n            <ion-col>\n              <ion-label style=\"font-size: small;\">QTY: {{ line.qty }}</ion-label>\n            </ion-col>\n            <ion-col>\n              <ion-label style=\"font-size: small;\">UOM: {{ line.uomname }}</ion-label>\n            </ion-col>\n            <ion-col>\n              <ion-label style=\"font-size: small;\">Rate:{{line.rate}}</ion-label>\n            </ion-col>\n          </ion-row>\n          \n          <ion-row>\n            <ion-col>\n              <ion-label style=\"font-size: small;\">Net Amount: {{ line.netamt }}</ion-label>\n            </ion-col>\n            <ion-col>\n              <ion-label style=\"font-size: small;\">Tax: {{ line.taxname}}</ion-label>\n            </ion-col>\n            <ion-col>\n              <ion-label style=\"font-size: small;\">Total Gross Amount: {{ line.totgrossamt}}</ion-label>\n            </ion-col>\n          </ion-row>\n          <div>\n            \n                  \n          </div>\n       </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-button size=\"default\"\n          class=\"submit-btn quo-btn-custom\" expand=\"block\"  color=\"primary\" (click)=\"reject(quotation)\">Reject\n        </ion-button>\n        </ion-col>\n        <ion-col>\n          <ion-button size=\"default\"\n          class=\"submit-btn quo-btn-custom\" expand=\"block\"  color=\"primary\" (click)=\"approve(quotation)\">Approve\n        </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  </ion-list>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_quotation_quotation-approval_quotation-approval_module_ts.js.map