"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_upload_upload_upload_module_ts"],{

/***/ 69652:
/*!************************************************!*\
  !*** ./src/app/upload/upload/upload.module.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UploadPageModule": () => (/* binding */ UploadPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _upload_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./upload.page */ 53099);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var src_app_common_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/common/pipes/pipes.module */ 22522);
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-file-upload */ 58356);










const routes = [
    {
        path: '',
        component: _upload_page__WEBPACK_IMPORTED_MODULE_0__.UploadPage
    }
];
let UploadPageModule = class UploadPageModule {
};
UploadPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, ionic_selectable__WEBPACK_IMPORTED_MODULE_7__.IonicSelectableModule, src_app_common_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_1__.PipesModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule, ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__.FileUploadModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule.forChild(routes)
        ],
        declarations: [_upload_page__WEBPACK_IMPORTED_MODULE_0__.UploadPage]
    })
], UploadPageModule);



/***/ }),

/***/ 53099:
/*!**********************************************!*\
  !*** ./src/app/upload/upload/upload.page.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UploadPage": () => (/* binding */ UploadPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _upload_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./upload.page.html?ngResource */ 9192);
/* harmony import */ var _upload_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./upload.page.scss?ngResource */ 87896);
/* harmony import */ var _ionic_native_file_chooser_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/file-chooser/ngx */ 86786);
/* harmony import */ var _upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./upload.service */ 57883);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var src_provider_message_helper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/provider/message-helper */ 98792);
/* harmony import */ var _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @awesome-cordova-plugins/file-transfer/ngx */ 80464);
/* harmony import */ var src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/login/loginauth.service */ 44010);
/* harmony import */ var _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @awesome-cordova-plugins/file/ngx */ 25453);
/* harmony import */ var _awesome_cordova_plugins_file_opener_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @awesome-cordova-plugins/file-opener/ngx */ 8456);
/* harmony import */ var _awesome_cordova_plugins_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @awesome-cordova-plugins/android-permissions/ngx */ 71183);
/* harmony import */ var _awesome_cordova_plugins_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @awesome-cordova-plugins/in-app-browser/ngx */ 12407);
/* harmony import */ var _awesome_cordova_plugins_file_path_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @awesome-cordova-plugins/file-path/ngx */ 3501);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _ionic_native_file_picker_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/file-picker/ngx */ 63507);
/* harmony import */ var src_app_common_Constants__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! src/app/common/Constants */ 68209);




















let UploadPage = class UploadPage {
  constructor(formBuilder, commonFunction, uploadService, fileChooser, msg, filePlugin, transfer, loginService, fileOpener, androidPermissions, iab, filePath, fileMeta, platform, filePicker) {
    this.formBuilder = formBuilder;
    this.commonFunction = commonFunction;
    this.uploadService = uploadService;
    this.fileChooser = fileChooser;
    this.msg = msg;
    this.filePlugin = filePlugin;
    this.transfer = transfer;
    this.loginService = loginService;
    this.fileOpener = fileOpener;
    this.androidPermissions = androidPermissions;
    this.iab = iab;
    this.filePath = filePath;
    this.fileMeta = fileMeta;
    this.platform = platform;
    this.filePicker = filePicker;
    this.TAG = "Upload Page";
    this.isdesktop = false;
    this.isFile = false;
    this.options = {
      location: 'yes',
      hidden: 'no',
      clearcache: 'yes',
      clearsessioncache: 'yes',
      zoom: 'yes',
      hardwareback: 'yes',
      mediaPlaybackRequiresUserAction: 'no',
      shouldPauseOnSuspend: 'no',
      closebuttoncaption: 'Close',
      disallowoverscroll: 'no',
      toolbar: 'yes',
      enableViewportScale: 'no',
      allowInlineMediaPlayback: 'no',
      presentationstyle: 'pagesheet',
      fullscreen: 'yes' //Windows only    

    };
  }

  ngOnInit() {
    this.uploadForm = this.formBuilder.group({
      interfaceMasterCategoryCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_16__.Validators.required],
      interfaceMasterSubCategoryCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_16__.Validators.required],
      interfaceMasterNameCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_16__.Validators.required],
      interfaceOrganizationCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_16__.Validators.required]
    });

    if (this.msg.isplatformweb == true) {
      this.isdesktop = true;
    } else {
      this.isdesktop = false;
    }
  }

  ionViewWillEnter() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this.commonFunction.loadingPresent();

        _this.masterCategoryList = yield _this.uploadService.getMasterCategoryList().toPromise();

        if (_this.masterCategoryList.length == 1) {
          _this.selectedMasterCategory = _this.masterCategoryList[0];
        }

        _this.bindOrganizationFromApi();

        _this.commonFunction.loadingDismiss();
      } catch (error) {
        _this.commonFunction.loadingDismiss();

        console.log(_this.TAG, error);
      }
    })();
  } // Master Category Start


  onMasterCategoryChange() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        console.log(_this2.TAG, "Selected Master Category", _this2.selectedMasterCategory.id); //  this.selectedMasterCategory = this.selectedMasterCategory.id;

        _this2.commonFunction.loadingPresent();

        _this2.masterNameList = [];
        _this2.masterSubCategoryList = [];

        _this2.uploadForm.controls['interfaceMasterSubCategoryCtrl'].reset();

        _this2.uploadForm.controls['interfaceMasterNameCtrl'].reset();

        if (!!_this2.selectedMasterCategory.id) {
          _this2.masterSubCategoryList = yield _this2.uploadService.getMasterSubCategoryList(_this2.selectedMasterCategory.id).toPromise();

          if (_this2.masterSubCategoryList.length == 1) {
            _this2.selectedMasterSubCategory = _this2.masterSubCategoryList[0];
          }
        }

        _this2.commonFunction.loadingDismiss();
      } catch (error) {
        _this2.commonFunction.loadingDismiss();

        console.log(_this2.TAG, error);
      }
    })();
  }

  onMasterCategoryClose(event) {
    try {
      event.component._searchText = "";
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  onMasterCategorySearchChange(event) {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  } //Master Sub Category Start


  onMasterSubCategoryChange() {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        // event.component._searchText = "";
        console.log(_this3.TAG, "Selected Master Category", _this3.selectedMasterSubCategory);

        _this3.uploadForm.controls['interfaceMasterNameCtrl'].reset(); // this.selectedMasterSubCategory = this.selectedMasterSubCategory;


        _this3.commonFunction.loadingPresent();

        if (!!_this3.selectedMasterCategory.id) {
          _this3.masterNameList = yield _this3.uploadService.getMasterNameList(_this3.selectedMasterCategory.id, _this3.selectedMasterSubCategory.id).toPromise();
          console.log(_this3.TAG, "Master Name List From Server", _this3.masterNameList);

          if (_this3.masterNameList.length == 1) {
            _this3.selectedMasterName = _this3.masterNameList[0];
          }
        }

        _this3.commonFunction.loadingDismiss();
      } catch (error) {
        console.log(_this3.TAG, error);

        _this3.commonFunction.loadingDismiss();
      }
    })();
  }

  onMasterSubCategoryClose(event) {
    try {
      event.component._searchText = "";
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  onMasterSubCategorySearchChange(event) {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  } // Master Name Start


  onMasterNameChange() {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  }

  onMasterNameClose(event) {
    try {
      event.component._searchText = "";
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  onMasterNameSearchChange(event) {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  } // Organization Start


  onOrganizationChange(selectedOrganization) {
    try {
      console.log(this.TAG, "Organization Selected", selectedOrganization); // this.selectedOrganization = event.value;
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  onOrganizationClose(event) {
    try {
      event.component._searchText = "";
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  onOrganizationSearchChange(event) {
    try {
      var custsearchtext = event.text;

      if (custsearchtext.length % 3 == 0) {
        this.bindOrganizationFromApi(custsearchtext);
      }
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  bindOrganizationFromApi(strsearch) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const cust_response = yield _this4.uploadService.getOrganizationList(strsearch).toPromise();
        console.log(_this4.TAG, cust_response);
        _this4.organizationList = cust_response;
        setTimeout(() => {
          if (_this4.organizationList.length == 1) {
            _this4.uploadForm.controls["interfaceOrganizationCtrl"].setValue(_this4.organizationList[0]);
          }
        }, 100);
      } catch (error) {
        console.log(_this4.TAG, error);
      }
    })();
  }

  uploadFileWeb(str) {
    var _this5 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        console.log(_this5.TAG, str.target.files);
        _this5.file = str.target.files[0];
        console.log(_this5.TAG, _this5.file);
        _this5.isFile = true;
      } catch (error) {
        // this.commonFunction.loadingDismiss();
        console.log(_this5.TAG, error);
      }
    })();
  }

  uploadFileDevice() {
    try {
      if (this.platform.is('android')) {
        this.fileChooser.open().then(uri => {
          this.selectedURI = uri;
          console.log(this.TAG, "Selected File", this.selectedURI);
          this.filePath.resolveNativePath(uri).then(filePathResult => {
            console.log(this.TAG, "Selected fileInfo", filePathResult);
            this.fileName = filePathResult.substring(filePathResult.lastIndexOf("/") + 1);
            let fileType = filePathResult.substring(filePathResult.lastIndexOf(".") + 1);
            console.log(this.TAG, "Selected fileInfo File Name", this.fileName);
            console.log(this.TAG, "Selected fileInfo File fileType", fileType);
            this.isFile = true;
          });
        }).catch(e => console.log(e));
      } else if (this.platform.is('ios')) {
        this.filePicker.pickFile().then(uri => {
          this.isFile = true;
          this.selectedURI = uri;
          this.fileName = uri.substring(uri.lastIndexOf("/") + 1);
          let fileType = uri.substring(uri.lastIndexOf(".") + 1);
        }).catch(err => console.log('File Picker Error', err));
      }
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  validate() {
    var _this6 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        if (_this6.msg.isplatformweb == true) {
          _this6.commonFunction.loadingPresent();

          let files = _this6.file;

          if (files.name != "") {
            let formData = new FormData();
            let i = 0;
            formData.append('inpFile', _this6.file, _this6.file.name);
            formData.append('inpmuulinterfaceConfigId', _this6.selectedMasterName.id);
            formData.append('validator', _this6.selectedMasterName.validator);
            formData.append('java_process', _this6.selectedMasterName.java_process);
            formData.append('inporganizationId', _this6.selectedOrganization.id);
            formData.append('validatedorexecuter', 'VALIDATOR');
            _this6.formatResponse = yield _this6.uploadService.uploadFileService(formData).toPromise();

            if (!!_this6.formatResponse) {
              console.log(_this6.TAG, _this6.formatResponse);

              _this6.commonFunction.loadingDismiss(); //  if(this.formatResponse.msgType == "ValidateError"){
              //  } 


              _this6.commonFunction.presentAlert("Upload", "Validate", _this6.formatResponse.msg);
            }

            _this6.commonFunction.loadingDismiss();
          } else {
            _this6.commonFunction.loadingDismiss();

            _this6.commonFunction.presentAlert("Upload", "Validate", "Please Select File");
          }
        } else if (_this6.platform.is('android')) {
          _this6.commonFunction.loadingPresent();

          let validateData = {
            "inpmuulinterfaceConfigId": _this6.selectedMasterName.id,
            "validator": _this6.selectedMasterName.validator,
            "java_process": _this6.selectedMasterName.java_process,
            "validatedorexecuter": 'VALIDATOR',
            "inporganizationId": _this6.selectedOrganization.id
          };
          _this6.formatResponse = yield _this6.uploadService.uploadFileServiceAndroidiOS(validateData, _this6.selectedURI).toPromise();

          if (!!_this6.formatResponse) {
            console.log(_this6.TAG, _this6.formatResponse);

            _this6.commonFunction.loadingDismiss();

            _this6.commonFunction.presentAlert("Upload", "Validate", _this6.formatResponse.msg);
          } else {
            _this6.commonFunction.loadingDismiss();
          }
        } else if (_this6.platform.is('ios')) {
          let login = _this6.loginService.user;
          let password = _this6.loginService.pass;
          const auth = btoa(login + ":" + password);
          let save_file_url = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_15__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.UploadService?';

          const fileTransfer = _this6.transfer.create();

          let options = {
            fileKey: 'inpFile',
            fileName: _this6.fileName,
            params: {
              "inpmuulinterfaceConfigId": _this6.selectedMasterName.id,
              "validator": _this6.selectedMasterName.validator,
              "java_process": _this6.selectedMasterName.java_process,
              "validatedorexecuter": 'VALIDATOR',
              "inporganizationId": _this6.selectedOrganization.id
            },
            headers: {
              'Authorization': 'Basic ' + auth
            }
          };
          fileTransfer.upload(_this6.selectedURI, save_file_url, options).then(data => {
            console.log("pravin YESSSSS", data);
            _this6.formatResponse = JSON.parse(data.response);
            console.log("File Uplaod Result", _this6.formatResponse);

            _this6.commonFunction.presentAlert("Upload", "Validate", _this6.formatResponse.msg);

            _this6.commonFunction.loadingDismiss();
          }, err => {
            console.log("pravin naaaaaaaa", err);

            _this6.commonFunction.loadingDismiss();
          });
        }
      } catch (error) {
        _this6.commonFunction.loadingDismiss();

        console.log(_this6.TAG, error);
      }
    })();
  }

  process() {
    var _this7 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        if (_this7.msg.isplatformweb == true) {
          _this7.commonFunction.loadingPresent();

          let files = _this7.file;

          if (files.name != "") {
            let processFormData = new FormData();
            let i = 0;
            processFormData.append('inpFile', _this7.file, _this7.file.name);
            processFormData.append('inpmuulinterfaceConfigId', _this7.selectedMasterName.id);
            processFormData.append('validator', _this7.selectedMasterName.validator);
            processFormData.append('java_process', _this7.selectedMasterName.java_process);
            processFormData.append('inporganizationId', _this7.selectedOrganization.id);
            processFormData.append('validatedorexecuter', 'EXECUTOR');
            processFormData.append('sessionIdval', _this7.formatResponse.sessionVal);
            let processResponse = yield _this7.uploadService.uploadFileService(processFormData).toPromise();

            if (!!processResponse) {
              console.log(_this7.TAG, processResponse);

              if (processResponse.msgType == "Success") {
                _this7.refreshPage();
              }

              _this7.commonFunction.presentAlert("Upload", processResponse.msgType, processResponse.msg);
            }

            _this7.commonFunction.loadingDismiss();
          } else {
            _this7.commonFunction.loadingDismiss();

            _this7.commonFunction.presentAlert("Upload", "Validate", "Please Select File");
          }
        } else if (_this7.platform.is('android')) {
          _this7.commonFunction.loadingPresent();

          let validateData = {
            "inpmuulinterfaceConfigId": _this7.selectedMasterName.id,
            "validator": _this7.selectedMasterName.validator,
            "java_process": _this7.selectedMasterName.java_process,
            "validatedorexecuter": 'EXECUTOR',
            "inporganizationId": _this7.selectedOrganization.id,
            "sessionIdval": _this7.formatResponse.sessionVal
          };
          _this7.formatResponse = yield _this7.uploadService.uploadFileServiceAndroidiOS(validateData, _this7.selectedURI).toPromise();

          if (!!_this7.formatResponse) {
            _this7.commonFunction.loadingDismiss();

            console.log(_this7.TAG, _this7.formatResponse);
            console.log(_this7.TAG, _this7.formatResponse);

            if (_this7.formatResponse.msgType == "Success") {
              _this7.refreshPage();
            }

            _this7.commonFunction.presentAlert("Upload", _this7.formatResponse.msgType, _this7.formatResponse.msg);
          } else {
            _this7.commonFunction.loadingDismiss();
          }
        } else if (_this7.platform.is('ios')) {
          let login = _this7.loginService.user;
          let password = _this7.loginService.pass;
          const auth = btoa(login + ":" + password);
          let save_file_url = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_15__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.UploadService?';

          const fileTransfer = _this7.transfer.create();

          let options = {
            fileKey: 'inpFile',
            fileName: _this7.fileName,
            params: {
              "inpmuulinterfaceConfigId": _this7.selectedMasterName.id,
              "validator": _this7.selectedMasterName.validator,
              "java_process": _this7.selectedMasterName.java_process,
              "validatedorexecuter": 'EXECUTOR',
              "inporganizationId": _this7.selectedOrganization.id,
              "sessionIdval": _this7.formatResponse.sessionVal
            },
            headers: {
              'Authorization': 'Basic ' + auth
            }
          };
          fileTransfer.upload(_this7.selectedURI, save_file_url, options).then(data => {
            console.log("pravin YESSSSS", data);
            let reposnse = JSON.parse(data.response);
            console.log("File Uplaod Result", reposnse);

            _this7.refreshPage();

            _this7.commonFunction.presentAlert("Upload", "Validate", reposnse.msg);

            _this7.commonFunction.loadingDismiss();
          }, err => {
            console.log("pravin naaaaaaaa", err);

            _this7.commonFunction.loadingDismiss();
          });
        }
      } catch (error) {
        _this7.commonFunction.loadingDismiss();

        console.log(_this7.TAG, error);
      }
    })();
  }

  format() {
    var _this8 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        if (!!_this8.selectedMasterName.doc_name) {
          if (_this8.msg.isplatformweb == false) {
            _this8.androidPermissions.hasPermission(_this8.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(status => {
              if (status.hasPermission) {
                _this8.androidPermissions.requestPermission(_this8.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(status => {
                  if (status.hasPermission) {
                    _this8.downloadFile();
                  }
                });
              } else {
                _this8.androidPermissions.requestPermission(_this8.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(status => {
                  if (status.hasPermission) {
                    _this8.androidPermissions.requestPermission(_this8.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(status => {
                      if (status.hasPermission) {
                        _this8.downloadFile();
                      }
                    });
                  }
                });
              }
            });
          } else {
            _this8.downloadFile();
          }
        } else {
          _this8.commonFunction.presentAlert("Upload", "Download", "Server does not have a file");
        }
      } catch (error) {
        console.log(_this8.TAG, error);
      }
    })();
  }

  downloadFile() {
    var _this9 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let fileDownloadURL = _this9.loginService.commonurl + 'ws/in.mbs.webservice.InterfaceDownload?' + _this9.loginService.parameter + '&recordId=' + _this9.selectedMasterName.id + '&user_id=' + _this9.loginService.userid;
        console.log(_this9.TAG, "getMasterCategory", fileDownloadURL);

        if (_this9.msg.isplatformweb == false) {
          let path;

          if (_this9.platform.is('android')) {
            path = _this9.filePlugin.externalRootDirectory + '/Download/';
          } else if (_this9.platform.is('ios')) {
            path = _this9.filePlugin.documentsDirectory;
          }

          const fileTransfer = _this9.transfer.create();

          fileTransfer.download(encodeURI(fileDownloadURL), path + _this9.selectedMasterName.doc_name).then(entry => {
            console.log('download complete: ' + entry.toURL());

            _this9.fileOpener.open(entry.toURL(), "text/csv").then(() => console.log("File is opened")).catch(e => console.log("Error opening file", e));
          }, error => {
            console.log('error download complete: ', error);
          });
        } else {
          let target = "_blank";

          _this9.iab.create(fileDownloadURL, target, _this9.options);
        }
      } catch (error) {
        console.log(_this9.TAG, error);
      }
    })();
  }

  refreshPage() {
    try {
      this.masterNameList = [];
      this.masterSubCategoryList = [];
      this.selectedMasterCategory = "";
      this.selectedMasterSubCategory = "";
      this.selectedMasterName = "";
      this.selectedOrganization = "";
      this.uploadForm.reset(); // this.uploadForm.controls['interfaceMasterCategoryCtrl'].reset();
      // this.uploadForm.controls['interfaceMasterSubCategoryCtrl'].reset();
      // this.uploadForm.controls['interfaceMasterNameCtrl'].reset();
      // this.uploadForm.controls['interfaceOrganizationCtrl'].reset();

      this.isFile = false;
      this.fileName = "";
      this.selectedURI = "";
      this.inputFileCtrl.nativeElement.value = ''; //this.fileField.clearAllFiles();
      // this.ionViewWillEnter();
    } catch (error) {}
  }

};

UploadPage.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_16__.FormBuilder
}, {
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_5__.Commonfun
}, {
  type: _upload_service__WEBPACK_IMPORTED_MODULE_4__.UploadService
}, {
  type: _ionic_native_file_chooser_ngx__WEBPACK_IMPORTED_MODULE_3__.FileChooser
}, {
  type: src_provider_message_helper__WEBPACK_IMPORTED_MODULE_6__.Message
}, {
  type: _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_9__.File
}, {
  type: _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_7__.FileTransfer
}, {
  type: src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_8__.LoginauthService
}, {
  type: _awesome_cordova_plugins_file_opener_ngx__WEBPACK_IMPORTED_MODULE_10__.FileOpener
}, {
  type: _awesome_cordova_plugins_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_11__.AndroidPermissions
}, {
  type: _awesome_cordova_plugins_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_12__.InAppBrowser
}, {
  type: _awesome_cordova_plugins_file_path_ngx__WEBPACK_IMPORTED_MODULE_13__.FilePath
}, {
  type: _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_9__.File
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_17__.Platform
}, {
  type: _ionic_native_file_picker_ngx__WEBPACK_IMPORTED_MODULE_14__.IOSFilePicker
}];

UploadPage.propDecorators = {
  inputFileCtrl: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_18__.ViewChild,
    args: ['inputFileCtrl', {
      static: false
    }]
  }]
};
UploadPage = (0,tslib__WEBPACK_IMPORTED_MODULE_19__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_18__.Component)({
  selector: 'app-upload',
  template: _upload_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_upload_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], UploadPage);


/***/ }),

/***/ 87896:
/*!***********************************************************!*\
  !*** ./src/app/upload/upload/upload.page.scss?ngResource ***!
  \***********************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1cGxvYWQucGFnZS5zY3NzIn0= */";

/***/ }),

/***/ 9192:
/*!***********************************************************!*\
  !*** ./src/app/upload/upload/upload.page.html?ngResource ***!
  \***********************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Upload\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <ion-buttons (click)=\"refreshPage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]=\"uploadForm\">\n  <ion-card> \n    <!-- Interface Master Category -->\n  <ion-row>\n    <ion-col>\n      <ion-item>\n        <ion-label position=\"stacked\">Interface Master Category<span style=\"color:red!important\">*</span></ion-label>\n          <ion-select [(ngModel)]=\"selectedMasterCategory\" (ionChange)=\"onMasterCategoryChange()\" interface=\"popover\" formControlName=\"interfaceMasterCategoryCtrl\"\n                      multiple=\"false\" placeholder=\"Interface Master Category\">\n          <ion-select-option *ngFor=\"let masterCategory of masterCategoryList\" [value]=\"masterCategory\">{{masterCategory.name}}\n          </ion-select-option>\n        </ion-select>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n  <!-- Interface Master Sub Category -->\n  <ion-row>\n    <ion-col>\n      <ion-item>\n        <ion-label position=\"stacked\">Interface Master Sub Category<span style=\"color:red!important\">*</span></ion-label>\n          <ion-select [(ngModel)]=\"selectedMasterSubCategory\" (ionChange)=\"onMasterSubCategoryChange()\" interface=\"popover\" formControlName=\"interfaceMasterSubCategoryCtrl\"\n                      multiple=\"false\" placeholder=\"Select Master Sub Category\">\n          <ion-select-option *ngFor=\"let masterSubCategory of masterSubCategoryList\" [value]=\"masterSubCategory\">{{masterSubCategory.name}}\n          </ion-select-option>\n        </ion-select>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n  <!-- Interface Master Name -->\n  <ion-row>\n    <ion-col>\n      <ion-item>\n        <ion-label position=\"stacked\">Interface Master Name<span style=\"color:red!important\">*</span></ion-label>\n          <ion-select [(ngModel)]=\"selectedMasterName\" (ionChange)=\"onMasterNameChange()\" interface=\"popover\" formControlName=\"interfaceMasterNameCtrl\"\n                      multiple=\"false\" placeholder=\"Select Master Name\">\n          <ion-select-option *ngFor=\"let masterName of masterNameList\" [value]=\"masterName\">{{masterName.name}}\n          </ion-select-option>\n          </ion-select>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n  <!-- Organization -->\n  <ion-row>\n    <ion-col>\n      <ion-item>\n        <ion-label position=\"stacked\">Organization<span style=\"color:red!important\">*</span></ion-label>\n        <ion-select [(ngModel)]=\"selectedOrganization\" (ionChange)=\"onOrganizationChange(selectedOrganization)\" interface=\"popover\" formControlName=\"interfaceOrganizationCtrl\"\n                      multiple=\"false\" placeholder=\"Select Organization\">\n          <ion-select-option *ngFor=\"let organization of organizationList\" [value]=\"organization\">{{organization.name}}\n          </ion-select-option>\n          </ion-select>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col>\n      <ion-item>\n        <!-- <app-multi-file-upload></app-multi-file-upload> -->\n        <section *ngIf=\"isdesktop===true\">\n          <ion-col size=\"2\">\n             <input type=\"file\" name=\"file\" id='inputFileCtrl' (change)=\"uploadFileWeb($event)\" #inputFileCtrl class=\"inputfile\"/>\n           </ion-col>\n         </section>\n         <section *ngIf=\"isdesktop===false\">\n           <ion-button color=\"light\"(click)=\"uploadFileDevice()\">Select File\n             <ion-icon color=\"primary\" name=\"Photos\" slot=\"icon-only\"></ion-icon>\n           </ion-button>\n           <ion-label>{{fileName}}</ion-label>\n         </section>\n     \n      </ion-item>\n   </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col>\n      <ion-button size=\"default\" class=\"submit-btn\" [disabled]=\"!uploadForm.valid || !isFile\" expand=\"block\" color=\"primary\" (click)=\"validate()\">Validate\n      </ion-button>\n    </ion-col>\n    <ion-col>\n      <ion-button size=\"default\" class=\"submit-btn\"  [disabled]=\"!uploadForm.valid || !isFile\" expand=\"block\" color=\"primary\" (click)=\"process()\">Process\n      </ion-button>\n    </ion-col>\n    <ion-col>\n      <ion-button size=\"default\" class=\"submit-btn\"  [disabled]=\"!uploadForm.valid\" expand=\"block\" color=\"primary\" (click)=\"format()\">Format\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</ion-card>  \n</form>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_upload_upload_upload_module_ts.js.map