"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_modals_page-name_page-name_module_ts"],{

/***/ 44354:
/*!******************************************************!*\
  !*** ./src/app/modals/page-name/page-name.module.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PageNamePageModule": () => (/* binding */ PageNamePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _page_name_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./page-name.page */ 13519);







const routes = [
    {
        path: '',
        component: _page_name_page__WEBPACK_IMPORTED_MODULE_0__.PageNamePage
    }
];
let PageNamePageModule = class PageNamePageModule {
};
PageNamePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_page_name_page__WEBPACK_IMPORTED_MODULE_0__.PageNamePage]
    })
], PageNamePageModule);



/***/ }),

/***/ 13519:
/*!****************************************************!*\
  !*** ./src/app/modals/page-name/page-name.page.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PageNamePage": () => (/* binding */ PageNamePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _page_name_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./page-name.page.html?ngResource */ 78243);
/* harmony import */ var _page_name_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./page-name.page.scss?ngResource */ 95103);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);




let PageNamePage = class PageNamePage {
    constructor() { }
    ngOnInit() {
    }
};
PageNamePage.ctorParameters = () => [];
PageNamePage = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-page-name',
        template: _page_name_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_page_name_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], PageNamePage);



/***/ }),

/***/ 95103:
/*!*****************************************************************!*\
  !*** ./src/app/modals/page-name/page-name.page.scss?ngResource ***!
  \*****************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwYWdlLW5hbWUucGFnZS5zY3NzIn0= */";

/***/ }),

/***/ 78243:
/*!*****************************************************************!*\
  !*** ./src/app/modals/page-name/page-name.page.html?ngResource ***!
  \*****************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>page-name</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_modals_page-name_page-name_module_ts.js.map