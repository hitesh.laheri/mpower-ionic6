"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_bom_bom_module_ts"],{

/***/ 12856:
/*!***********************************!*\
  !*** ./src/app/bom/bom.module.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BomPageModule": () => (/* binding */ BomPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _bom_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bom.page */ 177);







const routes = [
    {
        path: '',
        component: _bom_page__WEBPACK_IMPORTED_MODULE_0__.BomPage
    }
];
let BomPageModule = class BomPageModule {
};
BomPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_bom_page__WEBPACK_IMPORTED_MODULE_0__.BomPage]
    })
], BomPageModule);



/***/ }),

/***/ 177:
/*!*********************************!*\
  !*** ./src/app/bom/bom.page.ts ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BomPage": () => (/* binding */ BomPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _bom_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bom.page.html?ngResource */ 25370);
/* harmony import */ var _bom_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bom.page.scss?ngResource */ 73039);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);






let BomPage = class BomPage {
    constructor(loginservc, loadingCtrl) {
        this.loginservc = loginservc;
        this.loadingCtrl = loadingCtrl;
        this.loginservc.getbomlist().subscribe(data => {
            const response = data['response'];
            this.bomlist = response['data'];
            this.filterbomList = this.bomlist;
        });
    }
    ngOnInit() {
    }
    onSearchChange(ev) {
        // Reset items back to all of the items
        this.filterbomList = this.bomlist;
        const val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() !== '') {
            this.filterbomList = this.filterbomList.filter((bom) => {
                return (bom._identifier.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    }
};
BomPage.ctorParameters = () => [
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.LoadingController }
];
BomPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-bom',
        template: _bom_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_bom_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], BomPage);



/***/ }),

/***/ 73039:
/*!**********************************************!*\
  !*** ./src/app/bom/bom.page.scss?ngResource ***!
  \**********************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJib20ucGFnZS5zY3NzIn0= */";

/***/ }),

/***/ 25370:
/*!**********************************************!*\
  !*** ./src/app/bom/bom.page.html?ngResource ***!
  \**********************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n      <ion-buttons slot=\"start\">\n          <ion-back-button defaultHref=\"profile\"></ion-back-button>\n        </ion-buttons>\n    <ion-title>Bill Of Material</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [scrollEvents]=\"true\">\n  <ion-searchbar placeholder=\"Filter BOM\"\n           (ionChange)=\"onSearchChange($event)\" [debounce]=\"250\"></ion-searchbar>\n  <ion-list>\n    <ion-item *ngFor=\"let bom of filterbomList\">\n      {{ bom._identifier }}\n    </ion-item>\n  </ion-list>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_bom_bom_module_ts.js.map