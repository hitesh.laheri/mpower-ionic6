"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_quotation_customer-quotation_customer-quotation_module_ts"],{

/***/ 61382:
/*!***************************************************************************!*\
  !*** ./src/app/quotation/customer-quotation/customer-quotation.module.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CustomerQuotationPageModule": () => (/* binding */ CustomerQuotationPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _customer_quotation_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./customer-quotation.page */ 10840);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var src_app_common_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/common/pipes/pipes.module */ 22522);









const routes = [
    {
        path: '',
        component: _customer_quotation_page__WEBPACK_IMPORTED_MODULE_0__.CustomerQuotationPage
    }
];
let CustomerQuotationPageModule = class CustomerQuotationPageModule {
};
CustomerQuotationPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, ionic_selectable__WEBPACK_IMPORTED_MODULE_7__.IonicSelectableModule, src_app_common_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_1__.PipesModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterModule.forChild(routes)
        ],
        declarations: [_customer_quotation_page__WEBPACK_IMPORTED_MODULE_0__.CustomerQuotationPage]
    })
], CustomerQuotationPageModule);



/***/ }),

/***/ 10840:
/*!*************************************************************************!*\
  !*** ./src/app/quotation/customer-quotation/customer-quotation.page.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CustomerQuotationPage": () => (/* binding */ CustomerQuotationPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _customer_quotation_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./customer-quotation.page.html?ngResource */ 509);
/* harmony import */ var _customer_quotation_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./customer-quotation.page.scss?ngResource */ 11280);
/* harmony import */ var _customer_quotation_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./customer-quotation.service */ 23375);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/login/loginauth.service */ 44010);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! date-fns */ 86527);












let CustomerQuotationPage = class CustomerQuotationPage {
  constructor(formBuilder, commonFunction, alertCtrl, router, route, customerQuotationService, loginService, loginauth) {
    this.formBuilder = formBuilder;
    this.commonFunction = commonFunction;
    this.alertCtrl = alertCtrl;
    this.router = router;
    this.route = route;
    this.customerQuotationService = customerQuotationService;
    this.loginService = loginService;
    this.loginauth = loginauth;
    this.TAG = "Customer Quotation Page";
    this.reftextcount = 0;
    this.hasSerialNumber = false;
    this.showComplainNumber = false;
    this.datetoday = '';
    this.now = new Date();
    this.year = this.now.getFullYear();
    this.month = this.now.getMonth() + 1;
    this.day = this.now.getDate();
    /**
     *
     */

    this.maxDate = this.year + "-" + this.month + "-" + this.day;
    this.today = this.year + "-" + ("0" + this.month).slice(-2) + "-" + ("0" + this.day).slice(-2);
    this.validation_messages = {
      'selectedBusinessPartner': [{
        type: 'required',
        message: '*Please Select Customer.'
      }],
      'custAddShippingCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Shipping Address.'
      }],
      'custAddBillingCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Billing Address.'
      }]
    };
  }

  ngOnInit() {
    this.customerQuotationForm = this.formBuilder.group({
      customerCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required],
      selectedBPaddressShippingCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required],
      selectedBPAddressBillingCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required],
      orderDateCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required],
      serviceProductCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required],
      complaintNoCtrl: [,]
    }); //this.customerQuotationForm.get('orderDateCtrl').disable();
  }

  formatDate(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_7__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_8__["default"])(value), 'dd.MM.yyyy');
  }

  ionViewWillEnter() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // console.log(this.TAG,"ionViewWillEnter Fired");
      _this.commonFunction.loadingPresent();

      _this.Servicepricebasedon = _this.loginService.selectedactivity.Servicepricebasedon; // console.log(this.TAG,"Servicepricebasedon",this.Servicepricebasedon);

      let tempResponse = yield _this.customerQuotationService.getCustomer().toPromise(); // tempResponse =  tempResponse.slice(0,9);

      if (!!tempResponse.length) {
        if (tempResponse.length > _this.loginauth.minlistcount) {
          _this.businessPartnerList = [];
        } else {
          _this.businessPartnerList = tempResponse;

          if (_this.businessPartnerList.length == 1) {
            _this.customerQuotationForm.controls["customerCtrl"].setValue(_this.businessPartnerList[0]);

            _this.selectedBusinessPartner = _this.businessPartnerList[0];

            _this.bindCustomerBillingAddress();

            _this.tempServiceProductResponse = yield _this.customerQuotationService.getServiceProduct("", _this.selectedBusinessPartner.id).toPromise();
          } else {
            _this.dontClearCustomerQuotation = true;
          }
        }
      }

      if (!!_this.tempServiceProductResponse) {
        if (_this.tempServiceProductResponse.length > _this.loginauth.minlistcount) {
          _this.serviceProductList = "";
        } else {
          _this.serviceProductList = _this.tempServiceProductResponse;

          if (_this.serviceProductList.length == 1) {
            _this.customerQuotationForm.controls["serviceProductCtrl"].setValue(_this.serviceProductList[0]);

            _this.selectedServiceProduct = _this.serviceProductList[0]; // this.customerQuotationForm.get('serviceProductCtrl').disable();
          } else {
            _this.dontClearCustomerQuotation = true;
          }
        }
      }

      _this.bindData(); // let routeData = this.route.snapshot.params['cartList'];


      _this.commonFunction.loadingDismiss();
    })();
  }

  bindData() {
    try {
      this.route.queryParams.subscribe(data => {
        if (!!data["cartList"] && data["cartList"].length > 0) {
          this.selectedSerialNumberProductList = JSON.parse(data["cartList"]); //  console.log(this.TAG,"Previous Page Data Cart List",this.selectedSerialNumberProductList);

          this.previousPageData = JSON.parse(data["selectedQuotationData"]); //  console.log(this.TAG,"Previous Page Data",this.previousPageData);

          this.hasSerialNumber = true;

          if (!!this.previousPageData.selectedBusinessPartnerKey) {
            this.customerQuotationForm.controls["customerCtrl"].setValue(this.previousPageData.selectedBusinessPartnerKey);
            this.selectedBusinessPartner = this.previousPageData.selectedBusinessPartnerKey;
            this.customerQuotationForm.get('customerCtrl').disable();
            this.customerQuotationForm.get('selectedBPaddressShippingCtrl').disable();
            this.customerQuotationForm.get('selectedBPAddressBillingCtrl').disable();
          }

          if (!!this.previousPageData.bp_billing_add_id) {
            setTimeout(() => {
              let tempBillAdd = [] = this.previousPageData.bp_billing_add_id;
              let tempShipAdd = [] = this.previousPageData.bp_shiping_add_id;
              this.customerQuotationForm.controls["selectedBPaddressShippingCtrl"].setValue(tempShipAdd[0]);
              this.customerQuotationForm.controls["selectedBPAddressBillingCtrl"].setValue(tempBillAdd[0]);
              this.customerQuotationForm.get('selectedBPaddressShippingCtrl').disable();
              this.customerQuotationForm.get('selectedBPAddressBillingCtrl').disable();
            }, 100);
          }

          if (!!this.previousPageData.order_date) {
            this.customerQuotationForm.controls["orderDateCtrl"].setValue(this.previousPageData.order_date);
          }

          if (!!this.previousPageData.complaint_no) {
            this.customerQuotationForm.controls["complaintNoCtrl"].setValue(this.previousPageData.complaint_no);
            this.customerQuotationForm.get('complaintNoCtrl').disable();
          }

          if (!!this.previousPageData.service_product_id) {
            this.customerQuotationForm.controls["serviceProductCtrl"].setValue(this.previousPageData.service_product_id);
            this.selectedServiceProduct = this.previousPageData.service_product_id;
            this.customerQuotationForm.get('serviceProductCtrl').disable();
          }
        }
      });
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  onCustomerChange(event) {
    try {
      this.custShippingAddressList = null;
      this.custBillingAddressList = null;
      this.customerQuotationForm.controls["selectedBPaddressShippingCtrl"].setValue('');
      this.customerQuotationForm.controls["selectedBPAddressBillingCtrl"].setValue('');
      this.selectedBusinessPartner = event.value;
      this.bindCustomerBillingAddress(event.value.id);
      event.component._searchText = "";
      this.bindServiceProductIfSelected();
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  onCustomerClose(event) {
    try {
      event.component.searchText = "";
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  onCustomerSearchChange(event) {
    try {
      this.reftextcount++;
      var custsearchtext = event.text;

      if (custsearchtext.length >= 3) {
        this.bindCustomerFromApi(custsearchtext);
        this.reftextcount = 0;
      } else {
        if (!!this.dontClearCustomerQuotation && this.dontClearCustomerQuotation == true) {} else {
          this.businessPartnerList = [];
        }
      }
    } catch (error) {// console.log(this.TAG,error);
    }
  }

  onServiceProductClose(event) {
    try {
      event.component.searchText = "";
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  onServiceProductChange(event) {
    try {
      //  console.log(this.TAG,"onServiceProductChange"); 
      this.selectedServiceProduct = event.value;

      if (event.value.compnumberismandatory == 'Y') {
        this.showComplainNumber = true;
        this.customerQuotationForm.controls['complaintNoCtrl'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required]);
        this.customerQuotationForm.controls['complaintNoCtrl'].updateValueAndValidity();
      } else if (event.value.compnumberismandatory == 'N') {
        this.showComplainNumber = false;
        this.customerQuotationForm.controls['complaintNoCtrl'].clearValidators();
        this.customerQuotationForm.controls['complaintNoCtrl'].updateValueAndValidity();
      }

      event.component._searchText = "";
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  onServiceProductSearch(event) {
    try {
      var custsearchtext = event.text;

      if (!!custsearchtext) {
        if (!!this.selectedBusinessPartner) {
          this.bindServiceProduct(custsearchtext);
        } else {
          this.commonFunction.presentAlert("Customer Quotation", "Validation", "Please Select Customer");
        }
      }
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  bindCustomerFromApi(strsearch) {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        if (strsearch != "") {
          const cust_response = yield _this2.customerQuotationService.getCustomer(strsearch).toPromise(); //  console.log(this.TAG,cust_response);

          _this2.businessPartnerList = cust_response;
        } else {
          _this2.businessPartnerList = [];
        }
      } catch (error) {//  console.log(this.TAG,error);
      }
    })();
  }

  bindCustomerBillingAddress(id) {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const addressResponse = yield _this3.customerQuotationService.getcustmerbillingaddress(_this3.selectedBusinessPartner.id).toPromise();
        const response = addressResponse['response'];
        var jsondata = response.data;
        _this3.custBillingAddressList = jsondata.filter(e => e.invoiceToAddress == true);
        _this3.custShippingAddressList = jsondata.filter(e => e.shipToAddress == true);
        setTimeout(() => {
          if (_this3.custShippingAddressList.length == 1) {
            _this3.customerQuotationForm.controls["selectedBPaddressShippingCtrl"].setValue(_this3.custShippingAddressList[0]);
          }

          if (_this3.custBillingAddressList.length == 1) {
            _this3.customerQuotationForm.controls["selectedBPAddressBillingCtrl"].setValue(_this3.custBillingAddressList[0]);
          }
        }, 100);
      } catch (error) {// console.log(this.TAG,error);
      }
    })();
  }

  bindServiceProduct(strsearch) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        if (strsearch != "") {
          const serviceProductApiResponse = yield _this4.customerQuotationService.getServiceProduct(strsearch, _this4.selectedBusinessPartner.id).toPromise(); //  console.log(this.TAG,"Service Product API Response",serviceProductApiResponse);
          //  this.serviceProductList = [{"id":"FFF20200121121311736E8DB9B68EA18","name":"SC-002-Service Charges - Others","compnumberismandatory":"N"}];

          _this4.serviceProductList = serviceProductApiResponse;
        } else {
          _this4.serviceProductList = []; //  const serviceProductApiResponse = await this.customerQuotationService.getServiceProduct(strsearch,this.selectedBusinessPartner.id).toPromise();
          //  console.log(this.TAG,"Service Product API Response",serviceProductApiResponse);
          //  this.serviceProductList = serviceProductApiResponse;
          //  console.log(this.TAG,this.serviceProductList);
        }
      } catch (error) {//  console.log(this.TAG,error);
      }
    })();
  }

  toggle(selectedcartproduct) {
    if (selectedcartproduct.show == false) {
      for (var i = 0; i < this.selectedSerialNumberProductList.length; i++) {
        if (this.selectedSerialNumberProductList[i].show === "true") {
          this.selectedSerialNumberProductList[i].show = "false";
        }
      }
    }

    selectedcartproduct.show = !selectedcartproduct.show;
  }

  presentAlert(Header, SubHeader, Message) {
    var _this5 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this5.alertCtrl.create({
        header: Header,
        subHeader: SubHeader,
        message: Message,
        buttons: [{
          text: "OK",
          handler: ok => {
            _this5.customerQuotationForm.reset('', {
              emitEvent: false
            });

            _this5.router.navigateByUrl('/home');
          }
        }]
      });
      alert.dismiss(() => console.log('The alert has been closed.'));
      yield alert.present();
    })();
  }

  addSerialNo() {
    try {
      if (!!this.selectedSerialNumberProductList) {} else {
        this.selectedSerialNumberProductList = [];
      }

      if (!!this.customerQuotationForm.controls["customerCtrl"].value) {
        if (!!this.customerQuotationForm.controls["selectedBPaddressShippingCtrl"].value) {
          if (!!this.customerQuotationForm.controls["selectedBPAddressBillingCtrl"].value) {
            // if(!!this.customerQuotationForm.controls["serviceProductCtrl"].value){
            if (true) {
              let navigationExtras = {
                queryParams: {
                  selectedSerialNumberProductList: JSON.stringify(this.selectedSerialNumberProductList),
                  quotation_data: JSON.stringify({
                    selectedBusinessPartnerKey: this.selectedBusinessPartner,
                    bp_billing_add_id: this.customerQuotationForm.controls["selectedBPAddressBillingCtrl"].value,
                    bp_shiping_add_id: this.customerQuotationForm.controls["selectedBPaddressShippingCtrl"].value,
                    order_date: this.customerQuotationForm.controls["orderDateCtrl"].value,
                    service_product_id: this.selectedServiceProduct,
                    complaint_no: this.customerQuotationForm.controls["complaintNoCtrl"].value
                  })
                }
              };
              this.router.navigate(['add-edit-service-product'], navigationExtras);
            } else {}
          } else {
            this.commonFunction.presentAlert("Customer Quotation", "Validation", "Please Select Billing Address");
          }
        } else {
          this.commonFunction.presentAlert("Customer Quotation", "Validation", "Please Select Shipping Address");
        }
      } else {
        this.commonFunction.presentAlert("Customer Quotation", "Validation", "Please Select Customer");
      }
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  removeProduct(deletedSerial) {
    try {
      const result = this.selectedSerialNumberProductList.filter(item => item.m_attributesetinstance_id != deletedSerial.m_attributesetinstance_id);
      this.selectedSerialNumberProductList = result;

      if (this.selectedSerialNumberProductList.length < 1) {
        this.hasSerialNumber = false;
      }
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  onSaveQuotation(form) {
    var _this6 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        //  console.log(this.TAG,"Quotation Data From Page",form);
        _this6.commonFunction.loadingPresent();

        let data = {
          "bpid": _this6.customerQuotationForm.controls['customerCtrl'].value.id,
          "billid": _this6.customerQuotationForm.controls['selectedBPaddressShippingCtrl'].value.id,
          "shipid": _this6.customerQuotationForm.controls['selectedBPaddressShippingCtrl'].value.id,
          "orderdate": _this6.customerQuotationForm.controls['orderDateCtrl'].value,
          "m_product_id": _this6.customerQuotationForm.controls['serviceProductCtrl'].value.id,
          "complaintno": _this6.customerQuotationForm.controls['complaintNoCtrl'].value
        };
        const saveQuotationResponse = yield _this6.customerQuotationService.saveQuotation(data, _this6.selectedSerialNumberProductList).toPromise(); //  console.log(this.TAG,saveQuotationResponse);

        _this6.commonFunction.loadingDismiss();

        if (!!saveQuotationResponse) {
          _this6.presentAlert("Quotation Approval", "", saveQuotationResponse.msg);
        }
      } catch (error) {
        //  console.log(this.TAG,error);
        _this6.commonFunction.loadingDismiss();
      }
    })();
  }

  bindServiceProductIfSelected() {
    var _this7 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this7.tempServiceProductResponse = yield _this7.customerQuotationService.getServiceProduct("", _this7.selectedBusinessPartner.id).toPromise();

        if (!!_this7.tempServiceProductResponse) {
          if (_this7.tempServiceProductResponse.length > _this7.loginauth.minlistcount) {
            _this7.serviceProductList = "";
          } else {
            _this7.serviceProductList = _this7.tempServiceProductResponse;

            if (_this7.serviceProductList.length == 1) {
              _this7.customerQuotationForm.controls["serviceProductCtrl"].setValue(_this7.serviceProductList[0]);

              _this7.selectedServiceProduct = _this7.serviceProductList[0]; // this.customerQuotationForm.get('serviceProductCtrl').disable();
            }
          }
        }
      } catch (error) {//  console.log(this.TAG,error);
      }
    })();
  }

};

CustomerQuotationPage.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder
}, {
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__.Commonfun
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.AlertController
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_10__.Router
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_10__.ActivatedRoute
}, {
  type: _customer_quotation_service__WEBPACK_IMPORTED_MODULE_3__.CustomerQuotationService
}, {
  type: src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_5__.LoginauthService
}, {
  type: src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_5__.LoginauthService
}];

CustomerQuotationPage = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
  selector: 'app-customer-quotation',
  template: _customer_quotation_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_customer_quotation_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], CustomerQuotationPage);


/***/ }),

/***/ 11280:
/*!**************************************************************************************!*\
  !*** ./src/app/quotation/customer-quotation/customer-quotation.page.scss?ngResource ***!
  \**************************************************************************************/
/***/ ((module) => {

module.exports = ".cus {\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n}\n\n.margin-custom {\n  margin-left: 0px;\n}\n\nion-popover {\n  --width: 320px;\n}\n\nion-popover.dateTimePopover {\n  --offset-y: -350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImN1c3RvbWVyLXF1b3RhdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFSSxhQUFBO0VBQ0EsbUJBQUE7RUFDRCw4QkFBQTtBQUFIOztBQUdFO0VBQ0UsZ0JBQUE7QUFBSjs7QUFHRTtFQUNFLGNBQUE7QUFBSjs7QUFFRTtFQUNFLDZCQUFBO0FBQ0oiLCJmaWxlIjoiY3VzdG9tZXItcXVvdGF0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jdXN7XG4gIFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlciA7XG4gICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAvLyB3aWR0aDogMTAwJTtcbiAgIH1cbiAgLm1hcmdpbi1jdXN0b217XG4gICAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgfVxuXG4gIGlvbi1wb3BvdmVyIHtcbiAgICAtLXdpZHRoOiAzMjBweDtcbiAgfVxuICBpb24tcG9wb3Zlci5kYXRlVGltZVBvcG92ZXIge1xuICAgIC0tb2Zmc2V0LXk6IC0zNTBweCAhaW1wb3J0YW50O1xuICAgIH0iXX0= */";

/***/ }),

/***/ 509:
/*!**************************************************************************************!*\
  !*** ./src/app/quotation/customer-quotation/customer-quotation.page.html?ngResource ***!
  \**************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Customer Quotation\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n  <form [formGroup]=\"customerQuotationForm\">\n  \n    <!-- Customer -->\n    <ion-card>\n      <ion-card-content>\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label position=\"stacked\">Customer<span style=\"color:red!important\">*</span></ion-label>\n              <ionic-selectable placeholder=\"Select Customer\" [searchDebounce]=\"1000\" disable=\"true\" formControlName=\"customerCtrl\"\n                  [items]=\"businessPartnerList\"  \n                  itemValueField=\"id\" \n                  itemTextField=\"_identifier\" \n                  [canSearch]=\"true\"\n                  [shouldStoreItemValue]=\"false\"\n                  (onChange)=\"onCustomerChange($event)\"\n                  (onClose)=\"onCustomerClose($event)\"\n                  (onSearch)=\"onCustomerSearchChange($event)\">\n                </ionic-selectable>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <!-- Shipping Information -->\n        <ion-row>\n        <ion-col>\n          <h5 ion-text class=\"text-primary\">\n            <ion-icon name=\"locate\"></ion-icon> Shipping Information:\n          </h5>\n        </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n              <ion-item >\n                <ion-label position=\"stacked\">Shipping Address<span style=\"color:red!important\">*</span></ion-label>\n                <ion-select  formControlName=\"selectedBPaddressShippingCtrl\" interface=\"popover\" multiple=\"false\" placeholder=\"Select Address\">\n                  <ion-select-option *ngFor=\"let custShippingAddress of custShippingAddressList\" [value]=\"custShippingAddress\">{{custShippingAddress.name}}</ion-select-option>\n                </ion-select>\n                </ion-item>\n                <div padding-left>\n                  <ng-container *ngFor=\"let validation of validation_messages.custAddShippingCtrlErrorMessage\">\n                    <div *ngIf=\"customerQuotationForm.get('selectedBPaddressShippingCtrl').hasError(validation.type) && customerQuotationForm.get('selectedBPaddressShippingCtrl').touched\">\n                      <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                    </div>\n                  </ng-container>\n                </div>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n              <ion-item >\n                <ion-label position=\"stacked\">Billing Address<span style=\"color:red!important\">*</span></ion-label>\n                <ion-select  formControlName=\"selectedBPAddressBillingCtrl\" interface=\"popover\" multiple=\"false\" placeholder=\"Select Address\">\n                  <ion-select-option *ngFor=\"let badd of custBillingAddressList\" [value]=\"badd\">{{badd.name}}</ion-select-option>\n                </ion-select>\n                </ion-item>\n                <div padding-left>\n                  <ng-container *ngFor=\"let validation of validation_messages.custAddBillingCtrlErrorMessage\">\n                    <div *ngIf=\"customerQuotationForm.get('selectedBPAddressBillingCtrl').hasError(validation.type) && customerQuotationForm.get('selectedBPAddressBillingCtrl').touched\">\n                      <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                    </div>\n                  </ng-container>\n                </div>\n          </ion-col>\n        </ion-row>\n      </ion-card-content>\n    </ion-card>\n    <ion-card>\n      <ion-card-content>\n        <ion-row>\n          <ion-col>\n           <ion-item>\n            <div class=\"margin-custom\">\n              <ion-label style=\"color: darkgray;\">Order Date</ion-label>\n              <section class=\"cus\">\n               <!-- <ion-datetime  disabled style=\"--padding-start:0px\" displayFormat=\"DD-MM-YYYY\" [(ngModel)]=\"today\" formControlName=\"orderDateCtrl\">\n              </ion-datetime> -->\n              <ion-item disabled>\n                <ion-input placeholder=\"Select Date\" [value]=\"datetoday\"></ion-input>\n                <ion-button fill=\"clear\" id=\"open-date-input-1\">\n                  <ion-icon icon=\"calendar\"></ion-icon>\n                </ion-button>\n                <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\n                  <ng-template>\n                    <ion-datetime\n                      #popoverDatetime2\n                      presentation=\"date\"\n                      showDefaultButtons=\"true\"\n                      max=\"2050\"\n                      [(ngModel)]=\"today\" formControlName=\"orderDateCtrl\"\n                      (ionChange)=\"datetoday = formatDate(popoverDatetime2.value)\"\n                    ></ion-datetime>\n                  </ng-template>\n                </ion-popover>\n              </ion-item>\n            </section>\n            </div>\n          </ion-item>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label position=\"stacked\">Service Product <span style=\"color:red!important\">*</span></ion-label>\n              <ionic-selectable placeholder=\"Select Product\" [searchDebounce]=\"1000\" formControlName=\"serviceProductCtrl\"\n                                [items]=\"serviceProductList\" \n                                itemValueField=\"id\" \n                                itemTextField=\"name\" \n                                [canSearch]=\"true\"\n                                \n                                [shouldStoreItemValue]=\"false\"\n                                (onClose)=\"onServiceProductClose($event)\"\n                                (onChange)=\"onServiceProductChange($event)\"\n                                (onSearch)=\"onServiceProductSearch($event)\">\n              </ionic-selectable>\n            </ion-item>\n          </ion-col>\n        </ion-row> \n        <ion-row *ngIf=\"showComplainNumber\">\n           <ion-col>\n            <ion-item>\n              <ion-label position=\"floating\">Complaint No.</ion-label>\n              <ion-input type=\"text\" formControlName=\"complaintNoCtrl\"></ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        \n      </ion-card-content>\n    </ion-card> \n    <ion-card>\n      <ion-card-content>\n        <ion-row>\n          <ion-col>\n            <h5 ion-text class=\"text-primary\">\n              <ion-icon name=\"cart\"></ion-icon>Serial No\n            </h5>\n          </ion-col>\n          <ion-col>\n            <ion-fab-button size=\"small\" float-right (click)=\"addSerialNo()\">\n              <ion-icon name=\"add\"></ion-icon>\n            </ion-fab-button>\n          </ion-col>\n        </ion-row>\n        <ion-item *ngFor=\"let item of selectedSerialNumberProductList; index as i\"  text-wrap style=\"font-size: small;\">\n        \n            <div style=\"width: 100%;\" (click)=\"toggle(item)\">\n                <ion-row>\n                  <ion-col size=\"8\">\n                    <ion-label style=\"white-space: normal\">\n                     <ion-icon *ngIf=\"!item.MainProductQty\" style=\"color: springgreen;\" name=\"pricetags\"></ion-icon>\n                      {{ item.serialno }}\n                    </ion-label>\n                  </ion-col>\n                  <ion-col size=\"2\" style=\"width: 100%; text-align: right;\">\n                    <ion-icon name=\"trash\" (click)=\"removeProduct(item)\" style=\"font-size: x-large;\n                    color: red;\"></ion-icon>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col nowrap>\n                    <div *ngIf=\"item.show\">\n                      <ion-label style=\"font-size: small;\">QTY: {{ item.qty }}</ion-label>\n                      <ion-label style=\"font-size: small;\">UOM: {{ item.uomname }}</ion-label>\n                      <ion-label style=\"font-size: small;\" *ngIf=\"Servicepricebasedon == 'PLB'\">Rate:{{item.rate}}</ion-label>\n                      <ion-label style=\"font-size: small;\" *ngIf=\"Servicepricebasedon == 'PLB'\">Net Amount: {{ item.netamt }}</ion-label>\n                      <ion-label style=\"font-size: small;\" *ngIf=\"Servicepricebasedon == 'PLB'\">Tax: {{ item.taxname}}</ion-label>\n                      <ion-label style=\"font-size: small;\" *ngIf=\"Servicepricebasedon == 'PLB'\">Total Gross Amount: {{ item.totgrossamt}}</ion-label>\n                    </div>\n                 </ion-col>\n                </ion-row>\n            </div>\n        \n        </ion-item>\n      </ion-card-content>\n    </ion-card>\n    <ion-row>\n      <ion-col style=\"padding-left: 16px;padding-right: 16px;\">\n        <ion-button size=\"default\"\n          class=\"submit-btn\" expand=\"block\" [disabled]=\"(!customerQuotationForm.valid || customerQuotationForm.disabled ) || !hasSerialNumber\" color=\"primary\" (click)=\"onSaveQuotation(customerQuotationForm)\">Get Quotation\n        </ion-button>\n      </ion-col>\n    </ion-row>\n    \n</form>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_quotation_customer-quotation_customer-quotation_module_ts.js.map