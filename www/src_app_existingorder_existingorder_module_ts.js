"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_existingorder_existingorder_module_ts"],{

/***/ 29767:
/*!*******************************************************!*\
  !*** ./src/app/existingorder/existingorder.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ExistingorderPageModule": () => (/* binding */ ExistingorderPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _existingorder_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./existingorder.page */ 27077);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ionic-selectable */ 25073);








const routes = [
    {
        path: '',
        component: _existingorder_page__WEBPACK_IMPORTED_MODULE_0__.ExistingorderPage
    }
];
let ExistingorderPageModule = class ExistingorderPageModule {
};
ExistingorderPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule, ionic_selectable__WEBPACK_IMPORTED_MODULE_6__.IonicSelectableModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterModule.forChild(routes)
        ],
        declarations: [_existingorder_page__WEBPACK_IMPORTED_MODULE_0__.ExistingorderPage]
    })
], ExistingorderPageModule);



/***/ }),

/***/ 27077:
/*!*****************************************************!*\
  !*** ./src/app/existingorder/existingorder.page.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ExistingorderPage": () => (/* binding */ ExistingorderPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _existingorder_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./existingorder.page.html?ngResource */ 86353);
/* harmony import */ var _existingorder_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./existingorder.page.scss?ngResource */ 74444);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../neworder/neworder.service */ 17216);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);









let ExistingorderPage = class ExistingorderPage {
    constructor(loginauth, router, loadingController, commonfun, neworderservice) {
        this.loginauth = loginauth;
        this.router = router;
        this.loadingController = loadingController;
        this.commonfun = commonfun;
        this.neworderservice = neworderservice;
        this.Remarks = '';
        this.activitylist = [];
        this.selectedactivity = '';
        this.Issinglecust = false;
    }
    ionViewWillEnter() {
        this.ResetPage();
        // this.commonfun.chkcache('existingorder');
        setTimeout(() => {
            this.Bindallactivity();
        }, 1500);
    }
    ngOnInit() {
        //this.ResetPage();
    }
    checkcust() {
        try {
            if (this.loginauth.defaultprofile[0].mmstOrderusrtype == "CEB") {
                this.neworderservice.getexistcustmersearchapi("", this.selectedactivity).subscribe(data => {
                    // var response = data;
                    const response = data;
                    // 
                    this.exBusinessPartnerlist = response;
                    this.exleastBusinessPartnerlist = this.exBusinessPartnerlist;
                    if (this.exleastBusinessPartnerlist.length > this.loginauth.minlistcount) {
                    }
                    else {
                        this.dontClearDraftOrder = true;
                        this.passexBusinessPartnerlist = this.exBusinessPartnerlist;
                    }
                    //console.log("Draft Order Min Customer",this.exleastBusinessPartnerlist);
                    if (this.exBusinessPartnerlist.length == 1) {
                        this.exBusinessPartnerlist = response;
                        this.passexBusinessPartnerlist = this.exBusinessPartnerlist;
                        this.exselectedBusinessPartner = this.exBusinessPartnerlist[0];
                        this.onChangeCustomer();
                        this.Issinglecust = true;
                    }
                    else {
                        // this.exBusinessPartnerlist = null;
                        // this.passexBusinessPartnerlist = null;
                        this.exselectedBusinessPartner = null;
                        this.Issinglecust = false;
                    }
                });
                //
                // if (this.edtitdocid != undefined || this.edtitdocid != '') {
                //   this.editOrder(this.edtitdocid);
                // }
            }
        }
        catch (error) {
            //  console.log("Error:checkcust",error);
        }
    }
    custsearchChange(event) {
        console.log("custsearchChange");
        if (event.text.length >= 3) {
            this.bindcustomer1api(event.text);
        }
        else {
            if (!!this.dontClearDraftOrder && this.dontClearDraftOrder == true) {
            }
            else {
                this.exBusinessPartnerlist = [];
            }
        }
    }
    bindcustomer1api(strsearch) {
        try {
            console.log("bindcustomer1api");
            if (strsearch != "") {
                //strsearch="a";
                this.neworderservice.getexistcustmersearchapi(strsearch, this.selectedactivity).subscribe(data => {
                    var response = data;
                    // 
                    this.exBusinessPartnerlist = response;
                    this.passexBusinessPartnerlist = this.exBusinessPartnerlist;
                });
            }
            else {
                // this.exBusinessPartnerlist=null;
                //=============start for top 10================= 
                if (this.exleastBusinessPartnerlist) {
                    if (this.exleastBusinessPartnerlist.length > this.loginauth.minlistcount) {
                        this.exBusinessPartnerlist = null;
                    }
                    else {
                        this.exBusinessPartnerlist = this.exleastBusinessPartnerlist;
                        this.passexBusinessPartnerlist = this.exBusinessPartnerlist;
                        this.dontClearDraftOrder = true;
                    }
                }
                else {
                    this.neworderservice.getexistcustmersearchapi("", this.selectedactivity).subscribe(data => {
                        // var response = data;
                        const response = data;
                        this.exleastBusinessPartnerlist = response;
                        if (this.exleastBusinessPartnerlist) {
                            if (this.exleastBusinessPartnerlist.length > this.loginauth.minlistcount) {
                                this.exBusinessPartnerlist = null;
                            }
                            else {
                                this.exBusinessPartnerlist = this.exleastBusinessPartnerlist;
                                this.passexBusinessPartnerlist = this.exBusinessPartnerlist;
                                this.dontClearDraftOrder = true;
                            }
                        }
                    });
                }
                //=============end for top 10================= 
            }
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    onChangeCustomer() {
        try {
            this.Remarks = '';
            if (this.exselectedBusinessPartner != null) {
                this.Binddocument();
            }
        }
        catch (error) {
            // console.log("onChangeCustomer:Error",error);
        }
    }
    Binddocument() {
        try {
            this.neworderservice.getdocumentidbycust(this.exselectedBusinessPartner.id).subscribe(data => {
                const response = data['response'];
                //  console.log("Doc Number", response);
                this.alldocument = response.data.map(function (order) {
                    order.documentno = order.documentno + ' : ' + order.documentdate;
                    return order;
                });
                //documentdate,documentno
            }, error => {
                //  console.log("Binddocument:error",error);
            });
        }
        catch (error) {
            //  console.log("Binddocument:error",error);
        }
    }
    Bindallactivity() {
        try {
            this.selectedcustomer = null;
            this.selecteddocumentno = null;
            this.activitylist[0] = this.loginauth.selectedactivity;
            this.selectedactivity = this.activitylist[0].id;
            this.exonActChange();
            //  this.loginauth.getuserwiseactivity(this.loginauth.userid).subscribe(data => {
            //    this.activitylist = data;
            //    if(this.activitylist.length==1){
            //      this.selectedactivity=this.activitylist[0].id;
            //      this.exonActChange();
            //    }
            //   this.commonfun.loadingDismiss();
            //  },error=>{
            //    this.commonfun.loadingDismiss();
            //   }); 
            //  this.commonfun.loadingDismiss();
        }
        catch (error) {
            // console.log("error",error);
        }
    }
    exonActChange() {
        this.exBusinessPartnerlist = null;
        this.exselectedBusinessPartner = null;
        this.alldocument = null;
        this.selecteddocumentno = null;
        this.checkcust();
    }
    onEdit() {
        //  this.router.navigateByUrl('/neworder?edtitdocid='+this.selecteddocumentno.id);
        let navigationExtras = {
            state: {
                BusinessPartnerlist: this.passexBusinessPartnerlist,
                selectedBusinessPartner: this.exselectedBusinessPartner,
                selecteddocumentno: this.selecteddocumentno
            }
        };
        this.router.navigate(['neworder'], navigationExtras);
        this.ResetPage();
    }
    docChange(event) {
        event.component._searchText = "";
    }
    refChange(event) {
        this.onChangeCustomer();
        // 
        event.component._searchText = "";
    }
    onCustomerClose(event) {
        event.component._searchText = "";
    }
    ResetPage() {
        // this.selectedactivity='';
        this.selectedcustomer = null;
        // this.exBusinessPartnerlist=null;
        // this.exselectedBusinessPartner=null;
        this.selecteddocumentno = null;
        //
        if (this.activitylist.length != 1) {
            this.selectedactivity = '';
            this.exBusinessPartnerlist = null;
        }
        if (this.Issinglecust != true) {
            this.exselectedBusinessPartner = null;
            this.alldocument = null;
        }
    }
    RefreshPage() {
        this.ResetPage();
    }
    onCancel() {
        this.ResetPage();
    }
};
ExistingorderPage.ctorParameters = () => [
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.LoadingController },
    { type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_4__.Commonfun },
    { type: _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_3__.NeworderService }
];
ExistingorderPage = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-existingorder',
        template: _existingorder_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_existingorder_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ExistingorderPage);



/***/ }),

/***/ 74444:
/*!******************************************************************!*\
  !*** ./src/app/existingorder/existingorder.page.scss?ngResource ***!
  \******************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJleGlzdGluZ29yZGVyLnBhZ2Uuc2NzcyJ9 */";

/***/ }),

/***/ 86353:
/*!******************************************************************!*\
  !*** ./src/app/existingorder/existingorder.page.html?ngResource ***!
  \******************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Draft Order\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <ion-buttons (click)=\"RefreshPage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-grid fixed>\n    <div>\n      <ion-card>\n        <ion-card-content>\n          <ion-row>\n            <ion-col>\n              <ion-item>\n                <ion-label position=\"stacked\">Organization Activity<span style=\"color:red!important\">*</span>\n                </ion-label>\n                <ion-select name=\"selectedactivity\" #C [(ngModel)]=\"selectedactivity\" (ionChange)=\"exonActChange()\"\n                  interface=\"popover\" multiple=\"false\" placeholder=\"Select Activity\">\n                  <ion-select-option *ngFor=\"let activity of activitylist\" [value]=\"activity.id\">{{activity.name}}\n                  </ion-select-option>\n                </ion-select>\n              </ion-item>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col>\n              <ion-item>\n                <ion-label position=\"stacked\">Customer<span style=\"color:red!important\">*</span></ion-label>\n                <ionic-selectable placeholder=\"Select Customer\" [searchDebounce]=\"1000\"\n                  [(ngModel)]=\"exselectedBusinessPartner\" [items]=\"exBusinessPartnerlist\" itemValueField=\"id\"\n                  itemTextField=\"name\" [canSearch]=\"true\" (onChange)=\"refChange($event)\"\n                  (onSearch)=\"custsearchChange($event)\" (onClose)=\"onCustomerClose($event)\">\n                </ionic-selectable>\n              </ion-item>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col>\n              <ion-item>\n                <ion-label position=\"stacked\">Document No.<span style=\"color:red!important\">*</span></ion-label>\n                <ionic-selectable placeholder=\"Select Document No.\" [(ngModel)]=\"selecteddocumentno\"\n                  [items]=\"alldocument\" itemValueField=\"id\" itemTextField=\"documentno\" [canSearch]=\"true\"\n                  (onChange)=\"docChange($event)\">\n                </ionic-selectable>\n              </ion-item>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col>\n              <ion-button (click)=\"onEdit()\"\n                [disabled]=\"!exselectedBusinessPartner || !selectedactivity || !selecteddocumentno\">\n                Edit\n              </ion-button>\n            </ion-col>\n            <ion-col>\n              <ion-button (click)=\"onCancel()\">\n                Cancel\n              </ion-button>\n            </ion-col>\n          </ion-row>\n        </ion-card-content>\n      </ion-card>\n    </div>\n  </ion-grid>\n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_existingorder_existingorder_module_ts.js.map