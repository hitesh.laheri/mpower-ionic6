"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_newcustomer_newcustomer_module_ts"],{

/***/ 74653:
/*!***************************************************!*\
  !*** ./src/app/newcustomer/newcustomer.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NewcustomerPageModule": () => (/* binding */ NewcustomerPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../components/components.module */ 45642);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _newcustomer_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./newcustomer.page */ 39536);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ionic-selectable */ 25073);









const routes = [
    {
        path: '',
        component: _newcustomer_page__WEBPACK_IMPORTED_MODULE_1__.NewcustomerPage
    }
];
let NewcustomerPageModule = class NewcustomerPageModule {
};
NewcustomerPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, ionic_selectable__WEBPACK_IMPORTED_MODULE_6__.IonicSelectableModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterModule.forChild(routes),
            _components_components_module__WEBPACK_IMPORTED_MODULE_0__.ComponentsModule
        ],
        declarations: [_newcustomer_page__WEBPACK_IMPORTED_MODULE_1__.NewcustomerPage]
    })
], NewcustomerPageModule);



/***/ }),

/***/ 39536:
/*!*************************************************!*\
  !*** ./src/app/newcustomer/newcustomer.page.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NewcustomerPage": () => (/* binding */ NewcustomerPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _newcustomer_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./newcustomer.page.html?ngResource */ 79836);
/* harmony import */ var _newcustomer_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./newcustomer.page.scss?ngResource */ 55700);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _awesome_cordova_plugins_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @awesome-cordova-plugins/image-picker/ngx */ 92153);
/* harmony import */ var _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @awesome-cordova-plugins/camera/ngx */ 64587);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _newcustomer_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./newcustomer.service */ 85189);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _provider_validator_helper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../provider/validator-helper */ 35096);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/storage */ 80190);
/* harmony import */ var _login_login_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../login/login.page */ 66825);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../provider/message-helper */ 98792);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! date-fns */ 86527);



















let NewcustomerPage = class NewcustomerPage {
  //end
  constructor(loginauth, newcustomerservice, genericHTTP, imagePicker, camera, router, route, fb, val, loadingController, alertCtrl, cdRef, platform, common, msg, storage, loginpage, menuCtrl) {
    // cehck withouth login or not
    //console.log(this.loginauth.isloginactive);
    this.loginauth = loginauth;
    this.newcustomerservice = newcustomerservice;
    this.genericHTTP = genericHTTP;
    this.imagePicker = imagePicker;
    this.camera = camera;
    this.router = router;
    this.route = route;
    this.fb = fb;
    this.val = val;
    this.loadingController = loadingController;
    this.alertCtrl = alertCtrl;
    this.cdRef = cdRef;
    this.platform = platform;
    this.common = common;
    this.msg = msg;
    this.storage = storage;
    this.loginpage = loginpage;
    this.menuCtrl = menuCtrl;
    this.isLoading = false; //  activitylist: AllAct[];

    this.activitylist = [];
    this.test = {
      "organization": ""
    };
    this.selectedbprt = '';
    this.img = '';
    this.datefrom = '';
    this.selectedactivity = '';
    this.selectedbpcat = '';
    this.selectedbpl = '';
    this.selectedcn = '';
    this.isBPLEnabled = true;
    this.validationSummary = '';
    this.firstname = '';
    this.middlename = '';
    this.lastname = '';
    this.firmname = '';
    this.gstno = '';
    this.isClickOnAddress = false;
    this.add1 = '';
    this.add2 = '';
    this.add3 = '';
    this.district = '';
    this.mobileno = '';
    this.email = '';
    this.preferredTransportEmailIDCtrl = '';
    this.isbill = false;
    this.isShip = false;
    this.fileUrl = '';
    this.edtitcustid = '';
    this.reftextcount = 0;
    this.IsExisting = false;
    this.firstnamereq = '';
    this.lastnamereq = '';
    this.firmnamereq = '';
    this.mmstRegioncode = "";
    this.isdesktop = false;
    this.comdatasingle = [];
    this.today = new Date().toJSON().split('T')[0];
    this.isNewRegistration = false;
    this.singlephoto = [];
    this.showPreferredTransportControl = false;
    this.showPreferredTransportNameControl = false;
    this.validation_messages = {
      'selectedactivity': [{
        type: 'required',
        message: ' *Please Select Activity.'
      }],
      'selectedcn': [{
        type: 'required',
        message: '*Please Select Customer Nature'
      }],
      'firstname': [{
        type: 'required',
        message: '*Please Enter First Name'
      }],
      'lastname': [{
        type: 'required',
        message: '*Please Enter Last Name'
      }],
      'firmname': [{
        type: 'required',
        message: '*Please Enter Firm Name'
      }],
      'add1': [{
        type: 'required',
        message: '*Please Enter Address Line 1'
      }],
      'selectedsalesarea': [{
        type: 'required',
        message: ' *Please Select Sales Area.'
      }],
      'pincode': [{
        type: 'required',
        message: '*Please Enter Pin Code'
      }],
      'selectedarea': [{
        type: 'required',
        message: '*Please Select Area.'
      }],
      'mobileno': [{
        type: 'required',
        message: '*Please Enter Mobile No.'
      }, {
        type: 'InvalidNumber',
        message: '*Please Enter Valid Mobile No.'
      }],
      'panno': [{
        type: 'InvalidPanno',
        message: '*Please Enter Valid PAN No.'
      }],
      'email': [{
        type: 'InvalidEmail',
        message: '*Please Enter Valid Email.'
      }],
      'gstno': [{
        type: 'InvalidgstNumber',
        message: '*Please Enter Valid GST No.'
      }],
      'selectedbpcat': [{
        type: 'required',
        message: '*Please Select Business Partner Category'
      }],
      'preferredTransportNameMss': [{
        type: 'required',
        message: '*Please Enter Preferred Transport Name'
      }],
      'preferredTransportContactNumberCtrlMss': [{
        type: 'required',
        message: '*Please Enter Preferred Transport Contact Number'
      }, {
        type: 'InvalidNumber',
        message: '*Please Enter Valid Mobile No.'
      }],
      'preferredTransportEmailIDCtrl': [{
        type: 'InvalidEmail',
        message: '*Please Enter Valid Email.'
      }]
    };

    if (!this.loginauth.isloginactive) {
      this.storage.remove('username');
      this.storage.remove('password');
    }

    this.form = this.fb.group({
      selectedactivity: [, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required],
      selectedcn: [, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required],
      firstname: [],
      middlename: [],
      lastname: [],
      firmname: [],
      panno: [],
      selectedbpl: [],
      selectedbprt: [],
      selectedbpcat: [, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required],
      add1: [, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required],
      add2: [],
      add3: [],
      selectedsalesarea: [, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required],
      pincode: [, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required],
      selectedarea: [, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required],
      mobileno: [, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required, val.numberValid],
      //   email: [,this.val.emailValid],
      email: [],
      onecustomer: [],
      isbill: [],
      isShip: [],
      gstno: [],
      num: [],
      preferredCustomerTransport: [],
      preferredTransportName: [],
      preferredTransportContactNumberCtrl: [],
      preferredTransportEmailIDCtrl: []
    });
    this.getrout();
  }

  refChange(event) {}

  portChange(event) {} //async search


  filterPorts(Allcustomer1, text) {
    return Allcustomer1.filter(port => {
      return this.onecustomer.sfname.toLowerCase().indexOf(text) !== -1 || this.onecustomer.slname.toLowerCase().indexOf(text) !== -1 || this.onecustomer.sfirmName.toLowerCase().indexOf(text) !== -1;
    });
  }

  formatDate(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_14__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_15__["default"])(value), 'dd-MM-yyyy');
  }

  searchref(event) {
    this.reftextcount++;

    if (this.reftextcount == 3) {
      let text = event.text.trim().toLowerCase();
      event.component.startSearch(); // Close any running subscription.

      if (this.portsSubscription) {
        this.portsSubscription.unsubscribe();
      }

      if (!text) {
        // Close any running subscription.
        if (this.portsSubscription) {
          this.portsSubscription.unsubscribe();
        }

        event.component.items = [];
        event.component.endSearch();
        return;
      } // this.portsSubscription = this.newcustomerservice.getreferalcustmer(this.selectedactivity).subscribe(data => {
      //   // Subscription will be closed when unsubscribed manually.
      //   const response = data['response'];
      //   var organization = response.data.map(function (customer) {
      //     customer.sfname = customer.scusNature === 'F' ? customer.sfirmName : customer.sfname + ' ' + (customer.slname === "null" ? "" : customer.slname)
      //     customer.sfname = (customer.sfname == null ? "" : customer.sfname)
      //     return customer
      //   });
      //   this.Allcustomer = organization;
      //   if (this.portsSubscription.closed) {
      //     return;
      //   }
      //   event.component.items = this.filterPorts(this.Allcustomer, text);
      //   event.component.endSearch();
      // });


      this.reftextcount;
    }
  }

  getrout() {
    try {
      //console.log("getrout()1");
      this.storage.get('isNewRegistration').then(strisNewRegistration => {
        if (strisNewRegistration) {
          this.isNewRegistration = strisNewRegistration;

          if (this.isNewRegistration == true) {
            // this.loginauth.ReadOnlyparameter = 'user=ionic.appuser&password=ionic.appuser';
            // this.storage.set('username', "ionic.appuser");
            // this.storage.set('password', "ionic.appuser");
            // this.storage.set('ipport', this.loginauth.commonurl);
            // this.loginauth.parameter = 'user=ionic.appuser&password=ionic.appuser';
            // this.loginauth.user = 'ionic.appuser';
            // this.loginauth.pass = 'ionic.appuser';
            // this.genericHTTP.ReadOnlyUsername = 'ionic.appuser';
            // this.genericHTTP.ReadOnlypassword = 'ionic.appuser';
            this.BindallactivitynewReg();
          }
        }
      });
    } catch (error) {// console.log("getrout()-ERROR:",error);	
    }
  }

  ngOnInit() {
    this.form.reset();
    this.fileUrl = "";
    this.IsExisting = false;
    this.compliancedata = null;
    this.img = '';

    try {
      setTimeout(() => {
        this.route.queryParams.subscribe(params => {
          this.edtitcustid = params["selectedcustomer"];
        });
        this.checkplatform();

        if (this.isNewRegistration != true) {
          this.Bindallactivity();
        } else {
          this.menuCtrl.enable(false);
        }
      }, 1500);
    } catch (error) {} //Referal Code
    // this.BindAllrefcustomer();

  }

  ionViewWillEnter() {
    if (this.isNewRegistration == true) {
      //  console.log("ionViewWillEnter()1");	
      this.menuCtrl.enable(false);
    }
  } // chkcache() {
  //   try {
  //     if (this.loginauth.parameter == undefined) {
  //       var varusername = "";
  //       var varpassword = "";
  //       this.storage.get('username').then((username) => {
  //         if (username) {
  //           varusername = username;
  //         }
  //       });
  //       this.storage.get('password').then((password) => {
  //         if (password) {
  //           varpassword = password;
  //         }
  //       });
  //       this.storage.get('ipport').then((ipport) => {
  //         if (ipport) {
  //           this.loginpage.isipportvisible = false;
  //           this.loginpage.ipport = ipport;
  //           this.loginauth.commonurl = ipport + '/openbravo/'; //http(s) is added by nilesh
  //         } else {
  //           this.loginpage.isipportvisible = true;
  //         }
  //       });
  //       setTimeout(() => {
  //         this.common.LoginonClick(varusername, varpassword, 'newcustomer')
  //       }, 500);
  //     }
  //   }
  //   catch {
  //   }
  // }


  checkplatform() {
    try {
      if (!this.msg.isplatformweb) {
        this.isdesktop = false;
      } else {
        this.isdesktop = true;
      }
    } catch (error) {}
  }

  uploadImage(str) {
    try {
      var file = str.target.files[0];
      var myreader = new FileReader();

      myreader.onloadend = e => {
        var b64 = myreader.result.toString().replace(/^data:.+;base64,/, '');
        this.fileUrl = myreader.result;
        this.img = b64;
      };

      myreader.readAsDataURL(file);
    } catch (error) {}
  }

  uploadcompImage(str, id) {
    try {
      for (var k = 0; k < str.target.files.length; k++) {
        this.inneruploadcompImage(str, k, id);
        this.timeout(500);
      }
    } catch (error) {//  console.log("Catch",error);	
    }
  }

  timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  inneruploadcompImage(str, k, id) {
    var file = str.target.files[k];
    let fileType = file.type;
    console.log(fileType);
    var myreader = new FileReader();

    myreader.onloadend = e => {
      var b64 = myreader.result.toString().replace(/^data:.+;base64,/, ''); //  console.log("datacomp "+k,b64);	

      for (var i = 0; i < this.compliancedata.length; i++) {
        if (this.compliancedata[i].id == id) {
          //  console.log("datacomp ",this.compliancedata[i]);	
          // this.compliancedata[i].imgcomp1[k] = b64;	
          var existinglength = this.compliancedata[i].imgcomp.length;
          this.compliancedata[i].imgcomp[existinglength] = b64; // console.log("datacomp ",this.compliancedata[i]);	
          //--------nilesh-----------	

          try {
            if (this.compliancedata[i].Compliance_Type == "DL1VD" && this.compliancedata[i].num != "" && this.compliancedata[i].num != null) {
              var a1 = this.compliancedata[i].num.split("-");
              var dt = new Date(a1[2] + '-' + a1[1] + '-' + a1[0] + 'T00:00Z');
              this.compliancedata[i].num = dt.toISOString();
            }

            if (this.compliancedata[i].Compliance_Type == "DL2VD" && this.compliancedata[i].num != "" && this.compliancedata[i].num != null) {
              {
                var a1 = this.compliancedata[i].num.split("-");
                var dt = new Date(a1[2] + '-' + a1[1] + '-' + a1[0] + 'T00:00Z');
                this.compliancedata[i].num = dt.toISOString();
              }
            }
          } catch (error) {
            this.compliancedata[i].num = ""; // console.log("Catch");	
          } //-------end nilesh-----------	

        }
      }
    };

    myreader.readAsDataURL(file);
  }

  Bindallactivity() {
    try {
      this.activitylist[0] = this.loginauth.selectedactivity; //console.log("Selected Activity",this.loginauth.selectedactivity);
      // this.form.controls["selectedactivity"].setValue(this.loginauth.selectedactivity);

      setTimeout(() => {
        this.form.controls["selectedactivity"].setValue(this.activitylist[0].id);
        this.onActChange();
      }, 500);

      if (this.edtitcustid != undefined || this.edtitcustid == '') {
        this.editCustomer(this.edtitcustid);
      } //   this.loginauth.getuserwiseactivity(this.loginauth.userid).subscribe(data => {
      //      this.activitylist = data;
      // console.log("selectedactivity",this.activitylist)
      // if(this.activitylist.length==1){
      //   setTimeout( () => {
      //   this.form.controls["selectedactivity"].setValue(this.activitylist[0].id);
      //   this.onActChange();
      //   },500);
      // }
      //     if (this.edtitcustid != undefined || this.edtitcustid == '') {
      //       this.editCustomer(this.edtitcustid);
      //     }
      //   });

    } catch (error) {}
  }

  onClose(event) {
    event.component.searchText = "";
  }

  salseareasearchChange(event) {
    var searchtext = event.text; //.replace(/\s/g,'');

    if (searchtext.length % 3 == 0) {
      this.Bindallsalesarea(searchtext);
    }
  } //this.form.controls["selectedsalesarea"].setValue(this.customerheader[0].salesareaid);


  Bindallsalesarea(searchtext, salesareaid) {
    console.log("bind sales area called");

    try {
      this.newcustomerservice.getsalesarea(this.loginauth.userid, searchtext).subscribe(data => {
        this.leastsalesarealist = data;

        if (this.leastsalesarealist.length > this.loginauth.minlistcount && searchtext.trim() == '') {
          this.salesarealist = null;
        } else {
          this.salesarealist = this.leastsalesarealist;
        }

        if (!!this.salesarealist && this.salesarealist.length == 1) {
          setTimeout(() => {
            this.form.controls["selectedsalesarea"].setValue(this.salesarealist[0]);
            console.log('init3');
          }, 500);
        }

        if (salesareaid) {
          this.salesarealist = this.leastsalesarealist.filter(item => item.id == salesareaid);
          setTimeout(() => {
            this.form.controls["selectedsalesarea"].setValue(this.salesarealist[0]);
            console.log('init2');
          }, 500);
        }
      });
    } catch (error) {
      console.log("bind sales area called error");
    }
  }

  onChangecn() {
    this.selectedcn = this.form.controls["selectedcn"].value;

    if (this.selectedcn == 'I') {
      this.IsInd = true;
      this.Isfirm = false;
      this.firstnamereq = "*";
      this.lastnamereq = "*";
      this.firmnamereq = "";
      this.form.controls["firstname"].setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required);
      this.form.controls["lastname"].setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required);
      this.form.controls["firmname"].clearValidators();
    } else if (this.selectedcn == 'F') {
      this.IsInd = false;
      this.Isfirm = true;
      this.firstnamereq = "";
      this.lastnamereq = "";
      this.firmnamereq = "*";
      this.form.controls["firstname"].clearValidators();
      this.form.controls["lastname"].clearValidators();
      this.form.controls["firmname"].setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required);
    } else if (this.selectedcn == 'H') {
      this.IsInd = false;
      this.Isfirm = false;
      this.form.controls["firstname"].setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required);
      this.form.controls["lastname"].setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required);
      this.form.controls["firmname"].setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required);
    }
  } //Alert message


  presentAlert(Header, SubHeader, Message) {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this.alertCtrl.create({
        header: Header,
        subHeader: SubHeader,
        message: Message,
        buttons: ['OK']
      });
      yield alert.present();
    })();
  } //Capture Image from Camera


  takePicture(Type, id) {
    const options = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      targetWidth: 1500,
      targetHeight: 1500
    };
    this.camera.getPicture(options).then(imageData => {
      if (Type == "PANpic") {
        this.fileUrl = 'data:image/jpeg;base64,' + imageData;
        this.img = imageData;
      }

      if (Type == "complince") {
        for (var i = 0; i < this.compliancedata.length; i++) {
          if (this.compliancedata[i].id == id) {
            this.compliancedata[i].imgcomp.push(imageData);
          } //--------nilesh-----------


          try {
            if (this.compliancedata[i].Compliance_Type == "DL1VD" && this.compliancedata[i].num != "" && this.compliancedata[i].num != null) {
              this.compliancedata[i].num = new Date(this.dateyyyymmddT0000Z(this.compliancedata[i].num)).toISOString();
            }

            if (this.compliancedata[i].Compliance_Type == "DL2VD" && this.compliancedata[i].num != "" && this.compliancedata[i].num != null) {
              {
                this.compliancedata[i].num = new Date(this.dateyyyymmddT0000Z(this.compliancedata[i].num)).toISOString();
              }
            }
          } catch (error) {
            this.compliancedata[i].num = "";
          } //-------end nilesh-----------

        }
      }
    }, err => {});
  } //Select Image from library	


  getimage(Type, id) {
    // const options: CameraOptions = {
    //   quality: 50,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE,
    //   sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    //   targetWidth:1500,
    //   targetHeight:1500
    // };
    const options = {
      quality: 50,
      outputType: 1,
      width: 1500,
      height: 1500
    }; //----------------------	

    this.imagePicker.getPictures(options).then(imageData => {
      if (Type == "PANpic") {
        this.fileUrl = 'data:image/jpeg;base64,' + imageData[0];
        this.img = imageData[0];
      }

      if (Type == "complince") {
        for (var i = 0; i < this.compliancedata.length; i++) {
          if (this.compliancedata[i].id == id) {
            // 	
            //  console.log(" this.compliancedata[i].imgcomp", imageData);
            this.compliancedata[i].imgcomp = this.compliancedata[i].imgcomp.concat(imageData); // console.log(" this.compliancedata[i].imgcomp", this.compliancedata[i].imgcomp);
            //  this.compliancedata[i].imgcomp1 = imageData;	
          } //--------nilesh-----------	


          try {
            if (this.compliancedata[i].Compliance_Type == "DL1VD" && this.compliancedata[i].num != "" && this.compliancedata[i].num != null) {
              var valid_date1 = new Date(this.dateyyyymmddT0000Z(this.compliancedata[i].num)).toISOString();
              if (!valid_date1.includes('NaN')) this.compliancedata[i].num = valid_date1; //   this.compliancedata[i].num=new Date(this.dateyyyymmddT0000Z(this.compliancedata[i].num)).toISOString();
            }

            if (this.compliancedata[i].Compliance_Type == "DL2VD" && this.compliancedata[i].num != "" && this.compliancedata[i].num != null) {
              {
                var valid_date2 = new Date(this.dateyyyymmddT0000Z(this.compliancedata[i].num)).toISOString();
                if (!valid_date2.includes('NaN')) this.compliancedata[i].num = valid_date2;
              }
            }
          } catch (error) {
            //  console.log("error",error)
            this.compliancedata[i].num = "";
          } //-------end nilesh-----------

        }
      }
    }, err => {});
  }

  dateyyyymmddT0000Z(dt) {
    try {
      var dl1date = new Date(dt);
      var nmonth = dl1date.getMonth() + 1;
      var dd1 = dl1date.getDate() < 10 ? "0" + dl1date.getDate() : dl1date.getDate();
      var mm1 = nmonth < 10 ? "0" + nmonth : nmonth;
      var yyyy1 = dl1date.getFullYear(); // this.strfromdate=dd1+"-"+mm1+"-"+yyyy1

      return yyyy1 + "-" + mm1 + "-" + dd1 + "T00:00Z";
    } catch (error) {
      return null;
    }
  }

  BindAllrefcustomer() {
    try {
      this.onecustomer = null; // this.newcustomerservice.getreferalcustmer(this.selectedactivity).subscribe(data => {
      //   const response = data['response'];
      //   var organization = response.data.map(function (customer) {
      //     customer.sfname = customer.scusNature === 'F' ? customer.sfirmName : customer.sfname + ' ' + (customer.slname === "null" ? "" : customer.slname)
      //     customer.sfname = (customer.sfname == null ? "" : customer.sfname)
      //     return customer
      //   });
      //   if (this.edtitcustid != 'undefined' && this.edtitcustid != '') {
      //     var that = this;
      //     var refcust = organization.filter(function (emp) {
      //       if (emp.id == that.edtitcustid) {
      //         return false;
      //       }
      //       return true;
      //     });
      //   }
      //   this.Allcustomer = refcust;
      // });
    } catch (error) {}
  }

  onBpChange() {
    this.newcustomerservice.getCompilanceDataapi(this.form.controls['selectedbpcat'].value, "").subscribe(data11 => {
      this.compliancedata = data11;

      for (var c = 0; c < this.compliancedata.length; c++) {
        var varscompType = this.compliancedata[c].scompType;
        this.compliancedata[c].scompType = this.compliancedata[c].Compliance_Type;
        this.compliancedata[c].Compliance_Type = varscompType;
        this.compliancedata[c].imgcomp1 = [];
        this.form.addControl(this.compliancedata[c].Compliance_Type, new _angular_forms__WEBPACK_IMPORTED_MODULE_13__.FormControl());
      }
    });
  }

  onBPLChange() {
    this.selectedbpl = this.form.controls['selectedbpl'].value;
    if (this.selectedbpl == "S") this.isBPLEnabled = false;else this.isBPLEnabled = true;
  }

  onActChange() {
    this.selectedactivity = this.form.controls['selectedactivity'].value;
    console.log("selected activity ", this.selectedactivity); //-----------	

    if (this.isNewRegistration == true) {
      const a = this.activitylist.filter(item => item.id == this.selectedactivity);
      this.loginauth.selectedactivity = a[0];
      this.loginauth.userid = null; //this.loginauth.selectedactivity.user_id;	
    }

    console.log("ADDRESS VALIDATION FLAG", this.loginauth.selectedactivity.add_validation_lead);

    if (!!this.loginauth.selectedactivity && !!this.loginauth.selectedactivity.add_validation_lead && this.loginauth.selectedactivity.add_validation_lead == 'Y') {
      let add1 = null;
      add1 = this.form.get('add2');
      add1.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required);
      add1.updateValueAndValidity();
      let add2 = null;
      add2 = this.form.get('add3');
      add2.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required);
      add2.updateValueAndValidity();
    } else {
      let add1 = null;
      add1 = this.form.get('add2');
      add1.clearValidators();
      add1.updateValueAndValidity();
      let add2 = null;
      add2 = this.form.get('add3');
      add2.clearValidators();
      add2.updateValueAndValidity();
    } //---------------


    this.getbussnessPartnerCategory();
    this.getTaggedBusinessPartner();
    this.BindAllrefcustomer();
    this.Bindallsalesarea('');
  }

  getTaggedBusinessPartner() {
    try {
      this.newcustomerservice.getBPartner(this.selectedactivity).subscribe(data => {
        const response = data['response'];
        this.bplist = response.data;
      });
    } catch (error) {}
  }

  getbussnessPartnerCategory() {
    try {
      this.newcustomerservice.getBPCategory(this.selectedactivity).subscribe(data => {
        const response = data['response'];
        this.bpcatlist = response.data;
      });
    } catch (error) {}
  }

  onSaveDraft(frm) {
    // console.log('selectedsalesarea',this.form.controls["selectedsalesarea"].value)
    // return;
    var ismobilexist;

    try {
      if (this.onChangegst() == false) {
        return;
      }

      if (this.addressValidation()) {
        if (this.form.controls.mobileno.valid) {
          this.newcustomerservice.checkmobileno(this.form.controls["mobileno"].value).subscribe(data => {
            const response = data['response'];

            if (response.data.length > 0) {
              ismobilexist = false;
              this.presentAlert("Message", "Alert!", "Mobile no. is already exists.");
              this.form.controls["mobileno"].setValue("");
            } else {
              ismobilexist = true;
              this.onSaveDraft1(frm);
            }
          });
        }
      } else {
        this.presentAlert("Message", "Error!", "Address line cannot be same");
      }
    } catch (error) {
      this.presentAlert("Message", "Error!", error);
    }
  }

  addressValidation() {
    if (!!this.loginauth.selectedactivity && !!this.loginauth.selectedactivity.add_validation_lead && this.loginauth.selectedactivity.add_validation_lead == 'Y') {
      if (this.form.controls["add2"].value.replace(/\s/g, "") == "") {
        return false;
      }

      if (this.form.controls["add3"].value.replace(/\s/g, "") == "") {
        return false;
      }

      if (!!this.form.controls["add1"].value && !!this.form.controls["add2"].value && !!this.form.controls["add3"].value) {
        if (this.form.controls["add1"].value.trim().toLowerCase() != this.form.controls["add2"].value.trim().toLowerCase() && this.form.controls["add2"].value.trim().toLowerCase() != this.form.controls["add3"].value.trim().toLowerCase() && this.form.controls["add1"].value.trim().toLowerCase() != this.form.controls["add3"].value.trim().toLowerCase()) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    } else {
      let address1 = this.form.controls["add1"].value.trim().toLowerCase();
      let address2;
      let address3;

      if (!!this.form.controls["add2"].value) {
        address2 = this.form.controls["add2"].value.trim().toLowerCase();
      }

      if (!!this.form.controls["add3"].value) {
        address3 = this.form.controls["add3"].value.trim().toLowerCase();
      }

      if (address1 == address2) {
        console.log("Condtion 1");
        return false;
      } else if (!!address2 && address2 == address3) {
        console.log("Condtion 2");
        return false;
      } else if (address1 == address3) {
        console.log("Condtion 3");
        return false;
      } else {
        console.log("Condtion 4");
        return true;
      }
    }
  }

  onChangemobileno() {// this.checkmobileno();
  } //for new registration	


  BindallactivitynewReg() {
    try {
      // this.commonfun.loadingPresent();	
      // console.log("BindallactivitynewReg");	
      this.newcustomerservice.getUserActivityAgreementStatus().subscribe(data => {
        this.activitylist = data;

        if (this.activitylist.length == 1) {
          setTimeout(() => {
            this.form.controls["selectedactivity"].setValue(this.activitylist[0].id);
            this.onActChange();
          }, 500);
        }
      }, error => {//  console.log("BindallactivitynewReg:error",error);	
      });
    } catch (error) {// console.log("BindallactivitynewReg:error",error);	
    }
  }

  onChangepin(id = '', customerId) {
    //var that = this;
    this.newcustomerservice.getPincode(this.form.controls["pincode"].value).subscribe(data => {
      const response = data['response'];
      this.pincodelist = response.data;

      if (this.pincodelist.length > 0) {
        this.invalidpincode = '';
        this.newcustomerservice.getarea(this.pincodelist[0].id).subscribe(data => {
          const response = data['response'];
          this.arealist = response.data;
          this.state = this.pincodelist[0].region$_identifier;
          this.country = this.pincodelist[0].country$_identifier;
          this.district = this.pincodelist[0].district$_identifier;

          if (id != '' || id == undefined) {
            this.selectedarea = this.arealist.find(item => item.id === id);
            setTimeout(() => {
              this.form.controls["selectedarea"].setValue(this.selectedarea);
              this.form.controls["selectedbpcat"].setValue(this.customerheader[0].bpGroup);

              try {
                this.form.controls["selectedbprt"].setValue(this.bplist.find(item => item.id === this.customerheader[0].tAGBpartner));
                this.onecustomer = this.Allcustomer.find(item => item.id === this.customerheader[0].newlead);
                this.form.controls["onecustomer"].setValue(this.Allcustomer.find(item => item.id === this.customerheader[0].newlead));
              } catch {}

              this.Bindallsalesarea("", this.customerheader[0].salesareaid);
              setTimeout(() => {
                this.existingcustomercompliance(customerId);
              }, 500);
            }, 1500); // 
            //   this.city = this.selectedarea.cttv$_identifier;
          }
        });
        this.newcustomerservice.getregion(this.pincodelist[0].region).subscribe(data => {
          const response = data['response'];
          this.mmstRegioncode = response.data[0].mmstRegioncode;
        });
        this.invalidpincode = '';
      } else {
        this.state = '';
        this.country = '';
        this.district = '';
        this.invalidpincode = 'Invalid Pincode';
        this.arealist = null;
        this.city = '';
        this.form.controls["selectedarea"].setValue(null);
      }
    }), error => {
      this.state = '';
      this.country = '';
      this.district = '';
      this.invalidpincode = 'Invalid Pincode';
      this.arealist = null;
      this.city = '';
    };
  }

  onChange(activity) {}

  addAddress() {
    this.isClickOnAddress = true;
  }

  onChangeArea() {
    this.selectedarea = this.form.controls["selectedarea"].value; //  console.log("this.selectedarea",this.selectedarea);

    if (this.selectedarea != null) this.city = this.selectedarea.cttv$_identifier;
  }

  loadingPresent() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this2.isLoading = true;
      return yield _this2.loadingController.create({
        message: 'Please wait ...',
        spinner: 'circles'
      }).then(a => {
        a.present().then(() => {
          if (!_this2.isLoading) {
            a.dismiss().then(() => console.log('abort laoding'));
          }
        });
      });
    })();
  }

  loadingDismiss() {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this3.isLoading = false;
      return yield _this3.loadingController.dismiss().then(() => console.log('loading dismissed'));
    })();
  }

  onSaveDraft1(value) {
    console.log('saving draft', value);

    try {
      var referalcode = "";

      if (this.isBPLEnabled == false && value.selectedbprt == null) {
        this.presentAlert("Message", "Alert", "Please Select Tagged Business Partner");
        return;
      }

      if (value.onecustomer != null && value.onecustomer != undefined) {
        referalcode = value.onecustomer.id;
      }

      var taggbp = "";

      if (value.selectedbprt != null) {
        taggbp = value.selectedbprt.id;
      }

      if (this.compliancedata.some(item => item.Compliance_Type === 'DL1' && item.num != null && item.num != "")) {
        if (this.compliancedata.some(item => item.Compliance_Type === 'DL1VD' && (item.num === null || item.num === ""))) {
          this.presentAlert("Message", "Warning", "Please Select Date for DL No 1");
          return;
        }
      }

      if (this.compliancedata.some(item => item.Compliance_Type === 'DL2' && item.num != null && item.num != "")) {
        if (this.compliancedata.some(item => item.Compliance_Type === 'DL2VD' && (item.num === null || item.num === ""))) {
          this.presentAlert("Message", "Warning", "Please Select Date for DL No 2");
          return;
        }
      }

      var compliancepangstvalid = false;
      var panvalidmsg = "";

      if (this.compliancedata != undefined || this.compliancedata != null) {
        for (var i = 0; i < this.compliancedata.length; i++) {
          //for PAN validation
          if (this.compliancedata[i].Compliance_Type == "PAN") {
            const pattern = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;

            if (!pattern.test(this.compliancedata[i].num) && !(this.compliancedata[i].num == '')) {
              compliancepangstvalid = true;
              panvalidmsg = "PAN no. in Compliance data is not valid.(Row no. " + (i + 1) + ")";
              this.presentAlert("Message", "Alert", panvalidmsg);
              return;
            } else {
              this.compliancedata[i].num = this.compliancedata[i].num.toUpperCase();
            }
          } //for GST validation


          if (this.compliancedata[i].Compliance_Type == "GST") {
            //  console.log("GST",this.compliancedata[i].num);
            const pattern = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[A-Z]{1}[A-Z\d]{1}$/;

            if (!pattern.test(this.compliancedata[i].num.toUpperCase()) && !(this.compliancedata[i].num == '')) {
              compliancepangstvalid = true;
              panvalidmsg = "GST no. in Compliance data is not valid.(Row no. " + (i + 1) + ")";
              this.presentAlert("Message", "Alert", panvalidmsg);
              return;
            } else {
              //----state code validation---------
              var gstn = this.compliancedata[i].num;

              if (gstn.length > 2) {
                if (this.mmstRegioncode != gstn.substr(0, 2)) {
                  //  this.presentAlert("Message","Alert","GST No. in Compliance must be in same state.(Row no. "+ (i+1)+")");
                  panvalidmsg = "GST No. in Compliance must be of same state.(Row no. " + (i + 1) + ")";
                  this.compliancedata[i].num = "";
                  compliancepangstvalid = true;
                  this.presentAlert("Message", "Alert", panvalidmsg);
                  return;
                } else {
                  this.compliancedata[i].num = this.compliancedata[i].num.toUpperCase();
                }
              } //----end state code validation-----

            }
          }
        }
      }

      if (compliancepangstvalid == false) {
        this.loadingPresent();
        var jsondata = {
          "cust_id": this.edtitcustid == undefined ? "" : this.edtitcustid,
          "referalcode": referalcode,
          "org_id": "0",
          "activity_id": value.selectedactivity,
          "cust_nature": value.selectedcn,
          "firstname": value.firstname,
          "middlename": value.middlename,
          "lastname": value.lastname,
          "firmname": value.firmname,
          "panno": value.panno != null ? value.panno.toUpperCase() : "",
          "img": this.img,
          "bpc_id": value.selectedbpcat,
          "complete": "",
          "mobileno": value.mobileno,
          "taggbp": taggbp,
          "add1": value.add1,
          "add2": value.add2,
          "add3": value.add3,
          "pincode": this.pincodelist[0].spincode,
          "area": value.selectedarea.id,
          "city": this.selectedarea.cttv,
          "state": this.pincodelist[0].region,
          "country": this.pincodelist[0].country,
          "district": this.pincodelist[0].district,
          "gst": value.gstno != null ? value.gstno.toUpperCase() : "",
          "email": value.email == "null" ? '' : value.email,
          "billing": value.isbill == true ? "true" : "false",
          "shipping": value.isShip == true ? 'true' : 'false',
          "mmstComplianceDetails_id": this.compliancedata == undefined ? "" : this.compliancedata,
          "level": value.selectedbpl,
          "salesareaid": value.selectedsalesarea.id
        };

        if (this.showPreferredTransportNameControl) {
          jsondata["cusPreferredTransport"] = value.preferredCustomerTransport.id;
          jsondata["cusPreferredTransportOther"] = this.showPreferredTransportNameControl;
          jsondata["preferredTransportName"] = value.preferredTransportName;
          jsondata["preferredTransportContact"] = value.preferredTransportContactNumberCtrl;
          jsondata["preferredTransportEmailID"] = value.preferredTransportEmailIDCtrl;
        }

        let formData = new FormData();

        for (let j = 0; j < this.compliancedata.length; j++) {
          let fileField = this.form.get(this.compliancedata[j].Compliance_Type).value;

          if (fileField !== null) {
            let files = fileField.queue;
            let k = 0;
            files.forEach(fileItem => {
              var ext = fileItem.file.name.substr(fileItem.file.name.lastIndexOf('.') + 1);
              var filename = this.compliancedata[j].Compliance_Type + '_' + k + '.' + ext;
              formData.append(this.compliancedata[j].Compliance_Type + '_' + k, fileItem.file.rawFile, filename);
              k = k + 1;
            });
            formData.append('nooffiles' + this.compliancedata[j].Compliance_Type, k.toString());
          }
        }

        formData.append('jsondata', JSON.stringify(jsondata));
        console.log("jsondata", jsondata);
        this.newcustomerservice.LeadComplete(formData).subscribe(data => {
          if (data != null) {
            this.response = data;

            if (this.response.resposemsg == "success") {
              this.loadingDismiss(); // this.presentAlert("Message","Success","Customer Saved in Draft.");

              this.presentAlert("Message", "Success", "Lead Created Successfully.");
              this.Resetpage();

              if (this.isNewRegistration == true) {
                this.router.navigateByUrl('/login');
              }
            } else {
              this.loadingDismiss();
              this.presentAlert("message", "Fail", this.response.logmsg);
            }
          }
        });
      } else {
        this.presentAlert("Alert!", "Warning", panvalidmsg);
      }
    } catch (error) {
      console.log(error);
      this.loadingDismiss();
    }
  }

  onChangegst() {
    var validgstn = false;

    try {
      var gstn = this.form.controls["gstno"].value;

      if (gstn != null && gstn != undefined && gstn != "") {
        if (gstn.length > 2) {
          if (this.mmstRegioncode != gstn.substr(0, 2)) {
            this.presentAlert("Message", "Alert", "GST Number must be of same state.");
            this.form.controls["gstno"].setValue("");
            validgstn = false;
          } else {
            validgstn = true;
          }
        } else {
          validgstn = true;
        }
      } else {
        validgstn = true;
      }
    } catch (error) {}

    return validgstn;
  }

  onConvertintoCustomer(value) {
    if (this.addressValidation()) {
      this.newcustomerservice.checkmobileno(this.form.controls["mobileno"].value).subscribe(data => {
        const response = data['response'];

        if (response.data.length > 0 && (this.edtitcustid == "" || this.edtitcustid == undefined)) {
          this.loadingDismiss();
          this.presentAlert("Message", "Alert!", "Mobile no. is already exists.");
          this.form.controls["mobileno"].setValue("");
        } else {
          var compliancedatavalid = false;
          var compliancepangstvalid = false;
          var panvalidmsg = "";
          var gstvalidmsg = "";

          if (this.isBPLEnabled == false && value.selectedbprt == null) {
            this.presentAlert("Message", "Alert", "Please Select Tagged Business Partner");
            return;
          }

          if (this.onChangegst() == false) {
            return;
          }

          if (this.compliancedata != undefined || this.compliancedata != null) {
            //validation for the DL1 and DL2
            // var DL1=false;
            // var DL2=false;
            if (this.compliancedata.some(item => item.Compliance_Type === 'DL1' && item.num != null && item.num != "")) {
              if (this.compliancedata.some(item => item.Compliance_Type === 'DL1VD' && (item.num === null || item.num === ""))) {
                this.presentAlert("Message", "Warning", "Please Select Date for DL1");
                return;
              }
            }

            if (this.compliancedata.some(item => item.Compliance_Type === 'DL2' && item.num != null && item.num != "")) {
              if (this.compliancedata.some(item => item.Compliance_Type === 'DL2VD' && (item.num === null || item.num === ""))) {
                this.presentAlert("Message", "Warning", "Please Select Date for DL2");
                return;
              }
            }

            for (var i = 0; i < this.compliancedata.length; i++) {
              //for PAN validation
              if (this.compliancedata[i].Compliance_Type == "PAN") {
                const pattern = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;

                if (!pattern.test(this.compliancedata[i].num) && !(this.compliancedata[i].num == '')) {
                  compliancepangstvalid = true;
                  panvalidmsg = "PAN no. in Compliance data is not valid.(Row no. " + (i + 1) + ")";
                  this.presentAlert("Message", "Alert", panvalidmsg);
                  return;
                } else {
                  this.compliancedata[i].num = this.compliancedata[i].num.toUpperCase();
                }
              } //for GST validation


              if (this.compliancedata[i].Compliance_Type == "GST") {
                const pattern = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[A-Z]{1}[A-Z\d]{1}$/;

                if (!pattern.test(this.compliancedata[i].num.toUpperCase()) && !(this.compliancedata[i].num == '')) {
                  compliancepangstvalid = true;
                  panvalidmsg = "GST no. in Compliance data is not valid.(Row no. " + (i + 1) + ")";
                  this.presentAlert("Message", "Alert", panvalidmsg);
                  return;
                } else {
                  //----state code validation---------
                  var gstn = this.compliancedata[i].num;

                  if (gstn.length > 2) {
                    if (this.mmstRegioncode != gstn.substr(0, 2)) {
                      //  this.presentAlert("Message","Alert","GST No. in Compliance must be in same state.(Row no. "+ (i+1)+")");
                      panvalidmsg = "GST No. in Compliance must be of same state.(Row no. " + (i + 1) + ")";
                      this.compliancedata[i].num = "";
                      compliancepangstvalid = true;
                      this.presentAlert("Message", "Alert", panvalidmsg);
                      return;
                    } else {
                      this.compliancedata[i].num = this.compliancedata[i].num.toUpperCase();
                    }
                  } //----end state code validation-----

                }
              }

              if (this.compliancedata[i].ismandatory == true && this.compliancedata[i].num === "") {
                compliancedatavalid = true;
                this.presentAlert("Message", "Alert", "Please fill Compliance data.");
                return;
              }

              if (this.compliancedata[i].uploadImg) {
                let fileField = this.form.get(this.compliancedata[i].Compliance_Type).value;

                if (fileField === null && !this.compliancedata[i].isImage) {
                  compliancedatavalid = true;
                  this.presentAlert("Message", "Alert", "Please fill Compliance data.");
                  return;
                }
              }
            }
          }

          var referalcode = "";

          if (value.onecustomer != null) {
            referalcode = value.onecustomer.id;
          }

          var taggbp = "";

          if (value.selectedbprt != null) {
            taggbp = value.selectedbprt.id;
          }

          this.loadingPresent();
          var jsondata = {
            "cust_id": this.edtitcustid == undefined ? "" : this.edtitcustid,
            "referalcode": referalcode,
            "org_id": "0",
            "activity_id": value.selectedactivity,
            "cust_nature": value.selectedcn,
            "firstname": value.firstname,
            "middlename": value.middlename,
            "lastname": value.lastname,
            "firmname": value.firmname,
            "panno": value.panno != null ? value.panno.toUpperCase() : "",
            "img": this.img,
            "bpc_id": value.selectedbpcat,
            "complete": "true",
            "mobileno": value.mobileno,
            "taggbp": taggbp,
            "add1": value.add1,
            "add2": value.add2,
            "add3": value.add3,
            "pincode": this.pincodelist[0].spincode,
            "area": value.selectedarea.id,
            "city": this.selectedarea.cttv,
            "state": this.pincodelist[0].region,
            "country": this.pincodelist[0].country,
            "district": this.pincodelist[0].district,
            "gst": value.gstno != null ? value.gstno.toUpperCase() : "",
            "email": value.email,
            "billing": value.isbill == true ? "true" : "false",
            "shipping": value.isShip == true ? 'true' : 'false',
            "mmstComplianceDetails_id": this.compliancedata == undefined ? "" : this.compliancedata,
            "level": value.selectedbpl,
            "salesareaid": value.selectedsalesarea.id
          };

          if (this.showPreferredTransportNameControl) {
            jsondata["cusPreferredTransport"] = value.preferredCustomerTransport.id;
            jsondata["cusPreferredTransportOther"] = this.showPreferredTransportNameControl;
            jsondata["preferredTransportName"] = value.preferredTransportName;
            jsondata["preferredTransportContact"] = value.preferredTransportContactNumberCtrl;
            jsondata["preferredTransportEmailID"] = value.preferredTransportEmailIDCtrl;
          }

          let formData = new FormData();

          for (let j = 0; j < this.compliancedata.length; j++) {
            let fileField = this.form.get(this.compliancedata[j].Compliance_Type).value;

            if (fileField !== null) {
              let files = fileField.queue;
              let k = 0;
              files.forEach(fileItem => {
                var ext = fileItem.file.name.substr(fileItem.file.name.lastIndexOf('.') + 1);
                var filename = this.compliancedata[j].Compliance_Type + '_' + k + '.' + ext;
                formData.append(this.compliancedata[j].Compliance_Type + '_' + k, fileItem.file.rawFile, filename);
                k = k + 1;
              });
              formData.append('nooffiles' + this.compliancedata[j].Compliance_Type, k.toString());
            }
          }

          formData.append('jsondata', JSON.stringify(jsondata));
          this.newcustomerservice.LeadComplete(formData).subscribe(data => {
            if (data != null) {
              this.response = data;

              if (this.response.resposemsg == "success") {
                this.loadingDismiss();
                this.presentAlert("Message", "Success", "Lead Converted Successfully.");
                this.Resetpage();

                if (this.isNewRegistration == true) {
                  this.router.navigateByUrl('/login');
                }
              } else {
                this.loadingDismiss();
                this.presentAlert("Message", "Fail", this.response.logmsg);
              }
            } else {
              this.loadingDismiss();
            }
          });
        }
      });
    } else {
      this.presentAlert("Message", "Error!", "Address line cannot be same");
    }
  }

  Resetpage() {
    this.edtitcustid = "";
    this.form.reset();
    this.IsExisting = false;
    this.fileUrl = "";
    this.compliancedata = null;
    this.img = '';
    this.Iscnvalidate = false;
    this.arealist = null;
    this.state = '';
    this.country = '';
    this.district = '';
    this.city = '';

    if (this.activitylist.length == 1) {
      this.form.controls["selectedactivity"].setValue(this.activitylist[0].id); // this.onActChange();
    } // this.router.navigateByUrl('/newcustomer');
    //this.router.navigateByUrl('/newcustomer');
    //this.router.navigate([this.router.url]);

  }

  ChosePic(Type, mmstComplianceDetails_id) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this4.alertCtrl.create({
        header: 'Select Option',
        message: "Select Option to get Picture.",
        buttons: [{
          text: 'Gallery',
          handler: data => {
            _this4.getimage(Type, mmstComplianceDetails_id);
          }
        }, {
          text: 'Camera',
          handler: data => {
            _this4.takePicture(Type, mmstComplianceDetails_id);
          }
        }]
      });
      yield alert.present();
    })();
  }

  ImageViewr(img, rowcompliancedata) {
    var _this5 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      var msg = "";

      if (rowcompliancedata != null) {
        msg = '<div>' + '<img class="viewImagecss" src="data:image/png;base64,' + img + '">' + '</div>';
      } else {
        msg = '<div>' + '<img class="viewImagecss" src="' + img + '">' + '</div>';
      }

      const alert = yield _this5.alertCtrl.create({
        message: msg,
        buttons: [{
          text: 'Remove',
          handler: data => {
            //this.POimg64=null
            if (rowcompliancedata != null) {
              _this5.compliancedata = _this5.compliancedata.map(function (comdata) {
                if (comdata.scompType == rowcompliancedata.scompType) {
                  comdata.imgcomp = comdata.imgcomp.filter(item => item != img);
                }

                return comdata;
              });
            } else {
              _this5.fileUrl = null;
            }
          }
        }, {
          text: 'OK'
        }]
      });
      yield alert.present();
    })();
  }

  change(value) {
    //manually launch change detection
    this.cdRef.detectChanges(); // this.username = value.length > 8 ? value.substring(0,8) : value;

    this.form.controls["mobileno"].setValue(value.length > 8 ? value.substring(0, 8) : value);
  }

  editCustomer(customerId) {
    //Referal Code
    if (customerId != '') {
      this.loadingPresent(); //  this.newcustomerservice.geteditcustmerheader(customerId).subscribe(data => {

      this.newcustomerservice.geteditcustmerdetailapi(customerId).subscribe(data => {
        const response1 = data;
        this.customerheader = response1;
        console.log("Pravin", response1); //bind Category

        this.newcustomerservice.getBPCategory(this.customerheader[0].mmstOrgAct).subscribe(data => {
          const response = data['response'];
          this.bpcatlist = response.data;
          this.selectedbpcat = this.customerheader[0].bpGroup;
        });
        this.form.controls["selectedactivity"].setValue(this.customerheader[0].mmstOrgAct);
        this.form.controls["selectedcn"].setValue(this.customerheader[0].scusNature);
        this.form.controls["firstname"].setValue(this.customerheader[0].sfname);
        this.form.controls["middlename"].setValue(this.customerheader[0].smname);
        this.form.controls["lastname"].setValue(this.customerheader[0].slname);
        this.form.controls["panno"].setValue(this.customerheader[0].spanno);
        this.form.controls["firmname"].setValue(this.customerheader[0].sfirmName);
        this.form.controls["email"].setValue(this.customerheader[0].semail == "null" ? '' : this.customerheader[0].semail);
        this.form.controls["mobileno"].setValue(this.customerheader[0].smobile);
        this.form.controls["selectedsalesarea"].setValue(this.customerheader[0].salesarea);

        if (!!this.customerheader[0].preferred_cus_transport) {
          this.newcustomerservice.getPreferredTransportModes().subscribe(response => {
            this.preferredTransportList = response[0];
            this.preferredTransportList.forEach(transportResponse => {
              if (transportResponse.id == this.customerheader[0].preferred_cus_transport) {
                this.selectedPreferredTransport = transportResponse;
              }
            });
          });
        } //  this.form.controls["preferredTransportName"].setValue("PRavin");


        setTimeout(() => {
          if (!!this.customerheader[0].preferred_transport_name) {
            this.form.controls["preferredTransportName"].setValue(this.customerheader[0].preferred_transport_name);
          }

          if (!!this.customerheader[0].preferred_transport_contact) {
            this.form.controls["preferredTransportContactNumberCtrl"].setValue(this.customerheader[0].preferred_transport_contact);
          }

          if (!!this.customerheader[0].preferred_transport_email && this.customerheader[0].preferred_transport_email != "null") {
            this.form.controls["preferredTransportEmailIDCtrl"].setValue(this.customerheader[0].preferred_transport_email);
          }
        }, 1000);
        this.IsExisting = true;

        if (this.customerheader[0].mmstLovVal$_identifier == "Secondary") {
          this.form.controls["selectedbpl"].setValue("S");
          this.isBPLEnabled = false;
        }

        if (this.customerheader[0].mmstLovVal$_identifier == "Primary") {
          this.form.controls["selectedbpl"].setValue("P");
          this.isBPLEnabled = true;
        }

        this.customerbilling = this.customerheader;
        this.form.controls["add1"].setValue(this.customerbilling[0].saddress1);
        this.form.controls["add2"].setValue(this.customerbilling[0].saddress2 == "null" ? '' : this.customerbilling[0].saddress2);
        this.form.controls["add3"].setValue(this.customerbilling[0].saddress3 == "null" ? '' : this.customerbilling[0].saddress3);
        this.form.controls["pincode"].setValue(this.customerbilling[0].spincode);
        this.form.controls["isbill"].setValue(this.customerbilling[0].isbilling.toString() === "Y" ? true : false);
        this.form.controls["isShip"].setValue(this.customerbilling[0].isshipping.toString() === "Y" ? true : false);
        this.form.controls["gstno"].setValue(this.customerbilling[0].mmstSgstno);
        this.onActChange();
        this.onChangepin(this.customerbilling[0].mmstPostOffVal, customerId); //
      }); //   this.newcustomerservice.geteditcustmerbilling(customerId).subscribe(data => {
      //      const response = data['response'];
      // 
      //   });
    }

    this.loadingDismiss();
  }

  existingcustomercompliance(customerId) {
    this.newcustomerservice.getCompilanceDataapi("", customerId).subscribe(data11 => {
      this.compliancedata = data11;

      for (var c = 0; c < this.compliancedata.length; c++) {
        var varscompType = this.compliancedata[c].scompType;
        this.compliancedata[c].scompType = this.compliancedata[c].Compliance_Type;
        this.compliancedata[c].Compliance_Type = varscompType;

        try {
          if (this.compliancedata[c].Compliance_Type == "DL1VD" && this.compliancedata[c].num != "" && this.compliancedata[c].num != null) {
            var a1 = this.compliancedata[c].num.split("-");
            var dt = new Date(a1[2] + '-' + a1[1] + '-' + a1[0] + 'T00:00Z');
            this.compliancedata[c].num = dt.toISOString();
          }

          if (this.compliancedata[c].Compliance_Type == "DL2VD" && this.compliancedata[c].num != "" && this.compliancedata[c].num != null) {
            {
              var a1 = this.compliancedata[c].num.split("-");
              var dt = new Date(a1[2] + '-' + a1[1] + '-' + a1[0] + 'T00:00Z');
              this.compliancedata[c].num = dt.toISOString();
            }
          }
        } catch (error) {
          this.compliancedata[c].num = "";
        }
      }
    });
  }

  existingcustomercompliance1(customerId) {
    this.newcustomerservice.geteditcustmercompliance(customerId).subscribe(data => {
      const compliancedataresponse = data['response']; //var that=this;

      for (var i = 0; i < compliancedataresponse.data.length; i++) {
        var Isfoundsame = false;

        for (var j = 0; j < this.compliancedata.length; j++) {
          if (compliancedataresponse.data[i].scomplianceType == this.compliancedata[j].Compliance_Type) {
            Isfoundsame = true;
            this.compliancedata[j].mmstComplianceDetails_id = compliancedataresponse.data[i].id;
            this.compliancedata[j].Compliance_Type = compliancedataresponse.data[i].scomplianceType;
            this.compliancedata[j].scompType = compliancedataresponse.data[i].scomplianceType;
            this.compliancedata[j].num = compliancedataresponse.data[i].snumber == null ? "" : compliancedataresponse.data[i].snumber;
            this.compliancedata[j].isImage = compliancedataresponse.data[i].image == null ? "" : compliancedataresponse.data[i].image;
          }
        }
      }
    });
  }

  onCusPreTransChange() {
    try {
      // console.log("SELECTED PREPED TRANS",this.selectedPreferredTransport);
      this.form.controls['preferredTransportName'].reset();
      this.form.controls['preferredTransportContactNumberCtrl'].reset();
      this.form.controls['preferredTransportEmailIDCtrl'].reset();

      if (this.selectedPreferredTransport.name == "Others") {
        this.showPreferredTransportNameControl = true;
        let control2 = null;
        control2 = this.form.get('preferredTransportName');
        control2.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required);
        control2.updateValueAndValidity();
        let control4 = null;
        control4 = this.form.get('preferredTransportContactNumberCtrl');
        control4.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$"));
        control4.updateValueAndValidity();
      } else {
        setTimeout(() => {
          this.showPreferredTransportNameControl = false;
        });
        let control2 = null;
        control2 = this.form.get('preferredTransportName');
        control2.clearValidators();
        control2.updateValueAndValidity();
        let control4 = null;
        control4 = this.form.get('preferredTransportContactNumberCtrl');
        control4.clearValidators();
        control4.updateValueAndValidity();
      }
    } catch (error) {
      console.log(error);
    }
  }

  onShippingChange(value) {
    var _this6 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        console.log("Shipping Address ", value.detail.checked);

        if (value.detail.checked) {
          if (_this6.loginauth.selectedactivity.preferred_transport_required == 'Y') {
            let control1 = null;
            control1 = _this6.form.get('preferredCustomerTransport');
            control1.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_13__.Validators.required);
            control1.updateValueAndValidity();
            _this6.showPreferredTransportControl = true;

            _this6.common.loadingPresent();

            const result = yield _this6.newcustomerservice.getPreferredTransportModes().toPromise();
            _this6.preferredTransportList = result[0]; //   console.log("Preferred Transport Modes ",this.preferredTransportList);

            _this6.common.loadingDismiss();
          }
        } else {
          setTimeout(() => {
            _this6.showPreferredTransportControl = false;
            _this6.showPreferredTransportNameControl = false;
          });
          let control1 = null;
          control1 = _this6.form.get('preferredCustomerTransport');
          control1.clearValidators();
          control1.updateValueAndValidity();

          _this6.form.controls['preferredCustomerTransport'].reset();
        }
      } catch (error) {
        _this6.common.loadingDismiss();

        console.log(error);
      }
    })();
  }

  onChangePreferredTransportContact() {}

  testMethod(form) {
    console.log(form);
  }

};

NewcustomerPage.ctorParameters = () => [{
  type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_5__.LoginauthService
}, {
  type: _newcustomer_service__WEBPACK_IMPORTED_MODULE_6__.NewcustomerService
}, {
  type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_12__.GenericHttpClientService
}, {
  type: _awesome_cordova_plugins_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__.ImagePicker
}, {
  type: _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_4__.Camera
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_16__.Router
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_16__.ActivatedRoute
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_13__.FormBuilder
}, {
  type: _provider_validator_helper__WEBPACK_IMPORTED_MODULE_7__.Validator
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_17__.LoadingController
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_17__.AlertController
}, {
  type: _angular_core__WEBPACK_IMPORTED_MODULE_18__.ChangeDetectorRef
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_17__.Platform
}, {
  type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_8__.Commonfun
}, {
  type: _provider_message_helper__WEBPACK_IMPORTED_MODULE_11__.Message
}, {
  type: _ionic_storage__WEBPACK_IMPORTED_MODULE_9__.Storage
}, {
  type: _login_login_page__WEBPACK_IMPORTED_MODULE_10__.LoginPage
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_17__.MenuController
}];

NewcustomerPage = (0,tslib__WEBPACK_IMPORTED_MODULE_19__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_18__.Component)({
  selector: 'app-newcustomer',
  template: _newcustomer_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_newcustomer_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], NewcustomerPage);


/***/ }),

/***/ 55700:
/*!**************************************************************!*\
  !*** ./src/app/newcustomer/newcustomer.page.scss?ngResource ***!
  \**************************************************************/
/***/ ((module) => {

module.exports = "ion-scroll[scrollX] {\n  white-space: nowrap;\n  overflow: visible;\n  overflow-y: auto;\n}\nion-scroll[scrollX] .scroll-item {\n  display: inline-block;\n}\nion-scroll[scrollX] .selectable-icon {\n  margin: 10px 20px 10px 20px;\n  color: red;\n  font-size: 100px;\n}\nion-scroll[scrollX] ion-avatar img {\n  margin: 10px;\n}\nion-scroll[scroll-avatar] {\n  height: 60px;\n}\n/* Hide ion-content scrollbar */\n::-webkit-scrollbar {\n  display: none;\n}\nh5 {\n  font-style: oblique;\n  color: darkcyan;\n  font-size: large;\n}\nh5 ion-icon {\n  color: lightcoral;\n}\n.inputfile {\n  color: transparent;\n}\nion-popover {\n  --width: 320px;\n}\nion-popover.dateTimePopover {\n  --offset-y: -350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5ld2N1c3RvbWVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1CQUFBO0VBRUEsaUJBQUE7RUFDQSxnQkFBQTtBQUFGO0FBRUU7RUFDRSxxQkFBQTtBQUFKO0FBR0U7RUFDRSwyQkFBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtBQURKO0FBSUU7RUFDRSxZQUFBO0FBRko7QUFNQTtFQUNFLFlBQUE7QUFIRjtBQU1BLCtCQUFBO0FBQ0E7RUFDRSxhQUFBO0FBSEY7QUFNQTtFQUNFLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBSEY7QUFJRTtFQUNFLGlCQUFBO0FBRko7QUFPQTtFQUNFLGtCQUFBO0FBSkY7QUFNQTtFQUNFLGNBQUE7QUFIRjtBQUtBO0VBQ0UsNkJBQUE7QUFGRiIsImZpbGUiOiJuZXdjdXN0b21lci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tc2Nyb2xsW3Njcm9sbFhdIHtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAvLyBoZWlnaHQ6IDEyMHB4O1xuICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgb3ZlcmZsb3cteTogYXV0bztcbi8vIHdpZHRoOjEwMCU7XG4gIC5zY3JvbGwtaXRlbSB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB9XG5cbiAgLnNlbGVjdGFibGUtaWNvbntcbiAgICBtYXJnaW46IDEwcHggMjBweCAxMHB4IDIwcHg7XG4gICAgY29sb3I6IHJlZDtcbiAgICBmb250LXNpemU6IDEwMHB4O1xuICB9XG5cbiAgaW9uLWF2YXRhciBpbWd7XG4gICAgbWFyZ2luOiAxMHB4O1xuICB9XG59XG5cbmlvbi1zY3JvbGxbc2Nyb2xsLWF2YXRhcl17XG4gIGhlaWdodDogNjBweDtcbn1cblxuLyogSGlkZSBpb24tY29udGVudCBzY3JvbGxiYXIgKi9cbjo6LXdlYmtpdC1zY3JvbGxiYXJ7XG4gIGRpc3BsYXk6bm9uZTtcbn1cblxuaDV7XG4gIGZvbnQtc3R5bGU6IG9ibGlxdWU7XG4gIGNvbG9yOiBkYXJrY3lhbjtcbiAgZm9udC1zaXplOiBsYXJnZTtcbiAgaW9uLWljb257XG4gICAgY29sb3I6IGxpZ2h0Y29yYWw7XG4gIH1cblxufVxuXG4uaW5wdXRmaWxlIHtcbiAgY29sb3I6IHRyYW5zcGFyZW50O1xufVxuaW9uLXBvcG92ZXIge1xuICAtLXdpZHRoOiAzMjBweDtcbn1cbmlvbi1wb3BvdmVyLmRhdGVUaW1lUG9wb3ZlciB7XG4gIC0tb2Zmc2V0LXk6IC0zNTBweCAhaW1wb3J0YW50O1xuICB9Il19 */";

/***/ }),

/***/ 79836:
/*!**************************************************************!*\
  !*** ./src/app/newcustomer/newcustomer.page.html?ngResource ***!
  \**************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"start\" *ngIf=\"!isNewRegistration\">\r\n      <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      New Lead\r\n    </ion-title>\r\n    <ion-buttons *ngIf=\"!isNewRegistration\" slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\"\r\n      routerLink=\"/home\">\r\n      <ion-icon name=\"home\"></ion-icon>\r\n    </ion-buttons>\r\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\r\n      <ion-icon name=\"refresh\"></ion-icon>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n<ion-content>\r\n\r\n  <form [formGroup]=\"form\" (ngSubmit)=\"onSaveDraft(form.value)\">\r\n    <ion-grid>\r\n      <ion-card>\r\n        <ion-card-header style=\"background: #FFCC80;\">\r\n          Customer :\r\n        </ion-card-header>\r\n        <ion-row>\r\n          <!-- Activity -->\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"stacked\">Activity<span style=\"color:red!important\">*</span></ion-label>\r\n              <ion-select formControlName=\"selectedactivity\" interface=\"popover\"\r\n                (ionChange)=\"onActChange()\" multiple=\"false\" placeholder=\"Select Activity\">\r\n                <ion-select-option *ngFor=\"let activity of activitylist\" [value]=\"activity.id\">{{activity.name}}\r\n                </ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n            <div padding-left>\r\n              <ng-container *ngFor=\"let validation of validation_messages.selectedactivity\">\r\n                <div\r\n                  *ngIf=\"form.get('selectedactivity').hasError(validation.type) && form.get('selectedactivity').touched\">\r\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n          </ion-col>\r\n          <!-- Customer Nature -->\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"stacked\">Customer Nature<span style=\"color:red!important\">*</span></ion-label>\r\n              <ion-select #C formControlName=\"selectedcn\" interface=\"popover\" placeholder=\"Select One\"\r\n                (ionChange)='onChangecn()'>\r\n                <ion-select-option value=\"I\">Individual</ion-select-option>\r\n                <ion-select-option value=\"F\">Firm</ion-select-option>\r\n                <ion-select-option value=\"H\">HUF</ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n            <div padding-left>\r\n              <ng-container *ngFor=\"let validation of validation_messages.selectedcn\">\r\n                <div *ngIf=\"form.get('selectedcn').hasError(validation.type) && form.get('selectedcn').touched\">\r\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n          </ion-col>\r\n          <!-- Firm Name -->\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"floating\">Firm Name<span style=\"color:red!important\">{{firmnamereq}}</span>\r\n              </ion-label>\r\n              <ion-input formControlName=\"firmname\" type=\"text\"></ion-input>\r\n            </ion-item>\r\n            <div padding-left>\r\n              <ng-container *ngFor=\"let validation of validation_messages.firmname\">\r\n                <div *ngIf=\"form.get('firmname').hasError(validation.type) && form.get('firmname').touched\">\r\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <!-- First Name -->\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"floating\">First Name<span style=\"color:red!important\">{{firstnamereq}}</span>\r\n              </ion-label>\r\n              <ion-input formControlName=\"firstname\" type=\"text\"></ion-input>\r\n            </ion-item>\r\n            <div padding-left>\r\n              <ng-container *ngFor=\"let validation of validation_messages.firstname\">\r\n                <div *ngIf=\"form.get('firstname').hasError(validation.type) && form.get('firstname').touched\">\r\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n          </ion-col>\r\n          <!-- Middle Name -->\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"floating\">Middle Name</ion-label>\r\n              <ion-input formControlName=\"middlename\" type=\"text\"></ion-input>\r\n            </ion-item>\r\n          </ion-col>\r\n          <!-- Last Name -->\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"floating\">Last Name<span style=\"color:red!important\">{{lastnamereq}}</span>\r\n              </ion-label>\r\n              <ion-input formControlName=\"lastname\" type=\"text\"></ion-input>\r\n            </ion-item>\r\n            <div padding-left>\r\n              <ng-container *ngFor=\"let validation of validation_messages.lastname\">\r\n                <div *ngIf=\"form.get('lastname').hasError(validation.type) && form.get('lastname').touched\">\r\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-card>\r\n      <ion-card>\r\n        <ion-card-header style=\"background: #FFCC80;\">\r\n          Address :\r\n        </ion-card-header>\r\n        <ion-row>\r\n          <!-- Address Line 1 -->\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"floating\">Address Line 1<span style=\"color:red!important\">*</span></ion-label>\r\n              <ion-input formControlName=\"add1\" type=\"text\"></ion-input>\r\n            </ion-item>\r\n            <div padding-left>\r\n              <ng-container *ngFor=\"let validation of validation_messages.add1\">\r\n                <div *ngIf=\"form.get('add1').hasError(validation.type) && form.get('add1').touched\">\r\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n          </ion-col>\r\n          <!-- Address Line 2 -->\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"floating\">Address Line 2</ion-label>\r\n              <ion-input formControlName=\"add2\" type=\"text\"></ion-input>\r\n            </ion-item>\r\n          </ion-col>\r\n          <!-- Address Line 3 -->\r\n          <ion-col>\r\n            <ion-item>\r\n              <ion-label position=\"floating\">Address Line 3</ion-label>\r\n              <ion-input formControlName=\"add3\" type=\"text\"></ion-input>\r\n            </ion-item>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-row>\r\n                <ion-col>\r\n                  <ion-label position=\"floating\">Pin Code<span style=\"color:red!important\">*</span></ion-label>\r\n                </ion-col>\r\n              </ion-row>\r\n              <ion-input formControlName=\"pincode\" type=\"number\" (change)='onChangepin()'></ion-input>\r\n            </ion-item>\r\n            <div padding-left>\r\n              <ng-container *ngFor=\"let validation of validation_messages.pincode\">\r\n                <div *ngIf=\"form.get('pincode').hasError(validation.type) && form.get('pincode').touched\">\r\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n            <div padding-left>\r\n              <ng-container>\r\n                <div>\r\n                  <p style=\"color: red;font-size: small;\">{{invalidpincode}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"stacked\">Area<span style=\"color:red!important\">*</span></ion-label>\r\n              <ion-select #C formControlName=\"selectedarea\" interface=\"popover\"\r\n                (ionChange)=\"onChangeArea()\" multiple=\"false\" placeholder=\"Select Area\" required>\r\n                <ion-select-option *ngFor=\"let area of arealist\" [value]=\"area\">{{area.area}}</ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n            <div padding-left>\r\n              <ng-container *ngFor=\"let validation of validation_messages.selectedarea\">\r\n                <div *ngIf=\"form.get('selectedarea').hasError(validation.type) && form.get('selectedarea').touched\">\r\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"floating\">City</ion-label>\r\n              <ion-input type=\"text\" value=\"{{city}}\" disabled=\"true\"></ion-input>\r\n            </ion-item>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"floating\">District</ion-label>\r\n              <ion-input type=\"text\" value=\"{{district}}\" disabled=\"true\"></ion-input>\r\n            </ion-item>\r\n          </ion-col>\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"floating\">State</ion-label>\r\n              <ion-input type=\"text\" value=\"{{state}}\" disabled=\"true\"></ion-input>\r\n            </ion-item>\r\n          </ion-col>\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"floating\">Country</ion-label>\r\n              <ion-input type=\"text\" value=\"{{country}}\" disabled=\"true\"></ion-input>\r\n            </ion-item>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"stacked\">Sales Area<span style=\"color:red!important\">*</span></ion-label>\r\n              <ionic-selectable placeholder=\"Select Sales Area\" formControlName=\"selectedsalesarea\"\r\n                [items]=\"salesarealist\" itemValueField=\"id\" itemTextField=\"_identifier\" [canSearch]=\"true\"\r\n                (onClose)=\"onClose($event)\" (onSearch)=\"salseareasearchChange($event)\">\r\n              </ionic-selectable>\r\n\r\n            </ion-item>\r\n            <div padding-left>\r\n              <ng-container *ngFor=\"let validation of validation_messages.selectedsalesarea\">\r\n                <div\r\n                  *ngIf=\"form.get('selectedsalesarea').hasError(validation.type) && form.get('selectedsalesarea').touched\">\r\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label>Billing</ion-label>\r\n              <ion-checkbox slot=\"end\" formControlName=\"isbill\"></ion-checkbox>\r\n            </ion-item>\r\n          </ion-col>\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label>Shipping</ion-label>\r\n              <ion-checkbox slot=\"end\" formControlName=\"isShip\" (ionChange)=\"onShippingChange($event)\">\r\n              </ion-checkbox>\r\n            </ion-item>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"showPreferredTransportControl\">\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"stacked\">Customer Preferred Transport<span style=\"color:red!important\">*</span>\r\n              </ion-label>\r\n              <ion-select  [(ngModel)]=\"selectedPreferredTransport\" formControlName=\"preferredCustomerTransport\" interface=\"popover\"\r\n                (ionChange)=\"onCusPreTransChange()\" multiple=\"false\" placeholder=\"Select Activity\">\r\n                <ion-select-option *ngFor=\"let preferredTransport of preferredTransportList\"\r\n                  [value]=\"preferredTransport\">{{preferredTransport.name}}\r\n                </ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n          </ion-col>\r\n          <ion-col size=\"12\" size-lg=\"4\" *ngIf=\"showPreferredTransportNameControl\">\r\n            <ion-item>\r\n              <ion-label position=\"floating\">Preferred Transport Name<span style=\"color:red!important\">*</span>\r\n              </ion-label>\r\n              <ion-input formControlName=\"preferredTransportName\" type=\"text\"></ion-input>\r\n            </ion-item>\r\n            <div padding-left>\r\n              <ng-container *ngFor=\"let validation of validation_messages.preferredTransportNameMss\">\r\n                <div\r\n                  *ngIf=\"form.get('preferredTransportName').hasError(validation.type) && form.get('preferredTransportName').touched\">\r\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"12\" size-lg=\"4\" *ngIf=\"showPreferredTransportNameControl\">\r\n            <ion-item>\r\n              <ion-label position=\"floating\">Preferred Transport Contact Number</ion-label>\r\n              <ion-input formControlName=\"preferredTransportContactNumberCtrl\" type=\"number\"\r\n                (change)='onChangePreferredTransportContact()'></ion-input>\r\n            </ion-item>\r\n            <div padding-left>\r\n              <ng-container *ngFor=\"let validation of validation_messages.preferredTransportContactNumberCtrlMss\">\r\n                <div\r\n                  *ngIf=\"form.get('preferredTransportContactNumberCtrl').hasError(validation.type) && form.get('preferredTransportContactNumberCtrl').touched\">\r\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"12\" size-lg=\"4\" *ngIf=\"showPreferredTransportNameControl\">\r\n            <ion-item>\r\n              <ion-label position=\"floating\">Preferred Transport Email ID</ion-label>\r\n              <ion-input formControlName=\"preferredTransportEmailIDCtrl\"\r\n              pattern=\"[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})\">\r\n              </ion-input>\r\n            </ion-item>\r\n            <div padding-left>\r\n              <ng-container *ngFor=\"let validation of validation_messages.preferredTransportEmailIDCtrl\">\r\n                <div\r\n                  *ngIf=\"form.get('preferredTransportEmailIDCtrl').hasError(validation.type) && form.get('preferredTransportEmailIDCtrl').touched\">\r\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-card>\r\n      <ion-card>\r\n        <ion-card-header style=\"background: #FFCC80;\">\r\n          Other Information:\r\n        </ion-card-header>\r\n        <ion-row>\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"floating\">Mobile No<span style=\"color:red!important\">*</span></ion-label>\r\n              <ion-input formControlName=\"mobileno\" type=\"number\" (change)='onChangemobileno()'\r\n                [disabled]=\"IsExisting\"></ion-input>\r\n            </ion-item>\r\n            <div padding-left>\r\n              <ng-container *ngFor=\"let validation of validation_messages.mobileno\">\r\n                <div *ngIf=\"form.get('mobileno').hasError(validation.type) && form.get('mobileno').touched\">\r\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"floating\">Email</ion-label>\r\n              <ion-input formControlName=\"email\"\r\n                pattern=\"[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})\">\r\n              </ion-input>\r\n            </ion-item>\r\n            <div padding-left>\r\n              <ng-container *ngFor=\"let validation of validation_messages.email\">\r\n                <div *ngIf=\"form.get('email').hasError(validation.type) && form.get('email').touched\">\r\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"stacked\">Business Partner Category<span style=\"color:red!important\">*</span>\r\n              </ion-label>\r\n              <ion-select formControlName=\"selectedbpcat\" interface=\"popover\" (ionChange)=\"onBpChange()\"\r\n                multiple=\"false\" placeholder=\"Select Activity\">\r\n                <ion-select-option *ngFor=\"let bpcat of bpcatlist\" [value]=\"bpcat.id\">{{bpcat._identifier}}\r\n                </ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n            <div padding-left>\r\n              <ng-container *ngFor=\"let validation of validation_messages.selectedbpcat\">\r\n                <div *ngIf=\"form.get('selectedbpcat').hasError(validation.type) && form.get('selectedbpcat').touched\">\r\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\r\n                </div>\r\n              </ng-container>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\" size-lg=\"4\">\r\n            <ion-item>\r\n              <ion-label position=\"stacked\">Business Partner Level</ion-label>\r\n              <ion-select placeholder=\"Select One\" formControlName=\"selectedbpl\" interface=\"popover\"\r\n                (ionChange)=\"onBPLChange()\">\r\n                <ion-select-option value=\"P\">Primary</ion-select-option>\r\n                <ion-select-option value=\"S\">Secondary</ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n            <div>\r\n              <span style=\"font-size: small;\">(Required for convert to customer.)</span>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"12\" size-lg=\"4\" *ngIf=\"!isBPLEnabled\">\r\n            <ion-item>\r\n              <ion-label position=\"stacked\">Tagged Business Partner</ion-label>\r\n              <ionic-selectable\r\n              placeholder=\"Select Business Partner\"\r\n              formControlName=\"selectedbprt\"\r\n                [items]=\"bplist\"\r\n                itemValueField=\"id\"\r\n                itemTextField=\"_identifier\"\r\n                [canSearch]=\"true\"\r\n                (onChange)=\"portChange($event)\">\r\n              </ionic-selectable>\r\n            </ion-item>\r\n            </ion-col>\r\n            \r\n            \r\n        </ion-row>\r\n      </ion-card>\r\n      <ion-card>\r\n        <ion-card-header style=\"background: #FFCC80;\">\r\n          Compliance:\r\n        </ion-card-header>\r\n        <ion-row style=\"background-color: #b8b3b3; white-space:normal; font-size: xx-small; text-align: center;\"\r\n          class=\"row\">\r\n          <ion-col size=\"4\"> Compliance Type</ion-col>\r\n          <ion-col size=\"8\">Number</ion-col>\r\n        </ion-row>\r\n        <ion-row *ngFor=\"let item of compliancedata; index as i\" style=\"border: think solid black;font-size: small;\">\r\n          <ion-col size=\"4\" style=\"margin: auto;\">\r\n            <ion-label>\r\n              {{ item.scompType }}\r\n            </ion-label>\r\n          </ion-col>\r\n          <ion-col size=\"8\" *ngIf=\"item.Compliance_Type!='DL1VD' && item.Compliance_Type!='DL2VD'\">\r\n            <ion-item>\r\n              <ion-input type=\"text\" [(ngModel)]=\"item.num\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Enter\"\r\n                style=\"border: think solid black;font-size: small;\"></ion-input>\r\n            </ion-item>\r\n          </ion-col>\r\n          <ion-col size=\"8\" *ngIf=\"item.Compliance_Type==='DL1VD' || item.Compliance_Type==='DL2VD'\">\r\n            <!-- <ion-item style=\"border: think solid black;font-size: small;\">\r\n              <ion-datetime [min]=\"today\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"item.num\"\r\n                displayFormat=\"DD-MM-YYYY\" max=\"2050-12-31\" placeholder=\"Select Date\"></ion-datetime>\r\n            </ion-item> -->\r\n            <ion-item>\r\n              <ion-input placeholder=\"Select Date\" [value]=\"datefrom\"></ion-input>\r\n              <ion-button fill=\"clear\" id=\"open-date-input-1\">\r\n                <ion-icon icon=\"calendar\"></ion-icon>\r\n              </ion-button>\r\n              <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\r\n                <ng-template>\r\n                  <ion-datetime\r\n                    #popoverDatetime2\r\n                    [min]=\"today\" \r\n                    max=\"2050-12-31\"\r\n                    [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"item.num\"\r\n                    presentation=\"date\"\r\n                    showDefaultButtons=\"true\"\r\n                    (ionChange)=\"datefrom = formatDate(popoverDatetime2.value)\"\r\n                  ></ion-datetime>\r\n                </ng-template>\r\n              </ion-popover>\r\n            </ion-item>\r\n          </ion-col>\r\n          <ion-col *ngIf=\"item.Compliance_Type!='DL1VD' && item.Compliance_Type!='DL2VD'\">\r\n            <app-multi-file-upload [isOnlyCamera]=\"false\" [maxfile]=\"10\" [myform]=\"form\"\r\n              [controlID]=\"item.Compliance_Type\" formControlName=\"item.Compliance_Type\"></app-multi-file-upload>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col>\r\n            <ion-label style=\"color: red;\" class=\"ion-text-wrap\">{{validationSummary}}</ion-label>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-card>\r\n      \r\n    </ion-grid>\r\n  </form>\r\n  </ion-content>\r\n  <ion-footer>\r\n    <ion-row>\r\n        <ion-col  size=\"6\" size-lg=\"6\" *ngIf=\"!isNewRegistration\">\r\n          <ion-button style=\"width: 100%;\" (click)=\"onSaveDraft(form.value)\" [disabled]=\"!form.valid\">\r\n            <ion-label>Create Lead</ion-label>\r\n          </ion-button>\r\n        </ion-col>\r\n        <!-- <ion-col  size=\"6\" size-lg=\"6\">\r\n          <ion-button style=\"width: 100%;\" (click)=\"testMethod(form)\">\r\n            <ion-label>TEST Button</ion-label>\r\n          </ion-button>\r\n        </ion-col> -->\r\n        <ion-col size=\"6\" size-lg=\"6\">\r\n          <ion-button style=\"width: 100%;\" (click)=\"onConvertintoCustomer(form.value)\"\r\n            [disabled]=\"!form.valid || !form.get('selectedbpl').value\">\r\n            <ion-label>Submit for KYC Approval</ion-label>\r\n          </ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n  \r\n  </ion-footer>";

/***/ })

}]);
//# sourceMappingURL=src_app_newcustomer_newcustomer_module_ts.js.map