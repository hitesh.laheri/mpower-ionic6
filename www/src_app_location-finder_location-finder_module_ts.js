"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_location-finder_location-finder_module_ts"],{

/***/ 86618:
/*!***********************************************************!*\
  !*** ./src/app/location-finder/location-finder.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LocationFinderPageModule": () => (/* binding */ LocationFinderPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _location_finder_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./location-finder.page */ 24361);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ionic-selectable */ 25073);








const routes = [
    {
        path: '',
        component: _location_finder_page__WEBPACK_IMPORTED_MODULE_0__.LocationFinderPage
    }
];
let LocationFinderPageModule = class LocationFinderPageModule {
};
LocationFinderPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule,
            ionic_selectable__WEBPACK_IMPORTED_MODULE_6__.IonicSelectableModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterModule.forChild(routes)
        ],
        declarations: [_location_finder_page__WEBPACK_IMPORTED_MODULE_0__.LocationFinderPage]
    })
], LocationFinderPageModule);



/***/ }),

/***/ 24361:
/*!*********************************************************!*\
  !*** ./src/app/location-finder/location-finder.page.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LocationFinderPage": () => (/* binding */ LocationFinderPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _location_finder_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./location-finder.page.html?ngResource */ 46586);
/* harmony import */ var _location_finder_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./location-finder.page.scss?ngResource */ 66118);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../neworder/neworder.service */ 17216);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _awesome_cordova_plugins_geolocation_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @awesome-cordova-plugins/geolocation/ngx */ 36457);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _awesome_cordova_plugins_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @awesome-cordova-plugins/location-accuracy/ngx */ 35192);
/* harmony import */ var _awesome_cordova_plugins_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @awesome-cordova-plugins/native-geocoder/ngx */ 79683);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../provider/message-helper */ 98792);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _location_finder_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./location-finder.service */ 35087);














let LocationFinderPage = class LocationFinderPage {
    constructor(fb, neworderservice, geolocation, platform, nativeGeocoder, zone, loginauth, locationfinderservice, commonfun, msg, locationAccuracy) {
        this.fb = fb;
        this.neworderservice = neworderservice;
        this.geolocation = geolocation;
        this.platform = platform;
        this.nativeGeocoder = nativeGeocoder;
        this.zone = zone;
        this.loginauth = loginauth;
        this.locationfinderservice = locationfinderservice;
        this.commonfun = commonfun;
        this.msg = msg;
        this.locationAccuracy = locationAccuracy;
        // This variable will hold the class name.
        this.TAG = 'LocationFinderPage';
        // This variable will hold search text word count.
        this.reftextcount = 0;
        //
        this.BPaddressshipping = [];
        //
        this.showTxtAddress = false;
        //
        this.showBtnConvertAddressToLatLong = false;
        //
        this.showBtnCaptureLatLong = false;
        this.leastBusinessPartnerlist = null;
        this.validation_messages = {
            'selectedBusinessPartner': [{ type: 'required', message: ' *Please Select Customer.' }],
            'selectedBPaddressshipping': [{ type: 'required', message: '*Please Select Shipping Address.' }]
        };
        this.latLongForm = this.fb.group({
            selectedBusinessPartner: [, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required],
            selectedBPaddressshipping: [, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required]
        });
    }
    ngOnInit() {
        let mapOptions = {
            camera: {
                target: {
                    lat: 43.0741904,
                    lng: -89.3809802
                },
                zoom: 18,
                tilt: 30
            }
        };
        let latlong1 = {};
        latlong1;
        latlong1.lat = 17.6599;
        latlong1.lng = 75.9064;
        let latlong2 = {};
        latlong2.lat = 19.0760;
        latlong2.lng = 72.8777;
        //this.distance = Spherical.computeDistanceBetween(latlong1,latlong2);
        let km = this.distance / 1000;
        ////---------
        if (this.msg.isplatformweb == true) {
            // this.commonfun.chkcache('location-finder');
            setTimeout(() => {
                this.checkcust();
            }, 3000);
        }
        else {
            this.checkcust();
        }
        //-------
    }
    ionViewWillEnter() {
    }
    /**
     * @kind function
     * @summary This method will save data to server.
     * @since 1.0.0
     * @returns void
     * @public
     * @module Travel Expense
     * @author Pravin Bhosale
     * @classdesc  LocationFinderPage
     */
    saveForm(val) {
        let methodTAG = 'latlongSubmit';
        try {
            //localhost:8080/openbravo/ws/in.mbs.webservice.WMobileLatLongUpdate?addid=FFF202005200405006176B076E5C7E39&lat=100&long=2006
            this.locationfinderservice.LatLongUpdate(this.selectedAddressDropDown.addressid, this.txtLatitude, this.txtLongitude).subscribe(data => {
                var response = data;
                if (response.status == "Success") {
                    this.commonfun.presentAlert("Message", "Success", response.msg);
                    this.Resetpage();
                }
                else {
                    this.commonfun.presentAlert("Message", response.status, response.msg);
                }
            }, error => {
                //  console.log(this.TAG,methodTAG,error)
                this.commonfun.presentAlert("Message", "Error", error);
            });
        }
        catch (error) {
            console.log(this.TAG, methodTAG, error);
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    onClose(event) {
        event.component.searchText = "";
    }
    /**
     * @kind function
     * @summary This method provides information about the device's location, such as latitude and longitud.
     * @param null
     * @public
     * @returns void
     * @module Travel Expense
     * @since 0.0.7
     * @see https://github.com/apache/cordova-plugin-geolocation
     * @author Pravin Bhosale
     */
    captureLatLong() {
        let methodTAG = 'captureLatLong';
        try {
            //  console.log(methodTAG);	
            if (this.msg.isplatformweb == false) {
                this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
                    this.geolocation.getCurrentPosition().then((resp) => {
                        this.txtLatitude = resp.coords.latitude;
                        this.txtLongitude = resp.coords.longitude;
                        //  this.reverseGeocode(resp.coords.latitude,resp.coords.longitude);
                    }).catch((error) => {
                        //  console.log('Error getting location', error.message);	
                        // this.commonfun.presentAlert("Message","Error",error.message);
                    });
                }, error => console.log('Error requesting location permissions ' + JSON.stringify(error)));
            }
            else {
                //pwa
                this.geolocation.getCurrentPosition().then((resp) => {
                    this.txtLatitude = resp.coords.latitude;
                    this.txtLongitude = resp.coords.longitude;
                    //  this.reverseGeocode(resp.coords.latitude,resp.coords.longitude);
                }).catch((error) => {
                    //  console.log('Error getting location', error.message);	
                    // this.commonfun.presentAlert("Message","Error",error.message);
                });
            }
        }
        catch (error) {
            //  console.log(this.TAG,methodTAG,error);	
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    onAddChange() {
        try {
            if (!!this.selectedAddressDropDown.addressname) {
                this.txtselectedAddress = this.selectedAddressDropDown.addressname;
                this.showBtnConvertAddressToLatLong = false;
                this.showBtnCaptureLatLong = true;
            }
            else {
                this.showBtnCaptureLatLong = false;
                this.txtselectedAddress = null;
                this.showBtnConvertAddressToLatLong = false;
            }
        }
        catch (error) {
        }
    }
    convertAddressToLatLong() {
        let methodTAG = 'convertAddressToLatLong';
        try {
            this.selectedAddress = this.latLongForm.controls["selectedBPaddressshipping"].value;
            if (this.selectedAddress) {
                this.forwardGeocode(this.selectedAddress.addressname);
            }
        }
        catch (error) {
            // console.log(this.TAG,methodTAG,error);	
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    /**
     * @kind function
     * @description This method will fire when user will enter something on search box.
     * @public
     * @param $event
     * @returns void
     * @module Travel Expense
     * @since 1.0.0
     * @author Pravin Bhosale.
     */
    onCustDropDownSearch(event) {
        let methodTAG = 'onCustDropDownSearch';
        try {
            // console.log("onCustDropDownSearch");
            this.reftextcount++;
            let custsearchtext = event.text;
            if (custsearchtext.length % 3 == 0) {
                this.bindCustomerFromApi(custsearchtext);
                this.reftextcount = 0;
            }
        }
        catch (error) {
            //  console.log(this.TAG,methodTAG,error);	
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    /**
    * @kind function
    * @description This method will trigger when user will tap on list item in the drop down.
    * @public
    * @param $event
    * @returns void
    * @module Travel Expense
    * @since 1.0.0
    * @author Pravin Bhosale.
    */
    // public onCustDropDownChange(selectedBusinessPartner){
    onCustDropDownChange(event) {
        let methodTAG = 'onCustDropDownChange';
        try {
            //  console.log("onCustDropDownChange");
            // console.log("onCustDropDownChange:",event.value);
            this.getCustomerAddress(event.value);
        }
        catch (error) {
            //  console.log("Error:onCustDropDownChange",error);
        }
    }
    /**
     * @kind function
     * @description This method will fetch customer list from server and bind it local varibale.
     * @private
     * @param strsearch
     * @returns void
     * @module Travel Expense
     * @since 1.0.0.
     * @author Pravin Bhosale.
     */
    bindCustomerFromApi(strsearch) {
        let methodTAG = 'bindCustomerFromApi';
        try {
            if (strsearch != "") {
                this.locationfinderservice.getUserWiseCustomerForLatLong(strsearch).subscribe(data => {
                    var response = data;
                    this.BusinessPartnerlist = response;
                });
            }
            else {
                if (this.leastBusinessPartnerlist || this.leastBusinessPartnerlist != null) {
                    if (this.leastBusinessPartnerlist.length > this.loginauth.minlistcount) {
                        this.BusinessPartnerlist = null;
                    }
                    else {
                        this.BusinessPartnerlist = this.leastBusinessPartnerlist;
                    }
                }
                else {
                    this.BusinessPartnerlist = this.leastBusinessPartnerlist;
                }
            }
        }
        catch (error) {
            //  console.log(this.TAG,methodTAG,error);	
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    checkcust() {
        try {
            this.locationfinderservice.getUserWiseCustomerForLatLong("").subscribe(data => {
                var response = data;
                this.leastBusinessPartnerlist = response;
                if (this.leastBusinessPartnerlist) {
                    if (this.leastBusinessPartnerlist.length == 1) {
                        this.BusinessPartnerlist = this.leastBusinessPartnerlist;
                        this.latLongForm.controls["selectedBusinessPartner"].setValue(this.BusinessPartnerlist[0]);
                        //this.onCustDropDownChange(this.BusinessPartnerlist[0]);
                        this.getCustomerAddress(this.BusinessPartnerlist[0]);
                    }
                    else if (this.leastBusinessPartnerlist.length > this.loginauth.minlistcount) {
                        this.BusinessPartnerlist = null;
                        this.latLongForm.controls["selectedBusinessPartner"].setValue(null);
                    }
                    else {
                        this.BusinessPartnerlist = this.leastBusinessPartnerlist;
                        this.latLongForm.controls["selectedBusinessPartner"].setValue(null);
                    }
                }
            });
        }
        catch (error) {
            //  console.log("Error:chckcust",error);
        }
    }
    /**
    * @kind function
    * @description This method will get customer address from server.
    * @private
    * @param Id Customer Id
    * @returns void
    * @module Travel Expense
    * @since 1.0.0.
    * @author Pravin Bhosale.
    */
    getCustomerAddress(selectedBusinessPartner) {
        let methodTAG = 'getCustomerAddress';
        try {
            let jsondata = selectedBusinessPartner.AddressList;
            if (jsondata.length > 0) {
                this.BPaddressshipping = jsondata;
                this.selectedAddressDropDown = this.BPaddressshipping[0];
                this.showTxtAddress = true;
                // this.BPaddressshipping[0].name = null;
                if (!!this.BPaddressshipping[0].addressname) {
                    this.txtselectedAddress = this.BPaddressshipping[0].addressname;
                    this.showBtnConvertAddressToLatLong = false;
                    this.showBtnCaptureLatLong = true;
                }
                else {
                    this.showBtnCaptureLatLong = false;
                    this.txtselectedAddress = null;
                    this.showBtnConvertAddressToLatLong = false;
                }
            }
            else {
                this.showBtnCaptureLatLong = true;
                this.BPaddressshipping = null;
                this.selectedAddressDropDown = null;
                this.showTxtAddress = false;
                this.txtselectedAddress = null;
                this.showBtnConvertAddressToLatLong = false;
            }
        }
        catch (error) {
            //   console.log("Error:getCustomerAddress:",error)
        }
    }
    /**
   * @kind function
   * @description This method will convert your lat-long into the street address.
   * @param lat
   * @param lng
   * @returns void
   * @module  Travel Expense
   * @since 0.0.8
   * @author Pravin Bhosale.
   * @see https://ionicframework.com/docs/native/native-geocoder.
   */
    reverseGeocode(lat, lng) {
        let methodTAG = 'reverseGeocode';
        try {
            if (this.platform.is('cordova')) {
                let options = {
                    useLocale: true,
                    maxResults: 1
                };
                this.nativeGeocoder.reverseGeocode(lat, lng, options)
                    .then((result) => {
                    // this.userLocation = result[0]
                    // 
                })
                    .catch((error) => console.log(error));
            }
            else {
                //For PWA App
            }
        }
        catch (error) {
            console.log(this.TAG, methodTAG, error);
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    /**
     * @kind function
     * @description This method is converting addresses (like a street address) into geographic coordinates (like latitude and longitude).
     * @param address
     * @module  Travel Expense.
     * @since 0.0.8
     * @author Pravin Bhosale.
     * @see https://ionicframework.com/docs/native/native-geocoder.
     */
    forwardGeocode(address) {
        let methodTAG = 'forwardGeocode';
        try {
            //    if (this.platform.is('cordova')) {
            if (this.msg.isplatformweb == false) {
                let options = {
                    useLocale: true,
                    maxResults: 5
                };
                this.nativeGeocoder.forwardGeocode(address, options)
                    .then((result) => {
                    this.zone.run(() => {
                        this.txtLatitude = result[0].latitude;
                        this.txtLongitude = result[0].longitude;
                    });
                })
                    .catch((error) => {
                    console.log("cATCXH", error);
                    this.commonfun.presentAlert("Message", "Error", error);
                });
            }
            else {
                //For PWA App.
                //  console.log("else");
                //  this.captureLatLong()
            }
        }
        catch (error) {
            //  console.log(this.TAG,methodTAG,error);	
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    Resetpage() {
        try {
            this.latLongForm.reset();
            this.txtselectedAddress = null;
            this.txtLatitude = null;
            this.txtLongitude = null;
            this.showTxtAddress = false;
            this.showBtnConvertAddressToLatLong = false;
            this.showBtnCaptureLatLong = false;
            this.checkcust();
        }
        catch (error) {
            //  console.log("Error:Resetpage",error)
        }
    }
};
LocationFinderPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_10__.FormBuilder },
    { type: _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_2__.NeworderService },
    { type: _awesome_cordova_plugins_geolocation_ngx__WEBPACK_IMPORTED_MODULE_3__.Geolocation },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__.Platform },
    { type: _awesome_cordova_plugins_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_5__.NativeGeocoder },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_12__.NgZone },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_8__.LoginauthService },
    { type: _location_finder_service__WEBPACK_IMPORTED_MODULE_9__.LocationFinderService },
    { type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_6__.Commonfun },
    { type: _provider_message_helper__WEBPACK_IMPORTED_MODULE_7__.Message },
    { type: _awesome_cordova_plugins_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_4__.LocationAccuracy }
];
LocationFinderPage = (0,tslib__WEBPACK_IMPORTED_MODULE_13__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
        selector: 'app-location-finder',
        template: _location_finder_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_location_finder_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], LocationFinderPage);



/***/ }),

/***/ 35087:
/*!************************************************************!*\
  !*** ./src/app/location-finder/location-finder.service.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LocationFinderService": () => (/* binding */ LocationFinderService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);





let LocationFinderService = class LocationFinderService {
    constructor(http, loginauth, genericHTTP) {
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
    }
    getcustmerbillingaddress(leadid) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'org.openbravo.service.json.jsonrest/mmst_customer_billing?'
            + '_selectedProperties=id,name&'
            + '_where=active=true%20and%20Mmst_Customer_ID=\'' + leadid + '\'');
    }
    getUserWiseCustomerForLatLong(strsearch) {
        strsearch = strsearch.replace(/ /g, "%20");
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileUserWiseCustomerForLatLong?'
            + 'user_id=' + this.loginauth.userid
            + '&strsearch=' + strsearch
            + '&activity_id=' + this.loginauth.selectedactivity.id);
    }
    LatLongUpdate(addid, lat, long) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileLatLongUpdate?'
            + '&addid=' + addid
            + '&lat=' + long
            + '&long=' + lat);
    }
};
LocationFinderService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService }
];
LocationFinderService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], LocationFinderService);



/***/ }),

/***/ 66118:
/*!**********************************************************************!*\
  !*** ./src/app/location-finder/location-finder.page.scss?ngResource ***!
  \**********************************************************************/
/***/ ((module) => {

module.exports = ".btn-lat-long {\n  width: -webkit-fill-available !important;\n  background-color: vibrant !important;\n}\n\nion-select {\n  max-width: 100% !important;\n}\n\n/* Set the text color */\n\nion-select::part(text) {\n  color: #545ca7 !important;\n  font-size: 54px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvY2F0aW9uLWZpbmRlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRyx3Q0FBQTtFQUNBLG9DQUFBO0FBQ0g7O0FBR0E7RUFDRywwQkFBQTtBQUFIOztBQUdBLHVCQUFBOztBQUNBO0VBQ0cseUJBQUE7RUFDQSwwQkFBQTtBQUFIIiwiZmlsZSI6ImxvY2F0aW9uLWZpbmRlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnRuLWxhdC1sb25ne1xuICAgd2lkdGg6IC13ZWJraXQtZmlsbC1hdmFpbGFibGUgIWltcG9ydGFudDtcbiAgIGJhY2tncm91bmQtY29sb3I6IHZpYnJhbnQgIWltcG9ydGFudDtcbiAgLy8gLS1iYWNrZ3JvdW5kOiBibGFjayAhaW1wb3J0YW50O1xufVxuXG5pb24tc2VsZWN0e1xuICAgbWF4LXdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gICBcbn1cbi8qIFNldCB0aGUgdGV4dCBjb2xvciAqL1xuaW9uLXNlbGVjdDo6cGFydCh0ZXh0KSB7XG4gICBjb2xvcjogIzU0NWNhNyAhaW1wb3J0YW50O1xuICAgZm9udC1zaXplOiA1NHB4ICFpbXBvcnRhbnQ7XG4gfSJdfQ== */";

/***/ }),

/***/ 46586:
/*!**********************************************************************!*\
  !*** ./src/app/location-finder/location-finder.page.html?ngResource ***!
  \**********************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Lat-Long Finder</ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\"><ion-icon name=\"refresh\"></ion-icon> \n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n\n<ion-content>\n  <ion-grid>\n\n        <form [formGroup]=\"latLongForm\" (ngSubmit)=\"saveForm(latLongForm.value)\">\n          <ion-card>\n            <ion-row>\n              <ion-col>  \n            <ion-item>\n              <ion-label position=\"stacked\">Customer<span style=\"color:red!important\">*</span></ion-label>\n\n              <ionic-selectable placeholder=\"Select Customer\"\n                                formControlName=\"selectedBusinessPartner\"\n                                [items]=\"BusinessPartnerlist\" \n                                itemValueField=\"leadid\" \n                                itemTextField=\"_identifier\" \n                                [canSearch]=\"true\"\n                                (onClose)=\"onClose($event)\"\n                                (onSearch)=\"onCustDropDownSearch($event)\"\n                                \n                                (onChange)=\"onCustDropDownChange($event)\"\n                                > \n\n              </ionic-selectable>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.selectedBusinessPartner\">\n                <div *ngIf=\"latLongForm.get('selectedBusinessPartner').hasError(validation.type) && latLongForm.get('selectedBusinessPartner').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n            </ion-col>\n            </ion-row>\n<!-- (onChange)=\"onCustDropDownChange($event.component.value)\" -->\n           \n              <ion-row>\n                <ion-col>\n            <ion-item >\n              <ion-label position=\"stacked\">Shipping Address<span style=\"color:red!important\">*</span></ion-label>\n              <ion-select [disabled]=\"false\" \n              (ionChange)=\"onAddChange()\"\n              formControlName=\"selectedBPaddressshipping\" \n              [(ngModel)]=\"selectedAddressDropDown\" \n              multiple=\"false\" \n              interface=\"popover\" \n              placeholder=\"Select Address\">\n               <ion-select-option *ngFor=\"let sadd of BPaddressshipping\" [value]=\"sadd\">{{sadd.addressname}}</ion-select-option>                <!-- <ion-select-option>this is test data from testing purpose only we have to chnage this after testing is done.</ion-select-option> -->\n              </ion-select>\n              </ion-item>\n              <div padding-left>\n                <ng-container *ngFor=\"let validation of validation_messages.selectedBPaddressshipping\">\n                  <div *ngIf=\"latLongForm.get('selectedBPaddressshipping').hasError(validation.type) && latLongForm.get('selectedBPaddressshipping').touched\">\n                    <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                  </div>\n                </ng-container>\n              </div>\n              \n            </ion-col>\n          </ion-row>\n\n\n              <ion-item *ngIf=\"showTxtAddress\">\n                <ion-textarea>{{txtselectedAddress}}</ion-textarea>\n              </ion-item>\n\n              <ion-item>\n                <ion-text>\n                    {{txtLatitude}}\n                </ion-text>\n              </ion-item>\n\n              <ion-item>\n                <ion-text>\n                    {{txtLongitude}}\n                </ion-text>\n              </ion-item>\n              \n          </ion-card>  \n          <ion-item *ngIf=\"showBtnConvertAddressToLatLong\">\n            <ion-button (click)=\"convertAddressToLatLong()\" color=\"bluegrey\" size=\"large\" expand=\"block\" class=\"btn-lat-long\" >\n              Convert To Lat Long\n              <ion-icon slot=\"start\" name=\"compass\"></ion-icon>\n            </ion-button>\n          </ion-item>\n          <ion-item *ngIf=\"showBtnCaptureLatLong\">\n            <ion-button (click)=\"captureLatLong()\" color=\"bluegrey\" size=\"large\" expand=\"block\" class=\"btn-lat-long\" >\n              Capture Lat Long\n              <ion-icon slot=\"start\" name=\"compass\"></ion-icon>\n            </ion-button>\n          </ion-item>\n          <ion-item>\n            <ion-button (click)=\"saveForm(latLongForm.value)\" size=\"large\" expand=\"block\" class=\"btn-lat-long\" [disabled]=\"!latLongForm.valid || !txtLongitude || !txtLatitude\">\n               Save\n              <ion-icon slot=\"start\" name=\"compass\"></ion-icon>\n            </ion-button>\n\n           \n          </ion-item>\n        </form>\n  \n  </ion-grid>\n   \n\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_location-finder_location-finder_module_ts.js.map