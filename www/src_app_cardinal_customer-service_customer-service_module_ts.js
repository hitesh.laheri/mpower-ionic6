"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_cardinal_customer-service_customer-service_module_ts"],{

/***/ 39925:
/*!**********************************************************************!*\
  !*** ./src/app/cardinal/customer-service/customer-service.module.ts ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CustomerServicePageModule": () => (/* binding */ CustomerServicePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _customer_service_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./customer-service.page */ 32532);
/* harmony import */ var src_app_material_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/material.module */ 63806);
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/datepicker */ 42298);
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/core */ 59121);










const routes = [
    {
        path: '',
        component: _customer_service_page__WEBPACK_IMPORTED_MODULE_0__.CustomerServicePage
    }
];
let CustomerServicePageModule = class CustomerServicePageModule {
};
CustomerServicePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            src_app_material_module__WEBPACK_IMPORTED_MODULE_1__.MaterialModule,
            _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_7__.MatDatepickerModule,
            _angular_material_core__WEBPACK_IMPORTED_MODULE_8__.MatNativeDateModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule.forChild(routes)
        ],
        declarations: [_customer_service_page__WEBPACK_IMPORTED_MODULE_0__.CustomerServicePage]
    })
], CustomerServicePageModule);



/***/ }),

/***/ 32532:
/*!********************************************************************!*\
  !*** ./src/app/cardinal/customer-service/customer-service.page.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CustomerServicePage": () => (/* binding */ CustomerServicePage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _customer_service_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./customer-service.page.html?ngResource */ 27559);
/* harmony import */ var _customer_service_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./customer-service.page.scss?ngResource */ 93706);
/* harmony import */ var _assets_model_complain__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../assets/model/complain */ 6337);
/* harmony import */ var _customer_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./customer-service.service */ 59813);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _provider_validator_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../provider/validator-helper */ 35096);
/* harmony import */ var src_app_newcustomer_newcustomer_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/newcustomer/newcustomer.service */ 85189);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! date-fns */ 86527);














let CustomerServicePage = class CustomerServicePage {
  constructor(_formBuilder, val, customerService, newcustomerservice, commonFunction, alertCtrl, router) {
    this._formBuilder = _formBuilder;
    this.val = val;
    this.customerService = customerService;
    this.newcustomerservice = newcustomerservice;
    this.commonFunction = commonFunction;
    this.alertCtrl = alertCtrl;
    this.router = router;
    /**
     * This variable will hold the page name for logging purpose.
     */

    this.TAG = "CustomerServicePage";
    /**
     * If this variable is set to true then stepper will check the validation on form control.It will now allow to navigate next page or stepper.
     */

    this.isLinear = true;
    /**
     *
     */

    this.isProductCompliant = false;
    /**
     *
     */

    this.isEquipment = false;
    this.isConsumables = false;
    this.isOptional = false;
    this.dateevent = '';
    this.dateinvoice = '';
    this.dateinstall = '';
    this.now = new Date();
    this.year = this.now.getFullYear();
    this.month = this.now.getMonth() + 1;
    this.day = this.now.getDate();
    /**
     *
     */

    this.maxDate = this.year + "-" + this.month + "-" + this.day;
    this.today = this.year + "-" + ("0" + this.month).slice(-2) + "-" + ("0" + this.day).slice(-2);
    /**
     *
     */

    this.firstStepValid = false;
    this.secondStepValid = false;
    this.thirdStepValid = false;
    this.fourthStepValid = false;
    this.consumableStepValid = false;
    /**
     *
     */

    this.validation_messages = {
      'mobileno': [{
        type: 'required',
        message: '*Please Enter Contact No.'
      }, {
        type: 'InvalidNumber',
        message: '*Please Enter Valid Contact No.'
      }],
      'email': [{
        type: 'required',
        message: '*Please Enter Email.'
      }, {
        type: 'InvalidEmail',
        message: '*Please Enter Valid Email.'
      }],
      'designation': [{
        type: 'required',
        message: '*Please Designation.'
      }],
      'name': [{
        type: 'required',
        message: '*Please Enter Name.'
      }],
      'document_type': [{
        type: 'required',
        message: '*Please Select Document Type.'
      }],
      'eventdate': [{
        type: 'required',
        message: '*Please Select Event Date.'
      }],
      'serialNo': [{
        type: 'required',
        message: '*Please Enter Serial no .'
      }],
      'contractType': [{
        type: 'required',
        message: '*Please Enter Contract Type .'
      }],
      'SKUCode': [{
        type: 'required',
        message: '*Please Enter SKU Code'
      }],
      'brandName': [{
        type: 'required',
        message: '*Please Enter Brand Name'
      }],
      'customerName': [{
        type: 'required',
        message: '*Please Enter Customer Name'
      }],
      'customerAddress1': [{
        type: 'required',
        message: '*Please Enter Customer Address1'
      }],
      'customerAddress2': [{
        type: 'required',
        message: '*Please Enter Customer Customer Address 2'
      }],
      'customerAddress3': [{
        type: 'required',
        message: '*Please Enter Customer Customer Address 3'
      }],
      'pinCode': [{
        type: 'required',
        message: '*Please Enter Pin Code'
      }],
      'area': [{
        type: 'required',
        message: '*Please Select Your Area'
      }],
      'city': [{
        type: 'required',
        message: '*Please Enter City'
      }],
      'state': [{
        type: 'required',
        message: '*Please Enter State'
      }],
      'country': [{
        type: 'required',
        message: '*Please Enter Country'
      }],
      'documentType': [{
        type: 'required',
        message: '*Please Select Document Type.'
      }],
      'complaintDescriptionCtrl': [{
        type: 'required',
        message: '*Please Enter Complaint Description.'
      }],
      'errorCodeCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Error Code.'
      }]
    };
    this.test = true;
  }

  ngOnInit() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.firstFormGroup = _this._formBuilder.group({
        complaintDateCtrl: [,],
        documentTypeCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        mobileno: [, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required, _this.val.numberValid],
        email: [, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required, _this.val.emailValid],
        designationOfComplainerCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        btnDetail: [,],
        eventDateCtrl: [],
        complaintDescriptionCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required]
      });
      _this.equipmentFormGroup = _this._formBuilder.group({
        serialNo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        contractType: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        SrNoEquipmentCtrl: [,],
        invoiceNoCtrl: [,],
        invoiceDateCtrl: [,],
        dealerNameCtrl: [,],
        installationDateCtrl: [,],
        errorCode: [, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required]
      });
      _this.thirdFormGroup = _this._formBuilder.group({
        SKUCode: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        brandName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        skuName: [,]
      });
      _this.fourthFormGroup = _this._formBuilder.group({
        customerName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        customerAddress1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        customerAddress2: [,],
        customerAddress3: [,],
        pinCodeCtrl: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        area: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        city: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        state: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        country: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required]
      });
      _this.consumablesFormGroup = _this._formBuilder.group({
        lotNoConsumablesCtrl: [,]
      });
      _this.productCompliantFormGroup = _this._formBuilder.group({
        lotNoProductCompliantCtrl: []
      });
      let temp = yield (yield _this.customerService.getDocumentList()).toPromise(); // console.log(this.TAG,"API DATA TEST",temp);

      _this.errorCodeList = yield (yield _this.customerService.getErrorCodeList()).toPromise(); //  console.log(this.TAG,"Error Code List From Server",this.errorCodeList);

      _this.designationOfComplainerList = yield (yield _this.customerService.getDesignationOfComplainerList()).toPromise();
      _this.docTypeList = temp.response.data;
      _this.contractTypeList = yield (yield _this.customerService.getContractTypeList()).toPromise(); //console.log(this.TAG,'Contract Type List From Server',this.contractTypeList);
      //console.log(this.TAG,'Document Type From Server',this.docTypeList);
      //   console.log(this.TAG,"Today Date",this.maxDate);
      // this.firstFormGroup.controls.complaintDateCtrl.patchValue(this.day + "-" + this.month + "-" +this.year);

      _this.firstFormGroup.controls['eventDateCtrl'].setValue(_this.today);

      _this.fourthFormGroup.get('country').disable();

      _this.fourthFormGroup.get('state').disable();

      _this.fourthFormGroup.get('city').disable();
    })();
  }

  ionViewWillEnter() {
    this.stepper.selectedIndex = 0;
    this.firstFormGroup.get('complaintDateCtrl').disable();
    this.firstFormGroup.controls.complaintDateCtrl.patchValue(this.day + "-" + this.month + "-" + this.year);
  }

  formatDate(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_9__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_10__["default"])(value), 'dd-MM-yyyy');
  }

  formatDate1(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_9__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_10__["default"])(value), 'dd-MM-yyyy');
  }

  formatDate2(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_9__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_10__["default"])(value), 'dd-MM-yyyy');
  }

  docTypeSelectedChange(docTypeSelected) {
    try {
      // console.log("Document Type Selected", docTypeSelected);
      if (docTypeSelected == "Product Compliant") {
        this.isProductCompliant = true;
        this.isEquipment = false;
        this.isConsumables = false;
        this.thirdFormGroup.enable();
      } else if (docTypeSelected == "Equipment") {
        this.isEquipment = true;
        this.isProductCompliant = false;
        this.isConsumables = false;
      } else if (docTypeSelected == "Consumables") {
        this.isConsumables = true;
        this.isProductCompliant = false;
        this.isEquipment = false;
        this.thirdFormGroup.enable();
      }
    } catch (error) {//  console.log(this.TAG, error);
    }
  }

  errorCodeSelectedSelectedChange(errorCodeSelected) {
    try {//console.log(this.TAG,errorCodeSelected);
    } catch (error) {//  console.error(this.TAG,error);
    }
  }

  ProductReturnedSelectedChange(data) {
    try {} catch (error) {//  console.error(this.TAG,error);
    }
  }

  contractTypeSelectedChange(data) {
    try {} catch (error) {//  console.error(this.TAG,error);
    }
  }

  designationOfComplainerChange(data) {
    try {} catch (error) {//  console.error(this.TAG,error);
    }
  }

  onAreaSelectedChange() {
    try {
      this.selectedArea = this.fourthFormGroup.controls['area'].value; //    console.log(this.TAG,'Pravin Area Is',this.selectedArea);

      if (this.selectedArea != null) this.city = this.selectedArea.cttv$_identifier;
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  RefreshPage() {
    try {
      //this.firstFormGroup.controls.complaintDateCtrl.patchValue(this.maxDate);
      this.firstFormGroup.reset();
      this.firstStepValid = false;
      this.equipmentFormGroup.reset();
      this.secondStepValid = false;
      this.thirdFormGroup.reset();
      this.thirdStepValid = false;
      this.fourthFormGroup.reset();
      this.fourthStepValid = false;
      this.consumablesFormGroup.reset();
      this.productCompliantFormGroup.reset();
      this.ionViewWillEnter();
    } catch (error) {//   console.log(this.TAG, error);
    }
  }

  matSettper(event, form) {
    try {
      //   console.log(this.TAG, "matSettper Click event", event,form);
      this.firstStepValid = true;
    } catch (error) {//  console.error(this.TAG, error);
    }
  }

  matSettperSecond(evnet) {
    try {
      this.secondStepValid = true;
    } catch (error) {//  console.error(this.TAG, error);
    }
  }

  matSettperThird(evnet) {
    try {
      this.thirdStepValid = true;
    } catch (error) {
      console.error(this.TAG, error);
    }
  }

  matSettperProductCom(event) {
    try {} catch (error) {//   console.error(this.TAG,error);
    }
  }

  matSettperConsumables(event) {
    try {} catch (error) {// console.log(this.TAG,error);
    }
  }

  onCheckInstallationBase() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this2.commonFunction.loadingPresent();

        _this2.serialNoCheckResponse = "";
        let serialNo = _this2.equipmentFormGroup.get('serialNo').value ? _this2.equipmentFormGroup.get('serialNo').value : '';
        _this2.serialNoCheckResponse = yield (yield _this2.customerService.checkSerialNumber(serialNo, "2020-12-31")).toPromise(); //  console.log(this.TAG,"Installation Base Data",this.serialNoCheckResponse);

        if (Object.keys(_this2.serialNoCheckResponse).length != 0) {
          if (!!_this2.serialNoCheckResponse.contracttype) {
            for (let i = 0; i < _this2.contractTypeList.length; i++) {
              if (_this2.contractTypeList[i].code == _this2.serialNoCheckResponse.contracttype) {
                _this2.contractTypeSelected = _this2.contractTypeList[i];
              }
            }

            _this2.equipmentFormGroup.get('contractType').disable();
          }

          if (!!_this2.serialNoCheckResponse.invoiceno) {
            _this2.equipmentFormGroup.controls["invoiceNoCtrl"].setValue(_this2.serialNoCheckResponse.invoiceno);

            _this2.equipmentFormGroup.get('invoiceNoCtrl').disable();
          } else {
            _this2.equipmentFormGroup.controls["invoiceNoCtrl"].setValue("");
          }

          if (!!_this2.serialNoCheckResponse.invoicedate) {
            _this2.serialNoCheckResponse.invoicedate = _this2.serialNoCheckResponse.invoicedate.split(' ')[0];

            _this2.equipmentFormGroup.controls["invoiceDateCtrl"].setValue(_this2.serialNoCheckResponse.invoicedate);

            _this2.equipmentFormGroup.get('invoiceDateCtrl').disable();
          } else {
            _this2.equipmentFormGroup.controls["invoiceDateCtrl"].setValue("");
          }

          if (!!_this2.serialNoCheckResponse.dealername) {
            _this2.equipmentFormGroup.controls["dealerNameCtrl"].setValue(_this2.serialNoCheckResponse.dealername);

            _this2.equipmentFormGroup.get('dealerNameCtrl').disable();
          } else {
            _this2.equipmentFormGroup.controls["dealerNameCtrl"].setValue("");
          }

          if (!!_this2.serialNoCheckResponse.installationdate) {
            _this2.serialNoCheckResponse.installationdate = _this2.serialNoCheckResponse.installationdate.split(' ')[0];

            _this2.equipmentFormGroup.controls["installationDateCtrl"].setValue(_this2.serialNoCheckResponse.installationdate);

            _this2.equipmentFormGroup.get('installationDateCtrl').disable();
          } else {
            _this2.equipmentFormGroup.controls["installationDateCtrl"].setValue("");
          }

          if (!!_this2.serialNoCheckResponse.sku) {
            _this2.thirdFormGroup.controls["SKUCode"].setValue(_this2.serialNoCheckResponse.sku);

            _this2.thirdFormGroup.get('SKUCode').disable();
          } else {
            _this2.thirdFormGroup.controls["SKUCode"].setValue("");
          }

          if (!!_this2.serialNoCheckResponse.sku) {
            _this2.thirdFormGroup.controls["skuName"].setValue(_this2.serialNoCheckResponse.skuname);

            _this2.thirdFormGroup.get('skuName').disable();
          } else {
            _this2.thirdFormGroup.controls["skuName"].setValue("");
          }

          if (!!_this2.serialNoCheckResponse.brand) {
            _this2.thirdFormGroup.controls["brandName"].setValue(_this2.serialNoCheckResponse.brand);

            _this2.thirdFormGroup.get('brandName').disable();
          } else {
            _this2.thirdFormGroup.controls["brandName"].setValue("");
          } // if(!!this.serialNoCheckResponse.custname){
          //   this.fourthFormGroup.controls["customerName"].setValue(this.serialNoCheckResponse.custname);
          //   this.fourthFormGroup.get('customerName').disable();
          // }
          // if(!!this.serialNoCheckResponse.add1){
          //   this.fourthFormGroup.controls["customerAddress1"].setValue(this.serialNoCheckResponse.add1);
          //   this.fourthFormGroup.get('customerAddress1').disable();
          // }
          // if(!!this.serialNoCheckResponse.add2){
          //   this.fourthFormGroup.controls["customerAddress2"].setValue(this.serialNoCheckResponse.add2);
          //   this.fourthFormGroup.get('customerAddress2').disable();
          // }
          // if(!!this.serialNoCheckResponse.add3){
          //   this.fourthFormGroup.controls["customerAddress3"].setValue(this.serialNoCheckResponse.add3);
          //   this.fourthFormGroup.get('customerAddress3').disable();
          // }
          // if(Object.keys(this.serialNoCheckResponse.pincode).length != 0){
          //   this.fourthFormGroup.controls["pinCodeCtrl"].setValue(this.serialNoCheckResponse.pincode);
          //   if(Object.keys(this.serialNoCheckResponse.area).length != 0){
          //     this.fourthFormGroup.get('pinCodeCtrl').disable();
          //   }
          // }
          // if(Object.keys(this.serialNoCheckResponse.area).length != 0){
          //  let areaDummyObject = [{"id":this.serialNoCheckResponse.area[0].id,"mmst_cttv_id":this.serialNoCheckResponse.area[0].id, "cttv":this.serialNoCheckResponse.area[0].id, "_identifier":this.serialNoCheckResponse.area[0].id, "area":this.serialNoCheckResponse.area[0].name, "cttv$_identifier":this.serialNoCheckResponse.area[0].id}]
          //  this.areaList = areaDummyObject;
          //  this.areaSelected = areaDummyObject[0];
          //  this.fourthFormGroup.get('area').disable();
          // }
          // if(Object.keys(this.serialNoCheckResponse.city).length != 0){
          //   this.fourthFormGroup.controls["city"].setValue(this.serialNoCheckResponse.city[0].name);
          //  // this.fourthFormGroup.get('city').disable();
          // }
          // if(Object.keys(this.serialNoCheckResponse.state).length != 0){
          //   this.fourthFormGroup.controls["state"].setValue(this.serialNoCheckResponse.state[0].name);
          //  // this.fourthFormGroup.get('state').disable();
          // }
          // if(Object.keys(this.serialNoCheckResponse.country).length != 0){
          //   this.fourthFormGroup.controls["country"].setValue(this.serialNoCheckResponse.country[0].name);
          //   //this.fourthFormGroup.get('country').disable();
          // }

        } else {
          _this2.equipmentFormGroup.reset();

          _this2.equipmentFormGroup.enable();

          _this2.equipmentFormGroup.controls["serialNo"].setValue(serialNo);

          _this2.thirdFormGroup.reset();

          _this2.thirdFormGroup.enable();

          _this2.fourthFormGroup.reset();

          _this2.fourthFormGroup.enable();
        }

        _this2.commonFunction.loadingDismiss();
      } catch (error) {// console.log(this.TAG,error);
      }
    })();
  }

  onChangePinCode(id = '') {
    try {
      this.newcustomerservice.getPincode(this.fourthFormGroup.controls["pinCodeCtrl"].value).subscribe(data => {
        const response = data['response']; //   console.log(this.TAG,"Response From Pin Code",response);

        this.pinCodeList = response.data;

        if (this.pinCodeList.length > 0) {
          this.inValidPinCode = '';
          this.newcustomerservice.getarea(this.pinCodeList[0].id).subscribe(data => {
            const response = data['response'];
            console.log(this.TAG, "AREA LIST", response.data);
            this.areaList = response.data;
            this.state = this.pinCodeList[0].region$_identifier;
            this.country = this.pinCodeList[0].country$_identifier;
            this.district = this.pinCodeList[0].district$_identifier;

            if (id != '' || id == undefined) {
              this.selectedArea = this.areaList.find(item => item.id === id);
              setTimeout(() => {
                this.fourthFormGroup.controls["area"].setValue(this.selectedArea);
              }, 1500);
            }
          });
        }
      });
    } catch (error) {
      console.error(this.TAG, error);
    }
  }

  checkSerialNo() {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      let serialNumberTemp = _this3.equipmentFormGroup.get('serialNo').value ? _this3.equipmentFormGroup.get('serialNo').value : '';

      if (!!serialNumberTemp) {
        let equipmentData = yield _this3.customerService.getSerialNumberFromBase(serialNumberTemp).toPromise();

        if (equipmentData) {
          _this3.equipmentFormGroup.controls.SrNoEquipmentCtrl.setValue(equipmentData.srnoequipment);

          _this3.equipmentFormGroup.get('SrNoEquipmentCtrl').disable();

          _this3.equipmentFormGroup.controls.invoiceNoCtrl.setValue(equipmentData.invoiceno);

          _this3.equipmentFormGroup.controls.invoiceDateCtrl.setValue(equipmentData.invoicedate);

          _this3.equipmentFormGroup.controls.dealerNameCtrl.setValue(equipmentData.dealername);

          _this3.equipmentFormGroup.controls.installationDateCtrl.setValue(equipmentData.installationdate);

          _this3.thirdFormGroup.controls.SKUCode.setValue(equipmentData.skucode);

          _this3.thirdFormGroup.controls.skuName.setValue(equipmentData.skuname);

          _this3.thirdFormGroup.controls.brandName.setValue(equipmentData.brandname);
        }
      }
    })();
  }

  presentAlert(Header, SubHeader, Message) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this4.alertCtrl.create({
        header: Header,
        subHeader: SubHeader,
        message: Message,
        buttons: [{
          text: "OK",
          handler: ok => {
            _this4.firstFormGroup.reset('', {
              emitEvent: false
            });

            _this4.firstStepValid = false;

            _this4.equipmentFormGroup.reset('', {
              emitEvent: false
            });

            _this4.secondStepValid = false;

            _this4.thirdFormGroup.reset('', {
              emitEvent: false
            });

            _this4.ProductReturnedSelected = '';
            _this4.thirdStepValid = false;

            _this4.fourthFormGroup.reset('', {
              emitEvent: false
            });

            _this4.fourthStepValid = false;

            _this4.consumablesFormGroup.reset('', {
              emitEvent: false
            });

            _this4.productCompliantFormGroup.reset();

            _this4.firstFormGroup.markAsPristine();

            _this4.firstFormGroup.markAsUntouched();

            _this4.router.navigateByUrl('/home');
          }
        }]
      });
      alert.dismiss(() => console.log('The alert has been closed.'));
      yield alert.present();
    })();
  }

  testForm(form) {// console.log("Form Test",form);
  }

  submit() {
    var _this5 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this5.commonFunction.loadingPresent();

        _this5.fourthStepValid = true;
        let complain = new _assets_model_complain__WEBPACK_IMPORTED_MODULE_3__.Complain();
        complain.doctype = _this5.docTypeSelected.id; //  console.log("1");    

        complain.nameofcomplainer = _this5.firstFormGroup.get('name').value ? _this5.firstFormGroup.get('name').value : '';
        complain.desofcomplnr = _this5.firstFormGroup.get('designationOfComplainerCtrl').value.id ? _this5.firstFormGroup.get('designationOfComplainerCtrl').value.id : ''; //  console.log("2");    

        complain.contnumber = _this5.firstFormGroup.get('mobileno').value ? _this5.firstFormGroup.get('mobileno').value.toString() : '';
        complain.email = _this5.firstFormGroup.get('email').value ? _this5.firstFormGroup.get('email').value : '';
        complain.eventdate = _this5.firstFormGroup.get('eventDateCtrl').value ? _this5.firstFormGroup.get('eventDateCtrl').value : '';
        complain.description = _this5.firstFormGroup.get('complaintDescriptionCtrl').value ? _this5.firstFormGroup.get('complaintDescriptionCtrl').value : '';

        if (_this5.docTypeSelected.name == 'Consumables') {
          complain.lotno = _this5.consumablesFormGroup.get('lotNoConsumablesCtrl').value ? _this5.consumablesFormGroup.get('lotNoConsumablesCtrl').value : '';
        } else if (_this5.docTypeSelected.name == 'Product Compliant') {
          complain.lotno = _this5.productCompliantFormGroup.get('lotNoProductCompliantCtrl').value ? _this5.productCompliantFormGroup.get('lotNoProductCompliantCtrl').value : '';
        } else if (_this5.docTypeSelected.name == 'Equipment') {
          complain.serialno = _this5.equipmentFormGroup.get('serialNo').value ? _this5.equipmentFormGroup.get('serialNo').value : '';
          complain.srnoequipment = _this5.equipmentFormGroup.get('SrNoEquipmentCtrl').value ? _this5.equipmentFormGroup.get('SrNoEquipmentCtrl').value : '';
          complain.contracttype = _this5.equipmentFormGroup.get('contractType').value ? _this5.equipmentFormGroup.get('contractType').value.code : '';
          complain.invoiceno = _this5.equipmentFormGroup.get('invoiceNoCtrl').value ? _this5.equipmentFormGroup.get('invoiceNoCtrl').value : '';
          complain.invoicedate = _this5.equipmentFormGroup.get('invoiceDateCtrl').value ? _this5.equipmentFormGroup.get('invoiceDateCtrl').value : ''; // console.log("3");    

          complain.errorcode = _this5.errorCodeSelected.id ? _this5.errorCodeSelected.id : '';
          complain.dealername = _this5.equipmentFormGroup.get('dealerNameCtrl').value ? _this5.equipmentFormGroup.get('dealerNameCtrl').value : '';
          complain.installationdate = _this5.equipmentFormGroup.get('installationDateCtrl').value ? _this5.equipmentFormGroup.get('installationDateCtrl').value : '';
        }

        complain.skucode = _this5.thirdFormGroup.get('SKUCode').value ? _this5.thirdFormGroup.get('SKUCode').value : '';
        complain.skuname = _this5.thirdFormGroup.get('skuName').value ? _this5.thirdFormGroup.get('skuName').value : '';
        complain.brandname = _this5.thirdFormGroup.get('brandName').value ? _this5.thirdFormGroup.get('brandName').value : '';
        complain.producttobereturn = _this5.ProductReturnedSelected ? _this5.ProductReturnedSelected : '';
        complain.custname = _this5.fourthFormGroup.get('customerName').value ? _this5.fourthFormGroup.get('customerName').value : '';
        complain.add1 = _this5.fourthFormGroup.get('customerAddress1').value ? _this5.fourthFormGroup.get('customerAddress1').value : '';
        complain.add2 = _this5.fourthFormGroup.get('customerAddress2').value ? _this5.fourthFormGroup.get('customerAddress2').value : '';
        complain.add3 = _this5.fourthFormGroup.get('customerAddress3').value ? _this5.fourthFormGroup.get('customerAddress3').value : '';

        if (_this5.docTypeSelected.name == 'Equipment') {
          if (!!_this5.serialNoCheckResponse.pincode && Object.keys(_this5.serialNoCheckResponse.pincode).length != 0) {
            complain.pincode = _this5.serialNoCheckResponse.pincode;
          } else {
            complain.pincode = _this5.pinCodeList[0].spincode;
          }

          if (!!_this5.serialNoCheckResponse.area && Object.keys(_this5.serialNoCheckResponse.area).length != 0) {
            complain.area = _this5.serialNoCheckResponse.area[0].id;
            complain.city = _this5.serialNoCheckResponse.city[0].id;
            complain.state = _this5.serialNoCheckResponse.state[0].id;
            complain.country = _this5.serialNoCheckResponse.country[0].id;
          } else {
            let area = _this5.fourthFormGroup.get('area').value ? _this5.fourthFormGroup.get('area').value : '';
            complain.area = area.id;
            complain.city = _this5.selectedArea.cttv;
            complain.state = _this5.pinCodeList[0].region;
            complain.country = _this5.pinCodeList[0].country;
          }
        } else {
          complain.pincode = _this5.pinCodeList[0].spincode;
          let area = _this5.fourthFormGroup.get('area').value ? _this5.fourthFormGroup.get('area').value : '';
          complain.area = area.id;
          complain.city = _this5.selectedArea.cttv;
          complain.state = _this5.pinCodeList[0].region;
          complain.country = _this5.pinCodeList[0].country;
        } // console.log("4");    


        complain.assigntoservmg = "true"; //complain.appcomplaint = "true";
        //  console.log(this.TAG,"Final Customer Service Form",complain);

        let saveComplainResponse = yield _this5.customerService.saveComplain(complain).toPromise(); //   console.log(this.TAG,"Response From Save Complain",saveComplainResponse);

        if (saveComplainResponse) {
          _this5.presentAlert("Customer Service", "", saveComplainResponse.msg);
        } else {
          _this5.presentAlert("Customer Service", "", "Something went wrong please try again" + saveComplainResponse.msg);
        }

        _this5.commonFunction.loadingDismiss();
      } catch (error) {
        _this5.commonFunction.loadingDismiss();

        if (error.status == "200") {
          _this5.presentAlert("Customer Service", "", error.error.text);
        } else {
          console.error(_this5.TAG, error);

          _this5.commonFunction.presentAlert("Customer Service", error.status, "Something went wrong please try again");
        }
      }
    })();
  }

};

CustomerServicePage.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_8__.FormBuilder
}, {
  type: _provider_validator_helper__WEBPACK_IMPORTED_MODULE_5__.Validator
}, {
  type: _customer_service_service__WEBPACK_IMPORTED_MODULE_4__.CustomerServiceService
}, {
  type: src_app_newcustomer_newcustomer_service__WEBPACK_IMPORTED_MODULE_6__.NewcustomerService
}, {
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_7__.Commonfun
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__.AlertController
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_12__.Router
}];

CustomerServicePage.propDecorators = {
  stepper: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_13__.ViewChild,
    args: ["stepper", {
      static: false
    }]
  }]
};
CustomerServicePage = (0,tslib__WEBPACK_IMPORTED_MODULE_14__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_13__.Component)({
  selector: 'app-customer-service',
  template: _customer_service_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_customer_service_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], CustomerServicePage);


/***/ }),

/***/ 59813:
/*!***********************************************************************!*\
  !*** ./src/app/cardinal/customer-service/customer-service.service.ts ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CustomerServiceService": () => (/* binding */ CustomerServiceService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../login/loginauth.service */ 44010);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../common/generic-http-client.service */ 28475);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_common_Constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/common/Constants */ 68209);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ 58987);







let CustomerServiceService = class CustomerServiceService {
    constructor(genericHttpClientService, loginService, commonFunction, loginAuthService, httpClient) {
        this.genericHttpClientService = genericHttpClientService;
        this.loginService = loginService;
        this.commonFunction = commonFunction;
        this.loginAuthService = loginAuthService;
        this.httpClient = httpClient;
        /**
         *
         */
        this.TAG = "CustomerServiceService";
    }
    getDocumentList() {
        try {
            let URL = this.loginService.commonurl + 'org.openbravo.service.json.jsonrest/DocumentType?' + this.loginService.ReadOnlyparameter + '&' + '_where=EM_Mdts_Docnature=\'SRV\'';
            //console.log(this.TAG,"Doc Type Service",URL);
            return this.genericHttpClientService.get(URL);
        }
        catch (error) {
            console.error(this.TAG, error);
        }
    }
    getErrorCodeList() {
        try {
            let errorCodeURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_2__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.ListOfvalues?' +
                this.loginAuthService.parameter + '&user_id=' + this.loginAuthService.userid +
                '&type=EC';
            return this.genericHttpClientService.get(errorCodeURL);
        }
        catch (error) {
            console.error(this.TAG, error);
        }
    }
    getSerialNumberFromBase(serialNumberTemp) {
        try {
            let serialNumerURL;
            return this.genericHttpClientService.get(serialNumerURL);
        }
        catch (error) {
            console.error(this.TAG, error);
        }
    }
    getDesignationOfComplainerList() {
        try {
            //return this.httpClient.get('assets/data/designation.json');
            let designationOfComplainerListURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_2__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.ListOfvalues?' +
                this.loginAuthService.parameter + '&user_id=' + this.loginAuthService.userid +
                '&type=DC';
            return this.genericHttpClientService.get(designationOfComplainerListURL);
        }
        catch (error) {
            console.error(this.TAG, error);
        }
    }
    getContractTypeList() {
        try {
            let contractTypeListURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_2__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.ListOfvaluesReference?' +
                this.loginAuthService.parameter + '&user_id=' + this.loginAuthService.userid +
                '&refname=MSNR%20Contract%20Type';
            return this.genericHttpClientService.get(contractTypeListURL);
        }
        catch (error) {
            console.error(this.TAG, error);
        }
    }
    checkSerialNumber(serialNo, date) {
        try {
            let serialNumberCheckURL = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_2__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerServiceInstallationBase?' +
                this.loginAuthService.parameter + '&user_id=' + this.loginAuthService.userid +
                '&serialno=' + serialNo + '&activity=' + this.loginService.selectedactivity.id;
            //  console.log("Insta base Url",serialNumberCheckURL);
            return this.genericHttpClientService.get(serialNumberCheckURL);
        }
        catch (error) {
            // console.log(this.TAG,error);
        }
    }
    saveComplain(complain) {
        try {
            let login = this.loginAuthService.user;
            let password = this.loginAuthService.pass;
            const auth = btoa(login + ":" + password);
            const httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders({
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Basic ' + auth
                })
            };
            let data = {
                'complaintno': complain.complaint_no,
                'complaintid': complain.complaintid ? complain.complaintid : '',
                'doctype': complain.doctype,
                'nameofcomplainer': complain.nameofcomplainer,
                'desofcomplnr': complain.desofcomplnr,
                'contnumber': complain.contnumber,
                'email': complain.email,
                'eventdate': complain.eventdate,
                'serialno': complain.serialno,
                "srnoequipment": complain.srnoequipment,
                "contracttype": complain.contracttype,
                'invoiceno': complain.invoiceno,
                "invoicedate": complain.invoicedate,
                "errorcode": complain.errorcode,
                "dealername": complain.dealername,
                "installationdate": complain.installationdate,
                "skucode": complain.skucode,
                "skuname": complain.skuname,
                "brandname": complain.brandname,
                "producttobereturn": complain.producttobereturn,
                "custname": complain.custname,
                "add1": complain.add1,
                "add2": complain.add2,
                "add3": complain.add3,
                "pincode": complain.pincode,
                "area": complain.area,
                "city": complain.city,
                "state": complain.state,
                "country": complain.country,
                "description": complain.description,
                "lotno": complain.lotno ? complain.lotno : '',
                "appcomplaint": complain.appcomplaint,
                "assigntoservvendor": complain.assigntoservvendor,
                "salesrepresentative": complain.salesrepresentative ? complain.salesrepresentative : '',
                "newdealername": complain.newdealername,
                "serviceengname": complain.serviceengname,
                "serviceengcontact": complain.serviceengcontact,
                "servicevendor": complain.servicevendor,
                "visitdate": complain.visitdate,
                "servicevendorremark": complain.servicevendorremark,
                "assigntoservmg": complain.assigntoservmg,
                "activity": this.loginService.selectedactivity.id,
                "Appect": complain.Appect,
                "reject": complain.reject
            };
            //  console.log(this.TAG,"SALES SAERVICE FINAL",data);
            let complain_url = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_2__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerServiceInsert?';
            return this.genericHttpClientService.post(complain_url, data, httpOptions);
        }
        catch (error) {
            console.error(this.TAG, error);
        }
    }
};
CustomerServiceService.ctorParameters = () => [
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__.LoginauthService },
    { type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_3__.Commonfun },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__.LoginauthService },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpClient }
];
CustomerServiceService = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Injectable)({
        providedIn: 'root'
    })
], CustomerServiceService);



/***/ }),

/***/ 6337:
/*!**************************************!*\
  !*** ./src/assets/model/complain.ts ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Complain": () => (/* binding */ Complain)
/* harmony export */ });
class Complain {
    constructor() {
    }
}


/***/ }),

/***/ 93706:
/*!*********************************************************************************!*\
  !*** ./src/app/cardinal/customer-service/customer-service.page.scss?ngResource ***!
  \*********************************************************************************/
/***/ ((module) => {

module.exports = ".mat-stepper-vertical {\n  margin-top: 8px;\n}\n\n.mat-form-field {\n  margin-top: 16px;\n}\n\n.example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%;\n}\n\n.example-full-width {\n  width: 100%;\n}\n\n.cus {\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  width: 100%;\n}\n\nion-datetime {\n  width: 100%;\n}\n\nion-popover {\n  --width: 320px;\n}\n\nion-popover.dateTimePopover {\n  --offset-y: -350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImN1c3RvbWVyLXNlcnZpY2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtBQUNKOztBQUVFO0VBQ0UsZ0JBQUE7QUFDSjs7QUFFRTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBRUU7RUFDRSxXQUFBO0FBQ0o7O0FBRUM7RUFFQyxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtFQUNBLFdBQUE7QUFBRjs7QUFFQztFQUNDLFdBQUE7QUFDRjs7QUFDQTtFQUNFLGNBQUE7QUFFRjs7QUFBQTtFQUNFLDZCQUFBO0FBR0YiLCJmaWxlIjoiY3VzdG9tZXItc2VydmljZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWF0LXN0ZXBwZXItdmVydGljYWwge1xuICAgIG1hcmdpbi10b3A6IDhweDtcbiAgfVxuICBcbiAgLm1hdC1mb3JtLWZpZWxkIHtcbiAgICBtYXJnaW4tdG9wOiAxNnB4O1xuICB9XG5cbiAgLmV4YW1wbGUtZm9ybSB7XG4gICAgbWluLXdpZHRoOiAxNTBweDtcbiAgICBtYXgtd2lkdGg6IDUwMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIFxuICAuZXhhbXBsZS1mdWxsLXdpZHRoIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuXG4gLmN1c3tcbiAgXG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXIgO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIHdpZHRoOiAxMDAlO1xuIH1cbiBpb24tZGF0ZXRpbWUge1xuICB3aWR0aDogMTAwJTtcbn1cbmlvbi1wb3BvdmVyIHtcbiAgLS13aWR0aDogMzIwcHg7XG59XG5pb24tcG9wb3Zlci5kYXRlVGltZVBvcG92ZXIge1xuICAtLW9mZnNldC15OiAtMzUwcHggIWltcG9ydGFudDtcbiAgfSJdfQ== */";

/***/ }),

/***/ 27559:
/*!*********************************************************************************!*\
  !*** ./src/app/cardinal/customer-service/customer-service.page.html?ngResource ***!
  \*********************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Customer Service\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <ion-buttons (click)=\"RefreshPage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <mat-vertical-stepper [linear]=\"isLinear\" #stepper>\n    <mat-step [stepControl]=\"firstFormGroup\">\n      <form [formGroup]=\"firstFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Details</ng-template>\n        <!--Complaint Date -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Complaint Date</mat-label>\n          <input type=\"text\" matInput formControlName=\"complaintDateCtrl\">\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Complaint Date</ion-label>\n              <ion-input type=\"text\" formControlName=\"complaintDateCtrl\"></ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <!-- Document Type -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Document Type</mat-label>\n          <mat-select [(ngModel)]=\"docTypeSelected\" formControlName=\"documentTypeCtrl\"\n            (ionChange)=\"docTypeSelectedChange(docTypeSelected.name)\">\n            <mat-option *ngFor=\"let doc of docTypeList\" [value]=\"doc\">\n              {{doc.name}}\n            </mat-option>\n          </mat-select>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.documentType\">\n              <div *ngIf=\"firstFormGroup.get('name').hasError(validation.type) && firstStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <!--  -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Document Type</ion-label>\n              <ion-select [(ngModel)]=\"docTypeSelected\" formControlName=\"documentTypeCtrl\" interface=\"popover\"\n                (ionChange)=\"docTypeSelectedChange(docTypeSelected.name)\">\n                <ion-select-option *ngFor=\"let doc of docTypeList\" [value]=\"doc\">{{doc.name}}</ion-select-option>\n              </ion-select>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.documentType\">\n                <div *ngIf=\"firstFormGroup.get('documentTypeCtrl').hasError(validation.type) && firstStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- Name of Complainer -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Name of Complainer</mat-label>\n          <input matInput formControlName=\"name\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.name\">\n              <div *ngIf=\"firstFormGroup.get('name').hasError(validation.type) && firstStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Name of Complainer</ion-label>\n              <ion-input formControlName=\"name\"></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.name\">\n                <div *ngIf=\"firstFormGroup.get('name').hasError(validation.type) && firstStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- Designation of Complainer -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Designation of Complainer</mat-label>\n          <mat-select [(ngModel)]=\"designationOfComplainerSelected\" formControlName=\"designationOfComplainerCtrl\"\n            (ionChange)=\"designationOfComplainerChange(designationOfComplainerSelected)\">\n            <mat-option *ngFor=\"let designationObject of designationOfComplainerList\" [value]=\"designationObject\">\n              {{designationObject.name}}\n            </mat-option>\n          </mat-select>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.designation\">\n              <div\n                *ngIf=\"firstFormGroup.get('designationOfComplainerCtrl').hasError(validation.type) && firstStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Designation of Complainer</ion-label>\n              <ion-select [(ngModel)]=\"designationOfComplainerSelected\" formControlName=\"designationOfComplainerCtrl\"\n                interface=\"popover\" (ionChange)=\"designationOfComplainerChange(designationOfComplainerSelected)\">\n                <ion-select-option *ngFor=\"let designationObject of designationOfComplainerList\"\n                  [value]=\"designationObject\">{{designationObject.name}}</ion-select-option>\n              </ion-select>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.designation\">\n                <div\n                  *ngIf=\"firstFormGroup.get('designationOfComplainerCtrl').hasError(validation.type) && firstStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- Contact No -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Contact No.</mat-label>\n          <input type=\"number\" matInput formControlName=\"mobileno\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.mobileno\">\n              <div *ngIf=\"firstFormGroup.get('mobileno').hasError(validation.type) && firstStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Contact No.</ion-label>\n              <ion-input type=\"number\" formControlName=\"mobileno\"></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.mobileno\">\n                <div *ngIf=\"firstFormGroup.get('mobileno').hasError(validation.type) && firstStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- Email ID -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Email ID</mat-label>\n          <input type=\"email\" matInput formControlName=\"email\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.email\">\n              <div *ngIf=\"firstFormGroup.get('email').hasError(validation.type) && firstStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Email ID</ion-label>\n              <ion-input type=\"email\" formControlName=\"email\"></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.email\">\n                <div *ngIf=\"firstFormGroup.get('email').hasError(validation.type) && firstStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- Event Date -->\n        <div class=\"example-full-width bottom-border\">\n          <!-- <ion-label style=\"color: darkgray;\">Event Date</ion-label> -->\n          <section class=\"cus\">\n            <!-- <ion-datetime  style=\"--padding-start:0px\" displayFormat=\"DD-MM-YYYY\" [(ngModel)]=\"today\" formControlName=\"eventDateCtrl\" [max]=\"maxDate | date:'yyyy-MM-dd'\">\n          </ion-datetime>\n          <ion-icon name=\"calendar\"></ion-icon>  -->\n            <ion-item>\n              <ion-label position=\"stacked\">Event Date</ion-label>\n              <ion-item>\n                <ion-input placeholder=\"Select Date\" [value]=\"today\"></ion-input>\n                <ion-button fill=\"clear\" id=\"open-date-input-1\">\n                  <ion-icon icon=\"calendar\"></ion-icon>\n                </ion-button>\n                <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\n                  <ng-template>\n                    <ion-datetime #popoverDatetime2 presentation=\"date\" showDefaultButtons=\"true\" [(ngModel)]=\"today\"\n                      formControlName=\"eventDateCtrl\" [max]=\"maxDate | date:'yyyy-MM-dd'\"\n                      (ionChange)=\"dateevent = formatDate(popoverDatetime2.value)\"></ion-datetime>\n                  </ng-template>\n                </ion-popover>\n              </ion-item>\n            </ion-item>\n          </section>\n        </div>\n        <!-- Complaint Description -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Complaint Description</mat-label>\n          <textarea matInput formControlName=\"complaintDescriptionCtrl\"></textarea>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.complaintDescriptionCtrl\">\n              <div *ngIf=\"firstFormGroup.get('complaintDescriptionCtrl').hasError(validation.type) && firstStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Complaint Description</ion-label>\n              <ion-textarea formControlName=\"complaintDescriptionCtrl\"></ion-textarea>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.complaintDescriptionCtrl\">\n                <div *ngIf=\"firstFormGroup.get('complaintDescriptionCtrl').hasError(validation.type) && firstStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <div>\n          <button mat-button matStepperNext (click)=\"matSettper($event,firstFormGroup)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n\n    <mat-step [stepControl]=\"productCompliantFormGroup\" label=\"Product Compliant\" *ngIf=\"isProductCompliant\"\n      [optional]=\"isOptional\">\n      <form [formGroup]=\"productCompliantFormGroup\" class=\"example-form\">\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Lot No.</mat-label>\n          <input matInput formControlName=\"lotNoProductCompliantCtrl\">\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperProductCom($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n\n    <mat-step [stepControl]=\"consumablesFormGroup\" label=\"Consumables\" *ngIf=\"isConsumables\">\n      <form [formGroup]=\"consumablesFormGroup\" class=\"example-form\">\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Lot No.</mat-label>\n          <input matInput formControlName=\"lotNoConsumablesCtrl\">\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperConsumables($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n\n    <mat-step [stepControl]=\"equipmentFormGroup\" label=\"Equiment Detail\" *ngIf=\"isEquipment\">\n      <form [formGroup]=\"equipmentFormGroup\">\n        <!-- Serial No -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Serial No.</mat-label>\n          <input matInput formControlName=\"serialNo\" (change)='onCheckInstallationBase()'>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.serialNo\">\n              <div *ngIf=\"equipmentFormGroup.get('serialNo').hasError(validation.type) && secondStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Serial No</ion-label>\n              <ion-input formControlName=\"serialNo\" (change)='onCheckInstallationBase()'></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.serialNo\">\n                <div *ngIf=\"equipmentFormGroup.get('serialNo').hasError(validation.type) && secondStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- Sr. No. of Equipment -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label>Sr. No. of Equipment</mat-label>\n          <input matInput formControlName=\"SrNoEquipmentCtrl\">\n        </mat-form-field> -->\n        <!-- Contract Type -->\n\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Contract Type</mat-label>\n          <mat-select [(ngModel)]=\"contractTypeSelected\" formControlName=\"contractType\"\n            (ionChange)=\"contractTypeSelectedChange(contractTypeSelected)\">\n            <mat-option *ngFor=\"let contract of contractTypeList\" [value]=\"contract\">\n              {{contract.name}}\n            </mat-option>\n          </mat-select>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.contractType\">\n              <div *ngIf=\"equipmentFormGroup.get('contractType').hasError(validation.type) && firstStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Contract Type</ion-label>\n              <ion-select [(ngModel)]=\"contractTypeSelected\" formControlName=\"contractType\"\n                (ionChange)=\"contractTypeSelectedChange(contractTypeSelected)\" interface=\"popover\">\n                <ion-select-option *ngFor=\"let contract of contractTypeList\" [value]=\"contract\">{{contract.name}}\n                </ion-select-option>\n              </ion-select>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.contractType\">\n                <div *ngIf=\"equipmentFormGroup.get('contractType').hasError(validation.type) && firstStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n\n        <!-- Invoice No -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label>Invoice No.</mat-label>\n          <input matInput formControlName=\"invoiceNoCtrl\">\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label  position=\"stacked\">Invoice No.</ion-label>\n              <ion-input formControlName=\"invoiceNoCtrl\"></ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <!-- Invoice Date -->\n        <div class=\"example-full-width bottom-border\">\n          <!-- <ion-label style=\"color: darkgray;\">Invoice Date</ion-label> -->\n          <section class=\"cus\">\n            <!-- <ion-datetime  style=\"--padding-start:0px\" displayFormat=\"DD-MM-YYYY\"  formControlName=\"invoiceDateCtrl\" [max]=\"maxDate | date:'yyyy-MM-dd'\">\n          </ion-datetime> -->\n            <!-- <ion-icon name=\"calendar\"></ion-icon>  -->\n            <ion-item>\n              <ion-label position=\"stacked\">Invoice Date</ion-label>\n              <ion-item>\n                <ion-input placeholder=\"Select Date\" [value]=\"dateinvoice\"></ion-input>\n                <ion-button fill=\"clear\" id=\"open-date-input-2\">\n                  <ion-icon icon=\"calendar\"></ion-icon>\n                </ion-button>\n                <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-2\" show-backdrop=\"false\">\n                  <ng-template>\n                    <ion-datetime #popoverDatetime2 formControlName=\"invoiceDateCtrl\"\n                      [max]=\"maxDate | date:'yyyy-MM-dd'\" presentation=\"date\" showDefaultButtons=\"true\"\n                      (ionChange)=\"dateinvoice = formatDate1(popoverDatetime2.value)\"></ion-datetime>\n                  </ng-template>\n                </ion-popover>\n              </ion-item>\n            </ion-item>\n          </section>\n        </div>\n\n\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label>Invoice Date</mat-label>\n          <input matInput type=\"date\" [disabled]='true' formControlName=\"invoiceDateCtrl\" matInput [max]=\"maxDate | date:'yyyy-MM-dd'\">\n        </mat-form-field> -->\n        <!-- Error Code -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Error Code</mat-label>\n          <mat-select [(ngModel)]=\"errorCodeSelected\" formControlName=\"errorCode\"\n            (ionChange)=\"errorCodeSelectedSelectedChange(errorCodeSelected)\">\n            <mat-option *ngFor=\"let errorCode of errorCodeList\" [value]=\"errorCode\">\n              {{errorCode.name}}\n            </mat-option>\n          </mat-select>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.errorCodeCtrlErrorMessage\">\n              <div *ngIf=\"equipmentFormGroup.get('errorCode').hasError(validation.type) && firstStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Error Code</ion-label>\n              <ion-select [(ngModel)]=\"errorCodeSelected\" formControlName=\"errorCode\"\n                (ionChange)=\"errorCodeSelectedSelectedChange(errorCodeSelected)\" interface=\"popover\">\n                <ion-select-option *ngFor=\"let errorCode of errorCodeList\" [value]=\"errorCode\">{{errorCode.name}}\n                </ion-select-option>\n              </ion-select>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.errorCodeCtrlErrorMessage\">\n                <div *ngIf=\"equipmentFormGroup.get('errorCode').hasError(validation.type) && firstStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n\n        <!-- Dealer Name -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label>Dealer Name</mat-label>\n          <input matInput formControlName=\"dealerNameCtrl\">\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label  position=\"stacked\">Dealer Name</ion-label>\n              <ion-input formControlName=\"dealerNameCtrl\"></ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <!-- Installation Date -->\n        <div class=\"example-full-width bottom-border\">\n          <!-- <ion-label style=\"color: darkgray;\">Installation Date</ion-label> -->\n          <section class=\"cus\">\n            <!-- <ion-datetime  style=\"--padding-start:0px\" displayFormat=\"DD-MM-YYYY\"  formControlName=\"installationDateCtrl\" [max]=\"maxDate | date:'yyyy-MM-dd'\">\n          </ion-datetime>\n          <ion-icon name=\"calendar\"></ion-icon>  -->\n            <ion-item>\n              <ion-label position=\"stacked\">Installation Date</ion-label>\n              <ion-item>\n                <ion-input placeholder=\"Select Date\" [value]=\"dateinstall\"></ion-input>\n                <ion-button fill=\"clear\" id=\"open-date-input-3\">\n                  <ion-icon icon=\"calendar\"></ion-icon>\n                </ion-button>\n                <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-3\" show-backdrop=\"false\">\n                  <ng-template>\n                    <ion-datetime #popoverDatetime2 formControlName=\"installationDateCtrl\"\n                      [max]=\"maxDate | date:'yyyy-MM-dd'\" presentation=\"date\" showDefaultButtons=\"true\"\n                      (ionChange)=\"dateinstall = formatDate2(popoverDatetime2.value)\"></ion-datetime>\n                  </ng-template>\n                </ion-popover>\n              </ion-item>\n            </ion-item>\n          </section>\n        </div>\n\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label>Installation Date</mat-label>\n          <input type=\"date\" matInput formControlName=\"installationDateCtrl\" matInput\n            [max]=\"maxDate | date:'yyyy-MM-dd'\">\n        </mat-form-field> -->\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperSecond($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n\n    <mat-step [stepControl]=\"thirdFormGroup\" label=\"SKU Detail\">\n      <form [formGroup]=\"thirdFormGroup\">\n        <!-- SKU Code -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">SKU Code</mat-label>\n          <input matInput formControlName=\"SKUCode\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.SKUCode\">\n              <div *ngIf=\"thirdFormGroup.get('SKUCode').hasError(validation.type) && thirdStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">SKU Code</ion-label>\n              <ion-input formControlName=\"SKUCode\"></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.SKUCode\">\n                <div *ngIf=\"thirdFormGroup.get('SKUCode').hasError(validation.type) && thirdStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- SKU Name / Description -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label>SKU Name / Description</mat-label>\n          <input matInput formControlName=\"skuName\">\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label position=\"stacked\">SKU Name / Description</ion-label>\n              <ion-input formControlName=\"skuName\"></ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <!-- Brand Name -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Brand Name</mat-label>\n          <input matInput formControlName=\"brandName\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.brandName\">\n              <div *ngIf=\"thirdFormGroup.get('brandName').hasError(validation.type) && thirdStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Brand Name</ion-label>\n              <ion-input formControlName=\"brandName\"></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.brandName\">\n                <div *ngIf=\"thirdFormGroup.get('brandName').hasError(validation.type) && thirdStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- Product to be returned -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label>Product to be returned</mat-label>\n          <mat-select [(ngModel)]=\"ProductReturnedSelected\" [ngModelOptions]=\"{standalone: true}\"\n            (ionChange)=\"ProductReturnedSelectedChange(ProductReturnedSelected)\">\n            <mat-option value=\"Y\">Yes</mat-option>\n            <mat-option value=\"N\">No</mat-option>\n          </mat-select>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label position=\"stacked\">Product to be returned</ion-label>\n              <ion-select [(ngModel)]=\"ProductReturnedSelected\" [ngModelOptions]=\"{standalone: true}\"\n                (ionChange)=\"ProductReturnedSelectedChange(ProductReturnedSelected)\" interface=\"popover\">\n                <ion-select-option value=\"Y\">Yes</ion-select-option>\n                <ion-select-option value=\"N\">No</ion-select-option>\n              </ion-select>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperThird($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Customer Detail Tab -->\n    <mat-step [stepControl]=\"fourthFormGroup\" label=\"Customer Detail\">\n      <form [formGroup]=\"fourthFormGroup\">\n        <!-- Customer Name -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Customer Name</mat-label>\n          <input matInput formControlName=\"customerName\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.customerName\">\n              <div *ngIf=\"fourthFormGroup.get('customerName').hasError(validation.type) && fourthStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Customer Name</ion-label>\n              <ion-input formControlName=\"customerName\"></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.customerName\">\n                <div *ngIf=\"fourthFormGroup.get('customerName').hasError(validation.type) && fourthStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- Customer Address 1 -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Customer Address 1</mat-label>\n          <input matInput formControlName=\"customerAddress1\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.customerAddress1\">\n              <div *ngIf=\"fourthFormGroup.get('customerAddress1').hasError(validation.type) && fourthStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Customer Address 1</ion-label>\n              <ion-input formControlName=\"customerAddress1\"></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.customerAddress1\">\n                <div *ngIf=\"fourthFormGroup.get('customerAddress1').hasError(validation.type) && fourthStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- Customer Address 2 -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label>Customer Address 2</mat-label>\n          <input matInput formControlName=\"customerAddress2\">\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label position=\"stacked\">Customer Address 2</ion-label>\n              <ion-input formControlName=\"customerAddress2\"></ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <!-- Customer Address 3 -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label>Customer Address 3</mat-label>\n          <input matInput formControlName=\"customerAddress3\">\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label  position=\"stacked\">Customer Address 3</ion-label>\n              <ion-input formControlName=\"customerAddress3\"></ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <!-- Pin Code -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Pin Code</mat-label>\n          <input type=\"number\" matInput formControlName=\"pinCodeCtrl\" (change)='onChangePinCode()'>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.pinCode\">\n              <div *ngIf=\"fourthFormGroup.get('pinCodeCtrl').hasError(validation.type) && fourthStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Pin Code</ion-label>\n              <ion-input type=\"number\" formControlName=\"pinCodeCtrl\" (change)='onChangePinCode()'></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.pinCode\">\n                <div *ngIf=\"fourthFormGroup.get('pinCodeCtrl').hasError(validation.type) && fourthStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- Area -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Area</mat-label>\n          <mat-select formControlName=\"area\" (ionChange)=\"onAreaSelectedChange()\" [(ngModel)]=\"areaSelected\">\n            <mat-option *ngFor=\"let area of areaList\" [value]=\"area\">\n              {{area.area}}\n            </mat-option>\n          </mat-select>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.area\">\n              <div *ngIf=\"fourthFormGroup.get('area').hasError(validation.type) && fourthStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Area</ion-label>\n              <ion-select formControlName=\"area\" (ionChange)=\"onAreaSelectedChange()\" [(ngModel)]=\"areaSelected\" interface=\"popover\">\n              <ion-select-option *ngFor=\"let area of areaList\" [value]=\"area\"> {{area.area}}\n              </ion-select-option>\n            </ion-select>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.area\">\n                <div *ngIf=\"fourthFormGroup.get('area').hasError(validation.type) && fourthStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- City -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">City</mat-label>\n          <input matInput formControlName=\"city\" [(ngModel)]=\"city\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.city\">\n              <div *ngIf=\"fourthFormGroup.get('city').hasError(validation.type) && fourthStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">City</ion-label>\n              <ion-input formControlName=\"city\" [(ngModel)]=\"city\"></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.city\">\n                <div *ngIf=\"fourthFormGroup.get('city').hasError(validation.type) && fourthStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- State -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">State</mat-label>\n          <input matInput formControlName=\"state\" [(ngModel)]=\"state\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.state\">\n              <div *ngIf=\"fourthFormGroup.get('state').hasError(validation.type) && fourthStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">State</ion-label>\n              <ion-input formControlName=\"state\" [(ngModel)]=\"state\"></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.state\">\n                <div *ngIf=\"fourthFormGroup.get('state').hasError(validation.type) && fourthStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- Country -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Country</mat-label>\n          <input matInput formControlName=\"country\" [(ngModel)]=\"country\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.country\">\n              <div *ngIf=\"fourthFormGroup.get('country').hasError(validation.type) && fourthStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field> -->\n        <ion-row class=\"example-full-width\">\n          <ion-col>\n            <ion-item>\n              <ion-label class=\"asterisk_input\" position=\"stacked\">Country</ion-label>\n              <ion-input formControlName=\"country\" [(ngModel)]=\"country\"></ion-input>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.country\">\n                <div *ngIf=\"fourthFormGroup.get('country').hasError(validation.type) && fourthStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n        </div>\n        <div>\n          <ion-row>\n            <ion-col no-padding>\n              <ion-button size=\"default\" [disabled]=\"!fourthFormGroup.valid && fourthFormGroup.disabled === false\"\n                class=\"submit-btn\" expand=\"block\" color=\"primary\" (click)=\"submit()\">Assign to COP\n              </ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n      </form>\n    </mat-step>\n  </mat-vertical-stepper>\n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_cardinal_customer-service_customer-service_module_ts.js.map