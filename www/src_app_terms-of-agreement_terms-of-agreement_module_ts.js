"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_terms-of-agreement_terms-of-agreement_module_ts"],{

/***/ 35871:
/*!*****************************************************************!*\
  !*** ./src/app/terms-of-agreement/terms-of-agreement.module.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TermsOfAgreementPageModule": () => (/* binding */ TermsOfAgreementPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _terms_of_agreement_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./terms-of-agreement.page */ 39232);







const routes = [
    {
        path: '',
        component: _terms_of_agreement_page__WEBPACK_IMPORTED_MODULE_0__.TermsOfAgreementPage
    }
];
let TermsOfAgreementPageModule = class TermsOfAgreementPageModule {
};
TermsOfAgreementPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_terms_of_agreement_page__WEBPACK_IMPORTED_MODULE_0__.TermsOfAgreementPage]
    })
], TermsOfAgreementPageModule);



/***/ }),

/***/ 39232:
/*!***************************************************************!*\
  !*** ./src/app/terms-of-agreement/terms-of-agreement.page.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TermsOfAgreementPage": () => (/* binding */ TermsOfAgreementPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _terms_of_agreement_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./terms-of-agreement.page.html?ngResource */ 14430);
/* harmony import */ var _terms_of_agreement_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./terms-of-agreement.page.scss?ngResource */ 38976);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ 80190);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _terms_of_agreement_terms_of_agreement_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../terms-of-agreement/terms-of-agreement.service */ 54531);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _awesome_cordova_plugins_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @awesome-cordova-plugins/in-app-browser/ngx */ 12407);
/* harmony import */ var _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @awesome-cordova-plugins/file-transfer/ngx */ 80464);
/* harmony import */ var _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @awesome-cordova-plugins/file/ngx */ 25453);
/* harmony import */ var _awesome_cordova_plugins_file_opener_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @awesome-cordova-plugins/file-opener/ngx */ 8456);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../provider/message-helper */ 98792);
















let TermsOfAgreementPage = class TermsOfAgreementPage {
  constructor(router, storage, route, commonfun, termsOfAgreementService, file, transfer, fileOpener, loginservc, iab, platform, msg) {
    this.router = router;
    this.storage = storage;
    this.route = route;
    this.commonfun = commonfun;
    this.termsOfAgreementService = termsOfAgreementService;
    this.file = file;
    this.transfer = transfer;
    this.fileOpener = fileOpener;
    this.loginservc = loginservc;
    this.iab = iab;
    this.platform = platform;
    this.msg = msg;
    this.text = `Forms are the pillar of any business applications. You can use forms to perform countless data-entry tasks such as: login, submit a request, place an order, book a flight or create an account.

When developing a form, it’s important to create a good data-entry experience to efficiently guide the user through the workflow.

Developing good forms requires design and user experience skills, as well as a framework with support for two-way data binding, change tracking, validation, and error handling such as Angular. As you may know, Ionic is built on top of Angular. (if you didn’t know that you should consider reading Ionic Framework introduction and key components).

Forms are usually one of the major interaction points between an app and the user, allowing them to send data to the application. Commonly, the data is sent to the web server but the app can also intercept it to use it on its own.`;
    this.isalreadyaccepted = false;
    this.isActivitySelected = false;
    this.txt = "Accept";
    this.options = {
      location: 'yes',
      hidden: 'no',
      clearcache: 'yes',
      clearsessioncache: 'yes',
      zoom: 'yes',
      hardwareback: 'yes',
      mediaPlaybackRequiresUserAction: 'no',
      shouldPauseOnSuspend: 'no',
      closebuttoncaption: 'Close',
      disallowoverscroll: 'no',
      toolbar: 'yes',
      enableViewportScale: 'no',
      allowInlineMediaPlayback: 'no',
      presentationstyle: 'pagesheet',
      fullscreen: 'yes' //Windows only    

    };
    this.termslist = [];
    this.isallaccepted = false;
    this.type = '';
    this.iscustomeremp = false;
  }

  ngOnInit() {
    try {
      //  this.commonfun.chkcache('terms-of-agreement');
      setTimeout(() => {
        this.Bindallactivity();
      }, 3000);
    } catch (error) {}
  }

  getParam() {
    this.route.params.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        // this.selectedbunch=null;
        this.redirectto = this.router.getCurrentNavigation().extras.state.redirectto;
      }
    });
  }

  exonActChangefirst(data) {
    try {
      this.isActivitySelected = true;
      this.loginservc.selectedactivity = this.selectedactivity;
      this.type = this.selectedactivity.type;
      this.loginservc.dealer_id = this.selectedactivity.dealer_id;
      this.loginservc.service_manager_id = this.selectedactivity.service_manager_id;
      this.loginservc.vender_id = this.selectedactivity.vender_id;
      this.storage.set('selectedactivity', this.selectedactivity);
      this.loginservc.isschemeinfo = false; //this.selectedactivity.schemeinfo == "Y"  ? true : false;

      this.loginservc.Discount_Method = this.selectedactivity.Discount_Method ? this.selectedactivity.Discount_Method : '';
      this.loginservc.dashboard = this.selectedactivity.dashboard == "Y" ? true : false;
      this.iscustomeremp = this.loginservc.defaultprofile[0].mmstOrderusrtype === "CEB" ? true : false;

      if (this.selectedactivity.status == "A") {
        this.isalreadyaccepted = true;
        this.txt = "Next";
      } else {
        if (this.iscustomeremp) {
          this.txt = "Accept";
          this.isalreadyaccepted = false;
        } else {
          this.isalreadyaccepted = true;
          this.txt = "Next";
        }
      }

      this.getActivityWiseLOGO(); //this.getDownloadORBrowse();
      // this.events.publish('updateMenu');

      this.loginservc.publishSomeData('updateMenu');

      if (this.iscustomeremp) {
        if (this.selectedactivity.type == 'PDF') {
          //this.url = this.loginservc.commonurl + 'ws/in.mbs.webservice.DownloadORBrowse?record=' + this.selectedactivity.id + '&isdownload=false&' + this.loginservc.parameter;
          this.termslist = this.selectedactivity.termslist;

          if (this.termslist.length === 0) {
            this.router.navigateByUrl('/home');
          }
        } else if (this.selectedactivity.type == 'URL') {
          this.url = this.selectedactivity.url;

          if (this.isalreadyaccepted) {
            this.router.navigateByUrl('/home');
          }
        } else {
          this.url = '';
          this.isalreadyaccepted = true;
          this.txt = "Next";
          this.router.navigateByUrl('/home');
        }
      } else {
        this.router.navigateByUrl('/home');
      } // this.events.publish('dashbaordEvent');

    } catch (error) {}
  }

  exonActChange() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        if (_this.activitylist.length > 1) {
          _this.isActivitySelected = true; //  console.log("exonActChange");

          _this.loginservc.selectedactivity = _this.selectedactivity;
          _this.type = _this.selectedactivity.type; // console.log("exonActChange Selected Activity",this.selectedactivity);

          if (_this.selectedactivity.secondary_order == "Y") {
            _this.loginservc.isConsolidationOrder = true;
          } else if (_this.selectedactivity.secondary_order == "N") {
            _this.loginservc.isConsolidationOrder = false;
          }

          let windowdata = yield (yield _this.termsOfAgreementService.getWindowAccess(_this.selectedactivity.id)).toPromise();
          _this.loginservc.roleWiseDashboard = windowdata.RoleWiseDashboard; // console.log("Window Acccesss Data",windowdata);

          _this.loginservc.isNewLead = windowdata.WindowAccess["New Lead"] == "Y" ? true : false;
          _this.loginservc.isExistingLead = windowdata.WindowAccess["Existing Lead"] == "Y" ? true : false;
          _this.loginservc.isBusinessPartnerAddress = windowdata.WindowAccess["Business Partner Address"] == "Y" ? true : false;
          _this.loginservc.isNewOrder = windowdata.WindowAccess["New Order"] == "Y" ? true : false;
          _this.loginservc.isDraftOrder = windowdata.WindowAccess["Draft Order"] == "Y" ? true : false;
          _this.loginservc.isOrderStatus = windowdata.WindowAccess["Order Status"] == "Y" ? true : false;
          _this.loginservc.isLatLongFinder = windowdata.WindowAccess["Lat-Long Finder"] == "Y" ? true : false;
          _this.loginservc.isTravelPlan = windowdata.WindowAccess["Travel Plan"] == "Y" ? true : false;
          _this.loginservc.isActualTravelPlan = windowdata.WindowAccess["Actual Travel Plan"] == "Y" ? true : false;
          _this.loginservc.isTravelExpense = windowdata.WindowAccess["Travel Expense"] == "Y" ? true : false;
          _this.loginservc.isTravelPlanClosure = windowdata.WindowAccess["Travel Plan Closure"] == "Y" ? true : false;
          _this.loginservc.isApprovalAccess = windowdata.WindowAccess["Approval"] == "Y" ? true : false;
          _this.loginservc.isCustomerServiceAccess = windowdata.WindowAccess["Customer Service"] == "Y" ? true : false;
          _this.loginservc.isCompliantAcceptanceAccess = windowdata.WindowAccess["Complaint Acceptance"] == "Y" ? true : false;
          _this.loginservc.isComplaintReportingAccess = windowdata.WindowAccess["Complaint Report"] == "Y" ? true : false;
          _this.loginservc.isFieldVisitAccess = windowdata.WindowAccess["Field Visit"] == "Y" ? true : false;
          _this.loginservc.isQuotationAccess = windowdata.WindowAccess["isQuotationAccess"] == "Y" ? true : false;
          _this.loginservc.isQuotationApproval = windowdata.WindowAccess["isQuotationApproval"] == "Y" ? true : false;
          _this.loginservc.isUpload = windowdata.WindowAccess["isUpload"] == "Y" ? true : false;
          _this.loginservc.isReport = windowdata.WindowAccess["Reports"] == "Y" ? true : false;
          _this.loginservc.isARVisitSchedule = windowdata.WindowAccess["AR Visit Schedule"] == "Y" ? true : false;
          _this.loginservc.dealer_id = _this.selectedactivity.dealer_id;
          _this.loginservc.service_manager_id = _this.selectedactivity.service_manager_id;
          _this.loginservc.vender_id = _this.selectedactivity.vender_id;
          _this.loginservc.isschemeinfo = _this.selectedactivity.Isschemempower == "Y" && windowdata.WindowAccess["Scheme"] === 'Y' ? true : false;
          _this.loginservc.Discount_Method = _this.selectedactivity.Discount_Method ? _this.selectedactivity.Discount_Method : '';
          _this.loginservc.dashboard = _this.selectedactivity.dashboard == "Y" ? true : false;
          _this.iscustomeremp = _this.loginservc.defaultprofile[0].mmstOrderusrtype === "CEB" ? true : false; // this.events.publish('dashbaordEvent');

          _this.loginservc.publishSomeData('dashbaordEvent');

          _this.storage.set('selectedactivity', _this.selectedactivity);

          if (_this.selectedactivity.status == "A") {
            _this.isalreadyaccepted = true;
            _this.txt = "Next";
          } else {
            if (_this.loginservc.defaultprofile[0].mmstOrderusrtype == "CEB") {
              _this.txt = "Accept";
              _this.isalreadyaccepted = false;
            } else {
              _this.isalreadyaccepted = true;
              _this.txt = "Next";
            }
          }

          _this.getActivityWiseLOGO(); //this.getDownloadORBrowse();


          if (_this.selectedactivity.type == 'PDF') {
            // this.url = this.loginservc.commonurl + 'ws/in.mbs.webservice.DownloadORBrowse?record=' + this.selectedactivity.id
            // + '&isdownload=false&' + this.loginservc.parameter;
            _this.termslist = _this.selectedactivity.termslist;
          } else if (_this.selectedactivity.type == 'URL') {
            _this.url = _this.selectedactivity.url;
          } else {
            _this.url = '';
            _this.isalreadyaccepted = true;
            _this.txt = "Next";
          }
        } // this.events.publish('updateMenu');


        _this.loginservc.publishSomeData('updateMenu');
      } catch (error) {//  console.log("Error:exonActChange", error);
      }
    })();
  }

  onclickNext() {
    this.router.navigateByUrl('/home');
  }

  onclickacceptPdf(file) {
    file.accept = true;
    this.onSaveAgreement("A", file.fileid);
    this.isallaccepted = this.termslist.filter(terms => {
      return terms.accept === false;
    }).length > 0 ? false : true;
  }

  onclickRejectPdf(file) {
    file.reject = !file.reject;
    this.onSaveAgreement("R", file.fileid);
  }

  onclick(type) {
    try {
      if (type == 'Accept') {
        if (this.txt == "Accept") {
          this.onSaveAgreement("A", '');
        } else {
          this.router.navigateByUrl('/home');
        }
      } else {
        //window.open("http://africau.edu/images/default/sample.pdf","_system","location=yes,enableViewportScale=yes,hidden=no");
        this.onSaveAgreement("R", '');
      }
    } catch (error) {}
  }

  inapp(fileid) {
    try {
      if (this.msg.isplatformweb == false) {
        if (this.selectedactivity.type == 'PDF') {
          this.commonfun.loadingPresent();
          this.url = this.loginservc.commonurl + 'ws/in.mbs.webservice.DownloadORBrowse?fileid=' + fileid + '&isdownload=false&' + this.loginservc.parameter;
          const fileTransfer = this.transfer.create();
          fileTransfer.download(encodeURI(this.url), this.file.dataDirectory + 'file.pdf').then(entry => {
            this.commonfun.loadingDismiss();
            this.fileOpener.open(entry.toURL(), "application/pdf").then(() => console.log("File is opened")).catch(e => console.log("Error opening file", e));
          }, error => {
            this.commonfun.loadingDismiss();
          });
        } else {
          let target = "_blank";
          this.iab.create(this.url, target, this.options);
        }
      } else {
        //for WPA
        let target = "_blank";

        if (this.selectedactivity.type == 'PDF') {
          this.url = this.loginservc.commonurl + 'ws/in.mbs.webservice.DownloadORBrowse?fileid=' + fileid + '&isdownload=false&' + this.loginservc.parameter;
        }

        this.iab.create(this.url, target, this.options);
      }
    } catch (error) {//  console.log("error", error);
    }
  }

  Bindallactivity() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this2.termsOfAgreementService.getUserActivityAgreementStatus().subscribe(data => {
          _this2.activitylist = data;
          _this2.loginservc.isFieldVisitAccess = true;

          if (_this2.activitylist.length == 1) {
            _this2.selectedactivity = _this2.activitylist[0];

            if (_this2.selectedactivity.secondary_order == "Y") {
              _this2.loginservc.isConsolidationOrder = true;
            } else if (_this2.selectedactivity.secondary_order == "N") {
              _this2.loginservc.isConsolidationOrder = false;
            }

            _this2.termsOfAgreementService.getWindowAccess(_this2.activitylist[0].id).subscribe(windowdata => {
              _this2.loginservc.isNewLead = windowdata.WindowAccess["New Lead"] == "Y" ? true : false;
              _this2.loginservc.isExistingLead = windowdata.WindowAccess["Existing Lead"] == "Y" ? true : false;
              _this2.loginservc.isBusinessPartnerAddress = windowdata.WindowAccess["Business Partner Address"] == "Y" ? true : false;
              _this2.loginservc.isNewOrder = windowdata.WindowAccess["New Order"] == "Y" ? true : false;
              _this2.loginservc.isDraftOrder = windowdata.WindowAccess["Draft Order"] == "Y" ? true : false;
              _this2.loginservc.isOrderStatus = windowdata.WindowAccess["Order Status"] == "Y" ? true : false;
              _this2.loginservc.isLatLongFinder = windowdata.WindowAccess["Lat-Long Finder"] == "Y" ? true : false;
              _this2.loginservc.isTravelPlan = windowdata.WindowAccess["Travel Plan"] == "Y" ? true : false;
              _this2.loginservc.isActualTravelPlan = windowdata.WindowAccess["Actual Travel Plan"] == "Y" ? true : false;
              _this2.loginservc.isTravelExpense = windowdata.WindowAccess["Travel Expense"] == "Y" ? true : false;
              _this2.loginservc.isTravelPlanClosure = windowdata.WindowAccess["Travel Plan Closure"] == "Y" ? true : false;
              _this2.loginservc.isApprovalAccess = windowdata.WindowAccess["Approval"] == "Y" ? true : false;
              _this2.loginservc.isCustomerServiceAccess = windowdata.WindowAccess["Customer Service"] == "Y" ? true : false;
              _this2.loginservc.isCompliantAcceptanceAccess = windowdata.WindowAccess["Complaint Acceptance"] == "Y" ? true : false;
              _this2.loginservc.isComplaintReportingAccess = windowdata.WindowAccess["Complaint Report"] == "Y" ? true : false;
              _this2.loginservc.isFieldVisitAccess = windowdata.WindowAccess["Field Visit"] == "Y" ? true : false;
              _this2.loginservc.isQuotationAccess = windowdata.WindowAccess["isQuotationAccess"] == "Y" ? true : false;
              _this2.loginservc.isQuotationApproval = windowdata.WindowAccess["isQuotationApproval"] == "Y" ? true : false;
              _this2.loginservc.isUpload = windowdata.WindowAccess["isUpload"] == "Y" ? true : false;
              _this2.loginservc.isReport = windowdata.WindowAccess["Reports"] == "Y" ? true : false;
              _this2.loginservc.isARVisitSchedule = windowdata.WindowAccess["AR Visit Schedule"] == "Y" ? true : false; // this.events.publish('dashbaordEvent');

              _this2.loginservc.publishSomeData('dashbaordEvent');

              _this2.loginservc.roleWiseDashboard = windowdata.RoleWiseDashboard;

              _this2.exonActChangefirst(data);
            });
          }
        }, error => {//  console.log("error", error);
        });
      } catch (error) {//  console.log("error", error);
      }
    })();
  }

  onSaveAgreement(typeRA, fileid) {
    try {
      this.commonfun.loadingPresent(); //--------------

      this.termsOfAgreementService.SaveAgreement(this.selectedactivity.id, typeRA, fileid, this.selectedactivity.type).subscribe(data => {
        this.commonfun.loadingDismiss();

        if (data != null) {
          this.saveresponse = data;

          if (this.saveresponse.resposemsg == "Success") {
            //  this.commonfun.presentAlert("Message",this.saveplanresponse.resposemsg,this.saveplanresponse.logmsg);
            if (this.selectedactivity.type === 'PDF') {
              if (typeRA == 'R') {
                this.router.navigateByUrl('/login');
              }
            } else {
              if (typeRA == 'A') {
                this.router.navigateByUrl('/home');
              } else {
                this.router.navigateByUrl('/login');
              }
            }
          } else {
            this.commonfun.loadingDismiss(); //  this.Resetpage();

            this.commonfun.presentAlert("Message", this.saveresponse.resposemsg, this.saveresponse.logmsg);
          }
        }
      }, error => {
        this.commonfun.loadingDismiss();
        this.commonfun.presentAlert("Message", "Error!", error.error.text);
      }); //------------------
    } catch (error) {
      // console.log("error:save", error)
      this.commonfun.loadingDismiss();
    }
  }

  getActivityWiseLOGO() {
    try {
      //--------------
      this.commonfun.loadingPresent();
      this.termsOfAgreementService.ActivityWiseLOGO(this.selectedactivity.id).subscribe(data => {
        this.commonfun.loadingDismiss();

        if (data != null) {
          this.saveresponse = data;

          if (this.saveresponse.resposemsg == "success") {
            this.loginservc.logoimgeBase64 = "data:image/jpeg;base64," + this.saveresponse.img;
            this.storage.set('logoimgeBase64', "data:image/jpeg;base64," + this.saveresponse.img); //    this.router.navigateByUrl('/home');
          } else {
            this.storage.set('logoimgeBase64', "");
            this.loginservc.logoimgeBase64 = "";
          }
        }
      }, error => {
        this.commonfun.loadingDismiss();
        this.commonfun.presentAlert("Message", "Error!", error.error.text);
      }); //------------------
    } catch (error) {
      //  console.log("Error1:", error);
      this.commonfun.loadingDismiss();
    }
  }

};

TermsOfAgreementPage.ctorParameters = () => [{
  type: _angular_router__WEBPACK_IMPORTED_MODULE_12__.Router
}, {
  type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__.Storage
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_12__.ActivatedRoute
}, {
  type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_4__.Commonfun
}, {
  type: _terms_of_agreement_terms_of_agreement_service__WEBPACK_IMPORTED_MODULE_5__.TermsOfAgreementService
}, {
  type: _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_9__.File
}, {
  type: _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_8__.FileTransfer
}, {
  type: _awesome_cordova_plugins_file_opener_ngx__WEBPACK_IMPORTED_MODULE_10__.FileOpener
}, {
  type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_6__.LoginauthService
}, {
  type: _awesome_cordova_plugins_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_7__.InAppBrowser
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.Platform
}, {
  type: _provider_message_helper__WEBPACK_IMPORTED_MODULE_11__.Message
}];

TermsOfAgreementPage = (0,tslib__WEBPACK_IMPORTED_MODULE_14__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_15__.Component)({
  selector: 'app-terms-of-agreement',
  template: _terms_of_agreement_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_terms_of_agreement_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], TermsOfAgreementPage);


/***/ }),

/***/ 54531:
/*!******************************************************************!*\
  !*** ./src/app/terms-of-agreement/terms-of-agreement.service.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TermsOfAgreementService": () => (/* binding */ TermsOfAgreementService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);





let TermsOfAgreementService = class TermsOfAgreementService {
    constructor(http, loginauth, genericHTTP) {
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
    }
    getUserActivityAgreementStatus() {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileUserActivityAgreementStatus?'
            + 'userid=' + this.loginauth.userid);
    }
    getWindowAccess(activity_id) {
        let windowAccessAPI = this.loginauth.commonurl + 'ws/in.mbs.webservice.WindowAccess?'
            + 'userid=' + this.loginauth.userid + '&activity=' + activity_id;
        // console.log("Window Access API",windowAccessAPI);
        return this.genericHTTP.get(windowAccessAPI);
    }
    SaveAgreement(id, status, fileid, agreementtype) {
        var jsondatatemp = [{
                "id": id,
                "status": status,
                "user_id": this.loginauth.userid,
                "fileid": fileid,
                "agreementtype": agreementtype
            }];
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileMobileAgreement', jsondatatemp, httpOptions);
    }
    ActivityWiseLOGO(act_id) {
        var jsondata = {
            "act_id": act_id
        };
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.webservice.ActivityWiseLOGO', jsondata, httpOptions);
    }
};
TermsOfAgreementService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_1__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_0__.GenericHttpClientService }
];
TermsOfAgreementService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], TermsOfAgreementService);



/***/ }),

/***/ 38976:
/*!****************************************************************************!*\
  !*** ./src/app/terms-of-agreement/terms-of-agreement.page.scss?ngResource ***!
  \****************************************************************************/
/***/ ((module) => {

module.exports = ".terms-content {\n  margin: 10px;\n  border: thin;\n  border-color: black;\n  border-style: solid;\n  padding: 10px;\n  background-color: rgba(221, 221, 221, 0.8666666667);\n}\n\n.agreement-content {\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n  width: 50%;\n}\n\n@media only screen and (max-width: 768px) {\n  .agreement-content {\n    width: 100%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRlcm1zLW9mLWFncmVlbWVudC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBSUE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsbURBQUE7QUFISjs7QUFNQTtFQUVJLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQUpKOztBQVFBO0VBQ0k7SUFFQSxXQUFBO0VBTkY7QUFDRiIsImZpbGUiOiJ0ZXJtcy1vZi1hZ3JlZW1lbnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW9uLWNvbnRlbnR7XG4vLyAgICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojRERERCAhaW1wb3J0YW50O1xuLy8gfVxuXG4udGVybXMtY29udGVudHtcbiAgICBtYXJnaW46IDEwcHg7XG4gICAgYm9yZGVyOiB0aGluO1xuICAgIGJvcmRlci1jb2xvcjogYmxhY2s7XG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNEREREO1xuICAgIFxufVxuLmFncmVlbWVudC1jb250ZW50XG57XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAgIHdpZHRoOiA1MCU7XG4gICBcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOjc2OHB4KXtcbiAgICAuYWdyZWVtZW50LWNvbnRlbnRcbntcbiAgICB3aWR0aDogMTAwJTtcbn1cbn1cbiAgICBcbiJdfQ== */";

/***/ }),

/***/ 14430:
/*!****************************************************************************!*\
  !*** ./src/app/terms-of-agreement/terms-of-agreement.page.html?ngResource ***!
  \****************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Terms of Agreement</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [ngClass]=\"{'agreement-content' : msg.isplatformweb}\">\n<!-- <div class=\"terms-content\">\n  <p style=\"font-size: small;\">{{text}}</p>\n</div> -->\n\n<ion-card>\n  <ion-card-content>\n    <ion-row>\n      <ion-col>\n        <ion-item>\n          <ion-label position=\"stacked\">Organization Activity<span style=\"color:red!important\">*</span></ion-label>\n          <ion-select [(ngModel)]=\"selectedactivity\" (ionChange)=\"exonActChange()\" interface=\"popover\"\n            multiple=\"false\" placeholder=\"Select Activity\">\n            <ion-select-option *ngFor=\"let activity of activitylist\" [value]=\"activity\">{{activity.name}}\n            </ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n      \n    </ion-row>\n    <div *ngIf=\"iscustomeremp\">\n    <div *ngIf=\"type==='URL'\">\n      <ion-row *ngIf=\"!isalreadyaccepted\" >\n        <ion-col style=\"font-size: small;text-align: left;\" >\n          I agree to the <a (click)=\"inapp()\">Terms of Service</a>  \n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col *ngIf=\"!isalreadyaccepted\" size=\"6\">\n\n        <ion-button color=\"primary\" style=\"width: 98%;\" (click)=\"onclick('Decline')\">Decline</ion-button>\n\n        </ion-col>\n        <ion-col size=\"6\">\n        <ion-button color=\"primary\" style=\"width: 98%;\" [disabled]=\"!isActivitySelected\" (click)=\"onclick('Accept')\">{{txt}}</ion-button>\n\n        </ion-col>\n\n\n    </ion-row>\n    </div>\n    \n    <div *ngIf=\"type==='PDF'\">\n      <ion-row *ngIf=\"termslist.length>0\">\n        <ion-col style=\"font-size: small;text-align: left;\" >\n         List Of Agreements:          \n        </ion-col>\n      </ion-row>\n    <ion-row *ngFor=\"let terms of termslist\">\n      <ion-col style=\"font-size: small;text-align: center;\" size=\"4\">\n        <a (click)=\"inapp(terms.fileid)\">{{terms.name}}</a>\n       </ion-col>\n       <ion-col style=\"font-size: small;text-align: center;\">\n        <ion-button size=\"small\" [disabled]=\"terms.accept\" (click)=\"onclickacceptPdf(terms)\">{{!terms.accept?\"Accept\":\"Accepted\"}}</ion-button>\n       </ion-col>\n      <ion-col style=\"font-size: small;text-align: center;\">\n        <ion-button size=\"small\" [disabled]=\"terms.accept\" (click)=\"onclickRejectPdf(terms)\">{{!terms.reject?\"Decline\":\"Declined\"}}</ion-button>\n       </ion-col>\n    </ion-row>\n    <ion-row *ngIf=\"isallaccepted || termslist.length===0\">\n      \n      <ion-col size=\"6\">\n      <ion-button color=\"primary\" style=\"width: 98%;\"  (click)=\"onclickNext()\">NEXT</ion-button>\n\n      </ion-col>\n  </ion-row>\n  </div>\n  <div *ngIf=\"type!=='PDF' && type!=='URL'\">\n    <ion-row >\n      \n      <ion-col size=\"6\">\n      <ion-button color=\"primary\" [disabled]=\"!isActivitySelected\" style=\"width: 98%;\"  (click)=\"onclickNext()\">NEXT</ion-button>\n\n      </ion-col>\n  </ion-row>\n  </div>\n</div>\n<div *ngIf=\"!iscustomeremp\">\n  <ion-row >\n      \n    <ion-col size=\"6\">\n    <ion-button color=\"primary\" [disabled]=\"!isActivitySelected\" style=\"width: 98%;\"  (click)=\"onclickNext()\">NEXT</ion-button>\n\n    </ion-col>\n</ion-row>\n</div>\n    </ion-card-content>\n  </ion-card>\n\n      \n\n\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_terms-of-agreement_terms-of-agreement_module_ts.js.map