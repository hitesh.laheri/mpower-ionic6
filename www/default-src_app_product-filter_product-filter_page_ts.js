"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["default-src_app_product-filter_product-filter_page_ts"],{

/***/ 90865:
/*!*******************************************************!*\
  !*** ./src/app/product-filter/product-filter.page.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProductFilterPage": () => (/* binding */ ProductFilterPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _product_filter_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./product-filter.page.html?ngResource */ 62991);
/* harmony import */ var _product_filter_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./product-filter.page.scss?ngResource */ 82981);
/* harmony import */ var _product_filter_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./product-filter.service */ 28200);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/provider/commonfun */ 51156);








let ProductFilterPage = class ProductFilterPage {
  constructor(modalCtrl, productFilterService, commonFunction, alertController, navParams) {
    this.modalCtrl = modalCtrl;
    this.productFilterService = productFilterService;
    this.commonFunction = commonFunction;
    this.alertController = alertController;
    this.TAG = "Product Filter Page";
    this.active = 0;
    this.selectedMainIndex = 0;
    this.applyButton = false;
    this.isDataAvailable = false;
    console.log(this.TAG, "Prev Filter Data", this.selectedFilter);
    this.tempSelectedFilter = navParams.get('selectedPreviousFilter');
    this.business_partner = navParams.get('business_partner'); //console.log(this.TAG, "BP",this.business_partner);
  }

  ngOnInit() {
    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {})();
  }

  ionViewWillEnter() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      //console.log(this.TAG,"ionViewWillEnter");
      _this.commonFunction.loadingPresent();

      _this.sideMenuList = yield _this.productFilterService.getSubFilterList(_this.business_partner).toPromise(); // if(!!this.selectedFilter){
      //   this.sideMenuList = this.sideMenuList.concat(this.selectedFilter);
      //   console.log(this.TAG,"Side Menu list After Merge",this.selectedFilter);
      // }

      if (!!_this.tempSelectedFilter) {
        // this.sideMenuList = [ ...this.sideMenuList, ...this.selectedFilter];
        // console.log(this.TAG,"Side Menu list After Merge",this.sideMenuList);
        // this.tempSelectedFilter = this.selectedFilter;
        _this.tempSelectedFilter.forEach((element, index) => {
          _this.sideMenuList.forEach((side_element, sub_menu_main_index) => {
            if (element.catid == side_element.catid) {
              element.catvalues.forEach(cat_ele => {
                side_element.catvalues.forEach((cat_sub_element, cat_index) => {
                  if (cat_ele.subcatid == cat_sub_element.subcatid) {
                    _this.sideMenuList[sub_menu_main_index].catvalues[cat_index].checked = true;
                    console.log("Find");
                  }
                });
              });
            }
          });
        });

        if (!!_this.tempSelectedFilter && _this.tempSelectedFilter.length > 0) {
          _this.selectedFilter = _this.tempSelectedFilter;
          _this.tempSelectedFilter = [];
        }
      } //  console.log(this.TAG,this.sideMenuList);


      if (!!_this.sideMenuList) {
        _this.selectedMainItem = _this.sideMenuList[0];
        _this.isDataAvailable = true;
      }

      _this.commonFunction.loadingDismiss();
    })();
  }

  clearFilter() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const alert = yield _this2.alertController.create({
          header: 'Confirm!',
          message: 'Would you like to clear the all filters?',
          buttons: [{
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: blah => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Okay',
            handler: () => {
              console.log('Confirm Okay');
              _this2.selectedFilter = [];
              _this2.tempSelectedFilter = [];

              _this2.ionViewWillEnter();
            }
          }]
        });
        yield alert.present();
      } catch (error) {
        console.log(_this2.TAG, error);
      }
    })();
  }

  applyFilter() {
    try {
      if (Array.isArray(this.selectedFilter) && this.selectedFilter.length) {
        this.modalCtrl.dismiss(this.selectedFilter);
      } else {
        this.commonFunction.presentAlert("Product Filter", "Validation", "Please select at least one filter");
      }
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  menuItemClick(selectedItem, index) {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        //console.log(this.TAG,selectedItem);
        _this3.selectedMainItem = selectedItem;
        _this3.active = index;
        _this3.selectedMainIndex = index;
      } catch (error) {
        console.log(_this3.TAG, error);
      }
    })();
  }

  chkSubSelectFilter(subData, checked, event) {
    try {
      //console.log(this.TAG,subData);
      if (!!this.tempSelectedFilter && this.tempSelectedFilter.length > 0) {
        this.selectedFilter = this.tempSelectedFilter;
        this.tempSelectedFilter = [];
      }

      console.log(this.TAG, checked);

      if (checked == undefined || checked == null) {
        checked = true;
      } else {
        checked = false;
      }

      if (!!this.selectedFilter) {
        if (checked) {
          this.applyButton = true;
          let data = this.selectedFilter.find(ob => ob.catid === this.selectedMainItem.catid);

          if (data === null || data === undefined) {
            this.selectedFilter.push({
              catid: this.selectedMainItem.catid,
              catname: this.selectedMainItem.catname,
              catvalues: [{
                "subcatid": subData.subcatid,
                "subcatname": subData.subcatname,
                columnname: subData.columnname,
                "checked": true
              }]
            });
          } else {
            let index = this.selectedFilter.findIndex(ob => ob.catid === this.selectedMainItem.catid);
            this.selectedFilter[index].catvalues.push({
              "subcatid": subData.subcatid,
              "subcatname": subData.subcatname,
              columnname: subData.columnname,
              "checked": true
            });
          }
        } else {
          let main_index = this.selectedFilter.findIndex(ob => ob.catid === this.selectedMainItem.catid);
          this.selectedFilter[main_index].catvalues.forEach((value, index) => {
            if (value.subcatid == subData.subcatid) {
              //console.log("selected",this.selectedFilter[index]);
              if (this.selectedFilter[main_index].catvalues.length == 1) {
                this.selectedFilter.splice(main_index, 1);
              } else {
                this.selectedFilter[main_index].catvalues.splice(index, 1);
              }
            }
          });
        }
      } else {
        let temp = [{
          catid: this.selectedMainItem.catid,
          catname: this.selectedMainItem.catname,
          catvalues: [{
            "subcatid": subData.subcatid,
            "subcatname": subData.subcatname,
            columnname: subData.columnname,
            "checked": true
          }]
        }];
        this.selectedFilter = temp;
        this.applyButton = true;
      } //console.log(this.TAG,"Filter List IS Updated",this.selectedFilter);

    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss(this.selectedFilter);
  }

  onFilterSearch() {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  }

};

ProductFilterPage.ctorParameters = () => [{
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ModalController
}, {
  type: _product_filter_service__WEBPACK_IMPORTED_MODULE_3__.ProductFilterService
}, {
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__.Commonfun
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.AlertController
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavParams
}];

ProductFilterPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
  selector: 'app-product-filter',
  template: _product_filter_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_product_filter_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ProductFilterPage);


/***/ }),

/***/ 28200:
/*!**********************************************************!*\
  !*** ./src/app/product-filter/product-filter.service.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProductFilterService": () => (/* binding */ ProductFilterService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var _common_Constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/Constants */ 68209);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);







let ProductFilterService = class ProductFilterService {
    constructor(genericHttpClientService, loginService, commonFunction, httpClient) {
        this.genericHttpClientService = genericHttpClientService;
        this.loginService = loginService;
        this.commonFunction = commonFunction;
        this.httpClient = httpClient;
        this.TAG = "Product Filter Service";
    }
    getSubFilterList(business_partner) {
        try {
            //return this.httpClient.get('assets/data/price.json');
            let getFilterURL = _common_Constants__WEBPACK_IMPORTED_MODULE_1__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.DynamicFilterAsPerActivity?' +
                this.loginService.parameter + '&user_id=' + this.loginService.userid +
                '&activityid=' + this.loginService.selectedactivity.id + '&bpartner_id=' + business_partner.id;
            console.log("Get Filter URL", getFilterURL);
            return this.genericHttpClientService.get(getFilterURL);
        }
        catch (error) {
            console.log(this.TAG, error);
        }
    }
};
ProductFilterService.ctorParameters = () => [
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_2__.GenericHttpClientService },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__.LoginauthService },
    { type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_0__.Commonfun },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpClient }
];
ProductFilterService = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Injectable)({
        providedIn: 'root'
    })
], ProductFilterService);



/***/ }),

/***/ 82981:
/*!********************************************************************!*\
  !*** ./src/app/product-filter/product-filter.page.scss?ngResource ***!
  \********************************************************************/
/***/ ((module) => {

module.exports = ".box {\n  height: 100% !important;\n  display: flex;\n  flex-flow: column;\n}\n\n.list-item {\n  margin-top: 15px !important;\n}\n\n.row-custom {\n  height: 100% !important;\n}\n\n.no-scroll {\n  --overflow: hidden;\n}\n\n.footer-back-color {\n  background-color: white;\n}\n\n.footer-btn-color {\n  background-color: #F39E20 !important;\n}\n\n.custom-activated {\n  --ion-background-color: white !important;\n}\n\n.ion-item-custom {\n  --ion-background-color:#eeeeee !important;\n  padding: 0px !important;\n}\n\n.back-color {\n  background-color: #eeeeee !important;\n}\n\n.custom-ion-col {\n  padding-top: 40px !important;\n  background-color: #eeeeee !important;\n  padding-right: 0px !important;\n  padding-left: 0px !important;\n}\n\n.custom-content {\n  --background: auto !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2R1Y3QtZmlsdGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLHVCQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0FBQUo7O0FBR0E7RUFDSSwyQkFBQTtBQUFKOztBQUVBO0VBQ0ksdUJBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0o7O0FBQ0E7RUFDSSx1QkFBQTtBQUVKOztBQUFBO0VBQ0ksb0NBQUE7QUFHSjs7QUFEQTtFQUNJLHdDQUFBO0FBSUo7O0FBRkE7RUFDSSx5Q0FBQTtFQUNBLHVCQUFBO0FBS0o7O0FBSEE7RUFDSSxvQ0FBQTtBQU1KOztBQUhBO0VBQ0ksNEJBQUE7RUFDQSxvQ0FBQTtFQUNBLDZCQUFBO0VBQ0EsNEJBQUE7QUFNSjs7QUFKQTtFQUNHLDZCQUFBO0FBT0giLCJmaWxlIjoicHJvZHVjdC1maWx0ZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4uYm94e1xuICAgIGhlaWdodDogMTAwJSAhaW1wb3J0YW50OyBcbiAgICBkaXNwbGF5OiBmbGV4OyBcbiAgICBmbGV4LWZsb3c6IGNvbHVtbjtcbiAgIFxufSBcbi5saXN0LWl0ZW17XG4gICAgbWFyZ2luLXRvcDogMTVweCAhaW1wb3J0YW50O1xufVxuLnJvdy1jdXN0b217XG4gICAgaGVpZ2h0OjEwMCUgIWltcG9ydGFudDtcbn1cblxuLm5vLXNjcm9sbCB7XG4gICAgLS1vdmVyZmxvdzogaGlkZGVuO1xufVxuLmZvb3Rlci1iYWNrLWNvbG9ye1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuLmZvb3Rlci1idG4tY29sb3J7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0YzOUUyMCAhaW1wb3J0YW50O1xufVxuLmN1c3RvbS1hY3RpdmF0ZWQge1xuICAgIC0taW9uLWJhY2tncm91bmQtY29sb3IgOiB3aGl0ZSAhaW1wb3J0YW50O1xufVxuLmlvbi1pdGVtLWN1c3RvbXtcbiAgICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiNlZWVlZWUgIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cbi5iYWNrLWNvbG9ye1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNlZWVlZWUgIWltcG9ydGFudDtcbn1cblxuLmN1c3RvbS1pb24tY29se1xuICAgIHBhZGRpbmctdG9wOjQwcHggIWltcG9ydGFudDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlZWVlICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZy1yaWdodDogMHB4ICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZy1sZWZ0OiAwcHggIWltcG9ydGFudDtcbn1cbi5jdXN0b20tY29udGVudCB7XG4gICAtLWJhY2tncm91bmQ6IGF1dG8gIWltcG9ydGFudDtcbn1cbi8vIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogNzY4cHgpIGFuZCAobWluLWhlaWdodDogNzY4cHgpe1xuLy8gICAgLm1haW4tc2lkZXtcbi8vICAgICAgICBtYXJnaW4tdG9wOiAzMHB4O1xuLy8gICAgfVxuLy8gICB9XG4iXX0= */";

/***/ }),

/***/ 62991:
/*!********************************************************************!*\
  !*** ./src/app/product-filter/product-filter.page.html?ngResource ***!
  \********************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Product Filter</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-button (click)=\"dismiss()\">Close</ion-button>\n    </ion-buttons>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"clearFilter()\">Clear Filters</ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content class=\"no-scroll\">\n  <ion-grid class=\"box\" no-padding>\n    <ion-row class=\"row-custom\">\n      <ion-col size=\"4\" class=\"back-color custom-ion-col\">\n        <ion-content class=\"back-color custom-content\">\n          <section class=\"back-color\">\n            <ion-list *ngFor=\"let filterItem of sideMenuList;let i = index \" class=\"back-color\">\n              <ion-item lines=\"none\" class=\"list-item\" (click)=\"menuItemClick(filterItem,i)\" [ngClass]=\"{'custom-activated': (active == i),'ion-item-custom':(active != i)}\">\n                <ion-label color=\"primary\">\n                  {{ filterItem.catname }}\n                </ion-label>\n              </ion-item>\n            </ion-list>\n          </section>\n        </ion-content>\n     </ion-col>\n     <ion-col size=\"8\">\n      <ion-content *ngIf=\"isDataAvailable\">\n        <ion-searchbar animated [debounce]=\"1000\" showCancelButton=\"focus\" [(ngModel)]=\"searchText\"></ion-searchbar>\n        <ion-list *ngFor=\"let subData of sideMenuList[selectedMainIndex].catvalues | filter:searchText;let i=index\">\n          <ion-item lines=\"none\" text-wrap>\n            <ion-label>{{subData.subcatname}}</ion-label>\n            <ion-checkbox slot=\"start\"  checked=\"false\" value=\"subData.sub_name\" (click)=\"chkSubSelectFilter(subData,subData.checked,$event)\" [(ngModel)]=\"subData.checked\"></ion-checkbox>\n          </ion-item>\n        </ion-list>\n    </ion-content>\n    </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n<ion-footer class=\"footer-back-color\">\n  <ion-button  no-margin (click)=\"applyFilter()\"  color=\"primary\"  size=\"large\" expand=\"full\" fill=\"outline\" class=\"footer-btn-color\">\n    <ion-label style=\"color: white;\">\n      Apply\n    </ion-label>\n  </ion-button>\n</ion-footer>\n";

/***/ })

}]);
//# sourceMappingURL=default-src_app_product-filter_product-filter_page_ts.js.map