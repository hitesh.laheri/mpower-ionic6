"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_addedit-travel-plan_addedit-travel-plan_module_ts"],{

/***/ 95918:
/*!*******************************************************************!*\
  !*** ./src/app/addedit-travel-plan/addedit-travel-plan.module.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddeditTravelPlanPageModule": () => (/* binding */ AddeditTravelPlanPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _addedit_travel_plan_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./addedit-travel-plan.page */ 5777);








const routes = [
    {
        path: '',
        component: _addedit_travel_plan_page__WEBPACK_IMPORTED_MODULE_0__.AddeditTravelPlanPage
    }
];
let AddeditTravelPlanPageModule = class AddeditTravelPlanPageModule {
};
AddeditTravelPlanPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule, ionic_selectable__WEBPACK_IMPORTED_MODULE_5__.IonicSelectableModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterModule.forChild(routes)
        ],
        declarations: [_addedit_travel_plan_page__WEBPACK_IMPORTED_MODULE_0__.AddeditTravelPlanPage]
    })
], AddeditTravelPlanPageModule);



/***/ }),

/***/ 5777:
/*!*****************************************************************!*\
  !*** ./src/app/addedit-travel-plan/addedit-travel-plan.page.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddeditTravelPlanPage": () => (/* binding */ AddeditTravelPlanPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _addedit_travel_plan_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./addedit-travel-plan.page.html?ngResource */ 2920);
/* harmony import */ var _addedit_travel_plan_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./addedit-travel-plan.page.scss?ngResource */ 64970);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _travel_plan_travel_plan_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../travel-plan/travel-plan.service */ 85276);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! date-fns */ 86527);










let AddeditTravelPlanPage = class AddeditTravelPlanPage {
    constructor(fb, commonfun, router, route, travelplanservice, loginauth) {
        this.fb = fb;
        this.commonfun = commonfun;
        this.router = router;
        this.route = route;
        this.travelplanservice = travelplanservice;
        this.loginauth = loginauth;
        this.TAG = "Add Edit Travel Plan Page";
        this.selectedleadskm = [];
        this.offset = 0;
        this.strsearch = "";
        this.index = null;
        this.isfirst = false;
        this.islast = false;
        this.datefrom = '';
        this.getrout();
        this.formaddplan = this.fb.group({
            selectedlead: [],
            searchlead: [],
        });
    }
    ngOnInit() {
    }
    formatDate(value) {
        return (0,date_fns__WEBPACK_IMPORTED_MODULE_5__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_6__["default"])(value), 'dd-MM-yyyy');
    }
    onsearch() {
        var srchkey = this.formaddplan.controls["searchlead"].value;
        this.strsearch = srchkey;
        this.offset = 0;
        if (srchkey.length % 3 == 0 || srchkey == '') {
            if (this.selectedLead == null) {
                this.getsalesperson();
            }
            else {
                this.bindNearestPerson();
            }
        }
    }
    getsalesperson() {
        let methodTAG = 'getsalesperson';
        try {
            this.commonfun.loadingPresent();
            this.travelplanservice.getUserWiseSalesPerson(this.offset, this.strsearch, "N", "", "", this.outOrderChkCtrlValueAddEdit)
                .subscribe(data => {
                this.commonfun.loadingDismiss();
                var response = data;
                if (response.AddressList.length > 0) {
                    this.leadskm = response.AddressList;
                }
                else {
                    if (this.offset > 0)
                        this.offset = this.offset - 10;
                    if (this.strsearch != "")
                        this.leadskm = response.AddressList;
                }
            }, error => {
                console.log("YES ERROR CATCH", error);
                this.commonfun.loadingDismiss();
                this.commonfun.presentAlert("Message", "Error", error.error);
            });
        }
        catch (error) {
            this.commonfun.loadingDismiss();
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    onPrevious() {
        if (this.offset > 1) {
            this.offset = this.offset - 10;
            if (this.selectedLead == null) {
                this.getsalesperson();
            }
            else {
                this.bindNearestPerson();
            }
        }
    }
    onNext() {
        this.offset = this.offset + 10;
        if (this.selectedLead == null) {
            this.getsalesperson();
        }
        else {
            this.bindNearestPerson();
        }
    }
    bindNearestPerson() {
        try {
            this.travelplanservice.getSearchNearestPersoni(this.selectedLead.addressid, this.offset, this.strsearch, this.outOrderChkCtrlValueAddEdit).subscribe(data => {
                var response = data;
                if (response.AddressList.length > 0) {
                    this.leadskm = response.AddressList;
                }
                else {
                    if (this.offset > 0)
                        this.offset = this.offset - 10;
                    if (this.strsearch != "")
                        this.leadskm = response.AddressList;
                }
            });
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    onAddLead(item) {
        try {
            if (this.selectedleadskm) {
                if (this.selectedleadskm.some(i => i.addressid === item.addressid)) {
                    this.commonfun.presentAlert("Message", "Alert", "Lead already exists.");
                }
                else {
                    var slitem = { "custORbpatnerID": item.custORbpatnerID, "line": this.selectedleadskm.length + 1, "custname": item.custname, "addressid": item.addressid, "addressname": item.addressname, "pincode": item.pincode, "day": "0", "date": null, "latitude": item.latitude, "longitude": item.longitude, "km": item.km, "status": "Plan", "show": "false" };
                    if (this.index == null) {
                        this.selectedleadskm.push(slitem);
                    }
                    else {
                        this.selectedleadskm.splice(this.index + 1, 0, slitem);
                    }
                    this.onSave();
                }
            }
            else {
                var slitem1 = [{ "custORbpatnerID": item.custORbpatnerID, "line": "1", "custname": item.custname, "addressid": item.addressid, "addressname": item.addressname, "pincode": item.pincode, "day": "0", "date": null, "latitude": item.latitude, "longitude": item.longitude, "km": item.km, "status": "Plan", "show": "false" }];
                this.selectedleadskm = slitem1;
                this.onSave();
            }
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    getrout() {
        try {
            this.route.params.subscribe(params => {
                if (this.router.getCurrentNavigation().extras.state) {
                    this.selectedLead = this.router.getCurrentNavigation().extras.state.selectedLead;
                    this.selectedleadskm = this.router.getCurrentNavigation().extras.state.allleads;
                    this.fromdate = this.router.getCurrentNavigation().extras.state.fromdate;
                    this.todate = this.router.getCurrentNavigation().extras.state.todate;
                    this.index = this.router.getCurrentNavigation().extras.state.index;
                    this.outOrderChkCtrlValueAddEdit = this.router.getCurrentNavigation().extras.state.outOrderChkCtrl;
                    console.log(this.TAG, "getrout", this.outOrderChkCtrlValueAddEdit);
                    if (this.selectedLead == null) {
                        this.leadskm = this.router.getCurrentNavigation().extras.state.first10leads;
                    }
                    else {
                        this.bindNearestPerson();
                    }
                }
            });
        }
        catch (error) {
            //console.log("addsublead()-ERROR:",error);
        }
    }
    Resetpage() {
    }
    removeLeads(post) {
        try {
            // console.log("removeLeads()");
            let index = this.selectedleadskm.indexOf(post);
            const result = this.selectedleadskm.filter(item => item != post);
            this.selectedleadskm = result;
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    onSave() {
        try {
            let navigationExtras = {
                state: {
                    selectedddlsubleads: this.selectedleadskm,
                    selectedLead: this.selectedLead
                }
            };
            this.router.navigate(['travel-plan'], navigationExtras);
        }
        catch (error) {
        }
    }
};
AddeditTravelPlanPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormBuilder },
    { type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_3__.Commonfun },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.ActivatedRoute },
    { type: _travel_plan_travel_plan_service__WEBPACK_IMPORTED_MODULE_4__.TravelPlanService },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService }
];
AddeditTravelPlanPage = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
        selector: 'app-addedit-travel-plan',
        template: _addedit_travel_plan_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_addedit_travel_plan_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], AddeditTravelPlanPage);



/***/ }),

/***/ 64970:
/*!******************************************************************************!*\
  !*** ./src/app/addedit-travel-plan/addedit-travel-plan.page.scss?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

module.exports = "ion-scroll[scrollX] {\n  white-space: nowrap;\n  overflow: visible;\n  overflow-y: auto;\n}\nion-scroll[scrollX] .scroll-item {\n  display: inline-block;\n}\nion-scroll[scrollX] .selectable-icon {\n  margin: 10px 20px 10px 20px;\n  color: red;\n  font-size: 100px;\n}\nion-scroll[scrollX] ion-avatar img {\n  margin: 10px;\n}\nion-scroll[scroll-avatar] {\n  height: 60px;\n}\n/* Hide ion-content scrollbar */\n::-webkit-scrollbar {\n  display: none;\n}\nh5 {\n  font-style: oblique;\n  color: darkcyan;\n  font-size: large;\n}\nh5 ion-icon {\n  color: lightcoral;\n}\n.inputfile {\n  color: transparent;\n}\n.forecast_container {\n  overflow-x: scroll !important;\n  overflow-x: visible !important;\n  overflow-y: hidden;\n  word-wrap: break-word;\n  height: 20vw;\n  font-size: 0.8em;\n  font-weight: 300;\n}\n.grid-header {\n  font-weight: bold;\n}\n.forecast_div {\n  overflow-x: scroll !important;\n  overflow-x: visible !important;\n  overflow-y: hidden;\n  word-wrap: break-word;\n  font-size: 0.8em;\n  font-weight: 300;\n  max-width: 175px;\n}\nion-popover {\n  --width: 320px;\n}\nion-popover.dateTimePopover {\n  --offset-y: -350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZGVkaXQtdHJhdmVsLXBsYW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFFQSxpQkFBQTtFQUNBLGdCQUFBO0FBQUo7QUFFSTtFQUNFLHFCQUFBO0FBQU47QUFHSTtFQUNFLDJCQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0FBRE47QUFJSTtFQUNFLFlBQUE7QUFGTjtBQU1FO0VBQ0UsWUFBQTtBQUhKO0FBTUUsK0JBQUE7QUFDQTtFQUNFLGFBQUE7QUFISjtBQU1FO0VBQ0UsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFISjtBQUlJO0VBQ0UsaUJBQUE7QUFGTjtBQU9FO0VBQ0Usa0JBQUE7QUFKSjtBQU9FO0VBQ0UsNkJBQUE7RUFDQSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQUpKO0FBTUE7RUFDRSxpQkFBQTtBQUhGO0FBS0E7RUFDRSw2QkFBQTtFQUNBLDhCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQUZGO0FBSUE7RUFDRSxjQUFBO0FBREY7QUFHQTtFQUNFLDZCQUFBO0FBQUYiLCJmaWxlIjoiYWRkZWRpdC10cmF2ZWwtcGxhbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tc2Nyb2xsW3Njcm9sbFhdIHtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgLy8gaGVpZ2h0OiAxMjBweDtcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xuLy8gd2lkdGg6MTAwJTtcbiAgICAuc2Nyb2xsLWl0ZW0ge1xuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIH1cblxuICAgIC5zZWxlY3RhYmxlLWljb257XG4gICAgICBtYXJnaW46IDEwcHggMjBweCAxMHB4IDIwcHg7XG4gICAgICBjb2xvcjogcmVkO1xuICAgICAgZm9udC1zaXplOiAxMDBweDtcbiAgICB9XG5cbiAgICBpb24tYXZhdGFyIGltZ3tcbiAgICAgIG1hcmdpbjogMTBweDtcbiAgICB9XG4gIH1cblxuICBpb24tc2Nyb2xsW3Njcm9sbC1hdmF0YXJde1xuICAgIGhlaWdodDogNjBweDtcbiAgfVxuXG4gIC8qIEhpZGUgaW9uLWNvbnRlbnQgc2Nyb2xsYmFyICovXG4gIDo6LXdlYmtpdC1zY3JvbGxiYXJ7XG4gICAgZGlzcGxheTpub25lO1xuICB9XG5cbiAgaDV7XG4gICAgZm9udC1zdHlsZTogb2JsaXF1ZTtcbiAgICBjb2xvcjogZGFya2N5YW47XG4gICAgZm9udC1zaXplOiBsYXJnZTtcbiAgICBpb24taWNvbntcbiAgICAgIGNvbG9yOiBsaWdodGNvcmFsO1xuICAgIH1cblxuICB9XG4gIFxuICAuaW5wdXRmaWxlIHtcbiAgICBjb2xvcjogdHJhbnNwYXJlbnQ7XG4gIH1cblxuICAuZm9yZWNhc3RfY29udGFpbmVye1xuICAgIG92ZXJmbG93LXg6IHNjcm9sbCFpbXBvcnRhbnQ7XG4gICAgb3ZlcmZsb3cteDogdmlzaWJsZSFpbXBvcnRhbnQ7XG4gICAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICAgIHdvcmQtd3JhcDogYnJlYWstd29yZDtcbiAgICBoZWlnaHQ6MjB2dztcbiAgICBmb250LXNpemU6MC44ZW07XG4gICAgZm9udC13ZWlnaHQ6MzAwO1xufVxuLmdyaWQtaGVhZGVye1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5mb3JlY2FzdF9kaXZ7XG4gIG92ZXJmbG93LXg6IHNjcm9sbCFpbXBvcnRhbnQ7XG4gIG92ZXJmbG93LXg6IHZpc2libGUhaW1wb3J0YW50O1xuICBvdmVyZmxvdy15OiBoaWRkZW47XG4gIHdvcmQtd3JhcDogYnJlYWstd29yZDtcbiAgZm9udC1zaXplOjAuOGVtO1xuICBmb250LXdlaWdodDozMDA7XG4gIG1heC13aWR0aDogMTc1cHg7XG59XG5pb24tcG9wb3ZlciB7XG4gIC0td2lkdGg6IDMyMHB4O1xufVxuaW9uLXBvcG92ZXIuZGF0ZVRpbWVQb3BvdmVyIHtcbiAgLS1vZmZzZXQteTogLTM1MHB4ICFpbXBvcnRhbnQ7XG4gIH0iXX0= */";

/***/ }),

/***/ 2920:
/*!******************************************************************************!*\
  !*** ./src/app/addedit-travel-plan/addedit-travel-plan.page.html?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Add Customer</ion-title>\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card>\n    <ion-card-content>\n      <ion-row>\n        <ion-col>\n          <ion-item>\n            <ion-label position=\"floating\">Search Customer</ion-label>\n            <ion-input formControlName=\"searchlead\" (ionChange)=\"onsearch()\" type=\"text\">\n            </ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-row *ngIf=\"leadskm\">\n        <ion-col>\n          <ion-item float-left lines=\"none\">\n            <ion-button slot=\"start\" color=\"primary\" text-center [disabled]=\"isfirst\" (click)=\"onPrevious()\">Previous\n            </ion-button>\n          </ion-item>\n        </ion-col>\n        <ion-col>\n          <ion-item float-right lines=\"none\">\n            <ion-button slot=\"end\" color=\"primary\" [disabled]=\"islast\" text-center (click)=\"onNext()\">\n              &nbsp;&nbsp;&nbsp;&nbsp;Next&nbsp;&nbsp;&nbsp;&nbsp;</ion-button>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-item *ngFor=\"let item of leadskm; index as i\" text-wrap style=\"font-size: small;\">\n        <!-- <ion-content scrollX=\"true\"> -->\n        <div style=\"width: 100%;\">\n          <ion-row>\n            <ion-col size=\"2\" (click)=\"onAddLead(item)\">\n              <ion-icon ios=\"ios-add-circle\" md=\"md-add-circle\" style=\"font-size: x-large;\">\n              </ion-icon>\n            </ion-col>\n            <ion-col size=\"8\">\n              <ion-label style=\"white-space: normal\">\n                {{ item.custname }}\n              </ion-label>\n              <ion-label style=\"font-size: small;\">Address: {{ item.addressname }}</ion-label>\n            </ion-col>\n            <ion-col size=\"2\">\n              <ion-label style=\"white-space: normal\">\n                {{ item.km}} km\n              </ion-label>\n            </ion-col>\n          </ion-row>\n        </div>\n        <!-- </ion-content> -->\n      </ion-item>\n      <ion-row *ngIf=\"leadskm\">\n        <ion-col>\n          <ion-item float-left lines=\"none\">\n            <ion-button slot=\"start\" color=\"primary\" text-center [disabled]=\"isfirst\" (click)=\"onPrevious()\">Previous\n            </ion-button>\n          </ion-item>\n        </ion-col>\n        <ion-col>\n          <ion-item float-right lines=\"none\">\n            <ion-button slot=\"end\" color=\"primary\" text-center [disabled]=\"islast\" (click)=\"onNext()\">\n              &nbsp;&nbsp;&nbsp;&nbsp;Next&nbsp;&nbsp;&nbsp;&nbsp;</ion-button>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <h5 ion-text class=\"text-primary\">\n            <ion-icon name=\"bookmarks\"></ion-icon>Selected Customers:\n          </h5>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <div style=\"overflow-x:auto\">\n          <ion-grid>\n            <ion-row nowrap>\n              <ion-col nowrap>\n                <ion-row nowrap>\n                  <ion-col col-2 size=\"5\" class=\"grid-header\">Line Number</ion-col>\n                  <ion-col size=\"6\" class=\"grid-header\">Customer</ion-col>\n                  <ion-col size=\"6\" class=\"grid-header\">Address</ion-col>\n                  <ion-col size=\"2\" class=\"grid-header\">Day</ion-col>\n                  <ion-col size=\"6\" class=\"grid-header\" style=\"padding-left: 20px;\">Date</ion-col>\n                  <!-- <ion-col size=\"5\" class=\"grid-header\">Address Lat</ion-col> -->\n                  <!-- <ion-col size=\"5\" class=\"grid-header\">Address Long</ion-col> -->\n                  <ion-col size=\"4\" class=\"grid-header\">KM</ion-col>\n                  <ion-col size=\"3\" class=\"grid-header\">Status</ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n            <ion-row *ngFor=\"let data of selectedleadskm; index as i\" nowrap>\n              <ion-col nowrap>\n                <ion-row nowrap>\n                  <ion-col size=\"1\" style=\"width: 100%; text-align: right;\">\n                    <ion-icon name=\"trash\" (click)=\"removeLeads(data)\" style=\"font-size: x-large;\n          color: red;\"></ion-icon>\n                  </ion-col>\n                  <ion-col size=\"4\" class=\"forecast_div\">{{i+1}}</ion-col>\n                  <ion-col size=\"6\" class=\"forecast_div\">{{data.custname}}</ion-col>\n                  <ion-col size=\"6\" class=\"forecast_div\">{{data.addressname}}</ion-col>\n                  <ion-col size=\"2\" class=\"forecast_div\">{{data.day}}</ion-col>\n                  <!-- <ion-col size=\"6\" class=\"forecast_div\">{{data.date}}</ion-col> -->\n                  <ion-col size=\"6\" class=\"forecast_div\" style=\"padding-left: -10px !important;\">\n                    <!-- <ion-datetime placeholder=\"Select Date\" [(ngModel)]=\"data.date\"\n                      [ngModelOptions]=\"{standalone: true}\" displayFormat=\"DD-MM-YYYY\" readOnly\n                      style=\"margin-top: -10px;\"></ion-datetime> -->\n                      <ion-item>\n                        <ion-input placeholder=\"Select Date\" [value]=\"datefrom\"></ion-input>\n                        <ion-button fill=\"clear\" id=\"open-date-input-1\">\n                          <ion-icon icon=\"calendar\"></ion-icon>\n                        </ion-button>\n                        <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\n                          <ng-template>\n                            <ion-datetime\n                              #popoverDatetime2\n                              [(ngModel)]=\"data.date\"\n                              [ngModelOptions]=\"{standalone: true}\"\n                              presentation=\"date\"\n                              showDefaultButtons=\"true\"\n                              (ionChange)=\"datefrom = formatDate(popoverDatetime2.value)\"\n                              style=\"margin-top: -10px;\" readOnly\n                            ></ion-datetime>\n                          </ng-template>\n                        </ion-popover>\n                      </ion-item>\n\n                  </ion-col>\n                  <!-- <ion-col size=\"5\" class=\"forecast_div\">{{data.latitude}}</ion-col> -->\n                  <!-- <ion-col size=\"5\" class=\"forecast_div\">{{data.longitude}}</ion-col> -->\n                  <ion-col size=\"4\" class=\"forecast_div\">{{data.km}}</ion-col>\n                  <ion-col size=\"3\" class=\"forecast_div\">{{data.status}}</ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </div>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n  <ion-button expand=\"block\" class=\"ion-margin-start ion-margin-end ion-margin-bottom btn-scheme\" type=\"submit\"\n    [disabled]=\"!formaddplan.valid\" (click)=\"onSave()\">\n    Save\n  </ion-button>\n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_addedit-travel-plan_addedit-travel-plan_module_ts.js.map