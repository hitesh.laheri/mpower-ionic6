"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_use-vetcoins-redemption_use-vetcoins-redemption_module_ts"],{

/***/ 25704:
/*!***************************************************************************!*\
  !*** ./src/app/use-vetcoins-redemption/use-vetcoins-redemption.module.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UseVetcoinsRedemptionPageModule": () => (/* binding */ UseVetcoinsRedemptionPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _use_vetcoins_redemption_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./use-vetcoins-redemption.page */ 65168);







const routes = [
    {
        path: '',
        component: _use_vetcoins_redemption_page__WEBPACK_IMPORTED_MODULE_0__.UseVetcoinsRedemptionPage
    }
];
let UseVetcoinsRedemptionPageModule = class UseVetcoinsRedemptionPageModule {
};
UseVetcoinsRedemptionPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_use_vetcoins_redemption_page__WEBPACK_IMPORTED_MODULE_0__.UseVetcoinsRedemptionPage]
    })
], UseVetcoinsRedemptionPageModule);



/***/ }),

/***/ 65168:
/*!*************************************************************************!*\
  !*** ./src/app/use-vetcoins-redemption/use-vetcoins-redemption.page.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UseVetcoinsRedemptionPage": () => (/* binding */ UseVetcoinsRedemptionPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _use_vetcoins_redemption_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./use-vetcoins-redemption.page.html?ngResource */ 37221);
/* harmony import */ var _use_vetcoins_redemption_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./use-vetcoins-redemption.page.scss?ngResource */ 94570);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _use_vetcoins_use_vetcoins_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../use-vetcoins/use-vetcoins.service */ 6730);









let UseVetcoinsRedemptionPage = class UseVetcoinsRedemptionPage {
    constructor(loginauth, router, route, commonfun, usevetcoinsservice, _ngZone) {
        this.loginauth = loginauth;
        this.router = router;
        this.route = route;
        this.commonfun = commonfun;
        this.usevetcoinsservice = usevetcoinsservice;
        this._ngZone = _ngZone;
    }
    ngOnInit() {
        this.getparam();
    }
    getparam() {
        try {
            this.route.params.subscribe(params => {
                if (this.router.getCurrentNavigation().extras.state.Amount) {
                    this.currentamount = this.router.getCurrentNavigation().extras.state.Amount;
                    this.product = this.router.getCurrentNavigation().extras.state.Ispurchaseproduct;
                    this.vet = this.router.getCurrentNavigation().extras.state.Isvetfees;
                    this.vetcoinsused = this.router.getCurrentNavigation().extras.state.Balamount;
                    this.tocust = this.router.getCurrentNavigation().extras.state.tocust;
                    this.fromcust = this.router.getCurrentNavigation().extras.state.fromcust;
                    this.apiotp = this.router.getCurrentNavigation().extras.state.otp;
                    this.orderredeemper = this.router.getCurrentNavigation().extras.state.orderredeemper;
                    this.vetcoinuse = this.router.getCurrentNavigation().extras.state.vetcoinuse;
                    this.redeemlimit = this.router.getCurrentNavigation().extras.state.redeemlimit;
                    this.firmname = this.router.getCurrentNavigation().extras.state.firmname;
                    // this.currentamount=((amt*90)/100);
                    var orderamt = ((this.currentamount) * (this.orderredeemper) / 100);
                    // var orderamt=((this.currentamount)*(this.orderredeemper)/100)
                    this.RedemptionAmount = (orderamt < this.vetcoinuse ? orderamt : this.vetcoinuse);
                }
            });
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    Resetpage() {
        this.userotp = "";
    }
    onUseConfirm() {
        try {
            // this.tocust="";
            // this.fromcust="";
            if (this.userotp == this.apiotp) {
                this.description = (this.product == true ? "POP" : "POS");
                this.bal = this.vetcoinsused;
                this.rewardlimit = this.vetcoinuse;
                this.rewardtransfer = this.RedemptionAmount;
                // this.redeemlimit=this.RedemptionAmount;
                this.ordvalue = this.currentamount;
                this.usevetcoinsservice.VetCoinRewardTranstn(this.tocust, this.fromcust, this.description, this.bal, this.rewardlimit, this.rewardtransfer, this.redeemlimit, this.ordvalue).subscribe(data => {
                    // 
                    this.resp = data;
                    if (this.resp.status == "Success") {
                        this.commonfun.presentAlert("Message", "Success", this.resp.msg);
                        let navigationExtras = {
                            state: {
                                reset: true,
                            }
                        };
                        //  this.usevetcoinspage.Resetpage();
                        this.router.navigate(['/use-vetcoins'], navigationExtras);
                        // this._ngZone.run(() => {
                        //   this.router.navigate(['/use-vetcoins']);
                        // });
                    }
                    else {
                        this.commonfun.presentAlert("Message", "Error", "Error.");
                    }
                }, error => {
                    this.commonfun.presentAlert("Message", "Error", error.error.text);
                });
            }
            else {
                this.commonfun.presentAlert("Message", "Error", "OTP not matched.");
            }
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
};
UseVetcoinsRedemptionPage.ctorParameters = () => [
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.ActivatedRoute },
    { type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_3__.Commonfun },
    { type: _use_vetcoins_use_vetcoins_service__WEBPACK_IMPORTED_MODULE_4__.UseVetcoinsService },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.NgZone }
];
UseVetcoinsRedemptionPage = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-use-vetcoins-redemption',
        template: _use_vetcoins_redemption_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_use_vetcoins_redemption_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], UseVetcoinsRedemptionPage);



/***/ }),

/***/ 94570:
/*!**************************************************************************************!*\
  !*** ./src/app/use-vetcoins-redemption/use-vetcoins-redemption.page.scss?ngResource ***!
  \**************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1c2UtdmV0Y29pbnMtcmVkZW1wdGlvbi5wYWdlLnNjc3MifQ== */";

/***/ }),

/***/ 37221:
/*!**************************************************************************************!*\
  !*** ./src/app/use-vetcoins-redemption/use-vetcoins-redemption.page.html?ngResource ***!
  \**************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar class=\"cssion-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button style=\"color: white;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Use Vetcoins</ion-title>\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n\n  <ion-card style=\"text-align: center;\">\n    <ion-card-content>\n\n  <ion-row>\n    <ion-col>\n      <div class=\"ion-text-start\">\n    <ion-label>\n      Name\n    </ion-label>\n  </div>\n  </ion-col>\n    <ion-col>\n      <div class=\"ion-text-end\">\n      <ion-label>\n        {{firmname}}\n      </ion-label>\n      </div>\n    </ion-col>\n  </ion-row>\n\n  \n\n  <ion-row>\n    <ion-col>\n      <div class=\"ion-text-start\">\n    <ion-label>\n      VetCoins\n    </ion-label>\n  </div>\n  </ion-col>\n    <ion-col>\n      <div class=\"ion-text-end\">\n      <ion-label>\n        &#x20b9; {{vetcoinsused}}\n      </ion-label>\n    </div>\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col>\n      <div class=\"ion-text-start\">\n    <ion-label>\n      Transaction Amount\n    </ion-label>\n  </div>\n  </ion-col>\n    <ion-col>\n      <div class=\"ion-text-end\">\n      <ion-label>\n        &#x20b9; {{currentamount}}\n      </ion-label>\n      </div>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n  <ion-col>\n    <div class=\"ion-text-start\">\n    <ion-label>\n      Redemption Amount\n    </ion-label>\n  </div>\n  </ion-col>\n    <ion-col>\n      <div class=\"ion-text-end\">\n      <ion-label>\n        &#x20b9;{{RedemptionAmount| number:'1.0-0'}}\n      </ion-label>\n      </div>\n    </ion-col>\n  </ion-row>\n\n \n    <ion-item no-lines no-lines *ngIf=\"product\">\n      <ion-label>Purchase Product</ion-label>\n      <ion-radio slot=\"start\" color=\"orangevet\" checked></ion-radio>\n    </ion-item>\n\n    <ion-item no-lines *ngIf=\"vet\">\n      <ion-label>Vet Fees</ion-label>\n      <ion-radio slot=\"start\" color=\"orangevet\" checked></ion-radio>\n    </ion-item>\n\n\n\n    <ion-row>\n      <ion-col>\n        <ion-item>\n          <ion-label position=\"stacked\">Enter OTP for trn id 001813</ion-label>\n          <ion-input type=\"text\" type=\"number\" [(ngModel)]=\"userotp\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n<div>\n      <ion-button color=\"orangevet\" expand=\"block\" text-center (click)=\"onUseConfirm()\">Confirm Redemption</ion-button>\n  </div>\n\n</ion-card-content>\n</ion-card>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_use-vetcoins-redemption_use-vetcoins-redemption_module_ts.js.map