"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["default-src_app_components_components_module_ts"],{

/***/ 48598:
/*!************************************************************!*\
  !*** ./src/app/arvisitschedule/arvisitschedule.service.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ArvisitscheduleService": () => (/* binding */ ArvisitscheduleService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);





let ArvisitscheduleService = class ArvisitscheduleService {
    constructor(http, loginauth, genericHTTP) {
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
    }
    getArVisitPlan(body) {
        //console.log("Body: ",body);
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTgetRequest', body, httpOptions);
    }
    saveArVisitPlan(body) {
        //   console.log("Body: ",body);
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTSaveARVisitPlan', body, httpOptions);
    }
    saveArVisitUnplannedPlan(body) {
        //   console.log("Body: ",body);
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTSaveUnplannedARVisitPlan', body, httpOptions);
    }
    getRequest(body) {
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.qcinspection.MQCIgetRequest', body, httpOptions);
    }
    onSaveSectionQuestion(body) {
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Authorization': 'Basic ' + auth
            })
        };
        var specificHeader = { 'Authorization': 'Basic ' + auth };
        return this.genericHTTP.postformdata(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTonSaveChecklistTrx', body, specificHeader, httpOptions);
    }
};
ArvisitscheduleService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_1__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_0__.GenericHttpClientService }
];
ArvisitscheduleService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], ArvisitscheduleService);



/***/ }),

/***/ 31623:
/*!***************************************************!*\
  !*** ./src/app/common/image-preview.directive.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ImagePreviewDirective": () => (/* binding */ ImagePreviewDirective)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);


let ImagePreviewDirective = class ImagePreviewDirective {
    constructor(el) {
        this.el = el;
    }
    ngAfterViewInit() {
        let reader = new FileReader();
        let el = this.el;
        reader.onloadend = function (e) {
            el.nativeElement.src = reader.result;
        };
        if (this.ImagePreviewDirective) {
            //console.log(this.ImagePreviewDirective);
            return reader.readAsDataURL(this.ImagePreviewDirective.rawFile);
            //return window.URL.createObjectURL(this.ImagePreviewDirective.rawFile)
        }
    }
};
ImagePreviewDirective.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef }
];
ImagePreviewDirective.propDecorators = {
    ImagePreviewDirective: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }]
};
ImagePreviewDirective = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_0__.Directive)({
        selector: '[ImagePreviewDirective]'
    })
], ImagePreviewDirective);



/***/ }),

/***/ 45642:
/*!*************************************************!*\
  !*** ./src/app/components/components.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ComponentsModule": () => (/* binding */ ComponentsModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _multi_file_upload_multi_file_upload_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./multi-file-upload/multi-file-upload.component */ 93437);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-file-upload */ 58356);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _common_image_preview_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/image-preview.directive */ 31623);
/* harmony import */ var _listcontrol_listcontrol_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./listcontrol/listcontrol.component */ 28056);
/* harmony import */ var _listcontrolchkframwork_listcontrolchk_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./listcontrolchkframwork/listcontrolchk.component */ 33800);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ 2508);











let ComponentsModule = class ComponentsModule {
};
ComponentsModule = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.NgModule)({
        imports: [ng2_file_upload__WEBPACK_IMPORTED_MODULE_6__.FileUploadModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule, _angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, ionic_selectable__WEBPACK_IMPORTED_MODULE_9__.IonicSelectableModule, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.FormsModule],
        declarations: [_multi_file_upload_multi_file_upload_component__WEBPACK_IMPORTED_MODULE_0__.MultiFileUploadComponent, _common_image_preview_directive__WEBPACK_IMPORTED_MODULE_1__.ImagePreviewDirective, _listcontrol_listcontrol_component__WEBPACK_IMPORTED_MODULE_2__.ListcontrolComponent, _listcontrolchkframwork_listcontrolchk_component__WEBPACK_IMPORTED_MODULE_3__.ListcontrolComponentChk],
        exports: [_multi_file_upload_multi_file_upload_component__WEBPACK_IMPORTED_MODULE_0__.MultiFileUploadComponent, _common_image_preview_directive__WEBPACK_IMPORTED_MODULE_1__.ImagePreviewDirective, _listcontrol_listcontrol_component__WEBPACK_IMPORTED_MODULE_2__.ListcontrolComponent, _listcontrolchkframwork_listcontrolchk_component__WEBPACK_IMPORTED_MODULE_3__.ListcontrolComponentChk],
        schemas: [
            _angular_core__WEBPACK_IMPORTED_MODULE_5__.CUSTOM_ELEMENTS_SCHEMA
        ],
        entryComponents: [
            _listcontrol_listcontrol_component__WEBPACK_IMPORTED_MODULE_2__.ListcontrolComponent, _listcontrolchkframwork_listcontrolchk_component__WEBPACK_IMPORTED_MODULE_3__.ListcontrolComponentChk
        ]
    })
], ComponentsModule);



/***/ }),

/***/ 28056:
/*!*****************************************************************!*\
  !*** ./src/app/components/listcontrol/listcontrol.component.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ListcontrolComponent": () => (/* binding */ ListcontrolComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _listcontrol_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./listcontrol.component.html?ngResource */ 76011);
/* harmony import */ var _listcontrol_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./listcontrol.component.scss?ngResource */ 25700);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/login/loginauth.service */ 44010);





let ListcontrolComponent = class ListcontrolComponent {
    constructor(loginservc) {
        this.loginservc = loginservc;
        this.defaultvalue = '';
        this.isall = false;
    }
    ngOnInit() {
        //console.log(this)
        this.listofitem = this.strToObj(this.listofitem);
        this.value = this.strToObj(this.value);
        this.defaultvalue = this.getListStringFormat(this.value);
        this.listofitem.map((litem) => {
            this.value.map((item) => {
                if (item.id === litem.id) {
                    //console.log(item,litem)
                    litem.ischecked = true;
                }
            });
        });
        if (this.listofitem.length === this.value.length) {
            this.isall = true;
        }
        else {
            this.isall = false;
        }
    }
    selectMember(item, event) {
        item.ischecked = event.detail.checked;
        this.getAllSelectedmember();
        this.loginservc.reportjson[this.inpname] = this.getListStringFormat(this.value);
        this.defaultvalue = this.getListStringFormat(this.value);
    }
    selectAllMember(event) {
        this.isall = event.detail.checked;
        //this.isall=!this.isall;
        this.value = this.listofitem;
        this.listofitem.map((litem) => {
            litem.ischecked = this.isall;
        });
        this.loginservc.reportjson[this.inpname] = this.getListStringFormat(this.value);
        this.defaultvalue = this.getListStringFormat(this.value);
        //console.log(this.isall)
    }
    getAllSelectedmember() {
        this.value = this.listofitem.filter((item) => item.ischecked);
    }
    getListStringFormat(list) {
        let s = '';
        list.map((item) => {
            s = s + '\'' + item.id + '\',';
        });
        if (s !== '') {
            s = '(' + s.substring(0, s.length - 1) + ')';
        }
        return s;
    }
    strToObj(str) {
        var obj = [];
        if (str && typeof str === 'string') {
            // var objStr = str.match(/\[(.)+\]/g);
            eval("obj =" + str);
        }
        return obj;
    }
};
ListcontrolComponent.ctorParameters = () => [
    { type: src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService }
];
ListcontrolComponent.propDecorators = {
    listofitem: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    value: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    inpname: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    inpcaption: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    isall: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }]
};
ListcontrolComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-listcontrol',
        template: _listcontrol_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_listcontrol_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ListcontrolComponent);



/***/ }),

/***/ 33800:
/*!*******************************************************************************!*\
  !*** ./src/app/components/listcontrolchkframwork/listcontrolchk.component.ts ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ListcontrolComponentChk": () => (/* binding */ ListcontrolComponentChk)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _listcontrolchk_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./listcontrolchk.component.html?ngResource */ 88457);
/* harmony import */ var _listcontrolchk_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./listcontrolchk.component.scss?ngResource */ 35649);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _arvisitschedule_arvisitschedule_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../arvisitschedule/arvisitschedule.service */ 48598);

var ListcontrolComponentChk_1;






let ListcontrolComponentChk = ListcontrolComponentChk_1 = class ListcontrolComponentChk {
  constructor(checklistservice) {
    this.checklistservice = checklistservice;
    this.listofitem = [];
    this.defaultvalue = '';
    this.isall = false;

    this.onChange = value => {};
  }

  ngOnInit() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      //console.log(this)
      // this.listofitem =this.strToObj(this.listofitem);
      let body = {
        action: "getallselctordata",
        queid: _this.question.questionid,
        quemaster: _this.checklistservice.selectedinspection,
        orgid: "0"
      };

      _this.checklistservice.getRequest(body).subscribe(data => {
        var response = data;
        _this.listofitem = response.data; //console.log(this.listofitem);
      });
    })();
  }

  writeValue(obj) {
    this.obj = obj; //this.file = null;
    // this.uploader.clearQueue();
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onChange = fn;
  }

  setDisabledState(isDisabled) {}

  selectMember(item, event) {
    item.ischecked = event.detail.checked;
    this.getAllSelectedmember();
    this.question.ans = this.getListStringFormat(this.value);
    this.defaultvalue = this.getListStringFormat(this.value);
    this.writeValue(this.defaultvalue);
    this.myform.get(this.question.questionid).setValue(this.defaultvalue);
  }

  selectAllMember(event) {
    this.isall = event.detail.checked; //this.isall=!this.isall;

    this.value = this.listofitem;
    this.listofitem.map(litem => {
      litem.ischecked = this.isall;
    });

    if (this.isall === false) {
      this.value = [];
    }

    this.question.ans = this.getListStringFormat(this.value);
    this.defaultvalue = this.getListStringFormat(this.value);
    this.writeValue(this.defaultvalue);
    this.myform.get(this.question.questionid).setValue(this.defaultvalue); //console.log(this.isall)
  }

  getAllSelectedmember() {
    this.value = this.listofitem.filter(item => item.ischecked);

    if (this.value) {} else {
      this.value = '';
    }
  }

  getListStringFormat(list) {
    let s = '';
    list.map(item => {
      s = s + '\'' + item.name + '\',';
    });

    if (s !== '') {
      s = '(' + s.substring(0, s.length - 1) + ')';
    }

    return s;
  }

  strToObj(str) {
    var obj = [];

    if (str && typeof str === 'string') {
      // var objStr = str.match(/\[(.)+\]/g);
      eval("obj =" + str);
    }

    return obj;
  }

};

ListcontrolComponentChk.ctorParameters = () => [{
  type: _arvisitschedule_arvisitschedule_service__WEBPACK_IMPORTED_MODULE_3__.ArvisitscheduleService
}];

ListcontrolComponentChk.propDecorators = {
  question: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Input
  }],
  myform: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Input
  }],
  isall: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Input
  }]
};
ListcontrolComponentChk = ListcontrolComponentChk_1 = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
  selector: 'app-listcontrolchk',
  template: _listcontrolchk_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  providers: [{
    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.NG_VALUE_ACCESSOR,
    useExisting: ListcontrolComponentChk_1,
    multi: true
  }],
  styles: [_listcontrolchk_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ListcontrolComponentChk);


/***/ }),

/***/ 93437:
/*!*****************************************************************************!*\
  !*** ./src/app/components/multi-file-upload/multi-file-upload.component.ts ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MultiFileUploadComponent": () => (/* binding */ MultiFileUploadComponent)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _multi_file_upload_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./multi-file-upload.component.html?ngResource */ 90588);
/* harmony import */ var _multi_file_upload_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./multi-file-upload.component.scss?ngResource */ 20161);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-file-upload */ 58356);
/* harmony import */ var _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @awesome-cordova-plugins/camera/ngx */ 64587);
/* harmony import */ var _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @awesome-cordova-plugins/file/ngx */ 25453);
/* harmony import */ var _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/crop/ngx */ 82475);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var src_provider_message_helper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/provider/message-helper */ 98792);

var MultiFileUploadComponent_1;










let MultiFileUploadComponent = MultiFileUploadComponent_1 = class MultiFileUploadComponent {
  constructor(camera, file, msg, crop) {
    this.camera = camera;
    this.file = file;
    this.msg = msg;
    this.crop = crop;
    this.isOnlyCamera = false; //maxfile=0;

    this.allowedFileTypeoption = ["image", "pdf"];
    this.hasBaseDropZoneOver = false;
    this.isextrafiles = false;
    this.croppedImagepath = "";
    this.isLoading = false;
    this.imagePickerOptions = {
      maximumImagesCount: 1,
      quality: 50
    };

    this.onChange = uploader => {};

    this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_7__.FileUploader({
      queueLimit: this.maxfile !== undefined ? this.maxfile : this.msg.maxfile,
      allowedFileType: this.allowedFileTypeoption
    });

    if (this.myform && this.controlID) {
      this.uploader = this.myform.get(this.controlID).value;
    }

    this.uploader.onWhenAddingFileFailed = f => {
      if (this.msg.maxfile == this.uploader.queue.length || this.msg.maxfile < this.uploader.queue.length) this.isextrafiles = true;
    };

    this.uploader.onAfterAddingFile = f => {
      this.isextrafiles = false; //console.log('writevalue',this.uploader);

      this.writeValue(this.uploader);
      this.myform.get(this.controlID).setValue(this.uploader);
    };
  }

  ngOnInit() {
    if (this.myform && this.controlID) {
      // console.log('inside',this.myform.get(this.controlID).value);
      if (this.myform.get(this.controlID).value) {
        this.uploader = this.myform.get(this.controlID).value;
      }
    }

    if (this.maxfile !== undefined) {
      this.uploader.setOptions({
        queueLimit: this.maxfile !== undefined ? this.maxfile : this.msg.maxfile,
        allowedFileType: this.allowedFileTypeoption
      });
    }
  }

  writeValue(obj) {
    this.obj = obj; //this.file = null;
    // this.uploader.clearQueue();
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onChange = fn;
  }

  setDisabledState(isDisabled) {}

  removefiles() {
    this.uploader.clearQueue();
  }

  onfileuploadchange() {} // img1:[any];
  // onchangefile(){
  //    console.log("sada1");
  //    var that=this;
  //   // var reader = new FileReader();  
  //   // function readFile(index) {
  //   //   if( index >= files.length ) return;
  //   //   var file = files[index];
  //   //   reader.onload = function(e) {  
  //   //     // get file content  
  //   //     var bin=reader.result;
  //   //     if(index==0){
  //   //              that.img1=[bin];
  //   //            }
  //   //           else
  //   //           that.img1.push(bin);
  //   //     // do sth with bin
  //   //     readFile(index+1)
  //   //   }
  //   //   reader.readAsDataURL(file);
  //   // }
  //   // readFile(0);
  // ///////==================
  // // 
  // if(that.uploader.queue.length>0)
  // {
  // var reader = new FileReader();  
  // function readFile(index) {
  //   if( index >= that.uploader.queue.length ) return;
  //   var file = that.uploader.queue[index]._file;
  //   reader.onload = function(e) {  
  //     // get file content  
  //     var bin=reader.result;
  //     if(index==0){
  //       that.img1=[bin];
  //     }
  //     else
  //     that.img1.push(bin);
  //     // do sth with bin
  //     readFile(index+1)
  //   }
  //   reader.readAsDataURL(file);
  // }
  // readFile(0);
  // console.log("sada2");
  // }
  // ///////==================
  //   }


  removefile(i) {
    //  console.log("this.uploader.queue[i]",this.uploader.queue[i]);
    this.uploader.queue[i].remove();
  }

  getFiles() {
    console.log("Component: getFiles()");
    return this.uploader.queue.map(fileItem => {
      return fileItem.file;
    });
  }

  fileOverBase(ev) {
    console.log("Component: fileOverBase()");
    this.hasBaseDropZoneOver = ev;
  }

  reorderFiles(reorderEvent) {
    console.log("Component: reorderFiles()");
    let element = this.uploader.queue.splice(reorderEvent.detail.from, 1)[0];
    this.uploader.queue.splice(reorderEvent.detail.to, 0, element);
  } //Capture Image from Camera


  takePicture() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      var that = _this; //  console.log("this.uploader.queue[0]._file",this.uploader.queue[0]._file);
      // this.imgfile=this.uploader.queue[0]._file;

      console.log("Component: takePicture()");
      const options = {
        quality: 50,
        destinationType: _this.camera.DestinationType.FILE_URI,
        encodingType: _this.camera.EncodingType.JPEG,
        mediaType: _this.camera.MediaType.PICTURE,
        sourceType: _this.camera.PictureSourceType.CAMERA,
        targetWidth: 1500,
        targetHeight: 1500
      };

      _this.camera.getPicture(options).then(imageData => {
        _this.cropImage(imageData);
      }, err => {
        console.log("err", err); // Handle error
      }); //  console.log("File from camera:",varimageData);
      //  this.uploader.queue.push(FileItem)
      //var 
      //  var fileEntry= await this.file.resolveLocalFilesystemUrl(varimageData).then((entry:FileEntry) => 
      //        {
      //        entry.file(file=> {
      //         console.log("sdfsdf",file);
      //         this.uploader.addToQueue([file], { autoUpload: false });
      //         console.log("  this.uploader.addToQueue",  this.uploader.addToQueue);
      //       });
      //         return entry;
      //       });
      //   var varfile= new Promise<any>((resolve) => {
      //     fileEntry.file(
      //         meta =>
      //             resolve({
      //              meta
      //             })
      //     );
      // });
      // console.log("varentry:",varentry);
      //  const varfile = await varentry.file(file=> {
      //      console.log("sdfsdf",file);
      //     return file;
      //    });
      //       var filen={
      //         'end': 12885,
      //         'lastModified': 1614917601000,
      // 'lastModifiedDate': 1614917601000,
      // 'localURL': "cdvfile://localhost/cache-external/1614917601312.jpg",
      // name: "1614917601312.jpg",
      // size: 12885,
      // start: 0,
      // type: "image/jpeg",
      //       }
      // console.log("varfile:",varfile);

    })();
  }

  getBlob(b64Data, contentType, sliceSize = 512) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
    b64Data = b64Data.replace(/data\:image\/(jpeg|jpg|png)\;base64\,/gi, '');
    let byteCharacters = atob(b64Data);
    let byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      let slice = byteCharacters.slice(offset, offset + sliceSize);
      let byteNumbers = new Array(slice.length);

      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      let byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    let blob = new Blob(byteArrays, {
      type: contentType
    });
    return blob;
  }

  blobToFile(theBlob, fileName) {
    //A Blob() is almost a File() - it's just missing the two properties below which we will add
    theBlob.lastModifiedDate = new Date();
    theBlob.name = fileName;
    return theBlob;
  }

  cropImage(fileUrl) {
    this.crop.crop(fileUrl, {
      quality: 50
    }).then(newPath => {
      this.showCroppedImage(newPath.split('?')[0]);
    }, error => {//alert('Error cropping image' + error);
    });
  }

  showCroppedImage(ImagePath) {
    this.isLoading = true;
    var copyPath = ImagePath;
    var splitPath = copyPath.split('/');
    var imageName = splitPath[splitPath.length - 1];
    var filePath = ImagePath.split(imageName)[0];
    this.file.readAsDataURL(filePath, imageName).then(base64 => {
      this.croppedImagepath = base64; // console.log('cropped path',base64);

      let blob = this.getBlob(this.croppedImagepath, ".jpg");
      console.log("blob", blob);
      var filename = new Date().toISOString() + '.jpg';
      const file = this.blobToFile(blob, filename);
      this.uploader.addToQueue([file], {
        autoUpload: false
      });
      this.isLoading = false;
    }, error => {
      alert('Error in showing image' + error);
      this.isLoading = false;
    });
  }

};

MultiFileUploadComponent.ctorParameters = () => [{
  type: _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_3__.Camera
}, {
  type: _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_4__.File
}, {
  type: src_provider_message_helper__WEBPACK_IMPORTED_MODULE_6__.Message
}, {
  type: _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_5__.Crop
}];

MultiFileUploadComponent.propDecorators = {
  someInput: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Input
  }],
  myform: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Input
  }],
  controlID: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Input
  }],
  isOnlyCamera: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Input
  }],
  maxfile: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Input
  }]
};
MultiFileUploadComponent = MultiFileUploadComponent_1 = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
  selector: 'app-multi-file-upload',
  template: _multi_file_upload_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  providers: [{
    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_10__.NG_VALUE_ACCESSOR,
    useExisting: MultiFileUploadComponent_1,
    multi: true
  }],
  styles: [_multi_file_upload_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], MultiFileUploadComponent);


/***/ }),

/***/ 25700:
/*!******************************************************************************!*\
  !*** ./src/app/components/listcontrol/listcontrol.component.scss?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsaXN0Y29udHJvbC5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 35649:
/*!********************************************************************************************!*\
  !*** ./src/app/components/listcontrolchkframwork/listcontrolchk.component.scss?ngResource ***!
  \********************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsaXN0Y29udHJvbGNoay5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 20161:
/*!******************************************************************************************!*\
  !*** ./src/app/components/multi-file-upload/multi-file-upload.component.scss?ngResource ***!
  \******************************************************************************************/
/***/ ((module) => {

module.exports = ".drop-zone {\n  background-color: #f6f6f6;\n  border: dotted 3px #dedddd;\n  height: 30vh;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  margin: 20px 0;\n}\n\n.file-input-container {\n  text-align: center;\n  padding-left: 20px;\n}\n\n.file-input-container input[type=file] {\n  display: none;\n}\n\n.file-input-container label {\n  border: 1px solid #ccc;\n  padding: 6px 12px;\n  cursor: pointer;\n}\n\n.nv-file-over {\n  border: dotted 3px red;\n}\n\n.img-container {\n  position: relative;\n}\n\n.topright {\n  position: absolute;\n  top: -8px;\n  right: 25px;\n  font-size: 18px;\n  color: red;\n}\n\nimg {\n  width: 100%;\n  height: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm11bHRpLWZpbGUtdXBsb2FkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSwwQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUVJLGtCQUFBO0VBQ0Esa0JBQUE7QUFBSjs7QUFHSTtFQUNJLGFBQUE7QUFEUjs7QUFJSTtFQUNJLHNCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FBRlI7O0FBTUE7RUFDSSxzQkFBQTtBQUhKOztBQVFBO0VBQ0ksa0JBQUE7QUFMSjs7QUFRRTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtBQUxKOztBQVFFO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUFMSiIsImZpbGUiOiJtdWx0aS1maWxlLXVwbG9hZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5kcm9wLXpvbmUgeyBcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjZmNmY2O1xuICAgIGJvcmRlcjogZG90dGVkIDNweCAjZGVkZGRkOyBcbiAgICBoZWlnaHQ6IDMwdmg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIG1hcmdpbjogMjBweCAwO1xufVxuXG4uZmlsZS1pbnB1dC1jb250YWluZXIge1xuXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBhZGRpbmctbGVmdDogMjBweDtcblxuXG4gICAgaW5wdXRbdHlwZT1cImZpbGVcIl0ge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cblxuICAgIGxhYmVsIHtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgICAgICAgcGFkZGluZzogNnB4IDEycHg7XG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB9XG59XG5cbi5udi1maWxlLW92ZXIgeyBcbiAgICBib3JkZXI6IGRvdHRlZCAzcHggcmVkOyBcbn1cblxuXG4vL0ltYWdlXG4uaW1nLWNvbnRhaW5lciB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB9XG4gIFxuICAudG9wcmlnaHQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IC04cHg7XG4gICAgcmlnaHQ6IDI1cHg7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGNvbG9yOiByZWQ7XG4gIH1cbiAgXG4gIGltZyB7IFxuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogYXV0bztcbiAgICBcbiAgfVxuIl19 */";

/***/ }),

/***/ 76011:
/*!******************************************************************************!*\
  !*** ./src/app/components/listcontrol/listcontrol.component.html?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

module.exports = "\n  <div [id]=\"inpname\"  style=\"margin-top: 10px;margin-bottom: 10px;\" > \n    <ion-label id=\"labellist\" class=\"labelclass sc-ion-label-md-h sc-ion-label-md-s ion-color ion-color-primary md label-stacked hydrated\" \n    style=\"margin-left: 15px;font-size: 12px;\">{{inpcaption}}</ion-label>\n      <ion-item >\n      <ion-checkbox id=\"child\" (ionChange)=\"selectAllMember($event)\" [checked]=\"isall\" [(ngModel)]=\"isall\" style=\"margin-right: 10px;\"></ion-checkbox>\n    </ion-item>\n   \n    <ion-content  style=\"height: 200px;width: 90%;\" scrollY=\"true\" >    \n      <ion-item  *ngFor=\"let item of listofitem\">\n        <ion-checkbox id=\"child\" (ionChange)=\"selectMember(item,$event)\" [checked]=\"item.ischecked\" [(ngModel)]=\"item.ischecked\"  style=\"margin-right: 10px;\"></ion-checkbox>\n        <ion-text >{{item.name}}</ion-text>\n      </ion-item>\n      \n    </ion-content>\n  \n  </div>";

/***/ }),

/***/ 88457:
/*!********************************************************************************************!*\
  !*** ./src/app/components/listcontrolchkframwork/listcontrolchk.component.html?ngResource ***!
  \********************************************************************************************/
/***/ ((module) => {

module.exports = "\n  <div [id]=\"question.questionid\"  style=\" margin-top: 10px;margin-bottom: 10px;\" > \n    <!-- <ion-label id=\"labellist\" class=\"labelclass sc-ion-label-md-h sc-ion-label-md-s ion-color ion-color-primary md label-stacked hydrated\" \n    style=\"margin-left: 15px;font-size: 12px;\"> {{question.queno}}. {{question.question}} </ion-label> -->\n      <ion-item >\n      <ion-checkbox id=\"child\" (ionChange)=\"selectAllMember($event)\" [checked]=\"isall\" [(ngModel)]=\"isall\" style=\"margin-right: 10px;\"></ion-checkbox>\n    </ion-item>\n   \n    <ion-content  style=\"height: 200px;width: 90%;\" scrollY=\"true\" >    \n      <ion-item  *ngFor=\"let item of listofitem\">\n        <ion-checkbox id=\"child\" (ionChange)=\"selectMember(item,$event)\" [checked]=\"item.ischecked\" [(ngModel)]=\"item.ischecked\"  style=\"margin-right: 10px;\"></ion-checkbox>\n        <ion-text >{{item.name}}</ion-text>\n      </ion-item>\n      \n    </ion-content>\n  \n  </div>";

/***/ }),

/***/ 90588:
/*!******************************************************************************************!*\
  !*** ./src/app/components/multi-file-upload/multi-file-upload.component.html?ngResource ***!
  \******************************************************************************************/
/***/ ((module) => {

module.exports = "<!-- <div ng2FileDrop [ngClass]=\"{'nv-file-over': hasBaseDropZoneOver}\" (fileOver)=\"fileOverBase($event)\" [uploader]=\"uploader\" class=\"drop-zone\">\n  Drop files here...accept=\"image/*,.pdf,application/pdf\"\n</div> -->\n\n<div class=\"file-input-container\">\n  <ion-row size=\"6\" *ngIf=\"!isOnlyCamera\">\n  <label style=\"background-color: #f39e20;border-radius: 7px; color: white;\" class=\"bottom-button\">\n    <input type=\"file\" ng2FileSelect [uploader]=\"uploader\" multiple (change)=\"onfileuploadchange()\"/>\n    Gallery\n  </label>\n </ion-row>\n<ion-row size=\"6\" *ngIf='!msg.isplatformweb && msg.istakephoto'>\n    <label style=\"background-color: #f39e20;border-radius: 7px; color: white;\" class=\"bottom-button\" (click)=\"takePicture()\">\n      Capture  </label>\n  </ion-row>\n</div>\n\n<!-- <h2>Files: {{ uploader?.queue?.length }}</h2> -->\n<br/>\n<br/>\n\n<label><span *ngIf='isextrafiles' style=\"color:red!important\">Maximum {{maxfile!==undefined?maxfile:msg.maxfile }} files are allowed.</span></label>\n\n<ion-row>\n  <ion-col size='3' *ngFor=\"let item of uploader.queue;let i = index\">\n   \n    <!-- <ion-icon name=\"trash\" (click)=\"removefile(i)\" style=\"font-size:x-large;\n    color: red;\"></ion-icon> -->\n\n<div class=\"img-container\">\n  <ion-icon *ngIf=\"item?.file?.type=='application/pdf'\" style=\"height: 50px;width: 50px;background-color: coral;\"  name=\"document\"></ion-icon>\n  <img style=\"height: 50px;width: 50px;\" *ngIf=\"item?.file?.type!='application/pdf'\" [src]=\"\" [ImagePreviewDirective] =\"item.file\" />\n\n  <ion-icon name=\"trash\" (click)=\"removefile(i)\"\n  class=\"topright\"></ion-icon>\n</div>\n\n  </ion-col>\n</ion-row>\n\n\n";

/***/ })

}]);
//# sourceMappingURL=default-src_app_components_components_module_ts.js.map