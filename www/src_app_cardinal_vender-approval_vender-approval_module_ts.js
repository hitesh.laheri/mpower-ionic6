"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_cardinal_vender-approval_vender-approval_module_ts"],{

/***/ 68577:
/*!********************************************************************!*\
  !*** ./src/app/cardinal/vender-approval/vender-approval.module.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "VenderApprovalPageModule": () => (/* binding */ VenderApprovalPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _vender_approval_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./vender-approval.page */ 57650);







const routes = [
    {
        path: '',
        component: _vender_approval_page__WEBPACK_IMPORTED_MODULE_0__.VenderApprovalPage
    }
];
let VenderApprovalPageModule = class VenderApprovalPageModule {
};
VenderApprovalPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_vender_approval_page__WEBPACK_IMPORTED_MODULE_0__.VenderApprovalPage]
    })
], VenderApprovalPageModule);



/***/ }),

/***/ 57650:
/*!******************************************************************!*\
  !*** ./src/app/cardinal/vender-approval/vender-approval.page.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "VenderApprovalPage": () => (/* binding */ VenderApprovalPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _vender_approval_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./vender-approval.page.html?ngResource */ 60866);
/* harmony import */ var _vender_approval_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./vender-approval.page.scss?ngResource */ 66965);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _vender_approval_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./vender-approval.service */ 35217);







let VenderApprovalPage = class VenderApprovalPage {
  constructor(venderApprovalService, router) {
    this.venderApprovalService = venderApprovalService;
    this.router = router;
    this.TAG = "VenderApprovalPage";
  }

  ngOnInit() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.venderApprovalList = yield _this.venderApprovalService.getVenderApprovalList().toPromise();
    })();
  }

  ionViewWillEnter() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this2.venderApprovalList = yield _this2.venderApprovalService.getVenderApprovalList().toPromise();
    })();
  }

  refreshPage() {
    try {
      this.ionViewWillEnter();
    } catch (error) {
      console.error(this.TAG, error);
    }
  }

  detailsClick(service) {
    try {
      let navigationExtras = {
        queryParams: {
          special: JSON.stringify(service)
        }
      };
      this.router.navigate(['vender-approval-details'], navigationExtras);
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

};

VenderApprovalPage.ctorParameters = () => [{
  type: _vender_approval_service__WEBPACK_IMPORTED_MODULE_3__.VenderApprovalService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.Router
}];

VenderApprovalPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
  selector: 'app-vender-approval',
  template: _vender_approval_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_vender_approval_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], VenderApprovalPage);


/***/ }),

/***/ 66965:
/*!*******************************************************************************!*\
  !*** ./src/app/cardinal/vender-approval/vender-approval.page.scss?ngResource ***!
  \*******************************************************************************/
/***/ ((module) => {

module.exports = ".arrow-custom {\n  background-color: lightgrey;\n  padding: 5px;\n  border-radius: 5px;\n}\n\n.doc-custom {\n  background-color: #C8E6C9;\n  padding: 5px;\n  border-radius: 5px;\n  color: #1B5E20;\n}\n\n.card-custom {\n  padding-left: 2px !important;\n  padding-right: 0px !important;\n  padding-top: 8px !important;\n  padding-bottom: 0% !important;\n}\n\n.arrow-col {\n  margin-top: 8px;\n}\n\n.complain-custom {\n  font-weight: bolder;\n  font-size: large;\n}\n\n.comname-custom {\n  color: darkgoldenrod;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZlbmRlci1hcHByb3ZhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwyQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUNBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBRUo7O0FBQUE7RUFDSSw0QkFBQTtFQUNBLDZCQUFBO0VBQ0EsMkJBQUE7RUFDQSw2QkFBQTtBQUdKOztBQURBO0VBQ0ksZUFBQTtBQUlKOztBQUZBO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtBQUtKOztBQUhBO0VBQ0ksb0JBQUE7QUFNSiIsImZpbGUiOiJ2ZW5kZXItYXBwcm92YWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFycm93LWN1c3RvbXtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyZXk7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cbi5kb2MtY3VzdG9te1xuICAgIGJhY2tncm91bmQtY29sb3I6I0M4RTZDOTtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGNvbG9yOiAjMUI1RTIwO1xufVxuLmNhcmQtY3VzdG9te1xuICAgIHBhZGRpbmctbGVmdDogMnB4ICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZy1yaWdodDogMHB4ICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZy10b3A6IDhweCAhaW1wb3J0YW50O1xuICAgIHBhZGRpbmctYm90dG9tOiAwJSAhaW1wb3J0YW50O1xufVxuLmFycm93LWNvbHtcbiAgICBtYXJnaW4tdG9wOiA4cHg7XG59XG4uY29tcGxhaW4tY3VzdG9te1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgZm9udC1zaXplOiBsYXJnZTtcbn1cbi5jb21uYW1lLWN1c3RvbXtcbiAgICBjb2xvcjogZGFya2dvbGRlbnJvZDtcbiAgIC8vIGZvbnQtc2l6ZTogaW5pdGlhbDtcbn0iXX0= */";

/***/ }),

/***/ 60866:
/*!*******************************************************************************!*\
  !*** ./src/app/cardinal/vender-approval/vender-approval.page.html?ngResource ***!
  \*******************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Compliant Acceptance\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <ion-buttons (click)=\"refreshPage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-list>\n    <ion-card class=\"card-custom\" *ngFor=\"let service of venderApprovalList\" (click)=\"detailsClick(service)\" padding>\n     \n      <ion-row>\n        <ion-col size=\"4\">\n         <ion-label class=\"comname-custom\">\n           {{service.nameofcomplainer}}\n         </ion-label>\n        </ion-col>\n        <ion-col size=\"4\" text-left>\n         <ion-label>\n           {{service.complaintdate | date:'dd-MMM-yyyy'}}\n         </ion-label>\n        </ion-col>\n        <ion-col size=\"4\" text-end>\n         <ion-label class=\"doc-custom\">\n           {{service.doctype[0].name}}\n         </ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"10\">\n         <ion-label class=\"complain-custom\">\n           {{service.complaintno}}\n         </ion-label>\n        </ion-col>\n        <ion-col size=\"2\" text-end>\n          <div class=\"arrow-col\">\n           <ion-icon name=\"arrow-forward\" class=\"arrow-custom\"></ion-icon>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  </ion-list>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_cardinal_vender-approval_vender-approval_module_ts.js.map