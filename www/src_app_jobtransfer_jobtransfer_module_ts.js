"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_jobtransfer_jobtransfer_module_ts"],{

/***/ 69249:
/*!***************************************************!*\
  !*** ./src/app/jobtransfer/jobtransfer.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "JobtransferPageModule": () => (/* binding */ JobtransferPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _jobtransfer_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./jobtransfer.page */ 57652);







const routes = [
    {
        path: '',
        component: _jobtransfer_page__WEBPACK_IMPORTED_MODULE_0__.JobtransferPage
    }
];
let JobtransferPageModule = class JobtransferPageModule {
};
JobtransferPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_jobtransfer_page__WEBPACK_IMPORTED_MODULE_0__.JobtransferPage]
    })
], JobtransferPageModule);



/***/ }),

/***/ 57652:
/*!*************************************************!*\
  !*** ./src/app/jobtransfer/jobtransfer.page.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "JobtransferPage": () => (/* binding */ JobtransferPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _jobtransfer_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./jobtransfer.page.html?ngResource */ 37464);
/* harmony import */ var _jobtransfer_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./jobtransfer.page.scss?ngResource */ 45117);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 93819);







let JobtransferPage = class JobtransferPage {
    constructor(loginservc, route, router, loadingController) {
        this.loginservc = loginservc;
        this.route = route;
        this.router = router;
        this.loadingController = loadingController;
    }
    ngOnInit() {
        this.loadingController.create({
            duration: 5000,
            spinner: 'circles',
            message: 'Please Wait...'
        }).then((res) => {
            res.present();
        });
        this.route.params.subscribe(params => {
            this.jobid = params['jobid'];
            this.loginservc.getjob(this.jobid).subscribe(data => {
                const response = data['response'];
                this.job = response['data'];
                this.loginservc.getuserlist(this.job[0].mwmsJobtype).subscribe(userdata => {
                    const response1 = userdata['response'];
                    this.userlist = response1['data'];
                    this.loadingController.dismiss();
                });
            });
        });
    }
    onChange(user) {
        this.selectedUser = user;
    }
    onGo() {
        if (this.sreason === undefined || this.selectedUser === undefined) {
            this.txterror = 'All Fields Are Mendetory.';
        }
        else {
            this.loginservc.transferJob(this.jobid, this.selectedUser.user, this.sreason).subscribe(data => {
                this.txterror = 'Success.';
                this.router.navigateByUrl('/joblist');
            });
        }
        // use this.selecteduser and job to transfer web service
    }
};
JobtransferPage.ctorParameters = () => [
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.ActivatedRoute },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.LoadingController }
];
JobtransferPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-jobtransfer',
        template: _jobtransfer_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_jobtransfer_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], JobtransferPage);



/***/ }),

/***/ 45117:
/*!**************************************************************!*\
  !*** ./src/app/jobtransfer/jobtransfer.page.scss?ngResource ***!
  \**************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJqb2J0cmFuc2Zlci5wYWdlLnNjc3MifQ== */";

/***/ }),

/***/ 37464:
/*!**************************************************************!*\
  !*** ./src/app/jobtransfer/jobtransfer.page.html?ngResource ***!
  \**************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Job Transfer</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"joblist\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card>\n      <ion-grid fix>\n          <ion-row >\n              <ion-col >\n                <ion-item >\n                    <ion-label color=\"primary\" position=\"stacked\">Transfer To.</ion-label>\n                    <ion-select #C [ngModel]=\"selectedUser\" (ionChange)=\"onChange(C.value)\" multiple=\"false\" placeholder=\"Select User\">\n                      <ion-select-option *ngFor=\"let users of userlist\" [value]=\"users\">{{users.user$_identifier}}</ion-select-option>\n                    </ion-select>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n                <ion-row>\n                <ion-col >\n                  <ion-item>\n                      <ion-label  position=\"stacked\">Reason</ion-label>\n                      <ion-input  type=\"text\" [(ngModel)]=\"sreason\" placeholder=\"Reason\" ></ion-input>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n                <ion-row>\n                <ion-col >\n                  <ion-item>\n                      <ion-button (click)=\"onGo()\">\n                          Go\n                      </ion-button>\n                  </ion-item>\n              </ion-col>\n              </ion-row>\n              <ion-row>\n              <ion-col>\n                  <ion-text color=\"danger\">{{txterror}}</ion-text>\n              </ion-col>\n            </ion-row>\n      </ion-grid>\n </ion-card>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_jobtransfer_jobtransfer_module_ts.js.map