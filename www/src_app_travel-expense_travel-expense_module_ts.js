"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_travel-expense_travel-expense_module_ts"],{

/***/ 70115:
/*!*********************************************************!*\
  !*** ./src/app/travel-expense/travel-expense.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TravelExpensePageModule": () => (/* binding */ TravelExpensePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _travel_expense_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./travel-expense.page */ 34033);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ionic-selectable */ 25073);








const routes = [
    {
        path: '',
        component: _travel_expense_page__WEBPACK_IMPORTED_MODULE_0__.TravelExpensePage
    }
];
let TravelExpensePageModule = class TravelExpensePageModule {
};
TravelExpensePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule,
            ionic_selectable__WEBPACK_IMPORTED_MODULE_6__.IonicSelectableModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterModule.forChild(routes)
        ],
        declarations: [_travel_expense_page__WEBPACK_IMPORTED_MODULE_0__.TravelExpensePage]
    })
], TravelExpensePageModule);



/***/ }),

/***/ 34033:
/*!*******************************************************!*\
  !*** ./src/app/travel-expense/travel-expense.page.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TravelExpensePage": () => (/* binding */ TravelExpensePage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _travel_expense_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./travel-expense.page.html?ngResource */ 98721);
/* harmony import */ var _travel_expense_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./travel-expense.page.scss?ngResource */ 60135);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _travel_expense_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./travel-expense.service */ 36066);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../provider/message-helper */ 98792);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! date-fns */ 86527);













let TravelExpensePage = class TravelExpensePage {
  constructor(travelExpenseFromBuilder, platform, loginService, travelExpenseService, commonfun, router, route, msg) {
    this.travelExpenseFromBuilder = travelExpenseFromBuilder;
    this.platform = platform;
    this.loginService = loginService;
    this.travelExpenseService = travelExpenseService;
    this.commonfun = commonfun;
    this.router = router;
    this.route = route;
    this.msg = msg; // This variable will hold the class name.

    this.TAG = 'Travel Expense Page';
    this.expenseList = null;
    this.IsexpenseListlength = false;
    this.outstation_chk_box = false;
    this.datefrom = '';
    this.dateto = '';
    this.validation_messages = {
      'selectedPlanListUIControl': [{
        type: 'required',
        message: ' *Please Select Plan.'
      }],
      'selectedBPaddressshipping': [{
        type: 'required',
        message: '*Please Select Shipping Address.'
      }],
      'txtAmount': [{
        type: 'required',
        message: '*Please Enter Expense Amount.'
      }]
    };
    this.getrout();
    this.travelExpenseForm = this.travelExpenseFromBuilder.group({
      salesperson: [, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
      fromdate: [, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
      todate: [, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
      homelat: [, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
      homelong: [, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
      selectedPlanListUIControl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
      outstationChkCtrl: []
    });
  }

  ngOnInit() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.getsalesperson();
    })();
  }

  formatDate(value) {
    this.ondatechange();
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_8__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_9__["default"])(value), 'dd.MM.yyyy');
  }

  formatDate1(value) {
    this.ondatechange();
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_8__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_9__["default"])(value), 'dd.MM.yyyy');
  }

  removerow(post) {
    try {
      // console.log("removerow")
      const result = this.expenseList.filter(item => item != post);
      this.expenseList = result;
      this.chklength();
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  chklength() {
    try {
      if (this.expenseList.length > 0) this.IsexpenseListlength = true;else this.IsexpenseListlength = false;
    } catch (error) {
      this.IsexpenseListlength = false;
    }
  }

  getrout() {
    try {
      this.route.params.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.expenseList = this.router.getCurrentNavigation().extras.state.expenseList;
          this.chklength();
        }
      });
    } catch (error) {// console.log("addsublead()-ERROR:",error);
    }
  }

  onAddExpense() {
    try {
      let navigationExtras = {
        state: {
          expenseList: this.expenseList,
          selectedplan: this.selectedplan,
          expenseMasterList: this.expenseMasterList
        }
      };
      this.router.navigate(['addedit-travel-expense'], navigationExtras);
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }
  /**
   * @kind function
   * @summary This method will get sales person.
   * @since 1.0.0
   * @returns void
   * @public
   * @module Travel Expense
   * @author Nilesh Patil
   */


  getsalesperson() {
    let methodTAG = 'getsalesperson';

    try {
      //localhost:8080/openbravo/ws/in.mbs.webservice.WMobileLatLongUpdate?addid=FFF202005200405006176B076E5C7E39&lat=100&long=2006
      this.commonfun.loadingPresent();
      this.travelExpenseService.getWMobileTravelExpenseList().subscribe(data => {
        this.commonfun.loadingDismiss();
        var response = data; //  console.log("Travle REs",data);

        this.salespersoninfo = response;
        this.txtSalesPerson = this.salespersoninfo[0].salesperson;
        this.planMasterList = this.salespersoninfo;
        this.latitude = this.salespersoninfo[0].latitude;
        this.longitude = this.salespersoninfo[0].longitude;
        this.travelExpenseForm.controls["fromdate"].setValue(this.salespersoninfo[0].fromdate);
        this.travelExpenseForm.controls["todate"].setValue(this.salespersoninfo[0].todate);
      }, error => {
        //  console.log(this.TAG,methodTAG,error)
        this.commonfun.loadingDismiss();
        this.commonfun.presentAlert("Message", "Error", error.error);
      });
    } catch (error) {
      this.commonfun.loadingDismiss(); //  console.log(this.TAG,methodTAG,error)

      this.commonfun.presentAlert("Message", "Error", error.error);
    }
  }

  onSaveTravelExpense(val) {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      let methodTAG = 'onSaveTravelExpense';

      _this2.commonfun.loadingPresent(null, 60000);

      try {
        // this.Dateconversion();
        _this2.strfromdate = _this2.commonfun.Dateconversionddmmyyyy(val.fromdate);
        _this2.strtodate = _this2.commonfun.Dateconversionddmmyyyy(val.todate);
        _this2.strfromdated = _this2.commonfun.Dateconversionddmmyyyy(val.fromdated);
        _this2.strtodated = _this2.commonfun.Dateconversionddmmyyyy(val.todated);
        var jsondatatemp = {
          "salesperson": _this2.salespersoninfo[0].salesperson,
          "salespersonid": _this2.salespersoninfo[0].salespersonid,
          "expenseList": _this2.expenseList,
          "longitude": _this2.salespersoninfo[0].longitude,
          "latitude": _this2.salespersoninfo[0].latitude,
          "todate": _this2.strtodate,
          "fromdate": _this2.strfromdate,
          "plan": _this2.selectedplan.plan,
          "mexp_visitplan_id": _this2.selectedplan.mexp_visitplan_id //"amount":"",//this.leads,

        }; //--------------

        _this2.travelExpenseService.SaveWMobileTravelExpense(jsondatatemp).subscribe(data => {
          _this2.commonfun.loadingDismiss();

          if (data != null) {
            _this2.saveplanresponse = data;

            if (_this2.saveplanresponse.resposemsg == "Success") {
              _this2.commonfun.presentAlert("Message", _this2.saveplanresponse.resposemsg, _this2.saveplanresponse.logmsg);

              _this2.Resetpage();
            } else {
              _this2.commonfun.presentAlert("Message", _this2.saveplanresponse.resposemsg, _this2.saveplanresponse.logmsg);
            }
          }
        }, error => {
          _this2.commonfun.loadingDismiss();

          _this2.commonfun.presentAlert("Message", "Error!", error.error.text);
        }); //------------------

      } catch (error) {
        _this2.commonfun.loadingDismiss();
      }
    })();
  }

  Resetpage() {
    try {
      this.travelExpenseForm.reset();
      this.expenseList = null;
      this.getsalesperson();
      this.chklength();
    } catch (error) {}
  }

  onChangeplan() {
    //  console.log("onChangeplan");
    try {
      //  console.log("selectedplan",this.formactualtravel.controls["selectedplan"].value);
      this.selectedplan = this.travelExpenseForm.controls["selectedPlanListUIControl"].value;
      this.commonfun.loadingPresent();
      this.travelExpenseService.getWMobileTravelExpenseList(this.selectedplan.mexp_visitplan_id).subscribe(data => {
        this.commonfun.loadingDismiss();
        var response = data;

        if (response.length === 0) {
          return;
        }

        if (response[0].isactual != 'Y') {
          this.commonfun.presentAlert("Message", "Alert", "Expense is not applicable for this plan.");
        }

        this.expenseMasterList = response[0].TravelType;
        this.expenseList = response[0].expenseList; //console.log("Travle Exp",this.selectedplan);

        if (response[0].outstation == 'Y') {
          this.outstation_chk_box = true;
        } else {
          this.outstation_chk_box = false;
        }

        this.chklength();
        var a1 = response[0].fromdate.split("-");
        var dt = new Date(a1[2] + '-' + a1[1] + '-' + a1[0] + 'T00:00Z');
        this.travelExpenseForm.controls["fromdate"].setValue(dt.toISOString());
        var a2 = response[0].todate.split("-");
        var dt2 = new Date(a2[2] + '-' + a2[1] + '-' + a2[0] + 'T00:00Z');
        this.travelExpenseForm.controls["todate"].setValue(dt2.toISOString());
      });
    } catch (error) {}
  }

  ondatechange() {
    let methodTAG = 'ondatechange';

    try {} catch (error) {}
  }

  onPlanDropDownSearch() {
    let methodTAG = 'onPlanDropDownSearch';

    try {} catch (error) {}
  }

  onPlanDropDownChange() {
    let methodTAG = 'onPlanDropDownChange';

    try {} catch (error) {}
  }

  onExpenseDropDownSearch() {
    let methodTAG = 'onExpenseDropDownSearch';

    try {} catch (error) {}
  }

  onExpenseDropDownChange() {
    let methodTAG = 'onExpenseDropDownChange';

    try {} catch (error) {}
  }

};

TravelExpensePage.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormBuilder
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.Platform
}, {
  type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__.LoginauthService
}, {
  type: _travel_expense_service__WEBPACK_IMPORTED_MODULE_4__.TravelExpenseService
}, {
  type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_5__.Commonfun
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_11__.Router
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_11__.ActivatedRoute
}, {
  type: _provider_message_helper__WEBPACK_IMPORTED_MODULE_6__.Message
}];

TravelExpensePage = (0,tslib__WEBPACK_IMPORTED_MODULE_12__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_13__.Component)({
  selector: 'app-travel-expense',
  template: _travel_expense_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_travel_expense_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], TravelExpensePage);


/***/ }),

/***/ 60135:
/*!********************************************************************!*\
  !*** ./src/app/travel-expense/travel-expense.page.scss?ngResource ***!
  \********************************************************************/
/***/ ((module) => {

module.exports = ".forecast_div {\n  overflow-x: scroll !important;\n  overflow-x: visible !important;\n  overflow-y: hidden;\n  word-wrap: break-word;\n  height: 20vw;\n  font-size: 0.8em;\n  font-weight: 300;\n}\n\n.grid-header {\n  font-weight: bold;\n}\n\nh5 {\n  font-style: oblique;\n  color: darkcyan;\n  font-size: large;\n}\n\nh5 ion-icon {\n  color: lightcoral;\n}\n\nion-popover {\n  --width: 320px;\n}\n\nion-popover.dateTimePopover {\n  --offset-y: -350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRyYXZlbC1leHBlbnNlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDZCQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUFDSjs7QUFDQTtFQUNFLGlCQUFBO0FBRUY7O0FBQUE7RUFDRSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUdGOztBQUZFO0VBQ0UsaUJBQUE7QUFJSjs7QUFBQTtFQUNFLGNBQUE7QUFHRjs7QUFEQTtFQUNFLDZCQUFBO0FBSUYiLCJmaWxlIjoidHJhdmVsLWV4cGVuc2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcmVjYXN0X2RpdntcbiAgICBvdmVyZmxvdy14OiBzY3JvbGwhaW1wb3J0YW50O1xuICAgIG92ZXJmbG93LXg6IHZpc2libGUhaW1wb3J0YW50O1xuICAgIG92ZXJmbG93LXk6IGhpZGRlbjtcbiAgICB3b3JkLXdyYXA6IGJyZWFrLXdvcmQ7XG4gICAgaGVpZ2h0OjIwdnc7XG4gICAgZm9udC1zaXplOjAuOGVtO1xuICAgIGZvbnQtd2VpZ2h0OjMwMDtcbn1cbi5ncmlkLWhlYWRlcntcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5oNXtcbiAgZm9udC1zdHlsZTogb2JsaXF1ZTtcbiAgY29sb3I6IGRhcmtjeWFuO1xuICBmb250LXNpemU6IGxhcmdlO1xuICBpb24taWNvbntcbiAgICBjb2xvcjogbGlnaHRjb3JhbDtcbiAgfVxuXG59XG5pb24tcG9wb3ZlciB7XG4gIC0td2lkdGg6IDMyMHB4O1xufVxuaW9uLXBvcG92ZXIuZGF0ZVRpbWVQb3BvdmVyIHtcbiAgLS1vZmZzZXQteTogLTM1MHB4ICFpbXBvcnRhbnQ7XG4gIH0iXX0= */";

/***/ }),

/***/ 98721:
/*!********************************************************************!*\
  !*** ./src/app/travel-expense/travel-expense.page.html?ngResource ***!
  \********************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Travel Expense</ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n        <form [formGroup]=\"travelExpenseForm\" (ngSubmit)=\"onSaveTravelExpense(travelExpenseForm.value)\">\n          <ion-card>\n            <ion-card-content>\n              <h5 ion-text class=\"text-primary\">\n                <ion-icon name=\"person\"></ion-icon>\n              </h5>\n              <ion-row>\n                <ion-col>\n              <ion-item>\n                <ion-label  position=\"stacked\">Sales Person</ion-label>\n                <ion-input type=\"text\" formControlName=\"salesperson\" [(ngModel)]=\"txtSalesPerson\" [disabled]=\"true\"></ion-input>\n              </ion-item>\n            </ion-col>\n          </ion-row>\n  \n          <ion-row>\n            <ion-col>\n              <!-- <ion-item>\n                <ion-label position=\"fixed\">Plan</ion-label>\n\n                <ionic-selectable placeholder=\"Select Plan\"\n                                  formControlName=\"travelExpenseForm.controls['selectedPlanListUIControl']\"\n                                  [items]=\"planMasterList\" \n                                  itemValueField=\"id\" \n                                  itemTextField=\"name\" \n                                  [canSearch]=\"true\"\n                                  (onSearch)=\"onPlanDropDownSearch($event)\"\n                                  (onChange)=\"onPlanDropDownChange($event.component.value)\"> \n                </ionic-selectable>\n              </ion-item> -->\n\n              <ion-item>\n                <ion-label  position=\"stacked\">Plan</ion-label>\n\n                <ion-select formControlName=\"selectedPlanListUIControl\" interface=\"popover\" multiple=\"false\" (ionChange)=\"onChangeplan()\" placeholder=\"Select Plan\">\n                  <ion-select-option *ngFor=\"let plan of salespersoninfo\" [value]=\"plan\">{{plan.plan}}</ion-select-option>\n                </ion-select>\n              </ion-item>\n            </ion-col>\n          </ion-row>\n  \n          <ion-row>\n            <ion-col>\n\n              <!-- <ion-item>\n                <ion-label  position=\"stacked\">From Date</ion-label>\n                <ion-datetime placeholder=\"Select Date\" disabled=\"true\" formControlName=\"travelExpenseForm.controls['fromdate']\" (ionChange)=\"ondatechange()\" displayFormat=\"DD.MM.YYYY\" max=\"2050\"></ion-datetime>\n              </ion-item> -->\n              <ion-item [disabled]=\"true\">\n                <ion-label position=\"stacked\">From Date</ion-label>\n                <ion-item>\n                  <ion-input placeholder=\"Select Date\" [value]=\"datefrom\"></ion-input>\n                  <ion-button fill=\"clear\" id=\"open-date-input-1\">\n                    <ion-icon icon=\"calendar\"></ion-icon>\n                  </ion-button>\n                  <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\n                    <ng-template>\n                      <ion-datetime\n                        #popoverDatetime2\n                        presentation=\"date\"\n                        showDefaultButtons=\"true\"\n                        max=\"2050\"\n                        formControlName=\"fromdate\"\n                        (ionChange)=\"datefrom = formatDate(popoverDatetime2.value)\"\n                      ></ion-datetime>\n                    </ng-template>\n                  </ion-popover>\n                </ion-item>\n              </ion-item>\n            </ion-col>\n          </ion-row>\n  \n          <ion-row>\n            <ion-col>\n              <!-- <ion-item>\n                <ion-label  position=\"stacked\">To Date</ion-label>\n                <ion-datetime placeholder=\"Select Date\" disabled=\"true\" formControlName=\"travelExpenseForm.controls['todate']\" (ionChange)=\"ondatechange()\" displayFormat=\"DD.MM.YYYY\" max=\"2050\"></ion-datetime>\n              </ion-item> -->\n              <ion-item [disabled]=\"true\">\n                <ion-label position=\"stacked\">To Date</ion-label>\n                <ion-item>\n                  <ion-input placeholder=\"Select Date\" [value]=\"dateto\"></ion-input>\n                  <ion-button fill=\"clear\" id=\"open-date-input-2\">\n                    <ion-icon icon=\"calendar\"></ion-icon>\n                  </ion-button>\n                  <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-2\" show-backdrop=\"false\">\n                    <ng-template>\n                      <ion-datetime\n                        #popoverDatetime2\n                        presentation=\"date\"\n                        showDefaultButtons=\"true\"\n                        max=\"2050\"\n                        formControlName=\"todate\"\n                        (ionChange)=\"dateto = formatDate1(popoverDatetime2.value)\"\n                      ></ion-datetime>\n                    </ng-template>\n                  </ion-popover>\n                </ion-item>\n              </ion-item>\n            </ion-col>\n          </ion-row>\n  \n          <ion-row>\n            <ion-col>\n              <ion-item>\n                <ion-label  position=\"stacked\">Home Lat</ion-label>\n                <ion-input type=\"text\" formControlName=\"homelat\" [(ngModel)]=\"latitude\" [disabled]=\"true\"></ion-input>\n              </ion-item>\n            </ion-col>\n          </ion-row>\n  \n          <ion-row>\n            <ion-col>\n              <ion-item>\n                <ion-label  position=\"stacked\">Home Long</ion-label>\n                <ion-input type=\"text\" formControlName=\"homelong\" [(ngModel)]=\"longitude\"  [disabled]=\"true\"></ion-input>\n              </ion-item>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col>\n              <ion-item>\n                <ion-label>Outstation?</ion-label>\n                <ion-checkbox  slot=\"end\" [disabled]=\"true\" [checked]=\"outstation_chk_box\" formControlName=\"outstationChkCtrl\"></ion-checkbox>\n              </ion-item>\n            </ion-col>\n          </ion-row>\n            </ion-card-content>\n          </ion-card>\n          <ion-card>\n            <ion-card-content>\n              <ion-row>\n                <ion-col size=\"8\">\n                  <h5 ion-text class=\"text-primary\">\n                    <ion-icon name=\"wifi\"></ion-icon>Expense Detail\n                  </h5>\n                </ion-col>\n                <ion-col *ngIf=\"selectedplan?.isactual!='N'\">\n                  <!-- <ion-col> -->\n                  <ion-fab-button size=\"small\" float-right (click)=\"onAddExpense()\">\n                    <ion-icon name=\"add\"></ion-icon>\n                  </ion-fab-button>\n                </ion-col>\n              </ion-row>\n\n              <div style=\"overflow-x:auto\">\n                <ion-grid>\n\n              \n\n\n                 <ion-row nowrap >\n                  <ion-col nowrap>\n                  <ion-row nowrap>\n                    <ion-col size=\"1\" class=\"grid-header\"></ion-col>\n                    <ion-col size=\"4\" class=\"grid-header\">From</ion-col>\n                   <ion-col size=\"4\" class=\"grid-header\">To</ion-col>\n                   <ion-col size=\"4\" class=\"grid-header\">Travel Expense</ion-col>\n                   <ion-col size=\"4\" class=\"grid-header\">Amount</ion-col>\n                   <ion-col size=\"5\" class=\"grid-header\">Claimable Amount</ion-col>\n                   <ion-col size=\"5\" class=\"grid-header\">Supporting</ion-col>\n          \n                  </ion-row>\n                </ion-col>\n                 </ion-row>\n                \n                 <ion-row *ngFor=\"let data of expenseList; index as i\"  nowrap>\n                  <ion-col nowrap>\n                 <ion-row nowrap>\n                    \n                    <ion-col size=\"1\" style=\"width: 100%; text-align: right;\">\n                      <ion-icon name=\"trash\" (click)=\"removerow(data)\" style=\"font-size: x-large;\n                      color: red;\"></ion-icon>\n                    </ion-col>\n                  <ion-col size=\"4\" class=\"forecast_div\">{{data.fromdate}}</ion-col>\n                  <ion-col size=\"4\" class=\"forecast_div\">{{data.todate}}</ion-col>\n                  <ion-col size=\"4\" class=\"forecast_div\">{{data.travelexpense?.sname}}</ion-col>\n                  <ion-col  size=\"4\"class=\"forecast_div\">{{data.amount}}</ion-col>\n                  <ion-col size=\"5\" class=\"forecast_div\">{{data.claimableamount}}</ion-col>\n                  <ion-col size=\"3\" style=\"text-align: -webkit-center;\">\n                    <img [src]=\"'data:image/jpeg;base64,'+data.Supporting\" *ngIf=\"data.Supporting\" style=\"width: 35px; height: 35px;\">\n                  </ion-col>\n                  \n          \n                  </ion-row>\n                 \n                </ion-col>\n                  </ion-row>\n               \n          \n                </ion-grid>     \n              </div>\n            </ion-card-content>\n          </ion-card>\n\n      </form>\n          <ion-button expand=\"block\" class=\"ion-margin-start ion-margin-end ion-margin-bottom\" type=\"submit\"\n            [disabled]=\"!travelExpenseForm.valid || !IsexpenseListlength\" (click)=\"onSaveTravelExpense(travelExpenseForm.value)\">\n            Save Expenses\n          </ion-button>    \n  \n     \n  \n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_travel-expense_travel-expense_module_ts.js.map