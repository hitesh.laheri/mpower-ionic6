"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_scheme-information_scheme-information_module_ts"],{

/***/ 79590:
/*!*****************************************************************!*\
  !*** ./src/app/scheme-information/scheme-information.module.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SchemeInformationPageModule": () => (/* binding */ SchemeInformationPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _scheme_information_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./scheme-information.page */ 33197);








const routes = [
    {
        path: '',
        component: _scheme_information_page__WEBPACK_IMPORTED_MODULE_0__.SchemeInformationPage
    }
];
let SchemeInformationPageModule = class SchemeInformationPageModule {
};
SchemeInformationPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule, ionic_selectable__WEBPACK_IMPORTED_MODULE_6__.IonicSelectableModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterModule.forChild(routes)
        ],
        declarations: [_scheme_information_page__WEBPACK_IMPORTED_MODULE_0__.SchemeInformationPage]
    })
], SchemeInformationPageModule);



/***/ }),

/***/ 33197:
/*!***************************************************************!*\
  !*** ./src/app/scheme-information/scheme-information.page.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SchemeInformationPage": () => (/* binding */ SchemeInformationPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _scheme_information_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./scheme-information.page.html?ngResource */ 21341);
/* harmony import */ var _scheme_information_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./scheme-information.page.scss?ngResource */ 39649);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _scheme_information_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./scheme-information.service */ 64958);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../provider/message-helper */ 98792);









let SchemeInformationPage = class SchemeInformationPage {
    constructor(fb, loginauth, schemeInformationService, commonfun, msg) {
        this.fb = fb;
        this.loginauth = loginauth;
        this.schemeInformationService = schemeInformationService;
        this.commonfun = commonfun;
        this.msg = msg;
        this.Issinglecust = false;
        this.isgetscheme = false;
        this.selectedrow = null;
        this.schemedata = null;
        this.validation_messages = {
            'selectedBusinessPartner': [
                { type: 'required', message: '' }
            ],
            'selectedddlproduct': [
                { type: 'required', message: '' }
            ]
        };
        this.formscheme = this.fb.group({
            selectedBusinessPartner: [, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required],
            selectedddlproduct: [, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required],
        });
    }
    ngOnInit() {
        this.checkcust();
    }
    custsearchChange(event) {
        var custsearchtext = event.text; //.replace(/\s/g,'');
        if (custsearchtext.length >= 3) {
            this.bindcustomerapi(custsearchtext);
        }
    }
    onCancel(event) {
        // console.log("onCancel");
        event.component._searchText = "";
    }
    onClose(event) {
        // console.log("onClose");
        event.component.searchText = "";
    }
    onchangecust() {
        try {
            // console.log("oncustchange");
            this.selectedBusinessPartner = this.formscheme.controls["selectedBusinessPartner"].value;
            this.formscheme.controls['selectedddlproduct'].setValue('');
            this.selectedddlproduct = null;
            this.isgetscheme = false;
            this.checkproduct();
        }
        catch (error) {
            //  console.log("onchangecust:Error ",error);
        }
    }
    onchangeprod() {
        this.selectedddlproduct = this.formscheme.controls["selectedddlproduct"].value;
        this.isgetscheme = false;
    }
    checkproduct() {
        try {
            this.schemeInformationService.getproductapi("", this.selectedBusinessPartner.id).subscribe(data => {
                var response = data;
                this.leastddlproduct = response;
                //==================
                if (this.leastddlproduct.length == 1) {
                    this.ddlproduct = this.leastddlproduct;
                    this.formscheme.controls["selectedddlproduct"].setValue(this.ddlproduct[0]);
                    this.onchangeprod();
                }
                else if (this.leastddlproduct.length > this.loginauth.minlistcount) {
                    this.ddlproduct = null;
                    this.formscheme.controls["selectedddlproduct"].setValue(null);
                }
                else {
                    this.ddlproduct = this.leastddlproduct;
                    this.formscheme.controls["selectedddlproduct"].setValue(null);
                }
                //==================
            }, error => {
                this.commonfun.presentAlert("Message", "Error!", error.statusText + " with status code :" + error.status);
            });
        }
        catch (error) {
            // console.log("checkproduct Error: ",error);
        }
    }
    checkcust() {
        try {
            this.commonfun.loadingPresent();
            this.schemeInformationService.getcustmerapi("").subscribe(data => {
                this.commonfun.loadingDismiss();
                var response = data;
                this.leastBusinessPartnerlist = response;
                if (this.leastBusinessPartnerlist.length == 1) {
                    this.BusinessPartnerlist = this.leastBusinessPartnerlist;
                    this.formscheme.controls["selectedBusinessPartner"].setValue(this.BusinessPartnerlist[0]);
                    this.onchangecust();
                }
                else if (this.leastBusinessPartnerlist.length > this.loginauth.minlistcount) {
                    this.BusinessPartnerlist = null;
                    this.formscheme.controls["selectedBusinessPartner"].setValue(null);
                }
                else {
                    this.BusinessPartnerlist = this.leastBusinessPartnerlist;
                    this.formscheme.controls["selectedBusinessPartner"].setValue(null);
                }
            });
        }
        catch (error) {
            this.commonfun.loadingDismiss();
            // console.log("Error:chckcust",error);
        }
    }
    bindcustomerapi(strsearch) {
        try {
            if (strsearch != "") {
                this.schemeInformationService.getcustmerapi(strsearch).subscribe(data => {
                    var response = data;
                    this.BusinessPartnerlist = response;
                });
            }
            else {
                //=============start for top 10================= 
                if (this.leastBusinessPartnerlist.length == 1) {
                    this.BusinessPartnerlist = this.leastBusinessPartnerlist;
                    this.formscheme.controls["selectedBusinessPartner"].setValue(this.BusinessPartnerlist[0]);
                    this.onchangecust();
                }
                else if (this.leastBusinessPartnerlist.length > this.loginauth.minlistcount) {
                    this.BusinessPartnerlist = null;
                    this.formscheme.controls["selectedBusinessPartner"].setValue(null);
                }
                else {
                    this.BusinessPartnerlist = this.leastBusinessPartnerlist;
                    this.formscheme.controls["selectedBusinessPartner"].setValue(null);
                }
                //=============end for top 10================= 
            }
        }
        catch (error) {
            //this.commonfun.presentAlert("Message", "Error", error);
            // console.log("Error : bindcustomerapi", error);
        }
    }
    //#region productsearchChange
    productsearchChange(event) {
        var custsearchtext = event.text;
        if (this.selectedBusinessPartner != null) {
            if (custsearchtext.length % 3 == 0) {
                this.bindproduct(custsearchtext);
            }
        }
        else {
            this.commonfun.presentAlert("Message", "Alert", "Please select customer.");
        }
    }
    //#endregion
    //#region bindproduct(strsearch:string)  
    bindproduct(strsearch) {
        try {
            console.log("strsearch", strsearch);
            console.log("this.selectedBusinessPartner.id", this.selectedBusinessPartner.id);
            if (strsearch != "") {
                this.schemeInformationService.getproductapi(strsearch, this.selectedBusinessPartner.id).subscribe(data => {
                    var response = data;
                    this.ddlproduct = response;
                }, error => {
                    this.commonfun.presentAlert("Message", "Error!", error.statusText + " with status code :" + error.status);
                });
            }
            else if (this.selectedBusinessPartner && this.leastddlproduct && this.leastddlproduct != null) {
                //==================
                if (this.leastddlproduct.length == 1) {
                    this.ddlproduct = this.leastddlproduct;
                    this.formscheme.controls["selectedddlproduct"].setValue(this.ddlproduct[0]);
                    this.onchangeprod();
                }
                else if (this.leastddlproduct.length > this.loginauth.minlistcount) {
                    this.ddlproduct = null;
                    this.formscheme.controls["selectedddlproduct"].setValue(null);
                    this.selectedddlproduct = null;
                }
                else {
                    this.ddlproduct = this.leastddlproduct;
                    this.formscheme.controls["selectedddlproduct"].setValue(null);
                    this.selectedddlproduct = null;
                }
                //==================
            }
            else {
                this.ddlproduct = null;
            }
        }
        catch (error) {
            // this.commonfun.loadingDismiss();
            // this.commonfun.presentAlert("Message","Error!",error.statusText +" with status code :"+error.status);
            //  console.log("bindproduct : ERROR: ",error);
        }
    }
    //#endregion bindproduct(strsearch:string)
    getscheme(value) {
        try {
            this.schemedata = null;
            this.schemeInformationService.getCustProdWiseScheme(this.selectedBusinessPartner.id, this.selectedddlproduct.id).subscribe(data => {
                //const response= data['response'];
                var response = data;
                console.log("Product Schme DATA", data);
                this.schemedata = data;
                this.isgetscheme = true;
            }, error => {
                //this.commonfun.loadingDismiss();
                this.commonfun.presentAlert("Message", "Error!", error.statusText + " with status code :" + error.status);
            });
        }
        catch (error) {
        }
    }
    hideshowdetail(i) {
        if (this.selectedrow == i) {
            this.selectedrow = -1;
        }
        else {
            this.selectedrow = i;
        }
    }
    Resetpage() {
        this.formscheme.reset();
        this.ddlproduct = null;
        this.BusinessPartnerlist = null;
        //this.leastBusinessPartnerlist=null;
        this.selectedddlproduct = null;
        this.schemedata = null;
        this.isgetscheme = false;
        if (this.leastBusinessPartnerlist.length == 1) {
            this.BusinessPartnerlist = this.leastBusinessPartnerlist;
            this.formscheme.controls["selectedBusinessPartner"].setValue(this.BusinessPartnerlist[0]);
            // this.onCustDropDownChange(this.BusinessPartnerlist[0]);
        }
        else {
            this.selectedBusinessPartner = null;
        }
    }
};
SchemeInformationPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__.LoginauthService },
    { type: _scheme_information_service__WEBPACK_IMPORTED_MODULE_2__.SchemeInformationService },
    { type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_4__.Commonfun },
    { type: _provider_message_helper__WEBPACK_IMPORTED_MODULE_5__.Message }
];
SchemeInformationPage = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-scheme-information',
        template: _scheme_information_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_scheme_information_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], SchemeInformationPage);



/***/ }),

/***/ 64958:
/*!******************************************************************!*\
  !*** ./src/app/scheme-information/scheme-information.service.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SchemeInformationService": () => (/* binding */ SchemeInformationService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 93819);






let SchemeInformationService = class SchemeInformationService {
    constructor(http, loginauth, genericHTTP, platform) {
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
        this.platform = platform;
    }
    getcustmerapi(strsearch) {
        strsearch = strsearch.replace(/ /g, "%20");
        var ordertypeionic = "";
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.WMobileUserWiseCustomer?'
            + 'user_id=' + this.loginauth.userid
            + '&strsearch=' + strsearch
            + '&ordertypeionic=' + ordertypeionic
            + '&activity_id=' + this.loginauth.selectedactivity.id);
    }
    getproductapi(searchkey, cut_id) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.CustWiseProductScheme?'
            + 'searchchar=' + searchkey
            + '&cut_id=' + cut_id);
    }
    getCustProdWiseScheme(cut_id, prod_id) {
        return this.genericHTTP.get(this.loginauth.commonurl + 'ws/in.mbs.webservice.CustProdWiseScheme?'
            + 'user_id=' + this.loginauth.userid
            + '&activity_id=' + this.loginauth.selectedactivity.id
            + '&cut_id=' + cut_id
            + '&prod_id=' + prod_id);
    }
};
SchemeInformationService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_0__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_1__.GenericHttpClientService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.Platform }
];
SchemeInformationService = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable)({
        providedIn: 'root'
    })
], SchemeInformationService);



/***/ }),

/***/ 39649:
/*!****************************************************************************!*\
  !*** ./src/app/scheme-information/scheme-information.page.scss?ngResource ***!
  \****************************************************************************/
/***/ ((module) => {

module.exports = "ion-card {\n  margin-top: 3px !important;\n  margin-bottom: 3px !important;\n}\n\nion-card-header {\n  background-color: black;\n  color: white;\n  padding: 10px;\n}\n\nh5 {\n  font-style: oblique;\n  color: darkcyan;\n  font-size: large;\n}\n\nh5 ion-icon {\n  color: lightcoral;\n}\n\nion-col.icon-col {\n  flex: 0 0 4px;\n  margin-right: 10px;\n  margin-top: 10px;\n  font-size: 16px;\n  text-align: center;\n  background-color: #d0bdf4;\n  border-radius: 15px;\n  height: 27px;\n}\n\n.scheme-card {\n  padding-top: 0px;\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.btn-scheme {\n  max-width: 400px !important;\n}\n\n.card-1 {\n  background-color: #51e2f5 !important;\n  color: black;\n}\n\n.card-2 {\n  background-color: #9df9ef !important;\n  color: black;\n}\n\n.card-3 {\n  background-color: #edf756 !important;\n  color: black;\n}\n\n.card-4 {\n  background-color: #ffa8B6 !important;\n  color: black;\n}\n\n.card-5 {\n  background-color: #a28089 !important;\n  color: black;\n}\n\n.card-6 {\n  background-color: #a0d2eb !important;\n  color: black;\n}\n\n.card-7 {\n  background-color: #e5eaf5 !important;\n  color: black;\n}\n\n.card-8 {\n  background-color: #d0bdf4 !important;\n  color: black;\n}\n\n.card-9 {\n  background-color: #8458B3 !important;\n  color: black;\n}\n\n.card-10 {\n  background-color: #a28089 !important;\n  color: black;\n}\n\n.card-11 {\n  background-color: #f75990 !important;\n  color: black;\n}\n\n.first-col {\n  text-align: left;\n}\n\n.second-col {\n  text-align: right;\n}\n\n.w3-table, .w3-table-all {\n  border-collapse: collapse;\n  border-spacing: 0;\n  width: 100%;\n  display: table;\n  font-size: 12px;\n  background-color: white;\n}\n\n.w3-table-all {\n  border: 1px solid black;\n}\n\n.w3-bordered tr, .w3-table-all tr {\n  border-bottom: 1px solid black;\n}\n\n.w3-striped tbody tr:nth-child(even) {\n  background-color: #f1f1f1;\n}\n\n.w3-table-all tr:nth-child(odd) {\n  background-color: #fff;\n}\n\n.w3-table-all tr:nth-child(even) {\n  background-color: #f1f1f1;\n}\n\n.w3-hoverable tbody tr:hover, .w3-ul.w3-hoverable li:hover {\n  background-color: #ccc;\n}\n\n.w3-centered tr th, .w3-centered tr td {\n  text-align: center;\n}\n\n.w3-table td, .w3-table th, .w3-table-all td, .w3-table-all th {\n  padding: 8px 8px;\n  display: table-cell;\n  text-align: left;\n  vertical-align: top;\n}\n\n.w3-table th:first-child, .w3-table td:first-child, .w3-table-all th:first-child, .w3-table-all td:first-child {\n  padding-left: 16px;\n}\n\n.w3-bordered tr, .w3-table-all tr {\n  border-bottom: 1px solid #ddd;\n}\n\n.scheme-row {\n  border-bottom: 1px solid #ddd;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNjaGVtZS1pbmZvcm1hdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwwQkFBQTtFQUdBLDZCQUFBO0FBREo7O0FBSUM7RUFDSSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0FBREw7O0FBUUM7RUFDRyxtQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUxKOztBQU1JO0VBQ0UsaUJBQUE7QUFKTjs7QUFRQztFQUVPLGFBQUE7RUFFQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ1IsZUFBQTtFQUNRLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUFQUjs7QUFhQTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQVZKOztBQVlBO0VBQ0ksMkJBQUE7QUFUSjs7QUFXQTtFQUNJLG9DQUFBO0VBQ0EsWUFBQTtBQVJKOztBQVVBO0VBQ0ksb0NBQUE7RUFDQSxZQUFBO0FBUEo7O0FBU0E7RUFDSSxvQ0FBQTtFQUNBLFlBQUE7QUFOSjs7QUFRQTtFQUNJLG9DQUFBO0VBQ0EsWUFBQTtBQUxKOztBQU9BO0VBQ0ksb0NBQUE7RUFDQSxZQUFBO0FBSko7O0FBTUE7RUFDSSxvQ0FBQTtFQUNBLFlBQUE7QUFISjs7QUFJQztFQUNHLG9DQUFBO0VBQ0EsWUFBQTtBQURKOztBQUVDO0VBQ0csb0NBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBQUM7RUFDRyxvQ0FBQTtFQUNBLFlBQUE7QUFHSjs7QUFGQztFQUNHLG9DQUFBO0VBQ0EsWUFBQTtBQUtKOztBQUhBO0VBQ0ksb0NBQUE7RUFDQSxZQUFBO0FBTUo7O0FBSkE7RUFDSSxnQkFBQTtBQU9KOztBQUxBO0VBQ0ksaUJBQUE7QUFRSjs7QUFIQTtFQUVJLHlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtBQUtKOztBQUhBO0VBR0UsdUJBQUE7QUFJRjs7QUFGQTtFQUdHLDhCQUFBO0FBR0g7O0FBREE7RUFFSSx5QkFBQTtBQUdKOztBQURBO0VBRUksc0JBQUE7QUFHSjs7QUFEQTtFQUVJLHlCQUFBO0FBR0o7O0FBREE7RUFFSSxzQkFBQTtBQUdKOztBQURBO0VBRUksa0JBQUE7QUFHSjs7QUFEQTtFQUVJLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBR0o7O0FBREE7RUFFSSxrQkFBQTtBQUdKOztBQUFBO0VBRUksNkJBQUE7QUFFSjs7QUFDQTtFQUVJLDZCQUFBO0FBQ0oiLCJmaWxlIjoic2NoZW1lLWluZm9ybWF0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJke1xuICAgIG1hcmdpbi10b3A6IDNweCAhaW1wb3J0YW50O1xuICAvLyAgbWFyZ2luLWxlZnQ6IDNweCAhaW1wb3J0YW50O1xuICAvLyAgbWFyZ2luLXJpZ2h0OiAzcHggIWltcG9ydGFudDtcbiAgICBtYXJnaW4tYm90dG9tOiAzcHggIWltcG9ydGFudDtcbiAgICBcbn1cbiBpb24tY2FyZC1oZWFkZXJ7XG4gICAgIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICAgICBjb2xvcjogd2hpdGU7XG4gICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgXG4vLyAgICAgbWFyZ2luLXRvcDogLTE4cHghaW1wb3J0YW50O1xuLy8gICAgIG1hcmdpbi1sZWZ0OiAtMjBweDtcbi8vICAgICBtYXJnaW4tcmlnaHQ6IC0yMHB4O1xuXG4gfVxuIGg1e1xuICAgIGZvbnQtc3R5bGU6IG9ibGlxdWU7XG4gICAgY29sb3I6IGRhcmtjeWFuO1xuICAgIGZvbnQtc2l6ZTogbGFyZ2U7XG4gICAgaW9uLWljb257XG4gICAgICBjb2xvcjogbGlnaHRjb3JhbDtcbiAgICB9XG5cbiAgfVxuIGlvbi1jb2wuaWNvbi1jb2x7XG4gICBcbiAgICAgICAgZmxleDogMCAwIDRweDtcbiAgICAvLyAgICBwYWRkaW5nOjA7XG4gICAgICAgIG1hcmdpbi1yaWdodDoxMHB4O1xuICAgICAgICBtYXJnaW4tdG9wOjEwcHg7XG5mb250LXNpemU6IDE2cHg7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2QwYmRmNDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICAgICAgaGVpZ2h0OiAyN3B4O1xuICAgICAgLy8gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgLy8gICB0b3A6IDUwJTtcbiAgICAvLyAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICAgIC8vICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiB9XG4uc2NoZW1lLWNhcmR7XG4gICAgcGFkZGluZy10b3A6IDBweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uYnRuLXNjaGVtZXtcbiAgICBtYXgtd2lkdGg6IDQwMHB4ICFpbXBvcnRhbnQ7XG59XG4uY2FyZC0xe1xuICAgIGJhY2tncm91bmQtY29sb3I6ICM1MWUyZjUhaW1wb3J0YW50O1xuICAgIGNvbG9yOmJsYWNrO1xufVxuLmNhcmQtMntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjOWRmOWVmIWltcG9ydGFudDtcbiAgICBjb2xvcjpibGFjaztcbn1cbi5jYXJkLTN7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojZWRmNzU2IWltcG9ydGFudDtcbiAgICBjb2xvcjpibGFjaztcbn1cbi5jYXJkLTR7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmYThCNiFpbXBvcnRhbnQ7XG4gICAgY29sb3I6YmxhY2s7XG59XG4uY2FyZC01e1xuICAgIGJhY2tncm91bmQtY29sb3I6I2EyODA4OSFpbXBvcnRhbnQ7XG4gICAgY29sb3I6YmxhY2s7XG59XG4uY2FyZC02e1xuICAgIGJhY2tncm91bmQtY29sb3I6I2EwZDJlYiFpbXBvcnRhbnQ7XG4gICAgY29sb3I6YmxhY2s7XG59LmNhcmQtN3tcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNlNWVhZjUhaW1wb3J0YW50O1xuICAgIGNvbG9yOmJsYWNrO1xufS5jYXJkLTh7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojZDBiZGY0IWltcG9ydGFudDtcbiAgICBjb2xvcjpibGFjaztcbn0uY2FyZC05e1xuICAgIGJhY2tncm91bmQtY29sb3I6Izg0NThCMyFpbXBvcnRhbnQ7XG4gICAgY29sb3I6YmxhY2s7XG59LmNhcmQtMTB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojYTI4MDg5IWltcG9ydGFudDtcbiAgICBjb2xvcjpibGFjaztcbn1cbi5jYXJkLTExe1xuICAgIGJhY2tncm91bmQtY29sb3I6I2Y3NTk5MCFpbXBvcnRhbnQ7XG4gICAgY29sb3I6YmxhY2s7XG59XG4uZmlyc3QtY29se1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG59XG4uc2Vjb25kLWNvbHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbn1cblxuXG4vL25ldyBcbi53My10YWJsZSwudzMtdGFibGUtYWxsXG57XG4gICAgYm9yZGVyLWNvbGxhcHNlOmNvbGxhcHNlO1xuICAgIGJvcmRlci1zcGFjaW5nOjA7XG4gICAgd2lkdGg6MTAwJTtcbiAgICBkaXNwbGF5OnRhYmxlO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cbi53My10YWJsZS1hbGxcbntcbiAgLy8gIGJvcmRlcjoxcHggc29saWQgI2NjY1xuICBib3JkZXI6MXB4IHNvbGlkIGJsYWNrO1xufVxuLnczLWJvcmRlcmVkIHRyLC53My10YWJsZS1hbGwgdHJcbntcbiAgIC8vIGJvcmRlci1ib3R0b206MXB4IHNvbGlkICNkZGRcbiAgIGJvcmRlci1ib3R0b206MXB4IHNvbGlkIGJsYWNrO1xufVxuLnczLXN0cmlwZWQgdGJvZHkgdHI6bnRoLWNoaWxkKGV2ZW4pXG57XG4gICAgYmFja2dyb3VuZC1jb2xvcjojZjFmMWYxXG59XG4udzMtdGFibGUtYWxsIHRyOm50aC1jaGlsZChvZGQpXG57XG4gICAgYmFja2dyb3VuZC1jb2xvcjojZmZmXG59XG4udzMtdGFibGUtYWxsIHRyOm50aC1jaGlsZChldmVuKVxue1xuICAgIGJhY2tncm91bmQtY29sb3I6I2YxZjFmMVxufVxuLnczLWhvdmVyYWJsZSB0Ym9keSB0cjpob3ZlciwudzMtdWwudzMtaG92ZXJhYmxlIGxpOmhvdmVyXG57XG4gICAgYmFja2dyb3VuZC1jb2xvcjojY2NjXG59XG4udzMtY2VudGVyZWQgdHIgdGgsLnczLWNlbnRlcmVkIHRyIHRkXG57XG4gICAgdGV4dC1hbGlnbjpjZW50ZXJcbn1cbi53My10YWJsZSB0ZCwudzMtdGFibGUgdGgsLnczLXRhYmxlLWFsbCB0ZCwudzMtdGFibGUtYWxsIHRoXG57XG4gICAgcGFkZGluZzo4cHggOHB4O1xuICAgIGRpc3BsYXk6dGFibGUtY2VsbDtcbiAgICB0ZXh0LWFsaWduOmxlZnQ7XG4gICAgdmVydGljYWwtYWxpZ246dG9wXG59XG4udzMtdGFibGUgdGg6Zmlyc3QtY2hpbGQsLnczLXRhYmxlIHRkOmZpcnN0LWNoaWxkLC53My10YWJsZS1hbGwgdGg6Zmlyc3QtY2hpbGQsLnczLXRhYmxlLWFsbCB0ZDpmaXJzdC1jaGlsZFxue1xuICAgIHBhZGRpbmctbGVmdDoxNnB4XG59XG5cbi53My1ib3JkZXJlZCB0ciwudzMtdGFibGUtYWxsIHRyXG57XG4gICAgYm9yZGVyLWJvdHRvbToxcHggc29saWQgI2RkZFxufVxuLy9lbmQgbmV3XG4uc2NoZW1lLXJvd1xue1xuICAgIGJvcmRlci1ib3R0b206MXB4IHNvbGlkICNkZGQ7XG4gIC8vICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjFGMUYxO1xuXG4gICAgLy8gbWFyZ2luLXRvcDogLTVweDtcbiAgICAvLyBtYXJnaW4tbGVmdDogLTVweDtcbiAgICAvLyBtYXJnaW4tcmlnaHQ6IC01cHg7XG4gICAgXG59Il19 */";

/***/ }),

/***/ 21341:
/*!****************************************************************************!*\
  !*** ./src/app/scheme-information/scheme-information.page.html?ngResource ***!
  \****************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      <div style=\"white-space: normal;\">\n        Scheme Information\n      </div>\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n\n\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n  <form [formGroup]=\"formscheme\" (ngSubmit)=\"getscheme(formscheme.value)\">\n    <ion-card>\n      <ion-card-content>\n        <ion-row>\n          <ion-col>\n            <h5 ion-text class=\"text-primary\">\n              <ion-icon name=\"person\"></ion-icon> Customer Detail :\n            </h5>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label position=\"stacked\">Customer<span style=\"color:red!important\">*</span></ion-label>\n              <ionic-selectable placeholder=\"Select Customer\" [searchDebounce]=\"1000\"\n                formControlName=\"selectedBusinessPartner\" [items]=\"BusinessPartnerlist\"\n                itemValueField=\"id\" itemTextField=\"_identifier\" [canSearch]=\"true\" (onSearch)=\"custsearchChange($event)\"\n                (onClose)=\"onCancel($event)\" (onChange)=\"onchangecust()\">\n              </ionic-selectable>\n              <!-- (onSearch)=\"custsearchChange($event)\" -->\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.selectedBusinessPartner\">\n                <div\n                  *ngIf=\"formscheme.get('selectedBusinessPartner').hasError(validation.type) && formscheme.get('selectedBusinessPartner').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label position=\"stacked\">Product<span style=\"color:red!important\">*</span></ion-label>\n              <ionic-selectable placeholder=\"Select Product\" formControlName=\"selectedddlproduct\"\n                [items]=\"ddlproduct\" itemValueField=\"id\" itemTextField=\"_identifier\" [canSearch]=\"true\"\n                (onClose)=\"onClose($event)\" (onSearch)=\"productsearchChange($event)\" (onChange)=\"onchangeprod()\">\n              </ionic-selectable>\n            </ion-item>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.selectedddlproduct\">\n                <div\n                  *ngIf=\"formscheme.get('selectedddlproduct').hasError(validation.type) && formscheme.get('selectedddlproduct').touched\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-card-content>\n    </ion-card>\n  </form>\n  <ion-button expand=\"block\" class=\"ion-margin-start ion-margin-end ion-margin-bottom btn-scheme\" type=\"submit\"\n    (click)=\"getscheme(formscheme.value)\" [disabled]=\"!formscheme.valid\">\n    Get Scheme\n  </ion-button>\n  <ion-card *ngIf='isgetscheme'>\n    <ion-card-content class=\"scheme-card\">\n      <ion-card-header>\n        <p>{{selectedddlproduct?._identifier}}</p>\n      </ion-card-header>\n      <ion-row *ngFor=\"let data of schemedata; index as i\" class=\"scheme-row\">\n        <ion-col>\n          <ion-row (click)=\"hideshowdetail(i)\">\n            <ion-col class=\"icon-col\">\n              <ion-icon name=\"pricetags\"></ion-icon>\n            </ion-col>\n            <ion-col>\n              <p style=\"font-weight: bold;\">{{data.SchemeName}}</p>\n              <p>Type: {{data.Scheme_Type}}</p>\n              <p>{{data.From_To}}</p>\n            </ion-col>\n          </ion-row>\n          <div style=\"overflow-x:auto;white-space: nowrap;\">\n            <table class=\"w3-table w3-bordered\">\n              <tr *ngIf=\"selectedrow==i\" nowrap style=\"background-color: #F1F1F1;\">\n                <th *ngIf='data.Scheme_Type_id==\"Per\"'>Per Qty</th>\n                <th *ngIf='data.Scheme_Type_id==\"Slab\" || data.Scheme_Type_id==\"NE\"'>Quantity Range</th>\n                <th *ngIf='data.Scheme_Type_id==\"OB\"'>Weight Above</th>\n                <th *ngIf='data.Scheme_Type_id==\"OB\"'>Value Above</th>\n                <th *ngIf='data.Scheme_Type_id==\"OB\"'>Qty Above</th>\n                <th *ngIf='data.Scheme_Type_id==\"OB\"'>Payment Term</th>\n                <th *ngIf='data.Scheme_Type_id==\"CD\"'>Value Above</th>\n                <th *ngIf='data.Scheme_Type_id==\"CD\"'>Quantity Above</th>\n\n                <th *ngIf='data.Scheme_Type_id==\"SP\" || data.Scheme_Type_id==\"Slab\" || data.Scheme_Type_id==\"NE\"'>From Qty</th>\n                <th *ngIf='data.Scheme_Type_id==\"SP\" || data.Scheme_Type_id==\"Slab\" || data.Scheme_Type_id==\"NE\"'>To Qty</th>\n                <th *ngIf='data.Scheme_Type_id==\"SP\"'>Special Rate</th>\n\n                <th *ngIf='data.Scheme_Type_id==\"Slab\" || data.Scheme_Type_id==\"NE\" || data.Scheme_Type_id==\"Per\"'>Free\n                  Qty</th>\n                <th *ngIf='data.Scheme_Type_id==\"Slab\" || data.Scheme_Type_id==\"NE\" || data.Scheme_Type_id==\"Per\"  || data.Scheme_Type_id==\"SP\"'>\n                  Product</th>\n                <th *ngIf='data.Scheme_Type_id != \"SP\"'>Discount</th>\n                <th *ngIf='data.Scheme_Type_id==\"Slab\" || data.Scheme_Type_id==\"NE\" || data.Scheme_Type_id==\"Per\"'>\n                  Reward</th>\n              </tr>\n              <tr *ngFor=\"let schemeinfo of data.SchemeRecords; index as j\">\n                <ng-container *ngIf=\"selectedrow==i\">\n                  <td *ngIf='data.Scheme_Type_id==\"Per\"'>{{schemeinfo.PerQty}}</td>\n                  <td *ngIf='data.Scheme_Type_id==\"Slab\" || data.Scheme_Type_id==\"NE\"'>{{schemeinfo.fromqty}} To\n                    {{schemeinfo.toqty}}</td>\n                  <td *ngIf='data.Scheme_Type_id==\"OB\"'>{{schemeinfo.WeightAbove}}</td>\n                  <td *ngIf='data.Scheme_Type_id==\"OB\"'>{{schemeinfo.ValueAbove}}</td>\n                  <th *ngIf='data.Scheme_Type_id==\"OB\"'>{{schemeinfo.Qtyabove}}</th>\n                  <th *ngIf='data.Scheme_Type_id==\"OB\"'>{{schemeinfo.payment_term}}</th>\n                  <td *ngIf='data.Scheme_Type_id==\"CD\"'>{{schemeinfo.fromvalue}}</td>\n                  <td *ngIf='data.Scheme_Type_id==\"CD\"'>{{schemeinfo.tovalue}}</td>\n\n                  <td *ngIf='data.Scheme_Type_id==\"SP\" || data.Scheme_Type_id==\"Slab\" || data.Scheme_Type_id==\"NE\"'>{{schemeinfo.fromqty}}</td>\n                  <td *ngIf='data.Scheme_Type_id==\"SP\" || data.Scheme_Type_id==\"Slab\" || data.Scheme_Type_id==\"NE\"'>{{schemeinfo.toqty}}</td>\n                  <td *ngIf='data.Scheme_Type_id==\"SP\"'>{{schemeinfo.nspecialrate}}</td>\n\n\n                  <td *ngIf='data.Scheme_Type_id==\"Slab\" || data.Scheme_Type_id==\"NE\" || data.Scheme_Type_id==\"Per\"'>\n                    {{schemeinfo.FreeQuantity}}</td>\n                  <td *ngIf='data.Scheme_Type_id==\"Slab\" || data.Scheme_Type_id==\"NE\" || data.Scheme_Type_id==\"Per\" || data.Scheme_Type_id==\"SP\"'>\n                    {{schemeinfo.Product}}</td>\n                  <td *ngIf='data.Scheme_Type_id != \"SP\"'>{{schemeinfo.Discount}}</td>\n                  <td *ngIf='data.Scheme_Type_id==\"Slab\" || data.Scheme_Type_id==\"NE\" || data.Scheme_Type_id==\"Per\"'>\n                    {{schemeinfo.RewardPoints}}</td>\n                </ng-container>\n              </tr>\n            </table>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n\n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_scheme-information_scheme-information_module_ts.js.map