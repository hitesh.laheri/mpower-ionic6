"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_addedit-actual-travel-plan_addedit-actual-travel-plan_module_ts"],{

/***/ 75313:
/*!*********************************************************************************!*\
  !*** ./src/app/addedit-actual-travel-plan/addedit-actual-travel-plan.module.ts ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddeditActualTravelPlanPageModule": () => (/* binding */ AddeditActualTravelPlanPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _addedit_actual_travel_plan_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./addedit-actual-travel-plan.page */ 41290);







const routes = [
    {
        path: '',
        component: _addedit_actual_travel_plan_page__WEBPACK_IMPORTED_MODULE_0__.AddeditActualTravelPlanPage
    }
];
let AddeditActualTravelPlanPageModule = class AddeditActualTravelPlanPageModule {
};
AddeditActualTravelPlanPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_addedit_actual_travel_plan_page__WEBPACK_IMPORTED_MODULE_0__.AddeditActualTravelPlanPage]
    })
], AddeditActualTravelPlanPageModule);



/***/ }),

/***/ 41290:
/*!*******************************************************************************!*\
  !*** ./src/app/addedit-actual-travel-plan/addedit-actual-travel-plan.page.ts ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddeditActualTravelPlanPage": () => (/* binding */ AddeditActualTravelPlanPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _addedit_actual_travel_plan_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./addedit-actual-travel-plan.page.html?ngResource */ 19121);
/* harmony import */ var _addedit_actual_travel_plan_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./addedit-actual-travel-plan.page.scss?ngResource */ 39470);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _travel_plan_travel_plan_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../travel-plan/travel-plan.service */ 85276);









let AddeditActualTravelPlanPage = class AddeditActualTravelPlanPage {
    constructor(fb, commonfun, router, route, travelplanservice, loginauth) {
        this.fb = fb;
        this.commonfun = commonfun;
        this.router = router;
        this.route = route;
        this.travelplanservice = travelplanservice;
        this.loginauth = loginauth;
        this.selectedleadskm = [];
        this.offset = 0;
        this.strsearch = '';
        this.index = '';
        this.isfirst = false;
        this.islast = false;
        this.getrout();
        this.formaddactualplan = this.fb.group({
            selectedlead: [],
            searchlead: [],
        });
    }
    ngOnInit() {
    }
    Resetpage() { }
    onsearch() {
        var srchkey = this.formaddactualplan.controls["searchlead"].value;
        this.strsearch = srchkey;
        this.offset = 0;
        if (srchkey.length % 3 == 0 || srchkey == '') {
            this.bindNearestPerson();
        }
    }
    onPrevious() {
        if (this.offset > 1) {
            this.offset = this.offset - 10;
            this.bindNearestPerson();
        }
    }
    onNext() {
        this.offset = this.offset + 10;
        this.bindNearestPerson();
    }
    bindNearestPerson() {
        try {
            this.travelplanservice.getSearchNearestPersoni(this.selectedLead.addressid, this.offset, this.strsearch, this.outOrderChkCtrlValueAddEditActual).subscribe(data => {
                var response = data;
                if (response.AddressList.length > 0) {
                    this.leadskm = response.AddressList;
                    // this.leadskm=this.leadskm.slice(0,10)
                }
                else {
                    //       this.leadskm=response.AddressList;
                    //       if(this.offset>0)
                    // this.offset=this.offset-10;
                    if (this.offset > 0)
                        this.offset = this.offset - 10;
                    if (this.strsearch != "")
                        this.leadskm = response.AddressList;
                }
            });
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
            // this.commonfun.loadingDismiss();
        }
    }
    onAddLead(item) {
        try {
            if (this.selectedleadskm) {
                if (this.selectedleadskm.some(i => i.addressid === item.addressid)) {
                    this.commonfun.presentAlert("Message", "Alert", "Lead already exists.");
                }
                else {
                    var slitem = { "custORbpatnerID": item.custORbpatnerID, "mexp_visitplan_id": this.mexp_visitplan_id, "mexp_customervisit_id": item.custORbpatnerID, "line": this.selectedleadskm.length + 1, "custname": item.custname, "addressid": item.addressid, "addressname": item.addressname, "pincode": item.pincode, "visit_day": this.days, "visit_date": this.traveldate, "latitude": item.latitude, "longitude": item.longitude, "km": item.km, "status": item.status, "show": "false", "TaskList": item.TaskList };
                    this.selectedleadskm.splice(this.index + 1, 0, slitem);
                    this.onSave();
                    //  this.selectedleadskm.push(slitem);
                }
            }
            else {
                var slitem1 = [{ "custORbpatnerID": item.custORbpatnerID, "mexp_visitplan_id": this.mexp_visitplan_id, "mexp_customervisit_id": item.custORbpatnerID, "line": "1", "custname": item.custname, "addressid": item.addressid, "addressname": item.addressname, "pincode": item.pincode, "visit_day": this.days, "visit_date": this.traveldate, "latitude": item.latitude, "longitude": item.longitude, "km": item.km, "status": item.status, "show": "false", "TaskList": item.TaskList }];
                this.selectedleadskm = slitem1;
                this.onSave();
            }
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    getrout() {
        try {
            this.route.params.subscribe(params => {
                if (this.router.getCurrentNavigation().extras.state) {
                    this.selectedLead = this.router.getCurrentNavigation().extras.state.selectedLead;
                    this.selectedleadskm = this.router.getCurrentNavigation().extras.state.allleads;
                    this.mexp_visitplan_id = this.router.getCurrentNavigation().extras.state.mexp_visitplan_id;
                    this.index = this.router.getCurrentNavigation().extras.state.index;
                    this.fromdate = this.router.getCurrentNavigation().extras.state.fromdate;
                    this.todate = this.router.getCurrentNavigation().extras.state.todate;
                    this.days = this.router.getCurrentNavigation().extras.state.days;
                    this.traveldate = this.router.getCurrentNavigation().extras.state.traveldate;
                    this.outOrderChkCtrlValueAddEditActual = this.router.getCurrentNavigation().extras.state.outOrderChkCtrl;
                    if (this.selectedLead == null) {
                        this.leadskm = this.router.getCurrentNavigation().extras.state.first10leads;
                    }
                    else {
                        this.bindNearestPerson();
                    }
                }
            });
        }
        catch (error) {
            //console.log("addsublead()-ERROR:",error);
        }
    }
    removeLeads(post) {
        try {
            //console.log("removeLeads()");
            let index = this.selectedleadskm.indexOf(post);
            const result = this.selectedleadskm.filter(item => item != post);
            this.selectedleadskm = result;
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    onSave() {
        try {
            let navigationExtras = {
                state: {
                    selectedddlsubleads: this.selectedleadskm,
                    selectedLead: this.selectedLead
                }
            };
            this.router.navigate(['actual-travel-plan'], navigationExtras);
        }
        catch (error) {
        }
    }
};
AddeditActualTravelPlanPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormBuilder },
    { type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_3__.Commonfun },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.Router },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.ActivatedRoute },
    { type: _travel_plan_travel_plan_service__WEBPACK_IMPORTED_MODULE_4__.TravelPlanService },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService }
];
AddeditActualTravelPlanPage = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-addedit-actual-travel-plan',
        template: _addedit_actual_travel_plan_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_addedit_actual_travel_plan_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], AddeditActualTravelPlanPage);



/***/ }),

/***/ 39470:
/*!********************************************************************************************!*\
  !*** ./src/app/addedit-actual-travel-plan/addedit-actual-travel-plan.page.scss?ngResource ***!
  \********************************************************************************************/
/***/ ((module) => {

module.exports = "ion-scroll[scrollX] {\n  white-space: nowrap;\n  overflow: visible;\n  overflow-y: auto;\n}\nion-scroll[scrollX] .scroll-item {\n  display: inline-block;\n}\nion-scroll[scrollX] .selectable-icon {\n  margin: 10px 20px 10px 20px;\n  color: red;\n  font-size: 100px;\n}\nion-scroll[scrollX] ion-avatar img {\n  margin: 10px;\n}\nion-scroll[scroll-avatar] {\n  height: 60px;\n}\n/* Hide ion-content scrollbar */\n::-webkit-scrollbar {\n  display: none;\n}\nh5 {\n  font-style: oblique;\n  color: darkcyan;\n  font-size: large;\n}\nh5 ion-icon {\n  color: lightcoral;\n}\n.inputfile {\n  color: transparent;\n}\n.forecast_container {\n  overflow-x: scroll !important;\n  overflow-x: visible !important;\n  overflow-y: hidden;\n  word-wrap: break-word;\n  height: 20vw;\n  font-size: 0.8em;\n  font-weight: 300;\n}\n.forecast_div {\n  overflow-x: scroll !important;\n  overflow-x: visible !important;\n  overflow-y: hidden;\n  word-wrap: break-word;\n  font-size: 0.8em;\n  font-weight: 300;\n  max-width: 175px;\n}\n.grid-header {\n  font-weight: bold;\n  max-width: 175px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZGVkaXQtYWN0dWFsLXRyYXZlbC1wbGFuLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFBO0VBRUEsaUJBQUE7RUFDQSxnQkFBQTtBQUFKO0FBRUk7RUFDRSxxQkFBQTtBQUFOO0FBR0k7RUFDRSwyQkFBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtBQUROO0FBSUk7RUFDRSxZQUFBO0FBRk47QUFNRTtFQUNFLFlBQUE7QUFISjtBQU1FLCtCQUFBO0FBQ0E7RUFDRSxhQUFBO0FBSEo7QUFNRTtFQUNFLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBSEo7QUFJSTtFQUNFLGlCQUFBO0FBRk47QUFPRTtFQUNFLGtCQUFBO0FBSko7QUFPRTtFQUNFLDZCQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUFKSjtBQU1BO0VBQ0UsNkJBQUE7RUFDQSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUFIRjtBQUtBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQUZBIiwiZmlsZSI6ImFkZGVkaXQtYWN0dWFsLXRyYXZlbC1wbGFuLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1zY3JvbGxbc2Nyb2xsWF0ge1xuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAvLyBoZWlnaHQ6IDEyMHB4O1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgIG92ZXJmbG93LXk6IGF1dG87XG4vLyB3aWR0aDoxMDAlO1xuICAgIC5zY3JvbGwtaXRlbSB7XG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgfVxuXG4gICAgLnNlbGVjdGFibGUtaWNvbntcbiAgICAgIG1hcmdpbjogMTBweCAyMHB4IDEwcHggMjBweDtcbiAgICAgIGNvbG9yOiByZWQ7XG4gICAgICBmb250LXNpemU6IDEwMHB4O1xuICAgIH1cblxuICAgIGlvbi1hdmF0YXIgaW1ne1xuICAgICAgbWFyZ2luOiAxMHB4O1xuICAgIH1cbiAgfVxuXG4gIGlvbi1zY3JvbGxbc2Nyb2xsLWF2YXRhcl17XG4gICAgaGVpZ2h0OiA2MHB4O1xuICB9XG5cbiAgLyogSGlkZSBpb24tY29udGVudCBzY3JvbGxiYXIgKi9cbiAgOjotd2Via2l0LXNjcm9sbGJhcntcbiAgICBkaXNwbGF5Om5vbmU7XG4gIH1cblxuICBoNXtcbiAgICBmb250LXN0eWxlOiBvYmxpcXVlO1xuICAgIGNvbG9yOiBkYXJrY3lhbjtcbiAgICBmb250LXNpemU6IGxhcmdlO1xuICAgIGlvbi1pY29ue1xuICAgICAgY29sb3I6IGxpZ2h0Y29yYWw7XG4gICAgfVxuXG4gIH1cbiAgXG4gIC5pbnB1dGZpbGUge1xuICAgIGNvbG9yOiB0cmFuc3BhcmVudDtcbiAgfVxuXG4gIC5mb3JlY2FzdF9jb250YWluZXJ7XG4gICAgb3ZlcmZsb3cteDogc2Nyb2xsIWltcG9ydGFudDtcbiAgICBvdmVyZmxvdy14OiB2aXNpYmxlIWltcG9ydGFudDtcbiAgICBvdmVyZmxvdy15OiBoaWRkZW47XG4gICAgd29yZC13cmFwOiBicmVhay13b3JkO1xuICAgIGhlaWdodDoyMHZ3O1xuICAgIGZvbnQtc2l6ZTowLjhlbTtcbiAgICBmb250LXdlaWdodDozMDA7XG59XG4uZm9yZWNhc3RfZGl2e1xuICBvdmVyZmxvdy14OiBzY3JvbGwhaW1wb3J0YW50O1xuICBvdmVyZmxvdy14OiB2aXNpYmxlIWltcG9ydGFudDtcbiAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICB3b3JkLXdyYXA6IGJyZWFrLXdvcmQ7XG4gIGZvbnQtc2l6ZTowLjhlbTtcbiAgZm9udC13ZWlnaHQ6MzAwO1xuICBtYXgtd2lkdGg6IDE3NXB4O1xufVxuLmdyaWQtaGVhZGVye1xuZm9udC13ZWlnaHQ6IGJvbGQ7XG5tYXgtd2lkdGg6IDE3NXB4O1xufSJdfQ== */";

/***/ }),

/***/ 19121:
/*!********************************************************************************************!*\
  !*** ./src/app/addedit-actual-travel-plan/addedit-actual-travel-plan.page.html?ngResource ***!
  \********************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar  color=\"primary\">\n    <ion-title>Add Customer</ion-title>\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card>\n    <ion-card-content>\n     \n      <ion-row>\n        <ion-col>\n            <ion-item>\n              <ion-label position=\"floating\">Search Customer</ion-label>\n              <ion-input formControlName=\"searchlead\" (ionChange)=\"onsearch()\" type=\"text\"></ion-input>\n            </ion-item>\n        </ion-col>\n      </ion-row> \n\n\n      <ion-row *ngIf=\"leadskm\">\n        <ion-col>\n          <ion-item float-left lines=\"none\" >\n            <ion-button slot=\"start\" color=\"primary\"  [disabled]=\"isfirst\"  text-center (click)=\"onPrevious()\">Previous</ion-button>\n          </ion-item>\n        </ion-col>\n        <ion-col>\n          <ion-item float-right lines=\"none\">\n            <ion-button slot=\"end\" color=\"primary\"  [disabled]=\"islast\"  text-center (click)=\"onNext()\">&nbsp;&nbsp;&nbsp;&nbsp;Next&nbsp;&nbsp;&nbsp;&nbsp;</ion-button>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-item *ngFor=\"let item of leadskm; index as i\"  text-wrap style=\"font-size: small;\">\n        <!-- <ion-content scrollX=\"true\"> -->\n          <div style=\"width: 100%;\">\n        <ion-row>\n          <ion-col size=\"2\" (click)=\"onAddLead(item)\" >\n            <ion-icon ios=\"ios-add-circle\" md=\"md-add-circle\" style=\"font-size: x-large;\">\n            </ion-icon>\n          </ion-col>\n          <ion-col size=\"8\">\n            <ion-label style=\"white-space: normal\">\n               {{ item.custname }}\n            </ion-label>\n            <ion-label style=\"font-size: small;\">Address: {{ item.addressname }}</ion-label>\n          </ion-col>\n          <ion-col size=\"2\">\n            <ion-label style=\"white-space: normal\">\n               {{ item.km}} km\n            </ion-label>\n          </ion-col>\n        </ion-row>\n      </div>\n        <!-- </ion-content> -->\n\n\n      </ion-item>\n\n<ion-row *ngIf=\"leadskm\">\n  <ion-col>\n    <ion-item float-left lines=\"none\" >\n      <ion-button slot=\"start\" color=\"primary\" text-center  [disabled]=\"isfirst\" (click)=\"onPrevious()\">Previous</ion-button>\n    </ion-item>\n  </ion-col>\n  <ion-col>\n    <ion-item float-right lines=\"none\">\n      <ion-button slot=\"end\" color=\"primary\" text-center  [disabled]=\"islast\" (click)=\"onNext()\">&nbsp;&nbsp;&nbsp;&nbsp;Next&nbsp;&nbsp;&nbsp;&nbsp;</ion-button>\n    </ion-item>\n  </ion-col>\n</ion-row>\n\n\n      <ion-row>\n        <ion-col>\n          <h5 ion-text class=\"text-primary\">\n            <ion-icon name=\"bookmarks\"></ion-icon>Selected Customers:\n          </h5>\n        </ion-col>\n      </ion-row>\n\n\n<ion-row>\n  <div style=\"overflow-x:auto\">\n    <ion-grid>\n     <ion-row nowrap >\n      <ion-col nowrap>\n      <ion-row nowrap>\n       <ion-col col-2 size=\"4\" class=\"grid-header\">Line Number</ion-col>\n       <ion-col size=\"8\" class=\"grid-header\">Customer</ion-col>\n       <ion-col size=\"8\" class=\"grid-header\">Address</ion-col>\n       <ion-col size=\"3\" class=\"grid-header\">Day</ion-col>\n       <ion-col size=\"8\" class=\"grid-header\">Date</ion-col>\n       <!-- <ion-col size=\"5\" class=\"grid-header\">Address Lat</ion-col> -->\n       <!-- <ion-col size=\"5\" class=\"grid-header\">Address Long</ion-col> -->\n       <ion-col size=\"2\" class=\"grid-header\">KM</ion-col>\n       <ion-col size=\"3\" class=\"grid-header\">Status</ion-col>\n\n      </ion-row>\n    </ion-col>\n     </ion-row>\n    \n     <ion-row *ngFor=\"let data of selectedleadskm; index as i\"  nowrap>\n      <ion-col nowrap>\n     <ion-row nowrap>\n        \n        <ion-col size=\"2\" style=\"width: 100%; text-align: right;\">\n          <!-- <ion-icon name=\"trash\" (click)=\"removeLeads(data)\" style=\"font-size: x-large;\n          color: red;\"></ion-icon> -->\n        </ion-col>\n        <ion-col size=\"2\" class=\"forecast_div\">{{i+1}}</ion-col>\n        <ion-col size=\"8\" class=\"forecast_div\">{{data.custname}}</ion-col>\n        <ion-col size=\"8\" class=\"forecast_div\">{{data.addressname}}</ion-col>\n        <ion-col  size=\"3\"class=\"forecast_div\">{{data.visit_day}}</ion-col>\n        <ion-col size=\"8\" class=\"forecast_div\">{{data.visit_date}}</ion-col>\n        <!-- <ion-col size=\"5\" class=\"forecast_div\">{{data.latitude}}</ion-col> -->\n        <!-- <ion-col size=\"5\" class=\"forecast_div\">{{data.longitude}}</ion-col> -->\n        <ion-col size=\"2\" class=\"forecast_div\">{{data.km}}</ion-col>\n        <ion-col size=\"3\" class=\"forecast_div\">{{data.status}}</ion-col>\n\n     \n\n      </ion-row>\n    \n    </ion-col>\n      </ion-row>\n   \n\n    </ion-grid>     \n  </div>\n</ion-row>\n   \n    </ion-card-content>\n  </ion-card>\n  <ion-button expand=\"block\" class=\"ion-margin-start ion-margin-end ion-margin-bottom btn-scheme\" type=\"submit\" [disabled]=\"!formaddactualplan.valid\" (click)=\"onSave()\">\n    Save\n    </ion-button>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_addedit-actual-travel-plan_addedit-actual-travel-plan_module_ts.js.map