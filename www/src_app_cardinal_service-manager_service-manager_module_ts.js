"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_cardinal_service-manager_service-manager_module_ts"],{

/***/ 93550:
/*!********************************************************************!*\
  !*** ./src/app/cardinal/service-manager/service-manager.module.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ServiceManagerPageModule": () => (/* binding */ ServiceManagerPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _service_manager_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./service-manager.page */ 75297);







const routes = [
    {
        path: '',
        component: _service_manager_page__WEBPACK_IMPORTED_MODULE_0__.ServiceManagerPage
    }
];
let ServiceManagerPageModule = class ServiceManagerPageModule {
};
ServiceManagerPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_service_manager_page__WEBPACK_IMPORTED_MODULE_0__.ServiceManagerPage]
    })
], ServiceManagerPageModule);



/***/ }),

/***/ 75297:
/*!******************************************************************!*\
  !*** ./src/app/cardinal/service-manager/service-manager.page.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ServiceManagerPage": () => (/* binding */ ServiceManagerPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _service_manager_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./service-manager.page.html?ngResource */ 22809);
/* harmony import */ var _service_manager_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./service-manager.page.scss?ngResource */ 43058);
/* harmony import */ var _service_manager_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./service-manager.service */ 89635);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/provider/commonfun */ 51156);








let ServiceManagerPage = class ServiceManagerPage {
  constructor(serviceManagerService, router, commonFunction) {
    this.serviceManagerService = serviceManagerService;
    this.router = router;
    this.commonFunction = commonFunction;
    /*
     *
     */

    this.TAG = "ServiceManagerPage";
  }

  ngOnInit() {
    console.log(this.TAG, "ngOnInit Fired");
  }

  ionViewWillEnter() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      console.log(_this.TAG, "ionViewWillEnter Fired");

      try {
        _this.complaintList = yield (yield _this.serviceManagerService.getComplaintList()).toPromise();
        console.log(_this.TAG, _this.complaintList);
      } catch (error) {
        _this.commonFunction.presentAlert("Compliant Report", "Error", error.error);

        console.error(_this.TAG, error);
      }
    })();
  }

  refreshPage() {
    try {
      this.ionViewWillEnter();
    } catch (error) {
      console.error();
    }
  }

  detailsClick(complaint) {
    try {
      console.log(this.TAG, "Get Details IS Called", complaint);
      let navigationExtras = {
        queryParams: {
          special: JSON.stringify(complaint)
        }
      };
      this.router.navigate(['service-manager-details'], navigationExtras);
    } catch (error) {
      console.error(this.TAG, error);
    }
  }

};

ServiceManagerPage.ctorParameters = () => [{
  type: _service_manager_service__WEBPACK_IMPORTED_MODULE_3__.ServiceManagerService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router
}, {
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__.Commonfun
}];

ServiceManagerPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
  selector: 'app-service-manager',
  template: _service_manager_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_service_manager_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ServiceManagerPage);


/***/ }),

/***/ 43058:
/*!*******************************************************************************!*\
  !*** ./src/app/cardinal/service-manager/service-manager.page.scss?ngResource ***!
  \*******************************************************************************/
/***/ ((module) => {

module.exports = ".arrow-custom {\n  background-color: lightgrey;\n  padding: 5px;\n  border-radius: 5px;\n}\n\n.doc-custom {\n  background-color: #C8E6C9;\n  padding: 5px;\n  border-radius: 5px;\n  color: #1B5E20;\n}\n\n.card-custom {\n  padding-left: 2px !important;\n  padding-right: 0px !important;\n  padding-top: 8px !important;\n  padding-bottom: 0% !important;\n}\n\n.arrow-col {\n  margin-top: 8px;\n}\n\n.complain-custom {\n  font-weight: bolder;\n  font-size: large;\n}\n\n.comname-custom {\n  color: darkgoldenrod;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2UtbWFuYWdlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwyQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUNBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBRUo7O0FBQUE7RUFDSSw0QkFBQTtFQUNBLDZCQUFBO0VBQ0EsMkJBQUE7RUFDQSw2QkFBQTtBQUdKOztBQURBO0VBQ0ksZUFBQTtBQUlKOztBQUZBO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtBQUtKOztBQUhBO0VBQ0ksb0JBQUE7QUFNSiIsImZpbGUiOiJzZXJ2aWNlLW1hbmFnZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFycm93LWN1c3RvbXtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyZXk7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cbi5kb2MtY3VzdG9te1xuICAgIGJhY2tncm91bmQtY29sb3I6I0M4RTZDOTtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGNvbG9yOiAjMUI1RTIwO1xufVxuLmNhcmQtY3VzdG9te1xuICAgIHBhZGRpbmctbGVmdDogMnB4ICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZy1yaWdodDogMHB4ICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZy10b3A6IDhweCAhaW1wb3J0YW50O1xuICAgIHBhZGRpbmctYm90dG9tOiAwJSAhaW1wb3J0YW50O1xufVxuLmFycm93LWNvbHtcbiAgICBtYXJnaW4tdG9wOiA4cHg7XG59XG4uY29tcGxhaW4tY3VzdG9te1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgZm9udC1zaXplOiBsYXJnZTtcbn1cbi5jb21uYW1lLWN1c3RvbXtcbiAgICBjb2xvcjogZGFya2dvbGRlbnJvZDtcbiAgIC8vIGZvbnQtc2l6ZTogaW5pdGlhbDtcbn0iXX0= */";

/***/ }),

/***/ 22809:
/*!*******************************************************************************!*\
  !*** ./src/app/cardinal/service-manager/service-manager.page.html?ngResource ***!
  \*******************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Compliant Report List\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <ion-buttons (click)=\"refreshPage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>           \n  <ion-list>\n    <ion-card class=\"card-custom\" *ngFor=\"let complaint of complaintList\" (click)=\"detailsClick(complaint)\" padding>\n      <ion-row>\n        <ion-col size=\"4\">\n         <ion-label class=\"comname-custom\">\n           {{complaint.nameofcomplainer}}\n         </ion-label>\n        </ion-col>\n        <ion-col size=\"4\" text-left>\n         <ion-label>\n           {{complaint.complaintdate | date:'dd-MMM-yyyy'}}\n         </ion-label>\n        </ion-col>\n        <ion-col size=\"4\" text-end>\n         <ion-label class=\"doc-custom\">\n           {{complaint.doctype[0].name}}\n         </ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"10\">\n         <ion-label class=\"complain-custom\">\n           {{complaint.complaintno}}\n         </ion-label>\n        </ion-col>\n        <ion-col size=\"2\" text-end>\n          <div class=\"arrow-col\">\n           <ion-icon name=\"arrow-forward\" class=\"arrow-custom\"></ion-icon>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n    <!-- <ion-card *ngFor=\"let complaint of complaintList\" (click)=\"detailsClick(complaint)\" padding>\n      <ion-row>\n      <ion-col>\n        <ion-label style=\"color: #bc477b;font-weight: bolder;\">\n          <h2>\n            {{complaint.nameofcomplainer}}\n          </h2>\n          <p>\n            {{complaint.complaintno}}\n          </p>\n        </ion-label>\n      </ion-col>\n      <ion-col>\n        <ion-label>\n          {{complaint.complaintdate | date:'dd-MM-yyyy'}}\n        </ion-label>\n      </ion-col>\n      <ion-col text-end>\n        <ion-icon name=\"arrow-forward\"></ion-icon>\n      </ion-col>\n      </ion-row>\n    </ion-card> -->\n  </ion-list>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_cardinal_service-manager_service-manager_module_ts.js.map