"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_cardinal_service-engineer-visit_service-engineer-visit-detail_service-engineer-visit--bb3b16"],{

/***/ 99394:
/*!***********************************************************************************************************************!*\
  !*** ./src/app/cardinal/service-engineer-visit/service-engineer-visit-detail/service-engineer-visit-detail.module.ts ***!
  \***********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ServiceEngineerVisitDetailPageModule": () => (/* binding */ ServiceEngineerVisitDetailPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _service_engineer_visit_detail_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./service-engineer-visit-detail.page */ 55789);
/* harmony import */ var src_app_material_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/material.module */ 63806);
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/checkbox */ 44792);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/input */ 68562);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/form-field */ 75074);
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/list */ 6517);












const routes = [
    {
        path: '',
        component: _service_engineer_visit_detail_page__WEBPACK_IMPORTED_MODULE_0__.ServiceEngineerVisitDetailPage
    }
];
let ServiceEngineerVisitDetailPageModule = class ServiceEngineerVisitDetailPageModule {
};
ServiceEngineerVisitDetailPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            src_app_material_module__WEBPACK_IMPORTED_MODULE_1__.MaterialModule,
            _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_7__.MatCheckboxModule,
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__.MatFormFieldModule,
            _angular_material_input__WEBPACK_IMPORTED_MODULE_9__.MatInputModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule,
            _angular_material_list__WEBPACK_IMPORTED_MODULE_10__.MatListModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_11__.RouterModule.forChild(routes)
        ],
        declarations: [_service_engineer_visit_detail_page__WEBPACK_IMPORTED_MODULE_0__.ServiceEngineerVisitDetailPage]
    })
], ServiceEngineerVisitDetailPageModule);



/***/ }),

/***/ 55789:
/*!*********************************************************************************************************************!*\
  !*** ./src/app/cardinal/service-engineer-visit/service-engineer-visit-detail/service-engineer-visit-detail.page.ts ***!
  \*********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ServiceEngineerVisitDetailPage": () => (/* binding */ ServiceEngineerVisitDetailPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _service_engineer_visit_detail_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./service-engineer-visit-detail.page.html?ngResource */ 2113);
/* harmony import */ var _service_engineer_visit_detail_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./service-engineer-visit-detail.page.scss?ngResource */ 5367);
/* harmony import */ var _customer_service_customer_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../customer-service/customer-service.service */ 59813);
/* harmony import */ var _service_engineer_visit_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../service-engineer-visit.service */ 39797);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var src_provider_validator_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/provider/validator-helper */ 35096);
/* harmony import */ var src_app_neworder_neworder_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/neworder/neworder.service */ 17216);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var src_assets_model_complain__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/assets/model/complain */ 6337);
/* harmony import */ var src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/login/loginauth.service */ 44010);
/* harmony import */ var _awesome_cordova_plugins_image_picker_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @awesome-cordova-plugins/image-picker/ngx */ 92153);
/* harmony import */ var _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @awesome-cordova-plugins/camera/ngx */ 64587);
/* harmony import */ var src_provider_message_helper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/provider/message-helper */ 98792);
/* harmony import */ var _ionic_native_file_chooser_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/file-chooser/ngx */ 86786);
/* harmony import */ var _awesome_cordova_plugins_file_path_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @awesome-cordova-plugins/file-path/ngx */ 3501);
/* harmony import */ var _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @awesome-cordova-plugins/file/ngx */ 25453);
/* harmony import */ var src_app_common_Constants__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! src/app/common/Constants */ 68209);
/* harmony import */ var _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @awesome-cordova-plugins/file-transfer/ngx */ 80464);
/* harmony import */ var _ionic_native_file_picker_ngx__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ionic-native/file-picker/ngx */ 63507);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! date-fns */ 86527);

























let ServiceEngineerVisitDetailPage = class ServiceEngineerVisitDetailPage {
  constructor(route, router, formBuilder, validator, neworderservice, serviceEngineerVisitService, modalController, loginService, commonFunction, alertCtrl, customerService, imagePicker, camera, msg, platform, fileChooser, filePath, file, transfer, filePicker) {
    this.route = route;
    this.router = router;
    this.formBuilder = formBuilder;
    this.validator = validator;
    this.neworderservice = neworderservice;
    this.serviceEngineerVisitService = serviceEngineerVisitService;
    this.modalController = modalController;
    this.loginService = loginService;
    this.commonFunction = commonFunction;
    this.alertCtrl = alertCtrl;
    this.customerService = customerService;
    this.imagePicker = imagePicker;
    this.camera = camera;
    this.msg = msg;
    this.platform = platform;
    this.fileChooser = fileChooser;
    this.filePath = filePath;
    this.file = file;
    this.transfer = transfer;
    this.filePicker = filePicker; // @ViewChild('spareRequiredFormGroup',{ static: false }) spareRequiredFormGroup;

    this.TAG = "ServiceEngineerVisitDetailPage";
    this.isLinear = false;
    /**
     *
     */

    this.detailsStepValid = false;
    this.equimentStepValid = false;
    this.customerDetailStepValid = false;
    this.engineerDetailStepValid = false;
    this.spareRequiredStepValid = false;
    /**
     *
     */

    this.img = '';
    this.fileUrl = '';
    this.isdesktop = false;
    this.datereceived = '';
    this.datecomplete = '';
    this.datedef = '';
    this.datedefspare = '';
    this.spareInstallationStepValid = false;
    this.assignToCtrlVisible = false;
    this.closureAtFieldBtnVisible = false;
    this.engineerDetailsNextButtonVisible = false;
    this.isClosureATFieldCtrlVisibleActive = false;
    this.validation_messages = {
      'nameOfComplainer': [{
        type: 'required',
        message: '*Please Enter Name of complainer.'
      }],
      'designation_of_complainer_mss': [{
        type: 'required',
        message: '*Please Enter designation_of_complainer.'
      }],
      'mobileno': [{
        type: 'required',
        message: '*Please Enter Contact No.'
      }, {
        type: 'InvalidNumber',
        message: '*Please Enter Valid Contact No.'
      }],
      'email': [{
        type: 'required',
        message: '*Please Enter Email.'
      }, {
        type: 'InvalidEmail',
        message: '*Please Enter Valid Email.'
      }],
      'complaintDescription': [{
        type: 'required',
        message: '*Please Enter Complaint Description'
      }],
      'serialNo': [{
        type: 'required',
        message: '*Please Enter Serial no .'
      }],
      'customerAddress1': [{
        type: 'required',
        message: '*Please Enter Customer Address1'
      }],
      'customerAddress2': [{
        type: 'required',
        message: '*Please Enter Customer Customer Address 2'
      }],
      'customerAddress3': [{
        type: 'required',
        message: '*Please Enter Customer Customer Address 3'
      }],
      'pinCode': [{
        type: 'required',
        message: '*Please Enter Pin Code'
      }],
      'area': [{
        type: 'required',
        message: '*Please Select Your Area'
      }],
      'problemObservedMessage': [{
        type: 'required',
        message: '*Please Enter Problem Observed'
      }],
      'fieldVisitRemarksMessage': [{
        type: 'required',
        message: '*Please Enter Remark'
      }],
      'spareSKUCodeDropDownCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select SKU Code'
      }],
      'proposedActionCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Proposed Action'
      }],
      'spareReceivedDateCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Spare Received Date'
      }],
      'complaintStatusCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Complaint Status'
      }],
      'completionDateCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Completion Date'
      }],
      'serviceAttendedCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Enter Service Attended'
      }],
      'quantityCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Enter Quantity'
      }],
      'errorCodeCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Error Code'
      }],
      'replacedSparePartSerialNoCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Enter Replaced Spare Part Serial No'
      }],
      'defectiveSparePartNoCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Enter Defective Spare Part No'
      }],
      'copSalesOrderReferenceDropDownCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select COP Sales Order Reference'
      }],
      'skuCodeCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select SKU Code'
      }],
      'spareReceivedQuantityCtrlErrorMessage': [{
        type: 'required',
        message: '*Quantity can not be empty'
      }]
    };
    this.imgcomp = [];
    this.punchOrderSuccess = false;
    this.isPunchOrderVisible = false;
    this.isClosureATFieldCtrlVisible = false;
    this.isSpareInstallationTabVisible = false;
    this.isSpareInstallClosedCtrlVisible = false;
    this.isProductCompliantTab = false;
    this.isConsumablesTab = false;
    this.isEquimentTab = false;
    this.isDealerBillingAddress = false;
    this.isDealerShippingAddress = false;
    this.isAttachmentTabVisible = false;
    this.isServiceManagerTabVisible = false;
    this.isSpareRequiredTabVisible = false;
    this.tempCartStatus = false;
    this.spareOrderedTabVisible = false;
    this.complaintStatusControlVisible = false;
  }

  ngOnInit() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.route.queryParams.subscribe(params => {
        if (params && params.special) {
          _this.service = JSON.parse(params.special);
          console.log(_this.TAG, _this.service);
        }
      });

      _this.img = '';
      _this.fileUrl = "";
      _this.detailFormGroup = _this.formBuilder.group({
        nameOfComplainerCtrl: [],
        desigOfComplainerCtrl: [],
        mobilenoCtrl: [],
        emailIDCtrl: [],
        complaintDescriptionCtrl: []
      });
      _this.productCompliantFormGroup = _this.formBuilder.group({});
      _this.consumablesFormGroup = _this.formBuilder.group({});
      _this.equimentFormGroup = _this.formBuilder.group({
        serialNoCtrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        dealerNameCtrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        errorCodeCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required]
      });
      _this.skuDetailsFormGroup = _this.formBuilder.group({});
      _this.customerDetailFormGroup = _this.formBuilder.group({
        customerAddress1Ctrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        customerAddress2Ctrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        customerAddress3Ctrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        pinCodeCtrl: [{
          value: '',
          disabled: true
        }, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        approveComplaintCtrl: [,],
        rejectComplaintCtrl: [,]
      });
      _this.venderFormGroup = _this.formBuilder.group({
        serviceEngineerNameCtrl: [],
        serviceEngMobilenoCtrl: [],
        serviceVendorRemarksCtrl: [],
        dateOfVisitCtrl: []
      });
      _this.engineerDetailsFormGroup = _this.formBuilder.group({
        problemObservedCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        fieldVisitRemarksCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        proposedActionCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        dealerBillingAddressCtrl: [,],
        dealerShippingAddressCtrl: [,],
        assignToCtrl: [,],
        punchOrderCtrl: [,],
        closureATFieldCtrl: [,],
        complaintStatusCtrl: []
      });
      _this.spareInstallationFormGroup = _this.formBuilder.group({
        copSalesOrderReferenceDropDownCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        skuCodeCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        spareReceivedDateCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        spareReceivedQuantityCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        completionDateCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        replacedSparePartSerialNoCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        serviceAttendedCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        defectiveSparePartNoCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        repairReportCtrl: [,],
        defSpareDocketNoCtrl: [,],
        defSpareCourierCtrl: [,],
        defSpareSentDateCtrl: [,],
        spareInstallClosedCtrl: [,]
      });
      _this.serviceManagerFormGroup = _this.formBuilder.group({
        serviceManagerRemarkCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        defectiveSparePartNoCtrl: [,],
        defectiveSparePartReceivedDateCtrl: [,],
        smartSolveRefNoCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        finalClosureCtrl: [,]
      });
      _this.spareRequiredFormGroup = _this.formBuilder.group({
        spareSKUCodeDropDownCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_19__.Validators.required],
        quantityCtrl: [],
        dealerNameSpareRequiredCtrl: [],
        copSalesOrderReferenceCtrl: [,]
      });
      _this.attachmentFormGroup = _this.formBuilder.group({
        attachmentControlCtrl: []
      });
      _this.spareOrderedTab = _this.formBuilder.group({});
      _this.serviceManagerList = yield (yield _this.serviceEngineerVisitService.getServiceManagerList()).toPromise();
      _this.proposedActionList = yield (yield _this.serviceEngineerVisitService.getProposedActionList()).toPromise();
      _this.complaintStatusList = yield (yield _this.serviceEngineerVisitService.getComplaintStatus()).toPromise(); //  this.spareRequiredFormGroup.statusChanges
      //  .subscribe(val => this.onFormValid(val));
    })();
  }

  formatDate(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_20__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_21__["default"])(value), 'dd-MM-yyyy');
  }

  formatDate1(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_20__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_21__["default"])(value), 'dd-MM-yyyy');
  }

  formatDate2(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_20__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_21__["default"])(value), 'dd-MM-yyyy');
  }

  formatDate3(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_20__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_21__["default"])(value), 'dd-MM-yyyy');
  }

  ionViewWillEnter() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this2.spareRequiredFormGroup.statusChanges.subscribe(result => {
        // console.log("From Data",result);
        if (result == "VALID") {//  this.tempCartStatus = true;
        }
      });

      _this2.tempNewDealer = _this2.service.newdealername;

      _this2.commonFunction.loadingPresent();

      if (!_this2.msg.isplatformweb) {
        _this2.isdesktop = false;
      } else {
        _this2.isdesktop = true;
      }

      _this2.tempSpareCart = [];
      _this2.errorCodeList = yield (yield _this2.customerService.getErrorCodeList()).toPromise();
      _this2.proposedActionList = yield (yield _this2.serviceEngineerVisitService.getProposedActionList()).toPromise(); //  console.log(this.TAG,"Proposed action list",this.proposedActionList);

      _this2.complaintStatusList = yield (yield _this2.serviceEngineerVisitService.getComplaintStatus()).toPromise();
      _this2.serviceManagerList = yield (yield _this2.serviceEngineerVisitService.getServiceManagerList()).toPromise();
      let billingAddressResponse = yield (yield _this2.neworderservice.getcustmerbillingaddress(_this2.service.dealerid)).toPromise();
      const response = billingAddressResponse['response'];
      var jsondata = response.data;
      _this2.dealerBillingAddressList = jsondata.filter(e => e.invoiceToAddress == true);
      _this2.dealerShippingAddressList = jsondata.filter(e => e.shipToAddress == true);

      if (_this2.dealerBillingAddressList.length == 1) {
        _this2.engineerDetailsFormGroup.controls["dealerBillingAddressCtrl"].setValue(_this2.dealerBillingAddressList[0]);
      }

      if (_this2.dealerShippingAddressList.length == 1) {
        _this2.engineerDetailsFormGroup.controls["dealerShippingAddressCtrl"].setValue(_this2.dealerShippingAddressList[0]);
      } //this.venderFormGroup.get('serviceEngineerNameCtrl').disable();
      //this.venderFormGroup.get('serviceEngMobilenoCtrl').disable();
      //  this.venderFormGroup.get('dateOfVisitCtrl').disable();
      // this.venderFormGroup.get('serviceVendorRemarksCtrl').disable();


      _this2.spareRequiredFormGroup.get('copSalesOrderReferenceCtrl').disable();

      if (_this2.service.doctype[0].name == 'Product Compliant') {
        _this2.isProductCompliantTab = true;
        _this2.isConsumablesTab = false;
        _this2.isEquimentTab = false;
      } else if (_this2.service.doctype[0].name == 'Consumables') {
        _this2.isProductCompliantTab = false;
        _this2.isConsumablesTab = true;
        _this2.isEquimentTab = false;
      } else if (_this2.service.doctype[0].name == 'Equipment') {
        _this2.isProductCompliantTab = false;
        _this2.isConsumablesTab = false;
        _this2.isEquimentTab = true;
      }

      _this2.spareSKUCodeList = _this2.service.sparesku; // console.log(this.TAG, "Spare SKU Code", this.spareSKUCodeList);

      _this2.contractTypeList = yield (yield _this2.customerService.getContractTypeList()).toPromise();

      for (let i = 0; i < _this2.contractTypeList.length; i++) {
        if (_this2.contractTypeList[i].code == _this2.service.contracttype) {
          _this2.contracttypeSelected = _this2.contractTypeList[i].name;
        }
      }

      for (let i = 0; i < _this2.errorCodeList.length; i++) {
        if (_this2.errorCodeList[i].code == _this2.service.errorcode[0].code) {
          _this2.errorCodeSelected = _this2.errorCodeList[i];
        }
      } // this.errorCodeSelected = this.service.errorcode[0];


      if (_this2.service.producttobereturn === 'MSNR_N') {
        _this2.productToBeReturn = 'No';
      } else if (_this2.service.producttobereturn === 'MSNR_Y') {
        _this2.productToBeReturn = 'Yes';
      }

      if (!!_this2.service.closureatfield == true) {
        _this2.engineerDetailsFormGroup.controls["problemObservedCtrl"].setValue(_this2.service.probobserv);

        _this2.engineerDetailsFormGroup.get('problemObservedCtrl').disable();

        _this2.engineerDetailsFormGroup.controls["fieldVisitRemarksCtrl"].setValue(_this2.service.fieldvisitremark);

        _this2.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').disable();

        for (let i = 0; i < _this2.proposedActionList.length; i++) {
          if (_this2.proposedActionList[i].code == _this2.service.proposedaction.code) {
            _this2.proposedActionSelected = _this2.proposedActionList[i];

            _this2.engineerDetailsFormGroup.get('proposedActionCtrl').disable();
          }
        }

        if (_this2.service.proposedaction.code == 'SR') {
          _this2.isDealerBillingAddress = true;

          _this2.engineerDetailsFormGroup.get('dealerBillingAddressCtrl').disable();

          _this2.isDealerShippingAddress = true;

          _this2.engineerDetailsFormGroup.get('dealerShippingAddressCtrl').disable();
        }

        _this2.assignToCtrlVisible = true;

        for (let i = 0; i < _this2.serviceManagerList.length; i++) {
          if (_this2.serviceManagerList[i].id == _this2.service.assigntofieldvisit.id) {
            _this2.assignToSelected = _this2.serviceManagerList[i];

            _this2.engineerDetailsFormGroup.get('assignToCtrl').disable();
          }
        }

        _this2.engineerDetailsNextButtonVisible = true;
        _this2.isServiceManagerTabVisible = true;

        _this2.equimentFormGroup.get('errorCodeCtrl').disable();

        setTimeout(() => {
          for (let i = 0; i < _this2.dealerBillingAddressList.length; i++) {
            if (_this2.dealerBillingAddressList[i].id == _this2.service.engineerdetails.dealerbilladd.id) {
              _this2.dealerBillingAddressSelected = _this2.dealerBillingAddressList[i];
              _this2.isDealerBillingAddress = true;

              _this2.engineerDetailsFormGroup.get('dealerBillingAddressCtrl').disable();
            }
          }

          for (let i = 0; i < _this2.dealerShippingAddressList.length; i++) {
            if (_this2.dealerShippingAddressList[i].id == _this2.service.engineerdetails.dealershipadd.id) {
              _this2.dealerShippingAddressSelected = _this2.dealerShippingAddressList[i];
              _this2.isDealerShippingAddress = true;

              _this2.engineerDetailsFormGroup.get('dealerShippingAddressCtrl').disable();
            }
          }
        }, 300);
      }

      try {
        //New Change
        if (!!_this2.service.closureatfield == !true && !!_this2.service.engineerdetails.proposedaction && Object.keys(_this2.service.engineerdetails.proposedaction).length != 0) {
          _this2.engineerDetailsFormGroup.controls["problemObservedCtrl"].setValue(_this2.service.engineerdetails.probobserv);

          _this2.engineerDetailsFormGroup.get('problemObservedCtrl').disable();

          _this2.engineerDetailsFormGroup.controls["fieldVisitRemarksCtrl"].setValue(_this2.service.engineerdetails.fieldvisitremark);

          _this2.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').disable();

          for (let i = 0; i < _this2.proposedActionList.length; i++) {
            if (_this2.proposedActionList[i].code == _this2.service.engineerdetails.proposedaction.code) {
              _this2.proposedActionSelected = _this2.proposedActionList[i];

              _this2.engineerDetailsFormGroup.get('proposedActionCtrl').disable();
            }
          }

          setTimeout(() => {
            for (let i = 0; i < _this2.dealerBillingAddressList.length; i++) {
              if (_this2.dealerBillingAddressList[i].id == _this2.service.engineerdetails.dealerbilladd.id) {
                _this2.dealerBillingAddressSelected = _this2.dealerBillingAddressList[i];
                _this2.isDealerBillingAddress = true;

                _this2.engineerDetailsFormGroup.get('dealerBillingAddressCtrl').disable();
              }
            }

            for (let i = 0; i < _this2.dealerShippingAddressList.length; i++) {
              if (_this2.dealerShippingAddressList[i].id == _this2.service.engineerdetails.dealershipadd.id) {
                _this2.dealerShippingAddressSelected = _this2.dealerShippingAddressList[i];
                _this2.isDealerShippingAddress = true;

                _this2.engineerDetailsFormGroup.get('dealerShippingAddressCtrl').disable();
              }
            }
          }, 300);
          _this2.isSpareRequiredTabVisible = true;
          _this2.isSpareInstallationTabVisible = true;
          _this2.complaintStatusControlVisible = true;
          _this2.copSalesOrderReferenceList = _this2.service.orders;
          _this2.spareRequiredList = _this2.service.spare_required_list;

          if (!!_this2.spareRequiredList) {
            _this2.spareOrderedTabVisible = true;
          } // this.spareSKUCodeList =  this.service.sparerequired;


          if (_this2.service.sparerequired.sparerequest == false) {
            // this.spareRequiredFormGroup.valid = true;
            _this2.punchOrderSuccess = false;

            _this2.spareRequiredFormGroup.enable();

            _this2.spareRequiredFormGroup.get('spareSKUCodeDropDownCtrl').enable();

            _this2.spareRequiredFormGroup.get('quantityCtrl').enable();

            _this2.spareRequiredFormGroup.get('dealerNameSpareRequiredCtrl').enable(); // console.log("Form Data",this.spareRequiredFormGroup);

          } else {
            _this2.punchOrderSuccess = true;
          }
        }
      } catch (error) {// console.log(this.TAG,error);
      }

      _this2.commonFunction.loadingDismiss();
    })();
  }

  refreshPage() {
    try {} catch (error) {//  console.error(this.TAG, error);
    }
  }

  matDetailSettper(event) {
    try {} catch (error) {// console.error(this.TAG, error);
    }
  }

  matSettperProductCompliant(event) {
    try {} catch (error) {// console.error(this.TAG, error);
    }
  }

  matSettperConsumables(event) {
    try {} catch (error) {// console.error(this.TAG, error);
    }
  }

  matSettperSKUDetails(value) {
    try {} catch (error) {//  console.error(this.TAG, error);
    }
  }

  matSettperEquiment(event) {
    try {
      this.equimentStepValid = true;
    } catch (error) {// console.log(this.TAG, error);
    }
  }

  matSettperServiceManager() {
    try {} catch (error) {// console.log(this.TAG, error);
    }
  }

  matSettperSpareRequired(selectedValue) {
    try {
      this.spareRequiredStepValid = true; // console.log("Form Spare Required", selectedValue);
    } catch (error) {//  console.log(this.TAG, error);
    }
  }

  matSettperEngineerDetails() {
    try {
      this.engineerDetailStepValid = true;
    } catch (error) {// console.log(this.TAG, error);
    }
  }

  errorCodeSelectedChange(selected) {
    try {
      //  console.log(this.TAG,selected);
      this.service.errorcode[0] = selected;
    } catch (error) {}
  }

  serviceEngSelectedChange(serviceEngSelected) {
    try {} catch (error) {//  console.error(this.TAG, error);
    }
  }

  approveServiceCheckbox(event) {
    try {//  console.log(this.TAG, "CheckBox Value", event);
    } catch (error) {//  console.error(this.TAG, error);
    }
  }

  rejectServiceCheckbox() {
    try {} catch (error) {//  console.error(this.TAG, error);
    }
  }

  punchOrderCheckbox(event) {
    try {
      if (event) {
        this.isSpareInstallationTabVisible = true;
      } else {
        this.isSpareInstallationTabVisible = false;
      }
    } catch (error) {//  console.error(this.TAG, error);
    }
  }

  proposedActionClick(actionSelected) {
    try {
      // console.log(this.TAG, "Proposed action selected", actionSelected);
      if (this.dealerBillingAddressList.length == 1) {
        this.engineerDetailsFormGroup.controls["dealerBillingAddressCtrl"].setValue(this.dealerBillingAddressList[0]);
      }

      if (this.dealerShippingAddressList.length == 1) {
        this.engineerDetailsFormGroup.controls["dealerShippingAddressCtrl"].setValue(this.dealerShippingAddressList[0]);
      }

      if (actionSelected.code == 'SR') {
        this.isPunchOrderVisible = true;
        this.isClosureATFieldCtrlVisible = false;
        this.isSpareRequiredTabVisible = true;
        this.isDealerBillingAddress = true;
        this.isDealerShippingAddress = true;
        this.isSpareInstallationTabVisible = false;
        this.spareRequiredFormGroup.get('dealerNameSpareRequiredCtrl').disable();
        setTimeout(() => {
          if (this.dealerBillingAddressList.length == 1) {
            this.engineerDetailsFormGroup.controls["dealerBillingAddressCtrl"].setValue(this.dealerBillingAddressList[0]);
          }

          if (this.dealerShippingAddressList.length == 1) {
            this.engineerDetailsFormGroup.controls["dealerShippingAddressCtrl"].setValue(this.dealerShippingAddressList[0]);
          }
        }, 300); // this.isAttachmentTabVisible = false;

        this.engineerDetailsNextButtonVisible = true;
        this.assignToCtrlVisible = false;
      } else if (actionSelected.code == 'SNR') {
        this.isPunchOrderVisible = false; //  this.isClosureATFieldCtrlVisible = true;
        //  this.isAttachmentTabVisible = true;

        this.isSpareInstallationTabVisible = false;
        this.isDealerBillingAddress = false;
        console.log("Bill", this.isDealerBillingAddress);
        this.isDealerShippingAddress = false;
        this.isSpareRequiredTabVisible = false;
        this.complaintStatusControlVisible = false;
        this.assignToCtrlVisible = true;
      } else if (actionSelected.code == 'NSCT') {
        this.isPunchOrderVisible = false;
        this.isClosureATFieldCtrlVisible = true;
        this.isSpareInstallationTabVisible = false;
        this.isDealerBillingAddress = false;
        console.log("Bill", this.isDealerBillingAddress);
        this.isDealerShippingAddress = false;
        this.isSpareRequiredTabVisible = false; // this.isAttachmentTabVisible = true;

        this.assignToCtrlVisible = true;
        this.complaintStatusControlVisible = false;
      }
    } catch (error) {//  console.error(this.TAG, error);
    }
  }

  complaintStatusClick(complaintStatusSelected) {
    try {
      // console.log(this.TAG, complaintStatusSelected);
      if (complaintStatusSelected.code == 'NS') {
        this.isSpareInstallClosedCtrlVisible = false;
        this.spareRequiredFormGroup.reset();

        if (!!this.service.newdealername) {
          this.spareRequiredFormGroup.controls.dealerNameSpareRequiredCtrl.patchValue(this.service.newdealername);
        } else {
          this.spareRequiredFormGroup.controls.dealerNameSpareRequiredCtrl.patchValue(this.service.dealername);
        }

        this.punchOrderSuccess = false;
        this.spareRequiredFormGroup.get('spareSKUCodeDropDownCtrl').enable();
        this.spareRequiredFormGroup.get('quantityCtrl').enable();
        this.spareRequiredFormGroup.get('dealerNameSpareRequiredCtrl').enable(); //New Change

        this.isSpareRequiredTabVisible = true;
        this.isSpareInstallationTabVisible = true;
        this.complaintStatusControlVisible = true;
        this.assignToCtrlVisible = false;
      } else if (complaintStatusSelected.code == 'CAHTSC') {
        this.isSpareInstallClosedCtrlVisible = false;
        this.assignToCtrlVisible = true;
        this.spareOrderedTabVisible = false;
        this.engineerDetailsFormGroup.get('problemObservedCtrl').disable();
        this.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').disable();
        this.engineerDetailsFormGroup.get('proposedActionCtrl').disable();
        this.engineerDetailsFormGroup.get('dealerBillingAddressCtrl').disable();
        this.engineerDetailsFormGroup.get('dealerShippingAddressCtrl').disable(); //New Change

        this.isSpareRequiredTabVisible = false;
        this.isSpareInstallationTabVisible = false;
        this.assignToCtrlVisible = true;
      } else if (complaintStatusSelected.code == 'CL') {
        this.isSpareInstallClosedCtrlVisible = true;
        this.assignToCtrlVisible = true;
        this.spareOrderedTabVisible = false;
        this.engineerDetailsFormGroup.get('problemObservedCtrl').disable();
        this.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').disable();
        this.engineerDetailsFormGroup.get('proposedActionCtrl').disable();
        this.engineerDetailsFormGroup.get('dealerBillingAddressCtrl').disable();
        this.engineerDetailsFormGroup.get('dealerShippingAddressCtrl').disable(); //New Change

        this.isSpareRequiredTabVisible = false;
        this.isSpareInstallationTabVisible = false;
        this.assignToCtrlVisible = true;
      }
    } catch (error) {//  console.log(this.TAG, error);
    }
  }

  spareInstallClosedCheckbox(value) {
    try {
      // console.log(this.TAG, "Spare Install Closed", value);
      if (value) {//this.closureAtFieldBtnVisible = true;
        //  this.assignToCtrlVisible = true;
      } else {
        this.closureAtFieldBtnVisible = false; // this.assignToCtrlVisible = false;
      }
    } catch (error) {//  console.error(this.TAG, error);
    }
  }

  dealerBillingAddressClick(data) {
    try {} catch (error) {//  console.log(this.TAG, error);
    }
  }

  assignToClick() {
    try {
      // console.log("assignToClick Fired", value);
      //this.closureAtFieldBtnVisible = true;
      this.isAttachmentTabVisible = true;
      this.equimentFormGroup.get('errorCodeCtrl').disable();
    } catch (error) {//  console.log(this.TAG, error);
    }
  }

  dealerShippingAddressClick() {
    try {} catch (error) {//  console.log(this.TAG, error);
    }
  }

  spareSKUCodeClick(selectedSpare) {
    try {
      console.log(this.TAG, selectedSpare);
      this.spareRequiredFormGroup.controls["quantityCtrl"].setValue("1");
    } catch (error) {// console.log(this.TAG, error);
    }
  }

  onFormValid(val) {
    try {
      //  console.log("Attached Form Status", val);
      this.isClosureATFieldCtrlVisibleActive = true;
    } catch (val) {}
  }

  finalClosureCheckbox(finalClosureValue) {
    try {
      //  console.log(this.TAG, finalClosureValue);
      if (finalClosureValue) {
        this.serviceManagerFormGroup.get('serviceManagerRemarkCtrl').disable();
        this.serviceManagerFormGroup.get('defectiveSparePartNoCtrl').disable();
        this.serviceManagerFormGroup.get('defectiveSparePartReceivedDateCtrl').disable();
        this.serviceManagerFormGroup.get('smartSolveRefNoCtrl').disable();
        this.isClosureATFieldCtrlVisible = true;
      } else {
        this.serviceManagerFormGroup.get('serviceManagerRemarkCtrl').enable();
        this.serviceManagerFormGroup.get('defectiveSparePartNoCtrl').enable();
        this.serviceManagerFormGroup.get('defectiveSparePartReceivedDateCtrl').enable();
        this.serviceManagerFormGroup.get('smartSolveRefNoCtrl').enable();
        this.isClosureATFieldCtrlVisible = false;
      }
    } catch (error) {//  console.log(this.TAG, error);
    }
  }

  presentAlert(Header, SubHeader, Message) {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this3.alertCtrl.create({
        header: SubHeader,
        subHeader: '',
        message: Message,
        buttons: [{
          text: "OK",
          handler: ok => {
            if (Header == 'closer_success') {
              _this3.router.navigateByUrl('/service-engineer-visit');
            } else {// console.log(this.TAG, "In Present Alert Error");
            }
          }
        }]
      });
      alert.dismiss(() => console.log('The alert has been closed.'));
      yield alert.present();
    })();
  }

  presentSpareInstallAlert(Header, SubHeader, Message) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this4.alertCtrl.create({
        header: SubHeader,
        subHeader: '',
        message: Message,
        buttons: [{
          text: "OK",
          handler: ok => {
            let temp = _this4.spareInstallationFormGroup.get('complaintStatusCtrl').value ? _this4.spareInstallationFormGroup.get('complaintStatusCtrl').value.code : '';

            if (temp == 'NS') {
              // this.isSpareRequiredTabVisible = false;
              _this4.isSpareInstallationTabVisible = false;

              _this4.spareInstallationFormGroup.reset();

              _this4.stepper.previous();
            } else {
              //this.stepper.previous();
              // 
              _this4.isSpareRequiredTabVisible = false;
              _this4.isSpareInstallationTabVisible = false;
              _this4.isAttachmentTabVisible = true;
            }
          }
        }]
      });
      alert.dismiss(() => console.log('The alert has been closed.'));
      yield alert.present();
    })();
  }

  ChosePic() {
    var _this5 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this5.alertCtrl.create({
        header: 'Select Option',
        message: "Select Option to get Picture.",
        buttons: [{
          text: 'Gallery',
          handler: data => {
            _this5.getimage();
          }
        }, {
          text: 'Camera',
          handler: data => {
            _this5.takePicture();
          }
        }, {
          text: 'PDF',
          handler: data => {
            _this5.attachPDF();
          }
        }]
      });
      yield alert.present();
    })();
  }

  attachPDF() {
    try {
      if (this.platform.is('android')) {
        let filter = {
          "mime": "application/pdf"
        };
        this.fileChooser.open().then(uri => {
          console.log(uri);
          this.filePath.resolveNativePath(uri).then(filePathResult => {
            console.log(this.TAG, "Selected fileInfo", filePathResult);
            this.file.resolveLocalFilesystemUrl(filePathResult).then(fileEntry => {
              console.log(this.TAG, "Selected fileInfo fileEntry", fileEntry);
              this.getFileSize(fileEntry).then(metadata => {
                if (metadata.size / 1024 / 1024 > 3) {
                  console.log(this.TAG, "File Size Too Large");
                  this.commonFunction.presentAlert("New Order", "Validation", "File size must be less than Equal to 3 MB");
                } else {
                  console.log(this.TAG, "File Size IS OK");
                  this.closureAtFieldBtnVisible = true;
                  this.cardinalFileName = filePathResult.substring(filePathResult.lastIndexOf("/") + 1);
                  this.cardinalFileType = filePathResult.substring(filePathResult.lastIndexOf(".") + 1);
                  this.selectedFileUrl = uri;
                  let temp = {
                    "uri": uri,
                    "file_name": this.cardinalFileName,
                    "file_type": this.cardinalFileType,
                    "size": metadata.size
                  };
                  this.pdfFileSelectedURI = [temp]; // if(this.pdfFileSelectedURI ==undefined || this.pdfFileSelectedURI == null){
                  //   this.pdfFileSelectedURI = [temp];
                  // } else {
                  //   this.pdfFileSelectedURI.push(temp);
                  // }
                }
              });
            }).catch(error => {});
          });
        }).catch(e => console.log(e));
      } else if (this.platform.is('ios')) {
        this.filePicker.pickFile().then(uri => {
          this.file.resolveLocalFilesystemUrl('file:///' + uri).then(fileEntry => {
            console.log(this.TAG, "Selected fileInfo fileEntry", fileEntry);
            this.getFileSize(fileEntry).then(metadata => {
              if (metadata.size / 1024 / 1024 > 3) {
                console.log(this.TAG, "File Size Too Large");
                this.commonFunction.presentAlert("New Order", "Validation", "File size must be less than Equal to 3 MB");
              } else {
                console.log(this.TAG, "File Size IS OK");
                this.closureAtFieldBtnVisible = true;
                this.cardinalFileName = uri.substring(uri.lastIndexOf("/") + 1);
                this.cardinalFileType = uri.substring(uri.lastIndexOf(".") + 1);
                this.selectedFileUrl = uri;
                let temp = {
                  "uri": uri,
                  "file_name": this.cardinalFileName,
                  "file_type": this.cardinalFileType,
                  "size": metadata.size
                };
                this.pdfFileSelectedURI = [temp]; // if(this.pdfFileSelectedURI ==undefined || this.pdfFileSelectedURI == null){
                //   this.pdfFileSelectedURI = [temp];
                // } else {
                //   this.pdfFileSelectedURI.push(temp);
                // }
              }
            });
          }).catch(error => {});
        }).catch(err => console.log('File Picker Error', err));
      }
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  getFileSize(FileEntry) {
    let self = this;
    let promise = new Promise(function (resolve, reject) {
      FileEntry.getMetadata(metadata => {
        resolve(metadata);
      });
    });
    return promise;
  }

  uploadcompImage(str) {
    try {
      for (var k = 0; k < str.target.files.length; k++) {
        this.inneruploadcompImage(str, k);
        this.timeout(500);
      }
    } catch (error) {//  console.log("Catch",error);	
    }
  }

  timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  inneruploadcompImage(str, k) {
    var file = str.target.files[k];
    console.log(this.TAG, "SElected File DATA", str.target.files[k]);
    this.cardinalFileName = str.target.files[0].name.substring(str.target.files[0].name.lastIndexOf("/") + 1);
    this.cardinalFileType = str.target.files[0].name.substring(str.target.files[0].name.lastIndexOf(".") + 1);
    console.log("File Selected name", this.cardinalFileName);
    console.log("File Selected type", this.cardinalFileType);

    if (!!this.cardinalFileType && this.cardinalFileType == 'pdf') {
      this.imgcomp = str.target.files[0];
      let temp = {
        "uri": "tst",
        "file_name": this.cardinalFileName,
        "file_type": this.cardinalFileType,
        "size": (str.target.files[0].size / (1024 * 1024)).toFixed(2)
      };
      this.pdfFileSelectedURI = [temp]; // if(this.pdfFileSelectedURI ==undefined || this.pdfFileSelectedURI == null){
      //   this.pdfFileSelectedURI = [temp];
      // } else {
      //   this.pdfFileSelectedURI.push(temp);
      // }

      console.log(this.TAG, "File Size", str.target.files[0].size / 1024 / 1024);

      if (str.target.files[0].size / 1024 / 1024 > 3) {
        console.log(this.TAG, "File Size Too Large");
        this.commonFunction.presentAlert("New Order", "Validation", "File size must be less than Equal to 3 MB");
        this.imgcomp = null;
        this.cardinalFileName = "";
        this.cardinalFileType = "";
        this.pdfFileSelectedURI = [];
        this.selectedFile.nativeElement.value = '';
      } else {
        var myreader = new FileReader();

        myreader.onloadend = e => {
          var b64 = myreader.result.toString().replace(/^data:.+;base64,/, '');
          var existinglength = this.imgcomp.length;
          this.imgcomp[existinglength] = b64;
          this.equimentFormGroup.get('errorCodeCtrl').disable();
          this.closureAtFieldBtnVisible = true;
        };

        myreader.readAsDataURL(file);
      }
    } else {
      this.imgcomp = [];
      this.cardinalFileType = "image";
      this.pdfFileSelectedURI = [];
      var file = str.target.files[k];
      var myreader = new FileReader();

      myreader.onloadend = e => {
        var b64 = myreader.result.toString().replace(/^data:.+;base64,/, '');
        var existinglength = this.imgcomp.length;
        this.imgcomp[existinglength] = b64;
        this.equimentFormGroup.get('errorCodeCtrl').disable();
        this.closureAtFieldBtnVisible = true;
      };

      myreader.readAsDataURL(file);
    }
  }

  ImageViewr(img, rowcompliancedata) {
    var _this6 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      var msg = "";

      if (rowcompliancedata != null) {
        msg = '<div>' + '<img class="viewImagecss" src="data:image/png;base64,' + img + '">' + '</div>';
      } else {
        msg = '<div>' + '<img class="viewImagecss" src="' + img + '">' + '</div>';
      }

      const alert = yield _this6.alertCtrl.create({
        message: msg,
        buttons: [{
          text: 'Remove',
          handler: data => {
            //this.POimg64=null
            if (rowcompliancedata != null) {
              _this6.imgcomp = _this6.imgcomp.filter(item => item != img);
            } else {
              _this6.fileUrl = null;
            }
          }
        }, {
          text: 'OK'
        }]
      });
      yield alert.present();
    })();
  }

  uploadImage(str) {
    try {
      var file = str.target.files[0];
      var myreader = new FileReader();

      myreader.onloadend = e => {
        var b64 = myreader.result.toString().replace(/^data:.+;base64,/, '');
        this.fileUrl = myreader.result;
        this.img = b64;
      };

      myreader.readAsDataURL(file);
    } catch (error) {}
  }

  takePicture() {
    const options = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      targetWidth: 1500,
      targetHeight: 1500
    };
    this.camera.getPicture(options).then(imageData => {
      this.fileUrl = 'data:image/jpeg;base64,' + imageData;
      this.cardinalFileType = "image";

      if (!!this.imgcomp) {
        this.imgcomp = this.imgcomp.concat(imageData);
        this.closureAtFieldBtnVisible = true;
        this.equimentFormGroup.get('errorCodeCtrl').disable();
      } else {
        this.imgcomp = imageData;
      } // console.log("Cameara image",this.img);

    });
  } //Select Image from library	


  getimage() {
    const options = {
      quality: 50,
      outputType: 1,
      width: 1500,
      height: 1500
    };
    this.imagePicker.getPictures(options).then(imageData => {
      //  console.log("this is image date",imageData);
      // this.assignToCtrlVisible = true;
      this.cardinalFileType = "image";
      this.closureAtFieldBtnVisible = true;
      this.equimentFormGroup.get('errorCodeCtrl').disable();

      if (!!this.imgcomp) {
        this.imgcomp = this.imgcomp.concat(imageData);
      } else {
        this.imgcomp = imageData;
      }
    }, err => {//  console.log(this.TAG, err);
    });
  }

  punchedCOPSalesOrder() {
    var _this7 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        //  console.log(this.TAG, "punched COP Sales Order");
        _this7.commonFunction.loadingPresent();

        let spareComplain = new src_assets_model_complain__WEBPACK_IMPORTED_MODULE_8__.Complain();
        spareComplain.problem_observed = _this7.engineerDetailsFormGroup.get('problemObservedCtrl').value ? _this7.engineerDetailsFormGroup.get('problemObservedCtrl').value : '';
        spareComplain.field_visit_remarks = _this7.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').value ? _this7.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').value : '';
        spareComplain.proposed_action = _this7.engineerDetailsFormGroup.get('proposedActionCtrl').value ? _this7.engineerDetailsFormGroup.get('proposedActionCtrl').value.id : '';
        spareComplain.dealer_billing_address = _this7.engineerDetailsFormGroup.get('dealerBillingAddressCtrl').value ? _this7.engineerDetailsFormGroup.get('dealerBillingAddressCtrl').value.id : '';
        spareComplain.dealer_shipping_address = _this7.engineerDetailsFormGroup.get('dealerShippingAddressCtrl').value ? _this7.engineerDetailsFormGroup.get('dealerShippingAddressCtrl').value.id : '';
        spareComplain.assign_to = _this7.engineerDetailsFormGroup.get('assignToCtrl').value ? _this7.engineerDetailsFormGroup.get('assignToCtrl').value.id : '';
        spareComplain.spare_SKUCode = _this7.spareRequiredFormGroup.get('spareSKUCodeDropDownCtrl').value ? _this7.spareRequiredFormGroup.get('spareSKUCodeDropDownCtrl').value.id : '';
        spareComplain.quantity = _this7.spareRequiredFormGroup.get('quantityCtrl').value ? _this7.spareRequiredFormGroup.get('quantityCtrl').value : '';
        spareComplain.dealername = _this7.spareRequiredFormGroup.get('dealerNameSpareRequiredCtrl').value ? _this7.spareRequiredFormGroup.get('dealerNameSpareRequiredCtrl').value : '';
        spareComplain.cop_sales_order_reference = _this7.spareRequiredFormGroup.get('copSalesOrderReferenceCtrl').value ? _this7.spareRequiredFormGroup.get('copSalesOrderReferenceCtrl').value : '';
        spareComplain.dealer_billing_address = _this7.engineerDetailsFormGroup.get('dealerBillingAddressCtrl').value ? _this7.engineerDetailsFormGroup.get('dealerBillingAddressCtrl').value.id : '';
        spareComplain.proposed_action = _this7.engineerDetailsFormGroup.get('proposedActionCtrl').value ? _this7.engineerDetailsFormGroup.get('proposedActionCtrl').value.id : '', spareComplain.assign_to = _this7.engineerDetailsFormGroup.get('assignToCtrl').value ? _this7.engineerDetailsFormGroup.get('assignToCtrl').value.id : '';
        spareComplain.problem_observed = _this7.engineerDetailsFormGroup.get('problemObservedCtrl').value ? _this7.engineerDetailsFormGroup.get('problemObservedCtrl').value : '';
        spareComplain.field_visit_remarks = _this7.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').value ? _this7.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').value : '';

        if (!!_this7.service.sparerequired.sparerequiredid) {
          spareComplain.sparerequestid = _this7.service.sparerequired.sparerequiredid;
        } else {
          spareComplain.sparerequestid = "";
        }

        let data = {
          "complaintno": _this7.service.complaintno,
          "complaintid": _this7.service.complaintid,
          "billadd": spareComplain.dealer_billing_address,
          "shippadd": spareComplain.dealer_shipping_address,
          "proposeactn": spareComplain.proposed_action,
          "assigntofieldvisit": spareComplain.assign_to,
          "problemobserv": spareComplain.problem_observed,
          "fieldvisit": spareComplain.field_visit_remarks,
          "spares": _this7.tempSpareCart,
          "activity": _this7.loginService.selectedactivity.id
        };
        let punchOrderResponse = yield (yield _this7.serviceEngineerVisitService.punchedCOPSalesOrderPost(data)).toPromise(); //  console.log(this.TAG, "Spare Request Log", punchOrderResponse);

        _this7.commonFunction.loadingDismiss();

        if (punchOrderResponse) {
          console.log(_this7.TAG, "Spare Request Log", punchOrderResponse);
          _this7.isSpareInstallationTabVisible = true;
          _this7.complaintStatusControlVisible = true;

          _this7.commonFunction.presentAlert("Field Visit Details", "Spare Order Status", punchOrderResponse.msg);

          _this7.spareRequiredFormGroup.controls["copSalesOrderReferenceCtrl"].setValue(punchOrderResponse.orderno);

          _this7.spareRequiredFormGroup.get('copSalesOrderReferenceCtrl').disable();

          _this7.engineerDetailsFormGroup.get('proposedActionCtrl').disable(); //  this.spareRequiredFormGroup.get('spareSKUCodeDropDownCtrl').disable();
          //   this.spareRequiredFormGroup.get('quantityCtrl').disable();
          //   this.spareRequiredFormGroup.get('dealerNameSpareRequiredCtrl').disable();


          _this7.punchOrderSuccess = true;
          _this7.tempCartStatus = false;
          _this7.tempSpareCart = [];
          let tempComplainDetail = yield _this7.serviceEngineerVisitService.getCustomerServiceDetailById(_this7.service.complaintid).toPromise();
          console.log(_this7.TAG, "TEST Single", tempComplainDetail);
          _this7.copSalesOrderReferenceList = tempComplainDetail[0].orders;

          if (!!_this7.copSalesOrderReferenceList) {
            _this7.spareOrderedTabVisible = true;
          }

          _this7.spareRequiredList = tempComplainDetail[0].spare_required_list;

          if (!!_this7.spareRequiredList) {
            _this7.spareOrderedTabVisible = true;
          }

          console.log(_this7.TAG, "TEST Single", _this7.copSalesOrderReferenceList);
        } else {
          _this7.commonFunction.presentAlert("Field Visit Details", "Spare Order Status", "Something went wrong");
        }
      } catch (error) {
        _this7.isSpareInstallationTabVisible = true;
        _this7.complaintStatusControlVisible = true;

        _this7.commonFunction.loadingDismiss(); //  console.log(this.TAG, error);


        _this7.commonFunction.presentAlert("Field Visit Details", "Spare Order Status", error.error);
      }
    })();
  }

  spareInstallationSaveToOB(event) {
    var _this8 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this8.commonFunction.loadingPresent();

        let spareInstallComplain = new src_assets_model_complain__WEBPACK_IMPORTED_MODULE_8__.Complain();
        spareInstallComplain.complaint_no = _this8.service.complaintno;
        spareInstallComplain.complaintid = _this8.service.complaintid;
        spareInstallComplain.problem_observed = _this8.engineerDetailsFormGroup.get('problemObservedCtrl').value ? _this8.engineerDetailsFormGroup.get('problemObservedCtrl').value : '';
        spareInstallComplain.field_visit_remarks = _this8.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').value ? _this8.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').value : '';
        spareInstallComplain.proposed_action = _this8.engineerDetailsFormGroup.get('proposedActionCtrl').value ? _this8.engineerDetailsFormGroup.get('proposedActionCtrl').value.id : '';
        spareInstallComplain.spare_received_date = _this8.spareInstallationFormGroup.get('spareReceivedDateCtrl').value ? _this8.spareInstallationFormGroup.get('spareReceivedDateCtrl').value : '';
        spareInstallComplain.repair_report = _this8.spareInstallationFormGroup.get('repairReportCtrl').value ? _this8.spareInstallationFormGroup.get('repairReportCtrl').value : ''; //New Change
        //spareInstallComplain.complaint_status = this.spareInstallationFormGroup.get('complaintStatusCtrl').value ? this.spareInstallationFormGroup.get('complaintStatusCtrl').value.code : '';
        //  spareInstallComplain.orderid = this.spareInstallationFormGroup.get('copSalesOrderReferenceDropDownCtrl').value ? this.spareInstallationFormGroup.get('copSalesOrderReferenceDropDownCtrl').value.orderno : '';

        spareInstallComplain.orderid = _this8.skuCodeSelected.orderid;
        spareInstallComplain.sapreskuid = _this8.spareInstallationFormGroup.get('skuCodeCtrl').value ? _this8.spareInstallationFormGroup.get('skuCodeCtrl').value.sapreskuid : '';
        spareInstallComplain.qty = _this8.spareInstallationFormGroup.get('spareReceivedQuantityCtrl').value ? _this8.spareInstallationFormGroup.get('spareReceivedQuantityCtrl').value : '';
        spareInstallComplain.sparerequiredid = _this8.skuCodeSelected.sparerequiredid;
        spareInstallComplain.complaint_date = _this8.spareInstallationFormGroup.get('completionDateCtrl').value ? _this8.spareInstallationFormGroup.get('completionDateCtrl').value : '';
        spareInstallComplain.replaced_spare_part_serialNo = _this8.spareInstallationFormGroup.get('replacedSparePartSerialNoCtrl').value ? _this8.spareInstallationFormGroup.get('replacedSparePartSerialNoCtrl').value : '';
        spareInstallComplain.service_attended = _this8.spareInstallationFormGroup.get('serviceAttendedCtrl').value ? _this8.spareInstallationFormGroup.get('serviceAttendedCtrl').value : '';
        spareInstallComplain.defective_spare_part_no = _this8.spareInstallationFormGroup.get('defectiveSparePartNoCtrl').value ? _this8.spareInstallationFormGroup.get('defectiveSparePartNoCtrl').value : '';
        spareInstallComplain.def_spare_docket_no = _this8.spareInstallationFormGroup.get('defSpareDocketNoCtrl').value ? _this8.spareInstallationFormGroup.get('defSpareDocketNoCtrl').value : '';
        spareInstallComplain.def_spare_courier = _this8.spareInstallationFormGroup.get('defSpareCourierCtrl').value ? _this8.spareInstallationFormGroup.get('defSpareCourierCtrl').value : '';
        spareInstallComplain.def_spare_sent_date = _this8.spareInstallationFormGroup.get('defSpareSentDateCtrl').value ? _this8.spareInstallationFormGroup.get('defSpareSentDateCtrl').value : '';
        spareInstallComplain.spare_install_closed = _this8.spareInstallationFormGroup.get('spareInstallClosedCtrl').value ? _this8.spareInstallationFormGroup.get('spareInstallClosedCtrl').value : '';
        console.log(_this8.TAG, "Spare Install Form", spareInstallComplain);
        let saveComplainResponse = yield _this8.serviceEngineerVisitService.spareInstallSaveToOB(spareInstallComplain).toPromise(); //  console.log(this.TAG, "Response From Spare Install", saveComplainResponse);

        if (saveComplainResponse) {
          _this8.presentSpareInstallAlert("Field Visit Details", "Spare Install", saveComplainResponse.msg);

          let tempComplainDetail = yield _this8.serviceEngineerVisitService.getCustomerServiceDetailById(_this8.service.complaintid).toPromise();
          console.log(_this8.TAG, "TEST Single", tempComplainDetail);
          _this8.copSalesOrderReferenceList = tempComplainDetail[0].orders;

          _this8.engineerDetailsFormGroup.controls['complaintStatusCtrl'].reset();

          _this8.spareInstallationFormGroup.reset();

          _this8.stepper.selectedIndex = 5;

          _this8.commonFunction.loadingDismiss();
        } else {
          _this8.commonFunction.loadingDismiss();

          _this8.commonFunction.presentAlert("Field Visit Details", "Spare Install", "Something went wrong please try again" + saveComplainResponse.msg);
        }

        _this8.commonFunction.loadingDismiss();

        _this8.spareInstallationStepValid = true;
      } catch (error) {
        _this8.commonFunction.loadingDismiss(); //  console.log(this.TAG, error);


        _this8.commonFunction.presentAlert("Field Visit Details", "Spare Install", "Something went wrong please try again" + error.error);
      }
    })();
  }

  closureAtFieldBtnClick() {
    var _this9 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        // this.isSpareInstallationTabVisible = true;
        _this9.commonFunction.loadingPresent();

        let finalComplain = new src_assets_model_complain__WEBPACK_IMPORTED_MODULE_8__.Complain();
        finalComplain.complaint_no = _this9.service.complaintno;
        finalComplain.complaintid = _this9.service.complaintid;
        finalComplain.complaint_date = _this9.service.complaintdate;
        finalComplain.doctype = _this9.service.doctype[0].id;
        finalComplain.nameofcomplainer = _this9.service.nameofcomplainer;
        finalComplain.desofcomplnr = _this9.service.desofcomplnr[0].id;
        finalComplain.contnumber = _this9.service.contnumber;
        finalComplain.email = _this9.service.email;
        finalComplain.eventdate = _this9.service.eventdate;
        finalComplain.description = _this9.service.description;
        finalComplain.serialno = _this9.service.serialno;
        finalComplain.srnoequipment = _this9.service.srnoequipment;
        finalComplain.contracttype = _this9.service.contracttype;
        finalComplain.invoiceno = _this9.service.invoiceno;
        finalComplain.invoicedate = _this9.service.invoicedate;
        finalComplain.errorcode = _this9.service.errorcode[0].id;
        finalComplain.dealername = _this9.service.dealername;
        finalComplain.salesrepresentative = _this9.service.sales_representative ? _this9.service.sales_representative : "";
        finalComplain.installationdate = _this9.service.installationdate;
        finalComplain.skucode = _this9.service.skucode;
        finalComplain.skuname = _this9.service.skuname;
        finalComplain.brandname = _this9.service.brandname;
        finalComplain.producttobereturn = _this9.service.producttobereturn;
        finalComplain.custname = _this9.service.custname;
        finalComplain.add1 = _this9.service.add1;
        finalComplain.add2 = _this9.service.add2;
        finalComplain.add3 = _this9.service.add3;
        finalComplain.pincode = _this9.service.pincode;
        finalComplain.area = _this9.service.area[0].id;
        finalComplain.city = _this9.service.city[0].id;
        finalComplain.state = _this9.service.state[0].id;
        finalComplain.country = _this9.service.country[0].id;
        finalComplain.problem_observed = _this9.engineerDetailsFormGroup.get('problemObservedCtrl').value ? _this9.engineerDetailsFormGroup.get('problemObservedCtrl').value : '';
        finalComplain.field_visit_remarks = _this9.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').value ? _this9.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').value : '';
        finalComplain.proposed_action = _this9.engineerDetailsFormGroup.get('proposedActionCtrl').value ? _this9.engineerDetailsFormGroup.get('proposedActionCtrl').value.id : '';
        finalComplain.dealer_billing_address = _this9.engineerDetailsFormGroup.get('dealerBillingAddressCtrl').value ? _this9.engineerDetailsFormGroup.get('dealerBillingAddressCtrl').value.id : '';
        finalComplain.dealer_shipping_address = _this9.engineerDetailsFormGroup.get('dealerShippingAddressCtrl').value ? _this9.engineerDetailsFormGroup.get('dealerShippingAddressCtrl').value.id : '';
        finalComplain.assign_to = _this9.engineerDetailsFormGroup.get('assignToCtrl').value ? _this9.engineerDetailsFormGroup.get('assignToCtrl').value.id : '';

        if (_this9.isSpareRequiredTabVisible) {
          finalComplain.spare_SKUCode = _this9.spareRequiredFormGroup.get('spareSKUCodeDropDownCtrl').value ? _this9.spareRequiredFormGroup.get('spareSKUCodeDropDownCtrl').value.id : '';
          finalComplain.quantity = _this9.spareRequiredFormGroup.get('quantityCtrl').value ? _this9.spareRequiredFormGroup.get('quantityCtrl').value : '';
          finalComplain.dealername = _this9.spareRequiredFormGroup.get('dealerNameCtrl').value ? _this9.spareRequiredFormGroup.get('dealerNameCtrl').value : '';
          finalComplain.cop_sales_order_reference = _this9.spareRequiredFormGroup.get('copSalesOrderReferenceCtrl').value ? _this9.spareRequiredFormGroup.get('copSalesOrderReferenceCtrl').value : '';
        }

        if (_this9.isSpareInstallationTabVisible) {
          finalComplain.spare_received_date = _this9.spareInstallationFormGroup.get('spareReceivedDateCtrl').value ? _this9.spareInstallationFormGroup.get('spareReceivedDateCtrl').value : '';
          finalComplain.repair_report = _this9.spareInstallationFormGroup.get('repairReportCtrl').value ? _this9.spareInstallationFormGroup.get('repairReportCtrl').value : '';
          finalComplain.complaint_status = _this9.spareInstallationFormGroup.get('complaintStatusCtrl').value ? _this9.spareInstallationFormGroup.get('complaintStatusCtrl').value.code : '';
          finalComplain.complaint_date = _this9.spareInstallationFormGroup.get('completionDateCtrl').value ? _this9.spareInstallationFormGroup.get('completionDateCtrl').value : '';
          finalComplain.replaced_spare_part_serialNo = _this9.spareInstallationFormGroup.get('replacedSparePartSerialNoCtrl').value ? _this9.spareInstallationFormGroup.get('replacedSparePartSerialNoCtrl').value : '';
          finalComplain.service_attended = _this9.spareInstallationFormGroup.get('serviceAttendedCtrl').value ? _this9.spareInstallationFormGroup.get('serviceAttendedCtrl').value : '';
          finalComplain.defective_spare_part_no = _this9.spareInstallationFormGroup.get('defectiveSparePartNoCtrl').value ? _this9.spareInstallationFormGroup.get('defectiveSparePartNoCtrl').value : '';
          finalComplain.def_spare_docket_no = _this9.spareInstallationFormGroup.get('defSpareDocketNoCtrl').value ? _this9.spareInstallationFormGroup.get('defSpareDocketNoCtrl').value : '';
          finalComplain.def_spare_courier = _this9.spareInstallationFormGroup.get('defSpareCourierCtrl').value ? _this9.spareInstallationFormGroup.get('defSpareCourierCtrl').value : '';
          finalComplain.def_spare_sent_date = _this9.spareInstallationFormGroup.get('defSpareSentDateCtrl').value ? _this9.spareInstallationFormGroup.get('defSpareSentDateCtrl').value : '';
          finalComplain.spare_install_closed = _this9.spareInstallationFormGroup.get('spareInstallClosedCtrl').value ? _this9.spareInstallationFormGroup.get('spareInstallClosedCtrl').value : '';
        }

        finalComplain.servicevendorremark = _this9.serviceManagerFormGroup.get('serviceManagerRemarkCtrl').value ? _this9.serviceManagerFormGroup.get('serviceManagerRemarkCtrl').value : ''; //finalComplain.servicevendorremark = this.service.serivevendorremark;

        finalComplain.defective_spare_part_no = _this9.serviceManagerFormGroup.get('defectiveSparePartNoCtrl').value ? _this9.serviceManagerFormGroup.get('defectiveSparePartNoCtrl').value : '';
        finalComplain.defective_spare_part_received_date = _this9.serviceManagerFormGroup.get('defectiveSparePartReceivedDateCtrl').value ? _this9.serviceManagerFormGroup.get('defectiveSparePartReceivedDateCtrl').value : '';
        finalComplain.smart_solve_ref_no = _this9.serviceManagerFormGroup.get('smartSolveRefNoCtrl').value ? _this9.serviceManagerFormGroup.get('smartSolveRefNoCtrl').value : '';
        finalComplain.final_closure = _this9.serviceManagerFormGroup.get('finalClosureCtrl').value ? _this9.serviceManagerFormGroup.get('finalClosureCtrl').value : '';
        finalComplain.activity = _this9.loginService.selectedactivity.id;

        if (!!_this9.selectedFileUrl) {} else {
          finalComplain.imagebase64 = _this9.imgcomp;
        }

        finalComplain.closureatfield = "true";
        finalComplain.complaint_status = _this9.engineerDetailsFormGroup.get('complaintStatusCtrl').value ? _this9.engineerDetailsFormGroup.get('complaintStatusCtrl').value.code : '';
        finalComplain.file_type = _this9.cardinalFileType; // finalComplain.file_selected_uri = this.pdfFileSelectedURI.uri;
        //finalComplain.imagebase64 = this.imgcomp[0];

        let saveComplainResponse;
        console.log(_this9.TAG, "Final Service Manager Form", finalComplain);

        if (_this9.platform.is('android') && _this9.cardinalFileType == "pdf") {
          console.log(_this9.TAG, "Android And File Type PDF");
          let oneObject = {
            'complaintno': finalComplain.complaint_no,
            'complaintid': finalComplain.complaintid,
            'doctype': finalComplain.doctype,
            'nameofcomplainer': finalComplain.nameofcomplainer,
            'desofcomplnr': finalComplain.desofcomplnr,
            'contnumber': finalComplain.contnumber,
            'email': finalComplain.email,
            'eventdate': finalComplain.eventdate,
            'serialno': finalComplain.serialno,
            "srnoequipment": finalComplain.srnoequipment,
            "contracttype": finalComplain.contracttype,
            'invoiceno': finalComplain.invoiceno,
            "invoicedate": finalComplain.invoicedate,
            "errorcode": finalComplain.errorcode,
            "dealername": finalComplain.dealername,
            "installationdate": finalComplain.installationdate,
            "skucode": finalComplain.skucode,
            "skuname": finalComplain.skuname,
            "brandname": finalComplain.brandname,
            "producttobereturn": finalComplain.producttobereturn,
            "custname": finalComplain.custname,
            "add1": finalComplain.add1,
            "add2": finalComplain.add2,
            "add3": finalComplain.add3,
            "pincode": finalComplain.pincode,
            "area": finalComplain.area,
            "city": finalComplain.city,
            "state": finalComplain.state,
            "country": finalComplain.country,
            "description": finalComplain.description,
            "lotno": finalComplain.lotno ? finalComplain.lotno : '',
            "appcomplaint": finalComplain.appcomplaint,
            "assigntoservvendor": finalComplain.assigntoservvendor,
            "salesrepresentative": finalComplain.salesrepresentative,
            "newdealername": finalComplain.newdealername,
            "serviceengname": finalComplain.serviceengname,
            "serviceengcontact": finalComplain.serviceengcontact,
            "visitdate": finalComplain.visitdate,
            "assigntoservmg": finalComplain.assigntoservmg,
            "activity": _this9.loginService.selectedactivity.id,
            "Appect": finalComplain.Appect,
            "problemobserv": finalComplain.problem_observed,
            "fieldvisit": finalComplain.field_visit_remarks,
            "proposeactn": finalComplain.proposed_action,
            "assigntofieldvisit": finalComplain.assign_to,
            "closureatfield": finalComplain.closureatfield,
            "compltstatus": finalComplain.complaint_status,
            "imagebase64": finalComplain.imagebase64,
            "file_type": _this9.cardinalFileType
          };
          saveComplainResponse = yield _this9.serviceEngineerVisitService.uploadPDFFileServiceAndroidiOS({
            "postData": JSON.stringify(oneObject)
          }, _this9.selectedFileUrl).toPromise();
          console.log(_this9.TAG, "Response From Save Complain", saveComplainResponse);

          if (saveComplainResponse) {
            _this9.presentAlert("closer_success", "Field Visit Details", saveComplainResponse.msg);
          } else {
            _this9.presentAlert("closer_error", "Field Visit Details", "Something went wrong please try again" + saveComplainResponse.msg);
          }
        } else if (_this9.platform.is('ios') && _this9.cardinalFileType == "pdf") {
          let oneObject = {
            'complaintno': finalComplain.complaint_no,
            'complaintid': finalComplain.complaintid,
            'doctype': finalComplain.doctype,
            'nameofcomplainer': finalComplain.nameofcomplainer,
            'desofcomplnr': finalComplain.desofcomplnr,
            'contnumber': finalComplain.contnumber,
            'email': finalComplain.email,
            'eventdate': finalComplain.eventdate,
            'serialno': finalComplain.serialno,
            "srnoequipment": finalComplain.srnoequipment,
            "contracttype": finalComplain.contracttype,
            'invoiceno': finalComplain.invoiceno,
            "invoicedate": finalComplain.invoicedate,
            "errorcode": finalComplain.errorcode,
            "dealername": finalComplain.dealername,
            "installationdate": finalComplain.installationdate,
            "skucode": finalComplain.skucode,
            "skuname": finalComplain.skuname,
            "brandname": finalComplain.brandname,
            "producttobereturn": finalComplain.producttobereturn,
            "custname": finalComplain.custname,
            "add1": finalComplain.add1,
            "add2": finalComplain.add2,
            "add3": finalComplain.add3,
            "pincode": finalComplain.pincode,
            "area": finalComplain.area,
            "city": finalComplain.city,
            "state": finalComplain.state,
            "country": finalComplain.country,
            "description": finalComplain.description,
            "lotno": finalComplain.lotno ? finalComplain.lotno : '',
            "appcomplaint": finalComplain.appcomplaint,
            "assigntoservvendor": finalComplain.assigntoservvendor,
            "salesrepresentative": finalComplain.salesrepresentative,
            "newdealername": finalComplain.newdealername,
            "serviceengname": finalComplain.serviceengname,
            "serviceengcontact": finalComplain.serviceengcontact,
            "visitdate": finalComplain.visitdate,
            "assigntoservmg": finalComplain.assigntoservmg,
            "activity": _this9.loginService.selectedactivity.id,
            "Appect": finalComplain.Appect,
            "problemobserv": finalComplain.problem_observed,
            "fieldvisit": finalComplain.field_visit_remarks,
            "proposeactn": finalComplain.proposed_action,
            "assigntofieldvisit": finalComplain.assign_to,
            "closureatfield": finalComplain.closureatfield,
            "compltstatus": finalComplain.complaint_status,
            "imagebase64": finalComplain.imagebase64,
            "file_type": _this9.cardinalFileType
          };
          let login = _this9.loginService.user;
          let password = _this9.loginService.pass;
          const auth = btoa(login + ":" + password);
          let save_file_url = src_app_common_Constants__WEBPACK_IMPORTED_MODULE_16__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.CustomerServiceInsert?';

          const fileTransfer = _this9.transfer.create();

          let options = {
            fileKey: 'imagebase64',
            fileName: _this9.cardinalFileName,
            params: {
              "postData": JSON.stringify(oneObject)
            },
            headers: {
              'Authorization': 'Basic ' + auth
            }
          };
          fileTransfer.upload(_this9.selectedFileUrl, save_file_url, options).then(data => {
            console.log("pravin YESSSSS", data);
            let formatResponse = JSON.parse(data.response);
            console.log("File Uplaod Result", formatResponse);

            _this9.commonFunction.loadingDismiss();

            if (data != null) {
              console.log(_this9.TAG, "Response From Save Complain", formatResponse);

              if (formatResponse) {
                _this9.presentAlert("closer_success", "Field Visit Details", formatResponse.msg);
              } else {
                _this9.presentAlert("closer_error", "Field Visit Details", "Something went wrong please try again" + formatResponse.msg);
              }
            }
          }, err => {
            _this9.commonFunction.presentAlert("message", "Fail", err);

            _this9.commonFunction.loadingDismiss();
          });
        } else {
          saveComplainResponse = yield _this9.serviceEngineerVisitService.finalCloser(finalComplain).toPromise();
          console.log(_this9.TAG, "Response From Save Complain", saveComplainResponse);

          if (saveComplainResponse) {
            _this9.presentAlert("closer_success", "Field Visit Details", saveComplainResponse.msg);
          } else {
            _this9.presentAlert("closer_error", "Field Visit Details", "Something went wrong please try again" + saveComplainResponse.msg);
          }
        }

        _this9.commonFunction.loadingDismiss();
      } catch (error) {
        _this9.commonFunction.loadingDismiss();

        console.error(_this9.TAG, error);
      }
    })();
  }

  finalClosureCheckboxServiceManager() {
    var _this10 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this10.commonFunction.loadingPresent();

        let finalComplain = new src_assets_model_complain__WEBPACK_IMPORTED_MODULE_8__.Complain();
        finalComplain.complaint_no = _this10.service.complaintno;
        finalComplain.complaintid = _this10.service.complaintid;
        finalComplain.complaint_date = _this10.service.complaintdate;
        finalComplain.doctype = _this10.service.doctype[0].id;
        finalComplain.nameofcomplainer = _this10.service.nameofcomplainer;
        finalComplain.desofcomplnr = _this10.service.desofcomplnr[0].id;
        finalComplain.contnumber = _this10.service.contnumber;
        finalComplain.email = _this10.service.email;
        finalComplain.eventdate = _this10.service.eventdate;
        finalComplain.business = _this10.service.business;
        finalComplain.description = _this10.service.description;
        finalComplain.serialno = _this10.service.serialno;
        finalComplain.srnoequipment = _this10.service.srnoequipment;
        finalComplain.contracttype = _this10.service.contracttype;
        finalComplain.invoiceno = _this10.service.invoiceno;
        finalComplain.invoicedate = _this10.service.invoicedate;
        finalComplain.errorcode = _this10.service.errorcode[0].id;
        finalComplain.dealername = _this10.service.dealername;
        finalComplain.salesrepresentative = _this10.service.sales_representative;
        finalComplain.installationdate = _this10.service.installationdate;
        finalComplain.skucode = _this10.service.skucode;
        finalComplain.skuname = _this10.service.skuname;
        finalComplain.brandname = _this10.service.brandname;
        finalComplain.producttobereturn = _this10.service.producttobereturn;
        finalComplain.custname = _this10.service.custname;
        finalComplain.add1 = _this10.service.add1;
        finalComplain.add2 = _this10.service.add2;
        finalComplain.add3 = _this10.service.add3;
        finalComplain.pincode = _this10.service.pincode;
        finalComplain.area = _this10.service.area[0].id;
        finalComplain.city = _this10.service.city[0].id;
        finalComplain.state = _this10.service.state[0].id;
        finalComplain.country = _this10.service.country[0].id;
        finalComplain.problem_observed = _this10.engineerDetailsFormGroup.get('problemObservedCtrl').value ? _this10.engineerDetailsFormGroup.get('problemObservedCtrl').value : '';
        finalComplain.field_visit_remarks = _this10.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').value ? _this10.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').value : '';
        finalComplain.proposed_action = _this10.engineerDetailsFormGroup.get('proposedActionCtrl').value ? _this10.engineerDetailsFormGroup.get('proposedActionCtrl').value.id : '';
        finalComplain.dealer_billing_address = _this10.engineerDetailsFormGroup.get('dealerBillingAddressCtrl').value ? _this10.engineerDetailsFormGroup.get('dealerBillingAddressCtrl').value.id : '';
        finalComplain.dealer_shipping_address = _this10.engineerDetailsFormGroup.get('dealerShippingAddressCtrl').value ? _this10.engineerDetailsFormGroup.get('dealerShippingAddressCtrl').value.id : '';
        finalComplain.assign_to = _this10.engineerDetailsFormGroup.get('assignToCtrl').value ? _this10.engineerDetailsFormGroup.get('assignToCtrl').value.id : '';

        if (_this10.isSpareRequiredTabVisible) {
          finalComplain.spare_SKUCode = _this10.spareRequiredFormGroup.get('spareSKUCodeDropDownCtrl').value ? _this10.spareRequiredFormGroup.get('spareSKUCodeDropDownCtrl').value.id : '';
          finalComplain.quantity = _this10.spareRequiredFormGroup.get('quantityCtrl').value ? _this10.spareRequiredFormGroup.get('quantityCtrl').value : '';
          finalComplain.dealername = _this10.spareRequiredFormGroup.get('dealerNameCtrl').value ? _this10.spareRequiredFormGroup.get('dealerNameCtrl').value : '';
          finalComplain.cop_sales_order_reference = _this10.spareRequiredFormGroup.get('copSalesOrderReferenceCtrl').value ? _this10.spareRequiredFormGroup.get('copSalesOrderReferenceCtrl').value : '';
        }

        if (_this10.isSpareInstallationTabVisible) {
          finalComplain.spare_received_date = _this10.spareInstallationFormGroup.get('spareReceivedDateCtrl').value ? _this10.spareInstallationFormGroup.get('spareReceivedDateCtrl').value : '';
          finalComplain.repair_report = _this10.spareInstallationFormGroup.get('repairReportCtrl').value ? _this10.spareInstallationFormGroup.get('repairReportCtrl').value : '';
          finalComplain.complaint_status = _this10.spareInstallationFormGroup.get('complaintStatusCtrl').value ? _this10.spareInstallationFormGroup.get('complaintStatusCtrl').value.id : '';
          finalComplain.complaint_date = _this10.spareInstallationFormGroup.get('completionDateCtrl').value ? _this10.spareInstallationFormGroup.get('completionDateCtrl').value : '';
          finalComplain.replaced_spare_part_serialNo = _this10.spareInstallationFormGroup.get('replacedSparePartSerialNoCtrl').value ? _this10.spareInstallationFormGroup.get('replacedSparePartSerialNoCtrl').value : '';
          finalComplain.service_attended = _this10.spareInstallationFormGroup.get('serviceAttendedCtrl').value ? _this10.spareInstallationFormGroup.get('serviceAttendedCtrl').value : '';
          finalComplain.defective_spare_part_no = _this10.spareInstallationFormGroup.get('defectiveSparePartNoCtrl').value ? _this10.spareInstallationFormGroup.get('defectiveSparePartNoCtrl').value : '';
          finalComplain.def_spare_docket_no = _this10.spareInstallationFormGroup.get('defSpareDocketNoCtrl').value ? _this10.spareInstallationFormGroup.get('defSpareDocketNoCtrl').value : '';
          finalComplain.def_spare_courier = _this10.spareInstallationFormGroup.get('defSpareCourierCtrl').value ? _this10.spareInstallationFormGroup.get('defSpareCourierCtrl').value : '';
          finalComplain.def_spare_sent_date = _this10.spareInstallationFormGroup.get('defSpareSentDateCtrl').value ? _this10.spareInstallationFormGroup.get('defSpareSentDateCtrl').value : '';
          finalComplain.spare_install_closed = _this10.spareInstallationFormGroup.get('spareInstallClosedCtrl').value ? _this10.spareInstallationFormGroup.get('spareInstallClosedCtrl').value : '';
        }

        finalComplain.servicevendorremark = _this10.serviceManagerFormGroup.get('serviceManagerRemarkCtrl').value ? _this10.serviceManagerFormGroup.get('serviceManagerRemarkCtrl').value : '';
        finalComplain.defective_spare_part_no = _this10.serviceManagerFormGroup.get('defectiveSparePartNoCtrl').value ? _this10.serviceManagerFormGroup.get('defectiveSparePartNoCtrl').value : '';
        finalComplain.defective_spare_part_received_date = _this10.serviceManagerFormGroup.get('defectiveSparePartReceivedDateCtrl').value ? _this10.serviceManagerFormGroup.get('defectiveSparePartReceivedDateCtrl').value : '';
        finalComplain.smart_solve_ref_no = _this10.serviceManagerFormGroup.get('smartSolveRefNoCtrl').value ? _this10.serviceManagerFormGroup.get('smartSolveRefNoCtrl').value : '';
        finalComplain.final_closure = _this10.serviceManagerFormGroup.get('finalClosureCtrl').value ? _this10.serviceManagerFormGroup.get('finalClosureCtrl').value : ''; //  console.log(this.TAG, "Final Service Manager Form", finalComplain);

        let saveComplainResponse = yield _this10.serviceEngineerVisitService.serviceManagerClose(finalComplain).toPromise(); //  console.log(this.TAG, "Response From Save Complain", saveComplainResponse);

        if (saveComplainResponse) {
          _this10.presentAlert("closer_success", "Field Visit Details", saveComplainResponse.msg);
        } else {
          _this10.presentAlert("closer_error", "Field Visit Details", "Something went wrong please try again" + saveComplainResponse.msg);
        }

        _this10.commonFunction.loadingDismiss();
      } catch (error) {
        _this10.commonFunction.loadingDismiss(); //  console.log(this.TAG, error);

      }
    })();
  }

  submit(tab) {
    var _this11 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this11.commonFunction.loadingPresent();

        let finalComplain = new src_assets_model_complain__WEBPACK_IMPORTED_MODULE_8__.Complain();
        finalComplain.complaint_no = _this11.service.complaintno;
        finalComplain.complaintid = _this11.service.complaintid, finalComplain.complaint_date = _this11.service.complaintdate;
        finalComplain.doctype = _this11.service.doctype[0].id;
        finalComplain.nameofcomplainer = _this11.service.nameofcomplainer;
        finalComplain.desofcomplnr = _this11.service.desofcomplnr[0].id;
        finalComplain.contnumber = _this11.service.contnumber;
        finalComplain.email = _this11.service.email;
        finalComplain.eventdate = _this11.service.eventdate;
        finalComplain.business = _this11.service.business;
        finalComplain.description = _this11.service.description;
        finalComplain.serialno = _this11.service.serialno;
        finalComplain.srnoequipment = _this11.service.srnoequipment;
        finalComplain.contracttype = _this11.service.contracttype;
        finalComplain.invoiceno = _this11.service.invoiceno;
        finalComplain.invoicedate = _this11.service.invoicedate;
        finalComplain.errorcode = _this11.service.errorcode[0].id;
        finalComplain.dealername = _this11.service.dealername;
        finalComplain.salesrepresentative = _this11.service.sales_representative;
        finalComplain.installationdate = _this11.service.installationdate;
        finalComplain.skucode = _this11.service.skucode;
        finalComplain.skuname = _this11.service.skuname;
        finalComplain.brandname = _this11.service.brandname;
        finalComplain.producttobereturn = _this11.service.producttobereturn;
        finalComplain.custname = _this11.service.custname;
        finalComplain.add1 = _this11.service.add1;
        finalComplain.add2 = _this11.service.add2;
        finalComplain.add3 = _this11.service.add3;
        finalComplain.pincode = _this11.service.pincode;
        finalComplain.area = _this11.service.area[0].id;
        finalComplain.city = _this11.service.city[0].id;
        finalComplain.state = _this11.service.state[0].id;
        finalComplain.country = _this11.service.country[0].id;
        finalComplain.problem_observed = _this11.engineerDetailsFormGroup.get('problemObservedCtrl').value ? _this11.engineerDetailsFormGroup.get('problemObservedCtrl').value : '';
        finalComplain.field_visit_remarks = _this11.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').value ? _this11.engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').value : '';
        finalComplain.proposed_action = _this11.engineerDetailsFormGroup.get('proposedActionCtrl').value ? _this11.engineerDetailsFormGroup.get('proposedActionCtrl').value.id : '';
        finalComplain.dealer_billing_address = _this11.engineerDetailsFormGroup.get('dealerBillingAddressCtrl').value ? _this11.engineerDetailsFormGroup.get('dealerBillingAddressCtrl').value.id : '';
        finalComplain.dealer_shipping_address = _this11.engineerDetailsFormGroup.get('dealerShippingAddressCtrl').value ? _this11.engineerDetailsFormGroup.get('dealerShippingAddressCtrl').value.id : '';
        finalComplain.assign_to = _this11.engineerDetailsFormGroup.get('assignToCtrl').value ? _this11.engineerDetailsFormGroup.get('assignToCtrl').value.id : '';

        if (_this11.isSpareRequiredTabVisible) {
          finalComplain.spare_SKUCode = _this11.spareRequiredFormGroup.get('spareSKUCodeDropDownCtrl').value ? _this11.spareRequiredFormGroup.get('spareSKUCodeDropDownCtrl').value.id : '';
          finalComplain.quantity = _this11.spareRequiredFormGroup.get('quantityCtrl').value ? _this11.spareRequiredFormGroup.get('quantityCtrl').value : '';
          finalComplain.dealername = _this11.spareRequiredFormGroup.get('dealerNameCtrl').value ? _this11.spareRequiredFormGroup.get('dealerNameCtrl').value : '';
          finalComplain.cop_sales_order_reference = _this11.spareRequiredFormGroup.get('copSalesOrderReferenceCtrl').value ? _this11.spareRequiredFormGroup.get('copSalesOrderReferenceCtrl').value : '';
        }

        if (_this11.isSpareInstallationTabVisible) {
          finalComplain.spare_received_date = _this11.spareInstallationFormGroup.get('spareReceivedDateCtrl').value ? _this11.spareInstallationFormGroup.get('spareReceivedDateCtrl').value : '';
          finalComplain.repair_report = _this11.spareInstallationFormGroup.get('repairReportCtrl').value ? _this11.spareInstallationFormGroup.get('repairReportCtrl').value : '';
          finalComplain.complaint_status = _this11.spareInstallationFormGroup.get('complaintStatusCtrl').value ? _this11.spareInstallationFormGroup.get('complaintStatusCtrl').value.id : '';
          finalComplain.complaint_date = _this11.spareInstallationFormGroup.get('completionDateCtrl').value ? _this11.spareInstallationFormGroup.get('completionDateCtrl').value : '';
          finalComplain.replaced_spare_part_serialNo = _this11.spareInstallationFormGroup.get('replacedSparePartSerialNoCtrl').value ? _this11.spareInstallationFormGroup.get('replacedSparePartSerialNoCtrl').value : '';
          finalComplain.service_attended = _this11.spareInstallationFormGroup.get('serviceAttendedCtrl').value ? _this11.spareInstallationFormGroup.get('serviceAttendedCtrl').value : '';
          finalComplain.defective_spare_part_no = _this11.spareInstallationFormGroup.get('defectiveSparePartNoCtrl').value ? _this11.spareInstallationFormGroup.get('defectiveSparePartNoCtrl').value : '';
          finalComplain.def_spare_docket_no = _this11.spareInstallationFormGroup.get('defSpareDocketNoCtrl').value ? _this11.spareInstallationFormGroup.get('defSpareDocketNoCtrl').value : '';
          finalComplain.def_spare_courier = _this11.spareInstallationFormGroup.get('defSpareCourierCtrl').value ? _this11.spareInstallationFormGroup.get('defSpareCourierCtrl').value : '';
          finalComplain.def_spare_sent_date = _this11.spareInstallationFormGroup.get('defSpareSentDateCtrl').value ? _this11.spareInstallationFormGroup.get('defSpareSentDateCtrl').value : '';
          finalComplain.spare_install_closed = _this11.spareInstallationFormGroup.get('spareInstallClosedCtrl').value ? _this11.spareInstallationFormGroup.get('spareInstallClosedCtrl').value : '';
        } //  finalComplain.servicevendorremark = this.serviceManagerFormGroup.get('serviceManagerRemarkCtrl').value ? this.serviceManagerFormGroup.get('serviceManagerRemarkCtrl').value : '';


        finalComplain.servicevendorremark = _this11.service.serivevendorremark;
        finalComplain.defective_spare_part_no = _this11.serviceManagerFormGroup.get('defectiveSparePartNoCtrl').value ? _this11.serviceManagerFormGroup.get('defectiveSparePartNoCtrl').value : '';
        finalComplain.defective_spare_part_received_date = _this11.serviceManagerFormGroup.get('defectiveSparePartReceivedDateCtrl').value ? _this11.serviceManagerFormGroup.get('defectiveSparePartReceivedDateCtrl').value : '';
        finalComplain.smart_solve_ref_no = _this11.serviceManagerFormGroup.get('smartSolveRefNoCtrl').value ? _this11.serviceManagerFormGroup.get('smartSolveRefNoCtrl').value : '';
        finalComplain.final_closure = _this11.serviceManagerFormGroup.get('finalClosureCtrl').value ? _this11.serviceManagerFormGroup.get('finalClosureCtrl').value : '';
        finalComplain.Appect = "true"; //  console.log(this.TAG, "Final Service Manager Form", finalComplain);

        let saveComplainResponse = yield _this11.serviceEngineerVisitService.finalCloser(finalComplain).toPromise(); //  console.log(this.TAG, "Response From Save Complain", saveComplainResponse);

        if (saveComplainResponse) {
          _this11.presentAlert("Field Visit Details", "", saveComplainResponse.msg);
        } else {
          _this11.presentAlert("Field Visit Details", "", "Something went wrong please try again" + saveComplainResponse.msg);
        }

        _this11.commonFunction.loadingDismiss();
      } catch (error) {
        _this11.commonFunction.loadingDismiss(); // console.log(this.TAG, error);

      }
    })();
  }

  addToCart() {
    try {
      let tempObject = {
        "id": this.spareSKUCodeSelected.id,
        "name": this.spareSKUCodeSelected.name,
        "qty": this.spareRequiredFormGroup.get('quantityCtrl').value ? this.spareRequiredFormGroup.get('quantityCtrl').value : '',
        "dealer_name": this.spareRequiredFormGroup.get('dealerNameSpareRequiredCtrl').value ? this.spareRequiredFormGroup.get('dealerNameSpareRequiredCtrl').value : '',
        "cop_sales_order_ref": ""
      };

      if (this.tempSpareCart == undefined || this.tempSpareCart == null) {
        this.tempSpareCart = [tempObject];
      } else {
        var sameProduct = this.tempSpareCart.find(e => e.id === this.spareSKUCodeSelected.id);

        if (sameProduct != null || sameProduct != undefined) {
          this.commonFunction.presentAlert("Field Visit Details", "Validation", "This SKU is already added");
        } else {
          this.tempSpareCart.push(tempObject);
        }
      }

      this.spareRequiredFormGroup.controls['spareSKUCodeDropDownCtrl'].reset();
      this.spareRequiredFormGroup.controls['quantityCtrl'].reset(); //  this.spareRequiredFormGroup.get('spareSKUCodeDropDownCtrl').setErrors(null)

      this.tempCartStatus = true;
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  removeFromCart(spareItem) {
    try {
      const result = this.tempSpareCart.filter(item => item.id != spareItem.id);
      this.tempSpareCart = result;

      if (this.tempSpareCart.length == 0) {
        this.tempCartStatus = false;
      }
    } catch (error) {}
  }

  copSalesOrderReferenceDropDownChange(salesOrder) {
    try {
      console.log(this.TAG, "Selected Order", salesOrder);
      this.skuCodeList = salesOrder.order;
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  skuCodeListChange(skuObject) {
    try {
      console.log(this.TAG, "skuObject", skuObject);
      this.spareInstallationFormGroup.controls["spareReceivedQuantityCtrl"].setValue(skuObject.qty);
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

};

ServiceEngineerVisitDetailPage.ctorParameters = () => [{
  type: _angular_router__WEBPACK_IMPORTED_MODULE_22__.ActivatedRoute
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_22__.Router
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_19__.FormBuilder
}, {
  type: src_provider_validator_helper__WEBPACK_IMPORTED_MODULE_5__.Validator
}, {
  type: src_app_neworder_neworder_service__WEBPACK_IMPORTED_MODULE_6__.NeworderService
}, {
  type: _service_engineer_visit_service__WEBPACK_IMPORTED_MODULE_4__.ServiceEngineerVisitService
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_23__.ModalController
}, {
  type: src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_9__.LoginauthService
}, {
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_7__.Commonfun
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_23__.AlertController
}, {
  type: _customer_service_customer_service_service__WEBPACK_IMPORTED_MODULE_3__.CustomerServiceService
}, {
  type: _awesome_cordova_plugins_image_picker_ngx__WEBPACK_IMPORTED_MODULE_10__.ImagePicker
}, {
  type: _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_11__.Camera
}, {
  type: src_provider_message_helper__WEBPACK_IMPORTED_MODULE_12__.Message
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_23__.Platform
}, {
  type: _ionic_native_file_chooser_ngx__WEBPACK_IMPORTED_MODULE_13__.FileChooser
}, {
  type: _awesome_cordova_plugins_file_path_ngx__WEBPACK_IMPORTED_MODULE_14__.FilePath
}, {
  type: _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_15__.File
}, {
  type: _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_17__.FileTransfer
}, {
  type: _ionic_native_file_picker_ngx__WEBPACK_IMPORTED_MODULE_18__.IOSFilePicker
}];

ServiceEngineerVisitDetailPage.propDecorators = {
  stepper: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_24__.ViewChild,
    args: ["stepper", {
      static: false
    }]
  }],
  selectedFile: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_24__.ViewChild,
    args: ['selectedFile', {
      static: false
    }]
  }]
};
ServiceEngineerVisitDetailPage = (0,tslib__WEBPACK_IMPORTED_MODULE_25__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_24__.Component)({
  selector: 'app-service-engineer-visit-detail',
  template: _service_engineer_visit_detail_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_service_engineer_visit_detail_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ServiceEngineerVisitDetailPage);


/***/ }),

/***/ 5367:
/*!**********************************************************************************************************************************!*\
  !*** ./src/app/cardinal/service-engineer-visit/service-engineer-visit-detail/service-engineer-visit-detail.page.scss?ngResource ***!
  \**********************************************************************************************************************************/
/***/ ((module) => {

module.exports = ".mat-stepper-vertical {\n  margin-top: 8px;\n}\n\n.mat-form-field {\n  margin-top: 16px;\n}\n\n.example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%;\n}\n\n.example-full-width {\n  width: 100%;\n}\n\n.close {\n  margin-left: 20px;\n  margin-right: 20px;\n  max-width: 500px;\n}\n\n.fselect {\n  font-size: xxx-large;\n  padding-right: 20px;\n  color: #f39e20;\n}\n\n.cus {\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  width: 100%;\n}\n\nion-datetime {\n  width: 100%;\n}\n\n::ng-deep .mat-step-header .mat-step-icon-state-done {\n  background-color: #f39e20;\n}\n\nion-popover {\n  --width: 320px;\n}\n\nion-popover.dateTimePopover {\n  --offset-y: -350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2UtZW5naW5lZXItdmlzaXQtZGV0YWlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQUE7QUFDSjs7QUFFRTtFQUNFLGdCQUFBO0FBQ0o7O0FBRUU7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQUNKOztBQUVFO0VBQ0UsV0FBQTtBQUNKOztBQUNFO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBRUo7O0FBQUU7RUFDRSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtBQUdKOztBQVNFO0VBRUUsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxXQUFBO0FBUEo7O0FBU0c7RUFDQyxXQUFBO0FBTko7O0FBUUU7RUFDRSx5QkFBQTtBQUxKOztBQU9DO0VBQ0MsY0FBQTtBQUpGOztBQU1BO0VBQ0UsNkJBQUE7QUFIRiIsImZpbGUiOiJzZXJ2aWNlLWVuZ2luZWVyLXZpc2l0LWRldGFpbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWF0LXN0ZXBwZXItdmVydGljYWwge1xuICAgIG1hcmdpbi10b3A6IDhweDtcbiAgfVxuICBcbiAgLm1hdC1mb3JtLWZpZWxkIHtcbiAgICBtYXJnaW4tdG9wOiAxNnB4O1xuICB9XG5cbiAgLmV4YW1wbGUtZm9ybSB7XG4gICAgbWluLXdpZHRoOiAxNTBweDtcbiAgICBtYXgtd2lkdGg6IDUwMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIFxuICAuZXhhbXBsZS1mdWxsLXdpZHRoIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuICAuY2xvc2Uge1xuICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xuICAgIG1hcmdpbi1yaWdodDogMjBweDtcbiAgICBtYXgtd2lkdGg6IDUwMHB4O1xuICB9XG4gIC5mc2VsZWN0e1xuICAgIGZvbnQtc2l6ZTogeHh4LWxhcmdlO1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gICAgY29sb3I6ICNmMzllMjA7XG4gIH1cblxuICAvLyA6Om5nLWRlZXAgLm1hdC1zZWxlY3QtdHJpZ2dlciB7XG4gIC8vICAgbWluLXdpZHRoOiA4MHZ3O1xuICAvLyAgIG1pbi1oZWlnaHQ6IDEwMHZ3O1xuICAvLyB9XG4gIFxuICAvLyA6Om5nLWRlZXAgLm1hdC1zZWxlY3QtcGFuZWwge1xuICAvLyAgIG1heC1oZWlnaHQ6IDgwdmggIWltcG9ydGFudDtcbiAgLy8gfVxuXG4gIC5jdXN7XG4gIFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlciA7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgfVxuICAgaW9uLWRhdGV0aW1lIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuICA6Om5nLWRlZXAgLm1hdC1zdGVwLWhlYWRlciAubWF0LXN0ZXAtaWNvbi1zdGF0ZS1kb25lIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAgI2YzOWUyMDsgXG4gfVxuIGlvbi1wb3BvdmVyIHtcbiAgLS13aWR0aDogMzIwcHg7XG59XG5pb24tcG9wb3Zlci5kYXRlVGltZVBvcG92ZXIge1xuICAtLW9mZnNldC15OiAtMzUwcHggIWltcG9ydGFudDtcbiAgfSJdfQ== */";

/***/ }),

/***/ 2113:
/*!**********************************************************************************************************************************!*\
  !*** ./src/app/cardinal/service-engineer-visit/service-engineer-visit-detail/service-engineer-visit-detail.page.html?ngResource ***!
  \**********************************************************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      Field Visit Details\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <!-- <ion-buttons (click)=\"refreshPage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons> -->\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <mat-vertical-stepper [linear]=\"isLinear\" #stepper>\n    <ng-template matStepperIcon=\"edit\">\n      <mat-icon>done</mat-icon>\n    </ng-template>\n    <!-- Detail Tab -->\n    <mat-step [stepControl]=\"detailFormGroup\">\n      <form [formGroup]=\"detailFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Details</ng-template>\n        <!-- complaint_no -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Complaint No.</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.complaintno\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- complaint_date -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Complaint Date</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.complaintdate\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Document Type -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Document Type</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.doctype[0].name\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Name of Complainer -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Name of Complainer</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.nameofcomplainer\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Designation of Complainer -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Designation of Complainer</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.desofcomplnr[0].name\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Contact No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Contact No</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.contnumber\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Email ID -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Email ID</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.email\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Event Date -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Event Date</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.eventdate\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Complaint Description -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Complaint Description</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.description\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperNext (click)=\"matDetailSettper($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Product Compliant Tab-->\n    <mat-step [stepControl]=\"productCompliantFormGroup\" *ngIf=\"isProductCompliantTab\">\n      <form [formGroup]=\"productCompliantFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Product Compliant</ng-template>\n        <!-- Lot No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Lot No.</mat-label>\n          <input matInput >\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperProductCompliant($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Consumables Tab  -->\n    <mat-step [stepControl]=\"consumablesFormGroup\" *ngIf=\"isConsumablesTab\">\n      <form [formGroup]=\"consumablesFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Consumables</ng-template>\n        <!-- Lot No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Lot No.</mat-label>\n          <input matInput>\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperConsumables($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Equiment Tab-->\n    <mat-step [stepControl]=\"equimentFormGroup\" *ngIf=\"isEquimentTab\">\n      <form [formGroup]=\"equimentFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Equiment</ng-template>\n        <!-- Serial No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Serial No.</mat-label>\n          <input matInput formControlName=\"serialNoCtrl\" [(ngModel)]=\"service.serialno\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.serialNo\">\n              <div *ngIf=\"equimentFormGroup.get('serialNoCtrl').hasError(validation.type) && equimentStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Contract Type -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Contract Type</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"contracttypeSelected\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Invoice No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Invoice No.</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.invoiceno\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Invoice Date -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Invoice Date</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.invoicedate\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Error Code -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Error Code</mat-label>\n          <mat-select [(ngModel)]=\"errorCodeSelected\" formControlName=\"errorCodeCtrl\"\n            (selectionChange)=\"errorCodeSelectedChange(errorCodeSelected)\">\n            <mat-option *ngFor=\"let errorCode of errorCodeList\" [value]=\"errorCode\">\n              {{errorCode.name}}\n            </mat-option>\n          </mat-select>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.errorCodeCtrlErrorMessage\">\n              <div *ngIf=\"equimentFormGroup.get('errorCodeCtrl').hasError(validation.type) && equimentStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Dealer Name -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Dealer Name</mat-label>\n          <input matInput formControlName=\"dealerNameCtrl\" [(ngModel)]=\"service.newdealername\">\n        </mat-form-field>\n        <!-- Sales Representative -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Sales Representative</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.salesrep\"\n            [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Invoice Date -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Installation Date</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.installationdate\"\n            [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperEquiment($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- SKU Details Tab-->\n    <mat-step [stepControl]=\"skuDetailsFormGroup\">\n      <form [formGroup]=\"skuDetailsFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>SKU Details</ng-template>\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>SKU Code</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.skucode\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- SKU Name / Description -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>SKU Name / Description</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.skuname\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Brand Name -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Brand Name</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.brandname\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Product to be returned -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Product to be returned</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"productToBeReturn\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperSKUDetails($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Customer Detail Tab -->\n    <mat-step [stepControl]=\"customerDetailFormGroup\">\n      <form [formGroup]=\"customerDetailFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Customer Detail</ng-template>\n        <!-- Customer Name -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Customer Name</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.custname\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Customer Address 1 -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Customer Address 1</mat-label>\n          <input matInput formControlName=\"customerAddress1Ctrl\" [(ngModel)]=\"service.add1\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.customerAddress1\">\n              <div\n                *ngIf=\"customerDetailFormGroup.get('customerAddress1Ctrl').hasError(validation.type) && customerDetailStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Customer Address 2 -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Customer Address 2</mat-label>\n          <input matInput formControlName=\"customerAddress2Ctrl\" [(ngModel)]=\"service.add2\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.customerAddress2\">\n              <div\n                *ngIf=\"customerDetailFormGroup.get('customerAddress2Ctrl').hasError(validation.type) && customerDetailStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Customer Address 3 -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Customer Address 3</mat-label>\n          <input matInput formControlName=\"customerAddress3Ctrl\" [(ngModel)]=\"service.add3\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.customerAddress3\">\n              <div\n                *ngIf=\"customerDetailFormGroup.get('customerAddress3Ctrl').hasError(validation.type) && customerDetailStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Pin Code -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Pin Code</mat-label>\n          <input type=\"number\" matInput formControlName=\"pinCodeCtrl\" [(ngModel)]=\"service.pincode\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.pinCode\">\n              <div\n                *ngIf=\"customerDetailFormGroup.get('pinCodeCtrl').hasError(validation.type) && customerDetailStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Area -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Area</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.area[0].name\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- City -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>City</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.city[0].name\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- State -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>State</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.state[0].name\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Country -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Country</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.country[0].name\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperConsumables($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Vender Tab  -->\n    <mat-step [stepControl]=\"venderFormGroup\">\n      <form [formGroup]=\"venderFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Vendor Details</ng-template>\n        <!-- Service Engineer Name -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Service Engineer Name</mat-label>\n          <input matInput [disabled]='true'  [(ngModel)]=\"service.serviceengname.name\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!--Service Engineer Contact No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Contact No</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.servicontactno\" [ngModelOptions]=\"{standalone: true}\" >\n        </mat-form-field>\n        <!-- Date of Visit -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Date of Visit</mat-label>\n          <input type=\"date\" matInput [disabled]='true'  [(ngModel)]=\"service.dateofvisit\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Service Vendor Remarks -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Service Vendor Remarks</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"service.serivevendorremark\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperConsumables($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Engineer Details  -->\n    <mat-step [stepControl]=\"engineerDetailsFormGroup\">\n      <form [formGroup]=\"engineerDetailsFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Engineer Details</ng-template>\n        <!-- Problem Observed -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Problem Observed</mat-label>\n          <input matInput formControlName=\"problemObservedCtrl\" [(ngModel)]=\"service.problem_observed\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.problemObservedMessage\">\n              <div\n                *ngIf=\"engineerDetailsFormGroup.get('problemObservedCtrl').hasError(validation.type) && engineerDetailStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Field Visit Remarks -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Field Visit Remarks</mat-label>\n          <textarea matInput [maxLength]=\"250\" formControlName=\"fieldVisitRemarksCtrl\" [(ngModel)]=\"service.field_visit_remark\"></textarea>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.fieldVisitRemarksMessage\">\n              <div\n                *ngIf=\"engineerDetailsFormGroup.get('fieldVisitRemarksCtrl').hasError(validation.type) && engineerDetailStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n       <!-- Proposed Action -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Proposed Action</mat-label>\n          <mat-select [(ngModel)]=\"proposedActionSelected\" formControlName=\"proposedActionCtrl\"\n            (selectionChange)=\"proposedActionClick(proposedActionSelected)\">\n            <mat-option *ngFor=\"let proposedActionObject of proposedActionList\" [value]=\"proposedActionObject\">\n              {{proposedActionObject.name}}\n            </mat-option>\n          </mat-select>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.proposedActionCtrlErrorMessage\">\n              <div *ngIf=\"engineerDetailsFormGroup.get('proposedActionCtrl').hasError(validation.type) && engineerDetailStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Dealer Billing Address -->\n        <mat-form-field class=\"example-full-width\" *ngIf=\"isDealerBillingAddress\">\n          <mat-label>Dealer Billing Address</mat-label>\n          <mat-select  formControlName=\"dealerBillingAddressCtrl\" [(ngModel)]=\"dealerBillingAddressSelected\"\n            (selectionChange)=\"dealerBillingAddressClick(dealerBillingAddressSelected)\">\n            <mat-option *ngFor=\"let dealerBillingAddressObject of dealerBillingAddressList\" [value]=\"dealerBillingAddressObject\">\n              {{dealerBillingAddressObject.name}}\n            </mat-option>\n          </mat-select>\n        </mat-form-field>\n        <!-- Dealer Shipping Address -->\n        <mat-form-field class=\"example-full-width\" *ngIf=\"isDealerShippingAddress\">\n          <mat-label>Dealer Shipping Address</mat-label>\n          <mat-select  formControlName=\"dealerShippingAddressCtrl\" [(ngModel)]=\"dealerShippingAddressSelected\"\n            (selectionChange)=\"dealerBillingAddressClick(dealerShippingAddressSelected)\">\n            <mat-option *ngFor=\"let dealerShippingAddressObject of dealerShippingAddressList\" [value]=\"dealerShippingAddressObject\">\n              {{dealerShippingAddressObject.name}}\n            </mat-option>\n          </mat-select>\n        </mat-form-field>\n        <!-- Complaint Status -->\n         <mat-form-field class=\"example-full-width\" *ngIf=\"complaintStatusControlVisible\">\n          <mat-label class=\"asterisk_input\">Complaint Status</mat-label>\n          <mat-select [(ngModel)]=\"complaintStatusSelected\" formControlName=\"complaintStatusCtrl\"\n          (selectionChange)=\"complaintStatusClick(complaintStatusSelected)\">\n          <mat-option *ngFor=\"let complaintStatusObject of complaintStatusList\" [value]=\"complaintStatusObject\">\n            {{complaintStatusObject.name}}\n          </mat-option>\n        </mat-select>\n            <div padding-left>\n              <ng-container *ngFor=\"let validation of validation_messages.complaintStatusCtrlErrorMessage\">\n                <div\n                  *ngIf=\"engineerDetailsFormGroup.get('complaintStatusCtrl').hasError(validation.type) && engineerDetailStepValid\">\n                  <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                </div>\n              </ng-container>\n            </div>\n        </mat-form-field>\n         <!-- Assign To -->\n         <mat-form-field class=\"example-full-width\" *ngIf=\"assignToCtrlVisible\">\n          <mat-label>Assign To</mat-label>\n          <mat-select  formControlName=\"assignToCtrl\" \n          (selectionChange)=\"assignToClick()\" [(ngModel)]=\"assignToSelected\">\n          <mat-option *ngFor=\"let serviceManagerObject of serviceManagerList\" [value]=\"serviceManagerObject\">\n            {{serviceManagerObject.name}}\n          </mat-option>\n        </mat-select>\n        </mat-form-field>\n        <!-- Closure at Field -->\n        <div>\n          <ion-row>\n            <ion-col no-padding>\n              <ion-button size=\"default\" class=\"submit-btn\" [disabled]=\"!closureAtFieldBtnVisible\" expand=\"block\" color=\"primary\" (click)=\"closureAtFieldBtnClick()\">Closure at Field\n              </ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n        <div>\n          <!-- <button mat-button  class=\"submit-btn\" (click)=\"takePicture()\">Camera</button> -->\n          \n          <button mat-button matStepperNext (click)=\"matSettperSpareRequired(spareRequiredFormGroup)\" *ngIf=\"engineerDetailsNextButtonVisible\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Spare Required -->\n    <mat-step [stepControl]=\"spareRequiredFormGroup\" *ngIf=\"isSpareRequiredTabVisible\">\n      <form [formGroup]=\"spareRequiredFormGroup\" class=\"example-form\">\n      <ng-template matStepLabel>Spare Required</ng-template>\n      <!-- Spare SKU Code -->\n      <mat-form-field class=\"example-full-width\">\n        <mat-label class=\"asterisk_input\">Spare SKU Code</mat-label>\n        <mat-select formControlName=\"spareSKUCodeDropDownCtrl\" [(ngModel)]=\"spareSKUCodeSelected\"\n          (selectionChange)=\"spareSKUCodeClick(spareSKUCodeSelected)\">\n          <mat-option *ngFor=\"let spareSKUCodeObject of spareSKUCodeList\" [value]=\"spareSKUCodeObject\">\n            {{spareSKUCodeObject.name}}\n          </mat-option>\n        </mat-select>\n        <div padding-left>\n          <ng-container *ngFor=\"let validation of validation_messages.spareSKUCodeDropDownCtrlErrorMessage\">\n            <div *ngIf=\"spareRequiredFormGroup.get('spareSKUCodeDropDownCtrl').hasError(validation.type) && spareRequiredStepValid\">\n              <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n            </div>\n          </ng-container>\n        </div>\n      </mat-form-field>\n      <!-- Quantity -->\n      <mat-form-field class=\"example-full-width\">\n        <mat-label class=\"asterisk_input\">Quantity</mat-label>\n        <input matInput type=\"number\" formControlName=\"quantityCtrl\" [disabled]=\"true\">\n        <!-- <div padding-left>\n          <ng-container *ngFor=\"let validation of validation_messages.quantityCtrlErrorMessage\">\n            <div *ngIf=\"spareRequiredFormGroup.get('quantityCtrl').hasError(validation.type) && spareRequiredStepValid\">\n              <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n            </div>\n          </ng-container>\n        </div> -->\n      </mat-form-field>\n      <!-- Dealer Name -->\n      <mat-form-field class=\"example-full-width\">\n        <mat-label>Dealer Name</mat-label>\n        <input matInput formControlName=\"dealerNameSpareRequiredCtrl\" [(ngModel)]=\"tempNewDealer\" [disabled]=\"true\">\n      </mat-form-field>\n      <!-- COP Sales Order Reference -->\n      <mat-form-field class=\"example-full-width\">\n        <mat-label>COP Sales Order Reference</mat-label>\n        <input matInput formControlName=\"copSalesOrderReferenceCtrl\">\n      </mat-form-field>\n      <!-- Spare Request -->\n      <div>\n        <ion-row>\n          <ion-col no-padding>\n           \n            <ion-button size=\"default\"  [disabled]=\"!tempCartStatus\"  class=\"submit-btn\" expand=\"block\" color=\"primary\" (click)=\"punchedCOPSalesOrder()\">Spare Request\n            </ion-button>\n          </ion-col>\n          <ion-col no-padding>\n            <ion-button size=\"default\"  [disabled]=\"!spareRequiredFormGroup.valid\" class=\"submit-btn\" expand=\"block\" color=\"primary\" (click)=\"addToCart()\">Add To Cart\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </div>\n      <mat-list dense>\n        <mat-icon matListIcon>add_shopping_cart</mat-icon>\n        <mat-list-item *ngFor=\"let spareItem of tempSpareCart\">\n          <ion-col size=\"10\">\n            <p matLine style=\"font-weight: bolder;\"> {{spareItem.name}} </p>\n            <p matLine> {{spareItem.dealer_name}} </p>\n            <p matLine> {{spareItem.qty}} </p>\n          </ion-col>\n          <ion-col size=\"2\">\n            <button mat-icon-button color=\"warn\" aria-label=\"Example icon button with a heart icon\" (click)=\"removeFromCart(spareItem)\">\n              <mat-icon>delete_sweep</mat-icon>\n            </button>\n            \n          </ion-col>\n          <mat-divider></mat-divider>\n        </mat-list-item>\n      </mat-list>\n      <div>\n        <button mat-button matStepperPrevious class=\"submit-btn\">Back</button>\n        <button mat-button matStepperNext (click)=\"matSettperSpareRequired(spareRequiredFormGroup)\">Next</button>\n      </div>\n      </form>\n    </mat-step>\n     <!--Spare Ordered Tab-->\n     <mat-step [stepControl]=\"spareOrderedTab\" *ngIf=\"spareOrderedTabVisible\">\n      <form [formGroup]=\"spareOrderedTab\" class=\"example-form\">\n        <ng-template matStepLabel>Spare Ordered Tab</ng-template>\n        <mat-list dense>\n         <mat-list-item *ngFor=\"let orderItem of spareRequiredList\" style=\"padding: top 0px;\">\n            <ion-col>\n              <p matLine>Order Reference: {{orderItem.orderno}} </p>\n              <p matLine>Spare SKU Code: {{orderItem.sapresku}} </p>\n              <p matLine>Quantity: {{orderItem.qty}} </p>\n              <p matLine>Dealer Name: {{orderItem.dealername}} </p>\n            </ion-col>\n          <mat-divider></mat-divider>\n          </mat-list-item>\n        </mat-list>\n      </form>\n    </mat-step>\n    <!-- Spare Installation Tab -->\n    <mat-step [stepControl]=\"spareInstallationFormGroup\" *ngIf=\"isSpareInstallationTabVisible\">\n      <form [formGroup]=\"spareInstallationFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Spare Installation</ng-template>\n        <!-- COP Sales order Reference -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">COP Sales order Reference</mat-label>\n          <mat-select formControlName=\"copSalesOrderReferenceDropDownCtrl\" [(ngModel)]=\"copSalesOrderReferenceSelected\"\n            (selectionChange)=\"copSalesOrderReferenceDropDownChange(copSalesOrderReferenceSelected)\">\n            <mat-option *ngFor=\"let copSalesOrderReferenceObject of copSalesOrderReferenceList\" [value]=\"copSalesOrderReferenceObject\">\n              {{copSalesOrderReferenceObject.orderno}}\n            </mat-option>\n          </mat-select>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.copSalesOrderReferenceDropDownCtrlErrorMessage\">\n              <div *ngIf=\"spareInstallationFormGroup.get('copSalesOrderReferenceDropDownCtrl').hasError(validation.type) && spareInstallationStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n         <!-- SKU Code -->\n         <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">SKU Code</mat-label>\n          <mat-select formControlName=\"skuCodeCtrl\" [(ngModel)]=\"skuCodeSelected\"\n            (selectionChange)=\"skuCodeListChange(skuCodeSelected)\">\n            <mat-option *ngFor=\"let skuCodeListObject of skuCodeList\" [value]=\"skuCodeListObject\">\n              {{skuCodeListObject.sapresku}}\n            </mat-option>\n          </mat-select>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.skuCodeCtrlErrorMessage\">\n              <div *ngIf=\"spareInstallationFormGroup.get('skuCodeCtrl').hasError(validation.type) && spareInstallationStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n       <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Quantity</mat-label>\n          <input type=\"number\" formControlName=\"spareReceivedQuantityCtrl\" matInput [disabled]=\"true\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.spareReceivedQuantityCtrlErrorMessage\">\n              <div\n                *ngIf=\"spareInstallationFormGroup.get('spareReceivedQuantityCtrl').hasError(validation.type) && spareInstallationStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n\n        <!-- Spare Received Date -->\n        <div class=\"example-full-width bottom-border\">\n          <ion-label  class=\"asterisk_input\" style=\"color: darkgray;\">Spare Received Date</ion-label>\n          <section class=\"cus\">\n           <!-- <ion-datetime  style=\"--padding-start:0px\" displayFormat=\"DD-MM-YYYY\"  formControlName=\"spareReceivedDateCtrl\">\n          </ion-datetime>\n          <ion-icon name=\"calendar\"></ion-icon>  -->\n          <ion-item>\n            <ion-input placeholder=\"Select Date\" [value]=\"datereceived\"></ion-input>\n            <ion-button fill=\"clear\" id=\"open-date-input-1\">\n              <ion-icon icon=\"calendar\"></ion-icon>\n            </ion-button>\n            <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\n              <ng-template>\n                <ion-datetime\n                  #popoverDatetime2\n                  formControlName=\"spareReceivedDateCtrl\"\n                  presentation=\"date\"\n                  showDefaultButtons=\"true\"\n                  (ionChange)=\"datereceived = formatDate(popoverDatetime2.value)\"\n                ></ion-datetime>\n              </ng-template>\n            </ion-popover>\n          </ion-item>\n        </section>\n        </div>\n        <div padding-left>\n          <ng-container *ngFor=\"let validation of validation_messages.spareReceivedDateCtrlErrorMessage\">\n            <div\n              *ngIf=\"spareInstallationFormGroup.get('spareReceivedDateCtrl').hasError(validation.type) && spareInstallationStepValid\">\n              <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n            </div>\n          </ng-container>\n        </div>\n        <!-- Repair Report -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Repair Report</mat-label>\n          <textarea matInput formControlName=\"repairReportCtrl\"></textarea>\n        </mat-form-field>\n        <!-- Completion Date -->\n        <div class=\"example-full-width bottom-border\">\n          <ion-label class=\"asterisk_input\" style=\"color: darkgray;\">Completion Date</ion-label>\n          <section class=\"cus\">\n           <!-- <ion-datetime  style=\"--padding-start:0px\" displayFormat=\"DD-MM-YYYY\"  formControlName=\"completionDateCtrl\">\n          </ion-datetime>\n          <ion-icon name=\"calendar\"></ion-icon>  -->\n          <ion-item>\n            <ion-input placeholder=\"Select Date\" [value]=\"datecomplete\"></ion-input>\n            <ion-button fill=\"clear\" id=\"open-date-input-2\">\n              <ion-icon icon=\"calendar\"></ion-icon>\n            </ion-button>\n            <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-2\" show-backdrop=\"false\">\n              <ng-template>\n                <ion-datetime\n                  #popoverDatetime2\n                  formControlName=\"completionDateCtrl\"\n                  presentation=\"date\"\n                  showDefaultButtons=\"true\"\n                  (ionChange)=\"datecomplete = formatDate1(popoverDatetime2.value)\"\n                ></ion-datetime>\n              </ng-template>\n            </ion-popover>\n          </ion-item>\n        </section>\n        </div>\n        <div padding-left>\n          <ng-container *ngFor=\"let validation of validation_messages.completionDateCtrlErrorMessage\">\n            <div\n              *ngIf=\"spareInstallationFormGroup.get('completionDateCtrl').hasError(validation.type) && spareInstallationStepValid\">\n              <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n            </div>\n          </ng-container>\n        </div>\n        <!-- Replaced Spare Part Serial No. -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Replaced Spare Part Serial No</mat-label>\n          <input matInput formControlName=\"replacedSparePartSerialNoCtrl\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.replacedSparePartSerialNoCtrlErrorMessage\">\n              <div\n                *ngIf=\"spareInstallationFormGroup.get('replacedSparePartSerialNoCtrl').hasError(validation.type) && spareInstallationStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Service Attended by -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Service Attended by</mat-label>\n          <input matInput formControlName=\"serviceAttendedCtrl\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.serviceAttendedCtrlErrorMessage\">\n              <div\n                *ngIf=\"spareInstallationFormGroup.get('serviceAttendedCtrl').hasError(validation.type) && spareInstallationStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Defective Spare Part No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Defective Spare Part No</mat-label>\n          <input matInput formControlName=\"defectiveSparePartNoCtrl\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.defectiveSparePartNoCtrlErrorMessage\">\n              <div\n                *ngIf=\"spareInstallationFormGroup.get('defectiveSparePartNoCtrl').hasError(validation.type) && spareInstallationStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Def Spare Docket No. -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Def Spare Docket No</mat-label>\n          <input matInput formControlName=\"defSpareDocketNoCtrl\">\n        </mat-form-field>\n        <!-- Def Spare Courier -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Def Spare Courier</mat-label>\n          <input matInput formControlName=\"defSpareCourierCtrl\">\n        </mat-form-field>\n        <!-- Def Spare Sent Date -->\n        <div class=\"example-full-width bottom-border\">\n          <ion-label style=\"color: darkgray;\">Def Spare Sent Date</ion-label>\n          <section class=\"cus\">\n           <!-- <ion-datetime  style=\"--padding-start:0px\" displayFormat=\"DD-MM-YYYY\"  formControlName=\"defSpareSentDateCtrl\">\n          </ion-datetime>\n          <ion-icon name=\"calendar\"></ion-icon>  -->\n          <ion-item>\n            <ion-input placeholder=\"Select Date\" [value]=\"datedef\"></ion-input>\n            <ion-button fill=\"clear\" id=\"open-date-input-3\">\n              <ion-icon icon=\"calendar\"></ion-icon>\n            </ion-button>\n            <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-3\" show-backdrop=\"false\">\n              <ng-template>\n                <ion-datetime\n                  #popoverDatetime2\n                  formControlName=\"defSpareSentDateCtrl\"\n                  presentation=\"date\"\n                  showDefaultButtons=\"true\"\n                  (ionChange)=\"datedef = formatDate2(popoverDatetime2.value)\"\n                ></ion-datetime>\n              </ng-template>\n            </ion-popover>\n          </ion-item>\n        </section>\n        </div>\n        <!-- Spare Install Closed -->\n        <section class=\"example-full-width\" *ngIf=\"isSpareInstallClosedCtrlVisible\">\n          <mat-checkbox formControlName=\"spareInstallClosedCtrl\" (change)=\"spareInstallClosedCheckbox($event.checked)\"\n            class=\"example-margin\">Spare Install Closed</mat-checkbox>\n        </section>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext [disabled]=\"!spareInstallationFormGroup.valid\" (click)=\"spareInstallationSaveToOB($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Attachment -->\n    <mat-step [stepControl]=\"attachmentFormGroup\" *ngIf=\"isAttachmentTabVisible\">\n      <form [formGroup]=\"attachmentFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Attachment</ng-template>\n       \n        <section *ngIf=\"isdesktop===false\">\n          <ion-button color=\"light\"(click)=\"ChosePic()\">Select Attachment\n            <ion-icon color=\"primary\" name=\"Photos\" slot=\"icon-only\"></ion-icon>\n          </ion-button>\n        </section>\n        <section *ngIf=\"isdesktop===true\">\n         \n          <ion-col size=\"2\">\n            <input type=\"file\" name=\"file\" accept=\"image/*,application/pdf\" id='selectedFile' #selectedFile (change)=\"uploadcompImage($event)\" class=\"inputfile\"/>\n          </ion-col>\n        </section>\n\n        <section *ngIf=\"this.cardinalFileType=='image'\">\n          <ion-row *ngFor=\"let imgg of imgcomp;\">\t\n            <ion-col>\t\n              <img [src]=\"'data:image/jpeg;base64,'+imgg\" (click)=\"ImageViewr(imgg,imgcomp)\" *ngIf=\"imgg\" style=\"width: 35px; height: 35px;\">\t\n            </ion-col>\t\n          </ion-row>\n        </section>\n        <section *ngIf=\"this.cardinalFileType=='pdf'\">\n          <ion-row *ngFor=\"let pdfItem of pdfFileSelectedURI;\">\n            <ion-col size=\"2\">\n              <img src=\"./assets/transparent-pdf1.png\" style=\"width: 50px; height: 50px;\">\n            </ion-col>\n            <ion-col>\n              <ion-row>\n               <ion-col>\n                 <ion-row *ngIf=\"isdesktop===false\">\n                  <ion-label>{{pdfItem.file_name}}</ion-label>\n                 </ion-row>\n                 <!-- <ion-row margin-top=\"5px\">\n                  <ion-label>{{pdfItem.size / 1024 / 1024 }}</ion-label>\n                 </ion-row> -->\n               </ion-col>\n              </ion-row>\n            </ion-col>\n            \n          </ion-row>\n        </section>\n\n\n\n        \n          <!-- <mat-label>Select Attachment</mat-label> -->\n        <button mat-button matStepperPrevious>Next</button>\n            \n     </form>\n    </mat-step >\n    <!-- Service Manager Tab -->\n    <mat-step [stepControl]=\"serviceManagerFormGroup\" *ngIf=\"isServiceManagerTabVisible\">\n      <form [formGroup]=\"serviceManagerFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Service Manager</ng-template>\n        <!-- Service Manager Remark -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Service Manager Remark</mat-label>\n          <input matInput formControlName=\"serviceManagerRemarkCtrl\">\n        </mat-form-field>\n        <!-- Defective Spare Part No. -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Defective Spare Part No</mat-label>\n          <input matInput type=\"text\" formControlName=\"defectiveSparePartNoCtrl\">\n        </mat-form-field>\n        <!-- Defective Spare Part Received Date -->\n        <div class=\"example-full-width bottom-border\">\n          <ion-label style=\"color: darkgray;\">Defective Spare Part Received Date</ion-label>\n          <section class=\"cus\">\n           <!-- <ion-datetime  style=\"--padding-start:0px\" displayFormat=\"DD-MM-YYYY\"  formControlName=\"defectiveSparePartReceivedDateCtrl\">\n          </ion-datetime>\n          <ion-icon name=\"calendar\"></ion-icon>  -->\n          <ion-item>\n            <ion-input placeholder=\"Select Date\" [value]=\"datedefspare\"></ion-input>\n            <ion-button fill=\"clear\" id=\"open-date-input-4\">\n              <ion-icon icon=\"calendar\"></ion-icon>\n            </ion-button>\n            <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-4\" show-backdrop=\"false\">\n              <ng-template>\n                <ion-datetime\n                  #popoverDatetime2\n                  formControlName=\"defectiveSparePartReceivedDateCtrl\"\n                  presentation=\"date\"\n                  showDefaultButtons=\"true\"\n                  (ionChange)=\"datedefspare = formatDate3(popoverDatetime2.value)\"\n                ></ion-datetime>\n              </ng-template>\n            </ion-popover>\n          </ion-item>\n        </section>\n        </div>\n       \n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label>Defective Spare Part Received Date</mat-label>\n          <input type=\"date\" formControlName=\"defectiveSparePartReceivedDateCtrl\" matInput>\n        </mat-form-field> -->\n        <!-- Smart Solve Ref No. -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Smart Solve Ref No</mat-label>\n          <input matInput type=\"text\" formControlName=\"smartSolveRefNoCtrl\">\n        </mat-form-field>\n        <!-- Final Closure -->\n        <div>\n          <ion-row>\n            <ion-col no-padding>\n              <ion-button size=\"default\" class=\"submit-btn\" [disabled]=\"!serviceManagerFormGroup.valid\" expand=\"block\" color=\"primary\" (click)=\"finalClosureCheckboxServiceManager()\">Final Closure\n              </ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n        <!-- <section class=\"example-full-width\">\n          <mat-checkbox formControlName=\"finalClosureCtrl\"\n            (change)=\"finalClosureCheckbox($event.checked)\" class=\"example-margin\">Final Closure</mat-checkbox>\n        </section> -->\n      </form>\n      <div>\n        <button mat-button matStepperPrevious>Back</button>\n        <!-- <button mat-button matStepperNext (click)=\"matSettperServiceManager($event)\">Next</button> -->\n      </div>\n      \n    </mat-step>\n  </mat-vertical-stepper>\n   \n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_cardinal_service-engineer-visit_service-engineer-visit-detail_service-engineer-visit--bb3b16.js.map