"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_addedit-travel-expense_addedit-travel-expense_module_ts"],{

/***/ 97540:
/*!*************************************************************************!*\
  !*** ./src/app/addedit-travel-expense/addedit-travel-expense.module.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddeditTravelExpensePageModule": () => (/* binding */ AddeditTravelExpensePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _addedit_travel_expense_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./addedit-travel-expense.page */ 39003);







const routes = [
    {
        path: '',
        component: _addedit_travel_expense_page__WEBPACK_IMPORTED_MODULE_0__.AddeditTravelExpensePage
    }
];
let AddeditTravelExpensePageModule = class AddeditTravelExpensePageModule {
};
AddeditTravelExpensePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_addedit_travel_expense_page__WEBPACK_IMPORTED_MODULE_0__.AddeditTravelExpensePage]
    })
], AddeditTravelExpensePageModule);



/***/ }),

/***/ 39003:
/*!***********************************************************************!*\
  !*** ./src/app/addedit-travel-expense/addedit-travel-expense.page.ts ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddeditTravelExpensePage": () => (/* binding */ AddeditTravelExpensePage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _addedit_travel_expense_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./addedit-travel-expense.page.html?ngResource */ 44397);
/* harmony import */ var _addedit_travel_expense_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./addedit-travel-expense.page.scss?ngResource */ 13020);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _travel_expense_travel_expense_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../travel-expense/travel-expense.service */ 36066);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @awesome-cordova-plugins/camera/ngx */ 64587);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../provider/message-helper */ 98792);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! date-fns */ 86527);













let AddeditTravelExpensePage = class AddeditTravelExpensePage {
  constructor(travelExpenseaddeditFromBuilder, travelExpenseService, commonfun, router, route, alertCtrl, camera, msg) {
    this.travelExpenseaddeditFromBuilder = travelExpenseaddeditFromBuilder;
    this.travelExpenseService = travelExpenseService;
    this.commonfun = commonfun;
    this.router = router;
    this.route = route;
    this.alertCtrl = alertCtrl;
    this.camera = camera;
    this.msg = msg;
    this.IsexpenseListlength = false;
    this.Supportingimg64 = '';
    this.IsSupportingimg64 = false;
    this.IsSupportingrequired = false;
    this.isdesktop = false;
    this.datefrom = '';
    this.dateto = '';
    this.getrout();
    this.travelExpenseFormedit = this.travelExpenseaddeditFromBuilder.group({
      fromdate: [, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
      todate: [, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
      selectableTravelExpenseUIControl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
      txtAmount: [, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
      claimableamount: [, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required]
    });
  }

  ngOnInit() {
    setTimeout(() => {
      this.checkplatform();
    }, 1500);
  }

  formatDate(value) {
    this.ondatechange();
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_8__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_9__["default"])(value), 'dd.MM.yyyy');
  }

  formatDate1(value) {
    this.ondatechange();
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_8__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_9__["default"])(value), 'dd.MM.yyyy');
  }

  dateyyyymmddT0000Z(dt) {
    try {
      var dl1date = new Date(dt.substr(0, 4), dt.substr(5, 2) - 1, dt.substr(8, 2));
      var nmonth = dl1date.getMonth() + 1;
      var dd1 = dl1date.getDate() < 10 ? "0" + dl1date.getDate() : dl1date.getDate();
      var mm1 = nmonth < 10 ? "0" + nmonth : nmonth;
      var yyyy1 = dl1date.getFullYear(); // this.strfromdate=dd1+"-"+mm1+"-"+yyyy1

      return yyyy1 + "-" + mm1 + "-" + dd1 + "T00:00Z";
    } catch (error) {}
  }

  ondatechange() {
    let methodTAG = 'ondatechange';
    var isdateOk = true;

    try {
      var df = this.travelExpenseFormedit.controls["fromdate"].value;
      var dt = this.travelExpenseFormedit.controls["todate"].value;
      var fromdate = new Date(this.dateyyyymmddT0000Z(df)).toISOString();
      var todate = new Date(this.dateyyyymmddT0000Z(dt)).toISOString(); //if((this.plantodate<=fromdate || this.plantodate>=todate || this.planfromdate<=fromdate || this.planfromdate>=todate))

      if (todate < this.planfromdate || todate > this.plantodate || fromdate < this.planfromdate || fromdate > this.plantodate) {
        this.commonfun.presentAlert("Message", "Alert", "Expense Dates are must be in the range of Plan from date and Plan to date.");
        isdateOk = false; // return false;
      } else if (fromdate > todate) {
        this.commonfun.presentAlert("Message", "Alert", "From date must be less than To date.");
        isdateOk = false;
      }
    } catch (error) {}

    return isdateOk;
  }

  checkplatform() {
    try {
      if (!this.msg.isplatformweb) {
        //  if(!this.platform.is("desktop")){
        // 
        this.isdesktop = false;
      } else {
        this.isdesktop = true;
      }
    } catch (error) {}
  }

  getrout() {
    try {
      this.route.params.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.expenseList = this.router.getCurrentNavigation().extras.state.expenseList;
          this.selectedplan = this.router.getCurrentNavigation().extras.state.selectedplan;
          this.expenseMasterList = this.router.getCurrentNavigation().extras.state.expenseMasterList;
          ;
          var a1 = this.selectedplan.fromdate.split("-");
          var dt = new Date(a1[2] + '-' + a1[1] + '-' + a1[0] + 'T00:00Z');
          this.travelExpenseFormedit.controls["fromdate"].setValue(dt.toISOString());
          this.planfromdate = dt.toISOString();
          var a2 = this.selectedplan.todate.split("-");
          var dt2 = new Date(a2[2] + '-' + a2[1] + '-' + a2[0] + 'T00:00Z');
          this.travelExpenseFormedit.controls["todate"].setValue(dt2.toISOString());
          this.plantodate = dt2.toISOString();
          this.chklength();
        }
      });
    } catch (error) {// console.log("addsublead()-ERROR:",error);
    }
  }

  chklength() {
    try {
      if (this.expenseList.length > 0) this.IsexpenseListlength = true;else this.IsexpenseListlength = false;
    } catch (error) {
      this.IsexpenseListlength = false;
    }
  }

  uploadImage(str) {
    try {
      var file = str.target.files[0];
      var myreader = new FileReader();

      myreader.onloadend = e => {
        var b64 = myreader.result.toString().replace(/^data:.+;base64,/, '');
        this.Supportingimg64 = b64;
        this.IsSupportingimg64 = false;
      };

      myreader.readAsDataURL(file);
    } catch (error) {}
  }

  onChangeAmount() {
    try {
      var amt = this.travelExpenseFormedit.controls["txtAmount"].value;
      var claimableamount = this.travelExpenseFormedit.controls["claimableamount"].value;

      if (amt > claimableamount) {
        this.commonfun.presentAlert("Message", "Alert!", "Entered amount is greater than claimbal amount.");
        this.travelExpenseFormedit.controls["txtAmount"].setValue('');
      }
    } catch (error) {// console.log("Error: onChangeAmount: ",error);
    }
  }

  onAdd(val) {
    console.log("onAdd");
    let methodTAG = 'onAdd';

    try {
      if (this.ondatechange() == false) {
        return;
      }

      this.strfromdate = this.commonfun.Dateconversionddmmyyyy(val.fromdate);
      this.strtodate = this.commonfun.Dateconversionddmmyyyy(val.todate);

      if (this.expenseList.length > 0) {
        var slitem = {
          "fromdate": this.strfromdate,
          "todate": this.strtodate,
          "travelexpense": val.selectableTravelExpenseUIControl,
          "amount": val.txtAmount,
          "claimableamount": val.claimableamount,
          "Supporting": this.Supportingimg64
        };
        let objOne = {
          "fromdate": this.strfromdate,
          "todate": this.strtodate,
          "travelexpense": val.selectableTravelExpenseUIControl.sname
        };

        try {
          var checkAlreadyAdded = false;
          this.expenseList.forEach(obj => {
            let objTwo = {
              "fromdate": obj.fromdate,
              "todate": obj.todate,
              "travelexpense": obj.travelexpense.sname
            };

            if (JSON.stringify(objOne) === JSON.stringify(objTwo)) {
              console.log("True");
              checkAlreadyAdded = true;
            } else {
              console.log("False");
              checkAlreadyAdded = false;
            }
          });

          if (checkAlreadyAdded == false) {
            this.expenseList.push(slitem);
          } else {
            this.commonfun.presentAlert("Add Expense", "Validation", "You can not add same expense");
          }
        } catch (error) {
          console.log("True", error);
        }
      } // if(this.expenseList)
      //   {
      //      var slitem={"fromdate":this.strfromdate,"todate":this.strtodate,"travelexpense":val.selectableTravelExpenseUIControl,"amount":val.txtAmount,"claimableamount":val.claimableamount,"Supporting":this.Supportingimg64};
      //        this.expenseList.push(slitem);
      //  }	
      else {
        var slitem1 = [{
          "fromdate": this.strfromdate,
          "todate": this.strtodate,
          "travelexpense": val.selectableTravelExpenseUIControl,
          "amount": val.txtAmount,
          "claimableamount": val.claimableamount,
          "Supporting": this.Supportingimg64
        }];
        this.expenseList = slitem1;
      }

      this.Resetpage();
    } catch (error) {}
  } //Select Image from library


  getimage() {
    try {
      const options = {
        quality: 50,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        targetWidth: 1500,
        targetHeight: 1500
      };
      this.camera.getPicture(options).then(imageData => {
        this.Supportingimg64 = imageData;
        this.IsSupportingimg64 = false;
      }, err => {
        this.commonfun.presentAlert("Message", "Error", err);
      });
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  getSupportingImage() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const alert = yield _this.alertCtrl.create({
          header: 'Select Option',
          message: "Select Option to get Picture.",
          buttons: [{
            text: 'Gallery',
            handler: data => {
              _this.getimage();
            }
          }, {
            text: 'Camera',
            handler: data => {
              _this.takePicture();
            }
          }]
        });
        yield alert.present();
      } catch (error) {
        _this.commonfun.presentAlert("Message", "Error", error);
      }
    })();
  } //Capture Image from Camera


  takePicture() {
    try {
      const options = {
        quality: 50,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType.CAMERA,
        targetWidth: 1500,
        targetHeight: 1500
      };
      this.camera.getPicture(options).then(imageData => {
        this.Supportingimg64 = imageData;
        this.IsSupportingimg64 = false;
      }, err => {
        this.commonfun.presentAlert("Message", "Error", err);
      });
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  ImageViewr(img) {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this2.alertCtrl.create({
        message: '<div>' + '<img class="viewImagecss" src="data:image/jpeg;base64,' + img + '">' + '</div>',
        buttons: [{
          text: 'Remove',
          handler: data => {
            _this2.Supportingimg64 = null;
            _this2.IsSupportingimg64 = _this2.IsSupportingrequired;
          }
        }, {
          text: 'OK'
        }]
      });
      yield alert.present();
    })();
  }

  Resetpage() {
    let methodTAG = 'onChangeExpense'; // console.log(methodTAG)

    try {
      // this.travelExpenseFormedit.reset();
      this.travelExpenseFormedit.controls["selectableTravelExpenseUIControl"].setValue("");
      this.travelExpenseFormedit.controls["txtAmount"].setValue("");
      this.travelExpenseFormedit.controls["claimableamount"].setValue("");
      this.Supportingimg64 = "";
      this.IsSupportingimg64 = this.IsSupportingrequired;
      this.chklength();
    } catch (error) {}
  }

  removerow(post) {
    try {
      // console.log("removerow")
      const result = this.expenseList.filter(item => item != post);
      this.expenseList = result;
      this.chklength();
    } catch (error) {
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

  onChangeExpense() {
    let methodTAG = 'onChangeExpense';

    try {
      //  console.log(methodTAG);
      this.selectedexpense = this.travelExpenseFormedit.controls["selectableTravelExpenseUIControl"].value;
      this.travelExpenseFormedit.controls["claimableamount"].setValue(this.selectedexpense.claimableamount);
      this.IsSupportingrequired = this.selectedexpense.isproof;
      this.IsSupportingimg64 = this.IsSupportingrequired;
    } catch (error) {// console.log("Error",error);
    }
  }

  onAddTravelExpense() {
    let methodTAG = 'onSaveTravelExpense';

    try {
      let navigationExtras = {
        state: {
          expenseList: this.expenseList
        }
      };
      this.router.navigate(['travel-expense'], navigationExtras);
    } catch (error) {}
  }

};

AddeditTravelExpensePage.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormBuilder
}, {
  type: _travel_expense_travel_expense_service__WEBPACK_IMPORTED_MODULE_3__.TravelExpenseService
}, {
  type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_4__.Commonfun
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_10__.Router
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_10__.ActivatedRoute
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__.AlertController
}, {
  type: _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_5__.Camera
}, {
  type: _provider_message_helper__WEBPACK_IMPORTED_MODULE_6__.Message
}];

AddeditTravelExpensePage = (0,tslib__WEBPACK_IMPORTED_MODULE_12__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_13__.Component)({
  selector: 'app-addedit-travel-expense',
  template: _addedit_travel_expense_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_addedit_travel_expense_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], AddeditTravelExpensePage);


/***/ }),

/***/ 13020:
/*!************************************************************************************!*\
  !*** ./src/app/addedit-travel-expense/addedit-travel-expense.page.scss?ngResource ***!
  \************************************************************************************/
/***/ ((module) => {

module.exports = ".forecast_div {\n  overflow-x: scroll !important;\n  overflow-x: visible !important;\n  overflow-y: hidden;\n  word-wrap: break-word;\n  height: 20vw;\n  font-size: 0.8em;\n  font-weight: 300;\n  max-width: 175px;\n}\n\n.grid-header {\n  font-weight: bold;\n  max-width: 175px;\n}\n\nh5 {\n  font-style: oblique;\n  color: darkcyan;\n  font-size: large;\n}\n\nh5 ion-icon {\n  color: lightcoral;\n}\n\n.inputfile {\n  color: transparent;\n}\n\nion-popover {\n  --width: 320px;\n}\n\nion-popover.dateTimePopover {\n  --offset-y: -350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZGVkaXQtdHJhdmVsLWV4cGVuc2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksNkJBQUE7RUFDQSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBQ0E7RUFDRSxpQkFBQTtFQUNBLGdCQUFBO0FBRUY7O0FBQ0E7RUFDRSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUVGOztBQURFO0VBQ0UsaUJBQUE7QUFHSjs7QUFDQTtFQUNFLGtCQUFBO0FBRUY7O0FBQUE7RUFDRSxjQUFBO0FBR0Y7O0FBREE7RUFDRSw2QkFBQTtBQUlGIiwiZmlsZSI6ImFkZGVkaXQtdHJhdmVsLWV4cGVuc2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcmVjYXN0X2RpdntcbiAgICBvdmVyZmxvdy14OiBzY3JvbGwhaW1wb3J0YW50O1xuICAgIG92ZXJmbG93LXg6IHZpc2libGUhaW1wb3J0YW50O1xuICAgIG92ZXJmbG93LXk6IGhpZGRlbjtcbiAgICB3b3JkLXdyYXA6IGJyZWFrLXdvcmQ7XG4gICAgaGVpZ2h0OjIwdnc7XG4gICAgZm9udC1zaXplOjAuOGVtO1xuICAgIGZvbnQtd2VpZ2h0OjMwMDtcbiAgICBtYXgtd2lkdGg6IDE3NXB4O1xufVxuLmdyaWQtaGVhZGVye1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbWF4LXdpZHRoOiAxNzVweDtcbn1cblxuaDV7XG4gIGZvbnQtc3R5bGU6IG9ibGlxdWU7XG4gIGNvbG9yOiBkYXJrY3lhbjtcbiAgZm9udC1zaXplOiBsYXJnZTtcbiAgaW9uLWljb257XG4gICAgY29sb3I6IGxpZ2h0Y29yYWw7XG4gIH1cblxufVxuLmlucHV0ZmlsZSB7XG4gIGNvbG9yOiB0cmFuc3BhcmVudDtcbn1cbmlvbi1wb3BvdmVyIHtcbiAgLS13aWR0aDogMzIwcHg7XG59XG5pb24tcG9wb3Zlci5kYXRlVGltZVBvcG92ZXIge1xuICAtLW9mZnNldC15OiAtMzUwcHggIWltcG9ydGFudDtcbiAgfVxuIl19 */";

/***/ }),

/***/ 44397:
/*!************************************************************************************!*\
  !*** ./src/app/addedit-travel-expense/addedit-travel-expense.page.html?ngResource ***!
  \************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar  color=\"primary\">\n    <ion-title>Add Expense</ion-title>\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  \n  <form [formGroup]=\"travelExpenseFormedit\" (ngSubmit)=\"onAdd(travelExpenseFormedit.value)\">\n  <ion-card>\n    <ion-card-content>\n      <ion-row>\n        <ion-col>\n          <h5 ion-text class=\"text-primary\">\n            <ion-icon name=\"wifi\"></ion-icon> Expense Detail\n          </h5>\n        </ion-col>\n      </ion-row>\n\n      <!-- <ion-row>\n        <ion-col>\n\n      <ion-item>\n        <ion-label  position=\"stacked\">From Date</ion-label>\n        <ion-datetime placeholder=\"Select Date\" formControlName=\"travelExpenseFormedit.controls['fromdate']\" (ionChange)=\"ondatechange()\" displayFormat=\"DD.MM.YYYY\" max=\"2050\"></ion-datetime>\n      </ion-item>\n\n    </ion-col>\n  </ion-row> -->\n  <ion-item>\n    <ion-label position=\"stacked\">From Date</ion-label>\n    <ion-item>\n      <ion-input placeholder=\"Select Date\" [value]=\"datefrom\"></ion-input>\n      <ion-button fill=\"clear\" id=\"open-date-input-1\">\n        <ion-icon icon=\"calendar\"></ion-icon>\n      </ion-button>\n      <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\n        <ng-template>\n          <ion-datetime\n            #popoverDatetime2\n            formControlName=\"fromdate\" \n            presentation=\"date\"\n            showDefaultButtons=\"true\"\n            max=\"2050\"\n            (ionChange)=\"datefrom = formatDate(popoverDatetime2.value)\"\n          ></ion-datetime>\n        </ng-template>\n      </ion-popover>\n    </ion-item>\n  </ion-item>\n\n  <!-- <ion-row>\n    <ion-col>\n      <ion-item>\n        <ion-label  position=\"stacked\">To Date</ion-label>\n        <ion-datetime placeholder=\"Select Date\" formControlName=\"travelExpenseFormedit.controls['todate']\" (ionChange)=\"ondatechange()\" displayFormat=\"DD.MM.YYYY\" max=\"2050\"></ion-datetime>\n      </ion-item>\n    </ion-col>\n  </ion-row> -->\n\n  <ion-item>\n    <ion-label position=\"stacked\">To Date</ion-label>\n    <ion-item>\n      <ion-input placeholder=\"Select Date\" [value]=\"dateto\"></ion-input>\n      <ion-button fill=\"clear\" id=\"open-date-input-2\">\n        <ion-icon icon=\"calendar\"></ion-icon>\n      </ion-button>\n      <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-2\" show-backdrop=\"false\">\n        <ng-template>\n          <ion-datetime\n            #popoverDatetime2\n            formControlName=\"todate\" \n            presentation=\"date\"\n            showDefaultButtons=\"true\"\n            max=\"2050\"\n            (ionChange)=\"dateto = formatDate1(popoverDatetime2.value)\"\n          ></ion-datetime>\n        </ng-template>\n      </ion-popover>\n    </ion-item>\n  </ion-item>\n\n  <ion-row>\n    <ion-col>\n      <ion-item>\n       \n        <ion-label position=\"stacked\">Travel Expense</ion-label>\n\n        <ion-select formControlName=\"selectableTravelExpenseUIControl\" \n        interface=\"popover\" multiple=\"false\" placeholder=\"Select\"\n        (ionChange)=\"onChangeExpense()\">\n          <ion-select-option *ngFor=\"let exp of expenseMasterList\" [value]=\"exp\">{{exp.sname}}</ion-select-option>\n        </ion-select>\n      </ion-item>  \n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col>\n      <ion-item>\n        <ion-label  position=\"stacked\">Amount</ion-label>\n        <ion-input type=\"number\" (change)='onChangeAmount()' formControlName=\"txtAmount\"></ion-input>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col>\n      <ion-item>\n        <ion-label  position=\"stacked\">Claimable Amount</ion-label>\n        <ion-input type=\"number\" [disabled]=\"true\" formControlName=\"claimableamount\"></ion-input>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n\n  <ion-card>\n    <ion-card-content>\n      <ion-row>\n        <ion-col *ngIf=\"isdesktop===false\">\n         <ion-button (click)=\"getSupportingImage()\">Supporting</ion-button>\n        </ion-col>\n        <ion-col *ngIf=\"isdesktop===true\">\n          <input type=\"file\" name=\"file\" accept=\"image/*\" id='selectedFile' (change)=\"uploadImage($event)\" class=\"inputfile\"/>\n\n         </ion-col>\n        <ion-col>\n          <img (click)=\"ImageViewr(Supportingimg64)\" [src]=\"'data:image/jpeg;base64,'+Supportingimg64\" *ngIf=\"Supportingimg64\" style=\"width: 50px; height: 50px;\">\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n\n\n  \n    </ion-card-content>\n  </ion-card>\n</form>\n<ion-button expand=\"block\" class=\"ion-margin-start ion-margin-end ion-margin-bottom\" type=\"submit\"\n  [disabled]=\"!travelExpenseFormedit.valid || IsSupportingimg64\" (click)=\"onAdd(travelExpenseFormedit.value)\">\n  Add\n</ion-button>\n<ion-card>\n  <ion-card-content>\n    <div style=\"overflow-x:auto\">\n      <ion-grid>\n       <ion-row nowrap >\n        <ion-col nowrap>\n        <ion-row nowrap>\n          <ion-col size=\"1\" class=\"grid-header\"></ion-col>\n          <ion-col size=\"4\" class=\"grid-header\">From</ion-col>\n         <ion-col size=\"4\" class=\"grid-header\">To</ion-col>\n         <ion-col size=\"4\" class=\"grid-header\">Travel Expense</ion-col>\n         <ion-col size=\"4\" class=\"grid-header\">Amount</ion-col>\n         <ion-col size=\"5\" class=\"grid-header\">Claimable Amount</ion-col>\n         <ion-col size=\"5\" class=\"grid-header\">Supporting</ion-col>\n\n        </ion-row>\n      </ion-col>\n       </ion-row>\n      \n       <ion-row *ngFor=\"let data of expenseList; index as i\"  nowrap>\n        <ion-col nowrap>\n       <ion-row nowrap>\n          \n          <ion-col size=\"1\" style=\"width: 100%; text-align: right;\">\n            <ion-icon name=\"trash\" (click)=\"removerow(data)\" style=\"font-size: x-large;\n            color: red;\"></ion-icon>\n          </ion-col>\n        <ion-col size=\"4\" class=\"forecast_div\">{{data.fromdate}}</ion-col>\n        <ion-col size=\"4\" class=\"forecast_div\">{{data.todate}}</ion-col>\n        <ion-col size=\"4\" class=\"forecast_div\">{{data.travelexpense.sname}}</ion-col>\n        <ion-col  size=\"4\"class=\"forecast_div\">{{data.amount}}</ion-col>\n        <ion-col size=\"4\" class=\"forecast_div\">{{data.claimableamount}}</ion-col>\n        <ion-col size=\"3\" style=\"text-align: -webkit-center;\">\n          <img [src]=\"'data:image/jpeg;base64,'+data.Supporting\" *ngIf=\"data.Supporting\" style=\"width: 35px; height: 35px;\">\n        </ion-col>\n        \n\n        </ion-row>\n       \n      </ion-col>\n        </ion-row>\n     \n\n      </ion-grid>     \n    </div>\n  </ion-card-content>\n</ion-card>\n\n\n  <ion-button expand=\"block\" class=\"ion-margin-start ion-margin-end ion-margin-bottom\" type=\"submit\"\n  [disabled]=\"!IsexpenseListlength\" (click)=\"onAddTravelExpense()\">\n  Add Expenses\n</ion-button> \n\n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_addedit-travel-expense_addedit-travel-expense_module_ts.js.map