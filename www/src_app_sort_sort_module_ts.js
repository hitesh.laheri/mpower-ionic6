"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_sort_sort_module_ts"],{

/***/ 79088:
/*!************************************************************!*\
  !*** ./src/app/common/directives/hide-header.directive.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HideHeaderDirective": () => (/* binding */ HideHeaderDirective)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ 93819);
// hide-header.directive.ts - this directive will do the actual job to hide header on content scroll in Ionic Framework.



let HideHeaderDirective = class HideHeaderDirective {
    constructor(renderer, domCtrl) {
        this.renderer = renderer;
        this.domCtrl = domCtrl;
        this.lastY = 0;
    }
    ngOnInit() {
        this.header = this.header.el;
        this.domCtrl.write(() => {
            this.renderer.setStyle(this.header, 'transition', 'margin-top 700ms');
        });
    }
    onContentScroll($event) {
        if ($event.detail.scrollTop > this.lastY) {
            this.domCtrl.write(() => {
                this.renderer.setStyle(this.header, 'margin-top', `-${this.header.clientHeight}px`);
            });
        }
        else {
            this.domCtrl.write(() => {
                this.renderer.setStyle(this.header, 'margin-top', '0');
            });
        }
        this.lastY = $event.detail.scrollTop;
    }
};
HideHeaderDirective.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2 },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__.DomController }
];
HideHeaderDirective.propDecorators = {
    header: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input, args: ['header',] }],
    onContentScroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.HostListener, args: ['ionScroll', ['$event'],] }]
};
HideHeaderDirective = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_0__.Directive)({
        selector: '[appHideHeader]'
    })
], HideHeaderDirective);



/***/ }),

/***/ 44466:
/*!*****************************************!*\
  !*** ./src/app/shared/shared.module.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SharedModule": () => (/* binding */ SharedModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _common_directives_hide_header_directive__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common/directives/hide-header.directive */ 79088);




let SharedModule = class SharedModule {
};
SharedModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        declarations: [_common_directives_hide_header_directive__WEBPACK_IMPORTED_MODULE_0__.HideHeaderDirective],
        exports: [_common_directives_hide_header_directive__WEBPACK_IMPORTED_MODULE_0__.HideHeaderDirective],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule
        ]
    })
], SharedModule);



/***/ }),

/***/ 68387:
/*!*************************************!*\
  !*** ./src/app/sort/sort.module.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SortPageModule": () => (/* binding */ SortPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _sort_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./sort.page */ 61272);
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @swimlane/ngx-datatable */ 93143);
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shared/shared.module */ 44466);









const routes = [
    {
        path: '',
        component: _sort_page__WEBPACK_IMPORTED_MODULE_0__.SortPage
    }
];
let SortPageModule = class SortPageModule {
};
SortPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_1__.SharedModule,
            _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__.NgxDatatableModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterModule.forChild(routes)
        ],
        declarations: [_sort_page__WEBPACK_IMPORTED_MODULE_0__.SortPage]
    })
], SortPageModule);



/***/ }),

/***/ 61272:
/*!***********************************!*\
  !*** ./src/app/sort/sort.page.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SortPage": () => (/* binding */ SortPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _sort_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sort.page.html?ngResource */ 40108);
/* harmony import */ var _sort_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./sort.page.scss?ngResource */ 83368);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ 80190);
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @swimlane/ngx-datatable */ 93143);
/* harmony import */ var _order_approval_show_approval_details_modal_show_approval_details_modal_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../order-approval/show-approval-details-modal/show-approval-details-modal.page */ 19592);
/* harmony import */ var _order_approval_show_approval_details_modal_approval_modal_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../order-approval/show-approval-details-modal/approval-modal.service */ 15411);
/* harmony import */ var _awesome_cordova_plugins_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @awesome-cordova-plugins/screen-orientation/ngx */ 11898);












let SortPage = class SortPage {
  constructor(alertController, storage, router, approvalModalService, screenOrientation, elementRef) {
    this.alertController = alertController;
    this.storage = storage;
    this.router = router;
    this.approvalModalService = approvalModalService;
    this.screenOrientation = screenOrientation;
    this.elementRef = elementRef;
    this.TAG = "SortPage";
    this.active = 0;
    this.ColumnMode = _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__.ColumnMode;
    this.SelectionType = _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__.SelectionType;
    this.page = new _order_approval_show_approval_details_modal_show_approval_details_modal_page__WEBPACK_IMPORTED_MODULE_4__.Page();
    this.rows = new Array();
    this.page.pageNumber = 0;
    this.page.size = 20;
  }

  ngOnInit() {
    //  this.setPage({ offset: 0 });
    // set to landscape
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    this.filterList = [{
      "name": "Date"
    }];
    this.getdata();
  }

  clearFilter() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        console.log(_this, "clearFilter");
        const alert = yield _this.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Filters',
          subHeader: 'Clear Filters',
          message: 'Would you like to clear all filters?',
          buttons: [{
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: blah => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Ok',
            handler: data => {
              console.log('Confirm Okay');
              _this.startDate = "";
              _this.endDate = "";
            }
          }]
        });
        yield alert.present();
      } catch (error) {
        console.log(_this.TAG, error);
      }
    })();
  }

  menuItemClick(index) {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  }

  applyFilter() {
    console.log(this.TAG, "applyFilter method called");

    try {
      console.log(this.TAG, "applyFilter", this.startDate.split('T')[0]);
      console.log(this.TAG, "applyFilter", this.endDate.split('T')[0]);
      this.storage.set('filterStartDate', this.startDate ? this.startDate.split('T')[0] : 'CLEAR');
      this.storage.set('filterEndDate', this.endDate ? this.endDate.split('T')[0] : 'CLEAR');
      this.router.navigateByUrl('/order-approval');
      this.startDate;
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.approvalModalService.getApprovalDetails("1", "186", "12").subscribe(data => {
      console.log("Pravin Modal DATA ", data);
      this.column_data = data.colum_names;
      this.rows = data.colum_data;
    });
  }

  getdata() {
    this.approvalModalService.getApprovalDetails("1", "186", "12").subscribe(data => {
      console.log("Pravin Modal DATA ", data);
      this.column_data = data.data.colum_names;
      this.rows = data.data.colum_data;
    });
  }

  adjustColumnMinWidth() {
    const element = this.elementRef.nativeElement;
    const rows = element.getElementsByTagName("datatable-body-row");

    for (let i = 0; i < rows.length; i++) {
      const cells = rows[i].getElementsByTagName("datatable-body-cell");

      for (let k = 0; k < cells.length; k++) {
        const cell = cells[k];
        const cellSizer = cell.children[0].children[0];
        const sizerWidth = cellSizer.getBoundingClientRect().width;

        if (this.column_data[k].minWidth < sizerWidth) {
          this.column_data[k].minWidth = sizerWidth;
        }
      }
    }
  }

};

SortPage.ctorParameters = () => [{
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.AlertController
}, {
  type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__.Storage
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_9__.Router
}, {
  type: _order_approval_show_approval_details_modal_approval_modal_service__WEBPACK_IMPORTED_MODULE_5__.ApprovalModalService
}, {
  type: _awesome_cordova_plugins_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_6__.ScreenOrientation
}, {
  type: _angular_core__WEBPACK_IMPORTED_MODULE_10__.ElementRef
}];

SortPage.propDecorators = {
  datatable: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_10__.ViewChild,
    args: ["datatable", {
      static: true
    }]
  }]
};
SortPage = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
  selector: 'app-sort',
  template: _sort_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_sort_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], SortPage);


/***/ }),

/***/ 83368:
/*!************************************************!*\
  !*** ./src/app/sort/sort.page.scss?ngResource ***!
  \************************************************/
/***/ ((module) => {

module.exports = ".box {\n  height: 100% !important;\n  display: flex;\n  flex-flow: column;\n  padding: 0px !important;\n}\n\n.list-item {\n  margin-top: 25px !important;\n}\n\nion-content {\n  --overflow: hidden;\n}\n\ndiv[scrollx=true], div[scrolly=true] {\n  position: relative;\n  overflow: hidden;\n}\n\ndiv[scrollx=true] {\n  overflow-x: auto;\n}\n\ndiv[scrolly=true] {\n  overflow-y: auto;\n}\n\n.row-custom {\n  height: -webkit-fill-available !important;\n}\n\n.custom-activated {\n  --ion-background-color: white !important;\n}\n\n.ion-item-custom {\n  --ion-background-color:#eeeeee !important;\n  padding: 0px !important;\n}\n\n.back-color {\n  background-color: #eeeeee !important;\n}\n\n.custom-ion-col {\n  padding: 0px !important;\n}\n\n.footer-back-color {\n  background-color: white;\n}\n\n.footer-btn-color {\n  background-color: #F39E20 !important;\n}\n\n::ng-deep.ngx-datatable.bootstrap .datatable-body .datatable-body-row.active {\n  background-color: #F39E20 !important;\n}\n\n@media only screen and (min-width: 481px) {\n  ::ng-deep.ngx-datatable.bootstrap .datatable-header .datatable-header-cell {\n    padding-left: 0.75rem !important;\n    padding-right: 0.25rem !important;\n    padding-top: 0.25rem !important;\n    padding-bottom: 0.25rem !important;\n  }\n}\n\n@media (min-width: 1281px) {\n  ::ng-deep.ngx-datatable.bootstrap .datatable-header .datatable-header-cell {\n    padding: 0.75rem !important;\n  }\n}\n\n::ng-deep.ngx-datatable.bootstrap .datatable-header {\n  font-weight: bold !important;\n  background-color: #F39E20 !important;\n}\n\n@media screen and (max-width: 800px) {\n  .desktop-hidden {\n    display: initial;\n  }\n\n  .mobile-hidden {\n    display: none;\n  }\n}\n\n@media screen and (min-width: 800px) {\n  .desktop-hidden {\n    display: none;\n  }\n\n  .mobile-hidden {\n    display: initial;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNvcnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksdUJBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFFQSx1QkFBQTtBQUFKOztBQUlBO0VBQ0ksMkJBQUE7QUFESjs7QUFNQTtFQUNJLGtCQUFBO0FBSEo7O0FBTUE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0FBSEo7O0FBTUE7RUFDRyxnQkFBQTtBQUhIOztBQU1BO0VBQ0csZ0JBQUE7QUFISDs7QUFNQTtFQUNJLHlDQUFBO0FBSEo7O0FBS0E7RUFDSSx3Q0FBQTtBQUZKOztBQUlBO0VBQ0kseUNBQUE7RUFDQSx1QkFBQTtBQURKOztBQUdBO0VBQ0ksb0NBQUE7QUFBSjs7QUFHQTtFQUNJLHVCQUFBO0FBQUo7O0FBRUE7RUFDSSx1QkFBQTtBQUNKOztBQUNBO0VBQ0ksb0NBQUE7QUFFSjs7QUFBQTtFQUNJLG9DQUFBO0FBR0o7O0FBQUE7RUFDSTtJQUVJLGdDQUFBO0lBQ0EsaUNBQUE7SUFDQSwrQkFBQTtJQUNBLGtDQUFBO0VBRU47QUFDRjs7QUFDQTtFQUVJO0lBRUEsMkJBQUE7RUFERjtBQUNGOztBQU1BO0VBQ0UsNEJBQUE7RUFDQSxvQ0FBQTtBQUpGOztBQVFBO0VBQ0k7SUFDRSxnQkFBQTtFQUxKOztFQU9FO0lBQ0UsYUFBQTtFQUpKO0FBQ0Y7O0FBTUU7RUFDRTtJQUNFLGFBQUE7RUFKSjs7RUFNRTtJQUNFLGdCQUFBO0VBSEo7QUFDRiIsImZpbGUiOiJzb3J0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ib3h7XG4gICAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7IFxuICAgIGRpc3BsYXk6IGZsZXg7IFxuICAgIGZsZXgtZmxvdzogY29sdW1uO1xuICAgIFxuICAgIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuXG5cbi5saXN0LWl0ZW17XG4gICAgbWFyZ2luLXRvcDogMjVweCAhaW1wb3J0YW50O1xufVxuXG5cblxuaW9uLWNvbnRlbnQge1xuICAgIC0tb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuZGl2W3Njcm9sbHg9dHJ1ZV0sZGl2W3Njcm9sbHk9dHJ1ZV0ge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG5kaXZbc2Nyb2xseD10cnVlXSB7XG4gICBvdmVyZmxvdy14OiBhdXRvO1xufVxuXG5kaXZbc2Nyb2xseT10cnVlXSB7XG4gICBvdmVyZmxvdy15OiBhdXRvO1xufVxuXG4ucm93LWN1c3RvbXtcbiAgICBoZWlnaHQ6IC13ZWJraXQtZmlsbC1hdmFpbGFibGUgIWltcG9ydGFudDtcbn1cbi5jdXN0b20tYWN0aXZhdGVkIHtcbiAgICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yIDogd2hpdGUgIWltcG9ydGFudDtcbn1cbi5pb24taXRlbS1jdXN0b217XG4gICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojZWVlZWVlICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG59XG4uYmFjay1jb2xvcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlZWVlICFpbXBvcnRhbnQ7XG59XG5cbi5jdXN0b20taW9uLWNvbHtcbiAgICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cbi5mb290ZXItYmFjay1jb2xvcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cbi5mb290ZXItYnRuLWNvbG9ye1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGMzlFMjAgIWltcG9ydGFudDtcbn1cbjo6bmctZGVlcC5uZ3gtZGF0YXRhYmxlLmJvb3RzdHJhcCAuZGF0YXRhYmxlLWJvZHkgLmRhdGF0YWJsZS1ib2R5LXJvdy5hY3RpdmV7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0YzOUUyMCAhaW1wb3J0YW50O1xufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6NDgxcHgpIHtcbiAgICA6Om5nLWRlZXAubmd4LWRhdGF0YWJsZS5ib290c3RyYXAgLmRhdGF0YWJsZS1oZWFkZXIgLmRhdGF0YWJsZS1oZWFkZXItY2VsbHtcblxuICAgICAgICBwYWRkaW5nLWxlZnQ6MC43NXJlbSAhaW1wb3J0YW50OyBcbiAgICAgICAgcGFkZGluZy1yaWdodDogMC4yNXJlbSAhaW1wb3J0YW50O1xuICAgICAgICBwYWRkaW5nLXRvcDogMDAuMjVyZW0gIWltcG9ydGFudDtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDAwLjI1cmVtICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBcbiAgICAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6MTI4MXB4KSB7ICBcbiAgICBcbiAgICA6Om5nLWRlZXAubmd4LWRhdGF0YWJsZS5ib290c3RyYXAgLmRhdGF0YWJsZS1oZWFkZXIgLmRhdGF0YWJsZS1oZWFkZXItY2VsbHtcblxuICAgIHBhZGRpbmc6IDAuNzVyZW0gIWltcG9ydGFudDtcbiAgIC8vIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xuICAgIFxuIFxuIH19XG5cbjo6bmctZGVlcC5uZ3gtZGF0YXRhYmxlLmJvb3RzdHJhcCAuZGF0YXRhYmxlLWhlYWRlcntcbiAgZm9udC13ZWlnaHQ6IGJvbGQgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0YzOUUyMCAhaW1wb3J0YW50O1xufVxuXG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDgwMHB4KSB7XG4gICAgLmRlc2t0b3AtaGlkZGVuIHtcbiAgICAgIGRpc3BsYXk6IGluaXRpYWw7XG4gICAgfVxuICAgIC5tb2JpbGUtaGlkZGVuIHtcbiAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuICB9XG4gIEBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDgwMHB4KSB7XG4gICAgLmRlc2t0b3AtaGlkZGVuIHtcbiAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuICAgIC5tb2JpbGUtaGlkZGVuIHtcbiAgICAgIGRpc3BsYXk6IGluaXRpYWw7XG4gICAgfVxuICB9XG5cblxuXG4iXX0= */";

/***/ }),

/***/ 40108:
/*!************************************************!*\
  !*** ./src/app/sort/sort.page.html?ngResource ***!
  \************************************************/
/***/ ((module) => {

module.exports = "<!-- <ion-header>\n  <ion-toolbar>\n   \n    <ion-buttons slot=\"start\">\n      <ion-back-button  defaultHref=\"order-approval\"></ion-back-button>\n    </ion-buttons>\n    <ion-title slot=\"start\">Sort</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"clearFilter()\"> Clear Filters</ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid class=\"box\">\n    <ion-row class=\"row-custom\">\n      <ion-col size=\"4\" class=\"back-color custom-ion-col\">\n        <ion-list *ngFor=\"let filterItem of filterList;let i = index\" class=\"back-color\">\n          <ion-item lines=\"none\" class=\"list-item\" (click)=\"menuItemClick(i)\" [ngClass]=\"{'custom-activated': (active == i),'ion-item-custom':(active != i)}\">\n            <ion-label color=\"primary\">\n              {{ filterItem.name }}\n            </ion-label>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n      <ion-col>\n        <div>\n          <ion-item>\n            <ion-label>Start Date</ion-label>\n            <ion-datetime displayFormat=\"DDD. MMM DD, YY\" placeholder=\"Select Date\" [(ngModel)]=\"startDate\"></ion-datetime>\n\n          </ion-item>\n          <ion-item>\n            <ion-label>End Date</ion-label>\n            <ion-datetime displayFormat=\"DDD. MMM DD, YY\"  placeholder=\"Select Date\" [(ngModel)]=\"endDate\"></ion-datetime>\n          </ion-item>\n        </div>\n      </ion-col>\n    </ion-row>    \n  </ion-grid>  \n</ion-content>\n<ion-footer class=\"footer-back-color\">\n  <ion-button  (click)=\"applyFilter()\"  color=\"primary\"  size=\"large\" expand=\"full\" fill=\"outline\" class=\"footer-btn-color\">\n    <ion-label style=\"color: white;\">\n      Apply\n    </ion-label>\n  </ion-button>\n</ion-footer> -->\n\n\n<ion-header #header>\n  <ion-toolbar>\n    <ion-title>Details</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button>\n        <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content scrollEvents=\"true\" appHideHeader [header]=\"header\">\n  <ngx-datatable  #datatable style=\"height: 600px; overflow-y:visible\" class=\"bootstrap resizeable\"  (cdkObserveContent)=\"adjustColumnMinWidth()\"\n                 [rows]=\"rows\"\n                 [columns]=\"column_data\"\n                 [columnMode]=\"ColumnMode.force\"\n                 [headerHeight]=\"50\"\n                 [footerHeight]=\"50\"\n                 [rowHeight]=\"40\"\n                 [selectionType]=\"SelectionType.single\"\n                 [scrollbarH]=\"true\"\n                 [scrollbarV]=\"true\"\n                 [reorderable]=\"true\"\n                 [loadingIndicator]=\"true\"\n                 [swapColumns]=\"true\">\n    \n  </ngx-datatable>\n \n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_sort_sort_module_ts.js.map