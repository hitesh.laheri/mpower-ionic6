"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_consolidation-order_consolidation-order_module_ts"],{

/***/ 80512:
/*!*******************************************************************!*\
  !*** ./src/app/consolidation-order/consolidation-order.module.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ConsolidationOrderPageModule": () => (/* binding */ ConsolidationOrderPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _consolidation_order_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./consolidation-order.page */ 66647);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var _common_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/pipes/pipes.module */ 22522);
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @swimlane/ngx-datatable */ 93143);










const routes = [
    {
        path: '',
        component: _consolidation_order_page__WEBPACK_IMPORTED_MODULE_0__.ConsolidationOrderPage
    }
];
let ConsolidationOrderPageModule = class ConsolidationOrderPageModule {
};
ConsolidationOrderPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            ionic_selectable__WEBPACK_IMPORTED_MODULE_7__.IonicSelectableModule, _common_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_1__.PipesModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule,
            _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_8__.NgxDatatableModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule.forChild(routes)
        ],
        declarations: [_consolidation_order_page__WEBPACK_IMPORTED_MODULE_0__.ConsolidationOrderPage]
    })
], ConsolidationOrderPageModule);



/***/ }),

/***/ 66647:
/*!*****************************************************************!*\
  !*** ./src/app/consolidation-order/consolidation-order.page.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ConsolidationOrderPage": () => (/* binding */ ConsolidationOrderPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _consolidation_order_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./consolidation-order.page.html?ngResource */ 12462);
/* harmony import */ var _consolidation_order_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./consolidation-order.page.scss?ngResource */ 63861);
/* harmony import */ var _order_approval_show_approval_details_modal_show_approval_details_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../order-approval/show-approval-details-modal/show-approval-details-modal.page */ 19592);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @swimlane/ngx-datatable */ 93143);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var src_provider_validator_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/provider/validator-helper */ 35096);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _consolidation_order_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./consolidation-order.service */ 30010);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/angular */ 93819);















let ConsolidationOrderPage = class ConsolidationOrderPage {
  constructor(consolidationOrderService, formBuilder, validator, commonFunction, loginauth, http, alertCtrl, router) {
    this.consolidationOrderService = consolidationOrderService;
    this.formBuilder = formBuilder;
    this.validator = validator;
    this.commonFunction = commonFunction;
    this.loginauth = loginauth;
    this.http = http;
    this.alertCtrl = alertCtrl;
    this.router = router;
    this.TAG = "Consolidation Order Page";
    this.ColumnMode = _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_8__.ColumnMode;
    this.SelectionType = _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_8__.SelectionType;
    this.selected = [];
    this.selectedOrder = false;
    this.page = new _order_approval_show_approval_details_modal_show_approval_details_modal_page__WEBPACK_IMPORTED_MODULE_3__.Page();
    this.currentPageOffset = 0;
    this.higgestPageOffset = -1;
    this.SortType = _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_8__.SortType;
    this.validation_messages = {
      'selectedBusinessPartner': [{
        type: 'required',
        message: '*Please Select Customer.'
      }],
      'custAddShippingCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Shipping Address.'
      }],
      'custAddBillingCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Billing Address.'
      }]
    };
    this.columns = [{
      name: 'Secondary Customer Name',
      prop: 'secondary_cust'
    }, {
      name: 'Order No',
      prop: 'documentno'
    }, {
      name: 'Date',
      prop: 'documentdate'
    }];
  }

  ngOnInit() {
    this.consolidationOrderForm = this.formBuilder.group({
      primaryCustomerCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
      selectedBPaddressShippingCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
      selectedBPAddressBillingCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required]
    });
    this.page.pageNumber = 0;
    this.page.size = 10;
  }

  ionViewWillEnter() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        // console.log(this.TAG,"Dummy REsponse",this.rows);
        //  this.getSecondaryCustomer({ offset: 0 });
        //  console.log(this.TAG,"ionViewWillEnter Fired");
        _this.commonFunction.loadingPresent();

        let tempResponse = yield _this.consolidationOrderService.getPrimaryCustomer().toPromise(); //  console.log(this.TAG,tempResponse);

        if (!!tempResponse.length) {
          if (tempResponse.length > _this.loginauth.minlistcount) {
            _this.primaryBusinessPartnerList = [];
          } else {
            _this.primaryBusinessPartnerList = tempResponse;
            _this.dontClearConsolidate = true;

            if (_this.primaryBusinessPartnerList.length == 1) {
              _this.consolidationOrderForm.controls["primaryCustomerCtrl"].setValue(_this.primaryBusinessPartnerList[0]);

              _this.selectedPrimaryBusinessPartner = _this.primaryBusinessPartnerList[0];

              _this.bindCustomerBillingAddress();

              _this.higgestPageOffset = -1;
              _this.currentPageOffset = 0;

              _this.getSecondaryCustomer({
                offset: 0
              });
            }
          }
        }

        _this.commonFunction.loadingDismiss();
      } catch (error) {
        _this.commonFunction.loadingDismiss(); // console.log(this.TAG,error);

      }
    })();
  }

  bindCustomerBillingAddress(id) {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const addressResponse = yield _this2.consolidationOrderService.getPrimaryCustomerBillingAddress(_this2.selectedPrimaryBusinessPartner.id).toPromise();
        const response = addressResponse['response'];
        var jsondata = response.data;
        _this2.custBillingAddressList = jsondata.filter(e => e.invoiceToAddress == true);
        _this2.custShippingAddressList = jsondata.filter(e => e.shipToAddress == true);
        setTimeout(() => {
          if (_this2.custShippingAddressList.length == 1) {
            _this2.consolidationOrderForm.controls["selectedBPaddressShippingCtrl"].setValue(_this2.custShippingAddressList[0]);
          }

          if (_this2.custBillingAddressList.length == 1) {
            _this2.consolidationOrderForm.controls["selectedBPAddressBillingCtrl"].setValue(_this2.custBillingAddressList[0]);
          }
        }, 100);
      } catch (error) {// console.log(this.TAG,error);
      }
    })();
  }

  refreshPage() {
    try {
      this.consolidationOrderForm.reset();
      this.rows = [];
      this.higgestPageOffset = -1;
      this.currentPageOffset = 0;
      this.page.totalElements = 0;
    } catch (error) {// console.log(this.TAG,error);
    }
  }

  onCustomerChange(event) {
    try {
      //  console.log(this.TAG,"onCustomerChange");
      this.higgestPageOffset = -1;
      this.currentPageOffset = 0;
      this.custShippingAddressList = null;
      this.custBillingAddressList = null;
      this.consolidationOrderForm.controls["selectedBPaddressShippingCtrl"].setValue('');
      this.consolidationOrderForm.controls["selectedBPAddressBillingCtrl"].setValue('');
      this.selectedPrimaryBusinessPartner = event.value;
      this.bindCustomerBillingAddress(event.value.id);
      this.rows = [];
      this.getSecondaryCustomer({
        offset: 0
      }); // this.setPage({ offset: 0 });

      event.component._searchText = "";
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  onCustomerClose(event) {
    try {
      event.component.searchText = "";
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  onCustomerSearchChange(event) {
    try {
      var custsearchtext = event.text;

      if (custsearchtext.length >= 3) {
        this.bindPrimaryCustomerFromApi(custsearchtext);
      } else {
        if (!!this.dontClearConsolidate && this.dontClearConsolidate == true) {} else {
          this.primaryBusinessPartnerList = [];
        }
      }
    } catch (error) {// console.log(this.TAG,error);
    }
  }

  bindPrimaryCustomerFromApi(strsearch) {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        if (strsearch != "") {
          const cust_response = yield _this3.consolidationOrderService.getPrimaryCustomer(strsearch).toPromise(); // console.log(this.TAG,cust_response);

          _this3.primaryBusinessPartnerList = cust_response;
        } else {
          if (!!_this3.dontClearConsolidate && _this3.dontClearConsolidate == true) {} else {
            _this3.primaryBusinessPartnerList = [];
          }
        }
      } catch (error) {// console.log(this.TAG,error);
      }
    })();
  }

  toggleSecondaryBusinessPartner(event, item) {
    try {} catch (error) {//  console.log(this.TAG,error);
    }
  }

  presentAlert(Header, SubHeader, Message) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this4.alertCtrl.create({
        header: Header,
        subHeader: SubHeader,
        message: Message,
        buttons: [{
          text: "OK",
          handler: ok => {
            _this4.higgestPageOffset = -1;
            _this4.currentPageOffset = 0;
            _this4.page.totalElements = 0;

            _this4.consolidationOrderForm.reset();

            _this4.rows = [];

            _this4.router.navigateByUrl('/home');
          }
        }]
      });
      alert.dismiss(() => console.log('The alert has been closed.'));
      yield alert.present();
    })();
  }

  onConsolidateOrder(data) {
    var _this5 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        if (!!_this5.selected && _this5.selected.length > 0) {
          _this5.commonFunction.loadingPresent();

          let Shipping = _this5.consolidationOrderForm.get('selectedBPaddressShippingCtrl').value.id;

          let Billing = _this5.consolidationOrderForm.get('selectedBPAddressBillingCtrl').value.id;

          let consolidateOrderResponse = yield _this5.consolidationOrderService.consolidateOrderService(_this5.selected, _this5.selectedPrimaryBusinessPartner, Shipping, Billing).toPromise(); // console.log(this.TAG,"on Close Order Status",consolidateOrderResponse);

          if (!!consolidateOrderResponse) {
            _this5.presentAlert("Consolidation Order", "Status", consolidateOrderResponse.msg);
          }

          _this5.commonFunction.loadingDismiss();
        } else {
          _this5.commonFunction.presentAlert("Consolidation Order", "Validation", "Please Select Order");
        }
      } catch (error) {
        _this5.commonFunction.loadingDismiss();

        _this5.commonFunction.presentAlert("Consolidation Order", "Error", error.error); // console.log(this.TAG,error);

      }
    })();
  }

  onCloseOrder(formData) {
    var _this6 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        if (!!_this6.selected && _this6.selected.length > 0) {
          _this6.commonFunction.loadingPresent();

          let Shipping = _this6.consolidationOrderForm.get('selectedBPaddressShippingCtrl').value.id;

          let Billing = _this6.consolidationOrderForm.get('selectedBPAddressBillingCtrl').value.id;

          let consolidateOrderResponse = yield _this6.consolidationOrderService.consolidateOrderCloseService(_this6.selected, _this6.selectedPrimaryBusinessPartner, Shipping, Billing).toPromise();
          ; //  console.log(this.TAG,"on Close Order Status",consolidateOrderResponse);

          if (!!consolidateOrderResponse) {
            _this6.presentAlert("Consolidation Order", "Status", consolidateOrderResponse.msg);
          }

          _this6.commonFunction.loadingDismiss();
        } else {
          _this6.commonFunction.presentAlert("Consolidation Order", "Validation", "Please Select Order");
        }
      } catch (error) {
        _this6.commonFunction.loadingDismiss();

        _this6.commonFunction.presentAlert("Consolidation Order", "Error", error.error); // console.log(this.TAG,error);

      }
    })();
  }

  getSecondaryCustomer(pageInfo) {
    var _this7 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        // console.log(this.TAG,"getSecondaryCustomer CAlled");
        _this7.commonFunction.loadingPresent();

        _this7.page.pageNumber = pageInfo.offset;
        let offset = 0;

        if (_this7.higgestPageOffset < pageInfo.offset) {
          if (_this7.page.totalElements == 0) {
            offset = 0;
            _this7.currentPageOffset = 0;
          } else {
            _this7.currentPageOffset = _this7.page.totalElements + 20;
            offset = _this7.currentPageOffset;
          }

          _this7.secondaryBusinessPartnerList = yield _this7.consolidationOrderService.getSecondaryCustomerFromApi(_this7.selectedPrimaryBusinessPartner.id, offset).toPromise(); // console.log(this.TAG,"Secondary Customer",this.secondaryBusinessPartnerList);

          if (!!_this7.rows) {
            _this7.higgestPageOffset = pageInfo.offset;
            _this7.rows = _this7.rows.concat(_this7.secondaryBusinessPartnerList.filter(x => _this7.rows.every(y => y !== x)));
          } else {
            _this7.higgestPageOffset = pageInfo.offset;
            _this7.rows = _this7.secondaryBusinessPartnerList; // this.getSecondaryCustomer({ offset: 1 });
          }

          _this7.page.totalElements = _this7.rows.length;
          _this7.page.totalPages = _this7.page.totalElements / _this7.page.size; // this.loadingIndicator  = false;
        }

        _this7.commonFunction.loadingDismiss(); // this.page.pageNumber = 1;

      } catch (error) {
        _this7.commonFunction.loadingDismiss(); //  console.log(this.TAG,error);

      }
    })();
  }

  onSelect({
    selected
  }) {
    //  console.log('Select Event', selected, this.selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);

    if (!!this.selected && this.selected.length > 0) {
      this.selectedOrder = true;
    } else {
      this.selectedOrder = false;
    }
  }

  onActivate(event) {//  console.log('Activate Event', event);
  }

};

ConsolidationOrderPage.ctorParameters = () => [{
  type: _consolidation_order_service__WEBPACK_IMPORTED_MODULE_7__.ConsolidationOrderService
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_9__.FormBuilder
}, {
  type: src_provider_validator_helper__WEBPACK_IMPORTED_MODULE_5__.Validator
}, {
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__.Commonfun
}, {
  type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_6__.LoginauthService
}, {
  type: _angular_common_http__WEBPACK_IMPORTED_MODULE_10__.HttpClient
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__.AlertController
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_12__.Router
}];

ConsolidationOrderPage = (0,tslib__WEBPACK_IMPORTED_MODULE_13__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_14__.Component)({
  selector: 'app-consolidation-order',
  template: _consolidation_order_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_14__.ViewEncapsulation.None,
  styles: [_consolidation_order_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ConsolidationOrderPage);


/***/ }),

/***/ 30010:
/*!********************************************************************!*\
  !*** ./src/app/consolidation-order/consolidation-order.service.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ConsolidationOrderService": () => (/* binding */ ConsolidationOrderService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ 64139);
/* harmony import */ var src_assets_model_CorporateEmployee__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/assets/model/CorporateEmployee */ 52274);
/* harmony import */ var src_assets_model_PagedData__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/assets/model/PagedData */ 84162);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 86942);
/* harmony import */ var _common_Constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../common/Constants */ 68209);










// import data from '../../assets/data/company.json';


// const companyData = data as any[];
const companyData = __webpack_require__(/*! ../../assets/data/company.json */ 21028);
let ConsolidationOrderService = class ConsolidationOrderService {
    constructor(genericHttpClientService, loginService, commonFunction, platform, http) {
        this.genericHttpClientService = genericHttpClientService;
        this.loginService = loginService;
        this.commonFunction = commonFunction;
        this.platform = platform;
        this.http = http;
        this.TAG = "Consolidation Order Service";
    }
    getPrimaryCustomer(strsearch) {
        try {
            if (!!strsearch) {
                strsearch = strsearch.replace(/ /g, "%20");
            }
            else {
                strsearch = "";
            }
            let getCustomerURL = this.loginService.commonurl + 'ws/in.mbs.webservice.WMobileUserWiseCustomer?'
                + this.loginService.parameter
                + '&user_id=' + this.loginService.userid
                + '&strsearch=' + strsearch
                + '&activity_id=' + this.loginService.selectedactivity.id
                + '&ordertypeionic=' + ""
                + '&isprimary=' + "Y";
            //  console.log(this.TAG,"getPrimaryCustomer",getCustomerURL);
            return this.genericHttpClientService.get(getCustomerURL);
        }
        catch (error) {
            //  console.log(this.TAG,error);
        }
    }
    getPrimaryCustomerBillingAddress(businessPartner_id) {
        try {
            return this.genericHttpClientService.get(this.loginService.commonurl + 'org.openbravo.service.json.jsonrest/BusinessPartnerLocation?'
                + this.loginService.ReadOnlyparameter + '&'
                + '_selectedProperties=id,name,shipToAddress,invoiceToAddress&'
                + '_where=active=true%20and%20businessPartner=\'' + businessPartner_id + '\'');
        }
        catch (error) {
            throw error;
        }
    }
    getSecondaryCustomerFromApi(bp_id, offset) {
        try {
            //  console.log(this.TAG,offset);
            let getCustomerURL = this.loginService.commonurl + 'ws/in.mbs.webservice.SebiaSecondaryCustList?'
                + this.loginService.parameter
                + '&user_id=' + this.loginService.userid
                + '&bp_id=' + bp_id
                + '&offset=' + offset
                + '&activity_id=' + this.loginService.selectedactivity.id;
            let dummyURL = "https://p2test.pispl.in/openbravo/ws/in.mbs.webservice.SebiaSecondaryCustList?user=hardik.pandya&password=pass&user_id=FFF20200727061334922CD0B9DE67B63&bp_id=FFF2020030902464717516D985A526FB&offset=20&activity_id=FFF202012061211195489D3E4DD35FC1";
            console.log(this.TAG, "getSecondaryCustomerFromApi", getCustomerURL);
            return this.genericHttpClientService.get(getCustomerURL);
        }
        catch (error) {
            //  console.log(this.TAG,error);
        }
    }
    getDummy() {
        try {
            return this.http.get('assets/data/login.json');
        }
        catch (error) {
            //  console.log(this.TAG,error);
        }
    }
    /**
     * A method that mocks a paged server response
     * @param page The selected page
     * @returns {any} An observable containing the employee data
     */
    getResults(page) {
        return (0,rxjs__WEBPACK_IMPORTED_MODULE_6__.of)(companyData).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.map)(d => this.getPagedData(page)));
    }
    /**
     * Package companyData into a PagedData object based on the selected Page
     * @param page The page data used to get the selected data from companyData
     * @returns {PagedData<CorporateEmployee>} An array of the selected data and page
     */
    getPagedData(page) {
        const pagedData = new src_assets_model_PagedData__WEBPACK_IMPORTED_MODULE_1__.PagedData();
        page.totalElements = companyData.length;
        page.totalPages = page.totalElements / page.size;
        const start = page.pageNumber * page.size;
        const end = Math.min(start + page.size, page.totalElements);
        for (let i = start; i < end; i++) {
            const jsonObj = companyData[i];
            const employee = new src_assets_model_CorporateEmployee__WEBPACK_IMPORTED_MODULE_0__.CorporateEmployee(jsonObj.name, jsonObj.gender, jsonObj.company, jsonObj.age);
            pagedData.data.push(employee);
        }
        pagedData.page = page;
        return pagedData;
    }
    consolidateOrderService(orderList, selectedBusinessPartner, Shipping, Billing) {
        try {
            let login = this.loginService.user;
            let password = this.loginService.pass;
            const auth = btoa(login + ":" + password);
            const httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__.HttpHeaders({
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Basic ' + auth
                })
            };
            let mbso_copord_id = [];
            orderList.forEach(element => {
                mbso_copord_id.push(element.mbso_copord_id);
            });
            let dataClose = {
                "consolidated_order": "Y",
                "bp_id": selectedBusinessPartner.id,
                "shiploc_id": Shipping,
                "billloc_id": Billing,
                "user_id": this.loginService.userid,
                "mbso_copord_id": mbso_copord_id
            };
            //  console.log(this.TAG,"Consolidate Order Service Post FINAL",dataClose);
            let consolidate_order_url = _common_Constants__WEBPACK_IMPORTED_MODULE_5__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.SebiaConsolidatedOrder?';
            return this.genericHttpClientService.post(consolidate_order_url, dataClose, httpOptions);
        }
        catch (error) {
            //  console.log(this.TAG,error);
        }
    }
    consolidateOrderCloseService(orderList, selectedBusinessPartner, Shipping, Billing) {
        try {
            let login = this.loginService.user;
            let password = this.loginService.pass;
            const auth = btoa(login + ":" + password);
            const httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__.HttpHeaders({
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Basic ' + auth
                })
            };
            let mbso_copord_id = [];
            orderList.forEach(element => {
                mbso_copord_id.push(element.mbso_copord_id);
            });
            let dataClose = {
                "consolidated_order": "N",
                "bp_id": selectedBusinessPartner.id,
                "shiploc_id": Shipping,
                "billloc_id": Billing,
                "user_id": this.loginService.userid,
                "mbso_copord_id": mbso_copord_id
            };
            //  console.log(this.TAG,"Consolidate Order Close Service Post FINAL",dataClose);
            let consolidate_order_close_url = _common_Constants__WEBPACK_IMPORTED_MODULE_5__.Constants.DOMAIN_URL + '/openbravo' + '/ws/in.mbs.webservice.SebiaConsolidatedOrder?';
            return this.genericHttpClientService.post(consolidate_order_close_url, dataClose, httpOptions);
        }
        catch (error) {
            //  console.log(this.TAG,error);
        }
    }
};
ConsolidationOrderService.ctorParameters = () => [
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_3__.GenericHttpClientService },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_4__.LoginauthService },
    { type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_2__.Commonfun },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.Platform },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__.HttpClient }
];
ConsolidationOrderService = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.Injectable)({
        providedIn: 'root'
    })
], ConsolidationOrderService);



/***/ }),

/***/ 52274:
/*!***********************************************!*\
  !*** ./src/assets/model/CorporateEmployee.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CorporateEmployee": () => (/* binding */ CorporateEmployee)
/* harmony export */ });
class CorporateEmployee {
    constructor(name, gender, company, age) {
        this.name = name;
        this.gender = gender;
        this.company = company;
        this.age = age;
    }
}


/***/ }),

/***/ 84162:
/*!***************************************!*\
  !*** ./src/assets/model/PagedData.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PagedData": () => (/* binding */ PagedData)
/* harmony export */ });
/* harmony import */ var src_app_order_approval_show_approval_details_modal_show_approval_details_modal_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/order-approval/show-approval-details-modal/show-approval-details-modal.page */ 19592);

/**
 * An array of data with an associated page object used for paging
 */
class PagedData {
    constructor() {
        this.data = new Array();
        this.page = new src_app_order_approval_show_approval_details_modal_show_approval_details_modal_page__WEBPACK_IMPORTED_MODULE_0__.Page();
    }
}


/***/ }),

/***/ 63861:
/*!******************************************************************************!*\
  !*** ./src/app/consolidation-order/consolidation-order.page.scss?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

module.exports = "@media screen and (max-width: 800px) {\n  .ngx-datatable.fixed-header .datatable-header .datatable-header-inner .datatable-header-cell {\n    overflow: hidden !important;\n    text-overflow: ellipsis !important;\n    white-space: normal !important;\n    vertical-align: middle !important;\n  }\n}\n.ngx-datatable.fixed-header .datatable-header .datatable-header-inner .datatable-header-cell {\n  font-weight: bold !important;\n}\n.ngx-datatable.bootstrap .datatable-header {\n  font-weight: bold !important;\n  background-color: #D7CCC8 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnNvbGlkYXRpb24tb3JkZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQVdBO0VBQ0k7SUFDSSwyQkFBQTtJQUNBLGtDQUFBO0lBQ0EsOEJBQUE7SUFFQSxpQ0FBQTtFQVhOO0FBQ0Y7QUFpQkE7RUFDSSw0QkFBQTtBQWZKO0FBaUJBO0VBQ0ksNEJBQUE7RUFDQSxvQ0FBQTtBQWRKIiwiZmlsZSI6ImNvbnNvbGlkYXRpb24tb3JkZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4vLyA6Om5nLWRlZXAge1xuXG4vLyBAaW1wb3J0ICcuLi8uLi9hc3NldHMvc3R5bGVzL25neC1kYXRhdGFibGUvX2luZGV4Jztcbi8vIEBpbXBvcnQgJy4uLy4uL2Fzc2V0cy9zdHlsZXMvbmd4LWRhdGF0YWJsZS9fbWF0ZXJpYWwnO1xuLy8gQGltcG9ydCAnLi4vLi4vYXNzZXRzL3N0eWxlcy9uZ3gtZGF0YXRhYmxlL2Jvb3RzdHJhcCc7XG4vLyBAaW1wb3J0ICcuLi8uLi9hc3NldHMvc3R5bGVzL25neC1kYXRhdGFibGUvX2ljb25zLmNzcyc7XG5cblxuLy8gfVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA4MDBweCkge1xuICAgIC5uZ3gtZGF0YXRhYmxlLmZpeGVkLWhlYWRlciAuZGF0YXRhYmxlLWhlYWRlciAuZGF0YXRhYmxlLWhlYWRlci1pbm5lciAuZGF0YXRhYmxlLWhlYWRlci1jZWxsIHtcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbiAhaW1wb3J0YW50O1xuICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcyAhaW1wb3J0YW50O1xuICAgICAgICB3aGl0ZS1zcGFjZTogbm9ybWFsICFpbXBvcnRhbnQ7XG4gICAgICAgLy8gdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG4gICAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGUgIWltcG9ydGFudDtcbiAgICAgIH1cbiAgICAgIFxuICB9XG5cblxuXG4ubmd4LWRhdGF0YWJsZS5maXhlZC1oZWFkZXIgLmRhdGF0YWJsZS1oZWFkZXIgLmRhdGF0YWJsZS1oZWFkZXItaW5uZXIgLmRhdGF0YWJsZS1oZWFkZXItY2VsbHtcbiAgICBmb250LXdlaWdodDogYm9sZCAhaW1wb3J0YW50O1xufVxuLm5neC1kYXRhdGFibGUuYm9vdHN0cmFwIC5kYXRhdGFibGUtaGVhZGVye1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkICFpbXBvcnRhbnQ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0Q3Q0NDOCAhaW1wb3J0YW50O1xuICB9Il19 */";

/***/ }),

/***/ 12462:
/*!******************************************************************************!*\
  !*** ./src/app/consolidation-order/consolidation-order.page.html?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Consolidation Order\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <ion-buttons (click)=\"refreshPage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]=\"consolidationOrderForm\">\n    <ion-card>\n    <ion-row>\n      <ion-col>\n        <ion-item>\n          <ion-label position=\"stacked\">Customer<span style=\"color:red!important\">*</span></ion-label>\n          <ionic-selectable placeholder=\"Select Customer\" disable=\"true\" formControlName=\"primaryCustomerCtrl\"\n              [items]=\"primaryBusinessPartnerList\" \n              itemValueField=\"id\" \n              itemTextField=\"_identifier\" \n              [canSearch]=\"true\"\n              [shouldStoreItemValue]=\"false\"\n              (onChange)=\"onCustomerChange($event)\"\n              (onClose)=\"onCustomerClose($event)\"\n              (onSearch)=\"onCustomerSearchChange($event)\">\n            </ionic-selectable>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <!-- Shipping Information -->\n    <ion-row>\n      <ion-col>\n        <h5 ion-text class=\"text-primary\">\n          <ion-icon name=\"locate\"></ion-icon> Shipping Information:\n        </h5>\n      </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n            <ion-item >\n              <ion-label position=\"stacked\">Shipping Address<span style=\"color:red!important\">*</span></ion-label>\n              <ion-select  formControlName=\"selectedBPaddressShippingCtrl\" interface=\"popover\" multiple=\"false\" placeholder=\"Select Address\">\n                <ion-select-option *ngFor=\"let custShippingAddress of custShippingAddressList\" [value]=\"custShippingAddress\">{{custShippingAddress.name}}</ion-select-option>\n              </ion-select>\n              </ion-item>\n              <div padding-left>\n                <ng-container *ngFor=\"let validation of validation_messages.custAddShippingCtrlErrorMessage\">\n                  <div *ngIf=\"consolidationOrderForm.get('selectedBPaddressShippingCtrl').hasError(validation.type) && consolidationOrderForm.get('selectedBPaddressShippingCtrl').touched\">\n                    <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                  </div>\n                </ng-container>\n              </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n            <ion-item >\n              <ion-label position=\"stacked\">Billing Address<span style=\"color:red!important\">*</span></ion-label>\n              <ion-select  formControlName=\"selectedBPAddressBillingCtrl\" interface=\"popover\" multiple=\"false\" placeholder=\"Select Address\">\n                <ion-select-option *ngFor=\"let badd of custBillingAddressList\" [value]=\"badd\">{{badd.name}}</ion-select-option>\n              </ion-select>\n              </ion-item>\n              <div padding-left>\n                <ng-container *ngFor=\"let validation of validation_messages.custAddBillingCtrlErrorMessage\">\n                  <div *ngIf=\"consolidationOrderForm.get('selectedBPAddressBillingCtrl').hasError(validation.type) && consolidationOrderForm.get('selectedBPAddressBillingCtrl').touched\">\n                    <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                  </div>\n                </ng-container>\n              </div>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n      <ion-row>\n        <ion-col>\n          <ion-card>\n            <!-- bootstrap resizeable -->\n            <!-- material -->\n            <ngx-datatable \n            class=\"bootstrap resizeable\"\n           \n            \n            [rows]=\"rows\" \n            [rowHeight]=\"'auto'\"  \n            [limit]=\"page.size\"\n            [columnMode]=\"ColumnMode.force\" \n            [sortType]=\"SortType.multi\" \n            [headerHeight]=\"50\" \n            [footerHeight]=\"50\"\n            [selectionType]=\"SelectionType.checkbox\"\n            [selected]=\"selected\"\n            (select)=\"onSelect($event)\"\n           \n            [offset]=\"page.pageNumber\"\n            [count]=\"page.totalElements\"\n            (page)=\"getSecondaryCustomer($event)\">\n\n            <ngx-datatable-column\n            [width]=\"25\"\n            [sortable]=\"false\"\n            [canAutoResize]=\"false\"\n            [draggable]=\"false\"\n            [resizeable]=\"false\">\n          \n            <ng-template\n              ngx-datatable-cell-template\n              let-value=\"value\"\n              let-isSelected=\"isSelected\"\n              let-onCheckboxChangeFn=\"onCheckboxChangeFn\">\n              <input type=\"checkbox\" [checked]=\"isSelected\" (change)=\"onCheckboxChangeFn($event)\" />\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Secondary Customer Name\"  [flexGrow]=\"3\"  prop=\"secondary_cust\"></ngx-datatable-column>\n          <ngx-datatable-column name=\"Order No\" [flexGrow]=\"1\" prop=\"documentno\" ></ngx-datatable-column>\n          <ngx-datatable-column name=\"Date\" [flexGrow]=\"1\" prop=\"documentdate\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{row.documentdate | date:'dd-MMM-yyyy'}}\n            </ng-template>\n          </ngx-datatable-column>\n\n          </ngx-datatable>\n\n</ion-card>\n       \n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col style=\"padding-left: 16px;padding-right: 16px;\">\n          <ion-button size=\"default\"\n            class=\"submit-btn\" expand=\"block\" [disabled]= \"!consolidationOrderForm.valid || !selectedOrder\"  color=\"primary\" (click)=\"onConsolidateOrder(consolidationOrderForm)\">Consolidate Order\n          </ion-button>\n        </ion-col>\n        <ion-col style=\"padding-left: 16px;padding-right: 16px;\">\n          <ion-button size=\"default\"\n            class=\"submit-btn\" expand=\"block\" [disabled]= \"!consolidationOrderForm.valid || !selectedOrder\" color=\"primary\" (click)=\"onCloseOrder(consolidationOrderForm)\">Close\n          </ion-button>\n        </ion-col>\n      </ion-row>\n\n\n  </form>\n</ion-content>\n";

/***/ }),

/***/ 21028:
/*!**************************************!*\
  !*** ./src/assets/data/company.json ***!
  \**************************************/
/***/ ((module) => {

module.exports = JSON.parse('[{"name":"Ethel Price","gender":"female","company":"Johnson, Johnson and Partners, LLC CMP DDC","age":22},{"name":"Claudine Neal","gender":"female","company":"Sealoud","age":55},{"name":"Beryl Rice","gender":"female","company":"Velity","age":67},{"name":"Wilder Gonzales","gender":"male","company":"Geekko"},{"name":"Georgina Schultz","gender":"female","company":"Suretech"},{"name":"Carroll Buchanan","gender":"male","company":"Ecosys"},{"name":"Valarie Atkinson","gender":"female","company":"Hopeli"},{"name":"Schroeder Mathews","gender":"male","company":"Polarium"},{"name":"Lynda Mendoza","gender":"female","company":"Dogspa"},{"name":"Sarah Massey","gender":"female","company":"Bisba"},{"name":"Robles Boyle","gender":"male","company":"Comtract"},{"name":"Evans Hickman","gender":"male","company":"Parleynet"},{"name":"Dawson Barber","gender":"male","company":"Dymi"},{"name":"Bruce Strong","gender":"male","company":"Xyqag"},{"name":"Nellie Whitfield","gender":"female","company":"Exospace"},{"name":"Jackson Macias","gender":"male","company":"Aquamate"},{"name":"Pena Pena","gender":"male","company":"Quarx"},{"name":"Lelia Gates","gender":"female","company":"Proxsoft"},{"name":"Letitia Vasquez","gender":"female","company":"Slumberia"},{"name":"Trevino Moreno","gender":"male","company":"Conjurica"},{"name":"Barr Page","gender":"male","company":"Apex"},{"name":"Kirkland Merrill","gender":"male","company":"Utara"},{"name":"Blanche Conley","gender":"female","company":"Imkan"},{"name":"Atkins Dunlap","gender":"male","company":"Comveyor"},{"name":"Everett Foreman","gender":"male","company":"Maineland"},{"name":"Gould Randolph","gender":"male","company":"Intergeek"},{"name":"Kelli Leon","gender":"female","company":"Verbus"},{"name":"Freda Mason","gender":"female","company":"Accidency"},{"name":"Tucker Maxwell","gender":"male","company":"Lumbrex"},{"name":"Yvonne Parsons","gender":"female","company":"Zolar"},{"name":"Woods Key","gender":"male","company":"Bedder"},{"name":"Stephens Reilly","gender":"male","company":"Acusage"},{"name":"Mcfarland Sparks","gender":"male","company":"Comvey"},{"name":"Jocelyn Sawyer","gender":"female","company":"Fortean"},{"name":"Renee Barr","gender":"female","company":"Kiggle"},{"name":"Gaines Beck","gender":"male","company":"Sequitur"},{"name":"Luisa Farrell","gender":"female","company":"Cinesanct"},{"name":"Robyn Strickland","gender":"female","company":"Obones"},{"name":"Roseann Jarvis","gender":"female","company":"Aquazure"},{"name":"Johnston Park","gender":"male","company":"Netur"},{"name":"Wong Craft","gender":"male","company":"Opticall"},{"name":"Merritt Cole","gender":"male","company":"Techtrix"},{"name":"Dale Byrd","gender":"female","company":"Kneedles"},{"name":"Sara Delgado","gender":"female","company":"Netagy"},{"name":"Alisha Myers","gender":"female","company":"Intradisk"},{"name":"Felecia Smith","gender":"female","company":"Futurity"},{"name":"Neal Harvey","gender":"male","company":"Pyramax"},{"name":"Nola Miles","gender":"female","company":"Sonique"},{"name":"Herring Pierce","gender":"male","company":"Geeketron"},{"name":"Shelley Rodriquez","gender":"female","company":"Bostonic"},{"name":"Cora Chase","gender":"female","company":"Isonus"},{"name":"Mckay Santos","gender":"male","company":"Amtas"},{"name":"Hilda Crane","gender":"female","company":"Jumpstack"},{"name":"Jeanne Lindsay","gender":"female","company":"Genesynk"},{"name":"Frye Sharpe","gender":"male","company":"Eplode"},{"name":"Velma Fry","gender":"female","company":"Ronelon"},{"name":"Reyna Espinoza","gender":"female","company":"Prismatic"},{"name":"Spencer Sloan","gender":"male","company":"Comverges"},{"name":"Graham Marsh","gender":"male","company":"Medifax"},{"name":"Hale Boone","gender":"male","company":"Digial"},{"name":"Wiley Hubbard","gender":"male","company":"Zensus"},{"name":"Blackburn Drake","gender":"male","company":"Frenex"},{"name":"Franco Hunter","gender":"male","company":"Rockabye"},{"name":"Barnett Case","gender":"male","company":"Norali"},{"name":"Alexander Foley","gender":"male","company":"Geekosis"},{"name":"Lynette Stein","gender":"female","company":"Macronaut"},{"name":"Anthony Joyner","gender":"male","company":"Senmei"},{"name":"Garrett Brennan","gender":"male","company":"Bluegrain"},{"name":"Betsy Horton","gender":"female","company":"Zilla"},{"name":"Patton Small","gender":"male","company":"Genmex"},{"name":"Lakisha Huber","gender":"female","company":"Insource"},{"name":"Lindsay Avery","gender":"female","company":"Unq"},{"name":"Ayers Hood","gender":"male","company":"Accuprint"},{"name":"Torres Durham","gender":"male","company":"Uplinx"},{"name":"Vincent Hernandez","gender":"male","company":"Talendula"},{"name":"Baird Ryan","gender":"male","company":"Aquasseur"},{"name":"Georgia Mercer","gender":"female","company":"Skyplex"},{"name":"Francesca Elliott","gender":"female","company":"Nspire"},{"name":"Lyons Peters","gender":"male","company":"Quinex"},{"name":"Kristi Brewer","gender":"female","company":"Oronoko"},{"name":"Tonya Bray","gender":"female","company":"Insuron"},{"name":"Valenzuela Huff","gender":"male","company":"Applideck"},{"name":"Tiffany Anderson","gender":"female","company":"Zanymax"},{"name":"Jerri King","gender":"female","company":"Eventex"},{"name":"Rocha Meadows","gender":"male","company":"Goko"},{"name":"Marcy Green","gender":"female","company":"Pharmex"},{"name":"Kirk Cross","gender":"male","company":"Portico"},{"name":"Hattie Mullen","gender":"female","company":"Zilencio"},{"name":"Deann Bridges","gender":"female","company":"Equitox"},{"name":"Chaney Roach","gender":"male","company":"Qualitern"},{"name":"Consuelo Dickson","gender":"female","company":"Poshome"},{"name":"Billie Rowe","gender":"female","company":"Cemention"},{"name":"Bean Donovan","gender":"male","company":"Mantro"},{"name":"Lancaster Patel","gender":"male","company":"Krog"},{"name":"Rosa Dyer","gender":"female","company":"Netility"},{"name":"Christine Compton","gender":"female","company":"Bleeko"},{"name":"Milagros Finch","gender":"female","company":"Handshake"},{"name":"Ericka Alvarado","gender":"female","company":"Lyrichord"},{"name":"Sylvia Sosa","gender":"female","company":"Circum"},{"name":"Humphrey Curtis","gender":"male","company":"Corepan"}]');

/***/ })

}]);
//# sourceMappingURL=src_app_consolidation-order_consolidation-order_module_ts.js.map