"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_selectorsingle_selectorsingle_module_ts"],{

/***/ 94699:
/*!*****************************************************************!*\
  !*** ./src/app/selectorsingle/selectorsingle-routing.module.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SelectorsinglePageRoutingModule": () => (/* binding */ SelectorsinglePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _selectorsingle_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./selectorsingle.page */ 98982);




const routes = [
    {
        path: '',
        component: _selectorsingle_page__WEBPACK_IMPORTED_MODULE_0__.SelectorsinglePage
    }
];
let SelectorsinglePageRoutingModule = class SelectorsinglePageRoutingModule {
};
SelectorsinglePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SelectorsinglePageRoutingModule);



/***/ }),

/***/ 54978:
/*!*********************************************************!*\
  !*** ./src/app/selectorsingle/selectorsingle.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SelectorsinglePageModule": () => (/* binding */ SelectorsinglePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _selectorsingle_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./selectorsingle-routing.module */ 94699);
/* harmony import */ var _selectorsingle_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./selectorsingle.page */ 98982);








let SelectorsinglePageModule = class SelectorsinglePageModule {
};
SelectorsinglePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _selectorsingle_routing_module__WEBPACK_IMPORTED_MODULE_0__.SelectorsinglePageRoutingModule,
            ionic_selectable__WEBPACK_IMPORTED_MODULE_7__.IonicSelectableModule
        ],
        declarations: [_selectorsingle_page__WEBPACK_IMPORTED_MODULE_1__.SelectorsinglePage]
    })
], SelectorsinglePageModule);



/***/ }),

/***/ 98982:
/*!*******************************************************!*\
  !*** ./src/app/selectorsingle/selectorsingle.page.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SelectorsinglePage": () => (/* binding */ SelectorsinglePage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _selectorsingle_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./selectorsingle.page.html?ngResource */ 7574);
/* harmony import */ var _selectorsingle_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./selectorsingle.page.scss?ngResource */ 76655);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _selectorsingle_selectorsingleservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../selectorsingle/selectorsingleservice.service */ 67296);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);








let SelectorsinglePage = class SelectorsinglePage {
  constructor(singleselservc, route, loginservc, router) {
    this.singleselservc = singleselservc;
    this.route = route;
    this.loginservc = loginservc;
    this.router = router;

    this.getpagenofromlist = function (jolist) {
      let pagerow = 0;

      for (let item of jolist) {
        pagerow = pagerow + 1;
      }

      return pagerow;
    };
  }

  ngOnInit() {
    this.txtCaption = 'Select Value';
    this.route.params.subscribe(params => {
      this.rptid = params['rptid'];
      this.scontrolname = params['scontrolname'];
      this.controltype = params['controltype'];

      if (this.controltype === 'LST') {
        this.ismultiselect = true;
      } else {
        this.ismultiselect = false;
      }

      this.singleselservc.json['scontrolname'] = this.scontrolname;
      this.singleselservc.json['text'] = '';
      this.singleselservc.json['offset'] = 0;
      this.singleselservc.pageOffset = 0;
      this.singleselservc.getData(this.singleselservc.json).subscribe(data => {
        console.log('singleselect', data);
        this.data = data['data'];
        this.filterdata = data['data'];
        this.pagerow = this.getpagenofromlist(data['data']);
        this.totalrow = data['totalRows'];
        this.id1.open();
      });
    });
  }

  onSubmitSingleSelection() {
    if (this.ismultiselect) {
      let strmultivaluedata = '';

      for (let value of this.selecteddata) {
        strmultivaluedata = strmultivaluedata + '\'' + value.id + '\',';
      }

      strmultivaluedata = '(' + strmultivaluedata.substring(0, strmultivaluedata.length - 1) + ')';
      this.singleselservc.json[this.scontrolname] = strmultivaluedata; // console.log(strmultivaluedata);
    } else {
      this.singleselservc.json[this.scontrolname] = this.selecteddata.id;
    } // console.log(this.singleselservc.json);


    this.router.navigate(['/report', this.rptid]);
  }

  onSearchChange(event) {
    let text = event.text.trim().toLowerCase();
    this.singleselservc.json['offset'] = 0; // Reset items back to all of the items
    //this.filterdata = this.data;
    // if the value is an empty string don't filter the items

    if (text && text.trim() !== '') {
      if (text.length > 2) {
        this.singleselservc.json['text'] = text;
        this.singleselservc.getData(this.singleselservc.json).subscribe(data => {
          // this.data = data['data'];
          this.filterdata = data['data'];
          this.pagerow = this.getpagenofromlist(data['data']);
          this.totalrow = data['totalRows'];
        });
      }
    } else {
      this.singleselservc.json['text'] = text;
      this.singleselservc.getData(this.singleselservc.json).subscribe(data => {
        this.data = data['data'];
        this.filterdata = data['data'];
        this.pagerow = this.getpagenofromlist(data['data']);
        this.totalrow = data['totalRows'];
      });
    }
  }

  doInfinite(event) {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        //Set this initial 0 at top 
        let text = (event.text || '').trim().toLowerCase();

        if (_this.pagerow === 20 && _this.totalrow > 20) {
          _this.singleselservc.pageOffset = _this.singleselservc.pageOffset + 20;
          _this.singleselservc.json['offset'] = _this.singleselservc.pageOffset;

          if (text) {
            _this.singleselservc.json['text'] = text;
          }

          let tempData = yield (yield _this.singleselservc.getData(_this.singleselservc.json)).toPromise();

          if (!!tempData) {
            // This is your page varible where you bind data
            _this.data = _this.data.concat(tempData['data']);
            _this.pagerow = _this.getpagenofromlist(tempData['data']);
            _this.filterdata = _this.filterdata.concat(tempData['data']);
            _this.totalrow = tempData['totalRows'];
            event.component.endInfiniteScroll();
          } // console.log(this.filterjoblist.length+'  '+this.totalrow);

        }

        if (_this.filterdata.length === _this.totalrow) {
          event.component.disableInfiniteScroll();
          return;
        }
      } catch (error) {}
    })();
  }

};

SelectorsinglePage.ctorParameters = () => [{
  type: _selectorsingle_selectorsingleservice_service__WEBPACK_IMPORTED_MODULE_3__.SelectorsingleserviceService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.ActivatedRoute
}, {
  type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_4__.LoginauthService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router
}];

SelectorsinglePage.propDecorators = {
  id1: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.ViewChild,
    args: ["id1", {
      static: false
    }]
  }]
};
SelectorsinglePage = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
  selector: 'app-selectorsingle',
  template: _selectorsingle_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_selectorsingle_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], SelectorsinglePage);


/***/ }),

/***/ 76655:
/*!********************************************************************!*\
  !*** ./src/app/selectorsingle/selectorsingle.page.scss?ngResource ***!
  \********************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzZWxlY3RvcnNpbmdsZS5wYWdlLnNjc3MifQ== */";

/***/ }),

/***/ 7574:
/*!********************************************************************!*\
  !*** ./src/app/selectorsingle/selectorsingle.page.html?ngResource ***!
  \********************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Selection</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"report\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-item>\n    <ion-label  color=\"primary\" position=\"stacked\" >{{txtCaption}}</ion-label>\n    <ionic-selectable #id1 [(ngModel)]=\"selecteddata\" itemValueField=\"id\" itemTextField=\"name\" [items]=\"filterdata\"\n      [canSearch]=\"true\" [hasInfiniteScroll]=\"true\" (onSearch)=\"onSearchChange($event)\" (onClose)=\"onSubmitSingleSelection()\"\n      (onInfiniteScroll)=\"doInfinite($event)\" [isMultiple]=\"ismultiselect\">\n    </ionic-selectable>\n  </ion-item>\n  <ion-button size=\"small\" shape=\"round\" fill=\"outline\" (click)=\"onSubmitSingleSelection()\">\n    Submit\n  </ion-button>\n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_selectorsingle_selectorsingle_module_ts.js.map