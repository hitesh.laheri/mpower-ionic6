"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_cardinal_service-manager_service-manager-details_service-manager-details_module_ts"],{

/***/ 49990:
/*!****************************************************************************************************!*\
  !*** ./src/app/cardinal/service-manager/service-manager-details/service-manager-details.module.ts ***!
  \****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ServiceManagerDetailsPageModule": () => (/* binding */ ServiceManagerDetailsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _service_manager_details_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./service-manager-details.page */ 46121);
/* harmony import */ var src_app_material_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/material.module */ 63806);
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/checkbox */ 44792);









const routes = [
    {
        path: '',
        component: _service_manager_details_page__WEBPACK_IMPORTED_MODULE_0__.ServiceManagerDetailsPage
    }
];
let ServiceManagerDetailsPageModule = class ServiceManagerDetailsPageModule {
};
ServiceManagerDetailsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            src_app_material_module__WEBPACK_IMPORTED_MODULE_1__.MaterialModule,
            _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_7__.MatCheckboxModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterModule.forChild(routes)
        ],
        declarations: [_service_manager_details_page__WEBPACK_IMPORTED_MODULE_0__.ServiceManagerDetailsPage]
    })
], ServiceManagerDetailsPageModule);



/***/ }),

/***/ 46121:
/*!**************************************************************************************************!*\
  !*** ./src/app/cardinal/service-manager/service-manager-details/service-manager-details.page.ts ***!
  \**************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ServiceManagerDetailsPage": () => (/* binding */ ServiceManagerDetailsPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _service_manager_details_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./service-manager-details.page.html?ngResource */ 21397);
/* harmony import */ var _service_manager_details_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./service-manager-details.page.scss?ngResource */ 24776);
/* harmony import */ var _service_manager_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../service-manager.service */ 89635);
/* harmony import */ var _provider_validator_helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../../provider/validator-helper */ 35096);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var src_app_newcustomer_newcustomer_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/newcustomer/newcustomer.service */ 85189);
/* harmony import */ var _customer_service_customer_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../customer-service/customer-service.service */ 59813);
/* harmony import */ var src_assets_model_complain__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/assets/model/complain */ 6337);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! date-fns */ 86527);















let ServiceManagerDetailsPage = class ServiceManagerDetailsPage {
  constructor(route, router, formBuilder, validator, serviceManagerService, commonFunction, newcustomerservice, customerService, alertCtrl) {
    this.route = route;
    this.router = router;
    this.formBuilder = formBuilder;
    this.validator = validator;
    this.serviceManagerService = serviceManagerService;
    this.commonFunction = commonFunction;
    this.newcustomerservice = newcustomerservice;
    this.customerService = customerService;
    this.alertCtrl = alertCtrl;
    this.now = new Date();
    this.year = this.now.getFullYear();
    this.month = this.now.getMonth() + 1;
    this.day = this.now.getDate();
    this.dateinvoice = '';
    this.dateinstall = '';
    /**
     *
     */

    this.maxDate = this.year + "-" + this.month + "-" + this.day;
    /**
     *
     */

    this.TAG = "ServiceManagerDetailsPage";
    /**
     * If this variable is set to true then stepper will check the validation on form control.It will now allow to navigate next page or stepper.
     */

    this.isLinear = true;
    /**
     *
     */

    this.detailsStepValid = false;
    this.equimentStepValid = false;
    this.customerDetailStepValid = false;
    this.skuDetailsStepValid = false;
    /**
     *
     */

    this.validation_messages = {
      'complaintNoCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Enter Complaint Number'
      }],
      'complaintDateCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Complaint Date'
      }],
      'nameCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Enter The Name'
      }],
      'documentType': [{
        type: 'required',
        message: '*Please Select Document Type'
      }],
      'nameOfComplainer': [{
        type: 'required',
        message: '*Please Enter Name of complainer.'
      }],
      'designation_of_complainer_mss': [{
        type: 'required',
        message: '*Please Select Designation Of Complainer.'
      }],
      'mobileno': [{
        type: 'required',
        message: '*Please Enter Contact No.'
      }, {
        type: 'InvalidNumber',
        message: '*Please Enter Valid Contact No.'
      }],
      'email': [{
        type: 'required',
        message: '*Please Enter Email.'
      }, {
        type: 'InvalidEmail',
        message: '*Please Enter Valid Email.'
      }],
      'eventDateIDCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Event Date'
      }],
      'complaintDescription': [{
        type: 'required',
        message: '*Please Enter existingComplaint Description'
      }],
      'serialNo': [{
        type: 'required',
        message: '*Please Enter Serial no .'
      }],
      'customerAddress1': [{
        type: 'required',
        message: '*Please Enter Customer Address1'
      }],
      'customerAddress2': [{
        type: 'required',
        message: '*Please Enter Customer Customer Address 2'
      }],
      'customerAddress3': [{
        type: 'required',
        message: '*Please Enter Customer Customer Address 3'
      }],
      'pinCode': [{
        type: 'required',
        message: '*Please Enter Pin Code'
      }],
      'area': [{
        type: 'required',
        message: '*Please Select Your Area'
      }],
      'serviceVendorMessage': [{
        type: 'required',
        message: '*Please Select Service Vendor'
      }],
      'skuCodeCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Enter SKU Code'
      }],
      'SKUNameCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Enter SKU Name'
      }],
      'brandNameCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Enter Brand Name'
      }],
      'errorCodeCtrlErrorMessage': [{
        type: 'required',
        message: '*Please Select Error Code.'
      }]
    };
    this.isServiceVender = false;
    this.color = 'accent';
    this.isProductCompliantTab = false;
    this.isConsumablesTab = false;
    this.isEquimentTab = false;
    this.approveComplaintStatus = false;
    this.approveCheckBoxEditable = false;
    this.firstStepValid = false;
  }

  ngOnInit() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // console.log(this.TAG,"ngOnInit Fired");
      // console.log(this.TAG,"ionViewWillEnter Fired");
      _this.route.queryParams.subscribe(params => {
        if (params && params.special) {
          _this.existingComplaint = JSON.parse(params.special); // console.log(this.TAG,JSON.parse(params.special));
        }
      });

      _this.detailFormGroup = _this.formBuilder.group({
        complaintNoCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        complaintDateCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        documentTypeCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        nameOfComplainerCtrl: [_this.existingComplaint.nameofcomplainer, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        desigOfComplainerCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        mobilenoCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required, _this.validator.numberValid],
        emailIDCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required, _this.validator.emailValid],
        eventDateIDCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required, _this.validator.emailValid],
        complaintDescriptionCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required]
      });
      _this.productCompliantFormGroup = _this.formBuilder.group({
        lotNoCtrl: [,]
      });
      _this.consumablesFormGroup = _this.formBuilder.group({
        lotNoConsumablesCtrl: [,]
      });
      _this.equimentFormGroup = _this.formBuilder.group({
        serialNoCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        dealerNameCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        errorCode: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        newDealerNameCtrl: [,],
        installationDateCtrl: [,],
        salesRepresentativeCtrl: [,]
      });
      _this.skuDetailsFormGroup = _this.formBuilder.group({
        skuCodeCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        skuNameCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        brandNameCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required]
      });
      _this.customerDetailFormGroup = _this.formBuilder.group({
        customerAddress1Ctrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        customerAddress2Ctrl: [,],
        customerAddress3Ctrl: [,],
        pinCodeCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        areaCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        city: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        state: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        country: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        approveComplaintChkCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
        serviceVendorCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required]
      });
      _this.dealerList = yield (yield _this.serviceManagerService.getDealerList()).toPromise();
      _this.salesRepresentativeList = yield (yield _this.serviceManagerService.getSalesRepresentativeList(_this.existingComplaint.olddealerid)).toPromise(); // if(!!this.dealerList && Object.keys(this.dealerList).length == 0){
      //   this.salesRepresentativeList = await (await this.serviceManagerService.getSalesRepresentativeList("")).toPromise();
      // }

      _this.venderList = yield (yield _this.serviceManagerService.getVenderList()).toPromise();
    })();
  }

  formatDate(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_10__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_11__["default"])(value), 'dd-MM-yyyy');
  }

  formatDate1(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_10__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_11__["default"])(value), 'dd-MM-yyyy');
  }

  ionViewWillEnter() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this2.commonFunction.loadingPresent();

      _this2.skuDetailsFormGroup.get('skuCodeCtrl').disable();

      _this2.skuDetailsFormGroup.get('skuNameCtrl').disable();

      _this2.skuDetailsFormGroup.get('brandNameCtrl').disable();

      _this2.bindData();

      _this2.equimentFormGroup.get('installationDateCtrl').disable();

      if (!!_this2.existingComplaint.installationdate) {
        _this2.approveCheckBoxEditable = true;
      }

      let test = _this2.existingComplaint.area;
      _this2.areaList = test;
      _this2.areaType = test[0];
      _this2.docType = _this2.existingComplaint.doctype[0];
      _this2.designationOfComplainerList = yield (yield _this2.serviceManagerService.getDesignationOfComplainerList()).toPromise();

      for (let i = 0; i < _this2.designationOfComplainerList.length; i++) {
        if (_this2.designationOfComplainerList[i].name == _this2.existingComplaint.desofcomplnr[0].name) {
          _this2.designationOfComplainerType = _this2.designationOfComplainerList[i];
        }
      }

      _this2.errorCodeList = yield (yield _this2.customerService.getErrorCodeList()).toPromise();

      for (let i = 0; i < _this2.errorCodeList.length; i++) {
        if (_this2.errorCodeList[i].name == _this2.existingComplaint.errorcode[0].name) {
          _this2.errorCodeSelected = _this2.errorCodeList[i];
        }
      }

      _this2.customerDetailFormGroup.controls.pinCodeCtrl.patchValue(_this2.existingComplaint.pincode);

      _this2.customerDetailFormGroup.controls.city.patchValue(_this2.existingComplaint.city[0].name);

      _this2.customerDetailFormGroup.controls.state.patchValue(_this2.existingComplaint.country[0].name);

      _this2.customerDetailFormGroup.controls.country.patchValue(_this2.existingComplaint.state[0].name);

      _this2.customerDetailFormGroup.get('city').disable();

      _this2.customerDetailFormGroup.get('state').disable();

      _this2.customerDetailFormGroup.get('country').disable();

      _this2.contractTypeList = yield (yield _this2.customerService.getContractTypeList()).toPromise();

      for (let i = 0; i < _this2.contractTypeList.length; i++) {
        if (_this2.contractTypeList[i].code == _this2.existingComplaint.contracttype) {
          _this2.contracttypeSelected = _this2.contractTypeList[i].name;
        }
      }

      _this2.detailFormGroup.get('documentTypeCtrl').disable();

      if (_this2.existingComplaint.doctype[0].name == 'Product Compliant') {
        _this2.isProductCompliantTab = true;
        _this2.isConsumablesTab = false;
        _this2.isEquimentTab = false;

        _this2.productCompliantFormGroup.controls.lotNoCtrl.patchValue(_this2.existingComplaint.lotno);
      } else if (_this2.existingComplaint.doctype[0].name == 'Consumables') {
        _this2.isProductCompliantTab = false;
        _this2.isConsumablesTab = true;
        _this2.isEquimentTab = false;

        _this2.consumablesFormGroup.controls.lotNoCtrl.patchValue(_this2.existingComplaint.lotno);
      } else if (_this2.existingComplaint.doctype[0].name == 'Equipment') {
        _this2.isProductCompliantTab = false;
        _this2.isConsumablesTab = false;
        _this2.isEquimentTab = true;
      }

      if (_this2.existingComplaint.producttobereturn == "MSNR_N") {
        _this2.productToBeReturn = 'No';
      } else if (_this2.existingComplaint.producttobereturn == "MSNR_Y") {
        _this2.productToBeReturn = 'Yes';
      }

      _this2.commonFunction.loadingDismiss();
    })();
  }

  bindData() {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this3.detailFormGroup.controls.complaintNoCtrl.patchValue(_this3.existingComplaint.complaintno);

        _this3.detailFormGroup.get('complaintNoCtrl').disable();

        _this3.detailFormGroup.controls.complaintDateCtrl.patchValue(_this3.existingComplaint.complaintdate);

        _this3.detailFormGroup.get('complaintDateCtrl').disable(); //this.detailFormGroup.controls.nameOfComplainerCtrl.patchValue(this.existingComplaint.nameofcomplainer);


        _this3.detailFormGroup.get('documentTypeCtrl').disable();

        _this3.detailFormGroup.controls.mobilenoCtrl.patchValue(_this3.existingComplaint.contnumber);

        _this3.detailFormGroup.controls.emailIDCtrl.patchValue(_this3.existingComplaint.email);

        _this3.detailFormGroup.controls.eventDateIDCtrl.patchValue(_this3.existingComplaint.eventdate);

        _this3.detailFormGroup.get('eventDateIDCtrl').disable();

        _this3.detailFormGroup.controls.complaintDescriptionCtrl.patchValue(_this3.existingComplaint.description); // this.customerDetailFormGroup.controls.approveComplaintChkCtrl.patchValue(this.existingComplaint.approvecomplaint);


        if (!!_this3.existingComplaint.approvecomplaint) {
          _this3.isServiceVender = true;

          _this3.customerDetailFormGroup.get('approveComplaintChkCtrl').disable();

          _this3.isChecked = _this3.existingComplaint.approvecomplaint;
        }

        if (!!_this3.existingComplaint.newdealername) {
          _this3.dealerList = yield (yield _this3.serviceManagerService.getDealerList()).toPromise();

          for (let i = 0; i < _this3.dealerList.length; i++) {
            if (_this3.dealerList[i].id == _this3.existingComplaint.dealerid) {
              _this3.dealerName = _this3.dealerList[i]; // setTimeout( () => {
              //  this.newDealerNameSelected = this.dealerList[i];
              //  },300);
            }
          }
        }

        if (!!_this3.existingComplaint.salesrepid) {
          let dealer_id = "";

          if (!!_this3.existingComplaint.newdealername) {
            dealer_id = _this3.existingComplaint.dealerid;
          } else {
            dealer_id = _this3.existingComplaint.olddealerid;
          }

          _this3.salesRepresentativeList = yield (yield _this3.serviceManagerService.getSalesRepresentativeList(dealer_id)).toPromise();

          for (let i = 0; i < _this3.salesRepresentativeList.length; i++) {
            if (_this3.salesRepresentativeList[i].id == _this3.existingComplaint.salesrepid) {
              setTimeout(() => {
                _this3.salesRepresentativeSelected = _this3.salesRepresentativeList[i];
              }, 300);
            }
          }
        }
      } catch (error) {//  console.log(this.TAG,error);
      }
    })();
  }

  refreshPage() {
    try {} catch (error) {//  console.error(this.TAG,error);
    }
  }

  matDetailSettper(form) {
    try {
      this.detailsStepValid = true; //   console.log(this.TAG,"Form Error",form);
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  matSettperProductCompliant(event) {
    try {} catch (error) {//  console.error(this.TAG,error);
    }
  }

  matSettperConsumables(event) {
    try {} catch (error) {//   console.error(this.TAG,error);
    }
  }

  matSettperSKUDetails(event) {
    try {} catch (error) {//  console.error(this.TAG,error);
    }
  }

  matSettperEquiment(event) {
    try {
      this.firstStepValid = true;
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  approveComplaintCheckbox(value) {
    try {
      //  console.log(this.TAG,"Approve existingComplaint Checkbox",value);
      if (value) {
        this.isServiceVender = true;
        this.approveComplaintStatus = true;
        this.disFormControl("dis");
      } else {
        this.isServiceVender = false;
        this.approveComplaintStatus = false;
        this.disFormControl("ena");
      }
    } catch (error) {//  console.error(this.TAG,error);
    }
  }

  disFormControl(value) {
    try {
      if (value == "dis") {
        this.detailFormGroup.get('nameOfComplainerCtrl').disable();
        this.detailFormGroup.get('desigOfComplainerCtrl').disable();
        this.detailFormGroup.get('mobilenoCtrl').disable();
        this.detailFormGroup.get('emailIDCtrl').disable();
        this.detailFormGroup.get('complaintDescriptionCtrl').disable();
        this.productCompliantFormGroup.get('lotNoCtrl').disable();
        this.consumablesFormGroup.get('lotNoConsumablesCtrl').disable();
        this.equimentFormGroup.get('serialNoCtrl').disable();
        this.equimentFormGroup.get('dealerNameCtrl').disable();
        this.equimentFormGroup.get('newDealerNameCtrl').disable();
        this.equimentFormGroup.get('installationDateCtrl').disable();
        this.customerDetailFormGroup.get('customerAddress1Ctrl').disable();
        this.customerDetailFormGroup.get('customerAddress2Ctrl').disable();
        this.customerDetailFormGroup.get('customerAddress3Ctrl').disable();
        this.customerDetailFormGroup.get('pinCodeCtrl').disable();
        this.customerDetailFormGroup.get('areaCtrl').disable();
        this.customerDetailFormGroup.get('customerAddress1Ctrl').disable();
        this.customerDetailFormGroup.get('customerAddress1Ctrl').disable();
        this.customerDetailFormGroup.get('customerAddress1Ctrl').disable();
        this.customerDetailFormGroup.get('approveComplaintChkCtrl').disable();
      } else if (value == "ena") {
        this.detailFormGroup.get('nameOfComplainerCtrl').enable();
        this.detailFormGroup.get('desigOfComplainerCtrl').enable();
        this.detailFormGroup.get('mobilenoCtrl').enable();
        this.detailFormGroup.get('emailIDCtrl').enable();
        this.detailFormGroup.get('complaintDescriptionCtrl').enable();
        this.productCompliantFormGroup.get('lotNoCtrl').enable();
        this.consumablesFormGroup.get('lotNoConsumablesCtrl').enable();
        this.equimentFormGroup.get('serialNoCtrl').enable();
        this.equimentFormGroup.get('dealerNameCtrl').enable();
        this.equimentFormGroup.get('newDealerNameCtrl').enable();
        this.equimentFormGroup.get('installationDateCtrl').enable();
        this.customerDetailFormGroup.get('customerAddress1Ctrl').enable();
        this.customerDetailFormGroup.get('customerAddress2Ctrl').enable();
        this.customerDetailFormGroup.get('customerAddress3Ctrl').enable();
        this.customerDetailFormGroup.get('pinCodeCtrl').enable();
        this.customerDetailFormGroup.get('areaCtrl').enable();
        this.customerDetailFormGroup.get('customerAddress1Ctrl').enable();
        this.customerDetailFormGroup.get('customerAddress1Ctrl').enable();
      }
    } catch (error) {
      console.error(this.TAG, error);
    }
  }

  designationOfComplainerChange(data) {
    try {} catch (error) {
      console.error(this.TAG, error);
    }
  }

  docTypeSelectedChange(data) {
    try {} catch (error) {
      console.error(this.TAG, error);
    }
  }

  onAreaSelectedChange() {
    try {
      this.selectedArea = this.customerDetailFormGroup.controls['areaCtrl'].value; //  console.log(this.TAG,'Pravin Area Is',this.selectedArea);

      if (this.selectedArea != null) {
        this.city = this.selectedArea.cttv$_identifier;
      }
    } catch (error) {// console.log(this.TAG,error);
    }
  }

  onChangePinCode(id = '') {
    try {
      this.newcustomerservice.getPincode(this.customerDetailFormGroup.controls["pinCodeCtrl"].value).subscribe(data => {
        const response = data['response']; // console.log(this.TAG,"Response From Pin Code",response);

        this.pinCodeList = response.data;

        if (this.pinCodeList.length > 0) {
          this.inValidPinCode = '';
          this.newcustomerservice.getarea(this.pinCodeList[0].id).subscribe(data => {
            const response = data['response']; //  console.log(this.TAG,"AREA LIST",response.data);

            this.existingComplaint.city[0].id = response.data[0].cttv; //  console.log(this.TAG,this.existingComplaint.city[0].id);

            let addressArray = [];

            for (let j = 0; j < response.data.length; j++) {
              let addressObj = {
                "id": response.data[j].id,
                "name": response.data[j].area,
                "cttv$_identifier": response.data[j].cttv$_identifier
              };
              addressArray.push(addressObj);
            }

            this.areaList = addressArray;
            this.existingComplaint.area = this.areaList;
            this.state = this.pinCodeList[0].region$_identifier;
            this.existingComplaint.state[0].id = this.pinCodeList[0].region;
            this.country = this.pinCodeList[0].country$_identifier;
            this.district = this.pinCodeList[0].district$_identifier;

            if (id != '' || id == undefined) {
              this.selectedArea = this.areaList.find(item => item.id === id);
              setTimeout(() => {
                this.customerDetailFormGroup.controls["areaCtrl"].setValue(this.selectedArea);
              }, 1500);
            }
          });
        }
      });
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  newDealerNameChange(dealer) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        // console.log(this.TAG,"PRavin DEA selected",dealer);
        _this4.salesRepresentativeList = yield (yield _this4.serviceManagerService.getSalesRepresentativeList(dealer.id)).toPromise(); // console.log(this.TAG,"Sales Representative list",this.salesRepresentativeList);
      } catch (error) {
        console.error(_this4.TAG, error);
      }
    })();
  }

  salesRepresentativeChange(value) {
    try {} catch (error) {// console.log(this.TAG,error);
    }
  }

  presentAlert(Header, SubHeader, Message) {
    var _this5 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const alert = yield _this5.alertCtrl.create({
        header: Header,
        subHeader: SubHeader,
        message: Message,
        buttons: [{
          text: "OK",
          handler: ok => {
            _this5.router.navigateByUrl('/service-manager');
          }
        }]
      });
      alert.dismiss(() => console.log('The alert has been closed.'));
      yield alert.present();
    })();
  }

  errorCodeSelectedSelectedChange(data) {
    try {} catch (error) {// console.log(this.TAG,error);
    }
  }

  onCheckInstallationBase() {
    var _this6 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this6.commonFunction.loadingPresent();

        _this6.serialNoCheckResponse = "";
        let serialNo = _this6.equimentFormGroup.get('serialNoCtrl').value ? _this6.equimentFormGroup.get('serialNoCtrl').value : '';
        _this6.existingComplaint.serialno = serialNo;
        _this6.serialNoCheckResponse = yield (yield _this6.customerService.checkSerialNumber(serialNo, "2020-12-31")).toPromise();
        console.log(_this6.TAG, "Installation Base Data", _this6.serialNoCheckResponse);

        if (Object.keys(_this6.serialNoCheckResponse).length != 0) {
          if (!!_this6.serialNoCheckResponse.contracttype) {
            for (let i = 0; i < _this6.contractTypeList.length; i++) {
              if (_this6.contractTypeList[i].code == _this6.serialNoCheckResponse.contracttype) {
                _this6.contracttypeSelected = _this6.contractTypeList[i].name;
                _this6.existingComplaint.contracttype = _this6.contractTypeList[i].code;
              }
            }
          }

          if (!!_this6.serialNoCheckResponse.invoiceno) {
            // this.equimentFormGroup.controls["invoiceNoCtrl"].setValue(this.serialNoCheckResponse.invoiceno);
            // this.equimentFormGroup.get('invoiceNoCtrl').disable();
            _this6.existingComplaint.invoiceno = _this6.serialNoCheckResponse.invoiceno;
          } else {
            _this6.existingComplaint.invoiceno = "";
          }

          if (!!_this6.serialNoCheckResponse.invoicedate) {
            _this6.serialNoCheckResponse.invoicedate = _this6.serialNoCheckResponse.invoicedate.split(' ')[0]; // this.equimentFormGroup.controls["invoiceDateCtrl"].setValue(this.serialNoCheckResponse.invoicedate);

            _this6.existingComplaint.invoicedate = _this6.serialNoCheckResponse.invoicedate.split(' ')[0];
          } else {
            _this6.existingComplaint.invoicedate = "";
          }

          if (!!_this6.serialNoCheckResponse.dealername) {
            // this.equimentFormGroup.controls["dealerNameCtrl"].setValue(this.serialNoCheckResponse.dealername);
            //  this.equimentFormGroup.get('dealerNameCtrl').disable();
            _this6.existingComplaint.dealername = _this6.serialNoCheckResponse.dealername;

            if (!!_this6.serialNoCheckResponse.dealerid) {
              _this6.salesRepresentativeList = yield (yield _this6.serviceManagerService.getSalesRepresentativeList(_this6.serialNoCheckResponse.dealerid)).toPromise();
            }
          } else {
            _this6.equimentFormGroup.controls["dealerNameCtrl"].setValue("");
          }

          if (!!_this6.serialNoCheckResponse.installationdate) {
            _this6.serialNoCheckResponse.installationdate = _this6.serialNoCheckResponse.installationdate.split(' ')[0]; //  this.equimentFormGroup.controls["installationDateCtrl"].setValue(this.serialNoCheckResponse.installationdate);

            _this6.equimentFormGroup.get('installationDateCtrl').disable();

            _this6.existingComplaint.installationdate = _this6.serialNoCheckResponse.installationdate;
            _this6.approveCheckBoxEditable = true;
          } else {
            //this.equimentFormGroup.controls["installationDateCtrl"].setValue("");
            _this6.existingComplaint.installationdate = "";
          }

          if (!!_this6.serialNoCheckResponse.sku) {
            _this6.skuDetailsFormGroup.controls["skuCodeCtrl"].setValue(_this6.serialNoCheckResponse.sku);

            _this6.skuDetailsFormGroup.get('skuCodeCtrl').disable();

            _this6.existingComplaint.skucode = _this6.serialNoCheckResponse.sku;
          } else {
            _this6.skuDetailsFormGroup.controls["skuCodeCtrl"].setValue("");
          }

          if (!!_this6.serialNoCheckResponse.sku) {
            _this6.skuDetailsFormGroup.controls["skuNameCtrl"].setValue(_this6.serialNoCheckResponse.skuname);

            _this6.skuDetailsFormGroup.get('skuNameCtrl').disable();

            _this6.existingComplaint.skuname = _this6.serialNoCheckResponse.skuname;
          } else {
            _this6.skuDetailsFormGroup.controls["skuNameCtrl"].setValue("");
          }

          if (!!_this6.serialNoCheckResponse.brand) {
            _this6.skuDetailsFormGroup.controls["brandNameCtrl"].setValue(_this6.serialNoCheckResponse.brand);

            _this6.skuDetailsFormGroup.get('brandNameCtrl').disable();

            _this6.existingComplaint.brandname = _this6.serialNoCheckResponse.brand;
          } else {
            _this6.skuDetailsFormGroup.controls["brandNameCtrl"].setValue("");
          }
        } else {
          _this6.equimentFormGroup.reset();

          _this6.equimentFormGroup.enable();

          _this6.equimentFormGroup.controls["serialNo"].setValue(serialNo);

          _this6.skuDetailsFormGroup.reset();

          _this6.skuDetailsFormGroup.enable();
        }

        _this6.commonFunction.loadingDismiss();
      } catch (error) {
        console.log(_this6.TAG, error);
      }
    })();
  }

  submit(event) {
    var _this7 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        let date = _this7.equimentFormGroup.get('installationDateCtrl').value ? _this7.equimentFormGroup.get('installationDateCtrl').value : '';

        if (date === '' || date === null || date === undefined) {
          //this.presentAlert("Compliant Report Details","","Installation Date Can Not Be Blank");
          _this7.commonFunction.presentAlert("Compliant Report Details", "", "Installation Date Can Not Be Blank");
        } else {
          _this7.commonFunction.loadingPresent();

          let approveComplain = new src_assets_model_complain__WEBPACK_IMPORTED_MODULE_8__.Complain();
          approveComplain.complaint_no = _this7.existingComplaint.complaintno;
          approveComplain.complaintid = _this7.existingComplaint.complaintid;
          approveComplain.complaint_date = _this7.existingComplaint.complaintdate;
          approveComplain.doctype = _this7.existingComplaint.doctype[0].id;
          approveComplain.nameofcomplainer = _this7.detailFormGroup.get('nameOfComplainerCtrl').value ? _this7.detailFormGroup.get('nameOfComplainerCtrl').value : '';
          approveComplain.desofcomplnr = _this7.detailFormGroup.get('desigOfComplainerCtrl').value.id ? _this7.detailFormGroup.get('desigOfComplainerCtrl').value.id : '';
          approveComplain.contnumber = _this7.detailFormGroup.get('mobilenoCtrl').value ? _this7.detailFormGroup.get('mobilenoCtrl').value.toString() : '';
          approveComplain.email = _this7.detailFormGroup.get('emailIDCtrl').value ? _this7.detailFormGroup.get('emailIDCtrl').value : '';
          approveComplain.eventdate = _this7.existingComplaint.eventdate;
          approveComplain.description = _this7.detailFormGroup.get('complaintDescriptionCtrl').value ? _this7.detailFormGroup.get('complaintDescriptionCtrl').value : '';
          approveComplain.lotno = _this7.consumablesFormGroup.get('lotNoConsumablesCtrl').value ? _this7.consumablesFormGroup.get('lotNoConsumablesCtrl').value : '';
          approveComplain.serialno = _this7.existingComplaint.serialno ? _this7.existingComplaint.serialno : "";
          approveComplain.srnoequipment = _this7.existingComplaint.srnoequipment;
          approveComplain.contracttype = _this7.existingComplaint.contracttype;
          approveComplain.invoiceno = _this7.existingComplaint.invoiceno;
          approveComplain.invoicedate = _this7.existingComplaint.invoicedate;
          approveComplain.errorcode = _this7.errorCodeSelected.id ? _this7.errorCodeSelected.id : '';
          approveComplain.dealername = _this7.equimentFormGroup.get('dealerNameCtrl').value ? _this7.equimentFormGroup.get('dealerNameCtrl').value : '';
          approveComplain.newdealername = _this7.equimentFormGroup.get('newDealerNameCtrl').value ? _this7.equimentFormGroup.get('newDealerNameCtrl').value.id : '';
          approveComplain.salesrepresentative = _this7.equimentFormGroup.get('salesRepresentativeCtrl').value ? _this7.equimentFormGroup.get('salesRepresentativeCtrl').value.name : '';
          approveComplain.installationdate = _this7.equimentFormGroup.get('installationDateCtrl').value ? _this7.equimentFormGroup.get('installationDateCtrl').value : '';
          approveComplain.skucode = _this7.existingComplaint.skucode;
          approveComplain.skuname = _this7.existingComplaint.skuname;
          approveComplain.brandname = _this7.existingComplaint.brandname;
          approveComplain.producttobereturn = _this7.existingComplaint.producttobereturn;
          approveComplain.custname = _this7.existingComplaint.custname;
          approveComplain.add1 = _this7.customerDetailFormGroup.get('customerAddress1Ctrl').value ? _this7.customerDetailFormGroup.get('customerAddress1Ctrl').value : '';
          approveComplain.add2 = _this7.customerDetailFormGroup.get('customerAddress2Ctrl').value ? _this7.customerDetailFormGroup.get('customerAddress2Ctrl').value : '';
          approveComplain.add3 = _this7.customerDetailFormGroup.get('customerAddress3Ctrl').value ? _this7.customerDetailFormGroup.get('customerAddress3Ctrl').value : '';
          approveComplain.pincode = _this7.customerDetailFormGroup.get('pinCodeCtrl').value ? _this7.customerDetailFormGroup.get('pinCodeCtrl').value : '';
          let area = _this7.customerDetailFormGroup.get('areaCtrl').value ? _this7.customerDetailFormGroup.get('areaCtrl').value : '';
          approveComplain.area = area.id;
          approveComplain.city = _this7.existingComplaint.city[0].id;
          approveComplain.state = _this7.existingComplaint.state[0].id;
          approveComplain.country = _this7.existingComplaint.country[0].id;
          approveComplain.appcomplaint = "true";
          approveComplain.servicevendor = _this7.customerDetailFormGroup.get('serviceVendorCtrl').value ? _this7.customerDetailFormGroup.get('serviceVendorCtrl').value.id : '';
          approveComplain.assigntoservvendor = "true"; //  console.log(this.TAG,"Final Service Manager Form",approveComplain);

          let saveComplainResponse = yield _this7.customerService.saveComplain(approveComplain).toPromise(); //  console.log(this.TAG,"Response From Save Complain",saveComplainResponse);

          if (saveComplainResponse) {
            _this7.presentAlert("Compliant Report Details", "", saveComplainResponse.msg);
          } else {
            _this7.presentAlert("Compliant Report Details", "", "Something went wrong please try again" + saveComplainResponse.msg);
          }

          _this7.commonFunction.loadingDismiss();
        }
      } catch (error) {
        _this7.commonFunction.loadingDismiss(); // console.error(this.TAG,error);

      }
    })();
  }

};

ServiceManagerDetailsPage.ctorParameters = () => [{
  type: _angular_router__WEBPACK_IMPORTED_MODULE_12__.ActivatedRoute
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_12__.Router
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_9__.FormBuilder
}, {
  type: _provider_validator_helper__WEBPACK_IMPORTED_MODULE_4__.Validator
}, {
  type: _service_manager_service__WEBPACK_IMPORTED_MODULE_3__.ServiceManagerService
}, {
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_5__.Commonfun
}, {
  type: src_app_newcustomer_newcustomer_service__WEBPACK_IMPORTED_MODULE_6__.NewcustomerService
}, {
  type: _customer_service_customer_service_service__WEBPACK_IMPORTED_MODULE_7__.CustomerServiceService
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.AlertController
}];

ServiceManagerDetailsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_14__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_15__.Component)({
  selector: 'app-service-manager-details',
  template: _service_manager_details_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_service_manager_details_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ServiceManagerDetailsPage);


/***/ }),

/***/ 24776:
/*!***************************************************************************************************************!*\
  !*** ./src/app/cardinal/service-manager/service-manager-details/service-manager-details.page.scss?ngResource ***!
  \***************************************************************************************************************/
/***/ ((module) => {

module.exports = ".mat-stepper-vertical {\n  margin-top: 8px;\n}\n\n.mat-form-field {\n  margin-top: 16px;\n}\n\n.example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%;\n}\n\n.example-full-width {\n  width: 100%;\n}\n\n.cus {\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  width: 100%;\n}\n\nion-datetime {\n  width: 100%;\n}\n\nion-popover {\n  --width: 320px;\n}\n\nion-popover.dateTimePopover {\n  --offset-y: -350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2UtbWFuYWdlci1kZXRhaWxzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQUE7QUFDSjs7QUFFRTtFQUNFLGdCQUFBO0FBQ0o7O0FBRUU7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQUNKOztBQUVFO0VBQ0UsV0FBQTtBQUNKOztBQUNFO0VBRUUsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBQ0c7RUFDQyxXQUFBO0FBRUo7O0FBQUU7RUFDRSxjQUFBO0FBR0o7O0FBREU7RUFDRSw2QkFBQTtBQUlKIiwiZmlsZSI6InNlcnZpY2UtbWFuYWdlci1kZXRhaWxzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXQtc3RlcHBlci12ZXJ0aWNhbCB7XG4gICAgbWFyZ2luLXRvcDogOHB4O1xuICB9XG4gIFxuICAubWF0LWZvcm0tZmllbGQge1xuICAgIG1hcmdpbi10b3A6IDE2cHg7XG4gIH1cblxuICAuZXhhbXBsZS1mb3JtIHtcbiAgICBtaW4td2lkdGg6IDE1MHB4O1xuICAgIG1heC13aWR0aDogNTAwcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbiAgXG4gIC5leGFtcGxlLWZ1bGwtd2lkdGgge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIC5jdXN7XG4gIFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlciA7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgfVxuICAgaW9uLWRhdGV0aW1lIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuICBpb24tcG9wb3ZlciB7XG4gICAgLS13aWR0aDogMzIwcHg7XG4gIH1cbiAgaW9uLXBvcG92ZXIuZGF0ZVRpbWVQb3BvdmVyIHtcbiAgICAtLW9mZnNldC15OiAtMzUwcHggIWltcG9ydGFudDtcbiAgICB9Il19 */";

/***/ }),

/***/ 21397:
/*!***************************************************************************************************************!*\
  !*** ./src/app/cardinal/service-manager/service-manager-details/service-manager-details.page.html?ngResource ***!
  \***************************************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      Compliant Report Details\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <mat-vertical-stepper [linear]=\"isLinear\" #stepper>\n    <mat-step [stepControl]=\"detailFormGroup\">\n      <form [formGroup]=\"detailFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Details</ng-template>\n        <!-- complaint_no -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Complaint No</mat-label>\n          <input matInput formControlName=\"complaintNoCtrl\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.complaintNoCtrlErrorMessage\">\n              <div *ngIf=\"detailFormGroup.get('complaintNoCtrl').hasError(validation.type) && detailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- complaint_date -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Complaint Date</mat-label>\n          <input matInput formControlName=\"complaintDateCtrl\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.complaintDateCtrlErrorMessage\">\n              <div *ngIf=\"detailFormGroup.get('complaintNoCtrl').hasError(validation.type) && detailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Document Type -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Document Type</mat-label>\n          <mat-select formControlName=\"documentTypeCtrl\" [(ngModel)]=\"docType\"\n            (selectionChange)=\"docTypeSelectedChange(docTypeSelected)\">\n            <mat-option *ngFor=\"let doc of existingComplaint.doctype\" [value]=\"doc\">\n              {{doc.name}}\n            </mat-option>\n          </mat-select>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.documentType\">\n              <div *ngIf=\"detailFormGroup.get('documentTypeCtrl').hasError(validation.type) && detailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Name of Complainer -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Name of Complainer</mat-label>\n          <input matInput formControlName=\"nameOfComplainerCtrl\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.nameCtrlErrorMessage\">\n              <div *ngIf=\"detailFormGroup.get('nameOfComplainerCtrl').hasError(validation.type) && detailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Designation of Complainer -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Designation of Complainer</mat-label>\n          <mat-select [(ngModel)]=\"designationOfComplainerType\" formControlName=\"desigOfComplainerCtrl\"\n            (selectionChange)=\"designationOfComplainerChange(designationOfComplainerSelected)\">\n            <mat-option *ngFor=\"let designationObject of designationOfComplainerList\" [value]=\"designationObject\">\n              {{designationObject.name}}\n            </mat-option>\n          </mat-select>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.designation_of_complainer_mss\">\n              <div *ngIf=\"detailFormGroup.get('desigOfComplainerCtrl').hasError(validation.type) && detailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Contact No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Contact No</mat-label>\n          <input matInput formControlName=\"mobilenoCtrl\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.mobileno\">\n              <div *ngIf=\"detailFormGroup.get('mobilenoCtrl').hasError(validation.type) && detailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Email ID -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Email ID</mat-label>\n          <input matInput formControlName=\"emailIDCtrl\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.email\">\n              <div *ngIf=\"detailFormGroup.get('emailIDCtrl').hasError(validation.type) && detailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Event Date -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Event Date</mat-label>\n          <input matInput [disabled]='true' formControlName=\"eventDateIDCtrl\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.eventDateIDCtrlErrorMessage\">\n              <div *ngIf=\"detailFormGroup.get('emailIDCtrl').hasError(validation.type) && detailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Complaint Description -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Complaint Description</mat-label>\n          <textarea matInput formControlName=\"complaintDescriptionCtrl\"></textarea>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.complaintDescription\">\n              <div\n                *ngIf=\"detailFormGroup.get('complaintDescriptionCtrl').hasError(validation.type) && detailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperNext (click)=\"matDetailSettper(detailFormGroup)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Product Compliant Tab-->\n    <mat-step [stepControl]=\"productCompliantFormGroup\" *ngIf=\"isProductCompliantTab\">\n      <form [formGroup]=\"productCompliantFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Product Compliant</ng-template>\n        <!-- Lot No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Lot No.</mat-label>\n          <input matInput formControlName=\"lotNoCtrl\">\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperProductCompliant($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Consumables Tab  -->\n    <mat-step [stepControl]=\"consumablesFormGroup\" *ngIf=\"isConsumablesTab\">\n      <form [formGroup]=\"consumablesFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Consumables</ng-template>\n        <!-- Lot No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Lot No.</mat-label>\n          <input matInput formControlName=\"lotNoConsumablesCtrl\">\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperConsumables($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Equiment Tab-->\n    <mat-step [stepControl]=\"equimentFormGroup\" *ngIf=\"isEquimentTab\">\n      <form [formGroup]=\"equimentFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Equiment</ng-template>\n        <!-- Serial No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Serial No.</mat-label>\n          <input matInput formControlName=\"serialNoCtrl\" [(ngModel)]=\"existingComplaint.serialno\" (change)='onCheckInstallationBase()'>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.serialNo\">\n              <div *ngIf=\"equimentFormGroup.get('serialNoCtrl').hasError(validation.type) && equimentStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Sr. No. of Equipment -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label>Sr. No. of Equipment</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"existingComplaint.srnoequipment\"\n            [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field> -->\n        <!-- Contract Type -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Contract Type</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"contracttypeSelected\" [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Invoice No -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Invoice No.</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"existingComplaint.invoiceno\"\n            [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Invoice Date -->\n        <div class=\"example-full-width bottom-border\">\n          <ion-label style=\"color: darkgray;\">Invoice Date</ion-label>\n          <section class=\"cus\">\n           <!-- <ion-datetime  [disabled]='true' style=\"--padding-start:0px\" displayFormat=\"DD-MM-YYYY\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"existingComplaint.invoicedate\"  [max]=\"maxDate | date:'yyyy-MM-dd'\">\n          </ion-datetime>\n          <ion-icon name=\"calendar\"></ion-icon>  -->\n          <ion-item>\n            <ion-input placeholder=\"Select Date\" [value]=\"dateinvoice\"></ion-input>\n            <ion-button fill=\"clear\" id=\"open-date-input-1\">\n              <ion-icon icon=\"calendar\"></ion-icon>\n            </ion-button>\n            <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\n              <ng-template>\n                <ion-datetime\n                  #popoverDatetime2\n                  [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"existingComplaint.invoicedate\"  \n                  [max]=\"maxDate | date:'yyyy-MM-dd'\"\n                  presentation=\"date\"\n                  showDefaultButtons=\"true\"\n                  (ionChange)=\"dateinvoice = formatDate(popoverDatetime2.value)\"\n                ></ion-datetime>\n              </ng-template>\n            </ion-popover>\n          </ion-item>\n        </section>\n        </div>\n        \n      \n        <!-- Error Code -->\n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Error Code</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"existingComplaint.errorcode[0].name\"\n            [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field> -->\n\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Error Code</mat-label>\n          <mat-select [(ngModel)]=\"errorCodeSelected\" formControlName=\"errorCode\"\n            (selectionChange)=\"errorCodeSelectedSelectedChange(errorCodeSelected)\">\n            <mat-option *ngFor=\"let errorCode of errorCodeList\" [value]=\"errorCode\">\n              {{errorCode.name}}\n            </mat-option>\n          </mat-select>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.errorCodeCtrlErrorMessage\">\n              <div *ngIf=\"equimentFormGroup.get('errorCode').hasError(validation.type) && firstStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n\n        <!-- Dealer Name -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Dealer Name</mat-label>\n          <input matInput formControlName=\"dealerNameCtrl\" [(ngModel)]=\"existingComplaint.dealername\">\n        </mat-form-field>\n        <!-- New Dealer Name -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>New Dealer Name</mat-label>\n          <mat-select [(ngModel)]=\"dealerName\" formControlName=\"newDealerNameCtrl\"\n            (selectionChange)=\"newDealerNameChange(dealerName)\" >\n            <mat-option *ngFor=\"let dealer of dealerList\" [value]=\"dealer\">\n              {{dealer.name}}\n            </mat-option>\n          </mat-select>\n        </mat-form-field>\n        <!-- Sales Representative -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Sales Representative</mat-label>\n          <mat-select formControlName=\"salesRepresentativeCtrl\" [(ngModel)]=\"salesRepresentativeSelected\"\n            (selectionChange)=\"salesRepresentativeChange(salesRepresentative)\">\n            <mat-option *ngFor=\"let salesRepresentative of salesRepresentativeList\" [value]=\"salesRepresentative\">\n              {{salesRepresentative.name}}\n            </mat-option>\n          </mat-select>\n        </mat-form-field>\n        <!-- Installation Date -->\n        <div class=\"example-full-width bottom-border\">\n          <ion-label style=\"color: darkgray;\">Installation Date</ion-label>\n          <section class=\"cus\">\n           <!-- <ion-datetime  style=\"--padding-start:0px\" displayFormat=\"DD-MM-YYYY\" [(ngModel)]=\"existingComplaint.installationdate\" formControlName=\"installationDateCtrl\" [max]=\"maxDate | date:'yyyy-MM-dd'\">\n          </ion-datetime>\n          <ion-icon name=\"calendar\"></ion-icon>  -->\n          <ion-item>\n            <ion-input placeholder=\"Select Date\" [value]=\"dateinstall\"></ion-input>\n            <ion-button fill=\"clear\" id=\"open-date-input-2\">\n              <ion-icon icon=\"calendar\"></ion-icon>\n            </ion-button>\n            <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-2\" show-backdrop=\"false\">\n              <ng-template>\n                <ion-datetime\n                  #popoverDatetime2\n                  [(ngModel)]=\"existingComplaint.installationdate\" formControlName=\"installationDateCtrl\" \n                  [max]=\"maxDate | date:'yyyy-MM-dd'\"\n                  presentation=\"date\"\n                  showDefaultButtons=\"true\"\n                  (ionChange)=\"dateinvoice = formatDate1(popoverDatetime2.value)\"\n                ></ion-datetime>\n              </ng-template>\n            </ion-popover>\n          </ion-item>\n        </section>\n        </div>     \n        <!-- <mat-form-field class=\"example-full-width\">\n          <mat-label>Installation Date</mat-label>\n          <input matInput formControlName=\"installationDateCtrl\" [(ngModel)]=\"existingComplaint.installationdate\">\n        </mat-form-field> -->\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperEquiment($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- SKU Details Tab-->\n    <mat-step [stepControl]=\"skuDetailsFormGroup\">\n      <form [formGroup]=\"skuDetailsFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>SKU Details</ng-template>\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">SKU Code</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"existingComplaint.skucode\" formControlName=\"skuCodeCtrl\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.skuCodeCtrlErrorMessage\">\n              <div *ngIf=\"skuDetailsFormGroup.get('skuCodeCtrl').hasError(validation.type) && skuDetailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- SKU Name / Description -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">SKU Name / Description</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"existingComplaint.skuname\" formControlName=\"skuNameCtrl\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.SKUNameCtrlErrorMessage\">\n              <div *ngIf=\"skuDetailsFormGroup.get('skuNameCtrl').hasError(validation.type) && skuDetailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Brand Name -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Brand Name</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"existingComplaint.brandname\" formControlName=\"brandNameCtrl\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.brandNameCtrlErrorMessage\">\n              <div *ngIf=\"skuDetailsFormGroup.get('brandNameCtrl').hasError(validation.type) && skuDetailsStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Product to be returned -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Product to be returned</mat-label>\n          <mat-select [disabled]='true' [(ngModel)]=\"productToBeReturn\" [ngModelOptions]=\"{standalone: true}\">\n            <mat-option value=\"Yes\">Yes</mat-option>\n            <mat-option value=\"No\">No</mat-option>\n          </mat-select>\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button matStepperNext (click)=\"matSettperSKUDetails($event)\">Next</button>\n        </div>\n      </form>\n    </mat-step>\n    <!-- Customer Detail Tab -->\n    <mat-step [stepControl]=\"customerDetailFormGroup\">\n      <form [formGroup]=\"customerDetailFormGroup\" class=\"example-form\">\n        <ng-template matStepLabel>Customer Detail</ng-template>\n        <!-- Customer Name -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Customer Name</mat-label>\n          <input matInput [disabled]='true' [(ngModel)]=\"existingComplaint.custname\"\n            [ngModelOptions]=\"{standalone: true}\">\n        </mat-form-field>\n        <!-- Customer Address 1 -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Customer Address 1</mat-label>\n          <input matInput formControlName=\"customerAddress1Ctrl\" [(ngModel)]=\"existingComplaint.add1\">\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.customerAddress1\">\n              <div\n                *ngIf=\"customerDetailFormGroup.get('customerAddress1Ctrl').hasError(validation.type) && customerDetailStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Customer Address 2 -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Customer Address 2</mat-label>\n          <input matInput formControlName=\"customerAddress2Ctrl\" [(ngModel)]=\"existingComplaint.add2\">\n        </mat-form-field>\n        <!-- Customer Address 3 -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label>Customer Address 3</mat-label>\n          <input matInput formControlName=\"customerAddress3Ctrl\" [(ngModel)]=\"existingComplaint.add3\">\n        </mat-form-field>\n        <!-- Pin Code -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Pin Code</mat-label>\n          <input type=\"number\" matInput formControlName=\"pinCodeCtrl\" (change)='onChangePinCode()'>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.pinCode\">\n              <div\n                *ngIf=\"customerDetailFormGroup.get('pinCodeCtrl').hasError(validation.type) && customerDetailStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <!-- Area -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Area</mat-label>\n          <!-- <input matInput formControlName=\"areaCtrl\"> -->\n          <mat-select [(ngModel)]=\"areaType\" formControlName=\"areaCtrl\" (selectionChange)=\"onAreaSelectedChange()\">\n            <mat-option *ngFor=\"let areaObject of areaList\" [value]=\"areaObject\">\n              {{areaObject.name}}\n            </mat-option>\n          </mat-select>\n        </mat-form-field>\n        <!-- City -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">City</mat-label>\n          <input matInput formControlName=\"city\" [(ngModel)]=\"city\">\n        </mat-form-field>\n        <!-- State -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">State</mat-label>\n          <input matInput formControlName=\"state\" [(ngModel)]=\"state\">\n        </mat-form-field>\n        <!-- Country -->\n        <mat-form-field class=\"example-full-width\">\n          <mat-label class=\"asterisk_input\">Country</mat-label>\n          <input matInput formControlName=\"country\" [(ngModel)]=\"country\">\n        </mat-form-field>\n        <!-- Approve existingComplaint -->\n        <section class=\"example-full-width\">\n          <mat-checkbox formControlName=\"approveComplaintChkCtrl\" [color]=\"color\" [(ngModel)]=\"isChecked\"\n            (change)=\"approveComplaintCheckbox($event.checked)\" [disabled]=\"!approveCheckBoxEditable\" class=\"example-margin\">Approve Complaint</mat-checkbox>\n        </section>\n        <!-- Service Vendor -->\n        <mat-form-field class=\"example-full-width\" *ngIf=\"isServiceVender\">\n          <mat-label>Service Vendor</mat-label>\n          <mat-select formControlName=\"serviceVendorCtrl\" >\n            <mat-option *ngFor=\"let vender of venderList\" [value]=\"vender\">\n              {{vender.name}}\n            </mat-option>\n          </mat-select>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.serviceVendorMessage\">\n              <div\n                *ngIf=\"customerDetailFormGroup.get('serviceVendorCtrl').hasError(validation.type) && customerDetailStepValid\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </mat-form-field>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n        </div>\n        <div>\n          <ion-row>\n            <ion-col no-padding>\n              <ion-button size=\"default\" [disabled]=\"!customerDetailFormGroup.valid\" expand=\"block\" color=\"primary\"\n                (click)=\"submit($event)\">Assign Vendor\n              </ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n      </form>\n    </mat-step>\n  </mat-vertical-stepper>\n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_cardinal_service-manager_service-manager-details_service-manager-details_module_ts.js.map