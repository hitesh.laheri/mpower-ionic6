"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_quotation_add-edit-service-product_add-edit-service-product_module_ts"],{

/***/ 90488:
/*!***************************************************************************************!*\
  !*** ./src/app/quotation/add-edit-service-product/add-edit-service-product.module.ts ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddEditServiceProductPageModule": () => (/* binding */ AddEditServiceProductPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _add_edit_service_product_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-edit-service-product.page */ 15314);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var src_app_common_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/common/pipes/pipes.module */ 22522);









const routes = [
    {
        path: '',
        component: _add_edit_service_product_page__WEBPACK_IMPORTED_MODULE_0__.AddEditServiceProductPage
    }
];
let AddEditServiceProductPageModule = class AddEditServiceProductPageModule {
};
AddEditServiceProductPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, ionic_selectable__WEBPACK_IMPORTED_MODULE_7__.IonicSelectableModule, src_app_common_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_1__.PipesModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterModule.forChild(routes)
        ],
        declarations: [_add_edit_service_product_page__WEBPACK_IMPORTED_MODULE_0__.AddEditServiceProductPage]
    })
], AddEditServiceProductPageModule);



/***/ }),

/***/ 15314:
/*!*************************************************************************************!*\
  !*** ./src/app/quotation/add-edit-service-product/add-edit-service-product.page.ts ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddEditServiceProductPage": () => (/* binding */ AddEditServiceProductPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _add_edit_service_product_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./add-edit-service-product.page.html?ngResource */ 21432);
/* harmony import */ var _add_edit_service_product_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./add-edit-service-product.page.scss?ngResource */ 5991);
/* harmony import */ var _customer_quotation_customer_quotation_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../customer-quotation/customer-quotation.service */ 23375);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/login/loginauth.service */ 44010);










let AddEditServiceProductPage = class AddEditServiceProductPage {
  constructor(formBuilder, router, route, commonFunction, customerQuotationService, loginService) {
    this.formBuilder = formBuilder;
    this.router = router;
    this.route = route;
    this.commonFunction = commonFunction;
    this.customerQuotationService = customerQuotationService;
    this.loginService = loginService;
    this.TAG = "Add Edit Service Product Page";
    this.isCardCollapse = 1;
    this.prodSearchTextCount = 0;
    this.tempCartProduct = [];
    this.btnSaveDisabled = false;
  }

  ngOnInit() {
    this.addEditServiceProductForm = this.formBuilder.group({
      serialNoCtrl: [, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required],
      quantityCtrl: [{
        value: '',
        disabled: true
      }, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required]
    });
  }

  ionViewWillEnter() {
    try {
      //  console.log(this.TAG,"ionViewWillEnter"); 
      this.route.queryParams.subscribe(data => {
        if (!!data["quotation_data"] && data["quotation_data"].length > 0) {
          this.selectedQuotationData = JSON.parse(data["quotation_data"]); //  console.log(this.TAG,JSON.parse(data["quotation_data"]));

          this.selectedSerialNumberProductListQuotation = JSON.parse(data["selectedSerialNumberProductList"]);
          this.tempCartProduct = this.selectedSerialNumberProductListQuotation;

          if (!!this.selectedQuotationData) {
            if (this.selectedQuotationData.service_product_id.id) {
              this.customerQuotationService.getSerialNoProductList(this.selectedQuotationData.service_product_id.id, "").subscribe(tempSerialNumberLess10 => {
                //  console.log(this.TAG,tempSerialNumberLess10);
                if (!!tempSerialNumberLess10) {
                  // this.serialNoList = tempSerialNumberLess10;
                  if (tempSerialNumberLess10.length > this.loginService.minlistcount) {
                    this.tempSerialNumberLess10 = [];
                  } else {
                    setTimeout(() => {
                      this.tempSerialNumberLess10 = tempSerialNumberLess10;

                      if (this.tempSerialNumberLess10.length == 1) {
                        this.addEditServiceProductForm.controls["serialNoCtrl"].setValue(this.tempSerialNumberLess10[0]);
                        this.selectedSerialNo = this.serialNoList[0];
                      }
                    }, 100);
                  }
                }
              });
            }
          }

          if (!!this.tempCartProduct) {
            this.btnSaveDisabled = true;
          } //  console.log(this.TAG,this.selectedSerialNumberProductListQuotation);

        }
      });
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  onSerialNoClose(event) {
    try {
      event.component.searchText = "";
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  onSerialNoChange(event) {
    try {
      if (event.value != undefined) {
        //  console.log(this.TAG,event.value);
        // this.tempCartProduct = event.value;
        //this.tempCartProduct.push(event.value);
        this.selectedSerialNo = event.value;
        this.addEditServiceProductForm.controls['quantityCtrl'].setValue(1); //  console.log(this.TAG,"Array Of Product",this.tempCartProduct);

        event.component._searchText = "";
      }
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  onSerialNoSearch(event) {
    try {
      this.prodSearchTextCount++;
      var custsearchtext = event.text;

      if (custsearchtext.length >= 3) {
        this.prodSearchTextCount = 0;
        this.bindSerialNoProduct(custsearchtext);
      } else {
        this.serialNoList = this.tempSerialNumberLess10;
      }
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  bindSerialNoProduct(strsearch) {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        if (strsearch != "") {
          _this.customerQuotationService.getSerialNoProductList(_this.selectedQuotationData.service_product_id.id, strsearch).subscribe(response => {
            //  console.log(this.TAG,response);
            _this.serialNoList = response;
          });
        } else {
          _this.serialNoList = _this.tempSerialNumberLess10;
        }
      } catch (error) {//  console.log(this.TAG,error);
      }
    })();
  }

  removeProduct(deletedSerial) {
    try {
      const result = this.tempCartProduct.filter(item => item.m_attributesetinstance_id != deletedSerial.m_attributesetinstance_id);
      this.tempCartProduct = result;
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  onSaveCart() {
    try {
      if (this.tempCartProduct != null && this.tempCartProduct != undefined) {
        let navigationExtras = {
          queryParams: {
            cartList: JSON.stringify(this.tempCartProduct),
            selectedQuotationData: JSON.stringify(this.selectedQuotationData)
          }
        };
        this.router.navigate(['customer-quotation'], navigationExtras);
      } else {
        this.commonFunction.presentAlert("Add Product", "Message!", "Your Cart Is Empty");
      }
    } catch (error) {//  console.log(this.TAG,error);
    }
  }

  addToCart() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        //this.tempCartProduct.push(this.selectedSerialNo);
        if (!!_this2.tempCartProduct) {
          const result = _this2.tempCartProduct.filter(item => item.m_attributesetinstance_id == _this2.selectedSerialNo.m_attributesetinstance_id);

          if (result.length > 0) {
            _this2.commonFunction.presentAlert("Add Product", "Validation", "This product is already added with 1 quantity.");

            _this2.addEditServiceProductForm.controls['serialNoCtrl'].reset();
          } else {
            const productDetailResponse = yield _this2.customerQuotationService.getSerialNoProductDetail(_this2.selectedQuotationData.selectedBusinessPartnerKey.id, _this2.selectedQuotationData.service_product_id.id, _this2.selectedQuotationData.bp_shiping_add_id.id, _this2.selectedQuotationData.bp_billing_add_id.id).toPromise(); //  console.log(this.TAG,productDetailResponse);

            let tempBuild = {
              "m_attributesetinstance_id": _this2.selectedSerialNo.m_attributesetinstance_id,
              "serialno": _this2.selectedSerialNo.serialno ? _this2.selectedSerialNo.serialno : "",
              "taxname": productDetailResponse.taxname ? productDetailResponse.taxname : "",
              "taxrate": productDetailResponse.taxrate ? productDetailResponse.taxrate : "",
              "taxid": productDetailResponse.taxid ? productDetailResponse.taxid : "",
              "totgrossamt": productDetailResponse.totgrossamt ? productDetailResponse.totgrossamt : "",
              "netamt": productDetailResponse.newamt ? productDetailResponse.newamt : "",
              "qty": "1",
              "uomname": productDetailResponse.uomname ? productDetailResponse.uomname : "",
              "rate": productDetailResponse.rate ? productDetailResponse.rate : "",
              "c_uom_id": productDetailResponse.c_uom_id ? productDetailResponse.c_uom_id : ""
            };

            _this2.tempCartProduct.push(tempBuild);

            _this2.selectedSerialNo = "";

            _this2.addEditServiceProductForm.controls["serialNoCtrl"].setValue("");

            _this2.addEditServiceProductForm.controls['quantityCtrl'].setValue("");

            _this2.btnSaveDisabled = true; //  console.log(this.TAG,"ADDED ARRAY",this.tempCartProduct);
          }
        }
      } catch (error) {
        _this2.commonFunction.presentAlert("Add Product", error.status, error.error); //  console.log(this.TAG,error);

      }
    })();
  }

};

AddEditServiceProductPage.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.Router
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.ActivatedRoute
}, {
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_4__.Commonfun
}, {
  type: _customer_quotation_customer_quotation_service__WEBPACK_IMPORTED_MODULE_3__.CustomerQuotationService
}, {
  type: src_app_login_loginauth_service__WEBPACK_IMPORTED_MODULE_5__.LoginauthService
}];

AddEditServiceProductPage = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
  selector: 'app-add-edit-service-product',
  template: _add_edit_service_product_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_add_edit_service_product_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], AddEditServiceProductPage);


/***/ }),

/***/ 5991:
/*!**************************************************************************************************!*\
  !*** ./src/app/quotation/add-edit-service-product/add-edit-service-product.page.scss?ngResource ***!
  \**************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhZGQtZWRpdC1zZXJ2aWNlLXByb2R1Y3QucGFnZS5zY3NzIn0= */";

/***/ }),

/***/ 21432:
/*!**************************************************************************************************!*\
  !*** ./src/app/quotation/add-edit-service-product/add-edit-service-product.page.html?ngResource ***!
  \**************************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Add Serial No</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]=\"addEditServiceProductForm\" (ngSubmit)=\"onSaveCart()\">\n  <ion-card>\n    <ion-card-content>\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label position=\"stacked\">Serial No</ion-label>\n              <ionic-selectable placeholder=\"Select Serial No\" [searchDebounce]=\"1000\"\n              formControlName=\"serialNoCtrl\"\n              [items]=\"serialNoList\" \n              itemValueField=\"m_attributesetinstance_id\" \n              itemTextField=\"serialno\" \n              [canSearch]=\"true\"\n              [shouldStoreItemValue]=\"false\"\n              (onChange)=\"onSerialNoChange($event)\"\n              (onClose)=\"onSerialNoClose($event)\"\n              (onSearch)=\"onSerialNoSearch($event)\">\n              </ionic-selectable>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-item>\n            <ion-label position=\"floating\">Quantity</ion-label>\n            <ion-input type=\"number\" formControlName=\"quantityCtrl\"></ion-input>       \n          </ion-item>\n        </ion-row>\n        <ion-row>\n          <ion-item float-right lines=\"none\">\n            <ion-button color=\"primary\" text-center [disabled]=\"!addEditServiceProductForm.valid\" (click)=\"addToCart()\">Add to cart</ion-button>\n            <ion-button color=\"primary\" text-center [disabled]=\"!btnSaveDisabled\" (click)=\"onSaveCart()\">Save</ion-button>\n          </ion-item>\n        </ion-row>\n        </ion-grid>\n    </ion-card-content>\n  </ion-card>\n   \n   <ion-card>\n    <ion-card-header>\n      <ion-item color=\"medium\" lines=\"none\">\n        <ion-label>Serial No</ion-label>\n        <ion-icon slot=\"start\"  name=\"cart\">\n        </ion-icon>\n      </ion-item>\n    </ion-card-header>\n      <ion-card-content>\n        <ion-item *ngFor=\"let item of tempCartProduct; index as i\">\n          <ion-icon name=\"trash\" (click)=\"removeProduct(item)\" slot=\"end\"></ion-icon>\n          <ion-label>{{item.serialno}}</ion-label>\n        </ion-item>\n      </ion-card-content>\n   </ion-card>\n  </form>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_quotation_add-edit-service-product_add-edit-service-product_module_ts.js.map