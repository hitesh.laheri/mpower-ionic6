"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_actual-travel-plan_actual-travel-plan_module_ts"],{

/***/ 66045:
/*!*****************************************************************!*\
  !*** ./src/app/actual-travel-plan/actual-travel-plan.module.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActualTravelPlanPageModule": () => (/* binding */ ActualTravelPlanPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _actual_travel_plan_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./actual-travel-plan.page */ 35385);







const routes = [
    {
        path: '',
        component: _actual_travel_plan_page__WEBPACK_IMPORTED_MODULE_0__.ActualTravelPlanPage
    }
];
let ActualTravelPlanPageModule = class ActualTravelPlanPageModule {
};
ActualTravelPlanPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_actual_travel_plan_page__WEBPACK_IMPORTED_MODULE_0__.ActualTravelPlanPage]
    })
], ActualTravelPlanPageModule);



/***/ }),

/***/ 35385:
/*!***************************************************************!*\
  !*** ./src/app/actual-travel-plan/actual-travel-plan.page.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActualTravelPlanPage": () => (/* binding */ ActualTravelPlanPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _actual_travel_plan_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./actual-travel-plan.page.html?ngResource */ 12594);
/* harmony import */ var _actual_travel_plan_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./actual-travel-plan.page.scss?ngResource */ 12777);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _actual_travel_plan_actual_travel_plan_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../actual-travel-plan/actual-travel-plan.service */ 25461);
/* harmony import */ var _provider_message_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../provider/message-helper */ 98792);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! date-fns */ 86527);











let ActualTravelPlanPage = class ActualTravelPlanPage {
    constructor(fb, commonfun, router, route, loginauth, actualtravelplanservice, msg) {
        this.fb = fb;
        this.commonfun = commonfun;
        this.router = router;
        this.route = route;
        this.loginauth = loginauth;
        this.actualtravelplanservice = actualtravelplanservice;
        this.msg = msg;
        this.datefrom = '';
        this.dateto = '';
        this.datetravel = '';
        this.TAG = 'ActualTravelPlanPage';
        this.validation_messages = {
            'selectedplan': [
                { type: 'required', message: ' *Please Select Plan.' }
            ]
        };
        this.addsublead();
        this.formactualtravel = this.fb.group({
            salesperson: [],
            fromdate: [],
            todate: [],
            traveldate: [],
            homelat: [],
            homelong: [],
            outstationOrderChkCtrl: [],
            selectedplan: [, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required]
        });
    }
    ngOnInit() {
        if (this.msg.isplatformweb == true) {
            // this.commonfun.chkcache('actual-travel-plan');
            setTimeout(() => {
                this.getsalesperson();
            }, 1500);
        }
        else {
            setTimeout(() => {
                this.getsalesperson();
            }, 1500);
        }
    }
    formatDate(value) {
        return (0,date_fns__WEBPACK_IMPORTED_MODULE_7__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_8__["default"])(value), 'dd.MM.yyyy');
    }
    formatDate1(value) {
        return (0,date_fns__WEBPACK_IMPORTED_MODULE_7__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_8__["default"])(value), 'dd.MM.yyyy');
    }
    formatDate2(value) {
        console.log(value);
        try {
            this.traveldate = null;
            var df = this.formactualtravel.controls["fromdate"].value;
            var dt = this.formactualtravel.controls["todate"].value;
            var dtr = this.formactualtravel.controls["traveldate"].value;
            //---------------------
            this.traveldatemin = new Date(this.dateyyyymmddT0000Z(df)).toISOString();
            this.traveldatemax = new Date(this.dateyyyymmddT0000Z(dt)).toISOString();
            //-----------------------
            if (dtr != null && dtr != undefined) {
                this.fromdate = new Date(this.dateyyyymmddT0000Z(df)).toISOString();
                this.todate = new Date(this.dateyyyymmddT0000Z(dt)).toISOString();
                this.traveldate = new Date(this.dateyyyymmddT0000Z(dtr)).toISOString();
                if (this.traveldate < this.fromdate || this.traveldate > this.todate) {
                    this.commonfun.presentAlert("Message", "Alert", "Travel Date must be in the range of from date and to date.");
                    this.formactualtravel.controls["traveldate"].setValue(this.fromdate);
                }
            }
        }
        catch (error) {
            // console.log("ondatechange()-ERROR:",error);
        }
        return (0,date_fns__WEBPACK_IMPORTED_MODULE_7__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_8__["default"])(value), 'dd.MM.yyyy');
    }
    /**
       * @kind function
       * @summary This method will get sales person.
       * @since 1.0.0
       * @returns void
       * @public
       * @module Travel Expense
       * @author Nilesh Patil
       */
    getsalesperson() {
        let methodTAG = 'getsalesperson';
        try {
            //localhost:8080/openbravo/ws/in.mbs.webservice.WMobileLatLongUpdate?addid=FFF202005200405006176B076E5C7E39&lat=100&long=2006
            this.commonfun.loadingPresent();
            this.actualtravelplanservice.getWMobileUserWisePlanData("N").subscribe(data => {
                this.commonfun.loadingDismiss();
                var response = data;
                this.salespersoninfo = response;
                this.formactualtravel.controls["salesperson"].setValue(this.salespersoninfo[0].salesperson);
                this.fromdate = this.salespersoninfo[0].fromdate;
                this.todate = this.salespersoninfo[0].todate;
                this.formactualtravel.controls["homelat"].setValue(this.salespersoninfo[0].latitude);
                this.formactualtravel.controls["homelong"].setValue(this.salespersoninfo[0].longitude);
            }, error => {
                //  console.log(this.TAG,methodTAG,error)
                this.commonfun.loadingDismiss();
                this.commonfun.presentAlert("Message", "Error", error.error);
            });
        }
        catch (error) {
            this.commonfun.loadingDismiss();
            // console.log(this.TAG,methodTAG,error)
            this.commonfun.presentAlert("Message", "Error", error.error);
        }
    }
    Dateconversion() {
        try {
            var dl1date = new Date(this.fromdate);
            var nmonth = dl1date.getMonth() + 1;
            var dd1 = (dl1date.getDate() < 10 ? "0" + dl1date.getDate() : dl1date.getDate());
            var mm1 = (nmonth < 10 ? "0" + nmonth : nmonth);
            var yyyy1 = dl1date.getFullYear();
            this.strfromdate = dd1 + "-" + mm1 + "-" + yyyy1;
            var dl2date = new Date(this.fromdate);
            var nmonth = dl2date.getMonth() + 1;
            var dd2 = (dl2date.getDate() < 10 ? "0" + dl2date.getDate() : dl2date.getDate());
            var mm2 = (nmonth < 10 ? "0" + nmonth : nmonth);
            var yyyy2 = dl2date.getFullYear();
            this.strtodate = dd2 + "-" + mm2 + "-" + yyyy2;
            var dl3date = new Date(this.traveldate);
            var nmonth3 = dl3date.getMonth() + 1;
            var dd3 = (dl3date.getDate() < 10 ? "0" + dl3date.getDate() : dl3date.getDate());
            var mm3 = (nmonth3 < 10 ? "0" + nmonth3 : nmonth3);
            var yyyy3 = dl3date.getFullYear();
            this.strtraveldate = dd3 + "-" + mm3 + "-" + yyyy3;
            this.d1 = new Date(this.fromdate);
            this.d3 = new Date(this.traveldate);
            this.days = ((this.d3 - this.d1) / (24 * 3600 * 1000)) + 1;
        }
        catch (error) {
            //  console.log("Error",error)
        }
    }
    toggle(selectedcartproduct) {
        if (selectedcartproduct.show == false) {
            for (var i = 0; i < this.leads.length; i++) {
                if (this.leads[i].show === "true") {
                    this.leads[i].show = "false";
                }
            }
        }
        selectedcartproduct.show = !selectedcartproduct.show;
    }
    onChangeplan() {
        this.commonfun.loadingPresent();
        this.actualtravelplanservice.getWMobileUserWisePlanData("N", this.formactualtravel.controls["selectedplan"].value).subscribe(data => {
            this.commonfun.loadingDismiss();
            var result = data;
            //console.log("PLAN", result);
            if (result.length === 0) {
                return;
            }
            this.leads = result[0].AddressList;
            this.formactualtravel.controls["fromdate"].setValue(this.dateyyyymmddT0000Z(result[0].fromdate));
            this.formactualtravel.controls["todate"].setValue(this.dateyyyymmddT0000Z(result[0].todate));
            if (this.formactualtravel.controls["fromdate"].value == this.formactualtravel.controls["todate"].value)
                this.formactualtravel.controls["traveldate"].setValue(this.dateyyyymmddT0000Z(result[0].fromdate));
            else
                this.formactualtravel.controls["traveldate"].setValue(null);
            if (result[0].outstation == 'Y') {
                this.outstation_chk_box = true;
            }
            else {
                this.outstation_chk_box = false;
            }
            //---------------------this.dateyyyymmddT0000Z(result[0].todate)).toISOString()
            var df = this.formactualtravel.controls["fromdate"].value;
            var dt = this.formactualtravel.controls["todate"].value;
            this.traveldatemin = new Date(this.dateyyyymmddT0000Z(result[0].fromdate)).toISOString();
            this.traveldatemax = new Date(this.dateyyyymmddT0000Z(result[0].todate)).toISOString();
            //-----------------------
        });
    }
    onAddLead(item, i) {
        try {
            // if(this.checkplancount()==false)
            // {
            //   return
            // }
            if (this.traveldate == undefined || this.traveldate == null) {
                this.commonfun.presentAlert("Message", "Alert", "Please select Travel Date.");
                return;
            }
            this.Dateconversion();
            let navigationExtras = {
                state: {
                    selectedLead: item,
                    allleads: this.leads,
                    fromdate: this.strfromdate,
                    todate: this.todate,
                    mexp_visitplan_id: this.formactualtravel.controls["selectedplan"].value,
                    days: this.days,
                    traveldate: this.strtraveldate,
                    index: i,
                    outOrderChkCtrl: this.formactualtravel.controls["outstationOrderChkCtrl"].value ? "true" : "false"
                }
            };
            this.router.navigate(['addedit-actual-travel-plan'], navigationExtras);
            // this.formprod.controls["selectedBusinessPartner"].
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    removeLeads(post) {
        try {
            let index = this.leads.indexOf(post);
            const result = this.leads.filter(item => item.line != post.line);
            this.leads = result;
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    // checkplancount(){
    //   try {
    //     if(this.leads){
    //       if(this.leads.length>=this.loginauth.maxcustomerinplan)
    //       {
    //         this.commonfun.presentAlert("Message","Alert!","Your plan is full.");
    //         return false;
    //       }
    //       else{
    //         return true;
    //       }
    //     }
    //     else{
    //       return true;
    //     }
    //   } catch (error) {
    //     console.log("Error: checkplancount :",error);
    //   }
    // }
    removesubLeads(data, subdata) {
        try {
            for (var k = 0; k < this.leads.length; k++) {
                if (this.leads[k].selectedddlsubleads) {
                    if (this.leads[k].selectedddlsubleads.some(it => it = subdata)) {
                        const result = this.leads[k].selectedddlsubleads.filter(item => item != subdata);
                        this.leads[k].selectedddlsubleads = result;
                    }
                }
            }
            // let index = this.leads.indexOf(data);
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    dateyyyymmddT0000Z(dt) {
        try {
            var dl1date = new Date(dt.substr(0, 4), dt.substr(5, 2) - 1, dt.substr(8, 2));
            var nmonth = dl1date.getMonth() + 1;
            var dd1 = (dl1date.getDate() < 10 ? "0" + dl1date.getDate() : dl1date.getDate());
            var mm1 = (nmonth < 10 ? "0" + nmonth : nmonth);
            var yyyy1 = dl1date.getFullYear();
            // this.strfromdate=dd1+"-"+mm1+"-"+yyyy1
            return (yyyy1 + "-" + mm1 + "-" + dd1 + "T00:00Z");
        }
        catch (error) {
        }
    }
    // ondatechange() {
    //   try {
    //     this.traveldate = null;
    //     var df = this.formactualtravel.controls["fromdate"].value;
    //     var dt = this.formactualtravel.controls["todate"].value;
    //     var dtr = this.formactualtravel.controls["traveldate"].value;
    //     //---------------------
    //     this.traveldatemin = new Date(this.dateyyyymmddT0000Z(df)).toISOString();
    //     this.traveldatemax = new Date(this.dateyyyymmddT0000Z(dt)).toISOString();
    //     //-----------------------
    //     if (dtr != null && dtr != undefined) {
    //       this.fromdate = new Date(this.dateyyyymmddT0000Z(df)).toISOString();
    //       this.todate = new Date(this.dateyyyymmddT0000Z(dt)).toISOString();
    //       this.traveldate = new Date(this.dateyyyymmddT0000Z(dtr)).toISOString();
    //       if (this.traveldate < this.fromdate || this.traveldate > this.todate) {
    //         this.commonfun.presentAlert("Message", "Alert", "Travel Date must be in the range of from date and to date.");
    //         this.formactualtravel.controls["traveldate"].setValue(this.fromdate);
    //       }
    //     }
    //   } catch (error) {
    //   }
    // }
    addsublead() {
        try {
            this.route.params.subscribe(params => {
                if (this.router.getCurrentNavigation().extras.state) {
                    var selectedddlsubleads = this.router.getCurrentNavigation().extras.state.selectedddlsubleads;
                    var selectedLead = this.router.getCurrentNavigation().extras.state.selectedLead;
                    //  for(var k=0;k<this.leads.length;k++){
                    // if(this.leads[k].lead===selectedLead.lead){
                    //   this.leads[k].selectedddlsubleads=selectedddlsubleads;
                    // } 
                    // }
                }
            });
            //  console.log("addsublead()-this.leads:",this.leads);
        }
        catch (error) {
            console.log("addsublead()-ERROR:", error);
        }
    }
    hideshowsublead(selectedLead) {
        if (selectedLead.show == false) {
            for (var i = 0; i < this.leads.length; i++) {
                if (this.leads[i].show === "true") {
                    this.leads[i].show = "false";
                }
            }
        }
        selectedLead.show = !selectedLead.show;
    }
    onSaveActualTravelPlan(frnvala, issubmit) {
        try {
            console.log("LEAD ACUTAL", this.leads);
            if (this.traveldate == null || this.traveldate == undefined) {
                this.commonfun.presentAlert("Message", "Alert", "Please select Travel Date.");
                return;
            }
            console.log("TEST", this.leads);
            let showError = false;
            this.leads.forEach(item => {
                item.TaskList.forEach(taskItem => {
                    if (taskItem.info_required == 'Y') {
                        if (taskItem.Done == true) {
                            if (taskItem.remark == null || taskItem.remark == "" || taskItem.remark == undefined) {
                                showError = true;
                            }
                        }
                    }
                });
            });
            if (showError) {
                this.commonfun.presentAlert("Expense Closure", "Validation", "Please enter remark for task ");
            }
            else {
                this.Dateconversion();
                this.commonfun.loadingPresent();
                var jsondatatemp = {
                    "salesperson": this.salespersoninfo[0].salesperson,
                    "mexp_customervisit_id": this.salespersoninfo[0].salespersonid,
                    "salespersonid": this.salespersoninfo[0].salespersonid,
                    "addid": this.salespersoninfo[0].addid,
                    "longitude": this.salespersoninfo[0].longitude,
                    "mexp_visitplan_id": frnvala.selectedplan,
                    "AddressList": this.leads,
                    "latitude": this.salespersoninfo[0].latitude,
                    "fromdate": this.strfromdate,
                    "todate": this.strtodate,
                    "issubmit": issubmit
                };
                //--------------
                this.actualtravelplanservice.SaveActualPlan(jsondatatemp).subscribe(data => {
                    if (data != null) {
                        this.saveplanresponse = data;
                        if (this.saveplanresponse.resposemsg == "Success") {
                            this.commonfun.loadingDismiss();
                            this.commonfun.presentAlert("Message", this.saveplanresponse.resposemsg, this.saveplanresponse.logmsg);
                            this.Resetpage();
                        }
                        else {
                            this.commonfun.loadingDismiss();
                            //  this.Resetpage();
                            this.commonfun.presentAlert("Message", this.saveplanresponse.resposemsg, this.saveplanresponse.logmsg);
                        }
                    }
                }, error => {
                    this.commonfun.loadingDismiss();
                    //  console.log("error",error);
                    this.commonfun.presentAlert("Message", "Error!", error);
                });
                //------------------
            }
        }
        catch (error) {
        }
    }
    Resetpage() {
        try {
            //  this.fromdate=new Date().toISOString();
            // this.todate=new Date().toISOString();
            this.traveldate = null;
            this.formactualtravel.reset();
            this.leads = null;
            setTimeout(() => {
                this.getsalesperson();
            }, 1500);
        }
        catch (error) {
            //        console.log("Resetpage()-ERROR:",error);
        }
    }
};
ActualTravelPlanPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder },
    { type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_3__.Commonfun },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__.Router },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__.ActivatedRoute },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService },
    { type: _actual_travel_plan_actual_travel_plan_service__WEBPACK_IMPORTED_MODULE_4__.ActualTravelPlanService },
    { type: _provider_message_helper__WEBPACK_IMPORTED_MODULE_5__.Message }
];
ActualTravelPlanPage = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.Component)({
        selector: 'app-actual-travel-plan',
        template: _actual_travel_plan_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_actual_travel_plan_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ActualTravelPlanPage);



/***/ }),

/***/ 12777:
/*!****************************************************************************!*\
  !*** ./src/app/actual-travel-plan/actual-travel-plan.page.scss?ngResource ***!
  \****************************************************************************/
/***/ ((module) => {

module.exports = "ion-scroll[scrollX] {\n  white-space: nowrap;\n  overflow: visible;\n  overflow-y: auto;\n}\nion-scroll[scrollX] .scroll-item {\n  display: inline-block;\n}\nion-scroll[scrollX] .selectable-icon {\n  margin: 10px 20px 10px 20px;\n  color: red;\n  font-size: 100px;\n}\nion-scroll[scrollX] ion-avatar img {\n  margin: 10px;\n}\nion-scroll[scroll-avatar] {\n  height: 60px;\n}\n/* Hide ion-content scrollbar */\n::-webkit-scrollbar {\n  display: none;\n}\nh5 {\n  font-style: oblique;\n  color: darkcyan;\n  font-size: large;\n}\nh5 ion-icon {\n  color: lightcoral;\n}\n.inputfile {\n  color: transparent;\n}\n.forecast_container {\n  overflow-x: scroll !important;\n  overflow-x: visible !important;\n  overflow-y: hidden;\n  word-wrap: break-word;\n  height: 20vw;\n  font-size: 0.8em;\n  font-weight: 300;\n}\n.forecast_div {\n  overflow-x: scroll !important;\n  overflow-x: visible !important;\n  overflow-y: hidden;\n  word-wrap: break-word;\n  font-size: 0.8em;\n  font-weight: 300;\n  max-width: 175px;\n}\n.grid-header {\n  font-weight: bold;\n  max-width: 175px;\n}\nion-popover {\n  --width: 320px;\n}\nion-popover.dateTimePopover {\n  --offset-y: -350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFjdHVhbC10cmF2ZWwtcGxhbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtFQUVBLGlCQUFBO0VBQ0EsZ0JBQUE7QUFBSjtBQUVJO0VBQ0UscUJBQUE7QUFBTjtBQUdJO0VBQ0UsMkJBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7QUFETjtBQUlJO0VBQ0UsWUFBQTtBQUZOO0FBTUU7RUFDRSxZQUFBO0FBSEo7QUFNRSwrQkFBQTtBQUNBO0VBQ0UsYUFBQTtBQUhKO0FBTUU7RUFDRSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUhKO0FBSUk7RUFDRSxpQkFBQTtBQUZOO0FBT0U7RUFDRSxrQkFBQTtBQUpKO0FBT0U7RUFDRSw2QkFBQTtFQUNBLDhCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FBSko7QUFNQTtFQUNFLDZCQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FBSEY7QUFLQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUFGQTtBQUlBO0VBQ0UsY0FBQTtBQURGO0FBR0E7RUFDRSw2QkFBQTtBQUFGIiwiZmlsZSI6ImFjdHVhbC10cmF2ZWwtcGxhbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tc2Nyb2xsW3Njcm9sbFhdIHtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgLy8gaGVpZ2h0OiAxMjBweDtcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xuLy8gd2lkdGg6MTAwJTtcbiAgICAuc2Nyb2xsLWl0ZW0ge1xuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIH1cblxuICAgIC5zZWxlY3RhYmxlLWljb257XG4gICAgICBtYXJnaW46IDEwcHggMjBweCAxMHB4IDIwcHg7XG4gICAgICBjb2xvcjogcmVkO1xuICAgICAgZm9udC1zaXplOiAxMDBweDtcbiAgICB9XG5cbiAgICBpb24tYXZhdGFyIGltZ3tcbiAgICAgIG1hcmdpbjogMTBweDtcbiAgICB9XG4gIH1cblxuICBpb24tc2Nyb2xsW3Njcm9sbC1hdmF0YXJde1xuICAgIGhlaWdodDogNjBweDtcbiAgfVxuXG4gIC8qIEhpZGUgaW9uLWNvbnRlbnQgc2Nyb2xsYmFyICovXG4gIDo6LXdlYmtpdC1zY3JvbGxiYXJ7XG4gICAgZGlzcGxheTpub25lO1xuICB9XG5cbiAgaDV7XG4gICAgZm9udC1zdHlsZTogb2JsaXF1ZTtcbiAgICBjb2xvcjogZGFya2N5YW47XG4gICAgZm9udC1zaXplOiBsYXJnZTtcbiAgICBpb24taWNvbntcbiAgICAgIGNvbG9yOiBsaWdodGNvcmFsO1xuICAgIH1cblxuICB9XG4gIFxuICAuaW5wdXRmaWxlIHtcbiAgICBjb2xvcjogdHJhbnNwYXJlbnQ7XG4gIH1cblxuICAuZm9yZWNhc3RfY29udGFpbmVye1xuICAgIG92ZXJmbG93LXg6IHNjcm9sbCFpbXBvcnRhbnQ7XG4gICAgb3ZlcmZsb3cteDogdmlzaWJsZSFpbXBvcnRhbnQ7XG4gICAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICAgIHdvcmQtd3JhcDogYnJlYWstd29yZDtcbiAgICBoZWlnaHQ6MjB2dztcbiAgICBmb250LXNpemU6MC44ZW07XG4gICAgZm9udC13ZWlnaHQ6MzAwO1xufVxuLmZvcmVjYXN0X2RpdntcbiAgb3ZlcmZsb3cteDogc2Nyb2xsIWltcG9ydGFudDtcbiAgb3ZlcmZsb3cteDogdmlzaWJsZSFpbXBvcnRhbnQ7XG4gIG92ZXJmbG93LXk6IGhpZGRlbjtcbiAgd29yZC13cmFwOiBicmVhay13b3JkO1xuICBmb250LXNpemU6MC44ZW07XG4gIGZvbnQtd2VpZ2h0OjMwMDtcbiAgbWF4LXdpZHRoOiAxNzVweDtcbn1cbi5ncmlkLWhlYWRlcntcbmZvbnQtd2VpZ2h0OiBib2xkO1xubWF4LXdpZHRoOiAxNzVweDtcbn1cbmlvbi1wb3BvdmVyIHtcbiAgLS13aWR0aDogMzIwcHg7XG59XG5pb24tcG9wb3Zlci5kYXRlVGltZVBvcG92ZXIge1xuICAtLW9mZnNldC15OiAtMzUwcHggIWltcG9ydGFudDtcbiAgfSJdfQ== */";

/***/ }),

/***/ 12594:
/*!****************************************************************************!*\
  !*** ./src/app/actual-travel-plan/actual-travel-plan.page.html?ngResource ***!
  \****************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Visit Activity Report</ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]=\"formactualtravel\">\n    <ion-card>\n      <ion-card-content>\n        <ion-row>\n          <ion-col>\n            <h5 ion-text class=\"text-primary\">\n              <ion-icon name=\"person\"></ion-icon> Plan :\n            </h5>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label position=\"floating\">Sales Person</ion-label>\n              <ion-input type=\"text\" formControlName=\"salesperson\" [disabled]=\"true\"></ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n\n        <!-- <ion-row>\n        <ion-col>\n          <ion-item >\n            <ion-label position=\"stacked\">Plan<span style=\"color:red!important\">*</span></ion-label>\n            <ion-select #C formControlName=\"formactualtravel.controls['selectedplan']\" interface=\"popover\" placeholder=\"Select One\" (ionChange)='onChangeplan()'>\n              <ion-select-option value=\"A\">Plan A</ion-select-option>\n              <ion-select-option value=\"B\">Plan B</ion-select-option>\n              <ion-select-option value=\"C\">Plan C</ion-select-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n      </ion-row> -->\n\n\n      <ion-row>\n        <ion-col>\n            <ion-item >\n              <ion-label position=\"stacked\">Plan<span style=\"color:red!important\">*</span></ion-label>\n              <ion-select #C formControlName=\"selectedplan\" interface=\"popover\" (ionChange)=\"onChangeplan()\" multiple=\"false\" placeholder=\"Select Plan\">\n                <ion-select-option *ngFor=\"let plan of salespersoninfo\" [value]=\"plan.mexp_visitplan_id\">{{plan.plan}}</ion-select-option>\n              </ion-select>\n              </ion-item>\n              <div padding-left>\n                <ng-container *ngFor=\"let validation of validation_messages.selectedplan\">\n                  <div *ngIf=\"formactualtravel.get('selectedplan').hasError(validation.type) && formactualtravel.get('selectedplan').touched\">\n                    <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n                  </div>\n                </ng-container>\n              </div>\n        </ion-col>\n      </ion-row>\n\n\n\n        <!-- <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label>From Date</ion-label>\n              <ion-datetime placeholder=\"Select Date\" [disabled]=\"true\" formControlName=\"formactualtravel.controls['fromdate']\" displayFormat=\"DD.MM.YYYY\" max=\"2050\"></ion-datetime>\n            </ion-item>\n          </ion-col>\n        </ion-row> -->\n        <ion-item>\n          <ion-label position=\"stacked\">From Date</ion-label>\n          <ion-item [disabled]=\"true\">\n            <ion-input placeholder=\"Select Date\" [value]=\"datefrom\"></ion-input>\n            <ion-button fill=\"clear\" id=\"open-date-input-1\">\n              <ion-icon icon=\"calendar\"></ion-icon>\n            </ion-button>\n            <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\n              <ng-template>\n                <ion-datetime\n                  #popoverDatetime2\n                  formControlName=\"fromdate\" \n                  presentation=\"date\"\n                  showDefaultButtons=\"true\"\n                  (ionChange)=\"datefrom = formatDate(popoverDatetime2.value)\"\n                ></ion-datetime>\n              </ng-template>\n            </ion-popover>\n          </ion-item>\n        </ion-item>\n\n        <!-- <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label>To Date</ion-label>\n              <ion-datetime placeholder=\"Select Date\" [disabled]=\"true\" formControlName=\"formactualtravel.controls['todate']\" displayFormat=\"DD.MM.YYYY\" max=\"2050\"></ion-datetime>\n            </ion-item>\n          </ion-col>\n        </ion-row> -->\n        <ion-item>\n          <ion-label position=\"stacked\">To Date</ion-label>\n          <ion-item [disabled]=\"true\">\n            <ion-input placeholder=\"Select Date\" [value]=\"dateto\"></ion-input>\n            <ion-button fill=\"clear\" id=\"open-date-input-2\">\n              <ion-icon icon=\"calendar\"></ion-icon>\n            </ion-button>\n            <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-2\" show-backdrop=\"false\">\n              <ng-template>\n                <ion-datetime\n                  #popoverDatetime2\n                  formControlName=\"todate\" \n                  presentation=\"date\"\n                  showDefaultButtons=\"true\"\n                  (ionChange)=\"dateto = formatDate1(popoverDatetime2.value)\"\n                ></ion-datetime>\n              </ng-template>\n            </ion-popover>\n          </ion-item>\n        </ion-item>\n\n        <!-- <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label>Travel Date</ion-label>\n              <ion-datetime placeholder=\"Select Date\" formControlName=\"formactualtravel.controls['traveldate']\" (ionChange)=\"ondatechange()\" displayFormat=\"DD.MM.YYYY\" [min]='traveldatemin' [max]='traveldatemax'></ion-datetime>\n            </ion-item>\n          </ion-col>\n        </ion-row> -->\n        <ion-item>\n          <ion-label position=\"stacked\">Travel Date</ion-label>\n          <ion-item >\n            <ion-input placeholder=\"Select Date\" [value]=\"datetravel\"></ion-input>\n            <ion-button fill=\"clear\" id=\"open-date-input-3\">\n              <ion-icon icon=\"calendar\"></ion-icon>\n            </ion-button>\n            <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-3\" show-backdrop=\"true\">\n              <ng-template>\n                <ion-datetime\n                  #popoverDatetime2\n                  presentation=\"date\"\n                  showDefaultButtons=\"true\"\n                  formControlName=\"traveldate\"\n                  [min]='traveldatemin' [max]='traveldatemax'\n                  (ionChange)=\"datetravel = formatDate2(popoverDatetime2.value)\"\n                ></ion-datetime>\n              </ng-template>\n            </ion-popover>\n          </ion-item>\n        </ion-item>\n\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label position=\"floating\">Home Lat</ion-label>\n              <ion-input type=\"text\" formControlName=\"homelat\" [disabled]=\"true\"></ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label position=\"floating\">Home Long</ion-label>\n              <ion-input type=\"text\" formControlName=\"homelong\" [disabled]=\"true\"></ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label>Outstation?</ion-label>\n              <ion-checkbox slot=\"end\" [disabled]=\"true\" [checked]=\"outstation_chk_box\" formControlName=\"outstationOrderChkCtrl\"></ion-checkbox>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n      </ion-card-content>\n    </ion-card>\n\n    <ion-card>\n      <ion-card-content>\n        <ion-row>\n          <ion-col>\n            <h5 ion-text class=\"text-primary\">\n              <ion-icon name=\"bookmarks\"></ion-icon> Customer:\n            </h5>\n          </ion-col>\n          <ion-col>\n            <div style=\"display: none;\">\n          <ion-fab-button size=\"small\" float-right (click)=\"onAddLead(null,null)\">\t\n            <ion-icon name=\"add\"></ion-icon>\t\n          </ion-fab-button>\t\n            </div>\n        </ion-col>\n        </ion-row>\n\n\n\n      \n\n\n\n<div style=\"overflow-x:auto\">\n        <ion-grid>\n         <ion-row nowrap >\n          <ion-col nowrap>\n          <ion-row nowrap>\n           <ion-col col-3 size=\"4\" class=\"grid-header\">Line Number</ion-col>\n           <ion-col size=\"8\" class=\"grid-header\">Customer</ion-col>\n           <ion-col size=\"8\" class=\"grid-header\">Address</ion-col>\n           <ion-col size=\"2\" class=\"grid-header\">Day</ion-col>\n           <ion-col size=\"8\" class=\"grid-header\">Date</ion-col>\n           <!-- <ion-col size=\"5\" class=\"grid-header\">Address Lat</ion-col> -->\n           <!-- <ion-col size=\"5\" class=\"grid-header\">Address Long</ion-col> -->\n           <ion-col size=\"4\" class=\"grid-header\">KM</ion-col>\n           <ion-col size=\"4\" class=\"grid-header\">Status</ion-col>\n\n          </ion-row>\n        </ion-col>\n         </ion-row>\n        \n         <ion-row *ngFor=\"let data of leads; index as i\"  nowrap>\n          <ion-col nowrap>\n         <ion-row nowrap (click)=\"hideshowsublead(data)\">\n            <ion-col size=\"1\" style=\"width: 100%; text-align: right;\">\n              <ion-icon ios=\"ios-add-circle\" md=\"md-add-circle\" (click)=\"onAddLead(data,i)\" style=\"font-size: x-large;\">\n              </ion-icon>\n            </ion-col>\n            <ion-col size=\"1\" style=\"width: 100%; text-align: right;\">\n              <!-- <ion-icon name=\"trash\" (click)=\"removeLeads(data)\" style=\"font-size: x-large;\n              color: red;\"></ion-icon> -->\n            </ion-col>\n          <ion-col size=\"2\" class=\"forecast_div\">{{i+1}}</ion-col>\n          <ion-col size=\"8\" class=\"forecast_div\">{{data.custname}}</ion-col>\n          <ion-col size=\"8\" class=\"forecast_div\">{{data.addressname}}</ion-col>\n          <ion-col  size=\"2\"class=\"forecast_div\">{{data.visit_day}}</ion-col>\n          <ion-col size=\"8\" class=\"forecast_div\">{{data.visit_date}}</ion-col>\n          <!-- <ion-col size=\"5\" class=\"forecast_div\">{{data.latitude}}</ion-col> -->\n          <!-- <ion-col size=\"5\" class=\"forecast_div\">{{data.longitude}}</ion-col> -->\n          <ion-col size=\"4\" class=\"forecast_div\">{{data.km}} km</ion-col>\n          <!-- <ion-col size=\"3\" class=\"forecast_div\">{{data.status}}</ion-col> -->\n          <ion-col size=\"4\" class=\"forecast_div\">\n          <ion-select style=\"width: fit-content;\" [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"data.status\" interface=\"popover\">\n            <ion-select-option value=\"Actual\">Actual</ion-select-option>\n            <ion-select-option value=\"Cancel\">Cancel</ion-select-option>\n            <ion-select-option value=\"CNM\">Customer Not Meet</ion-select-option>\n          </ion-select>\n        </ion-col>\n\n          </ion-row>\n          <div *ngIf=\"data.show\">\n            <ion-row nowrap>\n              <ion-col nowrap>\n                <ion-row nowrap>\n                \n                 <ion-col size=\"4\" offsetLg=\"2\" class=\"grid-header\">Task</ion-col>\n                 <ion-col size=\"2\" class=\"grid-header\">Done?</ion-col>\n                </ion-row>\n\n                <ion-row *ngFor=\"let task of data.TaskList; index as i\" nowrap>\n                  \n                 <ion-col size=\"4\"  offsetLg=\"2\" class=\"forecast_div\">{{task.task}}</ion-col>\n                 <ion-col  size=\"2\" class=\"forecast_div\">\n                    <ion-checkbox [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"task.Done\"></ion-checkbox>\n                 </ion-col>\n                 <ion-col size=\"2\" class=\"forecast_div\" *ngIf=\"task.info_required!='N' && task.Done==true\" >\n                  <ion-textarea type=\"text\" placeholder=\"Remark\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"task.remark\"></ion-textarea>\n                </ion-col>\n                </ion-row>\n            \n\n              </ion-col>\n            </ion-row>\n                  \n            </div>\n        </ion-col>\n          </ion-row>\n       \n\n        </ion-grid>     \n      </div>\n              <ion-item float-right lines=\"none\">\n                <ion-button color=\"primary\" text-center (click)=\"onSaveActualTravelPlan(formactualtravel.value,false)\">Save Visit Report</ion-button>\n              </ion-item>\n              <ion-item float-right lines=\"none\">\n                <ion-button color=\"primary\" text-center (click)=\"onSaveActualTravelPlan(formactualtravel.value,true)\">Submit Visit Activity Report?</ion-button>\n              </ion-item>\n      </ion-card-content>\n    </ion-card>\n\n  </form>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_actual-travel-plan_actual-travel-plan_module_ts.js.map