"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_cardinal_service-engineer-visit_service-engineer-visit_module_ts"],{

/***/ 2035:
/*!**********************************************************************************!*\
  !*** ./src/app/cardinal/service-engineer-visit/service-engineer-visit.module.ts ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ServiceEngineerVisitPageModule": () => (/* binding */ ServiceEngineerVisitPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _service_engineer_visit_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./service-engineer-visit.page */ 79943);







const routes = [
    {
        path: '',
        component: _service_engineer_visit_page__WEBPACK_IMPORTED_MODULE_0__.ServiceEngineerVisitPage
    }
];
let ServiceEngineerVisitPageModule = class ServiceEngineerVisitPageModule {
};
ServiceEngineerVisitPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_service_engineer_visit_page__WEBPACK_IMPORTED_MODULE_0__.ServiceEngineerVisitPage]
    })
], ServiceEngineerVisitPageModule);



/***/ }),

/***/ 79943:
/*!********************************************************************************!*\
  !*** ./src/app/cardinal/service-engineer-visit/service-engineer-visit.page.ts ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ServiceEngineerVisitPage": () => (/* binding */ ServiceEngineerVisitPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _service_engineer_visit_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./service-engineer-visit.page.html?ngResource */ 60922);
/* harmony import */ var _service_engineer_visit_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./service-engineer-visit.page.scss?ngResource */ 94089);
/* harmony import */ var _service_engineer_visit_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./service-engineer-visit.service */ 39797);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 60124);







let ServiceEngineerVisitPage = class ServiceEngineerVisitPage {
  constructor(router, serviceEngineerVisitService) {
    this.router = router;
    this.serviceEngineerVisitService = serviceEngineerVisitService;
    this.TAG = "ServiceEngineerVisitPage";
  }

  ngOnInit() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.venderApprovalList = yield _this.serviceEngineerVisitService.getVenderApprovalList().toPromise(); // console.log(this.TAG,this.venderApprovalList);
    })();
  }

  ionViewWillEnter() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this2.venderApprovalList = yield _this2.serviceEngineerVisitService.getVenderApprovalList().toPromise(); // console.log(this.TAG,this.venderApprovalList);
    })();
  }

  refreshPage() {
    try {
      this.ionViewWillEnter();
    } catch (error) {
      console.error(this.TAG, error);
    }
  }

  detailsClick(service) {
    try {
      let navigationExtras = {
        queryParams: {
          special: JSON.stringify(service)
        }
      };
      this.router.navigate(['service-engineer-visit-detail'], navigationExtras);
    } catch (error) {// console.log(this.TAG,error);
    }
  }

};

ServiceEngineerVisitPage.ctorParameters = () => [{
  type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.Router
}, {
  type: _service_engineer_visit_service__WEBPACK_IMPORTED_MODULE_3__.ServiceEngineerVisitService
}];

ServiceEngineerVisitPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
  selector: 'app-service-engineer-visit',
  template: _service_engineer_visit_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_service_engineer_visit_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ServiceEngineerVisitPage);


/***/ }),

/***/ 94089:
/*!*********************************************************************************************!*\
  !*** ./src/app/cardinal/service-engineer-visit/service-engineer-visit.page.scss?ngResource ***!
  \*********************************************************************************************/
/***/ ((module) => {

module.exports = ".arrow-custom {\n  background-color: lightgrey;\n  padding: 5px;\n  border-radius: 5px;\n}\n\n.doc-custom {\n  background-color: #C8E6C9;\n  padding: 5px;\n  border-radius: 5px;\n  color: #1B5E20;\n}\n\n.card-custom {\n  padding-left: 2px !important;\n  padding-right: 0px !important;\n  padding-top: 8px !important;\n  padding-bottom: 0% !important;\n}\n\n.arrow-col {\n  margin-top: 8px;\n}\n\n.complain-custom {\n  font-weight: bolder;\n  font-size: large;\n}\n\n.comname-custom {\n  color: darkgoldenrod;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2UtZW5naW5lZXItdmlzaXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksMkJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFDQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQUVKOztBQUFBO0VBQ0ksNEJBQUE7RUFDQSw2QkFBQTtFQUNBLDJCQUFBO0VBQ0EsNkJBQUE7QUFHSjs7QUFEQTtFQUNJLGVBQUE7QUFJSjs7QUFGQTtFQUNJLG1CQUFBO0VBQ0EsZ0JBQUE7QUFLSjs7QUFIQTtFQUNJLG9CQUFBO0FBTUoiLCJmaWxlIjoic2VydmljZS1lbmdpbmVlci12aXNpdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYXJyb3ctY3VzdG9te1xuICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Z3JleTtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuLmRvYy1jdXN0b217XG4gICAgYmFja2dyb3VuZC1jb2xvcjojQzhFNkM5O1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgY29sb3I6ICMxQjVFMjA7XG59XG4uY2FyZC1jdXN0b217XG4gICAgcGFkZGluZy1sZWZ0OiAycHggIWltcG9ydGFudDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAwcHggIWltcG9ydGFudDtcbiAgICBwYWRkaW5nLXRvcDogOHB4ICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZy1ib3R0b206IDAlICFpbXBvcnRhbnQ7XG59XG4uYXJyb3ctY29se1xuICAgIG1hcmdpbi10b3A6IDhweDtcbn1cbi5jb21wbGFpbi1jdXN0b217XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBmb250LXNpemU6IGxhcmdlO1xufVxuLmNvbW5hbWUtY3VzdG9te1xuICAgIGNvbG9yOiBkYXJrZ29sZGVucm9kO1xuICAgLy8gZm9udC1zaXplOiBpbml0aWFsO1xufSJdfQ== */";

/***/ }),

/***/ 60922:
/*!*********************************************************************************************!*\
  !*** ./src/app/cardinal/service-engineer-visit/service-engineer-visit.page.html?ngResource ***!
  \*********************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Field Visit\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <ion-buttons (click)=\"refreshPage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-list>\n    <ion-card  class=\"card-custom\"  *ngFor=\"let service of venderApprovalList\" (click)=\"detailsClick(service)\" padding>\n     <ion-row>\n       <ion-col size=\"4\">\n        <ion-label class=\"comname-custom\">\n          {{service.nameofcomplainer}}\n        </ion-label>\n       </ion-col>\n       <ion-col size=\"4\" text-left>\n        <ion-label>\n          {{service.complaintdate | date:'dd-MMM-yyyy'}}\n        </ion-label>\n       </ion-col>\n       <ion-col size=\"4\" text-end>\n        <ion-label class=\"doc-custom\">\n          {{service.doctype[0].name}}\n        </ion-label>\n       </ion-col>\n     </ion-row>\n     <ion-row>\n       <ion-col size=\"10\">\n        <ion-label class=\"complain-custom\">\n          {{service.complaintno}}\n        </ion-label>\n       </ion-col>\n       <ion-col size=\"2\" text-end>\n         <div class=\"arrow-col\">\n          <ion-icon name=\"arrow-forward\" class=\"arrow-custom\"></ion-icon>\n         </div>\n       </ion-col>\n     </ion-row>\n    </ion-card>\n  </ion-list>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_cardinal_service-engineer-visit_service-engineer-visit_module_ts.js.map