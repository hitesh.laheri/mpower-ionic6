"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_lat-long-finder_lat-long-finder_module_ts"],{

/***/ 54586:
/*!***********************************************************!*\
  !*** ./src/app/lat-long-finder/lat-long-finder.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LatLongFinderPageModule": () => (/* binding */ LatLongFinderPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _lat_long_finder_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./lat-long-finder.page */ 1003);







const routes = [
    {
        path: '',
        component: _lat_long_finder_page__WEBPACK_IMPORTED_MODULE_0__.LatLongFinderPage
    }
];
let LatLongFinderPageModule = class LatLongFinderPageModule {
};
LatLongFinderPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_lat_long_finder_page__WEBPACK_IMPORTED_MODULE_0__.LatLongFinderPage]
    })
], LatLongFinderPageModule);



/***/ }),

/***/ 1003:
/*!*********************************************************!*\
  !*** ./src/app/lat-long-finder/lat-long-finder.page.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LatLongFinderPage": () => (/* binding */ LatLongFinderPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _lat_long_finder_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./lat-long-finder.page.html?ngResource */ 71151);
/* harmony import */ var _lat_long_finder_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./lat-long-finder.page.scss?ngResource */ 82824);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);




let LatLongFinderPage = class LatLongFinderPage {
    constructor() { }
    ngOnInit() {
    }
};
LatLongFinderPage.ctorParameters = () => [];
LatLongFinderPage = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-lat-long-finder',
        template: _lat_long_finder_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_lat_long_finder_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], LatLongFinderPage);



/***/ }),

/***/ 82824:
/*!**********************************************************************!*\
  !*** ./src/app/lat-long-finder/lat-long-finder.page.scss?ngResource ***!
  \**********************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsYXQtbG9uZy1maW5kZXIucGFnZS5zY3NzIn0= */";

/***/ }),

/***/ 71151:
/*!**********************************************************************!*\
  !*** ./src/app/lat-long-finder/lat-long-finder.page.html?ngResource ***!
  \**********************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>lat-long-finder</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_lat-long-finder_lat-long-finder_module_ts.js.map