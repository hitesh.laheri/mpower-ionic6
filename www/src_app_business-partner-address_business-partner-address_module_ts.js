"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_business-partner-address_business-partner-address_module_ts"],{

/***/ 96866:
/*!*****************************************************************************!*\
  !*** ./src/app/business-partner-address/business-partner-address.module.ts ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BusinessPartnerAddressPageModule": () => (/* binding */ BusinessPartnerAddressPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _business_partner_address_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./business-partner-address.page */ 94794);








const routes = [
    {
        path: '',
        component: _business_partner_address_page__WEBPACK_IMPORTED_MODULE_0__.BusinessPartnerAddressPage
    }
];
let BusinessPartnerAddressPageModule = class BusinessPartnerAddressPageModule {
};
BusinessPartnerAddressPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule, ionic_selectable__WEBPACK_IMPORTED_MODULE_5__.IonicSelectableModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterModule.forChild(routes)
        ],
        declarations: [_business_partner_address_page__WEBPACK_IMPORTED_MODULE_0__.BusinessPartnerAddressPage]
    })
], BusinessPartnerAddressPageModule);



/***/ }),

/***/ 94794:
/*!***************************************************************************!*\
  !*** ./src/app/business-partner-address/business-partner-address.page.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BusinessPartnerAddressPage": () => (/* binding */ BusinessPartnerAddressPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _business_partner_address_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./business-partner-address.page.html?ngResource */ 15923);
/* harmony import */ var _business_partner_address_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./business-partner-address.page.scss?ngResource */ 66000);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../neworder/neworder.service */ 17216);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../provider/commonfun */ 51156);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _business_partner_address_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./business-partner-address.service */ 84718);










let BusinessPartnerAddressPage = class BusinessPartnerAddressPage {
    constructor(fb, loginauth, neworderservice, commonfun, router, route, businesspartneraddressservice) {
        this.fb = fb;
        this.loginauth = loginauth;
        this.neworderservice = neworderservice;
        this.commonfun = commonfun;
        this.router = router;
        this.route = route;
        this.businesspartneraddressservice = businesspartneraddressservice;
        this.selectedactivity = '';
        this.validation_messages = {
            'selectedBusinessPartner': [
                { type: 'required', message: '' }
            ]
        };
        this.formaddr = this.fb.group({
            selectedBusinessPartner: [, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required]
        });
        route.params.subscribe(val => {
            this.getparam();
        });
    }
    ngOnInit() {
        this.Resetpage();
        //this.commonfun.chkcache('business-partner-address');
        setTimeout(() => {
            // this.Bindallactivity();
        }, 1500);
    }
    getparam() {
        this.route.params.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state.reset) {
                this.Resetpage();
            }
        });
    }
    oncustchange(event) {
        this.getaddressbycustid(event.value.id);
        event.component._searchText = "";
    }
    custsearchChange(event) {
        var custsearchtext = event.text; //.replace(/\s/g,'');
        if (custsearchtext.length % 3 == 0) {
            this.bindcustomerapi(custsearchtext);
        }
    }
    onCancel(event) {
        event.component._searchText = "";
    }
    getaddressbycustid(bp_id) {
        try {
            this.businesspartneraddressservice.getaddressbycustid(bp_id).subscribe(data => {
                this.ComplianceList = data["ComplianceList"];
                this.cartaddress = data["Listofadd"];
            }, error => {
                this.commonfun.presentAlert("Message", "Error", error.error.text);
            });
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    toggleaddress(selectedcartproduct) {
        if (selectedcartproduct.show == false) {
            for (var i = 0; i < this.cartaddress.length; i++) {
                if (this.cartaddress[i].show === "true") {
                    this.cartaddress[i].show = "false";
                }
            }
        }
        selectedcartproduct.show = !selectedcartproduct.show;
    }
    onAddAddress() {
        try {
            if ((this.formaddr.controls["selectedBusinessPartner"].value != undefined && this.formaddr.controls["selectedBusinessPartner"].value != null) && this.formaddr.controls["selectedBusinessPartner"].value != "") {
                let navigationExtras = {
                    state: {
                        ComplianceList: this.ComplianceList,
                        businessPartner: this.formaddr.controls["selectedBusinessPartner"].value,
                        cartaddress: this.cartaddress
                    }
                };
                this.router.navigate(['addedit-business-partner-address'], navigationExtras);
            }
            else {
                this.commonfun.presentAlert("Message", "Alert", "Please select customer.");
            }
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
        }
    }
    onSaveAddress(fvalue) {
    }
    bindcustomerapi(strsearch) {
        try {
            if (strsearch != "") {
                this.businesspartneraddressservice.getexistcustmerapi(strsearch).subscribe(data => {
                    var response = data;
                    this.BusinessPartnerlist = response;
                });
            }
            else {
                // this.BusinessPartnerlist=null;
                //=============start for top 10================= 
                this.businesspartneraddressservice.getexistcustmerapi("").subscribe(data => {
                    var response = data;
                    // this.BusinessPartnerlist = response;
                    this.leastBusinessPartnerlist = response;
                    if (this.leastBusinessPartnerlist.length > this.loginauth.minlistcount) {
                        this.BusinessPartnerlist = null;
                    }
                    else {
                        this.BusinessPartnerlist = response;
                    }
                });
                //=============end for top 10================= 
            }
        }
        catch (error) {
            this.commonfun.presentAlert("Message", "Error", error);
            // this.commonfun.loadingDismiss();
        }
    }
    Bindallactivity() {
        try {
            this.loginauth.getuserwiseactivity(this.loginauth.userid).subscribe(data => {
                this.activitylist = data;
                if (this.activitylist.length == 1) {
                    setTimeout(() => {
                        this.selectedactivity = this.activitylist[0].id;
                    }, 500);
                }
                //  this.loadingDismiss();
            }, error => {
                //.. this.loadingDismiss();
            });
        }
        catch (error) {
            //this.loadingDismiss();
        }
    }
    Resetpage() {
        try {
            this.BusinessPartnerlist = null;
            this.ComplianceList = null;
            this.BusinessPartnerlist = null;
            this.cartaddress = null;
            this.selectedactivity = '';
            this.formaddr.controls['selectedBusinessPartner'].setValue(null);
            this.formaddr.controls['selectedBusinessPartner'].clearValidators();
            if (this.activitylist.length == 1) {
                this.selectedactivity = this.activitylist[0].id;
            }
            else {
                this.selectedactivity = '';
            }
        }
        catch { }
    }
};
BusinessPartnerAddressPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_3__.LoginauthService },
    { type: _neworder_neworder_service__WEBPACK_IMPORTED_MODULE_2__.NeworderService },
    { type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_4__.Commonfun },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.Router },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.ActivatedRoute },
    { type: _business_partner_address_service__WEBPACK_IMPORTED_MODULE_5__.BusinessPartnerAddressService }
];
BusinessPartnerAddressPage = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
        selector: 'app-business-partner-address',
        template: _business_partner_address_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_business_partner_address_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], BusinessPartnerAddressPage);



/***/ }),

/***/ 66000:
/*!****************************************************************************************!*\
  !*** ./src/app/business-partner-address/business-partner-address.page.scss?ngResource ***!
  \****************************************************************************************/
/***/ ((module) => {

module.exports = "ion-scroll[scrollX] {\n  white-space: nowrap;\n  overflow: visible;\n  overflow-y: auto;\n}\nion-scroll[scrollX] .scroll-item {\n  display: inline-block;\n}\nion-scroll[scrollX] .selectable-icon {\n  margin: 10px 20px 10px 20px;\n  color: red;\n  font-size: 100px;\n}\nion-scroll[scrollX] ion-avatar img {\n  margin: 10px;\n}\nion-scroll[scroll-avatar] {\n  height: 60px;\n}\n/* Hide ion-content scrollbar */\n::-webkit-scrollbar {\n  display: none;\n}\nh5 {\n  font-style: oblique;\n  color: darkcyan;\n  font-size: large;\n}\nh5 ion-icon {\n  color: lightcoral;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJ1c2luZXNzLXBhcnRuZXItYWRkcmVzcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtFQUVBLGlCQUFBO0VBQ0EsZ0JBQUE7QUFBSjtBQUVJO0VBQ0UscUJBQUE7QUFBTjtBQUdJO0VBQ0UsMkJBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7QUFETjtBQUlJO0VBQ0UsWUFBQTtBQUZOO0FBTUU7RUFDRSxZQUFBO0FBSEo7QUFNRSwrQkFBQTtBQUNBO0VBQ0UsYUFBQTtBQUhKO0FBTUU7RUFDRSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUhKO0FBSUk7RUFDRSxpQkFBQTtBQUZOIiwiZmlsZSI6ImJ1c2luZXNzLXBhcnRuZXItYWRkcmVzcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tc2Nyb2xsW3Njcm9sbFhdIHtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgLy8gaGVpZ2h0OiAxMjBweDtcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xuLy8gd2lkdGg6MTAwJTtcbiAgICAuc2Nyb2xsLWl0ZW0ge1xuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIH1cblxuICAgIC5zZWxlY3RhYmxlLWljb257XG4gICAgICBtYXJnaW46IDEwcHggMjBweCAxMHB4IDIwcHg7XG4gICAgICBjb2xvcjogcmVkO1xuICAgICAgZm9udC1zaXplOiAxMDBweDtcbiAgICB9XG5cbiAgICBpb24tYXZhdGFyIGltZ3tcbiAgICAgIG1hcmdpbjogMTBweDtcbiAgICB9XG4gIH1cblxuICBpb24tc2Nyb2xsW3Njcm9sbC1hdmF0YXJde1xuICAgIGhlaWdodDogNjBweDtcbiAgfVxuXG4gIC8qIEhpZGUgaW9uLWNvbnRlbnQgc2Nyb2xsYmFyICovXG4gIDo6LXdlYmtpdC1zY3JvbGxiYXJ7XG4gICAgZGlzcGxheTpub25lO1xuICB9XG5cbiAgaDV7XG4gICAgZm9udC1zdHlsZTogb2JsaXF1ZTtcbiAgICBjb2xvcjogZGFya2N5YW47XG4gICAgZm9udC1zaXplOiBsYXJnZTtcbiAgICBpb24taWNvbntcbiAgICAgIGNvbG9yOiBsaWdodGNvcmFsO1xuICAgIH1cblxuICB9XG4gIFxuIl19 */";

/***/ }),

/***/ 15923:
/*!****************************************************************************************!*\
  !*** ./src/app/business-partner-address/business-partner-address.page.html?ngResource ***!
  \****************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n   <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title style=\"font-size: small;\">\n      Business Partner Address\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <ion-buttons (click)=\"Resetpage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n \n  <form [formGroup]=\"formaddr\" (ngSubmit)=\"onSaveAddress(formaddr.value)\">\n    <ion-grid fixed>\n  <ion-card>\n    <ion-card-content>\n      <ion-row>\n        <ion-col>\n          <h5 ion-text class=\"text-primary\">\n            <ion-icon name=\"person\"></ion-icon> Customer Detail :\n          </h5>\n        </ion-col>\n      </ion-row>\n\n      <!-- <ion-row>\n        <ion-col>\n            <ion-item >\n              <ion-label  position=\"stacked\">Organization Activity</ion-label>\n              <ion-select [ngModelOptions]=\"{standalone: true}\" name=\"selectedactivity\" #C [(ngModel)]=\"selectedactivity\" interface=\"popover\"  (ionChange)=\"exonActChange()\" multiple=\"false\" placeholder=\"Select Activity\">\n                <ion-select-option *ngFor=\"let activity of activitylist\" [value]=\"activity.id\">{{activity.name}}</ion-select-option>\n              </ion-select>\n              </ion-item>\n        </ion-col>\n      </ion-row> -->\n\n      \n      <ion-row>\n        <ion-col>\n          <ion-item>\n            <ion-label position=\"stacked\">Customer<span style=\"color:red!important\">*</span></ion-label>\n            <ionic-selectable placeholder=\"Select Customer\"\n            formControlName=\"selectedBusinessPartner\"\n            [items]=\"BusinessPartnerlist\" \n            itemValueField=\"id\" \n            itemTextField=\"sfname\" \n            [canSearch]=\"true\"\n            (onChange)=\"oncustchange($event)\"\n            (onSearch)=\"custsearchChange($event)\"\n            (onClose)=\"onCancel($event)\"\n            >\n            </ionic-selectable>\n              <!-- (onSearch)=\"custsearchChange($event)\" -->\n          </ion-item>\n          <div padding-left>\n            <ng-container *ngFor=\"let validation of validation_messages.selectedBusinessPartner\">\n              <div *ngIf=\"formaddr.get('selectedBusinessPartner').hasError(validation.type) && formaddr.get('selectedBusinessPartner').touched\">\n                <p style=\"color: red;font-size: small;\">{{validation.message}}</p>\n              </div>\n            </ng-container>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n    </ion-card>\n\n    <ion-card>\n      <ion-card-content>\n      <ion-row>\n        <ion-col>\n          <h5 ion-text class=\"text-primary\">\n            <ion-icon name=\"navigate\"></ion-icon>  Address:\n          </h5>\n        </ion-col>\n        <ion-col>\n          <ion-fab-button size=\"small\" float-right (click)=\"onAddAddress()\">\n            <ion-icon name=\"add\"></ion-icon>\n          </ion-fab-button>\n        </ion-col>\n      </ion-row>\n\n      <ion-item *ngFor=\"let item of cartaddress; index as i\"  text-wrap style=\"font-size: small; max-width: 100% !important;\">\n        <!-- <ion-content scrollX=\"true\"> -->\n          <div style=\"width: 100%;\" (click)=\"toggleaddress(item)\">\n        <ion-row>\n          <ion-col size=\"12\">\n            <ion-label style=\"white-space: normal;\">\n          <ion-icon *ngIf=\"!item.MainProductQty\" style=\"color: springgreen;\" name=\"locate\"></ion-icon>\n              {{ item._identifier }}\n            </ion-label>\n          </ion-col>\n        </ion-row>\n        \n        <ion-row>\n          <ion-col nowrap>\n            <div *ngIf=\"item.show\">\n              <ion-label style=\"font-size: small;\">Billing Address: <span *ngIf=\"!item.billadd\">No</span><span *ngIf=\"item.billadd\">Yes</span></ion-label>\n              <!-- <ion-checkbox slot=\"end\" [(ngModel)]=\"item.billadd\"></ion-checkbox> -->\n\n              <ion-label style=\"font-size: small;\">Shipping Address: <span *ngIf=\"!item.shipadd\">No</span><span *ngIf=\"item.shipadd\">Yes</span></ion-label>\n              <!-- <ion-checkbox slot=\"end\" [(ngModel)]=\"item.shipadd\"></ion-checkbox> -->\n\n              <ion-label style=\"font-size: small;\">Compliance Type Number: {{item.gstno}}</ion-label>\n              <ion-label style=\"font-size: small;\">Active: <span *ngIf=\"!item.isactive\">No</span><span *ngIf=\"item.isactive\">Yes</span></ion-label>\n           \n            </div>\n          </ion-col>\n         \n        </ion-row>\n      </div>\n        <!-- </ion-content> -->\n      </ion-item>\n\n\n    </ion-card-content>\n  </ion-card>\n\n \n</ion-grid>\n</form>\n\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_business-partner-address_business-partner-address_module_ts.js.map