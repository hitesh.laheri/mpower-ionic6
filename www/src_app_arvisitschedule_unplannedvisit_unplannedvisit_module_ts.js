"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_arvisitschedule_unplannedvisit_unplannedvisit_module_ts"],{

/***/ 48598:
/*!************************************************************!*\
  !*** ./src/app/arvisitschedule/arvisitschedule.service.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ArvisitscheduleService": () => (/* binding */ ArvisitscheduleService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);





let ArvisitscheduleService = class ArvisitscheduleService {
    constructor(http, loginauth, genericHTTP) {
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
    }
    getArVisitPlan(body) {
        //console.log("Body: ",body);
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTgetRequest', body, httpOptions);
    }
    saveArVisitPlan(body) {
        //   console.log("Body: ",body);
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTSaveARVisitPlan', body, httpOptions);
    }
    saveArVisitUnplannedPlan(body) {
        //   console.log("Body: ",body);
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTSaveUnplannedARVisitPlan', body, httpOptions);
    }
    getRequest(body) {
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.qcinspection.MQCIgetRequest', body, httpOptions);
    }
    onSaveSectionQuestion(body) {
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Authorization': 'Basic ' + auth
            })
        };
        var specificHeader = { 'Authorization': 'Basic ' + auth };
        return this.genericHTTP.postformdata(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTonSaveChecklistTrx', body, specificHeader, httpOptions);
    }
};
ArvisitscheduleService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_1__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_0__.GenericHttpClientService }
];
ArvisitscheduleService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], ArvisitscheduleService);



/***/ }),

/***/ 47894:
/*!*************************************************************************!*\
  !*** ./src/app/arvisitschedule/unplannedvisit/unplannedvisit.module.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UnplannedvisitPageModule": () => (/* binding */ UnplannedvisitPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _unplannedvisit_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./unplannedvisit.page */ 73381);








const routes = [
    {
        path: '',
        component: _unplannedvisit_page__WEBPACK_IMPORTED_MODULE_0__.UnplannedvisitPage
    }
];
let UnplannedvisitPageModule = class UnplannedvisitPageModule {
};
UnplannedvisitPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes),
            ionic_selectable__WEBPACK_IMPORTED_MODULE_7__.IonicSelectableModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule
        ],
        declarations: [_unplannedvisit_page__WEBPACK_IMPORTED_MODULE_0__.UnplannedvisitPage],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_2__.CUSTOM_ELEMENTS_SCHEMA]
    })
], UnplannedvisitPageModule);



/***/ }),

/***/ 73381:
/*!***********************************************************************!*\
  !*** ./src/app/arvisitschedule/unplannedvisit/unplannedvisit.page.ts ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UnplannedvisitPage": () => (/* binding */ UnplannedvisitPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _unplannedvisit_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./unplannedvisit.page.html?ngResource */ 10061);
/* harmony import */ var _unplannedvisit_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./unplannedvisit.page.scss?ngResource */ 49522);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _provider_commonfun__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../provider/commonfun */ 51156);
/* harmony import */ var _arvisitschedule_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../arvisitschedule.service */ 48598);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! date-fns */ 86527);










let UnplannedvisitPage = class UnplannedvisitPage {
  constructor(fb, arvisitScheduleService, commonfun, router) {
    this.fb = fb;
    this.arvisitScheduleService = arvisitScheduleService;
    this.commonfun = commonfun;
    this.router = router;
    this.datevisit = '';
    this.validation_messages = {
      'fromplan': [{
        type: 'required',
        message: ' *Please Select Plan.'
      }],
      'customer': [{
        type: 'required',
        message: '*Please Select Customer.'
      }],
      'billtoaddress': [{
        type: 'required',
        message: 'Please Select Billing Address.'
      }]
    };
    this.searchc = '';
    this.offsetc = 0;
    this.formplan = this.fb.group({
      plansch: [, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required],
      customer: [, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required],
      billtoaddress: [, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required],
      visitdate: [],
      targetformonth: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required]
    });
    this.bindARVisitSch();
  }

  ngOnInit() {
    this.currentdate = new Date().toISOString();
    this.formplan.get("visitdate").setValue(this.currentdate);
  }

  formatDate(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_6__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_7__["default"])(value), 'dd-MM-yyyy');
  }

  bindCustomerBillAddress(bpartnerid) {
    try {
      var body = {
        "action": "getcustomerinvoiceaddress",
        "bpartnerid": bpartnerid
      };
      this.arvisitScheduleService.getArVisitPlan(body).subscribe(data => {
        var response = data;
        this.billtoaddressList = response.data;

        if (this.billtoaddressList.length === 1) {
          this.formplan.get("billtoaddress").setValue(this.billtoaddressList[0]);
        }
      });
    } catch (error) {//console.log("bindOwnedDriverapi",error);
    }
  }

  bindARVisitSch() {
    try {
      var body = {
        "action": "getlistOfarvisitschedule"
      };
      this.arvisitScheduleService.getArVisitPlan(body).subscribe(data => {
        var response = data;
        this.arschlist = response.data;
      });
    } catch (error) {//console.log("bindOwnedDriverapi",error);
    }
  }

  onCloseCust(event) {
    event.component.searchText = "";
    this.offsetc = 0;

    if (this.formplan.get("customer").value) {
      let bpartnerid = this.formplan.get("customer").value.bpartnerid;
      this.bindCustomerBillAddress(bpartnerid);
    }
  }

  custSearchChange(event) {
    this.searchc = event.text; //.replace(/\s/g,'');

    this.offsetc = 0;
    this.bindCustomerList(this.searchc, 0);
  }

  bindCustomerList(searchtext, intoffset) {
    try {
      var body = {
        "action": "getcustomerforar",
        "offset": intoffset,
        "search": searchtext
      };
      this.arvisitScheduleService.getArVisitPlan(body).subscribe(data => {
        var response = data;
        this.customerlist = response.data;
        this.totalrowc = response.totalRows;
      });
    } catch (error) {//console.log("bindOwnedDriverapi",error);
    }
  }

  doInfiniteCust(event) {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        //Set this initial 0 at top 
        console.log('inside');
        let text = (event.text || '').trim().toLowerCase();

        if (_this.offsetc < _this.totalrowc && _this.totalrowc > 20) {
          console.log('inside', _this.offsetc);
          _this.offsetc = _this.offsetc + 20;
          var body = {
            "action": "getcustomerforar",
            "offset": _this.offsetc,
            "search": text
          };
          var tempData = yield (yield _this.arvisitScheduleService.getArVisitPlan(body)).toPromise();

          if (!!tempData) {
            _this.customerlist = _this.customerlist.concat(tempData['data']);
            event.component.endInfiniteScroll();
          }
        }

        if (_this.offsetc > _this.totalrowc) {
          event.component.disableInfiniteScroll();
          return;
        }
      } catch (error) {}
    })();
  }

  onSaveVisit(frmValue) {
    //console.log(frmValue);
    try {
      var body = {
        "schid": frmValue.plansch.schid,
        "bpartnerlocid": frmValue.billtoaddress.bpartnerlocid,
        "bpartnerid": frmValue.customer.bpartnerid,
        "targetformonth": frmValue.targetformonth,
        "visitdate": frmValue.visitdate
      };
      this.commonfun.loadingPresent();
      this.arvisitScheduleService.saveArVisitUnplannedPlan(body).subscribe(data => {
        this.commonfun.loadingDismiss();
        var response = data['response'];

        if (response.hasOwnProperty('messageType')) {
          if (response.messageType == 'success') this.commonfun.presentAlert("Message", "Success", response.messageText);
          this.router.navigateByUrl('/arvisitschedule');
        }

        if (response.error) this.commonfun.presentAlert("Message", "Error", response.error.message);
      });
    } catch (error) {
      this.commonfun.loadingDismiss();
      this.commonfun.presentAlert("Message", "Error", error);
    }
  }

};

UnplannedvisitPage.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormBuilder
}, {
  type: _arvisitschedule_service__WEBPACK_IMPORTED_MODULE_4__.ArvisitscheduleService
}, {
  type: _provider_commonfun__WEBPACK_IMPORTED_MODULE_3__.Commonfun
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router
}];

UnplannedvisitPage = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
  selector: 'app-unplannedvisit',
  template: _unplannedvisit_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_unplannedvisit_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], UnplannedvisitPage);


/***/ }),

/***/ 49522:
/*!************************************************************************************!*\
  !*** ./src/app/arvisitschedule/unplannedvisit/unplannedvisit.page.scss?ngResource ***!
  \************************************************************************************/
/***/ ((module) => {

module.exports = "ion-popover {\n  --width: 320px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVucGxhbm5lZHZpc2l0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7QUFDSiIsImZpbGUiOiJ1bnBsYW5uZWR2aXNpdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tcG9wb3ZlciB7XG4gICAgLS13aWR0aDogMzIwcHg7XG4gIH1cbiAgLy8gaW9uLXBvcG92ZXIuZGF0ZVRpbWVQb3BvdmVyIHtcbiAgLy8gICAtLW9mZnNldC15OiAtMzUwcHggIWltcG9ydGFudDtcbiAgLy8gICB9Il19 */";

/***/ }),

/***/ 10061:
/*!************************************************************************************!*\
  !*** ./src/app/arvisitschedule/unplannedvisit/unplannedvisit.page.html?ngResource ***!
  \************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Unplanned Visit\n    </ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n<ion-content>\n  <form [formGroup]=\"formplan\" (ngSubmit)=\"onSaveVisit(formplan.value)\">\n  \n    <ion-grid >\n      <ion-row>\n        <ion-col>\n          <ion-row>\n            <ion-col size=\"12\">\n              <ion-item>\n              <ion-label position=\"stacked\">Plan Selection<span style=\"color:red!important\">*</span></ion-label>\n              <ion-select formControlName=\"plansch\" interface=\"popover\" multiple=\"false\"\n                placeholder=\"Select Plan\">\n                <ion-select-option *ngFor=\"let sch of arschlist\" [value]=\"sch\">{{sch.schname}}\n                </ion-select-option>\n              </ion-select>\n            </ion-item>\n            </ion-col>\n          </ion-row>\n         \n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-item>\n            <ion-label position=\"stacked\">Customer Name<span style=\"color:red!important\">*</span></ion-label>\n            <ionic-selectable  placeholder=\"Select Plan\" \n              formControlName=\"customer\"\n              [items]=\"customerlist\"\n              itemValueField=\"bpartnerid\" \n              itemTextField=\"bpartnername\" \n              [canSearch]=\"true\" \n              (onClose)=\"onCloseCust($event)\" \n              (onSearch)=\"custSearchChange($event)\"\n              [hasInfiniteScroll]=\"true\"\n              (onInfiniteScroll)=\"doInfiniteCust($event)\">\n            </ionic-selectable>\n           \n          </ion-item>\n         \n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"12\">\n          <ion-item>\n          <ion-label position=\"stacked\">Billing Address<span style=\"color:red!important\">*</span></ion-label>\n          <ion-select formControlName=\"billtoaddress\" interface=\"popover\" multiple=\"false\"\n            placeholder=\"Select Address\">\n            <ion-select-option *ngFor=\"let billadd of billtoaddressList\" [value]=\"billadd\">{{billadd.bpartnerlocname}}\n            </ion-select-option>\n          </ion-select>\n        </ion-item>\n        </ion-col>\n      </ion-row>\n      <!-- <ion-row>\n        <ion-col size=\"12\">\n          <ion-item>\n          <ion-label position=\"stacked\">Visit Date<span style=\"color:red!important\">*</span></ion-label>\n          <ion-datetime [disabled]=\"true\" [max]=\"currentdate\" [min]=\"currentdate\" displayFormat=\"DD-MM-YYYY\" formControlName=\"visitdate\"></ion-datetime>\n        </ion-item>\n        </ion-col>\n      </ion-row> -->\n      <ion-item >\n        <ion-label position=\"stacked\">Visit Date<span style=\"color:red!important\">*</span></ion-label>\n        <ion-item [disabled]=\"true\">\n          <ion-input placeholder=\"Select Date\" [value]=\"datevisit\"></ion-input>\n          <ion-button fill=\"clear\" id=\"open-date-input-1\">\n            <ion-icon icon=\"calendar\"></ion-icon>\n          </ion-button>\n          <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-1\" show-backdrop=\"false\">\n            <ng-template>\n              <ion-datetime\n                #popoverDatetime2\n                formControlName=\"fromdate\" \n                presentation=\"date\"\n                showDefaultButtons=\"true\"\n                [max]=\"currentdate\" [min]=\"currentdate\"\n                (ionChange)=\"datevisit = formatDate(popoverDatetime2.value)\"\n              ></ion-datetime>\n            </ng-template>\n          </ion-popover>\n        </ion-item>\n      </ion-item>\n      <ion-row>\n        <ion-col size=\"12\">\n          <ion-item>\n          <ion-label position=\"stacked\">Target for the month<span style=\"color:red!important\">*</span></ion-label>\n         <ion-input type=\"number\" placeholder=\"Target for the month\" formControlName=\"targetformonth\"></ion-input>\n        </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n  \n\n</ion-content>\n<section class=\"full-width\">\n  <ion-row>\n    <ion-col>\n      <ion-button expand=\"full\" type=\"submit\" color=\"primary\" text-center [disabled]=\"!formplan.valid \"\n      (click)=\"onSaveVisit(formplan.value)\">Submit</ion-button>\n</ion-col>\n\n</ion-row>\n</section>";

/***/ })

}]);
//# sourceMappingURL=src_app_arvisitschedule_unplannedvisit_unplannedvisit_module_ts.js.map