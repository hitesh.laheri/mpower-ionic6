"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_hrms_mpr-list_mpr-list_module_ts"],{

/***/ 76208:
/*!**************************************************!*\
  !*** ./src/app/hrms/mpr-list/mpr-list.module.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MprListPageModule": () => (/* binding */ MprListPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _mpr_list_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mpr-list.page */ 54149);







const routes = [
    {
        path: '',
        component: _mpr_list_page__WEBPACK_IMPORTED_MODULE_0__.MprListPage
    }
];
let MprListPageModule = class MprListPageModule {
};
MprListPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_mpr_list_page__WEBPACK_IMPORTED_MODULE_0__.MprListPage]
    })
], MprListPageModule);



/***/ }),

/***/ 54149:
/*!************************************************!*\
  !*** ./src/app/hrms/mpr-list/mpr-list.page.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MprListPage": () => (/* binding */ MprListPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _mpr_list_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mpr-list.page.html?ngResource */ 79993);
/* harmony import */ var _mpr_list_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./mpr-list.page.scss?ngResource */ 88011);
/* harmony import */ var src_provider_commonfun__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/provider/commonfun */ 51156);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _mpr_form_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../mpr-form.service */ 89792);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);









let MprListPage = class MprListPage {
  constructor(MPRService, fb, commonfun, router) {
    this.MPRService = MPRService;
    this.fb = fb;
    this.commonfun = commonfun;
    this.router = router;
    this.TAG = "MPR List Page";
  }

  ngOnInit() {}

  ionViewWillEnter() {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this.commonfun.loadingPresent();

        const result = yield _this.MPRService.getMPRList().toPromise();
        console.log(_this.TAG, "Before", result);
        _this.MPRList = result.jobtitlename.JobTitleList;
        console.log(_this.TAG, _this.MPRList);

        _this.commonfun.loadingDismiss();
      } catch (error) {
        console.log(_this.TAG, error);

        _this.commonfun.loadingDismiss();
      }
    })();
  }

  refreshPage() {
    try {} catch (error) {
      console.log(this.TAG, error);
    }
  }

  detailsClick(mpr) {
    try {
      console.log(this.TAG, "Get Details IS Called", mpr);
      let navigationExtras = {
        queryParams: {
          special: JSON.stringify(mpr)
        }
      };
      this.router.navigate(['mpr-form'], navigationExtras);
    } catch (error) {
      console.log(this.TAG, error);
    }
  }

};

MprListPage.ctorParameters = () => [{
  type: _mpr_form_service__WEBPACK_IMPORTED_MODULE_4__.MprFormService
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormBuilder
}, {
  type: src_provider_commonfun__WEBPACK_IMPORTED_MODULE_3__.Commonfun
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.Router
}];

MprListPage = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
  selector: 'app-mpr-list',
  template: _mpr_list_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_mpr_list_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], MprListPage);


/***/ }),

/***/ 88011:
/*!*************************************************************!*\
  !*** ./src/app/hrms/mpr-list/mpr-list.page.scss?ngResource ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = ".arrow-custom {\n  background-color: lightgrey;\n  padding: 5px;\n  border-radius: 5px;\n}\n\n.doc-custom {\n  background-color: #C8E6C9;\n  padding: 5px;\n  border-radius: 5px;\n  color: #1B5E20;\n}\n\n.card-custom {\n  padding-left: 2px !important;\n  padding-right: 0px !important;\n  padding-top: 8px !important;\n  padding-bottom: 0% !important;\n}\n\n.arrow-col {\n  margin-top: 8px;\n}\n\n.complain-custom {\n  font-weight: bolder;\n  font-size: large;\n}\n\n.comname-custom {\n  color: darkgoldenrod;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1wci1saXN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDJCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FBQ0o7O0FBQ0E7RUFDSSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFFSjs7QUFBQTtFQUNJLDRCQUFBO0VBQ0EsNkJBQUE7RUFDQSwyQkFBQTtFQUNBLDZCQUFBO0FBR0o7O0FBREE7RUFDSSxlQUFBO0FBSUo7O0FBRkE7RUFDSSxtQkFBQTtFQUNBLGdCQUFBO0FBS0o7O0FBSEE7RUFDSSxvQkFBQTtBQU1KIiwiZmlsZSI6Im1wci1saXN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hcnJvdy1jdXN0b217XG4gICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmV5O1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG4uZG9jLWN1c3RvbXtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNDOEU2Qzk7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBjb2xvcjogIzFCNUUyMDtcbn1cbi5jYXJkLWN1c3RvbXtcbiAgICBwYWRkaW5nLWxlZnQ6IDJweCAhaW1wb3J0YW50O1xuICAgIHBhZGRpbmctcmlnaHQ6IDBweCAhaW1wb3J0YW50O1xuICAgIHBhZGRpbmctdG9wOiA4cHggIWltcG9ydGFudDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMCUgIWltcG9ydGFudDtcbn1cbi5hcnJvdy1jb2x7XG4gICAgbWFyZ2luLXRvcDogOHB4O1xufVxuLmNvbXBsYWluLWN1c3RvbXtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIGZvbnQtc2l6ZTogbGFyZ2U7XG59XG4uY29tbmFtZS1jdXN0b217XG4gICAgY29sb3I6IGRhcmtnb2xkZW5yb2Q7XG4gICAvLyBmb250LXNpemU6IGluaXRpYWw7XG59Il19 */";

/***/ }),

/***/ 79993:
/*!*************************************************************!*\
  !*** ./src/app/hrms/mpr-list/mpr-list.page.html?ngResource ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      MPR List\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"font-size: 1.8rem;\" routerDirection=\"root\" routerLink=\"/home\">\n      <ion-icon name=\"home\"></ion-icon>\n    </ion-buttons>\n    <ion-buttons (click)=\"refreshPage()\" slot=\"end\" style=\"font-size: 1.8rem;\">\n      <ion-icon name=\"refresh\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-card class=\"card-custom\" *ngFor=\"let mpr of MPRList\" (click)=\"detailsClick(mpr)\" padding>\n      <ion-row>\n        <ion-col size=\"4\">\n         <ion-label class=\"complain-custom\">\n           {{mpr.JobTitle}}\n         </ion-label>\n        </ion-col>\n        <ion-col size=\"4\" text-left>\n         <ion-label>\n           {{mpr.mpr_date | date:'dd-MMM-yyyy'}}\n         </ion-label>\n        </ion-col>\n        <ion-col size=\"4\" text-end>\n         <ion-label class=\"doc-custom\">\n           {{mpr.role_name}}\n         </ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"10\">\n         <ion-label class=\"\">\n           {{mpr.department_name}}\n         </ion-label>\n        </ion-col>\n        <ion-col size=\"2\" text-end>\n          <div class=\"arrow-col\">\n           <ion-icon name=\"arrow-forward\" class=\"arrow-custom\"></ion-icon>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  </ion-list>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_hrms_mpr-list_mpr-list_module_ts.js.map