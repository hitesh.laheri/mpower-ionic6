"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_arvisitschedule_section_section_module_ts"],{

/***/ 66722:
/*!*******************************************************************!*\
  !*** ./src/app/arvisitschedule/section/section-routing.module.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SectionPageRoutingModule": () => (/* binding */ SectionPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _section_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./section.page */ 20011);




const routes = [
    {
        path: '',
        component: _section_page__WEBPACK_IMPORTED_MODULE_0__.SectionPage
    }
];
let SectionPageRoutingModule = class SectionPageRoutingModule {
};
SectionPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SectionPageRoutingModule);



/***/ }),

/***/ 51836:
/*!***********************************************************!*\
  !*** ./src/app/arvisitschedule/section/section.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SectionPageModule": () => (/* binding */ SectionPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../components/components.module */ 45642);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _section_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./section-routing.module */ 66722);
/* harmony import */ var _section_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./section.page */ 20011);








let SectionPageModule = class SectionPageModule {
};
SectionPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _section_routing_module__WEBPACK_IMPORTED_MODULE_1__.SectionPageRoutingModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.ReactiveFormsModule,
            _components_components_module__WEBPACK_IMPORTED_MODULE_0__.ComponentsModule
        ],
        declarations: [_section_page__WEBPACK_IMPORTED_MODULE_2__.SectionPage]
    })
], SectionPageModule);



/***/ }),

/***/ 20011:
/*!*********************************************************!*\
  !*** ./src/app/arvisitschedule/section/section.page.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SectionPage": () => (/* binding */ SectionPage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _section_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./section.page.html?ngResource */ 42159);
/* harmony import */ var _section_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./section.page.scss?ngResource */ 27473);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _arvisitschedule_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../arvisitschedule.service */ 48598);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _awesome_cordova_plugins_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @awesome-cordova-plugins/barcode-scanner/ngx */ 38283);
/* harmony import */ var src_provider_validator_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/provider/validator-helper */ 35096);
/* harmony import */ var src_provider_message_helper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/provider/message-helper */ 98792);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! date-fns */ 86712);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! date-fns */ 86527);












let SectionPage = class SectionPage {
  constructor(fb, checklistservice, val, msg, router, route, barcodeScanner) {
    this.fb = fb;
    this.checklistservice = checklistservice;
    this.val = val;
    this.msg = msg;
    this.router = router;
    this.route = route;
    this.barcodeScanner = barcodeScanner;
    this.islast = false;
    this.sSection = "Section";
    this.sectionIndex = 0;
    this.curentquestionlist = [];
    this.remove = false;
    this.datequestion = '';
    this.datequestion1 = '';
    this.datequestion2 = '';
    this.myForm = this.fb.group({});
  }

  ionViewWillEnter() {
    this.route.params.subscribe(params => {
      if (this.router.getCurrentNavigation()) if (this.router.getCurrentNavigation().extras.state) {
        let currentques = this.router.getCurrentNavigation().extras.state.question;

        if (currentques.type === 'SL' || currentques.type === 'MET') {
          this.myForm.get(currentques.questionid).setValue(currentques.ans);
        }
      }
    });
  }

  backPress(event) {
    //console.log('test');
    this.resetJsonAns();
    this.curentquestionlist = [];
    this.router.navigateByUrl('/arvisitschedule');
  }

  OnClickSelector(question, event) {
    let navigationExtras = {
      state: {
        question: question
      }
    };
    this.router.navigate(['selectorsinglequestionframwork'], navigationExtras);
  }

  OnClickMultipleEntry(question, event) {
    let navigationExtras = {
      state: {
        question: question
      }
    };
    this.router.navigate(['multipleentry'], navigationExtras);
  }

  ngOnInit() {
    //console.log('insdie');
    // this.sectionIndex=this.checklistservice.secIndex;
    this.jsonSection = this.checklistservice.selectedinspection.sections[this.sectionIndex];
    this.sSection = this.jsonSection.sectionname;
    this.questionJsonList = this.checklistservice.selectedinspection.sections[this.sectionIndex].questions;
    this.firstControlLoads(this.questionJsonList);
    this.showcontrol(this.questionJsonList);

    if (this.sectionIndex === this.checklistservice.selectedinspection.sections.length - 1) {
      this.islast = true;
    } else {
      this.islast = false;
    }
  }

  formatDate(value) {
    console.log("click");
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_7__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_8__["default"])(value), 'dd-MM-yyyy');
  }

  formatDate1(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_7__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_8__["default"])(value), 'dd-MM-yyyy HH:mm:ss');
  }

  formatDate2(value) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_7__["default"])((0,date_fns__WEBPACK_IMPORTED_MODULE_8__["default"])(value), 'HH:mm A');
  }

  firstControlLoads(listOfQuestion) {
    this.curentquestionlist = [];

    for (let question of listOfQuestion) {
      if (question.isfirst && !question.isload) {
        this.createControls(question);
      }
    }
  }

  resetJsonAns() {
    for (let section of this.checklistservice.selectedinspection.sections) {
      for (let que of section.questions) {
        que.isload = false;
        que.ans = '';
      }
    }
  }

  createControls(jsonQuestion) {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.curentquestionlist.push(jsonQuestion);

      _this.setSequenceOfQuestion();

      const newFormControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_9__.FormControl(); // add dynamic que no. sequence
      // console.log('validation',control.validation);

      if (jsonQuestion.validation === 'emailValid') {
        newFormControl.setAsyncValidators(_this.val.emailValid);
      }

      if (jsonQuestion.validation === 'pannoValid') {
        newFormControl.setAsyncValidators(_this.val.pannoValid);
      }

      if (jsonQuestion.validation === 'digit15Valid') {
        newFormControl.setAsyncValidators(_this.val.digit15Valid);
      }

      if (jsonQuestion.validation === 'digit10Valid') {
        newFormControl.setAsyncValidators(_this.val.digit10Valid);
      }

      if (jsonQuestion.validation === 'gstnumberValid') {
        newFormControl.setAsyncValidators(_this.val.gstnumberValid);
      }

      if (jsonQuestion.validation === 'numberValid') {
        newFormControl.setAsyncValidators(_this.val.numberValid);
      }

      if (jsonQuestion.validation === 'positivenumberValid') {
        newFormControl.setAsyncValidators(_this.val.positivenumberValid);
      }

      if (jsonQuestion.validation === 'vehiclenumberValid') {
        newFormControl.setAsyncValidators(_this.val.vehiclenumberValid);
      }

      if (jsonQuestion.required) {
        newFormControl.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required);
      }

      newFormControl.setValue(jsonQuestion.ans);

      _this.myForm.addControl(jsonQuestion.questionid, newFormControl);

      jsonQuestion.isload = true;
      _this.remove = false;
      yield _this.onCreateGroupFormValueChange();
    })();
  }

  onCreateGroupFormValueChange() {
    var _this2 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this2.old = { ..._this2.myForm.value
      };

      _this2.myForm.valueChanges.subscribe(value => {
        // console.log('remove',this.remove);
        // console.log('add',question);
        const queid = Object.keys(value).find(k => value[k] != _this2.old[k]);

        if (queid && !_this2.remove) {
          var question = _this2.getQueDetFromKey(queid); // update ans of it


          if (question.type === 'DATE') {
            question.ans = value[queid].split('T')[0];
          } else if (question.type === 'TIME') {
            if (value[queid] !== '') question.ans = value[queid].split('T')[1].substring(0, 8);
          } else if (question.type === 'TUP') {
            question.ans = value[queid].toUpperCase();
          } else {
            question.ans = value[queid];
          }

          if (question) {
            _this2.setQuestionColor(question); //   console.log(question);


            if (question.dependentque) {
              _this2.removeDependentQuestionControl(question);
            }

            for (let s of question.dependentque.split(',')) {
              const addque = _this2.getQueDetFromQueNo(s);

              if (addque) {
                let v = addque.condition;

                let expression = _this2.iterateCondition(v);

                try {
                  //   console.log(this.runExpression(expression));
                  if (_this2.runExpression(expression) === true) {
                    _this2.remove = true;

                    _this2.createControls(addque);
                  }
                } catch (error) {//   console.log(error);
                }
              }

              _this2.remove = false;
            }
          }

          _this2.setSequenceOfQuestion();

          _this2.old = { ..._this2.myForm.value
          };
        }

        _this2.old = { ..._this2.myForm.value
        };
      });
    })();
  }

  getQueDetFromKey(key) {
    for (let question of this.questionJsonList) {
      if (key === question.questionid) {
        //console.log('getQueDetFromKey',question);
        return question;
      }
    }
  }

  getQueDetFromQueNo(qno) {
    for (let question of this.questionJsonList) {
      if (qno === question.questionno) {
        //console.log('getQueDetFromKey',question);
        return question;
      }
    }
  }

  setSequenceOfQuestion() {
    let xlist = this.curentquestionlist.sort((a, b) => a.srno > b.srno ? 1 : -1);
    let i = 1;

    for (let xx of xlist) {
      xx.queno = i;
      i = i + 1;
    }
  }

  prevSection(event) {
    var _this3 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (_this3.sectionIndex > 0) {
        // first hide all current component
        _this3.hideControl();

        _this3.sectionIndex = _this3.sectionIndex - 1;
        _this3.jsonSection = _this3.checklistservice.selectedinspection.sections[_this3.sectionIndex];
        _this3.sSection = _this3.jsonSection.sectionname;
        _this3.questionJsonList = _this3.checklistservice.selectedinspection.sections[_this3.sectionIndex].questions;

        _this3.firstControlLoads(_this3.questionJsonList);

        _this3.showcontrol(_this3.questionJsonList); // this.checklistservice.secIndex=this.sectionIndex;


        _this3.islast = false;
      }
    })();
  }

  nextSection(event) {
    var _this4 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (_this4.sectionIndex < _this4.checklistservice.selectedinspection.sections.length - 1) {
        // first hide all current component
        _this4.hideControl();

        _this4.sectionIndex = _this4.sectionIndex + 1;
        _this4.jsonSection = _this4.checklistservice.selectedinspection.sections[_this4.sectionIndex];
        _this4.sSection = _this4.jsonSection.sectionname;
        _this4.questionJsonList = _this4.checklistservice.selectedinspection.sections[_this4.sectionIndex].questions;

        _this4.firstControlLoads(_this4.questionJsonList);

        _this4.showcontrol(_this4.questionJsonList); // this.checklistservice.secIndex=this.sectionIndex;

      }

      if (_this4.sectionIndex === _this4.checklistservice.selectedinspection.sections.length - 1) {
        _this4.islast = true;
      } else {
        _this4.islast = false;
      } //   console.log(this.curentquestionlist);

    })();
  }

  showcontrol(questionJsonList) {
    var _this5 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      for (let que of questionJsonList) {
        que.ishide = false;
      }

      if (_this5.curentquestionlist.length === 0) {
        for (let que of questionJsonList) {
          if (que.isload) {
            _this5.curentquestionlist.push(que);
          }
        }
      }
    })();
  }

  hideControl() {
    var _this6 = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      for (let que of _this6.curentquestionlist) {
        que.ishide = true;
      }
    })();
  }

  removeDependentQuestionControl(control) {
    let v = control.dependentque;

    for (let tag of v.split(',')) {
      //console.log(tag);
      // console.log(this.myForm.get(tag));
      this.remove = true;
      const f = this.curentquestionlist.find(({
        questionno
      }) => questionno === tag);
      const index = this.curentquestionlist.indexOf(f); // console.log(f);

      if (f === undefined || f === null) {} else {
        this.curentquestionlist.splice(index, 1);

        if (this.myForm.get(f.questionid) !== null) {
          f.ans = '';
          f.isload = false;
          this.myForm.removeControl(f.questionid); //   console.log('remove control',f);
        }
      }
    }

    this.setSequenceOfQuestion(); //   console.log(this.curentquestionlist);
  }

  iterateCondition(condition) {
    let expression = "";
    expression = condition;
    const questionnos = this.extractQuestion(condition);

    for (let queno of questionnos) {
      expression = expression.replace(queno, this.getFormControlNamefromQuestion(queno)).replace('[', '').replace(']', '');
    } //   console.log(expression);


    return expression;
  }

  getFormControlNamefromQuestion(qno) {
    const que = this.curentquestionlist.find(({
      questionno
    }) => questionno === qno);

    if (que) {
      return '\'' + this.myForm.get(que.questionid).value + '\'';
    } else {
      return 'false';
    }
  }

  extractQuestion(str) {
    const words = [];

    for (let i = 0; i < str.length; i++) {
      if (str.charAt(i) === '[') {
        const stopIndex = str.indexOf(']', i);
        if (stopIndex !== -1) words.push(str.substring(i + 1, stopIndex));
      }
    }

    return words;
  }

  runExpression(expression) {
    return eval(expression);
  }

  submitForm(event) {
    for (let section of this.checklistservice.selectedinspection.sections) {
      ////   console.log('section',section);
      for (let que of section.questions) {
        if (que.ans !== '' && que.isload) {
          this.setQuestionColor(que);
        }
      }

      this.router.navigateByUrl('/sectionview');
    }
  }

  findInvalidControls() {
    const invalid = [];
    const controls = this.myForm.controls;

    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }

    console.log(invalid);
  }

  setQuestionColor(question) {
    for (let color of question.colors) {
      let istrue = false;

      if (color.operator === 'EQ' && question.ans === color.value) {
        istrue = true;
        question.colorid = color.colorid;
        question.colorbackground = color.colorbackground;
        question.forecolor = color.forecolor;
        return;
      } else if (color.operator === 'NE' && question.ans !== color.value) {
        istrue = true;
        question.colorid = color.colorid;
        question.colorbackground = color.colorbackground;
        question.forecolor = color.forecolor;
        return;
      } else if (color.operator === 'CT' && question.ans.includes(color.value)) {
        istrue = true;
        question.colorid = color.colorid;
        question.colorbackground = color.colorbackground;
        question.forecolor = color.forecolor;
        return;
      } else if (color.operator === 'GT') {
        if (question.type === 'DATE' && new Date(question.ans) > new Date(color.value)) {
          istrue = true;
          question.colorid = color.colorid;
          question.colorbackground = color.colorbackground;
          question.forecolor = color.forecolor;
          return;
        } else if (question.type === 'INPUT' && question.inputtype === "number" && new Number(question.ans) > new Number(color.value)) {
          istrue = true;
          question.colorid = color.colorid;
          question.colorbackground = color.colorbackground;
          question.forecolor = color.forecolor;
          return;
        }
      } else if (color.operator === 'GTE') {
        if (question.type === 'DATE' && new Date(question.ans) >= new Date(color.value)) {
          istrue = true;
          question.colorid = color.colorid;
          question.colorbackground = color.colorbackground;
          question.forecolor = color.forecolor;
          return;
        } else if (question.type === 'INPUT' && question.inputtype === "number" && new Number(question.ans) >= new Number(color.value)) {
          istrue = true;
          question.colorid = color.colorid;
          question.colorbackground = color.colorbackground;
          question.forecolor = color.forecolor;
          return;
        }
      } else if (color.operator === 'LT') {
        if (question.type === 'DATE' && new Date(question.ans) < new Date(color.value)) {
          istrue = true;
          question.colorid = color.colorid;
          question.colorbackground = color.colorbackground;
          question.forecolor = color.forecolor;
          return;
        } else if (question.type === 'INPUT' && question.inputtype === "number" && new Number(question.ans) < new Number(color.value)) {
          istrue = true;
          question.colorid = color.colorid;
          question.colorbackground = color.colorbackground;
          question.forecolor = color.forecolor;
          return;
        }
      } else if (color.operator === 'LTE') {
        if (question.type === 'DATE' && new Date(question.ans) <= new Date(color.value)) {
          istrue = true;
          question.colorid = color.colorid;
          question.colorbackground = color.colorbackground;
          question.forecolor = color.forecolor;
          return;
        } else if (question.type === 'INPUT' && question.inputtype === "number" && new Number(question.ans) <= new Number(color.value)) {
          istrue = true;
          question.colorid = color.colorid;
          question.colorbackground = color.colorbackground;
          question.forecolor = color.forecolor;
          return;
        }
      } else if (color.operator === 'BT') {
        if (question.type === 'DATE' && new Date(question.ans) >= new Date(color.fromvalue) && new Date(question.ans) <= new Date(color.tovalue)) {
          istrue = true;
          question.colorid = color.colorid;
          question.colorbackground = color.colorbackground;
          question.forecolor = color.forecolor;
          return;
        } else if (question.type === 'INPUT' && question.inputtype === "number" && new Number(question.ans) >= new Number(color.fromvalue) && new Number(question.ans) <= new Number(color.tovalue)) {
          istrue = true;
          question.colorid = color.colorid;
          question.colorbackground = color.colorbackground;
          question.forecolor = color.forecolor;
          return;
        }
      } else if (color.operator === 'NBT') {
        if (question.type === 'DATE' && new Date(question.ans) < new Date(color.fromvalue) && new Date(question.ans) > new Date(color.tovalue)) {
          istrue = true;
          question.colorid = color.colorid;
          question.colorbackground = color.colorbackground;
          question.forecolor = color.forecolor;
          return;
        } else if (question.type === 'INPUT' && question.inputtype === "number" && new Number(question.ans) < new Number(color.fromvalue) || new Number(question.ans) > new Number(color.tovalue)) {
          istrue = true;
          question.colorid = color.colorid;
          question.colorbackground = color.colorbackground;
          question.forecolor = color.forecolor;
          return;
        }
      } else {
        istrue = false;
        question.colorid = '';
        question.colorbackground = '';
        question.forecolor = '';
      }
    }
  }

  ScanQRcode(question) {
    console.log(question);

    try {
      //console.log("ScanQRcode");
      this.barcodeScanner.scan().then(barcodeData => {
        console.log(barcodeData.text);
        question.ans = barcodeData.text;
        console.log(question.queno);
        this.myForm.get(question.questionid).setValue(barcodeData.text);
      });
    } catch (error) {//console.log("ScanQRcode: error:",error)
    }
  }

};

SectionPage.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_9__.FormBuilder
}, {
  type: _arvisitschedule_service__WEBPACK_IMPORTED_MODULE_3__.ArvisitscheduleService
}, {
  type: src_provider_validator_helper__WEBPACK_IMPORTED_MODULE_5__.Validator
}, {
  type: src_provider_message_helper__WEBPACK_IMPORTED_MODULE_6__.Message
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_10__.Router
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_10__.ActivatedRoute
}, {
  type: _awesome_cordova_plugins_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_4__.BarcodeScanner
}];

SectionPage = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
  selector: 'app-section',
  template: _section_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_section_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], SectionPage);


/***/ }),

/***/ 27473:
/*!**********************************************************************!*\
  !*** ./src/app/arvisitschedule/section/section.page.scss?ngResource ***!
  \**********************************************************************/
/***/ ((module) => {

module.exports = "ion-popover {\n  --width: 320px;\n}\n\nion-popover.dateTimePopover {\n  --offset-y: -350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlY3Rpb24ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksY0FBQTtBQUNKOztBQUNFO0VBQ0UsNkJBQUE7QUFFSiIsImZpbGUiOiJzZWN0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1wb3BvdmVyIHtcbiAgICAtLXdpZHRoOiAzMjBweDtcbiAgfVxuICBpb24tcG9wb3Zlci5kYXRlVGltZVBvcG92ZXIge1xuICAgIC0tb2Zmc2V0LXk6IC0zNTBweCAhaW1wb3J0YW50O1xuICAgIH0iXX0= */";

/***/ }),

/***/ 42159:
/*!**********************************************************************!*\
  !*** ./src/app/arvisitschedule/section/section.page.html?ngResource ***!
  \**********************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button (click)=\"backPress($event)\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>{{sSection}}</ion-title>\n    <!-- <ion-buttons slot=\"end\">\n      <div class=\"ion-text-center\">\n        <ion-button (click)=\"prevSection($event)\" >\n          <<\n        </ion-button>\n        <ion-button (click)=\"nextSection($event)\" >\n            >>\n        </ion-button>\n      </div>\n    </ion-buttons> -->\n  </ion-toolbar>\n  <ion-card>\n    <ion-row>\n      <ion-col size=\"12\">\n        <div style=\"background-color: antiquewhite;\">\n          {{checklistservice.visitplanforchecklist.dateofvisit}} - {{checklistservice.visitplanforchecklist.bpartnername}} \n      </div>\n      </ion-col>\n    </ion-row>\n    \n  </ion-card>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]=\"myForm\" (ngSubmit) = \"submitForm($event)\">\n\n    <div  style=\"width: 100%;padding-top: 10px;padding-left: 10px;\" *ngFor=\"let c of curentquestionlist\">\n    <ion-item  style=\"width: 100%;padding-top: 10px;padding-left: 10px;\" *ngIf=\"!c.ishide\" >\n     \n      <ion-label class=\"ion-text-wrap\"  position=\"stacked\">\n        {{c.queno}}. {{c.question}} \n      </ion-label>\n   \n      <div style=\"width: 100%;\" [ngSwitch]=\"c.type\" [ngStyle]=\"{'background-color': c.colorbackground,'color':c.forecolor}\">\n        <ion-input *ngSwitchCase=\"'INPUT'\" [type]=\"c.inputtype ? c.inputtype:'text'\" \n        [formControlName]=\"c.questionid\" [placeholder]=\"c.placeholder\"></ion-input>\n        <ion-input style=\"text-transform: uppercase;\" *ngSwitchCase=\"'TUP'\" [type]=\"c.inputtype ? c.inputtype:'text'\" \n        [formControlName]=\"c.questionid\" [placeholder]=\"c.placeholder\" ></ion-input>\n\n        <!-- <ion-datetime *ngSwitchCase=\"'DATE'\" [max]=\"c.maxdate\" [min]=\"c.mindate\" displayFormat=\"DD-MM-YYYY\" [formControlName]=\"c.questionid\"></ion-datetime>\n        <ion-datetime *ngSwitchCase=\"'DTIME'\" [max]=\"c.maxdate\" [min]=\"c.mindate\"  displayFormat=\"DD-MM-YYYY HH:mm\" [formControlName]=\"c.questionid\"></ion-datetime> -->\n\n        <ion-item *ngSwitchCase=\"'DATE'\">\n          <ion-input placeholder=\"Select Date\" [value]=\"datequestion\"></ion-input>\n          <ion-button fill=\"clear\" id=\"open-date-input-11\">\n            <ion-icon icon=\"calendar\"></ion-icon>\n          </ion-button>\n          <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-11\" show-backdrop=\"false\">\n            <ng-template>\n              <ion-datetime\n                #popoverDatetime2\n                [formControlName]=\"c.questionid\"\n                presentation=\"date\"\n                showDefaultButtons=\"true\"\n                [max]=\"c.maxdate\" [min]=\"c.mindate\"\n                (ionChange)=\"datequestion = formatDate(popoverDatetime2.value)\"\n              ></ion-datetime>\n            </ng-template>\n          </ion-popover>\n        </ion-item>\n        <ion-item *ngSwitchCase=\"'DTIME'\">\n          <ion-input placeholder=\"Select Date and Time\" [value]=\"datequestion1\"></ion-input>\n          <ion-button fill=\"clear\" id=\"open-date-input-12\">\n            <ion-icon icon=\"calendar\"></ion-icon>\n          </ion-button>\n          <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-12\" show-backdrop=\"false\">\n            <ng-template>\n              <ion-datetime\n                #popoverDatetime2\n                [formControlName]=\"c.questionid\"\n                presentation=\"date-time\"\n                showDefaultButtons=\"true\"\n                [max]=\"c.maxdate\" [min]=\"c.mindate\"\n                (ionChange)=\"datequestion1 = formatDate1(popoverDatetime2.value)\"\n              ></ion-datetime>\n            </ng-template>\n          </ion-popover>\n        </ion-item>\n\n\n        <ion-select class=\"ion-text-wrap\" *ngSwitchCase=\"'LIST'\" [formControlName]=\"c.questionid\" interface=\"popover\"  >\n          <ion-select-option *ngFor=\"let item of c.listvalue.split(',')\" [value]=\"item\">{{item}}</ion-select-option>\n        </ion-select>\n\n        <!-- <ion-datetime displayFormat=\"h:mm A\" pickerFormat=\"h:mm A\" *ngSwitchCase=\"'TIME'\" [formControlName]=\"c.questionid\" ></ion-datetime>   -->\n        <ion-item *ngSwitchCase=\"'TIME'\">\n          <ion-input placeholder=\"Select Date and Time\" [value]=\"datequestion2\"></ion-input>\n          <ion-button fill=\"clear\" id=\"open-date-input-13\">\n            <ion-icon icon=\"calendar\"></ion-icon>\n          </ion-button>\n          <ion-popover class=\"dateTimePopover\" trigger=\"open-date-input-13\" show-backdrop=\"false\">\n            <ng-template>\n              <ion-datetime\n                #popoverDatetime2\n                [formControlName]=\"c.questionid\"\n                presentation=\"time\"\n                showDefaultButtons=\"true\"\n                (ionChange)=\"datequestion2 = formatDate2(popoverDatetime2.value)\"\n              ></ion-datetime>\n            </ng-template>\n          </ion-popover>\n        </ion-item>\n\n        <ion-row *ngSwitchCase=\"'IMAGE'\" style=\"width: 100%;padding-top: 10px;padding-left: 10px;\">\n          <app-multi-file-upload [myform]=\"myForm\" [controlID]=\"c.questionid\"  [formControlName]=\"c.questionid\"  ></app-multi-file-upload>\n        </ion-row>\n        <ion-row *ngSwitchCase=\"'IMAGEC'\" style=\"width: 100%;padding-top: 10px;padding-left: 10px;\">\n          <app-multi-file-upload [isOnlyCamera]=\"true\" [myform]=\"myForm\" [controlID]=\"c.questionid\"  [formControlName]=\"c.questionid\"  ></app-multi-file-upload>\n        </ion-row>\n        <ion-row *ngSwitchCase=\"'SL'\">\n        <ion-input  [formControlName]=\"c.questionid\" [placeholder]=\"c.placeholder\" readonly type=\"text\" ></ion-input>\n        <ion-icon style=\"font-size: 40px;\" (click)=\"OnClickSelector(c,$event)\" name=\"search\"></ion-icon>\n      </ion-row>\n      <div *ngSwitchCase=\"'MSL'\">\n        <!-- <ion-input  [formControlName]=\"c.questionid\" [placeholder]=\"c.placeholder\" readonly type=\"text\" ></ion-input>\n        <ion-icon style=\"font-size: 40px;\" (click)=\"OnClickSelector(c,$event)\"  name=\"search\"></ion-icon> -->\n         <app-listcontrolchk [myform]=\"myForm\" [formControlName]=\"c.questionid\" [question]=\"c\"></app-listcontrolchk> \n      </div>\n      <ion-row *ngSwitchCase=\"'MET'\">\n        <ion-input  [formControlName]=\"c.questionid\" [placeholder]=\"c.placeholder\" readonly type=\"text\" ></ion-input>\n        <ion-icon style=\"font-size: 40px;\" (click)=\"OnClickMultipleEntry(c,$event)\"  name=\"search\"></ion-icon>\n      </ion-row>\n      <ion-row *ngSwitchCase=\"'QRC'\">\n           \n        <ion-col size=\"8\" >\n          <ion-textarea [formControlName]=\"c.questionid\"  [placeholder]=\"c.placeholder\" readonly type=\"text\" style=\"margin-top: -30px\"></ion-textarea>\n        </ion-col>\n        <ion-col size=\"4\"> \n          <div style=\"margin-left: 16px;margin-top: -44px\">\n            <ion-fab-button color=\"light\" vertical=\"center\" size=\"large\" (click)=\"ScanQRcode(c)\">\n              <img src=\"../../../assets/QRscan_image.png\" style=\"background-color: #EFEFEF!important;\" />\n            </ion-fab-button>\n          </div>\n        </ion-col>\n      </ion-row>\n      </div>\n  \n    </ion-item>  \n    \n  </div>  \n \n    \n  </form>\n\n</ion-content>\n<ion-footer>\n  <ion-button  *ngIf=\"islast\" type=\"submit\" expand=\"block\" (click)=\"submitForm($event)\"  [disabled]=\"!myForm.valid\">Submit!</ion-button>\n  <!-- <ion-button  expand=\"block\" (click)=\"findInvalidControls()\"  >Submit!</ion-button> -->\n</ion-footer>";

/***/ })

}]);
//# sourceMappingURL=src_app_arvisitschedule_section_section_module_ts.js.map