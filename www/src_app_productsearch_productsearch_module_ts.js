"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_productsearch_productsearch_module_ts"],{

/***/ 32170:
/*!*******************************************************!*\
  !*** ./src/app/productsearch/productsearch.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProductsearchPageModule": () => (/* binding */ ProductsearchPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _productsearch_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./productsearch.page */ 64525);







const routes = [
    {
        path: '',
        component: _productsearch_page__WEBPACK_IMPORTED_MODULE_0__.ProductsearchPage
    }
];
let ProductsearchPageModule = class ProductsearchPageModule {
};
ProductsearchPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_productsearch_page__WEBPACK_IMPORTED_MODULE_0__.ProductsearchPage]
    })
], ProductsearchPageModule);



/***/ }),

/***/ 64525:
/*!*****************************************************!*\
  !*** ./src/app/productsearch/productsearch.page.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProductsearchPage": () => (/* binding */ ProductsearchPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _productsearch_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./productsearch.page.html?ngResource */ 28236);
/* harmony import */ var _productsearch_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./productsearch.page.scss?ngResource */ 74992);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);





let ProductsearchPage = class ProductsearchPage {
    constructor(loginservc) {
        this.loginservc = loginservc;
    }
    ngOnInit() {
    }
    onSearchChange(ev) {
        // Reset items back to all of the items
        this.filterproductList = this.productlist;
        const val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() !== '') {
            this.filterproductList = this.filterproductList.filter((product) => {
                return ((product.mmstMainprodname + product.mmstMainprodcode + product.attributeSetValue)
                    .toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    }
};
ProductsearchPage.ctorParameters = () => [
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService }
];
ProductsearchPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-productsearch',
        template: _productsearch_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_productsearch_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ProductsearchPage);



/***/ }),

/***/ 74992:
/*!******************************************************************!*\
  !*** ./src/app/productsearch/productsearch.page.scss?ngResource ***!
  \******************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9kdWN0c2VhcmNoLnBhZ2Uuc2NzcyJ9 */";

/***/ }),

/***/ 28236:
/*!******************************************************************!*\
  !*** ./src/app/productsearch/productsearch.page.html?ngResource ***!
  \******************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n        <ion-back-button defaultHref=\"options\"></ion-back-button>\n      </ion-buttons>\n  <ion-title>Product</ion-title>\n</ion-toolbar>\n</ion-header>\n\n<ion-content [scrollEvents]=\"true\">\n  <ion-searchbar placeholder=\"Filter BOM\"\n           (ionChange)=\"onSearchChange($event)\" [debounce]=\"250\"></ion-searchbar>\n  <ion-list>\n    <ion-item *ngFor=\"let product of filterproductList\">\n      {{ product.mmstMainprodname}}\n    </ion-item>\n  </ion-list>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_productsearch_productsearch_module_ts.js.map