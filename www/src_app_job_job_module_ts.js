"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_job_job_module_ts"],{

/***/ 34298:
/*!***********************************!*\
  !*** ./src/app/job/job.module.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "JobPageModule": () => (/* binding */ JobPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _job_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./job.page */ 58896);







const routes = [
    {
        path: '',
        component: _job_page__WEBPACK_IMPORTED_MODULE_0__.JobPage
    }
];
let JobPageModule = class JobPageModule {
};
JobPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)
        ],
        declarations: [_job_page__WEBPACK_IMPORTED_MODULE_0__.JobPage]
    })
], JobPageModule);



/***/ }),

/***/ 58896:
/*!*********************************!*\
  !*** ./src/app/job/job.page.ts ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "JobPage": () => (/* binding */ JobPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _job_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./job.page.html?ngResource */ 42690);
/* harmony import */ var _job_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./job.page.scss?ngResource */ 93451);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 93819);








let JobPage = class JobPage {
    constructor(loginservc, datePipe, route, router, loadingController) {
        this.loginservc = loginservc;
        this.datePipe = datePipe;
        this.route = route;
        this.router = router;
        this.loadingController = loadingController;
    }
    ngOnInit() {
        this.loadingController.create({
            duration: 5000,
            spinner: 'circles',
            message: 'Please Wait...'
        }).then((res) => {
            res.present();
        });
        this.route.params.subscribe(params => {
            this.jobid = params['jobid'];
            this.loginservc.getjob(this.jobid).subscribe(data => {
                const response = data['response'];
                this.job = response['data'];
                this.loginservc.getjobassignment(this.job[0].mwmsJobassignment).subscribe(dataasgn => {
                    const response1 = dataasgn['response'];
                    this.jobassignment = response1['data'];
                    this.loadingController.dismiss();
                });
            });
        });
    }
    onScan(joba) {
        this.router.navigate(['/jobdetails', this.jobid]);
    }
    onCancel() {
        this.router.navigateByUrl('/joblist');
    }
};
JobPage.ctorParameters = () => [
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_2__.LoginauthService },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_3__.DatePipe },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.ActivatedRoute },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController }
];
JobPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-job',
        template: _job_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_job_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], JobPage);



/***/ }),

/***/ 93451:
/*!**********************************************!*\
  !*** ./src/app/job/job.page.scss?ngResource ***!
  \**********************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJqb2IucGFnZS5zY3NzIn0= */";

/***/ }),

/***/ 42690:
/*!**********************************************!*\
  !*** ./src/app/job/job.page.html?ngResource ***!
  \**********************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n      <ion-buttons slot=\"start\">\n          <ion-back-button defaultHref=\"joblist\"></ion-back-button>\n        </ion-buttons>\n    <ion-title>Job</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n<ion-card>\n  <ion-item  *ngFor=\"let joba of jobassignment\">\n    <ion-card-content>\n    <ion-grid fix>\n        <ion-row>\n          <ion-col size=\"12\">\n              <ion-label position=\"stacked\">Branch</ion-label>\n              <ion-card-subtitle>{{joba.organization$_identifier}}</ion-card-subtitle>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col size=\"12\">\n              <ion-col size=\"6\">\n                  <ion-label position=\"stacked\">Vendor/Customer/Sending Branch</ion-label>\n                  <ion-card-subtitle>{{joba.bpartner$_identifier}}</ion-card-subtitle>\n              </ion-col>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"6\">\n                <ion-label position=\"stacked\">Document No.</ion-label>\n                <ion-card-subtitle>{{joba.documentno}}</ion-card-subtitle>\n            </ion-col>\n            <ion-col size=\"6\">\n                <ion-label position=\"stacked\">Doument Date</ion-label>\n                <ion-card-subtitle>{{joba.documentdate| date: 'dd/MM/yyyy'}}</ion-card-subtitle>\n                \n            </ion-col>\n        </ion-row>\n        <ion-row class=\"ion-text-right\">\n          <ion-col size=\"6\">\n              <ion-button (click)=\"onScan(joba)\">\n                  Start Scanning\n              </ion-button>\n          </ion-col>\n          <ion-col size=\"6\">\n              <ion-button (click)=\"onCancel()\">\n                  Cancel\n              </ion-button>\n          </ion-col>\n        </ion-row>\n\n      </ion-grid>\n    </ion-card-content>\n    </ion-item>\n</ion-card>\n</ion-content>\n";

/***/ })

}]);
//# sourceMappingURL=src_app_job_job_module_ts.js.map