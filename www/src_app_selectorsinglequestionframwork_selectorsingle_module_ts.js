"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_selectorsinglequestionframwork_selectorsingle_module_ts"],{

/***/ 48598:
/*!************************************************************!*\
  !*** ./src/app/arvisitschedule/arvisitschedule.service.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ArvisitscheduleService": () => (/* binding */ ArvisitscheduleService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common/generic-http-client.service */ 28475);
/* harmony import */ var _login_loginauth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../login/loginauth.service */ 44010);





let ArvisitscheduleService = class ArvisitscheduleService {
    constructor(http, loginauth, genericHTTP) {
        this.http = http;
        this.loginauth = loginauth;
        this.genericHTTP = genericHTTP;
    }
    getArVisitPlan(body) {
        //console.log("Body: ",body);
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTgetRequest', body, httpOptions);
    }
    saveArVisitPlan(body) {
        //   console.log("Body: ",body);
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTSaveARVisitPlan', body, httpOptions);
    }
    saveArVisitUnplannedPlan(body) {
        //   console.log("Body: ",body);
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTSaveUnplannedARVisitPlan', body, httpOptions);
    }
    getRequest(body) {
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + auth
            })
        };
        return this.genericHTTP.post(this.loginauth.commonurl + 'ws/in.mbs.qcinspection.MQCIgetRequest', body, httpOptions);
    }
    onSaveSectionQuestion(body) {
        let login = this.loginauth.user; //"P2admin";
        let password = this.loginauth.pass; //"Pass2020";
        const auth = btoa(login + ":" + password);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders({
                'Authorization': 'Basic ' + auth
            })
        };
        var specificHeader = { 'Authorization': 'Basic ' + auth };
        return this.genericHTTP.postformdata(this.loginauth.commonurl + 'ws/in.mbs.artracker.MARTonSaveChecklistTrx', body, specificHeader, httpOptions);
    }
};
ArvisitscheduleService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _login_loginauth_service__WEBPACK_IMPORTED_MODULE_1__.LoginauthService },
    { type: _common_generic_http_client_service__WEBPACK_IMPORTED_MODULE_0__.GenericHttpClientService }
];
ArvisitscheduleService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], ArvisitscheduleService);



/***/ }),

/***/ 37486:
/*!*********************************************************************************!*\
  !*** ./src/app/selectorsinglequestionframwork/selectorsingle-routing.module.ts ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SelectorsinglePageRoutingModule": () => (/* binding */ SelectorsinglePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _selectorsingle_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./selectorsingle.page */ 43408);




const routes = [
    {
        path: '',
        component: _selectorsingle_page__WEBPACK_IMPORTED_MODULE_0__.SelectorsinglePage
    }
];
let SelectorsinglePageRoutingModule = class SelectorsinglePageRoutingModule {
};
SelectorsinglePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SelectorsinglePageRoutingModule);



/***/ }),

/***/ 38861:
/*!*************************************************************************!*\
  !*** ./src/app/selectorsinglequestionframwork/selectorsingle.module.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SelectorsinglePageModule": () => (/* binding */ SelectorsinglePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ionic-selectable */ 25073);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _selectorsingle_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./selectorsingle-routing.module */ 37486);
/* harmony import */ var _selectorsingle_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./selectorsingle.page */ 43408);








let SelectorsinglePageModule = class SelectorsinglePageModule {
};
SelectorsinglePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _selectorsingle_routing_module__WEBPACK_IMPORTED_MODULE_0__.SelectorsinglePageRoutingModule,
            ionic_selectable__WEBPACK_IMPORTED_MODULE_7__.IonicSelectableModule
        ],
        declarations: [_selectorsingle_page__WEBPACK_IMPORTED_MODULE_1__.SelectorsinglePage]
    })
], SelectorsinglePageModule);



/***/ }),

/***/ 43408:
/*!***********************************************************************!*\
  !*** ./src/app/selectorsinglequestionframwork/selectorsingle.page.ts ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SelectorsinglePage": () => (/* binding */ SelectorsinglePage)
/* harmony export */ });
/* harmony import */ var _home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _selectorsingle_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./selectorsingle.page.html?ngResource */ 32111);
/* harmony import */ var _selectorsingle_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./selectorsingle.page.scss?ngResource */ 36824);
/* harmony import */ var _arvisitschedule_arvisitschedule_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../arvisitschedule/arvisitschedule.service */ 48598);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _selectorsingle_selectorsingleservice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../selectorsingle/selectorsingleservice.service */ 67296);








let SelectorsinglePage = class SelectorsinglePage {
  constructor(singleselservc, route, router, checklistservice) {
    this.singleselservc = singleselservc;
    this.route = route;
    this.router = router;
    this.checklistservice = checklistservice;

    this.getpagenofromlist = function (jolist) {
      let pagerow = 0;

      for (let item of jolist) {
        pagerow = pagerow + 1;
      }

      return pagerow;
    };
  }

  ngOnInit() {
    this.txtCaption = 'Select Value';
    this.route.params.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.question = this.router.getCurrentNavigation().extras.state.question;

        if (this.question.type === 'MSL') {
          this.ismultiselect = true;
        } else {
          this.ismultiselect = false;
        }

        this.singleselservc.json['action'] = 'getselctordata';
        this.singleselservc.json['queid'] = this.question.questionid;
        this.singleselservc.json['orgid'] = '0';
        this.singleselservc.json['text'] = '';
        this.singleselservc.json['offset'] = 0;
        this.singleselservc.json['quemaster'] = this.checklistservice.selectedinspection;
        this.singleselservc.pageOffset = 0;
        this.checklistservice.getRequest(this.singleselservc.json).subscribe(data => {
          var response = data;
          this.filterdata = response.data;
          this.pagerow = this.getpagenofromlist(data['data']);
          this.totalrow = response.totalRows;
          this.id1.open();
          console.log("filterdata", this.filterdata); // 
        });
      }
    });
  }

  onSubmitSingleSelection() {
    if (this.ismultiselect) {
      let strmultivaluedata = '';

      for (let value of this.selecteddata) {
        strmultivaluedata = strmultivaluedata + '\'' + value.name + '\',';
      }

      strmultivaluedata = '(' + strmultivaluedata.substring(0, strmultivaluedata.length - 1) + ')';
      this.question.ans = strmultivaluedata; // console.log(strmultivaluedata);
    } else {
      //let que = this.getQueDetFromQueID(this.question.questionid)
      this.question.ans = this.selecteddata.name;
    }

    console.log(this.checklistservice.selectedinspection);
    let navigationExtras = {
      state: {
        question: this.question
      }
    };
    this.router.navigate(['section'], navigationExtras);
  }

  getQueDetFromQueID(key) {
    for (let section of this.checklistservice.selectedinspection.sections) {
      //  console.log('section',section);
      for (let que of section.questions) {
        if (key === que.questionid) {
          return que;
        }
      }
    }
  }

  onSearchChange(event) {
    let text = event.text.trim().toLowerCase();
    this.singleselservc.json['offset'] = 0;
    this.singleselservc.json['quemaster'] = this.checklistservice.selectedinspection;
    this.singleselservc.json['action'] = 'getselctordata';
    this.singleselservc.json['queid'] = this.question.questionid;
    this.singleselservc.json['orgid'] = '0'; // Reset items back to all of the items
    //this.filterdata = this.data;
    // if the value is an empty string don't filter the items

    if (text && text.trim() !== '') {
      if (text.length > 2) {
        this.singleselservc.json['text'] = text;
        this.checklistservice.getRequest(this.singleselservc.json).subscribe(data => {
          var response = data;
          this.filterdata = response.data;
          this.pagerow = this.getpagenofromlist(data['data']);
          this.totalrow = response.totalRows;
          console.log("searchdata", this.filterdata); // 
        });
      }
    } else {
      this.singleselservc.json['text'] = text;
      this.checklistservice.getRequest(this.singleselservc.json).subscribe(data => {
        var response = data;
        this.filterdata = response.data;
        this.pagerow = this.getpagenofromlist(data['data']);
        this.totalrow = response.totalRows;
        console.log("searchdata", this.filterdata); // 
      });
    }
  }

  doInfinite(event) {
    var _this = this;

    return (0,_home_openbravo_ionicworkspace_mpower_ionic6_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        //Set this initial 0 at top 
        let text = (event.text || '').trim().toLowerCase();

        if (_this.pagerow === 20 && _this.totalrow > 20) {
          _this.singleselservc.pageOffset = _this.singleselservc.pageOffset + 20;
          _this.singleselservc.json['offset'] = _this.singleselservc.pageOffset;
          _this.singleselservc.json['quemaster'] = _this.checklistservice.selectedinspection;
          _this.singleselservc.json['action'] = 'getselctordata';
          _this.singleselservc.json['queid'] = _this.question.questionid;
          _this.singleselservc.json['orgid'] = '0';

          if (text) {
            _this.singleselservc.json['text'] = text;
          }

          let tempData = yield (yield _this.checklistservice.getRequest(_this.singleselservc.json)).toPromise();

          if (!!tempData) {
            // This is your page varible where you bind data
            _this.data = _this.data.concat(tempData['data']);
            _this.pagerow = _this.getpagenofromlist(tempData['data']);
            _this.filterdata = _this.filterdata.concat(tempData['data']);
            _this.totalrow = tempData['totalRows'];
            event.component.endInfiniteScroll();
          } // console.log(this.filterjoblist.length+'  '+this.totalrow);

        }

        if (_this.filterdata.length === _this.totalrow) {
          event.component.disableInfiniteScroll();
          return;
        }
      } catch (error) {}
    })();
  }

};

SelectorsinglePage.ctorParameters = () => [{
  type: _selectorsingle_selectorsingleservice_service__WEBPACK_IMPORTED_MODULE_4__.SelectorsingleserviceService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.ActivatedRoute
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router
}, {
  type: _arvisitschedule_arvisitschedule_service__WEBPACK_IMPORTED_MODULE_3__.ArvisitscheduleService
}];

SelectorsinglePage.propDecorators = {
  id1: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.ViewChild,
    args: ["id1", {
      static: false
    }]
  }]
};
SelectorsinglePage = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
  selector: 'app-selectorsingle',
  template: _selectorsingle_page_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_selectorsingle_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], SelectorsinglePage);


/***/ }),

/***/ 36824:
/*!************************************************************************************!*\
  !*** ./src/app/selectorsinglequestionframwork/selectorsingle.page.scss?ngResource ***!
  \************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzZWxlY3RvcnNpbmdsZS5wYWdlLnNjc3MifQ== */";

/***/ }),

/***/ 32111:
/*!************************************************************************************!*\
  !*** ./src/app/selectorsinglequestionframwork/selectorsingle.page.html?ngResource ***!
  \************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Selection</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"section\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  \n  <ion-item>\n    <ion-label  color=\"primary\" position=\"stacked\" >{{txtCaption}}</ion-label>\n    <ionic-selectable #id1 [(ngModel)]=\"selecteddata\" itemValueField=\"name\" itemTextField=\"name\" [items]=\"filterdata\"\n      [canSearch]=\"true\" [hasInfiniteScroll]=\"true\" (onSearch)=\"onSearchChange($event)\" (onClose)=\"onSubmitSingleSelection()\"\n      (onInfiniteScroll)=\"doInfinite($event)\" [isMultiple]=\"ismultiselect\">\n    </ionic-selectable>\n  </ion-item>\n  <ion-button size=\"small\" shape=\"round\" fill=\"outline\" (click)=\"onSubmitSingleSelection()\">\n    Submit\n  </ion-button>\n  \n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_selectorsinglequestionframwork_selectorsingle_module_ts.js.map