import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { ScreenOrientation } from '@awesome-cordova-plugins/screen-orientation/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@awesome-cordova-plugins/splash-screen/ngx';
import { StatusBar } from '@awesome-cordova-plugins/status-bar/ngx';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginauthService } from './login/loginauth.service';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { IonicStorageModule } from '@ionic/storage-angular';
import { SecureStorage } from '@ionic-native/secure-storage/ngx';
import { DatePipe } from '@angular/common';
import { ImagePicker } from '@awesome-cordova-plugins/image-picker/ngx';
import { Camera } from '@awesome-cordova-plugins/camera/ngx';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Validator } from 'src/provider/validator-helper';
import { IonicSelectableModule } from 'ionic-selectable';
import { Commonfun } from '../provider/commonfun';
import { PinCheck } from '@ionic-native/pin-check/ngx';
import { Geolocation } from "@awesome-cordova-plugins/geolocation/ngx";
import { LocationAccuracy } from '@awesome-cordova-plugins/location-accuracy/ngx';
import { NativeGeocoder } from '@awesome-cordova-plugins/native-geocoder/ngx';
import { HTTP } from '@awesome-cordova-plugins/http/ngx';
import { Message } from '../provider/message-helper';
import { NeworderPage } from './neworder/neworder.page';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { Market } from '@ionic-native/market/ngx';
import { LoginPage } from './login/login.page';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { FileTransfer } from '@awesome-cordova-plugins/file-transfer/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { PipesModule } from './common/pipes/pipes.module';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';
import { FilePath } from '@awesome-cordova-plugins/file-path/ngx';
import { IOSFilePicker } from '@ionic-native/file-picker/ngx';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Crop } from '@ionic-native/crop/ngx';
import { BarcodeScanner } from '@awesome-cordova-plugins/barcode-scanner/ngx';
import { MatStepperModule } from '@angular/material/stepper';



@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, HttpClientModule, FormsModule,
    ReactiveFormsModule, IonicSelectableModule,
    MaterialModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatStepperModule,
    Ng2SearchPipeModule,
    IonicModule.forRoot({ mode: 'ios' }), AppRoutingModule, IonicStorageModule.forRoot(), BrowserAnimationsModule],
  providers: [
    StatusBar, LoginPage,
    SplashScreen,
    LoginauthService,
    FingerprintAIO,
    Geolocation,
    LocationAccuracy,
    NativeGeocoder,
    SecureStorage,
    DatePipe, PipesModule,
    ImagePicker, Message,
    PinCheck,
    HTTP,
    Market,
    AndroidPermissions,
    ScreenOrientation,
    FilePath,
    BarcodeScanner,
    Camera, Validator, Commonfun, NeworderPage, SocialSharing, FileTransfer, File, FileOpener,
    Crop,
    InAppBrowser, FileChooser, IOSFilePicker,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
